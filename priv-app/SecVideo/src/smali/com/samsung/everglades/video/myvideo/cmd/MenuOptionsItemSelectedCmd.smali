.class public Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
.super Ljava/lang/Object;
.source "MenuOptionsItemSelectedCmd.java"

# interfaces
.implements Lcom/samsung/everglades/video/myvideo/cmd/ICommand;


# static fields
.field private static final DELAY_DISSMISS_PROGRESS_DIALOG:I = 0xfa0

.field private static final HANDLE_DISSMISS_PROGRESS_DIALOG:I

.field private static uniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->uniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 835
    new-instance v0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$15;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$15;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mHandler:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->updateAutoPlay(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->openVideoEditorDownloadPageOnSamsungApps()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->openSamsungAppsInstallPage()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewBy(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)Lcom/samsung/everglades/video/myvideo/common/ListType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->sortBy(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewAsCloudOption(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showProgressDialog()V

    return-void
.end method

.method private cancelSelectionMode()V
    .locals 2

    .prologue
    .line 696
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 697
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 698
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->stopActionMode()V

    .line 700
    :cond_0
    return-void
.end method

.method private createPersonalPopup(Landroid/app/AlertDialog$Builder;I)Landroid/app/AlertDialog$Builder;
    .locals 2
    .param p1, "popup"    # Landroid/app/AlertDialog$Builder;
    .param p2, "sortValue"    # I

    .prologue
    .line 576
    const/4 v0, 0x0

    .line 577
    .local v0, "sortOption":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 578
    :cond_0
    const v0, 0x7f060005

    .line 583
    :goto_0
    new-instance v1, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$8;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$8;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {p1, v0, p2, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 603
    return-object p1

    .line 580
    :cond_1
    const v0, 0x7f060008

    goto :goto_0
.end method

.method private createPersonalViewOptionPopup(Landroid/app/AlertDialog$Builder;I)Landroid/app/AlertDialog$Builder;
    .locals 2
    .param p1, "popup"    # Landroid/app/AlertDialog$Builder;
    .param p2, "sortValue"    # I

    .prologue
    .line 653
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    const v0, 0x7f06000a

    .line 661
    .local v0, "rsid":I
    :goto_0
    new-instance v1, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$10;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$10;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {p1, v0, p2, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 670
    return-object p1

    .line 655
    .end local v0    # "rsid":I
    :cond_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isKorSKTModel()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isTcloudVideoAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 656
    const v0, 0x7f06000b

    .restart local v0    # "rsid":I
    goto :goto_0

    .line 658
    .end local v0    # "rsid":I
    :cond_1
    const v0, 0x7f060009

    .restart local v0    # "rsid":I
    goto :goto_0
.end method

.method private detailsMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 686
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 687
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performDetails(Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 688
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 863
    :cond_0
    return-void
.end method

.method private getAutoStateString(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isOn"    # Z

    .prologue
    .line 199
    if-eqz p2, :cond_0

    const v0, 0x7f0c000a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f0c0009

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->uniqueInstance:Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;

    return-object v0
.end method

.method private handleVideoEditorCase()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v0

    const/16 v1, 0x6b

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 229
    return-void
.end method

.method private openSamsungAppsInstallPage()V
    .locals 5

    .prologue
    .line 280
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 281
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 282
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "http://apps.samsung.com/mw/apps311.as"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 284
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 291
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 285
    .restart local v1    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 287
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 288
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private openVideoEditorDownloadPageOnSamsungApps()V
    .locals 5

    .prologue
    .line 260
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 262
    .local v1, "i":Landroid/content/Intent;
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/Features;->VIDEO_EDITOR_FULL:Z

    if-eqz v2, :cond_0

    .line 263
    const-string v2, "samsungapps://ProductDetail/com.sec.android.app.vefull"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 268
    :goto_0
    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 271
    :try_start_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_1
    return-void

    .line 265
    :cond_0
    const-string v2, "samsungapps://ProductDetail/com.sec.android.app.ve"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Activity Not found!!!"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 274
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 275
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private renameMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 691
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getFirstItem()Landroid/net/Uri;

    move-result-object v0

    .line 692
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/dialogs/RenameDialog;->showDialog()V

    .line 693
    return-void
.end method

.method private selectMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 2
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 465
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 467
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 468
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectContentViewMode(Z)V

    .line 470
    :cond_0
    return-void
.end method

.method private sendToAnotherDeviceMenu()V
    .locals 6

    .prologue
    .line 771
    const/4 v1, 0x0

    .line 772
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 773
    .local v2, "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    if-eqz v2, :cond_0

    .line 774
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performSendToAnotherDevice(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/content/Intent;

    move-result-object v1

    .line 775
    if-eqz v1, :cond_1

    .line 777
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 787
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->cancelSelectionMode()V

    .line 788
    return-void

    .line 778
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Activity Not found!!!"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 780
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 781
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v4, "Target Activity Not Found"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 784
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const-string v3, "fail to send to another device"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showEditPopup()V
    .locals 3

    .prologue
    .line 728
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c0060

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 730
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const/high16 v1, 0x7f060000

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$11;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 746
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mDialog:Landroid/app/AlertDialog;

    .line 747
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 748
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 749
    return-void
.end method

.method private showProgressDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 846
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0026

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 848
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 849
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 850
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 852
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 853
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 855
    :cond_1
    return-void
.end method

.method private showSamsungAppsDownloadDialog()V
    .locals 3

    .prologue
    .line 246
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c0037

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c00b3

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0097

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$4;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$4;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 257
    return-void
.end method

.method private showVideoEditorDownloadDialog()V
    .locals 3

    .prologue
    .line 232
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c0037

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0100

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0036

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$3;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$3;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 243
    return-void
.end method

.method private showVideoEditorPopup()V
    .locals 3

    .prologue
    .line 752
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c0060

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 754
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f060001

    new-instance v2, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$12;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$12;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 767
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 768
    return-void
.end method

.method private sortBy(I)V
    .locals 4
    .param p1, "which"    # I

    .prologue
    .line 607
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 608
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "allsharesortorder"

    invoke-virtual {v2, v3, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 615
    :goto_0
    sget-boolean v2, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v2, :cond_4

    .line 616
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 617
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_1

    .line 618
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setGridSort()V

    .line 627
    .end local v0    # "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    :cond_1
    :goto_1
    return-void

    .line 609
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 610
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "slinksortorder"

    invoke-virtual {v2, v3, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    goto :goto_0

    .line 612
    :cond_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "slinksortorder"

    invoke-virtual {v2, v3, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 613
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    invoke-virtual {v2, v3, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    goto :goto_0

    .line 621
    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.videoplayer.SORT_BY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 622
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "sortby"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 623
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 625
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->callOptionChanged()V

    goto :goto_1
.end method

.method private updateAutoPlay(Z)V
    .locals 5
    .param p1, "isOn"    # Z

    .prologue
    .line 204
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "autoPlay"

    invoke-virtual {v1, v2, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;Z)V

    .line 205
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    const v4, 0x7f0c000b

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-direct {p0, v3, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->getAutoStateString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.videoplayer.AUTO_PLAY_NEXT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "autoplay"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 209
    return-void
.end method

.method private viewAsCloudOption(I)V
    .locals 3
    .param p1, "which"    # I

    .prologue
    .line 674
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastCloudViewOption"

    invoke-virtual {v1, v2, p1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 676
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.videoplayer.CLOUD_OPTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 677
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "cloud_option"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 678
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 680
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->callOptionChanged()V

    .line 681
    return-void
.end method

.method private viewBy(I)V
    .locals 7
    .param p1, "which"    # I

    .prologue
    const/4 v6, 0x2

    .line 509
    const/4 v2, -0x1

    .line 511
    .local v2, "listType":I
    if-nez p1, :cond_2

    .line 512
    const/4 v2, 0x0

    .line 519
    :goto_0
    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    .line 520
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v4

    const-string v5, "lastViewAs"

    invoke-virtual {v4, v5, v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->saveState(Ljava/lang/String;I)V

    .line 521
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4, v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->setType(I)V

    .line 522
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "viewBy() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 524
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 525
    .local v0, "f":Landroid/app/Fragment;
    sget-boolean v4, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    instance-of v4, v4, Lcom/samsung/everglades/video/VideoMain;

    if-eqz v4, :cond_0

    .line 526
    if-ne v2, v6, :cond_4

    instance-of v4, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-nez v4, :cond_4

    .line 527
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/samsung/everglades/video/VideoMain;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/VideoMain;->switchFragment()V

    .line 532
    :cond_0
    :goto_1
    sget-object v0, Lcom/samsung/everglades/video/VideoMain;->mListFragment:Landroid/app/Fragment;

    .line 533
    instance-of v4, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v4, :cond_5

    move-object v1, v0

    .line 534
    check-cast v1, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    .line 535
    .local v1, "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;->changeListType()V

    .line 542
    .end local v0    # "f":Landroid/app/Fragment;
    .end local v1    # "folder":Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;
    :cond_1
    :goto_2
    return-void

    .line 513
    :cond_2
    const/4 v4, 0x1

    if-ne p1, v4, :cond_3

    .line 514
    const/4 v2, 0x1

    goto :goto_0

    .line 516
    :cond_3
    const/4 v2, 0x2

    goto :goto_0

    .line 528
    .restart local v0    # "f":Landroid/app/Fragment;
    :cond_4
    if-eq v2, v6, :cond_0

    instance-of v4, v0, Lcom/samsung/everglades/video/myvideo/fragments/FolderListFragment;

    if-eqz v4, :cond_0

    .line 529
    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/samsung/everglades/video/VideoMain;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/VideoMain;->switchFragment()V

    goto :goto_1

    .line 536
    :cond_5
    instance-of v4, v0, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    if-eqz v4, :cond_1

    move-object v3, v0

    .line 537
    check-cast v3, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    .line 538
    .local v3, "videoList":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setListView()V

    .line 539
    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->restartLoader()V

    goto :goto_2
.end method


# virtual methods
.method public autoPlayMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 149
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v3

    const-string v4, "autoPlay"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 152
    .local v0, "isOn":Z
    if-eqz v0, :cond_0

    .line 153
    const/4 v2, 0x0

    .line 158
    .local v2, "setValue":I
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 159
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0c000b

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 161
    const v3, 0x7f060006

    new-instance v4, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;

    invoke-direct {v4, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$1;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v1, v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 187
    const v3, 0x7f0c000d

    new-instance v4, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$2;

    invoke-direct {v4, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$2;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 194
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 196
    return-void

    .line 155
    .end local v1    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v2    # "setValue":I
    :cond_0
    const/4 v2, 0x1

    .restart local v2    # "setValue":I
    goto :goto_0
.end method

.method public callVideoEditorMenu()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.ve"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.vefull"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->handleVideoEditorCase()V

    .line 224
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->cancelSelectionMode()V

    .line 225
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.app.samsungapps"

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showVideoEditorDownloadDialog()V

    goto :goto_0

    .line 221
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showSamsungAppsDownloadDialog()V

    goto :goto_0
.end method

.method public cancelMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 1
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 297
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 298
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 299
    return-void
.end method

.method public deleteMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 4
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 305
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 307
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x65

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 308
    return-void
.end method

.method public doneMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 4
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 314
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 315
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    .line 317
    .local v1, "listType":Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isShareViaAttr()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 318
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x68

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isVideoEditorAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 320
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x6b

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    goto :goto_0

    .line 321
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isCloudDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 322
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x6d

    invoke-virtual {v2, v3, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    goto :goto_0

    .line 323
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 324
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x6a

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0

    .line 325
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isMoveKNOXAttr()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 326
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x73

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0

    .line 327
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isRemoveKNOXAttr()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 328
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x74

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0

    .line 329
    :cond_6
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 330
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x75

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0
.end method

.method public editMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v5, 0x1

    .line 703
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    .line 704
    .local v1, "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    const-string v4, "com.sec.android.app.storycam"

    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPackageAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 705
    .local v0, "isAvailableVideoClip":Z
    if-eqz v1, :cond_1

    .line 706
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getSelectedItemCount()I

    move-result v2

    .line 707
    .local v2, "selectedItemsCount":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 708
    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getNumberOfItemFromFolers()I

    move-result v2

    .line 710
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "editMenu() : isAvailableVideoClip = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " selectedItemsCount = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 711
    if-eqz v0, :cond_3

    .line 712
    const/16 v3, 0xf

    if-gt v2, v3, :cond_2

    .line 713
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showEditPopup()V

    .line 725
    .end local v2    # "selectedItemsCount":I
    :cond_1
    :goto_0
    return-void

    .line 715
    .restart local v2    # "selectedItemsCount":I
    :cond_2
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showVideoEditorPopup()V

    goto :goto_0

    .line 718
    :cond_3
    if-ne v2, v5, :cond_4

    .line 719
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showEditPopup()V

    goto :goto_0

    .line 721
    :cond_4
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showVideoEditorPopup()V

    goto :goto_0
.end method

.method public execute(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 5
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    .line 62
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    .line 63
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 64
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 65
    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 67
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getMenuItemId()I

    move-result v0

    .line 69
    .local v0, "menuId":I
    const v1, 0x7f09006e

    if-ne v0, v1, :cond_1

    .line 70
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->searchMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const v1, 0x7f090079

    if-ne v0, v1, :cond_2

    .line 72
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->autoPlayMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 73
    :cond_2
    const v1, 0x7f09007f

    if-ne v0, v1, :cond_3

    .line 74
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->callVideoEditorMenu()V

    goto :goto_0

    .line 75
    :cond_3
    const v1, 0x7f090071

    if-ne v0, v1, :cond_4

    .line 76
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->deleteMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 77
    :cond_4
    const v1, 0x7f090075

    if-ne v0, v1, :cond_5

    .line 78
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->openDeleteListMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 79
    :cond_5
    const v1, 0x7f09007c

    if-ne v0, v1, :cond_6

    .line 80
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->openFileDownloadListMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 81
    :cond_6
    const v1, 0x7f090082

    if-ne v0, v1, :cond_7

    .line 82
    invoke-virtual {p0, p1, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->moveKnoxMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V

    goto :goto_0

    .line 83
    :cond_7
    const v1, 0x7f090083

    if-ne v0, v1, :cond_8

    .line 84
    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->moveKnoxMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V

    goto :goto_0

    .line 85
    :cond_8
    const v1, 0x7f090080

    if-ne v0, v1, :cond_9

    .line 86
    invoke-virtual {p0, p1, v3}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->movePrivateMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V

    goto :goto_0

    .line 87
    :cond_9
    const v1, 0x7f090081

    if-ne v0, v1, :cond_a

    .line 88
    invoke-virtual {p0, p1, v4}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->movePrivateMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V

    goto :goto_0

    .line 89
    :cond_a
    const v1, 0x7f09007b

    if-ne v0, v1, :cond_b

    .line 90
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->shareViaMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 91
    :cond_b
    const v1, 0x7f09003f

    if-eq v0, v1, :cond_c

    const v1, 0x7f09003d

    if-ne v0, v1, :cond_d

    .line 92
    :cond_c
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->selectAllMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 93
    :cond_d
    const v1, 0x7f09006f

    if-ne v0, v1, :cond_e

    .line 94
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->selectMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 95
    :cond_e
    const v1, 0x7f090077

    if-ne v0, v1, :cond_f

    .line 96
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewByMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto :goto_0

    .line 97
    :cond_f
    const v1, 0x7f090070

    if-ne v0, v1, :cond_10

    .line 98
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->sortByMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto/16 :goto_0

    .line 99
    :cond_10
    const v1, 0x7f090078

    if-ne v0, v1, :cond_11

    .line 100
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->viewOptionMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto/16 :goto_0

    .line 101
    :cond_11
    const v1, 0x7f090086

    if-ne v0, v1, :cond_12

    .line 102
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->detailsMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto/16 :goto_0

    .line 103
    :cond_12
    const v1, 0x7f090085

    if-ne v0, v1, :cond_13

    .line 104
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->renameMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto/16 :goto_0

    .line 105
    :cond_13
    const v1, 0x7f090076

    if-ne v0, v1, :cond_14

    .line 106
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    const-string v2, "com.vcast.mediamanager.ACTION_VIDEOS"

    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 107
    :cond_14
    const v1, 0x7f09007e

    if-ne v0, v1, :cond_15

    .line 108
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x77

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto/16 :goto_0

    .line 109
    :cond_15
    const v1, 0x7f09007d

    if-ne v0, v1, :cond_16

    .line 110
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->editMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V

    goto/16 :goto_0

    .line 111
    :cond_16
    const v1, 0x7f090084

    if-ne v0, v1, :cond_17

    .line 112
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->sendToAnotherDeviceMenu()V

    goto/16 :goto_0

    .line 113
    :cond_17
    const v1, 0x7f090073

    if-ne v0, v1, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->showPopupDeregistered()V

    goto/16 :goto_0
.end method

.method public moveKnoxMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V
    .locals 4
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .param p2, "moveDirection"    # I

    .prologue
    .line 365
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 366
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCount()I

    move-result v1

    .line 368
    .local v1, "count":I
    if-lez v1, :cond_1

    .line 369
    if-nez p2, :cond_2

    .line 370
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x73

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    .line 374
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->cancelSelectionMode()V

    .line 376
    :cond_1
    return-void

    .line 371
    :cond_2
    const/4 v2, 0x1

    if-ne p2, v2, :cond_0

    .line 372
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    const/16 v3, 0x74

    invoke-virtual {v2, v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0
.end method

.method public movePrivateMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;I)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;
    .param p2, "moveDirection"    # I

    .prologue
    .line 382
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 384
    .local v0, "context":Landroid/content/Context;
    if-nez p2, :cond_2

    .line 385
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x70

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    .line 397
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->cancelSelectionMode()V

    .line 398
    return-void

    .line 388
    :cond_1
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x6f

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0

    .line 390
    :cond_2
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 391
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 392
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x72

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0

    .line 394
    :cond_3
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    const/16 v2, 0x71

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(I)V

    goto :goto_0
.end method

.method public openDeleteListMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 2
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 338
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 339
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setSelectionModeforDelete()V

    .line 342
    :cond_0
    return-void
.end method

.method public openFileDownloadListMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 3
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "downloadCmd":I
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    const/16 v0, 0x6a

    .line 358
    :goto_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 359
    :goto_1
    return-void

    .line 351
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    const/16 v0, 0x75

    goto :goto_0

    .line 354
    :cond_1
    const/16 v0, 0x6d

    .line 355
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performCloudContentDownload(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    goto :goto_1
.end method

.method public searchMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 7
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getListType()Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v2

    .line 121
    .local v2, "listtype":Lcom/samsung/everglades/video/myvideo/common/ListType;
    const/4 v4, -0x1

    .line 122
    .local v4, "type":I
    const/4 v3, -0x1

    .line 124
    .local v3, "slink_id":I
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 125
    const/16 v4, 0xd

    .line 134
    :goto_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/everglades/video/myvideo/activities/SearchList;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 135
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "list_type"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 136
    const-string v5, "s_link_device_id"

    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    const/high16 v5, 0x24000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 138
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 126
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 127
    const/16 v4, 0xe

    .line 128
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getSLinkDeviceId()I

    move-result v3

    goto :goto_0

    .line 130
    :cond_1
    const/4 v4, 0x6

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public selectAllMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 447
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 448
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getIsChecked()Z

    move-result v3

    .line 449
    .local v3, "isChecked":Z
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 450
    .local v1, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    .line 452
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    if-eqz v3, :cond_0

    .line 454
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v4, v1, v5, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->selectAll(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;Landroid/widget/BaseAdapter;)V

    .line 459
    :goto_0
    return-void

    .line 457
    :cond_0
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->deSelectAll(Landroid/widget/BaseAdapter;)V

    goto :goto_0
.end method

.method public shareViaMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 6
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 404
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v2

    .line 405
    .local v2, "ms":Lcom/samsung/everglades/video/myvideo/list/MultiSelector;
    if-eqz v2, :cond_0

    .line 406
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 407
    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->performSlinkShareVia()Landroid/content/Intent;

    move-result-object v1

    .line 408
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_1

    .line 410
    :try_start_0
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mActivity:Landroid/app/Activity;

    const/16 v4, 0x64

    invoke-virtual {v3, v1, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    :goto_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->cancelSelectionMode()V

    .line 424
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 411
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 412
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Activity Not found!!!"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 413
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 414
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const-string v4, "Target Activity Not Found"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 417
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const-string v3, "fail to slink share via"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 421
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    const/16 v3, 0x68

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->perform(ILcom/samsung/everglades/video/myvideo/common/ListType;)V

    goto :goto_1
.end method

.method public shortcutToPopupMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 5
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 430
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 432
    .local v0, "activity":Landroid/app/Activity;
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/samsung/everglades/video/VideoMain;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 433
    .local v2, "shortcutIntent":Landroid/content/Intent;
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 435
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 436
    .local v1, "addIntent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 437
    const-string v3, "android.intent.extra.shortcut.NAME"

    const-string v4, "Video"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    const/high16 v4, 0x7f030000

    invoke-static {v0, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 439
    const-string v3, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 440
    invoke-virtual {v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 441
    return-void
.end method

.method public showPopupDeregistered()V
    .locals 4

    .prologue
    .line 792
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 793
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    .line 795
    .local v1, "mSLinkUtil":Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;
    const v2, 0x7f0c00a4

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 796
    const v2, 0x7f0c0025

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 797
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$13;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$13;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 804
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0024

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$14;

    invoke-direct {v3, p0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$14;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 830
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mDialog:Landroid/app/AlertDialog;

    .line 831
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 833
    return-void
.end method

.method public sortByMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 5
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    const/4 v4, 0x0

    .line 548
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    .line 549
    const/4 v1, 0x0

    .line 550
    .local v1, "sortValue":I
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyDownloadAttr()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 551
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "allsharesortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 552
    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    .line 553
    add-int/lit8 v1, v1, -0x2

    .line 561
    :cond_1
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 562
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c00be

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 563
    invoke-direct {p0, v0, v1}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->createPersonalPopup(Landroid/app/AlertDialog$Builder;I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 565
    const v2, 0x7f0c000d

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$7;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$7;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 572
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 573
    return-void

    .line 555
    .end local v0    # "popup":Landroid/app/AlertDialog$Builder;
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 556
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "slinksortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    .line 558
    :cond_3
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "sortorder"

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public viewByMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 5
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 476
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    .line 478
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastViewAs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    .line 480
    .local v1, "viewValue":I
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 481
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0c006e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 483
    const v2, 0x7f06000c

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$5;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 498
    const v2, 0x7f0c000d

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$6;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$6;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 506
    return-void
.end method

.method public viewOptionMenu(Lcom/samsung/everglades/video/myvideo/cmd/Args;)V
    .locals 5
    .param p1, "args"    # Lcom/samsung/everglades/video/myvideo/cmd/Args;

    .prologue
    .line 633
    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/cmd/Args;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    .line 635
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 636
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0c005c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 638
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v2

    const-string v3, "lastCloudViewOption"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v0

    .line 639
    .local v0, "optionValue":I
    invoke-direct {p0, v1, v0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;->createPersonalViewOptionPopup(Landroid/app/AlertDialog$Builder;I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 641
    const v2, 0x7f0c000d

    new-instance v3, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$9;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd$9;-><init>(Lcom/samsung/everglades/video/myvideo/cmd/MenuOptionsItemSelectedCmd;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 648
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 649
    return-void
.end method
