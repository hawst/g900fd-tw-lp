.class Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;
.super Ljava/lang/Object;
.source "DetailsDialogForMultiSelector.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->setOnUpdateTotalSizeListener(Lcom/samsung/everglades/video/myvideo/list/MultiSelector$OnUpdateTotalSizeListener;)V

    .line 72
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mContext:Landroid/content/Context;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$002(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Landroid/content/Context;)Landroid/content/Context;

    .line 73
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$102(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Lcom/samsung/everglades/video/myvideo/common/ListType;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 74
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mSelectedItemCount:I
    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$202(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;I)I

    .line 75
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$2;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    const-wide/16 v2, 0x0

    # setter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J
    invoke-static {v0, v2, v3}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$302(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;J)J

    .line 76
    return-void
.end method
