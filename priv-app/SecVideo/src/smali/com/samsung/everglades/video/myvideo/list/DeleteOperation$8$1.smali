.class Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;
.super Ljava/lang/Object;
.source "DeleteOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$500(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$500(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->setAbortOperation(Z)V

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;

    iget-object v0, v0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$8;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 289
    :cond_1
    return-void
.end method
