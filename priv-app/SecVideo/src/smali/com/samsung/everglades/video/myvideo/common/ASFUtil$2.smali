.class Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;
.super Ljava/lang/Object;
.source "ASFUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/common/ASFUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V
    .locals 0

    .prologue
    .line 407
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 1
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 422
    const-string v0, "mRemoteDeviceDiscoveryListener - onDeviceAdded"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteDeviceList()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    .line 424
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 410
    const-string v0, "mRemoteDeviceDiscoveryListener - onDeviceRemoved"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # invokes: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->updateRemoteDeviceList()V
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$400(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)V

    .line 414
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$500(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/Device;

    move-result-object v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mRemoteDeviceList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$600(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    # getter for: Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->mSelectedRemoteDevice:Lcom/samsung/android/allshare/Device;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->access$500(Lcom/samsung/everglades/video/myvideo/common/ASFUtil;)Lcom/samsung/android/allshare/Device;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    const-string v0, "mRemoteDeviceDiscoveryListener - Selected Device is disconnected"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/ASFUtil$2;->this$0:Lcom/samsung/everglades/video/myvideo/common/ASFUtil;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/everglades/video/myvideo/common/ASFUtil;->notifySettingChanged(II)V

    .line 418
    :cond_0
    return-void
.end method
