.class Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;
.super Ljava/lang/Object;
.source "SLinkDevicesListAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->showPopupDeregistered(ILandroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

.field final synthetic val$deviceItem:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->val$deviceItem:Lcom/samsung/everglades/video/myvideo/list/DeviceListItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$100(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0026

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 172
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 173
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;->this$0:Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;->access$200(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 174
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/SLinkDevicesListAdapter$5;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 193
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 194
    return-void
.end method
