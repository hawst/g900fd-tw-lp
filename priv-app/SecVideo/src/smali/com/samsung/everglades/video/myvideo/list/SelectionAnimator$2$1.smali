.class Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;
.super Ljava/lang/Object;
.source "SelectionAnimator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->onPreDraw()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

.field final synthetic val$chbTranslationToX:I

.field final synthetic val$childCount:I


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;II)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iput p2, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->val$childCount:I

    iput p3, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->val$chbTranslationToX:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "anim"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 128
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    .line 130
    .local v2, "fraction":F
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$400(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isListOrFolder()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 131
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$500(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080048

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 132
    .local v4, "list_menu_margin":F
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    sub-float v6, v7, v2

    mul-float/2addr v6, v4

    invoke-virtual {v5, v6}, Lcom/sec/android/touchwiz/widget/TwGridView;->setTranslationX(F)V

    .line 135
    .end local v4    # "list_menu_margin":F
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->val$childCount:I

    if-ge v3, v5, :cond_3

    .line 136
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 137
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mView:Lcom/sec/android/touchwiz/widget/TwGridView;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$000(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/sec/android/touchwiz/widget/TwGridView;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 138
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 139
    const v5, 0x7f09002c

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 140
    .local v0, "cb":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 141
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->this$1:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;

    iget-object v5, v5, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2;->this$0:Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;->access$400(Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator;)Lcom/samsung/everglades/video/myvideo/common/ListType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 142
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 146
    :goto_1
    sub-float v5, v7, v2

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 135
    .end local v0    # "cb":Landroid/view/View;
    .end local v1    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 144
    .restart local v0    # "cb":Landroid/view/View;
    .restart local v1    # "child":Landroid/view/View;
    :cond_2
    iget v5, p0, Lcom/samsung/everglades/video/myvideo/list/SelectionAnimator$2$1;->val$chbTranslationToX:I

    int-to-float v5, v5

    mul-float/2addr v5, v2

    invoke-virtual {v0, v5}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 151
    .end local v0    # "cb":Landroid/view/View;
    .end local v1    # "child":Landroid/view/View;
    :cond_3
    return-void
.end method
