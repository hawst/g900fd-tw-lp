.class Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;
.super Ljava/lang/Object;
.source "MovePrivateOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showRenamePopup(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 831
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 834
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 835
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0078

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->val$fileName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 836
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0076

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 838
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$1;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 860
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$2;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 882
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;->this$0:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    iget-object v1, v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 904
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 929
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 930
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 931
    return-void
.end method
