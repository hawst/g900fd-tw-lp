.class public Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;
.super Ljava/util/Observable;
.source "AsfErrorDialog.java"


# static fields
.field private static mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mContext:Landroid/content/Context;

    .line 19
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->notifyChanged()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private create(Ljava/lang/String;)V
    .locals 5
    .param p1, "dmsName"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0032

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 50
    const v1, 0x7f0c0087

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 52
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$1;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 58
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$2;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    new-instance v1, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$3;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog$3;-><init>(Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    .line 71
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    invoke-direct {v0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;-><init>()V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    .line 29
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    iput-object p0, v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mContext:Landroid/content/Context;

    .line 31
    sget-object v0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mInstance:Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;

    return-object v0
.end method

.method private notifyChanged()V
    .locals 0

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->setChanged()V

    .line 75
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->notifyObservers()V

    .line 76
    return-void
.end method


# virtual methods
.method public show(Ljava/lang/String;)V
    .locals 2
    .param p1, "dmsName"    # Ljava/lang/String;

    .prologue
    .line 35
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->create(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 41
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/AsfErrorDialog;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
