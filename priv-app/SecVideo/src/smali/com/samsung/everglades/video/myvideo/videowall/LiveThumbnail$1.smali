.class Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;
.super Ljava/lang/Object;
.source "LiveThumbnail.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->createTransThread(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 79
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$000(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/videowall/FileMgr;->getVideoList(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v9

    .line 81
    .local v9, "videoCursor":Landroid/database/Cursor;
    if-nez v9, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_2

    .line 83
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 87
    :cond_2
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 89
    .local v0, "cnt":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v0, :cond_6

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->transcodingThreadinterrupt:I
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$200()I

    move-result v1

    if-nez v1, :cond_6

    .line 90
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v9, v8}, Lcom/sec/android/videowall/FileMgr;->getPathFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "spathname":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v9, v8}, Lcom/sec/android/videowall/FileMgr;->getOnlyFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v3

    .line 92
    .local v3, "sfilename":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v9, v8}, Lcom/sec/android/videowall/FileMgr;->getVideoId(Landroid/database/Cursor;I)J

    move-result-wide v4

    .line 93
    .local v4, "vid":J
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " vid [ "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " ] : name [ "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " ]"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/videowall/FileMgr;->hasThumbnailsforList(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$100(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 96
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->drmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$400(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->drmUtil:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$400(Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isNonDrmContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 97
    const/4 v1, 0x0

    invoke-static {v2, v1}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_getDurationTime(Ljava/lang/String;Z)I

    move-result v1

    # setter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$502(I)I

    .line 98
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " duration time : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$500()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 99
    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->duration:I
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$500()I

    move-result v1

    if-lez v1, :cond_3

    .line 100
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$1;->this$0:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;

    const/16 v6, 0xa

    invoke-virtual/range {v1 .. v6}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->CreateLiveThumbnailforListView(Ljava/lang/String;Ljava/lang/String;JI)I

    .line 102
    const-wide/16 v10, 0x0

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :cond_3
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 103
    :catch_0
    move-exception v7

    .line 104
    .local v7, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 108
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " VIDEO_DRM"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    goto :goto_2

    .line 111
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$300()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " thumbnail is existed!!"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    goto :goto_2

    .line 115
    .end local v2    # "spathname":Ljava/lang/String;
    .end local v3    # "sfilename":Ljava/lang/String;
    .end local v4    # "vid":J
    :cond_6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 117
    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$600()Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 118
    # getter for: Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mUpdateLiveThumbnailListener:Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->access$600()Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail$UpdateLiveThumbnailListener;->UpdateLiveThumbnail()V

    goto/16 :goto_0
.end method
