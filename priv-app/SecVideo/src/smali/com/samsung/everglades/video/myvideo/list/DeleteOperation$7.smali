.class Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;
.super Ljava/lang/Object;
.source "DeleteOperation.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->showConfirmDeleteFiles(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;


# direct methods
.method constructor <init>(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v6, 0x0

    .line 156
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 157
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$300(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 159
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getCurrentVideoListFragment(Landroid/app/Activity;)Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;

    move-result-object v0

    .line 161
    .local v0, "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getCheckedItemPositions()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 164
    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getCheckedItemPositions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setDeletePending(Ljava/util/ArrayList;)V

    .line 174
    .end local v0    # "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    :cond_0
    :goto_0
    sput-boolean v6, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->popupCreated:Z

    .line 176
    :cond_1
    return-void

    .line 166
    .restart local v0    # "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    :cond_2
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->getItemPositions(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setDeletePending(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 170
    .end local v0    # "f":Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$200(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$300(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$400(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;-><init>(Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;)V

    # setter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    invoke-static {v1, v2}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$502(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    .line 171
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$7;->this$0:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;

    # getter for: Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->mDeleteFilesThread:Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;->access$500(Lcom/samsung/everglades/video/myvideo/list/DeleteOperation;)Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/list/DeleteOperation$DeleteFilesThread;->start()V

    goto :goto_0
.end method
