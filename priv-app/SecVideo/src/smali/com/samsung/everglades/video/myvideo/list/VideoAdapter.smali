.class public Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;
.super Landroid/widget/CursorAdapter;
.source "VideoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final NULLPATH:Ljava/lang/String; = "null_filepath"

.field private static selectedItem:Ljava/lang/String;


# instance fields
.field private mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

.field private mContext:Landroid/content/Context;

.field private mFileMgr:Lcom/sec/android/videowall/FileMgr;

.field private mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

.field private mOrientation:I

.field private mSearchKey:Ljava/lang/String;

.field private positiveX:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/everglades/video/myvideo/common/ListType;Landroid/database/Cursor;Landroid/view/View;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;
    .param p3, "cursor"    # Landroid/database/Cursor;
    .param p4, "view"    # Landroid/view/View;
    .param p5, "orientation"    # I

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->positiveX:I

    .line 58
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 60
    iput p5, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    .line 61
    invoke-static {p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    .line 62
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    .line 63
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    invoke-direct {v0, p1}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 64
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 65
    return-void
.end method

.method private getCurrentBucketId()I
    .locals 4

    .prologue
    .line 465
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    .line 466
    .local v0, "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Pref;->get(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/Pref;

    move-result-object v1

    const-string v2, "lastFolder"

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoFirstBucketId()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Pref;->loadInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getFilePath(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 336
    const/4 v0, 0x0

    .line 338
    .local v0, "filePath":Ljava/lang/String;
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->pathIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 339
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 340
    :cond_0
    const-string v0, "null_filepath"

    .line 343
    :cond_1
    return-object v0
.end method

.method private getRowLayout()I
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 104
    const v0, 0x7f04001e

    .line 110
    :goto_0
    return v0

    .line 106
    :cond_0
    const v0, 0x7f04001f

    goto :goto_0

    .line 110
    :cond_1
    const v0, 0x7f04001d

    goto :goto_0
.end method

.method private getTitle(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 347
    const/4 v0, 0x0

    .line 349
    .local v0, "videoTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    iget v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->displayNameIndex:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 359
    :goto_0
    return-object v0

    .line 352
    :cond_0
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 353
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 355
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private isVideoWallSupportContent(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isViewIncorrect(Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Z
    .locals 2
    .param p1, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 165
    iget v0, p1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->orientation:I

    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    if-ne v0, v1, :cond_0

    iget v0, p1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->layoutId:I

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getRowLayout()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setCloudIcon(Landroid/view/View;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;I)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "value"    # I

    .prologue
    .line 568
    const v1, 0x7f090051

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 569
    .local v0, "cloudStub":Landroid/view/ViewStub;
    if-eqz v0, :cond_0

    .line 570
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 571
    const v1, 0x7f09002d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    .line 574
    :cond_0
    packed-switch p3, :pswitch_data_0

    .line 586
    :goto_0
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 587
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 589
    :cond_1
    return-void

    .line 576
    :pswitch_0
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020011

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 579
    :pswitch_1
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02004b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 582
    :pswitch_2
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 574
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setCurrentFolder(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v3, 0x7f070030

    .line 684
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 694
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .line 687
    .local v0, "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->selectedItem:Ljava/lang/String;

    .line 688
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 689
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 691
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 692
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 693
    const v1, 0x7f02003b

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private unsetOtherFolder(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v3, 0x7f070021

    .line 697
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 698
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .line 700
    .local v0, "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->isSelectedFolder:Z

    .line 701
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 702
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 704
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 705
    iget-object v1, v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 706
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private updateCloudIcon(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x8

    .line 547
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    :cond_0
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 565
    :cond_1
    :goto_0
    return-void

    .line 554
    :cond_2
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 555
    const/4 v0, 0x0

    invoke-direct {p0, p3, p2, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setCloudIcon(Landroid/view/View;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;I)V

    goto :goto_0

    .line 556
    :cond_3
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 557
    const/4 v0, 0x1

    invoke-direct {p0, p3, p2, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setCloudIcon(Landroid/view/View;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;I)V

    goto :goto_0

    .line 558
    :cond_4
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v0, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduServerOnlyContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 559
    const/4 v0, 0x2

    invoke-direct {p0, p3, p2, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setCloudIcon(Landroid/view/View;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;I)V

    goto :goto_0

    .line 561
    :cond_5
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 562
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateFirstRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 363
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    if-nez v2, :cond_0

    .line 382
    :goto_0
    return-void

    .line 365
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 366
    const-string v2, "bucket_display_name"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 368
    .local v0, "bucketDisplayIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 369
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    .end local v0    # "bucketDisplayIndex":I
    :goto_1
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 371
    .restart local v0    # "bucketDisplayIndex":I
    :cond_1
    const-string v2, "updateFirstRowText() : Failed to read bucketDisplay Index"

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 374
    .end local v0    # "bucketDisplayIndex":I
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getTitle(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v1

    .line 375
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isDeviceSearchFileLists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 376
    :cond_3
    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateSearchHighLight(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Ljava/lang/String;)V

    goto :goto_1

    .line 378
    :cond_4
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateFolderIcon(Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 592
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 593
    const v1, 0x7f090052

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 594
    .local v0, "folderStub":Landroid/view/ViewStub;
    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 596
    const v1, 0x7f09002e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->folderIcon:Landroid/widget/ImageView;

    .line 603
    .end local v0    # "folderStub":Landroid/view/ViewStub;
    :cond_0
    :goto_0
    return-void

    .line 599
    :cond_1
    iget-object v1, p1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->folderIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 600
    iget-object v1, p1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->folderIcon:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateFolderSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 417
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    if-nez v3, :cond_1

    .line 462
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isDropboxContent(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 420
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    const v4, 0x7f0c003a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 446
    :cond_2
    :goto_1
    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->selectedItem:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 447
    const-string v3, "bucket_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 448
    .local v0, "bucket_index":I
    if-eq v0, v6, :cond_3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCurrentBucketId()I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 449
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->selectedItem:Ljava/lang/String;

    .line 453
    .end local v0    # "bucket_index":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->selectedItem:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 454
    sget-object v3, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->selectedItem:Ljava/lang/String;

    iget-object v4, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 455
    const/4 v3, 0x1

    iput-boolean v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->isSelectedFolder:Z

    goto :goto_0

    .line 421
    :cond_4
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isSKTCloudContent(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 422
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    const v4, 0x7f0c00d5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 423
    :cond_5
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isBaiduContent(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 424
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    const v4, 0x7f0c000c

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 426
    :cond_6
    iget v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->pathIndex:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 428
    const/4 v1, -0x1

    .line 429
    .local v1, "index":I
    const-string v3, "/storage"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 430
    const-string v3, "/storage"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 435
    :cond_7
    :goto_2
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 437
    if-eq v1, v6, :cond_8

    .line 438
    invoke-virtual {v2, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 441
    :cond_8
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 442
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 431
    :cond_9
    const-string v3, "/mnt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 432
    const-string v3, "/mnt"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 457
    .end local v1    # "index":I
    .end local v2    # "path":Ljava/lang/String;
    :cond_a
    iput-boolean v5, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->isSelectedFolder:Z

    goto/16 :goto_0
.end method

.method private updateIndices(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 206
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowIndex:I

    .line 207
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowIndex:I

    .line 208
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->idIndex:I

    .line 209
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "bookmark"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->resumePosIndex:I

    .line 214
    :goto_0
    const-string v0, "_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->displayNameIndex:I

    .line 216
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    const-string v0, "thumbnail"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->pathIndex:I

    .line 221
    :goto_1
    return-void

    .line 212
    :cond_0
    const-string v0, "resumePos"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->resumePosIndex:I

    goto :goto_0

    .line 219
    :cond_1
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->pathIndex:I

    goto :goto_1
.end method

.method private updateListSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 470
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 506
    :goto_0
    return-void

    .line 472
    :cond_0
    const/4 v0, 0x0

    .line 473
    .local v0, "duration":Ljava/lang/String;
    const-wide/16 v2, 0x0

    .line 475
    .local v2, "currentTime":J
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowIndex:I

    const/4 v6, -0x1

    if-eq v1, v6, :cond_1

    .line 476
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 479
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isNearbyFileList()Z

    move-result v1

    if-nez v1, :cond_2

    .line 480
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->resumePosIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    .line 483
    :cond_2
    if-nez v0, :cond_3

    .line 484
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->pathIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1, v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getTotalDuration(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 486
    :cond_3
    if-nez v0, :cond_4

    const-string v0, "0"

    .line 488
    :cond_4
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 490
    .local v4, "durationTime":J
    const-string v1, "%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->stringForTime(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 492
    .local v9, "strElapsed":Ljava/lang/String;
    const/4 v8, 0x0

    .line 493
    .local v8, "strDuration":Ljava/lang/String;
    const-string v1, "%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->stringForTime(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 495
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 496
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_5

    .line 497
    const-string v8, "--:--:--"

    .line 500
    :cond_5
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 501
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 502
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p0

    move-object v6, p2

    move-object v7, p3

    .line 503
    invoke-direct/range {v1 .. v7}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateProgessBar(JJLcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 504
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7, v9}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v7, v8}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private updatePrivateContentIcon(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    .line 606
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 607
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getFilePath(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v0

    .line 608
    .local v0, "filePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 610
    .local v2, "secretDir":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 611
    const v3, 0x7f090055

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 612
    .local v1, "personalStub":Landroid/view/ViewStub;
    if-eqz v1, :cond_0

    .line 613
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 614
    const v3, 0x7f09002f

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    .line 617
    :cond_0
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 618
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 630
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "personalStub":Landroid/view/ViewStub;
    .end local v2    # "secretDir":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 621
    .restart local v0    # "filePath":Ljava/lang/String;
    .restart local v2    # "secretDir":Ljava/lang/String;
    :cond_2
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 622
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 626
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v2    # "secretDir":Ljava/lang/String;
    :cond_3
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 627
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->privateContentIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateProgessBar(JJLcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 9
    .param p1, "currentTime"    # J
    .param p3, "durationTime"    # J
    .param p5, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p6, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0x8

    .line 509
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 510
    :cond_0
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    .line 511
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 544
    :cond_1
    :goto_0
    return-void

    .line 516
    :cond_2
    const/4 v0, 0x0

    .line 518
    .local v0, "currentPercent":I
    cmp-long v2, p3, v6

    if-lez v2, :cond_3

    .line 519
    cmp-long v2, p1, v6

    if-gtz v2, :cond_5

    .line 520
    const/4 v0, 0x0

    .line 529
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    const/16 v2, 0x64

    if-ne v0, v2, :cond_6

    .line 530
    :cond_4
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    .line 531
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 522
    :cond_5
    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    div-long/2addr v2, p3

    long-to-int v0, v2

    .line 523
    if-nez v0, :cond_3

    .line 524
    const/4 v0, 0x1

    goto :goto_1

    .line 534
    :cond_6
    const v2, 0x7f090053

    invoke-virtual {p6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 535
    .local v1, "progressStub":Landroid/view/ViewStub;
    if-eqz v1, :cond_7

    .line 536
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 537
    const v2, 0x7f090030

    invoke-virtual {p6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    .line 539
    :cond_7
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    .line 540
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 541
    iget-object v2, p5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method private updateSearchHighLight(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 9
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 386
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    .line 387
    .local v2, "len":I
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 388
    .local v5, "temp":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "findString":Ljava/lang/String;
    const/4 v3, 0x0

    .line 391
    .local v3, "prefixForIndian":[C
    iget-object v6, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    invoke-static {v6, p3, v7}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v3

    .line 392
    if-eqz v3, :cond_0

    .line 393
    new-instance v0, Ljava/lang/String;

    .end local v0    # "findString":Ljava/lang/String;
    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([C)V

    .line 394
    .restart local v0    # "findString":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 396
    :cond_0
    invoke-virtual {v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 398
    .local v1, "idx":I
    const/4 v6, -0x1

    if-eq v1, v6, :cond_2

    .line 399
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, p3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 400
    .local v4, "sText":Landroid/text/SpannableString;
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    const v7, -0xe16f01

    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v7, v1, v2

    const/4 v8, 0x0

    invoke-virtual {v4, v6, v1, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 401
    iget-object v6, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    .end local v0    # "findString":Ljava/lang/String;
    .end local v1    # "idx":I
    .end local v2    # "len":I
    .end local v3    # "prefixForIndian":[C
    .end local v4    # "sText":Landroid/text/SpannableString;
    .end local v5    # "temp":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 403
    .restart local v0    # "findString":Ljava/lang/String;
    .restart local v1    # "idx":I
    .restart local v2    # "len":I
    .restart local v3    # "prefixForIndian":[C
    .restart local v5    # "temp":Ljava/lang/String;
    :cond_2
    iget-object v6, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v6, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateFolderSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    .line 414
    :goto_0
    return-void

    .line 412
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateListSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    goto :goto_0
.end method

.method private updateSelectedFolderText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 658
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 667
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    iget-boolean v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->isSelectedFolder:Z

    if-eqz v0, :cond_2

    .line 662
    invoke-direct {p0, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setCurrentFolder(Landroid/view/View;)V

    goto :goto_0

    .line 664
    :cond_2
    invoke-direct {p0, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->unsetOtherFolder(Landroid/view/View;)V

    goto :goto_0
.end method

.method private updateSelectedState(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 633
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 634
    const v2, 0x7f090050

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 635
    .local v0, "checkboxStub":Landroid/view/ViewStub;
    if-eqz v0, :cond_0

    .line 636
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 637
    const v2, 0x7f09002c

    invoke-virtual {p3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    .line 640
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v2}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 641
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 643
    :cond_1
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    if-eqz v2, :cond_2

    .line 644
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 647
    :cond_2
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v2, p1, v3}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v1

    .line 648
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/list/MultiSelector;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/everglades/video/myvideo/list/MultiSelector;->has(Landroid/net/Uri;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 655
    .end local v0    # "checkboxStub":Landroid/view/ViewStub;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_3
    :goto_0
    return-void

    .line 650
    :cond_4
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    if-eqz v2, :cond_3

    .line 651
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 652
    iget-object v2, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateThumb(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    const v7, 0x7f090003

    const v6, 0x7f020015

    const/4 v5, 0x0

    .line 285
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    if-nez v3, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 289
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "filePath":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 295
    .local v2, "thumbnail":Landroid/graphics/Bitmap;
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 296
    if-eqz v2, :cond_6

    .line 297
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 306
    :goto_2
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 307
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isFolder()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 308
    if-nez v2, :cond_2

    .line 309
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 310
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 312
    :cond_2
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-virtual {v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->getFolderHoverListener()Landroid/view/View$OnHoverListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 313
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v4, p1, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 314
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3, v4}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V

    .line 316
    const-string v3, "bucket_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 317
    .local v0, "bucket_index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    .line 318
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const/high16 v4, 0x7f090000

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 320
    :cond_3
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const v4, 0x7f090002

    iget-object v5, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 331
    .end local v0    # "bucket_index":I
    :cond_4
    :goto_3
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const v4, 0x7f090001

    iget-object v5, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 291
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getFilePath(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "filePath":Ljava/lang/String;
    goto :goto_1

    .line 299
    .restart local v2    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_6
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isGrid()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 300
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const v4, 0x7f02000c

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 302
    :cond_7
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 321
    :cond_8
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isTypical()Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v3}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSearch()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 322
    :cond_9
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mCloudUtil:Lcom/samsung/everglades/video/myvideo/common/CloudUtil;

    invoke-virtual {v3, p1}, Lcom/samsung/everglades/video/myvideo/common/CloudUtil;->isCloudTypeContent(Landroid/database/Cursor;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 323
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 325
    :cond_a
    if-nez v2, :cond_b

    .line 326
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 328
    :cond_b
    iget-object v3, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v4, p1, v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private updateThumbs(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 224
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->isVideoWallSupportContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateVideowall(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    .line 232
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVisibility(I)V

    .line 230
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateThumb(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    goto :goto_0
.end method

.method private updateVideowall(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .prologue
    .line 239
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    if-nez v1, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    iget v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->idIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 241
    .local v4, "videoId":J
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getFilePath(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "filePath":Ljava/lang/String;
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getTitle(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v3

    .line 243
    .local v3, "videoTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    const/4 v6, -0x1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v9

    .line 245
    .local v9, "sthumbName":Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    .line 246
    .local v8, "position":I
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v7

    .line 247
    .local v7, "num":I
    if-nez v8, :cond_3

    const/4 v0, 0x0

    .line 249
    .local v0, "engposition":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    const/4 v6, -0x1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 250
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    sget v6, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallWidth:I

    sget v10, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallHeight:I

    sget v11, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->mVideoWallfps:I

    invoke-virtual {v1, v6, v10, v11}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setView(III)V

    .line 252
    invoke-static {v0, v9}, Lcom/samsung/everglades/video/myvideo/videowall/HandleVWLib;->MscEngine_setThumbnail(ILjava/lang/String;)I

    move-result v1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_5

    .line 254
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSLinkFileList()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 255
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/everglades/video/myvideo/common/SLinkUtil;->thumbIdToString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 259
    :goto_2
    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    .line 261
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    invoke-virtual {v1, v0}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setBitmapView(I)V

    .line 263
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/videowall/LiveThumbnail;->isVideoWallOn()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 264
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    .line 265
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 268
    :cond_2
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVisibility(I)V

    .line 270
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-virtual {v1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->isSelectable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    const v6, 0x7f090003

    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    invoke-static {v10, p1, v11}, Lcom/samsung/everglades/video/myvideo/common/DB;->getUri(Landroid/content/Context;Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v1, v6, v10}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setTag(ILjava/lang/Object;)V

    .line 272
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    const v6, 0x7f090001

    iget-object v10, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v1, v6, v10}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setTag(ILjava/lang/Object;)V

    .line 273
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    invoke-static {}, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->getInstance()Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/everglades/video/myvideo/list/ThumbHoverViewer;->mListHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v6}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 247
    .end local v0    # "engposition":I
    :cond_3
    rem-int v1, v8, v7

    add-int/lit8 v0, v1, 0x1

    goto :goto_1

    .line 257
    .restart local v0    # "engposition":I
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getFilePath(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 280
    :cond_5
    iget-object v1, p2, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Lcom/samsung/everglades/video/myvideo/videowall/ListView;->setVisibility(I)V

    .line 281
    invoke-direct {p0, p1, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateThumb(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 187
    if-eqz p3, :cond_0

    .line 188
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .line 190
    .local v0, "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    invoke-direct {p0, p3, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateIndices(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    .line 192
    invoke-direct {p0, p3, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateThumbs(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    .line 193
    invoke-direct {p0, p3, v0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateFirstRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)V

    .line 194
    invoke-direct {p0, p3, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateSecondRowText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 195
    invoke-direct {p0, p3, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateCloudIcon(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 196
    invoke-direct {p0, p3, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updatePrivateContentIcon(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 197
    invoke-direct {p0, p3, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateSelectedState(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 198
    invoke-direct {p0, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateFolderIcon(Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 199
    sget-boolean v1, Lcom/samsung/everglades/video/myvideo/common/Features;->MODEL_T:Z

    if-eqz v1, :cond_0

    .line 200
    invoke-direct {p0, p3, v0, p1}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->updateSelectedFolderText(Landroid/database/Cursor;Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;Landroid/view/View;)V

    .line 203
    .end local v0    # "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    :cond_0
    return-void
.end method

.method public dismissFolderHoverViewer()V
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mFolderHoverViewer:Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/FolderHoverViewer;->dismissFolderPreviewPopup()V

    .line 681
    :cond_0
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 116
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 117
    .local v3, "scrollBounds":Landroid/graphics/Rect;
    const/4 v6, 0x4

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    .line 119
    .local v2, "location":[I
    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 120
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "couldn\'t move cursor to position "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 123
    :cond_0
    if-eqz p2, :cond_2

    .line 124
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    .line 125
    .local v5, "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    iget-object v6, v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    if-eqz v6, :cond_1

    .line 126
    iget-object v6, v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v3}, Landroid/widget/CheckBox;->getHitRect(Landroid/graphics/Rect;)V

    .line 127
    iget-object v6, v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v2}, Landroid/widget/CheckBox;->getLocationOnScreen([I)V

    .line 129
    iget-object v6, v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v6, v3}, Landroid/widget/CheckBox;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 130
    iget v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->positiveX:I

    if-gez v6, :cond_1

    aget v6, v2, v7

    if-lez v6, :cond_1

    .line 131
    aget v6, v2, v7

    iput v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->positiveX:I

    .line 140
    :cond_1
    :goto_0
    invoke-direct {p0, v5}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->isViewIncorrect(Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 p2, 0x0

    .line 145
    .end local v5    # "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    :cond_2
    if-nez p2, :cond_5

    .line 146
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    invoke-virtual {p0, v6, v7, p3}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 150
    .local v4, "v":Landroid/view/View;
    :goto_1
    iget-object v6, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v7

    invoke-virtual {p0, v4, v6, v7}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 151
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "VideoAdapter - getView() position:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->ismENGMode()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 153
    const/4 v6, 0x7

    if-ne p1, v6, :cond_3

    if-nez p2, :cond_3

    .line 155
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 156
    .local v0, "endTime":J
    const-string v6, "APP_PERFORMANCE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[VideoMain PERFORMANCE] Launching time : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-wide v8, Lcom/samsung/everglades/video/myvideo/common/Utils;->mVideoLaunchingTime:J

    sub-long v8, v0, v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ms ##############################"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    const-wide/16 v6, 0x0

    sput-wide v6, Lcom/samsung/everglades/video/myvideo/common/Utils;->mVideoLaunchingTime:J

    .line 161
    .end local v0    # "endTime":J
    :cond_3
    return-object v4

    .line 134
    .end local v4    # "v":Landroid/view/View;
    .restart local v5    # "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    :cond_4
    sget-boolean v6, Lcom/samsung/everglades/video/myvideo/common/Features;->FLAG_SUPPORT_ANIMATION:Z

    if-eqz v6, :cond_1

    .line 135
    iget-object v6, v5, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->listcheckBox:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setTranslationX(F)V

    goto :goto_0

    .line 148
    .end local v5    # "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    :cond_5
    move-object v4, p2

    .restart local v4    # "v":Landroid/view/View;
    goto :goto_1

    .line 117
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 170
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->getRowLayout()I

    move-result v0

    .line 171
    .local v0, "layoutId":I
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 173
    .local v2, "view":Landroid/view/View;
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;-><init>(Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;)V

    .line 174
    .local v1, "vh":Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;
    iput v0, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->layoutId:I

    .line 175
    iget v3, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    iput v3, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->orientation:I

    .line 176
    const v3, 0x7f090057

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    .line 177
    const v3, 0x7f090058

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    .line 178
    const v3, 0x7f090044

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 179
    const v3, 0x7f090059

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    iput-object v3, v1, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter$ViewHolder;->videoWallView:Lcom/samsung/everglades/video/myvideo/videowall/ListView;

    .line 181
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 182
    return-object v2
.end method

.method public setListType(Lcom/samsung/everglades/video/myvideo/common/ListType;)V
    .locals 3
    .param p1, "listType"    # Lcom/samsung/everglades/video/myvideo/common/ListType;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mListType:Lcom/samsung/everglades/video/myvideo/common/ListType;

    .line 73
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VideoAdapter : setListType() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/everglades/video/myvideo/common/ListType;->getListType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 74
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mOrientation:I

    .line 78
    return-void
.end method

.method public setSearchKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->mSearchKey:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public updateFolderView(Lcom/sec/android/touchwiz/widget/TwGridView;Landroid/view/View;)V
    .locals 3
    .param p1, "listView"    # Lcom/sec/android/touchwiz/widget/TwGridView;
    .param p2, "selectedView"    # Landroid/view/View;

    .prologue
    .line 670
    invoke-virtual {p1}, Lcom/sec/android/touchwiz/widget/TwGridView;->getCount()I

    move-result v0

    .line 671
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 672
    invoke-virtual {p1, v1}, Lcom/sec/android/touchwiz/widget/TwGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->unsetOtherFolder(Landroid/view/View;)V

    .line 671
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 674
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/everglades/video/myvideo/list/VideoAdapter;->setCurrentFolder(Landroid/view/View;)V

    .line 675
    return-void
.end method
