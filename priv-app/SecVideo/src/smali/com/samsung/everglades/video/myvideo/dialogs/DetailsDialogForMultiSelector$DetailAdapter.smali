.class Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetailsDialogForMultiSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;


# direct methods
.method public constructor <init>(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 108
    iput-object p3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mItem:Ljava/util/ArrayList;

    .line 109
    iput-object p2, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mContext:Landroid/content/Context;

    .line 110
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 125
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 130
    if-nez p2, :cond_0

    .line 131
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 132
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f040019

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 134
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x7f090042

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 136
    .local v1, "tv":Landroid/widget/TextView;
    if-nez p1, :cond_1

    .line 137
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :goto_0
    invoke-static {}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isRTL()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 144
    const/16 v3, 0x15

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 148
    :goto_1
    return-object p2

    .line 139
    :cond_1
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->mTotalSize:J
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$300(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "sizeText":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00d8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector$DetailAdapter;->this$0:Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;

    # getter for: Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->COLON:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;->access$500(Lcom/samsung/everglades/video/myvideo/dialogs/DetailsDialogForMultiSelector;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 146
    .end local v0    # "sizeText":Ljava/lang/String;
    :cond_2
    const/16 v3, 0x13

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_1
.end method
