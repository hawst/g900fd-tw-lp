.class public Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
.super Ljava/lang/Object;
.source "MovePrivateOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;
    }
.end annotation


# static fields
.field public static final ABORT_MOVE_PROGRESS:I = 0x4

.field private static final CLEAR_KEEP_SCREEN_ON:I = 0x8

.field public static final COMPLETE_MOVE_PROGRESS:I = 0x2

.field static final DirPath:Ljava/lang/String; = "/storage/emulated/0/Movies"

.field public static final FINISH_MOVE_PROGRESS:I = 0x3

.field public static final NOT_ENOUGH_MEMORY:I = 0x6

.field private static final SET_KEEP_SCREEN_ON:I = 0x7

.field public static final SHOW_ERROR_MESSAGE:I = 0x5

.field public static final START_MOVE_PROGRESS:I = 0x0

.field public static final UPDATE_MOVE_PROGRESS:I = 0x1

.field private static mDestPath:Ljava/lang/String;


# instance fields
.field mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mCancellation:Z

.field mContext:Landroid/content/Context;

.field private mCountText:Landroid/widget/TextView;

.field private mFolderPickerPopup:Landroid/app/Dialog;

.field private final mHandler:Landroid/os/Handler;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsFolder:Z

.field protected mMoveFilesThread:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;

.field mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

.field private mPercentage:Landroid/widget/TextView;

.field private mPrivateModeBinder:Landroid/os/IBinder;

.field private mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

.field private mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressDialog:Landroid/app/Dialog;

.field private mRename:Z

.field private mRenameCancellation:Z

.field private mRenameRunnable:Ljava/lang/Runnable;

.field private mReplace:Z

.field private mSuccessMoveCnt:I

.field private mToast:Landroid/widget/Toast;

.field private mTotalCnt:I

.field private mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z

    .line 67
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    .line 69
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 70
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 71
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 80
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    .line 81
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z

    .line 82
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z

    .line 83
    iput-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z

    .line 96
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$1;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$1;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    .line 534
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$3;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mHandler:Landroid/os/Handler;

    .line 91
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    .line 92
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mInflater:Landroid/view/LayoutInflater;

    .line 93
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mOnPrivateModeListener:Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment;->setOnPrivateModeListener(Lcom/samsung/everglades/video/myvideo/fragments/VideoListFragment$OnPrivateModeListener;)V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->createProgressDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateCount(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->updateProgressPercentage(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->closeProgressDialog()V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showFinishToast(I)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->setKeepScreenOn()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->clearKeepScreenOn()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mFolderPickerPopup:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->selectFolder(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->startMoveFiles(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showRenamePopup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRename:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameCancellation:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I

    return v0
.end method

.method static synthetic access$808(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mReplace:Z

    return p1
.end method

.method private declared-synchronized clearKeepScreenOn()V
    .locals 2

    .prologue
    .line 729
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 730
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 732
    :cond_0
    monitor-exit p0

    return-void

    .line 729
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized closeProgressDialog()V
    .locals 1

    .prologue
    .line 716
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 717
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 718
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressDialog:Landroid/app/Dialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 720
    :cond_0
    monitor-exit p0

    return-void

    .line 716
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private createFolderPickerPopup(Ljava/util/ArrayList;)Landroid/app/Dialog;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .prologue
    .local p1, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v9, 0x2

    .line 785
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-direct {v6, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 786
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04000a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 787
    .local v8, "view":Landroid/view/View;
    const v1, 0x7f090025

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    .line 788
    .local v7, "listView":Landroid/widget/ListView;
    invoke-virtual {v7}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 790
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    const v3, 0x7f04000b

    new-array v4, v9, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "Name"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "Path"

    aput-object v5, v4, v2

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 795
    .local v0, "folderAdapter":Landroid/widget/SimpleAdapter;
    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 797
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 798
    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 800
    const v1, 0x7f0c000d

    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$6;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$6;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    invoke-virtual {v6, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 807
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$7;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$7;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    invoke-virtual {v6, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 813
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;

    invoke-direct {v1, p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$8;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/util/ArrayList;)V

    invoke-virtual {v7, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 822
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1

    .line 790
    :array_0
    .array-data 4
        0x7f090026
        0x7f090027
    .end array-data
.end method

.method private createProgressDialog()Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 603
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 605
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040012

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 606
    .local v1, "progressView":Landroid/view/View;
    const v2, 0x7f090034

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPercentage:Landroid/widget/TextView;

    .line 607
    const v2, 0x7f090033

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCountText:Landroid/widget/TextView;

    .line 608
    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressBar:Landroid/widget/ProgressBar;

    .line 610
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 612
    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    if-nez v2, :cond_0

    .line 613
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0080

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 618
    :goto_0
    const v2, 0x7f0c000d

    new-instance v3, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$4;

    invoke-direct {v3, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$4;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 627
    new-instance v2, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$5;

    invoke-direct {v2, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$5;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 635
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 615
    :cond_0
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private existFolderCheck()Z
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 735
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/samsung/everglades/video/myvideo/common/DB;->getVideoCursorFolder(Lcom/samsung/everglades/video/myvideo/common/ListType;)Landroid/database/Cursor;

    move-result-object v0

    .line 737
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 739
    .local v7, "totalCount":I
    if-gt v7, v9, :cond_0

    .line 780
    :goto_0
    return v8

    .line 743
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 745
    .local v6, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v2, 0x0

    .line 746
    .local v2, "folderpath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 748
    .local v1, "folderName":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v7, :cond_4

    .line 749
    const-string v10, "_data"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 750
    const-string v10, "bucket_display_name"

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 752
    if-eqz v2, :cond_3

    .line 753
    const/4 v4, -0x1

    .line 755
    .local v4, "index":I
    iget-object v10, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 756
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 748
    .end local v4    # "index":I
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 760
    .restart local v4    # "index":I
    :cond_1
    const/16 v10, 0x2f

    invoke-virtual {v2, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 761
    const/4 v10, -0x1

    if-eq v4, v10, :cond_2

    .line 762
    invoke-virtual {v2, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 763
    const-string v10, "/storage"

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v2, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 766
    :cond_2
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 767
    .local v5, "listInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v10, "Name"

    invoke-virtual {v5, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 768
    const-string v10, "Path"

    invoke-virtual {v5, v10, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 770
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 772
    .end local v4    # "index":I
    .end local v5    # "listInfo":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    .line 775
    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 776
    const-string v9, "MovePrivateOperation - folder picker playeList is empty"

    invoke-static {v9}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 779
    :cond_5
    invoke-direct {p0, v6}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->createFolderPickerPopup(Ljava/util/ArrayList;)Landroid/app/Dialog;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mFolderPickerPopup:Landroid/app/Dialog;

    move v8, v9

    .line 780
    goto :goto_0
.end method

.method private getTotalSize()J
    .locals 8

    .prologue
    .line 199
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 200
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/net/Uri;>;"
    iget-object v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/DB;->getInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DB;

    move-result-object v0

    .line 201
    .local v0, "db":Lcom/samsung/everglades/video/myvideo/common/DB;
    const/4 v4, 0x0

    .line 202
    .local v4, "uri":Landroid/net/Uri;
    const-wide/16 v2, 0x0

    .line 204
    .local v2, "totalSize":J
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 205
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "uri":Landroid/net/Uri;
    check-cast v4, Landroid/net/Uri;

    .line 206
    .restart local v4    # "uri":Landroid/net/Uri;
    iget-boolean v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mIsFolder:Z

    if-eqz v5, :cond_0

    .line 207
    invoke-virtual {v0, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getCurrentFolderSizeOnExternalDB(Landroid/net/Uri;)J

    move-result-wide v6

    add-long/2addr v2, v6

    goto :goto_0

    .line 209
    :cond_0
    invoke-virtual {v0, v4}, Lcom/samsung/everglades/video/myvideo/common/DB;->getSize(Landroid/net/Uri;)J

    move-result-wide v6

    add-long/2addr v2, v6

    goto :goto_0

    .line 213
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MovePrivateOperation - getTotalSize() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 214
    return-wide v2
.end method

.method private selectFolder(Ljava/lang/String;)V
    .locals 1
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 826
    sput-object p1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    .line 827
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->startMoveFiles(Ljava/lang/String;)V

    .line 828
    return-void
.end method

.method private declared-synchronized setKeepScreenOn()V
    .locals 2

    .prologue
    .line 723
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 726
    :cond_0
    monitor-exit p0

    return-void

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private showFinishToast(I)V
    .locals 7
    .param p1, "item"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 675
    const/4 v0, 0x0

    .line 676
    .local v0, "msg":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 677
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 712
    :goto_0
    invoke-virtual {p0, v0, v6}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showToast(Ljava/lang/CharSequence;I)V

    .line 713
    return-void

    .line 679
    :cond_0
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    if-nez v1, :cond_6

    .line 680
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I

    if-eq p1, v1, :cond_4

    iget-boolean v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCancellation:Z

    if-nez v1, :cond_4

    .line 681
    if-ne p1, v5, :cond_2

    .line 682
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I

    sub-int/2addr v1, p1

    if-ne v1, v5, :cond_1

    .line 683
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 685
    :cond_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 688
    :cond_2
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I

    sub-int/2addr v1, p1

    if-ne v1, v5, :cond_3

    .line 689
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0082

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 691
    :cond_3
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0081

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 695
    :cond_4
    if-le p1, v5, :cond_5

    .line 696
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0083

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 698
    :cond_5
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 701
    :cond_6
    iget v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    if-ne v1, v5, :cond_8

    .line 702
    if-le p1, v5, :cond_7

    .line 703
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00a6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget-object v4, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 705
    :cond_7
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00a8

    new-array v3, v5, [Ljava/lang/Object;

    sget-object v4, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 708
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private showRenamePopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 831
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;

    invoke-direct {v1, p0, p1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$9;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    .line 934
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 935
    :try_start_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 937
    :try_start_1
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mRenameRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 941
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 942
    return-void

    .line 938
    :catch_0
    move-exception v0

    .line 939
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 941
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method private startMoveFiles(Ljava/lang/String;)V
    .locals 13
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 172
    if-nez p1, :cond_0

    .line 173
    const-string v0, "MovePrivateOperation - destination path is null"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0077

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 175
    .local v7, "errorMsg":Ljava/lang/String;
    invoke-virtual {p0, v7, v12}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->showToast(Ljava/lang/CharSequence;I)V

    .line 178
    .end local v7    # "errorMsg":Ljava/lang/String;
    :cond_0
    new-instance v6, Ljava/io/File;

    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 179
    .local v6, "destFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 180
    const-string v0, "MovePrivateOperation - destination path is not exist"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_1

    .line 196
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-virtual {v6}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v8

    .line 185
    .local v8, "destFreeSpace":J
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->getTotalSize()J

    move-result-wide v10

    .line 186
    .local v10, "srcTotalSize":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MovePrivateOperation - startMoveFiles() - destFreeSpace : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " srcTotalSize : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 188
    cmp-long v0, v8, v10

    if-gez v0, :cond_2

    .line 189
    const-string v0, "MovePrivateOperation - not enough space!!!!!"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 192
    :cond_2
    new-instance v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    iget-boolean v5, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mIsFolder:Z

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;Ljava/util/Iterator;Landroid/os/Handler;Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mMoveFilesThread:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;

    .line 193
    iput v12, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mSuccessMoveCnt:I

    .line 194
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mMoveFilesThread:Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$MoveFilesThread;->start()V

    goto :goto_0
.end method

.method private updateCount(I)V
    .locals 3
    .param p1, "cnt"    # I

    .prologue
    .line 646
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCountText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 649
    :goto_0
    return-void

    .line 648
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mTotalCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateProgressPercentage(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 639
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPercentage:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    .line 643
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPercentage:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 642
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method


# virtual methods
.method public MoveFilesToPrivate(Ljava/util/ArrayList;Z)V
    .locals 4
    .param p2, "isFolder"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 153
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    const-string v2, "VPMP"

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;I)V

    .line 110
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    .line 111
    iput-boolean p2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mIsFolder:Z

    .line 112
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->getPrivateDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    .line 113
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    .line 115
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/everglades/video/myvideo/common/Utils;->isPrivateMounted(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    sget-object v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->startMoveFiles(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_1
    :try_start_0
    new-instance v1, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;

    invoke-direct {v1, p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation$2;-><init>(Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;)V

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 146
    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    invoke-static {v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 149
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 150
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodError;->printStackTrace()V

    goto :goto_0
.end method

.method public RemoveFilesFromPrivate(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "isFolder"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p1, :cond_0

    .line 169
    :goto_0
    return-void

    .line 158
    :cond_0
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mArrayList:Ljava/util/ArrayList;

    .line 159
    iput-boolean p2, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mIsFolder:Z

    .line 160
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mType:I

    .line 162
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->existFolderCheck()Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    const-string v0, "/storage/emulated/0/Movies"

    sput-object v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    .line 164
    sget-object v0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mDestPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->startMoveFiles(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mFolderPickerPopup:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method public showToast(II)V
    .locals 1
    .param p1, "resID"    # I
    .param p2, "length"    # I

    .prologue
    .line 652
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 660
    :goto_0
    return-void

    .line 654
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 655
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    .line 659
    :goto_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 657
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method public showToast(Ljava/lang/CharSequence;I)V
    .locals 1
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "length"    # I

    .prologue
    .line 663
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 672
    :goto_0
    return-void

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 666
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    .line 671
    :goto_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 668
    :cond_1
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/list/MovePrivateOperation;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
