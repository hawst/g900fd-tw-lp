.class public Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
.super Ljava/lang/Object;
.source "DRMUtil.java"


# static fields
.field public static final VIDEO_DRM_DIVX:I = 0x4

.field public static final VIDEO_DRM_OMADRM:I = 0x1

.field public static final VIDEO_DRM_PRDRM:I = 0x2

.field public static final VIDEO_DRM_WMDRM:I = 0x3

.field public static final VIDEO_NON_DRM:I = -0x1

.field public static final VIDEO_RIGHTS_EXPIRED:I = 0x2

.field public static final VIDEO_RIGHTS_INVALID:I = 0x1

.field public static final VIDEO_RIGHTS_NOT_ACQUIRED:I = 0x3

.field public static final VIDEO_RIGHTS_VALID:I

.field private static mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDrmClient:Landroid/drm/DrmManagerClient;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 22
    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mContext:Landroid/content/Context;

    .line 25
    iput-object p1, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mContext:Landroid/content/Context;

    .line 26
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->initDrmMgrClient()V

    .line 27
    return-void
.end method

.method public static declared-synchronized createInstance(Landroid/content/Context;)Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const-class v1, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    invoke-direct {v0, p0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 33
    :cond_0
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static destroyInstance()V
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    if-eqz v0, :cond_0

    .line 130
    sget-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    invoke-virtual {v0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->removeDrmMgrClient()V

    .line 131
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mUniqueInstance:Lcom/samsung/everglades/video/myvideo/common/DRMUtil;

    .line 133
    :cond_0
    return-void
.end method

.method private getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v1, 0x0

    .line 46
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "fileName":Ljava/lang/String;
    const-string v2, ".dcf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    const-string v1, "application/vnd.oma.drm.content"

    .line 70
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDrmMimeType. mimeType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 71
    return-object v1

    .line 51
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_1
    const-string v2, ".avi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 52
    const-string v1, "video/mux/AVI"

    goto :goto_0

    .line 53
    :cond_2
    const-string v2, ".mkv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 54
    const-string v1, "video/mux/MKV"

    goto :goto_0

    .line 55
    :cond_3
    const-string v2, ".divx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 56
    const-string v1, "video/mux/DivX"

    goto :goto_0

    .line 57
    :cond_4
    const-string v2, ".pyv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 58
    const-string v1, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 59
    :cond_5
    const-string v2, ".pya"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 60
    const-string v1, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 61
    :cond_6
    const-string v2, ".wmv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 62
    const-string v1, "video/x-ms-wmv"

    goto :goto_0

    .line 63
    :cond_7
    const-string v2, ".wma"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 64
    const-string v1, "audio/x-ms-wma"

    goto :goto_0

    .line 66
    :cond_8
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getFileType(Ljava/lang/String;)I
    .locals 5
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    .line 75
    const/4 v1, -0x1

    .line 77
    .local v1, "fileType":I
    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 80
    .local v0, "fileName":Ljava/lang/String;
    const-string v2, ".dcf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    const/4 v1, 0x1

    .line 93
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFileType. fileType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    .line 94
    return v1

    .line 82
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_1
    const-string v2, ".avi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, ".mkv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, ".divx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    :cond_2
    const/4 v1, 0x4

    goto :goto_0

    .line 84
    :cond_3
    const-string v2, ".pyv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, ".pya"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 85
    :cond_4
    const/4 v1, 0x2

    goto :goto_0

    .line 86
    :cond_5
    const-string v2, ".wmv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, ".wma"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 87
    :cond_6
    const/4 v1, 0x3

    goto :goto_0

    .line 89
    :cond_7
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private initDrmMgrClient()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    .line 38
    const-string v0, "initDrmMgrClient"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 39
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 41
    :cond_0
    return-void
.end method


# virtual methods
.method public checkRightsStatus(Ljava/lang/String;)I
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 98
    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 125
    :goto_0
    return v1

    .line 100
    :cond_0
    invoke-direct {p0}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->initDrmMgrClient()V

    .line 102
    iget-object v2, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 103
    .local v0, "LicenseStatus":I
    const/4 v1, 0x1

    .line 104
    .local v1, "rightStatus":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkRightsStatus. LicenseStatus = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 106
    packed-switch v0, :pswitch_data_0

    .line 121
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 108
    :pswitch_1
    const/4 v1, 0x2

    .line 109
    goto :goto_0

    .line 112
    :pswitch_2
    const/4 v1, 0x3

    .line 113
    goto :goto_0

    .line 116
    :pswitch_3
    const/4 v1, 0x0

    .line 117
    goto :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isDrmContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDrmFile(Ljava/lang/String;)I
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 142
    const-string v3, "isDrmFile"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 143
    if-nez p1, :cond_0

    .line 144
    const/4 v0, -0x1

    .line 162
    :goto_0
    return v0

    .line 147
    :cond_0
    const/4 v0, -0x1

    .line 149
    .local v0, "drmType":I
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v3, :cond_1

    .line 152
    iget-object v3, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v3, p1, v2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 154
    .local v1, "isDrmSupported":Z
    if-eqz v1, :cond_2

    .line 155
    invoke-direct {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 161
    .end local v1    # "isDrmSupported":Z
    :cond_1
    :goto_1
    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. drmType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(ZLjava/lang/String;)V

    goto :goto_0

    .line 157
    .restart local v1    # "isDrmSupported":Z
    :cond_2
    const-string v3, "isDrmFile. canHandle returned false. Not a drm file by extension"

    invoke-static {v3}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isNonDrmContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 170
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRightExpired(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeDrmMgrClient()V
    .locals 1

    .prologue
    .line 136
    const-string v0, "removeDrmMgrClient"

    invoke-static {v0}, Lcom/samsung/everglades/video/myvideo/common/Utils;->log(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/everglades/video/myvideo/common/DRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 139
    return-void
.end method
