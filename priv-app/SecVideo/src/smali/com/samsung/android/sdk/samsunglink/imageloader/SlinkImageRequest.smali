.class public Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
.super Ljava/lang/Object;
.source "SlinkImageRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private albumId:J

.field private localData:Ljava/lang/String;

.field private localSourceAlbumId:J

.field private localSourceMediaId:J

.field private maxHeight:I

.field private maxWidth:I

.field private mediaType:I

.field private orientation:I

.field private priority:I

.field private rowId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 291
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 292
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 293
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 294
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 295
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 296
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 297
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 298
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 299
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 300
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V
    .locals 2
    .param p1, "original"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 106
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 107
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 108
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 109
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 110
    iget-wide v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    iput-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 111
    iget-object v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 112
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 113
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 114
    iget v0, p1, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    iput v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 115
    return-void
.end method

.method public static createFromCursor(Landroid/database/Cursor;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 12
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I
    .param p3, "priority"    # I

    .prologue
    const-wide/16 v10, 0x0

    .line 37
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>()V

    .line 38
    .local v0, "imageRequest":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 39
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 40
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 42
    const-string v8, "_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    .line 45
    const-string v8, "media_type"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iput v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    .line 48
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 49
    const-string v8, "orientation"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iput v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    .line 53
    :cond_0
    const-string v8, "local_source_media_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 55
    .local v5, "localSourceMediaIdColumnIndex":I
    if-ltz v5, :cond_1

    .line 56
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 57
    .local v6, "localSourceMediaId":J
    cmp-long v8, v6, v10

    if-lez v8, :cond_1

    .line 58
    iput-wide v6, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    .line 62
    .end local v6    # "localSourceMediaId":J
    :cond_1
    const-string v8, "local_data"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 63
    .local v1, "localDataColumnIndex":I
    if-ltz v1, :cond_2

    .line 64
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    .line 67
    :cond_2
    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/4 v9, 0x2

    if-eq v8, v9, :cond_3

    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v9, 0xd

    if-eq v8, v9, :cond_3

    iget v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    const/16 v9, 0xe

    if-ne v8, v9, :cond_4

    .line 71
    :cond_3
    const-string v8, "album_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    .line 74
    const-string v8, "local_source_album_id"

    invoke-interface {p0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 76
    .local v4, "localSourceAlbumIdColumnIndex":I
    if-ltz v4, :cond_4

    .line 77
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 78
    .local v2, "localSourceAlbumId":J
    cmp-long v8, v2, v10

    if-lez v8, :cond_4

    .line 79
    iput-wide v2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    .line 84
    .end local v2    # "localSourceAlbumId":J
    .end local v4    # "localSourceAlbumIdColumnIndex":I
    :cond_4
    return-object v0
.end method

.method public static createFromRequest(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;III)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .locals 1
    .param p0, "request"    # Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I
    .param p3, "priority"    # I

    .prologue
    .line 93
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;)V

    .line 94
    .local v0, "imageRequest":Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;
    iput p1, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    .line 95
    iput p2, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    .line 96
    iput p3, v0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    .line 98
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 186
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    return-wide v0
.end method

.method public getLocalData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalSourceAlbumId()J
    .locals 2

    .prologue
    .line 206
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    return-wide v0
.end method

.method public getLocalSourceMediaId()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    return-wide v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    return v0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    return v0
.end method

.method public getRowId()J
    .locals 2

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SlinkImageRequest [rowId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 277
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->rowId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 278
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->mediaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->orientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceMediaId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 281
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->albumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 282
    iget-wide v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localSourceAlbumId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->localData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 284
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->maxHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 286
    iget v0, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageRequest;->priority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 287
    return-void
.end method
