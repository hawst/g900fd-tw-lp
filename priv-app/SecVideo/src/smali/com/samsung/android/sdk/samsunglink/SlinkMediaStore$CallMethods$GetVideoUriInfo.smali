.class public Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods$GetVideoUriInfo;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetVideoUriInfo"
.end annotation


# static fields
.field public static final INTENT_ARG_CONTENT_ID:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.INTENT_ARG_CONTENT_ID"

.field public static final KEY_RESULT_CAPTION_INDEX_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_CAPTION_INDEX_URI"

.field public static final KEY_RESULT_CAPTION_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_CAPTION_URI"

.field public static final KEY_RESULT_HTTP_PROXY_INFO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_HTTP_PROXY_INFO"

.field public static final KEY_RESULT_LOCAL_FILE_INFO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_LOCAL_FILE_INFO"

.field public static final KEY_RESULT_OPTIMIZED_VIDEO_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_OPTIMIZED_VIDEO_URI"

.field public static final KEY_RESULT_SAME_ACCESS_POINT_INFO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_SAME_ACCESS_POINT_INFO"

.field public static final KEY_RESULT_SCS_INFO:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_SCS_INFO"

.field public static final KEY_RESULT_VIDEO_URI:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.KEY_RESULT_VIDEO_URI"

.field public static final NAME:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetVideoUriInfo.NAME"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
