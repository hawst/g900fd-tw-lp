.class public Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;
.super Ljava/lang/Object;
.source "SlinkUserSettings.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->context:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 24
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 26
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;

    if-nez v0, :cond_1

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;

    .line 29
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public getMarketingPushEnabled()Z
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.MARKETING_PUSH"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->getBooleanSetting(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getVideoOptimizationEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->getBooleanSetting(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setMargetingPushEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.MARKETING_PUSH"

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->setSetting(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 90
    return-void
.end method

.method public setVideoOptimizationEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkUserSettings;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->setSetting(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 66
    return-void
.end method
