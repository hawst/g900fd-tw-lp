.class public Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;
.super Ljava/lang/Object;
.source "SlinkFileBrowserUtils.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;->context:Landroid/content/Context;

    .line 42
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const-class v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 26
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "context is null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 28
    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;

    if-nez v0, :cond_1

    .line 29
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;

    .line 31
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;->sInstance:Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 45
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/SlinkFileBrowserUtils;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$CallMethods;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.CallMethods.GetASP10FileBrowserErrorCode.NAME"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 50
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "method_result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method
