.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser$DirectoryInfoColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$FileBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DirectoryInfoColumns"
.end annotation


# static fields
.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final FILE_COUNT:Ljava/lang/String; = "file_count"

.field public static final FILE_ID:Ljava/lang/String; = "document_id"

.field public static final HOME_SYNC_FLAGS:Ljava/lang/String; = "home_sync_flags"

.field public static final ICON_ID:Ljava/lang/String; = "icon"

.field public static final PARENT_DISPLAY_NAME:Ljava/lang/String; = "parent_display_name"

.field public static final PARENT_FILE_ID:Ljava/lang/String; = "parent_id"

.field public static final PARENT_HOME_SYNC_FLAGS:Ljava/lang/String; = "home_sync_flags"

.field public static final PARENT_ICON_ID:Ljava/lang/String; = "parent_icon"

.field public static final PATH:Ljava/lang/String; = "path"
