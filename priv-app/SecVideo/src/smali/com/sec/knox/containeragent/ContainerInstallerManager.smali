.class public Lcom/sec/knox/containeragent/ContainerInstallerManager;
.super Ljava/lang/Object;
.source "ContainerInstallerManager.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private conn:Landroid/content/ServiceConnection;

.field private containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

.field private mContext:Landroid/content/Context;

.field private mServiceBound:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "ContainerInstallerManager"

    sput-object v0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "connection"    # Landroid/content/ServiceConnection;

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    .line 36
    iput-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mContext:Landroid/content/Context;

    .line 37
    iput-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    .line 39
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mServiceBound:Z

    .line 42
    iput-object p1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mContext:Landroid/content/Context;

    .line 43
    new-instance v1, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/knox/containeragent/ContainerInstallerManager$1;-><init>(Lcom/sec/knox/containeragent/ContainerInstallerManager;Landroid/content/ServiceConnection;)V

    iput-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    .line 61
    new-instance v0, Landroid/content/Intent;

    .line 62
    const-string v1, "com.sec.knox.containeragent.service.containerinstallermanager.ContainerInstallerManagerService"

    .line 61
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 66
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/knox/containeragent/ContainerInstallerManager;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/knox/containeragent/ContainerInstallerManager;Z)V
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mServiceBound:Z

    return-void
.end method

.method static synthetic access$3(Lcom/sec/knox/containeragent/ContainerInstallerManager;Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    return-void
.end method


# virtual methods
.method public KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "srcPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v1, p1, p2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .param p2, "sndPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "srcPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v1, p1, p2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 161
    .local v1, "result":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v2, :cond_0

    .line 163
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v2, p1}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 168
    :cond_0
    :goto_0
    return-object v1

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z
    .locals 6
    .param p1, "packagePath"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;

    .prologue
    const/4 v2, 0x0

    .line 84
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v1, "filePackage":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 86
    sget-object v3, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " does not exist() and return false"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    .end local v1    # "filePackage":Ljava/io/File;
    :cond_0
    :goto_0
    return v2

    .line 90
    .restart local v1    # "filePackage":Ljava/io/File;
    :cond_1
    iget-object v3, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v3, :cond_0

    .line 91
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v2, p1, p2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->installPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .end local v1    # "filePackage":Ljava/io/File;
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public isContainerAvailable()Z
    .locals 3

    .prologue
    .line 118
    const/4 v1, 0x0

    .line 120
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v2, :cond_0

    .line 121
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->isContainerAvailable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 125
    :cond_0
    :goto_0
    return v1

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isKNOXFileRelayAvailable()Z
    .locals 3

    .prologue
    .line 130
    const/4 v1, 0x0

    .line 132
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->isKNOXFileRelayAvailable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 137
    :cond_0
    :goto_0
    return v1

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public resetLockTimer()V
    .locals 3

    .prologue
    .line 172
    sget-object v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    const-string v2, "resetLockTimer()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_0

    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v1}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->resetLockTimer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindContainerManager()V
    .locals 3

    .prologue
    .line 74
    sget-object v0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unbindContainerManager() mServiceBound="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mServiceBound:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", conn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-boolean v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mServiceBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->conn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 78
    :cond_0
    return-void
.end method

.method public uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;

    .prologue
    .line 104
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v1, p1, p2}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->uninstallPackage(Ljava/lang/String;Lcom/sec/knox/containeragent/IContainerInstallerManagerCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 107
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public upgradeCompleted(Z)V
    .locals 4
    .param p1, "bResult"    # Z

    .prologue
    .line 183
    sget-object v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "upgradeCompleted("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    if-eqz v1, :cond_0

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/sec/knox/containeragent/ContainerInstallerManager;->containerService:Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;

    invoke-interface {v1, p1}, Lcom/sec/knox/containeragent/service/containerinstallermanager/IContainerInstallerManagerService;->upgradeCompleted(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
