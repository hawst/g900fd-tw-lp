.class public Lcom/sec/android/videowall/FileMgr;
.super Ljava/lang/Object;
.source "FileMgr.java"


# static fields
.field private static BuildVersion:Ljava/lang/String; = null

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final DBG:Z

.field private static final INDEX_DB_ID:I = 0x0

.field private static final INDEX_DB_IS_EXIST:I = 0x7

.field private static final INDEX_DB_NB_CHAPTER:I = 0x5

.field private static final INDEX_DB_NB_LIST:I = 0x6

.field private static final INDEX_DB_THUMB_STATUS:I = 0x4

.field private static final INDEX_DB_VIDEO_ID:I = 0x1

.field private static final INDEX_DB_VIDEO_NAME:I = 0x3

.field private static final INDEX_DB_VIDEO_PATH:I = 0x2

.field public static final INDEX_VIDEO_ID:I = 0x0

.field public static final INDEX_VIDEO_NAME:I = 0x2

.field public static final INDEX_VIDEO_PATH:I = 0x1

.field public static final KEY_ID:Ljava/lang/String; = "_id"

.field public static final KEY_IS_EXIST:Ljava/lang/String; = "isexist"

.field public static final KEY_NB_CHAPTER:Ljava/lang/String; = "nbchapter"

.field public static final KEY_NB_LIST:Ljava/lang/String; = "nblist"

.field public static final KEY_THUMB_STATUS:Ljava/lang/String; = "thumbnailstat"

.field public static final KEY_VIDEO_ID:Ljava/lang/String; = "videoid"

.field public static final KEY_VIDEO_NAME:Ljava/lang/String; = "name"

.field public static final KEY_VIDEO_PATH:Ljava/lang/String; = "path"

.field private static final PROVIDER_NAME:Ljava/lang/String; = "com.sec.provider.videowall"

.field private static final TAG:Ljava/lang/String; = "videowall-FileMgr"

.field public static THUMBANIL_PROJECTION:[Ljava/lang/String; = null

.field private static VWGlobal:Lcom/sec/android/videowall/Global; = null

.field public static final VW_STAT_COMPLETE:I = 0xa03

.field public static final VW_STAT_NEW:I = 0xa01

.field public static final VW_STAT_NOT_SUPPORT:I = 0xa04

.field public static final VW_STAT_ONGOING:I = 0xa02

.field private static VersionName:Ljava/lang/String; = null

.field private static final baseThumbnailPath:Ljava/lang/String;

.field private static final defaultThumbnailPath:Ljava/lang/String;

.field private static final externalPath:Ljava/lang/String; = "/storage/extSdCard"

.field private static final internalPath:Ljava/lang/String;


# instance fields
.field public filemgrInterrupt:I

.field private m_context:Landroid/content/Context;

.field public proj:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const-string v0, "content://com.sec.provider.videowall"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    .line 42
    sget-boolean v0, Lcom/sec/android/videowall/Global;->DBG:Z

    sput-boolean v0, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 45
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 46
    const-string v2, "videoid"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 47
    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 48
    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    .line 49
    const-string v2, "thumbnailstat"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 50
    const-string v2, "nbchapter"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 51
    const-string v2, "nblist"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 52
    const-string v2, "isexist"

    aput-object v2, v0, v1

    .line 44
    sput-object v0, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    .line 60
    const-string v0, "gsm.version.baseband"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->VersionName:Ljava/lang/String;

    .line 61
    const-string v0, "ro.build.version.release"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->BuildVersion:Ljava/lang/String;

    .line 63
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->internalPath:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->internalPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/.thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->defaultThumbnailPath:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->defaultThumbnailPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->VersionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->BuildVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->baseThumbnailPath:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/videowall/FileMgr;->VWGlobal:Lcom/sec/android/videowall/Global;

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    .line 72
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    .line 73
    const-string v3, "_id"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    .line 74
    const-string v4, "_data"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 76
    const-string v4, "title"

    aput-object v4, v2, v3

    iput-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    .line 79
    iput v5, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    .line 87
    iput-object p1, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    .line 88
    new-instance v2, Lcom/sec/android/videowall/Global;

    invoke-direct {v2}, Lcom/sec/android/videowall/Global;-><init>()V

    sput-object v2, Lcom/sec/android/videowall/FileMgr;->VWGlobal:Lcom/sec/android/videowall/Global;

    .line 89
    iput v5, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    .line 91
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->baseThumbnailPath:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 93
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->defaultThumbnailPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .local v1, "folder_default":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    sget-object v2, Lcom/sec/android/videowall/FileMgr;->defaultThumbnailPath:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/videowall/FileMgr;->DeleteDir(Ljava/lang/String;)V

    .line 97
    .end local v1    # "folder_default":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private DeleteDir(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 101
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 103
    .local v1, "childFileList":[Ljava/io/File;
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    .line 112
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 113
    return-void

    .line 103
    :cond_0
    aget-object v0, v1, v3

    .line 105
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 106
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/videowall/FileMgr;->DeleteDir(Ljava/lang/String;)V

    .line 103
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method public static isVzwVosDir(Ljava/lang/String;)Z
    .locals 1
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 394
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public DeleteFolderNotDBSync()V
    .locals 19

    .prologue
    .line 439
    new-instance v13, Ljava/io/File;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->baseThumbnailPath:Ljava/lang/String;

    invoke-direct {v13, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 441
    .local v13, "folder":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    new-instance v12, Ljava/io/File;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->baseThumbnailPath:Ljava/lang/String;

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 443
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v9

    .line 445
    .local v9, "childFileList":[Ljava/io/File;
    array-length v0, v9

    move/from16 v18, v0

    const/4 v2, 0x0

    move v15, v2

    :goto_0
    move/from16 v0, v18

    if-lt v15, v0, :cond_1

    .line 467
    .end local v9    # "childFileList":[Ljava/io/File;
    .end local v12    # "file":Ljava/io/File;
    :cond_0
    return-void

    .line 445
    .restart local v9    # "childFileList":[Ljava/io/File;
    .restart local v12    # "file":Ljava/io/File;
    :cond_1
    aget-object v8, v9, v15

    .line 447
    .local v8, "childFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 448
    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "movie_"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 450
    .local v14, "vid":Ljava/lang/String;
    const-wide/16 v16, -0x1

    .line 452
    .local v16, "video_id":J
    :try_start_0
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    .line 456
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    .line 457
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "videoid="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v16

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 456
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 459
    .local v10, "cs":Landroid/database/Cursor;
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_2

    .line 460
    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/videowall/FileMgr;->DeleteDir(Ljava/lang/String;)V

    .line 461
    sget-boolean v2, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v2, :cond_2

    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "deleteAllThumbnail(), vid : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 445
    .end local v10    # "cs":Landroid/database/Cursor;
    .end local v14    # "vid":Ljava/lang/String;
    .end local v16    # "video_id":J
    :cond_3
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_0

    .line 453
    .restart local v14    # "vid":Ljava/lang/String;
    .restart local v16    # "video_id":J
    :catch_0
    move-exception v11

    .line 454
    .local v11, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1
.end method

.method public changeThumbnailFolder(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "oldVid"    # J
    .param p5, "newVid"    # J

    .prologue
    .line 404
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v3

    .line 405
    .local v3, "oldThmbPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 406
    .local v2, "oldFile":Ljava/io/File;
    invoke-virtual {p0, p1, p2, p5, p6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v1

    .line 407
    .local v1, "newThmbPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 408
    .local v0, "newFile":Ljava/io/File;
    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 409
    const-string v4, "videowall-FileMgr"

    const-string v5, "folder name changes failed!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_0
    return-void
.end method

.method public checkExtSdCardFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pathName"    # Ljava/lang/String;

    .prologue
    .line 399
    const-string v0, ".*/storage/extSdCard/.*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public createVideoThumbnail(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 280
    const/4 v0, 0x0

    .line 281
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 282
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    sget-boolean v2, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createVideoThumbnail. time : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    const-wide/16 v2, 0x3e8

    mul-long/2addr p2, v2

    .line 285
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 286
    invoke-virtual {v1, p2, p3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 287
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 289
    return-object v0
.end method

.method public dbDeleteRecordnThumbs()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 415
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    .line 416
    sget-object v2, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    const-string v3, "isexist=0"

    move-object v5, v4

    .line 415
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 418
    .local v6, "cs":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 420
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    if-eqz v0, :cond_1

    .line 430
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/videowall/FileMgr;->DeleteFolderNotDBSync()V

    .line 435
    return-void

    .line 421
    :cond_1
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 422
    .local v8, "path":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 423
    .local v7, "name":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 425
    .local v10, "vid":J
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/videowall/FileMgr;->dbDelete_id(I)V

    .line 426
    invoke-virtual {p0, v8, v7, v10, v11}, Lcom/sec/android/videowall/FileMgr;->deleteAllThumbnail(Ljava/lang/String;Ljava/lang/String;J)V

    .line 428
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0
.end method

.method public dbDelete_id(I)V
    .locals 4
    .param p1, "rowid"    # I

    .prologue
    .line 607
    const-string v1, "videowall-FileMgr"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dbDelete_id : thumbid[ "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ] Deleted"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    iget-object v1, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 609
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 610
    return-void
.end method

.method public dbInsert(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vid"    # J

    .prologue
    const/4 v5, 0x0

    .line 563
    sget-boolean v2, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dbInsert : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :cond_0
    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 568
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 569
    .local v1, "initialValues":Landroid/content/ContentValues;
    const-string v2, "videoid"

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 570
    const-string v2, "path"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v2, "name"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    const-string v2, "thumbnailstat"

    const/16 v3, 0xa01

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 573
    const-string v2, "nbchapter"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 574
    const-string v2, "nblist"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 575
    const-string v2, "isexist"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 577
    sget-object v2, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 578
    return-void
.end method

.method public dbIsexistFlagInit()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 614
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 616
    .local v6, "cs":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 617
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    if-eqz v0, :cond_1

    .line 621
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 622
    return-void

    .line 618
    :cond_1
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0, v7}, Lcom/sec/android/videowall/FileMgr;->dbUpdate_isexist(II)V

    .line 619
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0
.end method

.method public dbPrintList()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 524
    const-string v0, "videowall-FileMgr"

    const-string v1, "-> dbPrintList()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 526
    .local v6, "cs":Landroid/database/Cursor;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v7, v0, :cond_0

    .line 529
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 530
    return-void

    .line 527
    :cond_0
    invoke-virtual {p0, v6, v7}, Lcom/sec/android/videowall/FileMgr;->dbPrintOut(Landroid/database/Cursor;I)V

    .line 526
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public dbPrintOut(Landroid/database/Cursor;I)V
    .locals 4
    .param p1, "cur"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 533
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 535
    sget-boolean v0, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v0, :cond_0

    .line 536
    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dbPrintOut : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const-string v0, "videowall-FileMgr"

    const-string v1, "=========================================="

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "id : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "videoid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isexist : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_0
    return-void
.end method

.method public dbUpdate_extExist(IJJ)V
    .locals 6
    .param p1, "rowid"    # I
    .param p2, "newVideoid"    # J
    .param p4, "exist"    # J

    .prologue
    .line 594
    sget-boolean v2, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dbUpdate_isexist : vid [ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] dbid[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_0
    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 597
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 599
    .local v1, "initialValues":Landroid/content/ContentValues;
    const-string v2, "isexist"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 600
    const-string v2, "videoid"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 602
    sget-object v2, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 603
    return-void
.end method

.method public dbUpdate_isexist(II)V
    .locals 5
    .param p1, "rowid"    # I
    .param p2, "exist"    # I

    .prologue
    .line 582
    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dbUpdate_isexist : dbid[ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 585
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 587
    .local v1, "initialValues":Landroid/content/ContentValues;
    const-string v2, "isexist"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 589
    sget-object v2, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 590
    return-void
.end method

.method public deleteAllThumbnail(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 5
    .param p1, "Path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vid"    # J

    .prologue
    .line 310
    sget-boolean v2, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v2, :cond_0

    const-string v2, "videowall-FileMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "deleteAllThumbnail(), vid : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "FilePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 314
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 315
    invoke-direct {p0, v0}, Lcom/sec/android/videowall/FileMgr;->DeleteDir(Ljava/lang/String;)V

    .line 317
    :cond_1
    return-void
.end method

.method public deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 7
    .param p1, "Path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "chapter"    # I

    .prologue
    .line 294
    sget-boolean v4, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v4, :cond_0

    const-string v4, "videowall-FileMgr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "deleteLastThumbnail(), vid : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", chapter : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    const/4 v2, 0x0

    .line 297
    .local v2, "lastThumbnail":Ljava/lang/String;
    const/4 v3, 0x0

    .line 298
    .local v3, "lastTmpthumbnail":Ljava/lang/String;
    sget-object v4, Lcom/sec/android/videowall/FileMgr;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v4}, Lcom/sec/android/videowall/Global;->getNumberOfChapter()I

    move-result v4

    if-ge p5, v4, :cond_1

    .line 299
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v2

    .line 300
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/videowall/FileMgr;->getTmpThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v3

    .line 302
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 303
    .local v0, "file":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 304
    .local v1, "file1":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 305
    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 306
    :cond_3
    return-void
.end method

.method public getFileNameByPathName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 169
    const-string v7, ""

    .line 170
    .local v7, "name":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_data=\""

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\""

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 170
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 172
    .local v6, "cur":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 173
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 174
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 176
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 177
    return-object v7
.end method

.method public getFileNameByVideoid(J)Ljava/lang/String;
    .locals 9
    .param p1, "vid"    # J

    .prologue
    const/4 v4, 0x0

    .line 156
    const-string v7, ""

    .line 157
    .local v7, "name":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    .line 158
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 157
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 159
    .local v6, "cur":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 160
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 161
    const/4 v0, 0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 163
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 164
    return-object v7
.end method

.method public getOnlyFileName(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 2
    .param p1, "video_cur"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 136
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 137
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "name":Ljava/lang/String;
    return-object v0
.end method

.method public getPathFileName(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 2
    .param p1, "video_cur"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 129
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 130
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "path":Ljava/lang/String;
    return-object v0
.end method

.method public getPathFileNameByVideoid(J)Ljava/lang/String;
    .locals 9
    .param p1, "vid"    # J

    .prologue
    const/4 v4, 0x0

    .line 143
    const-string v7, ""

    .line 144
    .local v7, "path":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    .line 145
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 144
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 146
    .local v6, "cur":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 147
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 148
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 150
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 151
    return-object v7
.end method

.method public getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;
    .locals 7
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "chapter"    # I

    .prologue
    .line 233
    const-string v4, "\\/"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, "tmp":Ljava/lang/String;
    move-object p2, v3

    .line 235
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "thumbnailname":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 238
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 240
    :cond_0
    if-gez p5, :cond_1

    .line 241
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".ListView.lvl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 246
    :goto_0
    return-object v2

    .line 243
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0000"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, "number":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".lvl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 3
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "mvideoid"    # J

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->baseThumbnailPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "movie_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTmpThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;
    .locals 7
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "chapter"    # I

    .prologue
    .line 251
    const-string v4, "\\/"

    const-string v5, ""

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 252
    .local v3, "tmp":Ljava/lang/String;
    move-object p2, v3

    .line 253
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    .line 254
    .local v2, "thumbnailname":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 256
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 258
    :cond_0
    if-gez p5, :cond_1

    .line 259
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".ListView.lvl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 264
    :goto_0
    return-object v2

    .line 261
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0000"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "number":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".lvl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getVWStatus(I)Ljava/lang/String;
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 552
    packed-switch p1, :pswitch_data_0

    .line 557
    const-string v0, "VW_UNKNOWN"

    :goto_0
    return-object v0

    .line 553
    :pswitch_0
    const-string v0, "VW_STAT_NEW"

    goto :goto_0

    .line 554
    :pswitch_1
    const-string v0, "VW_STAT_ONGOING"

    goto :goto_0

    .line 555
    :pswitch_2
    const-string v0, "VW_STAT_COMPLETE"

    goto :goto_0

    .line 556
    :pswitch_3
    const-string v0, "VW_STAT_NOT_SUPPORT"

    goto :goto_0

    .line 552
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getVideoId(Landroid/database/Cursor;I)J
    .locals 3
    .param p1, "video_cur"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 122
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 123
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 124
    .local v0, "id":J
    return-wide v0
.end method

.method public getVideoList(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 369
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getVideoidByPathName(Ljava/lang/String;)J
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 182
    const-wide/16 v8, -0x1

    .line 183
    .local v8, "vid":J
    const/4 v6, 0x0

    .line 186
    .local v6, "cur":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    .line 187
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_data=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 186
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 188
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 189
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 194
    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_1
    :goto_0
    return-wide v8

    .line 191
    :catch_0
    move-exception v7

    .line 192
    .local v7, "ex":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 193
    .end local v7    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 194
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 195
    :cond_2
    throw v0
.end method

.method public hasFreeSpace()Z
    .locals 6

    .prologue
    .line 384
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->internalPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 385
    .local v0, "extdir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    const-wide/32 v4, 0x1dcd6500

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 386
    const/4 v1, 0x1

    .line 389
    :goto_0
    return v1

    .line 388
    :cond_0
    sget-boolean v1, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "videowall-FileMgr"

    const-string v2, "free space is not sufficient"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasThumbnailsforChapter(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 7
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J

    .prologue
    .line 222
    const/4 v0, 0x1

    .line 223
    .local v0, "isthumb":Z
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/videowall/FileMgr;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v1}, Lcom/sec/android/videowall/Global;->getNumberOfChapter()I

    move-result v1

    if-lt v6, v1, :cond_0

    .line 228
    return v0

    :cond_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    .line 224
    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v1

    if-nez v1, :cond_1

    .line 225
    const/4 v0, 0x0

    .line 223
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public hasThumbnailsforList(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 7
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J

    .prologue
    .line 213
    const/4 v0, 0x1

    .line 214
    .local v0, "isthumb":Z
    const/4 v6, -0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v1

    if-nez v1, :cond_0

    .line 215
    const/4 v0, 0x0

    .line 217
    :cond_0
    return v0
.end method

.method public isExtSdCard()Z
    .locals 6

    .prologue
    .line 374
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/extSdCard"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v0, "extdir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 376
    const/4 v1, 0x1

    .line 378
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z
    .locals 3
    .param p1, "sPathName"    # Ljava/lang/String;
    .param p2, "sOnlyFileName"    # Ljava/lang/String;
    .param p3, "vid"    # J
    .param p5, "chapter"    # I

    .prologue
    .line 202
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "thumbnailname":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 204
    .local v0, "fileImage":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    const/4 v2, 0x1

    .line 207
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public loadVideoThumbnail(Landroid/database/Cursor;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "video_cur"    # Landroid/database/Cursor;
    .param p2, "position"    # I

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Lcom/sec/android/videowall/FileMgr;->getVideoId(Landroid/database/Cursor;I)J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 275
    .local v0, "tmpbmp":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public renameLiveThumbnailFile(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 23
    .param p1, "old_vid"    # J
    .param p3, "oldFileName"    # Ljava/lang/String;
    .param p4, "newFilePath"    # Ljava/lang/String;

    .prologue
    .line 320
    const-string v7, "videowall-FileMgr"

    const-string v12, "renameLiveThumnailFile()"

    invoke-static {v7, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/sec/android/videowall/FileMgr;->getVideoidByPathName(Ljava/lang/String;)J

    move-result-wide v10

    .line 325
    .local v10, "new_vid":J
    const-wide/16 v20, -0x1

    cmp-long v7, v10, v20

    if-nez v7, :cond_0

    const/4 v7, 0x0

    .line 355
    :goto_0
    return v7

    .line 327
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    move-wide/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v17

    .line 328
    .local v17, "oldThumbDirPath":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v10, v11}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v8

    .line 330
    .local v8, "newThumbDirPath":Ljava/lang/String;
    new-instance v16, Ljava/io/File;

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 331
    .local v16, "oldDirFile":Ljava/io/File;
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 333
    .local v13, "newDirFile":Ljava/io/File;
    if-eqz v16, :cond_1

    :try_start_0
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 334
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 335
    const/4 v12, -0x1

    move-object/from16 v7, p0

    move-object/from16 v9, p3

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v19

    .line 337
    .local v19, "oldThumbFilePath":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 338
    .local v18, "oldThumbFile":Ljava/io/File;
    const/16 v7, 0x2f

    move-object/from16 v0, p4

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    const/16 v12, 0x2e

    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 340
    .local v9, "newFileName":Ljava/lang/String;
    if-eqz v18, :cond_1

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 341
    const/4 v12, -0x1

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v15

    .line 342
    .local v15, "newThumbFilePath":Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 343
    .local v14, "newThumbFile":Ljava/io/File;
    if-eqz v14, :cond_1

    .line 344
    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_1

    .line 345
    const/4 v7, 0x1

    goto :goto_0

    .line 351
    .end local v9    # "newFileName":Ljava/lang/String;
    .end local v14    # "newThumbFile":Ljava/io/File;
    .end local v15    # "newThumbFilePath":Ljava/lang/String;
    .end local v18    # "oldThumbFile":Ljava/io/File;
    .end local v19    # "oldThumbFilePath":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 352
    .local v6, "ex":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 355
    .end local v6    # "ex":Ljava/lang/Exception;
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public renameThumbFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "tmp"    # Ljava/lang/String;
    .param p2, "thumb"    # Ljava/lang/String;

    .prologue
    .line 360
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 361
    .local v1, "oldFile":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 362
    .local v0, "newFile":Ljava/io/File;
    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 363
    const-string v2, "videowall-FileMgr"

    const-string v3, "folder name changes failed!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    :cond_0
    return-void
.end method

.method public setInterruptflag(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 117
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    .line 118
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public thumbnailDBSync()V
    .locals 14

    .prologue
    .line 470
    const-string v0, "videowall-FileMgr"

    const-string v1, "thumbnailDBSync -1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/videowall/FileMgr;->dbIsexistFlagInit()V

    .line 473
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 474
    iget-object v2, p0, Lcom/sec/android/videowall/FileMgr;->proj:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 473
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 476
    .local v13, "service_videoCursor":Landroid/database/Cursor;
    if-nez v13, :cond_0

    .line 520
    :goto_0
    return-void

    .line 478
    :cond_0
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v10, v0, :cond_1

    iget v0, p0, Lcom/sec/android/videowall/FileMgr;->filemgrInterrupt:I

    if-eqz v0, :cond_2

    .line 517
    :cond_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 519
    invoke-virtual {p0}, Lcom/sec/android/videowall/FileMgr;->dbDeleteRecordnThumbs()V

    goto :goto_0

    .line 480
    :cond_2
    invoke-virtual {p0, v13, v10}, Lcom/sec/android/videowall/FileMgr;->getPathFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v12

    .line 481
    .local v12, "path":Ljava/lang/String;
    invoke-virtual {p0, v13, v10}, Lcom/sec/android/videowall/FileMgr;->getOnlyFileName(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v11

    .line 482
    .local v11, "name":Ljava/lang/String;
    invoke-virtual {p0, v13, v10}, Lcom/sec/android/videowall/FileMgr;->getVideoId(Landroid/database/Cursor;I)J

    move-result-wide v6

    .line 484
    .local v6, "vid":J
    if-eqz v12, :cond_3

    if-nez v11, :cond_4

    .line 478
    :cond_3
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 487
    :cond_4
    sget-boolean v0, Lcom/sec/android/videowall/FileMgr;->DBG:Z

    if-eqz v0, :cond_5

    const-string v0, "videowall-FileMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "][ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ][ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " sync !!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    :cond_5
    invoke-virtual {p0, v12}, Lcom/sec/android/videowall/FileMgr;->checkExtSdCardFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 490
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    .line 491
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "videoid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 490
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 492
    .local v8, "cs":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_6

    .line 493
    invoke-virtual {p0, v12, v11, v6, v7}, Lcom/sec/android/videowall/FileMgr;->dbInsert(Ljava/lang/String;Ljava/lang/String;J)V

    .line 498
    :goto_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 495
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 496
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/videowall/FileMgr;->dbUpdate_isexist(II)V

    goto :goto_3

    .line 500
    .end local v8    # "cs":Landroid/database/Cursor;
    :cond_7
    iget-object v0, p0, Lcom/sec/android/videowall/FileMgr;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/videowall/FileMgr;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/videowall/FileMgr;->THUMBANIL_PROJECTION:[Ljava/lang/String;

    .line 501
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "path=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 500
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 502
    .local v9, "cur":Landroid/database/Cursor;
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_8

    .line 503
    invoke-virtual {p0, v12, v11, v6, v7}, Lcom/sec/android/videowall/FileMgr;->dbInsert(Ljava/lang/String;Ljava/lang/String;J)V

    .line 513
    :goto_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 505
    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 506
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v0, v6, v0

    if-eqz v0, :cond_9

    .line 507
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const-wide/16 v4, 0x1

    move-object v0, p0

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/videowall/FileMgr;->dbUpdate_extExist(IJJ)V

    .line 508
    const/4 v0, 0x1

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object v1, p0

    move-object v2, v12

    move-object v3, v11

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/videowall/FileMgr;->changeThumbnailFolder(Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_4

    .line 510
    :cond_9
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/videowall/FileMgr;->dbUpdate_isexist(II)V

    goto :goto_4
.end method
