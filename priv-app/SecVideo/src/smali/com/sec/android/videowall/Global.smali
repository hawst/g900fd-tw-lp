.class public Lcom/sec/android/videowall/Global;
.super Ljava/lang/Object;
.source "Global.java"


# static fields
.field public static Clock:I = 0x0

.field public static Cores:I = 0x0

.field public static final DBG:Z

.field private static Density:F = 0.0f

.field public static Device:I = 0x0

.field public static final NATIVE_DBG:Z = false

.field public static final Samsung_Device_1024x600_hw:I = 0xa09

.field public static final Samsung_Device_1280x720_hw:I = 0xa02

.field public static final Samsung_Device_1280x720_sw:I = 0xa01

.field public static final Samsung_Device_1280x800_dp10_hw:I = 0xa08

.field public static final Samsung_Device_1280x800_hw:I = 0xa07

.field public static final Samsung_Device_1920x1080_hw:I = 0xa0a

.field public static final Samsung_Device_2560x1440_hw:I = 0xa0f

.field public static final Samsung_Device_2560x1600_hw:I = 0xa0b

.field public static final Samsung_Device_800x480_hw:I = 0xa06

.field public static final Samsung_Device_800x480_sw:I = 0xa05

.field public static final Samsung_Device_960x540_hw:I = 0xa04

.field public static final Samsung_Device_960x540_sw:I = 0xa03

.field public static final Samsung_Device_dual_under15:I = 0xa0d

.field public static final Samsung_Device_no_library:I = 0xa0e

.field public static final Samsung_Device_single:I = 0xa0c

.field public static final Support_AP_Frequency:I = 0x162010

.field public static final TAG:Ljava/lang/String; = "videowall-Global"

.field public static final VIDEOWALL_LANDSCAPE:I = 0x0

.field public static final VIDEOWALL_PHONE:I = 0x0

.field public static final VIDEOWALL_PORTRATE:I = 0x1

.field public static final VIDEOWALL_TABLET:I = 0x1

.field public static isswkey:Z = false

.field public static final longTime:I = 0xa

.field public static final shortTime:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/sec/android/videowall/Global;->DBG:Z

    .line 64
    sput v1, Lcom/sec/android/videowall/Global;->Device:I

    .line 66
    sput v1, Lcom/sec/android/videowall/Global;->Cores:I

    .line 68
    sput v1, Lcom/sec/android/videowall/Global;->Clock:I

    .line 74
    sput-boolean v1, Lcom/sec/android/videowall/Global;->isswkey:Z

    .line 76
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getChapterTranscodeThread()I
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->readCoreNum()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getChapterViewThumbnailDisplayHeight()I
    .locals 2

    .prologue
    const/16 v0, 0xa0

    .line 282
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 305
    :goto_0
    :pswitch_0
    return v0

    .line 285
    :pswitch_1
    const/16 v0, 0x80

    goto :goto_0

    .line 291
    :pswitch_2
    const/16 v0, 0xd0

    goto :goto_0

    .line 293
    :pswitch_3
    const/16 v0, 0x90

    goto :goto_0

    .line 295
    :pswitch_4
    const/16 v0, 0xb0

    goto :goto_0

    .line 297
    :pswitch_5
    const/16 v0, 0xc0

    goto :goto_0

    .line 299
    :pswitch_6
    const/16 v0, 0x138

    goto :goto_0

    .line 301
    :pswitch_7
    const/high16 v0, 0x42740000    # 61.0f

    sget v1, Lcom/sec/android/videowall/Global;->Density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 303
    :pswitch_8
    const/16 v0, 0x180

    goto :goto_0

    .line 282
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public getChapterViewThumbnailDisplayWidth()I
    .locals 2

    .prologue
    const/16 v0, 0x130

    .line 252
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 277
    :pswitch_0
    const/16 v0, 0x100

    :goto_0
    :pswitch_1
    return v0

    .line 255
    :pswitch_2
    const/16 v0, 0xb0

    goto :goto_0

    .line 257
    :pswitch_3
    const/16 v0, 0xd0

    goto :goto_0

    .line 259
    :pswitch_4
    const/16 v0, 0xe0

    goto :goto_0

    .line 261
    :pswitch_5
    const/16 v0, 0x120

    goto :goto_0

    .line 265
    :pswitch_6
    const/16 v0, 0xc0

    goto :goto_0

    .line 271
    :pswitch_7
    const/16 v0, 0x1ce

    goto :goto_0

    .line 273
    :pswitch_8
    const/high16 v0, 0x42be0000    # 95.0f

    sget v1, Lcom/sec/android/videowall/Global;->Density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0

    .line 275
    :pswitch_9
    const/16 v0, 0x260

    goto :goto_0

    .line 252
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public getChapterViewThumbnailHeight()I
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailDisplayHeight()I

    move-result v0

    .line 244
    .local v0, "height":I
    rem-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_0

    .line 245
    div-int/lit8 v1, v0, 0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v0, v1, 0x10

    .line 248
    :cond_0
    return v0
.end method

.method public getChapterViewThumbnailHeightforPort()I
    .locals 3

    .prologue
    const/16 v1, 0x140

    const/16 v0, 0xa0

    .line 338
    sget v2, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v2, :pswitch_data_0

    .line 359
    :goto_0
    :pswitch_0
    return v0

    .line 341
    :pswitch_1
    const/16 v0, 0x80

    goto :goto_0

    .line 347
    :pswitch_2
    const/16 v0, 0xd0

    goto :goto_0

    .line 349
    :pswitch_3
    const/16 v0, 0x90

    goto :goto_0

    :pswitch_4
    move v0, v1

    .line 355
    goto :goto_0

    :pswitch_5
    move v0, v1

    .line 357
    goto :goto_0

    .line 338
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getChapterViewThumbnailWidth()I
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailDisplayWidth()I

    move-result v0

    .line 232
    .local v0, "width":I
    rem-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_0

    .line 233
    div-int/lit8 v1, v0, 0x10

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v0, v1, 0x10

    .line 236
    :cond_0
    return v0
.end method

.method public getChapterViewThumbnailWidthforPort()I
    .locals 2

    .prologue
    const/16 v0, 0x100

    .line 310
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 333
    :goto_0
    :pswitch_0
    return v0

    .line 313
    :pswitch_1
    const/16 v0, 0xb0

    goto :goto_0

    .line 315
    :pswitch_2
    const/16 v0, 0xd0

    goto :goto_0

    .line 317
    :pswitch_3
    const/16 v0, 0xe0

    goto :goto_0

    .line 319
    :pswitch_4
    const/16 v0, 0x120

    goto :goto_0

    .line 321
    :pswitch_5
    const/16 v0, 0x130

    goto :goto_0

    .line 323
    :pswitch_6
    const/16 v0, 0xc0

    goto :goto_0

    .line 329
    :pswitch_7
    const/16 v0, 0x1c0

    goto :goto_0

    .line 331
    :pswitch_8
    const/16 v0, 0x200

    goto :goto_0

    .line 310
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getListViewThumbnailHeight()I
    .locals 3

    .prologue
    const/16 v1, 0xd0

    const/16 v0, 0xa0

    .line 390
    sget v2, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 410
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 398
    goto :goto_0

    .line 402
    :pswitch_2
    const/16 v0, 0xf0

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 406
    goto :goto_0

    .line 408
    :pswitch_4
    const/16 v0, 0x130

    goto :goto_0

    .line 390
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getListViewThumbnailHeightEverglades()I
    .locals 2

    .prologue
    const/16 v0, 0xa0

    .line 435
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 450
    :goto_0
    :pswitch_0
    return v0

    .line 437
    :pswitch_1
    const/16 v0, 0x80

    goto :goto_0

    .line 442
    :pswitch_2
    const/16 v0, 0xd0

    goto :goto_0

    .line 444
    :pswitch_3
    const/16 v0, 0x140

    goto :goto_0

    .line 446
    :pswitch_4
    const/16 v0, 0x110

    goto :goto_0

    .line 448
    :pswitch_5
    const/16 v0, 0x170

    goto :goto_0

    .line 435
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public getListViewThumbnailWidth()I
    .locals 2

    .prologue
    const/16 v0, 0x100

    .line 364
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 385
    :goto_0
    :pswitch_0
    return v0

    .line 367
    :pswitch_1
    const/16 v0, 0xe0

    goto :goto_0

    .line 373
    :pswitch_2
    const/16 v0, 0x150

    goto :goto_0

    .line 375
    :pswitch_3
    const/16 v0, 0x120

    goto :goto_0

    .line 377
    :pswitch_4
    const/16 v0, 0x190

    goto :goto_0

    .line 381
    :pswitch_5
    const/16 v0, 0x160

    goto :goto_0

    .line 383
    :pswitch_6
    const/16 v0, 0x1f0

    goto :goto_0

    .line 364
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getListViewThumbnailWidthEverglades()I
    .locals 2

    .prologue
    const/16 v0, 0x110

    .line 415
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 430
    :goto_0
    :pswitch_0
    return v0

    .line 417
    :pswitch_1
    const/16 v0, 0xf0

    goto :goto_0

    .line 422
    :pswitch_2
    const/16 v0, 0x160

    goto :goto_0

    .line 424
    :pswitch_3
    const/16 v0, 0x220

    goto :goto_0

    .line 426
    :pswitch_4
    const/16 v0, 0x1e0

    goto :goto_0

    .line 428
    :pswitch_5
    const/16 v0, 0x280

    goto :goto_0

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public getLongTimeDuration()I
    .locals 2

    .prologue
    const/16 v0, 0x64

    .line 492
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->getNumberOfChapter()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 500
    :goto_0
    :sswitch_0
    return v0

    .line 496
    :sswitch_1
    const/16 v0, 0x78

    goto :goto_0

    .line 498
    :sswitch_2
    const/16 v0, 0xaa

    goto :goto_0

    .line 492
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xc -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public getNumberOfChapter()I
    .locals 1

    .prologue
    .line 187
    const/16 v0, 0xc

    return v0
.end method

.method public getNumberOfList()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x1

    return v0
.end method

.method public getNumberOfMaxStream()I
    .locals 1

    .prologue
    .line 183
    const/16 v0, 0x10

    return v0
.end method

.method public getNumberOfMaxThumbnailList()I
    .locals 1

    .prologue
    .line 191
    const/16 v0, 0x14

    return v0
.end method

.method public getServiceTranscodeThread()I
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    return v0
.end method

.method public getShortTimeDuration()I
    .locals 2

    .prologue
    const/16 v0, 0x1e

    .line 479
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->getNumberOfChapter()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 487
    :goto_0
    :sswitch_0
    return v0

    .line 483
    :sswitch_1
    const/16 v0, 0x28

    goto :goto_0

    .line 485
    :sswitch_2
    const/16 v0, 0x3c

    goto :goto_0

    .line 479
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xc -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public getSupportList(I)I
    .locals 1
    .param p1, "device_type"    # I

    .prologue
    .line 218
    packed-switch p1, :pswitch_data_0

    .line 223
    const/16 v0, 0xc

    :goto_0
    return v0

    .line 220
    :pswitch_0
    const/16 v0, 0x14

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getSupportVW()Z
    .locals 1

    .prologue
    .line 207
    sget v0, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v0, :pswitch_data_0

    .line 213
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 211
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0xa0c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getThumbnailFps()I
    .locals 1

    .prologue
    .line 179
    const/16 v0, 0xa

    return v0
.end method

.method public getTimetextHeight()I
    .locals 2

    .prologue
    const/16 v0, 0x2d

    .line 455
    sget v1, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 462
    const/16 v0, 0x1e

    :pswitch_0
    return v0

    .line 455
    :pswitch_data_0
    .packed-switch 0xa07
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getTimetextHeightforPort()I
    .locals 2

    .prologue
    const/16 v1, 0x46

    .line 467
    sget v0, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v0, :pswitch_data_0

    .line 474
    :pswitch_0
    return v1

    .line 467
    :pswitch_data_0
    .packed-switch 0xa07
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isWFDConnected(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 625
    const/4 v0, 0x0

    return v0
.end method

.method public printDeviceInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    sget v0, Lcom/sec/android/videowall/Global;->Device:I

    packed-switch v0, :pswitch_data_0

    .line 535
    :pswitch_0
    const-string v0, "else"

    :goto_0
    return-object v0

    .line 507
    :pswitch_1
    const-string v0, "Samsung_Device_1280x720_sw"

    goto :goto_0

    .line 509
    :pswitch_2
    const-string v0, "Samsung_Device_1280x720_hw"

    goto :goto_0

    .line 511
    :pswitch_3
    const-string v0, "Samsung_Device_960x540_sw"

    goto :goto_0

    .line 513
    :pswitch_4
    const-string v0, "Samsung_Device_960x540_hw"

    goto :goto_0

    .line 515
    :pswitch_5
    const-string v0, "Samsung_Device_800x480_sw"

    goto :goto_0

    .line 517
    :pswitch_6
    const-string v0, "Samsung_Device_800x480_hw"

    goto :goto_0

    .line 519
    :pswitch_7
    const-string v0, "Samsung_Device_1024x600_hw"

    goto :goto_0

    .line 521
    :pswitch_8
    const-string v0, "Samsung_Device_1280x800_hw"

    goto :goto_0

    .line 523
    :pswitch_9
    const-string v0, "Samsung_Device_1280x800_dp10_hw"

    goto :goto_0

    .line 525
    :pswitch_a
    const-string v0, "Samsung_Device_single"

    goto :goto_0

    .line 527
    :pswitch_b
    const-string v0, "Samsung_Device_dual_under15"

    goto :goto_0

    .line 529
    :pswitch_c
    const-string v0, "Samsung_Device_1920x1080_hw"

    goto :goto_0

    .line 531
    :pswitch_d
    const-string v0, "Samsung_Device_2560x1440_hw"

    goto :goto_0

    .line 533
    :pswitch_e
    const-string v0, "Samsung_Device_2560x1600_hw"

    goto :goto_0

    .line 505
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_c
        :pswitch_e
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_d
    .end packed-switch
.end method

.method public readCoreNum()I
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v8, 0x0

    .line 541
    const/4 v0, 0x0

    .line 542
    .local v0, "core_num":I
    const-string v6, ""

    .line 543
    .local v6, "line":Ljava/lang/String;
    const/4 v4, 0x0

    .line 545
    .local v4, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/FileReader;

    const-string v10, "/sys/devices/system/cpu/possible"

    invoke-direct {v9, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 546
    .end local v4    # "in":Ljava/io/BufferedReader;
    .local v5, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 547
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 548
    sget-boolean v9, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v9, :cond_0

    const-string v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "read line = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 562
    :cond_0
    if-eqz v6, :cond_1

    const-string v9, "null"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 563
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v12, :cond_4

    .line 564
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 579
    :cond_1
    :goto_0
    if-ge v0, v12, :cond_2

    const/4 v0, 0x1

    .line 580
    :cond_2
    const-string v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "core_num = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .end local v5    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    move v8, v0

    .line 582
    :cond_3
    :goto_1
    return v8

    .line 550
    :catch_0
    move-exception v1

    .line 551
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    const-string v9, "videowall-Global"

    const-string v10, "IOException!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    if-eqz v4, :cond_3

    .line 554
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 555
    :catch_1
    move-exception v2

    .line 557
    .local v2, "e1":Ljava/io/IOException;
    const-string v9, "videowall-Global"

    const-string v10, "IOException! e1"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 565
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v5    # "in":Ljava/io/BufferedReader;
    :cond_4
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_1

    .line 566
    const/4 v7, 0x0

    .line 567
    .local v7, "start_num":C
    const/4 v3, 0x0

    .line 568
    .local v3, "end_num":C
    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 569
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 571
    sget-boolean v8, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v8, :cond_5

    .line 572
    const-string v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "char 1 = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    const-string v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "char 2 = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    :cond_5
    sub-int v8, v3, v7

    add-int/lit8 v0, v8, 0x1

    goto :goto_0

    .line 550
    .end local v3    # "end_num":C
    .end local v7    # "start_num":C
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public readMaxClock()I
    .locals 9

    .prologue
    .line 586
    const/4 v0, 0x0

    .line 587
    .local v0, "core_clock":I
    const-string v5, ""

    .line 588
    .local v5, "line":Ljava/lang/String;
    const/4 v3, 0x0

    .line 590
    .local v3, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 591
    .end local v3    # "in":Ljava/io/BufferedReader;
    .local v4, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 592
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 593
    sget-boolean v6, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v6, :cond_0

    const-string v6, "videowall-Global"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "read line = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 606
    :cond_0
    if-eqz v5, :cond_1

    .line 607
    const-string v6, "null"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 608
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 609
    sget-boolean v6, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v6, :cond_1

    const-string v6, "videowall-Global"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "core_clock = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    move v6, v0

    .line 612
    :goto_0
    return v6

    .line 594
    :catch_0
    move-exception v1

    .line 595
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    const-string v6, "videowall-Global"

    const-string v7, "IOException!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    if-eqz v3, :cond_2

    .line 598
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 603
    :cond_2
    :goto_2
    const/4 v6, 0x0

    goto :goto_0

    .line 599
    :catch_1
    move-exception v2

    .line 601
    .local v2, "e1":Ljava/io/IOException;
    const-string v6, "videowall-Global"

    const-string v7, "IOException! e1"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 594
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public setDevice(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v12, 0x41200000    # 10.0f

    .line 79
    sget-boolean v10, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v10, :cond_0

    const-string v10, "videowall-Global"

    const-string v11, "-> setDevice"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    sget v10, Lcom/sec/android/videowall/Global;->Device:I

    if-eqz v10, :cond_2

    .line 81
    sget-boolean v10, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v10, :cond_1

    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Device : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->printDeviceInfo()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->readCoreNum()I

    move-result v10

    sput v10, Lcom/sec/android/videowall/Global;->Cores:I

    .line 86
    sget v10, Lcom/sec/android/videowall/Global;->Cores:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 87
    const/16 v10, 0xa0c

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    .line 88
    sget-boolean v10, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v10, :cond_1

    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Device : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->printDeviceInfo()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :cond_3
    sget v10, Lcom/sec/android/videowall/Global;->Cores:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_5

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->readMaxClock()I

    move-result v10

    sput v10, Lcom/sec/android/videowall/Global;->Clock:I

    .line 92
    sget v10, Lcom/sec/android/videowall/Global;->Clock:I

    const v11, 0x162010

    if-lt v10, v11, :cond_4

    const-string v10, "msm8660"

    const-string v11, "ro.board.platform"

    invoke-static {v11}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 93
    :cond_4
    const/16 v10, 0xa0d

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    .line 94
    sget-boolean v10, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v10, :cond_1

    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Device : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->printDeviceInfo()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Clock : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget v12, Lcom/sec/android/videowall/Global;->Clock:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 100
    :cond_5
    :try_start_0
    const-string v10, "savsff"

    invoke-static {v10}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 101
    const-string v10, "vwengine"

    invoke-static {v10}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    const-string v10, "window"

    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    .line 109
    .local v9, "wm":Landroid/view/WindowManager;
    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 110
    .local v0, "display":Landroid/view/Display;
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 111
    .local v4, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 112
    iget v10, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v10, v12

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v10, v12

    sput v10, Lcom/sec/android/videowall/Global;->Density:F

    .line 113
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v8

    .line 114
    .local v8, "wd":I
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    .line 116
    .local v3, "ht":I
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 117
    .local v5, "point":Landroid/graphics/Point;
    invoke-virtual {v0, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 118
    iget v7, v5, Landroid/graphics/Point;->x:I

    .line 119
    .local v7, "rawwd":I
    iget v6, v5, Landroid/graphics/Point;->y:I

    .line 121
    .local v6, "rawht":I
    if-le v7, v6, :cond_6

    move v1, v6

    .line 124
    .local v1, "dsp":I
    :goto_1
    const/4 v10, 0x0

    sput-boolean v10, Lcom/sec/android/videowall/Global;->isswkey:Z

    .line 132
    sparse-switch v1, :sswitch_data_0

    .line 172
    :goto_2
    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "density : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v12, Lcom/sec/android/videowall/Global;->Density:F

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " width : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " height : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Raw width : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " Raw height : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "dsp : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", swkey : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/sec/android/videowall/Global;->isswkey:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Cores : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget v12, Lcom/sec/android/videowall/Global;->Cores:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", Clock : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget v12, Lcom/sec/android/videowall/Global;->Clock:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    sget-boolean v10, Lcom/sec/android/videowall/Global;->DBG:Z

    if-eqz v10, :cond_1

    const-string v10, "videowall-Global"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Device : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/videowall/Global;->printDeviceInfo()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 102
    .end local v0    # "display":Landroid/view/Display;
    .end local v1    # "dsp":I
    .end local v3    # "ht":I
    .end local v4    # "metrics":Landroid/util/DisplayMetrics;
    .end local v5    # "point":Landroid/graphics/Point;
    .end local v6    # "rawht":I
    .end local v7    # "rawwd":I
    .end local v8    # "wd":I
    .end local v9    # "wm":Landroid/view/WindowManager;
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/lang/UnsatisfiedLinkError;
    const/16 v10, 0xa0e

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    .line 104
    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v11, "Fail to load library. Disable videowall."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v2    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v0    # "display":Landroid/view/Display;
    .restart local v3    # "ht":I
    .restart local v4    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v5    # "point":Landroid/graphics/Point;
    .restart local v6    # "rawht":I
    .restart local v7    # "rawwd":I
    .restart local v8    # "wd":I
    .restart local v9    # "wm":Landroid/view/WindowManager;
    :cond_6
    move v1, v7

    .line 121
    goto/16 :goto_1

    .line 134
    .restart local v1    # "dsp":I
    :sswitch_0
    sget-boolean v10, Lcom/sec/android/videowall/Global;->isswkey:Z

    if-eqz v10, :cond_7

    .line 135
    const/16 v10, 0xa01

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 137
    :cond_7
    const/16 v10, 0xa02

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 140
    :sswitch_1
    sget-boolean v10, Lcom/sec/android/videowall/Global;->isswkey:Z

    if-eqz v10, :cond_8

    .line 141
    const/16 v10, 0xa03

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 143
    :cond_8
    const/16 v10, 0xa04

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 146
    :sswitch_2
    sget-boolean v10, Lcom/sec/android/videowall/Global;->isswkey:Z

    if-eqz v10, :cond_9

    .line 147
    const/16 v10, 0xa05

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 149
    :cond_9
    const/16 v10, 0xa06

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 152
    :sswitch_3
    const/16 v10, 0xa09

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 155
    :sswitch_4
    sget v10, Lcom/sec/android/videowall/Global;->Density:F

    float-to-double v10, v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpl-double v10, v10, v12

    if-nez v10, :cond_a

    .line 156
    const/16 v10, 0xa08

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 158
    :cond_a
    const/16 v10, 0xa07

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 161
    :sswitch_5
    const/16 v10, 0xa0a

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 165
    :sswitch_6
    const/16 v10, 0xa0f

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 168
    :sswitch_7
    const/16 v10, 0xa0b

    sput v10, Lcom/sec/android/videowall/Global;->Device:I

    goto/16 :goto_2

    .line 132
    nop

    :sswitch_data_0
    .sparse-switch
        0x1e0 -> :sswitch_2
        0x21c -> :sswitch_1
        0x258 -> :sswitch_3
        0x2d0 -> :sswitch_0
        0x320 -> :sswitch_4
        0x438 -> :sswitch_5
        0x5a0 -> :sswitch_6
        0x5f0 -> :sswitch_6
        0x640 -> :sswitch_7
    .end sparse-switch
.end method
