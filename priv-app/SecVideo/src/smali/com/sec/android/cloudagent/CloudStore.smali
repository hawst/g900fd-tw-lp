.class public Lcom/sec/android/cloudagent/CloudStore;
.super Ljava/lang/Object;
.source "CloudStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;,
        Lcom/sec/android/cloudagent/CloudStore$API;,
        Lcom/sec/android/cloudagent/CloudStore$Music;,
        Lcom/sec/android/cloudagent/CloudStore$Videos;,
        Lcom/sec/android/cloudagent/CloudStore$Images;,
        Lcom/sec/android/cloudagent/CloudStore$Files;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "cloud"

.field public static final CACHED_PATH:Ljava/lang/String; = "CACHED_PATH"

.field public static final CLOUDDB_BASE_INDEX:I = 0x989680

.field public static final CLOUD_EXCEPTION:Ljava/lang/String; = "com.sec.android.cloudagent.exception"

.field public static final CONTENT_AUTHORITY:Ljava/lang/String; = "content://cloud/data/"

.field public static final DOWNLOAD_URL:Ljava/lang/String; = "DOWNLOAD_URL"

.field public static final KEY_ACTION:Ljava/lang/String; = "com.sec.android.cloudagent.exception.action"

.field public static final KEY_CONTENT_URI:Ljava/lang/String; = "content_uri"

.field private static final MEDIA:Ljava/lang/String; = "media"

.field public static final SHARE_URL:Ljava/lang/String; = "SHARE_URL"

.field public static final STREAMING_URL:Ljava/lang/String; = "STREAMING_URL"

.field protected static final TABLENAME_FILES:Ljava/lang/String; = "cloudfiles"

.field public static final TABLENAME_IMAGES:Ljava/lang/String; = "images"

.field public static final TABLENAME_MUSIC:Ljava/lang/String; = "music"

.field public static final TABLENAME_VIDEO:Ljava/lang/String; = "video"

.field public static final THUMBNAIL_BITMAP:Ljava/lang/String; = "ThumbnailBitmap"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 434
    return-void
.end method
