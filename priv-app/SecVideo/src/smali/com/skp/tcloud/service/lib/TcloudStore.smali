.class public Lcom/skp/tcloud/service/lib/TcloudStore;
.super Ljava/lang/Object;
.source "TcloudStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/skp/tcloud/service/lib/TcloudStore$API;,
        Lcom/skp/tcloud/service/lib/TcloudStore$Buckets;,
        Lcom/skp/tcloud/service/lib/TcloudStore$Files;,
        Lcom/skp/tcloud/service/lib/TcloudStore$Images;,
        Lcom/skp/tcloud/service/lib/TcloudStore$Videos;
    }
.end annotation


# static fields
.field public static final AGENT_VERSION:Ljava/lang/String; = "3.0"

.field public static final AUTHORITY:Ljava/lang/String; = "com.skp.tcloud"

.field public static final CLOUDDB_BASE_INDEX:I = 0x5f5e100

.field public static final CONTENT_AUTHORITY:Ljava/lang/String; = "content://tcloud/data/"

.field public static final IMAGE_ROOT_BUCKET_ID:Ljava/lang/String; = "0"

.field public static final IMAGE_ROOT_BUCKET_NAME:Ljava/lang/String; = "root"

.field public static final KEY_ACTION:Ljava/lang/String; = "com.sec.android.cloudagent.exception.action"

.field public static final KEY_CONTENT_URI:Ljava/lang/String; = "content_uri"

.field public static final PROJECTION_BUCKET:[Ljava/lang/String;

.field public static final PROJECTION_CLOUDFILES:[Ljava/lang/String;

.field public static final PROJECTION_IMAGES:[Ljava/lang/String;

.field public static final PROVIDER_AUTHORITY:Ljava/lang/String; = "com.skp.tcloud"

.field public static final PROVIDER_BUCKET:Ljava/lang/String; = "buckets"

.field public static final PROVIDER_FILES:Ljava/lang/String; = "files"

.field public static final PROVIDER_IMAGES:Ljava/lang/String; = "images"

.field public static final PROVIDER_VIDEOS:Ljava/lang/String; = "videos"

.field public static final SHARE_URL:Ljava/lang/String; = "SHARE_URL"

.field public static final STREAMING_URL:Ljava/lang/String; = "STREAMING_URL"

.field public static final TABLENAME_ALBUMS:Ljava/lang/String; = "albums"

.field public static final TABLENAME_ARTIST:Ljava/lang/String; = "artists"

.field public static final TABLENAME_AUDIO:Ljava/lang/String; = "audio"

.field public static final TABLENAME_BUCKET:Ljava/lang/String; = "buckets"

.field public static final TABLENAME_FILES:Ljava/lang/String; = "tcloudfiles"

.field public static final TABLENAME_IMAGES:Ljava/lang/String; = "images"

.field public static final TABLENAME_MAPPING:Ljava/lang/String; = "mapping"

.field public static final TABLENAME_PLAYLISTS:Ljava/lang/String; = "playlists"

.field public static final TABLENAME_VIDEO:Ljava/lang/String; = "video"

.field public static final TCLOUD_BUCKETS_URI:Landroid/net/Uri;

.field public static final TCLOUD_DB_NAME:Ljava/lang/String; = "tcloud"

.field public static final TCLOUD_EXCEPTION:Ljava/lang/String; = "com.skp.tcloud.service.exception"

.field public static final TCLOUD_FILES_URI:Landroid/net/Uri;

.field public static final TCLOUD_PHOTO_URI:Landroid/net/Uri;

.field public static final TCLOUD_VIDEO_URI:Landroid/net/Uri;

.field public static final THUMBNAIL_BITMAP:Ljava/lang/String; = "ThumbnailBitmap"

.field public static final VIDEO_ROOT_BUCKET_ID:Ljava/lang/String; = "1"

.field public static final VIDEO_ROOT_BUCKET_NAME:Ljava/lang/String; = "root"

.field public static final VIEW_TABLENAME_CLOUDFILES:Ljava/lang/String; = " [cloudfiles] "

.field public static final VIEW_TABLENAME_IMAGES:Ljava/lang/String; = " [images] "

.field public static final VIEW_TABLENAME_VIDEO:Ljava/lang/String; = " [video] "


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-array v0, v5, [Ljava/lang/String;

    .line 13
    const-string v1, "bucket_id"

    aput-object v1, v0, v2

    .line 14
    const-string v1, "bucket_display_name"

    aput-object v1, v0, v3

    .line 15
    const-string v1, "media_type"

    aput-object v1, v0, v4

    .line 12
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->PROJECTION_BUCKET:[Ljava/lang/String;

    .line 18
    new-array v0, v2, [Ljava/lang/String;

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->PROJECTION_IMAGES:[Ljava/lang/String;

    .line 22
    const/16 v0, 0x30

    new-array v0, v0, [Ljava/lang/String;

    .line 23
    const-string v1, "_id"

    aput-object v1, v0, v2

    .line 24
    const-string v1, "_data"

    aput-object v1, v0, v3

    .line 25
    const-string v1, "content_id"

    aput-object v1, v0, v4

    .line 26
    const-string v1, "bucket_display_name"

    aput-object v1, v0, v5

    const/4 v1, 0x4

    .line 27
    const-string v2, "bucket_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 28
    const-string v2, "category"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    .line 29
    const-string v2, "tcloud_albumart_path"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 30
    const-string v2, "tcloud_cached_path"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    .line 31
    const-string v2, "tcloud_is_cached"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    .line 32
    const-string v2, "tcloud_is_dir"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 33
    const-string v2, "tcloud_revision"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    .line 34
    const-string v2, "tcloud_revision"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    .line 35
    const-string v2, "tcloud_server_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    .line 36
    const-string v2, "tcloud_thumb_path"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    .line 37
    const-string v2, "composer"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 38
    const-string v2, "date_added"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    .line 39
    const-string v2, "date_modified"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    .line 40
    const-string v2, "datetaken"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 41
    const-string v2, "description"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    .line 42
    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    .line 43
    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    .line 44
    const-string v2, "format"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    .line 45
    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    .line 46
    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    .line 47
    const-string v2, "is_alarm"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    .line 48
    const-string v2, "is_drm"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    .line 49
    const-string v2, "is_favorite"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    .line 50
    const-string v2, "is_music"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    .line 51
    const-string v2, "is_notification"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    .line 52
    const-string v2, "tcloud_last_viewed"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    .line 53
    const-string v2, "isprivate"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    .line 54
    const-string v2, "language"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    .line 55
    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    .line 56
    const-string v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    .line 57
    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    .line 58
    const-string v2, "mini_thumb_data"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    .line 59
    const-string v2, "mini_thumb_magic"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    .line 60
    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    .line 61
    const-string v2, "orientation"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    .line 62
    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    .line 63
    const-string v2, "resumePos"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    .line 64
    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    .line 65
    const-string v2, "tags"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    .line 66
    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    .line 67
    const-string v2, "title_key"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    .line 68
    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    .line 69
    const-string v2, "tcloud_server_path"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    .line 70
    const-string v2, "media_type"

    aput-object v2, v0, v1

    .line 22
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->PROJECTION_CLOUDFILES:[Ljava/lang/String;

    .line 103
    const-string v0, "content://com.skp.tcloud/files"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 102
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_FILES_URI:Landroid/net/Uri;

    .line 105
    const-string v0, "content://com.skp.tcloud/images"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 104
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_PHOTO_URI:Landroid/net/Uri;

    .line 107
    const-string v0, "content://com.skp.tcloud/videos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 106
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_VIDEO_URI:Landroid/net/Uri;

    .line 109
    const-string v0, "content://com.skp.tcloud/buckets"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 108
    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_BUCKETS_URI:Landroid/net/Uri;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
