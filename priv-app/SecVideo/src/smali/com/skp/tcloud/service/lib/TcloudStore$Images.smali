.class public Lcom/skp/tcloud/service/lib/TcloudStore$Images;
.super Ljava/lang/Object;
.source "TcloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Images"
.end annotation


# static fields
.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final CLOUD_CACHED_PATH:Ljava/lang/String; = "tcloud_cached_path"

.field public static final CLOUD_IS_CACHED:Ljava/lang/String; = "tcloud_is_cached"

.field public static final CLOUD_IS_DIR:Ljava/lang/String; = "tcloud_is_dir"

.field public static final CLOUD_LAST_VIEWED:Ljava/lang/String; = "tcloud_last_viewed"

.field public static final CLOUD_REVISION:Ljava/lang/String; = "tcloud_revision"

.field public static final CLOUD_SERVER_ID:Ljava/lang/String; = "tcloud_server_id"

.field public static final CLOUD_THUMB_PATH:Ljava/lang/String; = "tcloud_thumb_path"

.field public static final CONTENT_ID:Ljava/lang/String; = "content_id"

.field public static final DATETAKEN:Ljava/lang/String; = "datetaken"

.field public static final DATE_ADDED:Ljava/lang/String; = "date_added"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "date_modified"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ISPRIVATE:Ljava/lang/String; = "isprivate"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final MINI_THUMB_DATA:Ljava/lang/String; = "mini_thumb_data"

.field public static final MINI_THUMB_MAGIC:Ljava/lang/String; = "mini_thumb_magic"

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"

.field public static final SIZE:Ljava/lang/String; = "_size"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final _DATA:Ljava/lang/String; = "_data"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 293
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_PHOTO_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public static getThumbnailUri(J)Landroid/net/Uri;
    .locals 4
    .param p0, "id"    # J

    .prologue
    .line 297
    sget-object v1, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 298
    .local v0, "uribuilder":Landroid/net/Uri$Builder;
    const-string v1, "id"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 299
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method
