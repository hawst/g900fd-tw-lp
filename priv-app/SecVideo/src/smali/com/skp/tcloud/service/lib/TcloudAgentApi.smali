.class public Lcom/skp/tcloud/service/lib/TcloudAgentApi;
.super Ljava/lang/Object;
.source "TcloudAgentApi.java"


# static fields
.field private static final OPERATION_CLOUD_AVAILABLE:I = 0x6

.field private static final OPERATION_DOWNLOAD:I = 0x2

.field private static final OPERATION_EMPTY:I = 0x0

.field private static final OPERATION_GET_IMAGE:I = 0x9

.field private static final OPERATION_OFFLINE_AVAILABLE:I = 0x7

.field private static final OPERATION_REVERT_OFFLINE:I = 0x8

.field private static final OPERATION_SHARED_URL:I = 0x3

.field private static final OPERATION_STREAMING_URL:I = 0x4

.field private static final OPERATION_SYNC:I = 0x1

.field private static final OPERATION_THUMBNAIL:I = 0x5

.field private static final TAG:Ljava/lang/String;

.field public static final TCLOUD_ACCOUNT_NAME:Ljava/lang/String; = "com.skp.tcloud"


# instance fields
.field private binded:Z

.field private operation:I

.field private serviceConnection:Landroid/content/ServiceConnection;

.field private tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

.field private uri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;

    invoke-direct {v0, p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;-><init>(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)V

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->serviceConnection:Landroid/content/ServiceConnection;

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    .line 112
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    return-void
.end method

.method static synthetic access$10(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Z
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->isCloudAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->makeAvailableOffline(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$13(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->revertAvailableOffline(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$14(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getImage(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/skp/tcloud/service/lib/TcloudAgentApi;I)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    return-void
.end method

.method static synthetic access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    return v0
.end method

.method static synthetic access$4(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Lcom/skp/tcloud/service/ITcloudAgentService;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    return-void
.end method

.method static synthetic access$5(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->sync()V

    return-void
.end method

.method static synthetic access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$7(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->download(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$8(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getSharedURL(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getStreamingURL(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bindService(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->serviceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    return v0
.end method

.method private download(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 129
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->download(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :goto_0
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static existsAccount(Landroid/content/Context;)Z
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 46
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 47
    .local v1, "am":Landroid/accounts/AccountManager;
    const-string v2, "com.skp.tcloud"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 48
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v2, v0

    if-lez v2, :cond_0

    .line 49
    const/4 v2, 0x1

    .line 51
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getFileCursor(Landroid/content/Context;J)Landroid/database/Cursor;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "_id"    # J

    .prologue
    const/4 v5, 0x0

    .line 373
    const-string v3, "_id = ?"

    .line 375
    .local v3, "mWhereClause":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 376
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 377
    sget-object v2, Lcom/skp/tcloud/service/lib/TcloudStore;->PROJECTION_CLOUDFILES:[Ljava/lang/String;

    .line 378
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    .line 379
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 376
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 382
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 383
    :cond_0
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "not found cloudfile db. id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :goto_0
    return-object v5

    .line 387
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 388
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fail moveToFirst id = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object v5, v6

    .line 391
    goto :goto_0
.end method

.method private getImage(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 188
    const/4 v1, 0x0

    .line 190
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/skp/tcloud/service/ITcloudAgentService;->getImage(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 194
    :goto_0
    return-object v1

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getSharedURL(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 138
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->getSharedURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 140
    :goto_0
    return-object v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getStreamingURL(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 145
    sget-object v1, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v2, "getStreamingURL call"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->getStreamingURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 149
    :goto_0
    return-object v1

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->getThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 157
    :goto_0
    return-object v1

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isCloudAvailable()Z
    .locals 3

    .prologue
    .line 162
    sget-object v1, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v2, "[TcloudApi] isCloudAvailable() call"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-interface {v1}, Lcom/skp/tcloud/service/ITcloudAgentService;->isCloudAvailable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 167
    :goto_0
    return v1

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v2, "[TcloudApi] isCloudAvailable() exception"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeAvailableOffline(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 173
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->makeAvailableOffline(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private revertAvailableOffline(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/skp/tcloud/service/ITcloudAgentService;->revertAvailableOffline(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private sync()V
    .locals 3

    .prologue
    .line 120
    :try_start_0
    sget-object v1, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v2, "[TcloudApi] sync() call"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->tcloudService:Lcom/skp/tcloud/service/ITcloudAgentService;

    invoke-interface {v1}, Lcom/skp/tcloud/service/ITcloudAgentService;->sync()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 199
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct download call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->download(Landroid/net/Uri;)V

    .line 210
    :cond_0
    return-void

    .line 204
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 205
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 207
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0
.end method

.method public getImage(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct getThumbnail call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getImage(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 258
    :goto_0
    return-object v0

    .line 252
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 253
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 255
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSharedURL(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 214
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct getSharedURL call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getSharedURL(Landroid/net/Uri;)Ljava/lang/String;

    .line 225
    :cond_0
    return-void

    .line 219
    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 220
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 222
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0
.end method

.method public getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_0

    .line 229
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct getStreamingURL call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getStreamingURL(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 239
    :goto_0
    return-object v0

    .line 233
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 234
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 236
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 237
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 243
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 345
    sget-object v6, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[getThumbnail] TcloudThumbnailManager getThumbnail call (uri): "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 348
    .local v3, "metauri":Landroid/net/Uri;
    const-string v6, "id"

    invoke-virtual {v3, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 349
    .local v0, "_id":J
    sget-object v6, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[getThumbnail] TcloudThumbnailManager getThumbnail (_id): "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-direct {p0, p1, v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getFileCursor(Landroid/content/Context;J)Landroid/database/Cursor;

    move-result-object v2

    .line 353
    .local v2, "cursor":Landroid/database/Cursor;
    if-nez v2, :cond_0

    .line 354
    sget-object v6, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v7, "[getThumbnail] TcloudThumbnailManager getThumbnail cursor is null"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :goto_0
    return-object v5

    .line 358
    :cond_0
    const-string v6, "tcloud_thumb_path"

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 359
    .local v4, "thumbnailPath":Ljava/lang/String;
    sget-object v6, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "[getThumbnail] TcloudThumbnailManager getThumbnail path : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_1

    .line 362
    sget-object v6, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v7, "[getThumbnail] TcloudThumbnailManager getThumbnail file not exist"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 367
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 368
    sget-object v5, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v6, "[getThumbnail] TcloudThumbnailManager getThumbnail return success"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0
.end method

.method public init(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 330
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "[TcloudApi] init(Context context) call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 332
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "[getThumbnail] already binded"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 337
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "[getThumbnail] init throw TcloudException.TCLOUD_AGENT_NOT_AVAILABLE"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isCloudAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-static {p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->existsAccount(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    const/4 v0, 0x0

    .line 266
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 287
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct makeAvailableOffline call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->makeAvailableOffline(Landroid/net/Uri;)V

    .line 298
    :cond_0
    return-void

    .line 292
    :cond_1
    const/4 v0, 0x7

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 293
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 295
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0
.end method

.method public revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 302
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct revertAvailableOffline call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-direct {p0, p2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->revertAvailableOffline(Landroid/net/Uri;)V

    .line 313
    :cond_0
    return-void

    .line 307
    :cond_1
    const/16 v0, 0x8

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 308
    iput-object p2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;

    .line 310
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0
.end method

.method public sync(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 316
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "[TcloudApi] sync(Context context) call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->binded:Z

    if-eqz v0, :cond_1

    .line 318
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;

    const-string v1, "binded... direct sync call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-direct {p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->sync()V

    .line 327
    :cond_0
    return-void

    .line 323
    :cond_1
    iput v2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I

    .line 324
    invoke-direct {p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->bindService(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudException;

    invoke-direct {v0, v2}, Lcom/skp/tcloud/service/lib/TcloudException;-><init>(I)V

    throw v0
.end method
