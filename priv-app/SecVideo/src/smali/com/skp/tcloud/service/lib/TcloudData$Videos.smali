.class public Lcom/skp/tcloud/service/lib/TcloudData$Videos;
.super Ljava/lang/Object;
.source "TcloudData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Videos"
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public artist:Ljava/lang/String;

.field public bookmark:I

.field public bucketDisplayName:Ljava/lang/String;

.field public bucketId:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public cloudCachedPath:Ljava/lang/String;

.field public cloudIsAvailableOffline:Z

.field public cloudIsCached:Z

.field public cloudIsDir:Z

.field public cloudRevision:I

.field public cloudServerId:Ljava/lang/String;

.field public cloudthumbPath:Ljava/lang/String;

.field public contentId:Ljava/lang/String;

.field public data:Ljava/lang/String;

.field public dateAdded:J

.field public dateModified:J

.field public dateTaken:J

.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public duration:J

.field public height:I

.field public id:J

.field public isPlayed:Z

.field public isPrivate:Z

.field public latitude:D

.field public longitude:D

.field public mediaType:I

.field public mimeType:Ljava/lang/String;

.field public miniThumbData:Ljava/lang/String;

.field public miniThumbMagic:I

.field public resolution:Ljava/lang/String;

.field public resumePos:I

.field public size:J

.field public tags:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->id:J

    .line 175
    iput v1, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->mediaType:I

    .line 176
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->data:Ljava/lang/String;

    .line 177
    const-string v0, "bucket_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->bucketDisplayName:Ljava/lang/String;

    .line 178
    const-string v0, "album"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->album:Ljava/lang/String;

    .line 179
    const-string v0, "artist"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->artist:Ljava/lang/String;

    .line 180
    const-string v0, "bucket_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->bucketId:Ljava/lang/String;

    .line 181
    const-string v0, "tcloud_cached_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudCachedPath:Ljava/lang/String;

    .line 182
    const-string v0, "category"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->category:Ljava/lang/String;

    .line 183
    const-string v0, "tcloud_is_cached"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudIsCached:Z

    .line 184
    const-string v0, "tcloud_is_dir"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudIsDir:Z

    .line 185
    const-string v0, "tcloud_is_available_offline"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudIsAvailableOffline:Z

    .line 186
    const-string v0, "tcloud_revision"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudRevision:I

    .line 187
    const-string v0, "bookmark"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->bookmark:I

    .line 188
    const-string v0, "tcloud_server_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->cloudServerId:Ljava/lang/String;

    .line 189
    const-string v0, "content_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->contentId:Ljava/lang/String;

    .line 190
    const-string v0, "date_added"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->dateAdded:J

    .line 191
    const-string v0, "date_modified"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->dateModified:J

    .line 192
    const-string v0, "datetaken"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->dateTaken:J

    .line 193
    const-string v0, "description"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->description:Ljava/lang/String;

    .line 194
    const-string v0, "_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->displayName:Ljava/lang/String;

    .line 195
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->duration:J

    .line 196
    const-string v0, "height"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->height:I

    .line 197
    const-string v0, "isPlayed"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->isPlayed:Z

    .line 198
    const-string v0, "isprivate"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->isPrivate:Z

    .line 199
    const-string v0, "latitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->latitude:D

    .line 200
    const-string v0, "longitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->longitude:D

    .line 201
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->mimeType:Ljava/lang/String;

    .line 202
    const-string v0, "mini_thumb_magic"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->miniThumbMagic:I

    .line 203
    const-string v0, "_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->size:J

    .line 204
    const-string v0, "resolution"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->resolution:Ljava/lang/String;

    .line 205
    const-string v0, "resumePos"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->resumePos:I

    .line 206
    const-string v0, "tags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->tags:Ljava/lang/String;

    .line 207
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->title:Ljava/lang/String;

    .line 208
    const-string v0, "width"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->width:I

    .line 209
    const-string v0, "mini_thumb_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Videos;->miniThumbData:Ljava/lang/String;

    .line 210
    return-void

    :cond_0
    move v0, v2

    .line 183
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 184
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 185
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 197
    goto/16 :goto_3

    :cond_4
    move v1, v2

    .line 198
    goto/16 :goto_4
.end method
