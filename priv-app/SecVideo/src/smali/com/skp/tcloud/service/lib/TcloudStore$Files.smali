.class public Lcom/skp/tcloud/service/lib/TcloudStore$Files;
.super Ljava/lang/Object;
.source "TcloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Files"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ARTIST:Ljava/lang/String; = "album_art"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ALBUM_KEY:Ljava/lang/String; = "album_key"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final ARTIST_KEY:Ljava/lang/String; = "artist_key"

.field public static final BOOKMARK:Ljava/lang/String; = "bookmark"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final CATEGORY:Ljava/lang/String; = "category"

.field public static final CLOUD_ALBUMART_PATH:Ljava/lang/String; = "tcloud_albumart_path"

.field public static final CLOUD_CACHED_PATH:Ljava/lang/String; = "tcloud_cached_path"

.field public static final CLOUD_ETAG:Ljava/lang/String; = "tcloud_etag"

.field public static final CLOUD_IS_AVAILABLE_OFFLINE:Ljava/lang/String; = "tcloud_is_available_offline"

.field public static final CLOUD_IS_AVAILABLE_THUMB:Ljava/lang/String; = "tcloud_is_available_thumb"

.field public static final CLOUD_IS_CACHED:Ljava/lang/String; = "tcloud_is_cached"

.field public static final CLOUD_IS_DIR:Ljava/lang/String; = "tcloud_is_dir"

.field public static final CLOUD_LAST_VIEWED:Ljava/lang/String; = "tcloud_last_viewed"

.field public static final CLOUD_PARENT_ID:Ljava/lang/String; = "tcloud_revision"

.field public static final CLOUD_REVISION:Ljava/lang/String; = "tcloud_revision"

.field public static final CLOUD_SERVER_ID:Ljava/lang/String; = "tcloud_server_id"

.field public static final CLOUD_SERVER_PATH:Ljava/lang/String; = "tcloud_server_path"

.field public static final CLOUD_THUMB_PATH:Ljava/lang/String; = "tcloud_thumb_path"

.field public static final COMPOSER:Ljava/lang/String; = "composer"

.field public static final CONTENT_ID:Ljava/lang/String; = "content_id"

.field public static final DATETAKEN:Ljava/lang/String; = "datetaken"

.field public static final DATE_ADDED:Ljava/lang/String; = "date_added"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "date_modified"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "_display_name"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final FORMAT:Ljava/lang/String; = "format"

.field public static final GENRE_NAME:Ljava/lang/String; = "genre_name"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ISPRIVATE:Ljava/lang/String; = "isprivate"

.field public static final IS_ALARM:Ljava/lang/String; = "is_alarm"

.field public static final IS_DRM:Ljava/lang/String; = "is_drm"

.field public static final IS_FAVORITE:Ljava/lang/String; = "is_favorite"

.field public static final IS_MUSIC:Ljava/lang/String; = "is_music"

.field public static final IS_NOTIFICATION:Ljava/lang/String; = "is_notification"

.field public static final IS_PLAYED:Ljava/lang/String; = "isPlayed"

.field public static final IS_PODCAST:Ljava/lang/String; = "is_podcast"

.field public static final IS_RINGTONE:Ljava/lang/String; = "is_ringtone"

.field public static final IS_SOUND:Ljava/lang/String; = "is_sound"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final MEDIA_TYPE:Ljava/lang/String; = "media_type"

.field public static final MEDIA_TYPE_ALL:I = 0x5

.field public static final MEDIA_TYPE_AUDIO:I = 0x2

.field public static final MEDIA_TYPE_DOC:I = 0x4

.field public static final MEDIA_TYPE_IMAGE:I = 0x1

.field public static final MEDIA_TYPE_NONE:I = 0x0

.field public static final MEDIA_TYPE_VIDEO:I = 0x3

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final MINI_THUMB_DATA:Ljava/lang/String; = "mini_thumb_data"

.field public static final MINI_THUMB_MAGIC:Ljava/lang/String; = "mini_thumb_magic"

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"

.field public static final PARENT:Ljava/lang/String; = "parent"

.field public static final RECENTLY_ADDED_REMOVE_FLAG:Ljava/lang/String; = "recently_added_remove_flag"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final RESUME_POS:Ljava/lang/String; = "resumePos"

.field public static final SIZE:Ljava/lang/String; = "_size"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TITLE_KEY:Ljava/lang/String; = "title_key"

.field public static final TRACK:Ljava/lang/String; = "track"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final YEAR:Ljava/lang/String; = "year"

.field public static final YEAR_NAME:Ljava/lang/String; = "year_name"

.field public static final _DATA:Ljava/lang/String; = "_data"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 258
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore;->TCLOUD_FILES_URI:Landroid/net/Uri;

    return-object v0
.end method
