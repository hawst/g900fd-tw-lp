.class Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;
.super Ljava/lang/Object;
.source "TcloudAgentApi.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudAgentApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;


# direct methods
.method constructor <init>(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v3, 0x1

    .line 66
    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[TcloudApi] onServiceConnected = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-static {p2}, Lcom/skp/tcloud/service/ITcloudAgentService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/skp/tcloud/service/ITcloudAgentService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$4(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Lcom/skp/tcloud/service/ITcloudAgentService;)V

    .line 68
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-static {v0, v3}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$1(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Z)V

    .line 69
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 70
    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[TcloudApi] call sync"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->sync()V
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$5(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 74
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->download(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$7(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 78
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getSharedURL(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$8(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;

    .line 81
    :cond_2
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 82
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getStreamingURL(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$9(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;

    .line 85
    :cond_3
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 86
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$10(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    .line 89
    :cond_4
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 90
    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "[TcloudApi] call isCloudAvailable"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->isCloudAvailable()Z
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$11(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Z

    .line 94
    :cond_5
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_6

    .line 95
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->makeAvailableOffline(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$12(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V

    .line 98
    :cond_6
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_7

    .line 99
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->revertAvailableOffline(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$13(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)V

    .line 102
    :cond_7
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->operation:I
    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$3(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_8

    .line 103
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    iget-object v1, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->uri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$6(Lcom/skp/tcloud/service/lib/TcloudAgentApi;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getImage(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$14(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Landroid/net/Uri;)Ljava/lang/String;

    .line 106
    :cond_8
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$2(Lcom/skp/tcloud/service/lib/TcloudAgentApi;I)V

    .line 107
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v2, 0x0

    .line 59
    # getter for: Lcom/skp/tcloud/service/lib/TcloudAgentApi;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onServiceDisconnected call"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-static {v0, v2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$1(Lcom/skp/tcloud/service/lib/TcloudAgentApi;Z)V

    .line 61
    iget-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudAgentApi$1;->this$0:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-static {v0, v2}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->access$2(Lcom/skp/tcloud/service/lib/TcloudAgentApi;I)V

    .line 62
    return-void
.end method
