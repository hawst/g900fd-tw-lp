.class final Lcom/samsung/android/scloud/backup/core/BNRTaskService$7;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 223
    const-string v7, "SOURCE_LIST"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 224
    .local v5, "sourceList":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 225
    .local v4, "source":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/scloud/backup/core/BNRTaskService$7$1;

    invoke-direct {v3, p0, v4, p1}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$7$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$7;Ljava/lang/String;Landroid/content/Context;)V

    .line 238
    .local v3, "runnable":Ljava/lang/Runnable;
    new-instance v6, Ljava/lang/Thread;

    const-string v7, "OPERATION_PAUSE"

    invoke-direct {v6, v3, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 239
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 242
    .end local v3    # "runnable":Ljava/lang/Runnable;
    .end local v4    # "source":Ljava/lang/String;
    .end local v6    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
