.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 510
    sget-boolean v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestPause()V

    .line 512
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 513
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    .line 525
    :goto_0
    return-void

    .line 515
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setSourceList()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    .line 516
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-boolean v0, v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    if-eqz v0, :cond_1

    .line 518
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onOptionsItemSelected - noneSelected = true"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 521
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->onRestoreClicked()V

    goto :goto_0
.end method
