.class Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;
.super Ljava/lang/Thread;
.source "SCloudBackupReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReceiverThread"
.end annotation


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 175
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->context:Landroid/content/Context;

    .line 176
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$1;

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private startNotificationService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v0, "service":Landroid/content/Intent;
    const-class v1, Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 185
    const-string v1, "Notification Type"

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 186
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 187
    return-void
.end method

.method private startRestore(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 190
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    .line 191
    .local v2, "mMetaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, "mTargetDeviceId":Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    const-string v6, "REQUEST_RESTORE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 193
    .local v5, "service":Landroid/content/Intent;
    invoke-virtual {v2, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v3, v6, [Ljava/lang/String;

    .line 194
    .local v3, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v2, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v6, "TRIGGER"

    const-string v7, "SetupWizard"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    const-string v6, "SOURCE_DEVICE"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p1, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 198
    invoke-virtual {p1, v5}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 199
    invoke-virtual {v2, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 200
    .local v1, "key":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-virtual {v2, v1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    goto :goto_0

    .line 201
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityRestore()V

    .line 202
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->startRestore(Landroid/content/Context;)V

    .line 180
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->context:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->startNotificationService(Landroid/content/Context;)V

    .line 181
    return-void
.end method
