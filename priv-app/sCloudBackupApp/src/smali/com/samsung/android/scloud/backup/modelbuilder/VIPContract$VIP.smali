.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/VIPContract$VIP;
.super Ljava/lang/Object;
.source "VIPContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/VIPContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VIP"
.end annotation


# static fields
.field public static final CONTACT_ID:Ljava/lang/String; = "Contact_Id"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "DisplayName"

.field public static final EMAIL_ADDRESS:Ljava/lang/String; = "EmailAddress"

.field public static final EMAIL_ID:Ljava/lang/String; = "Email_Id"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final VIPLIST:Ljava/lang/String; = "VIPLIST"
