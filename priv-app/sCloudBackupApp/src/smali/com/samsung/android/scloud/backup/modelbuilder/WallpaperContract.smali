.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract;
.super Ljava/lang/Object;
.source "WallpaperContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;
    }
.end annotation


# static fields
.field public static final HOMESCREEN_KEY:Ljava/lang/String; = "HOMESCREEN"

.field public static final HOMESCREEN_NAME:Ljava/lang/String; = "Home_Screen_WallPaper"

.field public static final HOMESCREEN_WALLPAPER:Ljava/lang/String; = "wallpaper"

.field public static final HOMESCREEN_WALLPAPER_PATH:Ljava/lang/String; = "/data/system/users/0"

.field public static final ID_HOMESCREEN_WALLPAPER:I = 0x1

.field public static final ID_LOCKSCREEN_WALLPAPER:I = 0x2

.field public static final LOCKSCREEN_WALLPAPER:Ljava/lang/String; = "lockscreen_wallpaper"

.field public static final LOCKSCREEN_WALLPAPER_PATH_CHOOSER:Ljava/lang/String; = "/data/data/com.sec.android.app.wallpaperchooser/"

.field public static final LOCKSCREEN_WALLPAPER_PATH_GALLERY:Ljava/lang/String; = "/data/data/com.sec.android.gallery3d/"

.field public static final LOCKSCREEN_WALLPAPER_PATH_KEYGUARD:Ljava/lang/String; = "/data/data/com.samsung.android.keyguardwallpaperupdator/"

.field public static final LOCKSCREEN_WALLPAPER_PATH_TRAVEL:Ljava/lang/String; = "/data/data/com.samsung.android.service.travel/files/images/"

.field public static final LOCKSCREEN_WALLPAPER_RIPPLE:Ljava/lang/String; = "lockscreen_wallpaper_ripple"

.field public static final PREV_FILE_POSTFIX:Ljava/lang/String; = "_prev"

.field public static final SERVER_PATH:Ljava/lang/String; = "/mnt/sdcard/Wallpaper/"

.field public static final oemUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    sget-object v0, Landroid/provider/Settings$System;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract;->oemUri:Landroid/net/Uri;

    return-void
.end method
