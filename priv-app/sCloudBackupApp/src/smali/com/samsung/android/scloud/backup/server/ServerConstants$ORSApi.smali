.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants$ORSApi;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/server/ServerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ORSApi"
.end annotation


# static fields
.field public static final DELETE_DIRECTORY:Ljava/lang/String; = "rmdir"

.field public static final DELETE_FILE:Ljava/lang/String; = "rm"

.field public static final DOWNLOAD:Ljava/lang/String; = "download"

.field public static final LIST:Ljava/lang/String; = "ls"

.field public static final ORS_API:Ljava/lang/String; = "/ors/v2/"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TX_CANCEL:Ljava/lang/String; = "tx/cancel/"

.field public static final TX_DELETE_DIRECTORY:Ljava/lang/String; = "tx/rmdir/"

.field public static final TX_DELETE_FILE:Ljava/lang/String; = "tx/rm/"

.field public static final TX_END:Ljava/lang/String; = "tx/end/"

.field public static final TX_SET_TAG:Ljava/lang/String; = "tx/chdir/"

.field public static final TX_START:Ljava/lang/String; = "tx/start"

.field public static final TX_UPLOAD:Ljava/lang/String; = "tx/upload/"

.field public static final UPLOAD:Ljava/lang/String; = "upload"
