.class Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;
.super Ljava/lang/Object;
.source "BackupAndRestoreMain.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->processRestoreReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V
    .locals 0

    .prologue
    .line 687
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 689
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # getter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$000(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # getter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$300(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$202(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;Ljava/util/List;)Ljava/util/List;

    .line 691
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # getter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$200(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # getter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$200(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 692
    :cond_0
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "onCreate: No Restore List Found in the DB"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 705
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 706
    return-void
.end method
