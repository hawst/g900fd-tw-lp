.class final Lcom/samsung/android/scloud/backup/core/BNRTask$1;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field final synthetic val$model:Lcom/samsung/android/scloud/backup/model/IModel;

.field final synthetic val$serverKeys:Ljava/util/List;

.field final synthetic val$startKey:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$ctid:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$startKey:[Ljava/lang/String;

    iput-object p5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$serverKeys:Ljava/util/List;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 15
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 76
    iget-object v10, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "BNRTask-"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v13}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "restoreKeys Finished - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$ctid:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v10, v11, v12, v13}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v10, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$startKey:[Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "nextKey"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    .line 78
    if-eqz p3, :cond_0

    .line 80
    iget-object v10, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    instance-of v3, v10, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    .line 82
    .local v3, "hasFile":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-ge v4, v10, :cond_0

    .line 83
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    .line 85
    .local v5, "item":Lorg/json/JSONObject;
    if-nez v5, :cond_1

    .line 101
    .end local v3    # "hasFile":Z
    .end local v4    # "i":I
    .end local v5    # "item":Lorg/json/JSONObject;
    :cond_0
    return-void

    .line 87
    .restart local v3    # "hasFile":Z
    .restart local v4    # "i":I
    .restart local v5    # "item":Lorg/json/JSONObject;
    :cond_1
    const-string v10, "key"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 88
    .local v6, "key":Ljava/lang/String;
    const-string v10, "deleted"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 89
    .local v2, "deleted":Z
    const-string v10, "timestamp"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 90
    .local v8, "timeStamp":J
    if-nez v2, :cond_3

    .line 92
    if-eqz v3, :cond_2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "attachment_file_info_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v11}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 93
    :cond_2
    new-instance v7, Lcom/samsung/android/scloud/backup/data/BNRItem;

    invoke-direct {v7, v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 94
    .local v7, "serverItem":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setTimeStamp(J)V

    .line 95
    iget-object v10, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$1;->val$serverKeys:Ljava/util/List;

    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    .end local v7    # "serverItem":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method
