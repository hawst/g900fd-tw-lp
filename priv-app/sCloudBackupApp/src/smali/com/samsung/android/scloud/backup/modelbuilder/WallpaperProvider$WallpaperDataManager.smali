.class public Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;
.super Ljava/lang/Object;
.source "WallpaperProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WallpaperDataManager"
.end annotation


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager; = null

.field private static final TAG:Ljava/lang/String; = "WallpaperDataManager"


# instance fields
.field private mBackupMeta:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    const-string v0, "WallpaperDataManager"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    .line 157
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->INSTANCE:Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->INSTANCE:Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    .line 151
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->INSTANCE:Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 224
    const-string v0, "WallpaperDataManager"

    const-string v1, "clear !!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 227
    return-void
.end method

.method public commit()V
    .locals 4

    .prologue
    .line 214
    const-string v1, "WallpaperDataManager"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "commit !!  wallpaper : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "WALLPAPER_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", lockscreen : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "LOCKSCREEN_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", ripple : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "LOCKSCREEN_RIPPLE_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WALLPAPER_KEY_TIMESTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 216
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "WALLPAPER_KEY_CHECKSUM"

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "WALLPAPER_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 217
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LOCKSCREEN_KEY_TIMESTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 218
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "LOCKSCREEN_KEY_CHECKSUM"

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "LOCKSCREEN_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 219
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LOCKSCREEN_RIPPLE_KEY_TIMESTAMP"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 220
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "LOCKSCREEN_RIPPLE_KEY_CHECKSUM"

    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v3, "LOCKSCREEN_RIPPLE_KEY_"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 221
    return-void
.end method

.method public getLockScreenChecksum()Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "LOCKSCREEN_KEY_CHECKSUM"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLockScreenRippleChecksum()Ljava/lang/String;
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "LOCKSCREEN_RIPPLE_KEY_CHECKSUM"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLockScreenRippleTimestamp()J
    .locals 2

    .prologue
    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLockScreenTimestamp()J
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public getWallpaperChecksum()Ljava/lang/String;
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "WALLPAPER_KEY_CHECKSUM"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWallpaperTimestamp()J
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public setLockScreenChecksum(Ljava/lang/String;)V
    .locals 2
    .param p1, "checksum"    # Ljava/lang/String;

    .prologue
    .line 197
    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v1, "LOCKSCREEN_KEY_"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    return-void
.end method

.method public setLockScreenRippleChecksum(Ljava/lang/String;)V
    .locals 2
    .param p1, "checksum"    # Ljava/lang/String;

    .prologue
    .line 200
    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v1, "LOCKSCREEN_RIPPLE_KEY_"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    return-void
.end method

.method public setWallpaperChecksum(Ljava/lang/String;)V
    .locals 2
    .param p1, "checksum"    # Ljava/lang/String;

    .prologue
    .line 194
    # getter for: Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->CHECKSUM_MAP:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider;->access$000()Ljava/util/Map;

    move-result-object v0

    const-string v1, "WALLPAPER_KEY_"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    return-void
.end method
