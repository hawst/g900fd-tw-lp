.class Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 647
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-boolean v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-nez v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 651
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 652
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 653
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 654
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 656
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iput-boolean v1, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->longFlag:Z

    .line 657
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iput-boolean v2, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    .line 658
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->invalidateOptionsMenu()V

    .line 659
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 661
    return v2
.end method
