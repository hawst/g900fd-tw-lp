.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;
.super Ljava/lang/Object;
.source "SetupWizardRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showSelectDeviceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0

    .prologue
    .line 510
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 512
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 513
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v3

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$502(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;I)I

    .line 514
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceIndex(I)V

    .line 516
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v4

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$702(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 518
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$700(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v3

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$902(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 519
    const-string v2, "SetupWizardRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[processRestoreReady] mBackupDetailsMap : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initView()V
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1100(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    .line 524
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1202(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    .line 525
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 526
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 528
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method
