.class public Lcom/samsung/android/scloud/backup/common/BackupConstants;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$LastOperationStatus;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$UIStatus;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$EmailVerification;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$NOTIFICATION;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$INTENT;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$DIALOG;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$ResultCode;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$Status;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$ServiceType;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$Action;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$KEY;,
        Lcom/samsung/android/scloud/backup/common/BackupConstants$Activity;
    }
.end annotation


# static fields
.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static final AUTOBACKUP_INTERVAL:J = 0x5265c00L

.field public static final AUTOBACKUP_INTERVAL_DEBUG:J = 0x2bf20L

.field public static final BACKUP_ATTACHEMANT_INFO:Ljava/lang/String; = "attachment_file_info_"

.field public static final DEFAULT_ID:I = 0x0

.field public static final DEFAULT_NUM_OF_SIM_SLOTS:I = 0x1

.field public static final DIALOG_TYPE:Ljava/lang/String; = "Dialog_Type"

.field public static final DUAL_SIM_MODEL:I = 0x2

.field public static final MAX_PROGRESS:I = 0x64

.field public static final MAX_RETRY_COUNT:I = 0x3

.field public static final MODE_ALLOWED:I = 0x0

.field public static final NOTIFICATION_ID:I = 0xd

.field public static final NOTIFICATION_TYPE:Ljava/lang/String; = "Notification Type"

.field public static final OP_WRITE_SMS:I = 0xf

.field public static final PROPERTY_ACTIVE_SLOT_ID_DOUBLE:Ljava/lang/String; = "persist.radio.calldefault.simid"

.field public static final PROPERTY_ACTIVE_SLOT_ID_SINGLE:Ljava/lang/String; = "ril.MSIMM"

.field public static final PROPERTY_SIM_SLOT_COUNT:Ljava/lang/String; = "ro.multisim.simslotcount"

.field public static final RETRY_INTERVAL:J = 0x7d0L

.field public static final SIM_SLOT_1:I = 0x0

.field public static final SIM_SLOT_2:I = 0x1

.field public static final SINGLE_SIM_MODEL:I = 0x1

.field public static final TIME_TO_RESTART_AUTOBACKUP:J = 0x36ee80L

.field public static final TIME_TO_RESTART_AUTOBACKUP_DEBUG:J = 0x1d4c0L

.field public static final TIME_TO_START_AUTOBACKUP:J = 0x36ee80L

.field public static final TIME_TO_START_AUTOBACKUP_DEBUG:J = 0xea60L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    return-void
.end method

.method public static getRCode(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 189
    rem-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static getServiceType(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 181
    const v0, 0xf4240

    div-int v0, p0, v0

    return v0
.end method

.method public static getStatus(I)I
    .locals 1
    .param p0, "msgCode"    # I

    .prologue
    .line 185
    const v0, 0xf4240

    rem-int v0, p0, v0

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public static makeMassageCode(III)I
    .locals 2
    .param p0, "serviceType"    # I
    .param p1, "status"    # I
    .param p2, "rCode"    # I

    .prologue
    .line 175
    const v0, 0xf4240

    mul-int/2addr v0, p0

    mul-int/lit16 v1, p1, 0x3e8

    add-int/2addr v0, v1

    mul-int/lit8 v1, p2, 0x1

    add-int/2addr v0, v1

    return v0
.end method
