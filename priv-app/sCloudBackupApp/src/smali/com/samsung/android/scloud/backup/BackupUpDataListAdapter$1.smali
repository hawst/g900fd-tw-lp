.class Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;
.super Ljava/lang/Object;
.source "BackupUpDataListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

.field final synthetic val$holder:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;ILcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    iput p2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->val$position:I

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->access$100(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->access$000(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Ljava/util/List;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->val$position:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->notifyDataSetChanged()V

    .line 119
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
