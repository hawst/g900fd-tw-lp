.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/server/ServerConstants$API;,
        Lcom/samsung/android/scloud/backup/server/ServerConstants$Response;,
        Lcom/samsung/android/scloud/backup/server/ServerConstants$Type;,
        Lcom/samsung/android/scloud/backup/server/ServerConstants$STATUS;,
        Lcom/samsung/android/scloud/backup/server/ServerConstants$ORSApi;
    }
.end annotation


# static fields
.field public static final ACCESS_TOKEN_PARM:Ljava/lang/String; = "access_token"

.field public static final BACKUP_CLEAR:Ljava/lang/String; = "/bnr/?action=clear&"

.field public static final BACKUP_CLEARALL:Ljava/lang/String; = "/bnr/?action=clearall&"

.field public static final BACKUP_CLEAR_COMMIT:Ljava/lang/String; = "/bnr/?action=clearcommit&"

.field public static final BACKUP_COMMIT:Ljava/lang/String; = "/bnr/?action=commit&"

.field public static final BACKUP_DELETE:Ljava/lang/String; = "/bnr/?action=delete&"

.field public static final BACKUP_READY:Ljava/lang/String; = "/bnr/backupready?"

.field public static final BACKUP_RELEASE_CLEARED:Ljava/lang/String; = "/bnr/?action=releasecleared&"

.field public static final BACKUP_ROLLBACK:Ljava/lang/String; = "/bnr/?action=rollback&"

.field public static final BACKUP_SET:Ljava/lang/String; = "/bnr/?action=set&"

.field public static final BAD_ACCESS_TOKEN:I = 0x4a40

.field public static final BASE_URL:Ljava/lang/String; = "/user/quota"

.field public static final BNR_CID:Ljava/lang/String; = "E2PW6NSgIO"

.field public static final BNR_SERVER_MIGRATION:I = 0x4e27

.field public static final BNR_SERVER_NOT_READY:I = 0x4e26

.field public static final BOUNDARY:Ljava/lang/String; = "AaB03x"

.field public static final CDID:Ljava/lang/String; = "cdid"

.field public static final CID_PARM:Ljava/lang/String; = "cid"

.field public static final CLIENT_DID_PARM:Ljava/lang/String; = "client_did"

.field public static final CLOUD_END:Ljava/lang/String; = "/cloud/end?"

.field public static final CLOUD_PREPARE:Ljava/lang/String; = "/cloud/prepare?"

.field public static final CLOUD_START:Ljava/lang/String; = "/cloud/start?"

.field public static final CONTENT_TYPE_URI:Ljava/lang/String; = "Application/JSON;charset=UTF-8"

.field public static final COUNT_PARM:Ljava/lang/String; = "count"

.field public static final CREATE_FOLDER:Ljava/lang/String; = "?action=create"

.field public static final CTID:Ljava/lang/String; = "ctid"

.field public static final CTID_PARM:Ljava/lang/String; = "ctid"

.field public static final DELETE_BNR_ACTION:Ljava/lang/String; = "/bnr/?action="

.field public static final DELETE_FILE:Ljava/lang/String; = "?action=delete"

.field public static final DEVICE_ID_PARM:Ljava/lang/String; = "did"

.field public static final DID:Ljava/lang/String; = "did"

.field public static final DOWNLOAD_FILE:Ljava/lang/String; = "?action=download"

.field public static final FILE_NAME:Ljava/lang/String; = "&file_name="

.field public static final FILE_TIMESTAMP:Ljava/lang/String; = "&timestamp="

.field public static final FOLDER_NAME:Ljava/lang/String; = "&folder_name="

.field public static final GETMETADATA:Ljava/lang/String; = "?action=get"

.field public static final GET_DETAILS:Ljava/lang/String; = "/bnr/details?"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final KVS_SERVER_STORAGE_FULL:I = 0x4e23

.field public static final METADATA:Ljava/lang/String; = "metadata"

.field public static final MODIFIED_AFTER_PARM:Ljava/lang/String; = "modified_after"

.field public static final ORS:Ljava/lang/String; = "/ors"

.field public static final ORS_SERVER_STORAGE_FULL:I = 0x98c7

.field public static final RESTORE_ITEM:Ljava/lang/String; = "/bnr/restoreitem?"

.field public static final RESTORE_KEYS:Ljava/lang/String; = "/bnr/restorekeys?"

.field public static final RESTORE_READY:Ljava/lang/String; = "/bnr/restoreready?"

.field public static final RESULT:Ljava/lang/String; = "result"

.field public static final RESULT_ERROR:Ljava/lang/String; = "e"

.field public static final RESULT_RCODE:Ljava/lang/String; = "r"

.field public static final RESULT_RCODE_SUC:Ljava/lang/String; = "0"

.field public static final REVISION_PARM:Ljava/lang/String; = "revision"

.field public static final START_PARM:Ljava/lang/String; = "start"

.field public static final TAG_PARM:Ljava/lang/String; = "tag"

.field public static final TRIGGER:Ljava/lang/String; = "trigger"

.field public static final TRIGGER_BACKUP_AUTO:Ljava/lang/String; = "backup_auto"

.field public static final TRIGGER_BACKUP_MANUAL:Ljava/lang/String; = "backup_manual"

.field public static final TRIGGER_CLEAR:Ljava/lang/String; = "clear"

.field public static final TRIGGER_RESTORE:Ljava/lang/String; = "restore"

.field public static final TX_COUNT_PARM:Ljava/lang/String; = "tx_count"

.field public static final TX_KEY_PARM:Ljava/lang/String; = "tx_key"

.field public static final TX_SEQ_PARM:Ljava/lang/String; = "tx_seq"

.field public static final UPLOAD_FILE:Ljava/lang/String; = "?action=upload"

.field public static final USAGE_BY_USER:Ljava/lang/String; = "?view=user&"

.field public static final USER_ID_PARM:Ljava/lang/String; = "uid"
