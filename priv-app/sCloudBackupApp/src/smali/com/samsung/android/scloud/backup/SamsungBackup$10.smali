.class Lcom/samsung/android/scloud/backup/SamsungBackup$10;
.super Ljava/lang/Object;
.source "SamsungBackup.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 530
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->val$key:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    .line 533
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->val$key:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$900(Lcom/samsung/android/scloud/backup/SamsungBackup;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->val$key:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 534
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$900(Lcom/samsung/android/scloud/backup/SamsungBackup;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$10;->val$key:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->buttonStatus(Z)V
    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1000(Lcom/samsung/android/scloud/backup/SamsungBackup;Z)V

    .line 535
    return v4
.end method
