.class public Lcom/samsung/android/scloud/backup/server/ORSServiceManager;
.super Ljava/lang/Object;
.source "ORSServiceManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ORSServiceManager"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p3, "serverPath"    # Ljava/lang/String;
    .param p4, "revision"    # I
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 212
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFile!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 217
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 218
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 222
    :cond_0
    const-string v2, "/ors/v2/rm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    if-lez p4, :cond_1

    .line 227
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 228
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p5, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 230
    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 231
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p6}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 233
    return-void
.end method

.method public static deleteFileInTx(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "serverPath"    # Ljava/lang/String;
    .param p6, "revision"    # I
    .param p7, "ctid"    # Ljava/lang/String;
    .param p8, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 404
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFile!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 409
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 410
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 414
    :cond_0
    const-string v2, "/ors/v2/tx/rm/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    if-lez p6, :cond_1

    .line 420
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 421
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p7, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 423
    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 424
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p8}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 426
    return-void
.end method

.method public static deleteFolder(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p3, "folderPath"    # Ljava/lang/String;
    .param p4, "revision"    # I
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 180
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFolder!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 186
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 190
    :cond_0
    const-string v2, "/ors/v2/rmdir"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    if-lez p4, :cond_1

    .line 195
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 196
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p5, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 198
    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 199
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p6}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 202
    return-void
.end method

.method public static deleteFolderInTx(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "folderPath"    # Ljava/lang/String;
    .param p6, "revision"    # I
    .param p7, "ctid"    # Ljava/lang/String;
    .param p8, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 369
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFolder!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 374
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 375
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 379
    :cond_0
    const-string v2, "/ors/v2/tx/rmdir/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    if-lez p6, :cond_1

    .line 385
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 386
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p7, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 388
    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 389
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p8}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 392
    return-void
.end method

.method public static downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p3, "localPath"    # Ljava/lang/String;
    .param p4, "serverPath"    # Ljava/lang/String;
    .param p5, "revision"    # I
    .param p6, "ctid"    # Ljava/lang/String;
    .param p7, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 151
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", from : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-> to : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 157
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 161
    :cond_0
    const-string v2, "/ors/v2/download"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    if-lez p5, :cond_1

    .line 166
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 167
    :cond_1
    const-string v2, "ctid"

    invoke-static {v1, v2, p6, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 169
    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 170
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p3, p7}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;)V

    .line 171
    return-void
.end method

.method public static getTimestamp(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 40
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "listDirectory!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 45
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 46
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 50
    :cond_0
    const-string v2, "/ors/v2/timestamp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v3}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 54
    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 55
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 56
    return-void
.end method

.method public static listDirectory(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;IJLcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "directoryPath"    # Ljava/lang/String;
    .param p5, "revision"    # I
    .param p6, "startKey"    # Ljava/lang/String;
    .param p7, "count"    # I
    .param p8, "modifiedAfter"    # J
    .param p10, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 62
    const-string v4, "ORSServiceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "listDirectory!!!!!!!!!! - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 67
    .local v3, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 68
    const-string v4, "ORSServiceManager"

    const-string v5, "There is NO Base URL."

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v5, 0x131

    invoke-direct {v4, v5}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v4

    .line 72
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/ors/v2/ls"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    const-wide/16 v4, 0x0

    cmp-long v4, p8, v4

    if-lez v4, :cond_1

    .line 75
    const-string v4, "modified_after"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, p8

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 76
    :cond_1
    if-lez p5, :cond_2

    .line 77
    const-string v4, "revision"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 78
    :cond_2
    const-string v4, "ctid"

    const/4 v5, 0x0

    invoke-static {v3, v4, p2, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 80
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    .line 81
    .local v2, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p10

    invoke-static {v2, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 82
    return-void
.end method

.method public static transactionCancel(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "cid"    # Ljava/lang/String;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "txKey"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 288
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionCancel!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 293
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 294
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 298
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/ors/v2/tx/cancel/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p2}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p3, v3}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 302
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->delete(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 303
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 305
    return-void
.end method

.method public static transactionEnd(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txCnt"    # I
    .param p4, "ctid"    # Ljava/lang/String;
    .param p5, "cid"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 264
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionEnd!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 269
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 270
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 274
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/ors/v2/tx/end/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 275
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p5}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    const-string v2, "tx_count"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 277
    const-string v2, "ctid"

    invoke-static {v1, v2, p4, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 279
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p5, v2}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 280
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p6}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 282
    return-void
.end method

.method public static transactionStart(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "cid"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 238
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transactionStart!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 243
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 244
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 248
    :cond_0
    const-string v2, "/ors/v2/tx/start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1, p3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string v2, "ctid"

    const/4 v3, 0x0

    invoke-static {v1, v2, p2, v3}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 253
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 255
    return-void
.end method

.method public static updateTagInTx(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "serverPath"    # Ljava/lang/String;
    .param p6, "tag"    # Ljava/lang/String;
    .param p7, "revision"    # I
    .param p8, "ctid"    # Ljava/lang/String;
    .param p9, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    const/4 v5, 0x0

    .line 439
    const-string v2, "ORSServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTimestamp!!!!!!!!!! - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", to : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", tag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 444
    .local v1, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 445
    const-string v2, "ORSServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 449
    :cond_0
    const-string v2, "/ors/v2/tx/chdir/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    if-lez p7, :cond_1

    .line 455
    const-string v2, "revision"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 456
    :cond_1
    const-string v2, "tag"

    invoke-static {v1, v2, p6, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 457
    const-string v2, "ctid"

    invoke-static {v1, v2, p8, v5}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 459
    invoke-interface {p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 460
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p9}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 462
    return-void
.end method

.method public static uploadFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p3, "localPath"    # Ljava/lang/String;
    .param p4, "serverPath"    # Ljava/lang/String;
    .param p5, "tag"    # Ljava/lang/String;
    .param p6, "revision"    # I
    .param p7, "ctid"    # Ljava/lang/String;
    .param p8, "progressListener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;
    .param p9, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 95
    const-string v3, "ORSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "uploadFile!!!!!!!!!! - "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p7

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", from : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "-> to : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v13, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 100
    .local v13, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 101
    const-string v3, "ORSServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 105
    :cond_0
    const-string v3, "/ors/v2/upload"

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    move-object/from16 v0, p4

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    if-lez p6, :cond_1

    .line 110
    const-string v3, "revision"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p6

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {v13, v3, v4, v6}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 111
    :cond_1
    if-eqz p5, :cond_2

    .line 112
    const-string v3, "tag"

    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-static {v13, v3, v0, v4}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 113
    :cond_2
    const-string v3, "ctid"

    const/4 v4, 0x0

    move-object/from16 v0, p7

    invoke-static {v13, v3, v0, v4}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 116
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 118
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 119
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File not exists : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 121
    :cond_3
    const/4 v11, 0x0

    .line 123
    .local v11, "stream":Ljava/io/InputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    .end local v11    # "stream":Ljava/io/InputStream;
    .local v12, "stream":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object/from16 v0, p8

    invoke-direct {v5, v12, v6, v7, v0}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;-><init>(Ljava/io/InputStream;JLcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    .line 125
    .local v5, "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    const-string v3, "application/octet-stream"

    invoke-virtual {v5, v3}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 126
    invoke-interface/range {p2 .. p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/InputStreamEntity;JLjava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 127
    .local v10, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p9

    invoke-static {v10, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 131
    if-eqz v12, :cond_4

    .line 133
    :try_start_2
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 139
    :cond_4
    return-void

    .line 134
    :catch_0
    move-exception v2

    .line 135
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    invoke-direct {v3, v4, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3

    .line 128
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    .end local v10    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    :catch_1
    move-exception v2

    .line 129
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_0
    :try_start_3
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    invoke-direct {v3, v4, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 131
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    :goto_1
    if-eqz v11, :cond_5

    .line 133
    :try_start_4
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 135
    :cond_5
    throw v3

    .line 134
    :catch_2
    move-exception v2

    .line 135
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    invoke-direct {v3, v4, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3

    .line 131
    .end local v2    # "e":Ljava/io/IOException;
    .end local v11    # "stream":Ljava/io/InputStream;
    .restart local v12    # "stream":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v11, v12

    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    goto :goto_1

    .line 128
    .end local v11    # "stream":Ljava/io/InputStream;
    .restart local v12    # "stream":Ljava/io/InputStream;
    :catch_3
    move-exception v2

    move-object v11, v12

    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public static uploadFileInTx(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;ILcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "txKey"    # Ljava/lang/String;
    .param p3, "txSeq"    # I
    .param p4, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "localPath"    # Ljava/lang/String;
    .param p6, "serverPath"    # Ljava/lang/String;
    .param p7, "tag"    # Ljava/lang/String;
    .param p8, "revision"    # I
    .param p9, "ctid"    # Ljava/lang/String;
    .param p10, "progressListener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;
    .param p11, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;

    .prologue
    .line 321
    const-string v3, "ORSServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "uploadFile!!!!!!!!!! - "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p9

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", from : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "-> to : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v12, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 326
    .local v12, "url":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 327
    const-string v3, "ORSServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 331
    :cond_0
    const-string v3, "/ors/v2/tx/upload/"

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    move-object/from16 v0, p6

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p4 .. p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    if-lez p8, :cond_1

    .line 337
    const-string v3, "revision"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p8

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {v12, v3, v4, v6}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 338
    :cond_1
    if-eqz p7, :cond_2

    .line 339
    const-string v3, "tag"

    const/4 v4, 0x0

    move-object/from16 v0, p7

    invoke-static {v12, v3, v0, v4}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 340
    :cond_2
    const-string v3, "ctid"

    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-static {v12, v3, v0, v4}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    .line 343
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p5

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 345
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 346
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File not exists : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v3

    .line 349
    :cond_3
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 350
    .local v11, "stream":Ljava/io/InputStream;
    new-instance v5, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object/from16 v0, p10

    invoke-direct {v5, v11, v6, v7, v0}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;-><init>(Ljava/io/InputStream;JLcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    .line 351
    .local v5, "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    const-string v3, "application/octet-stream"

    invoke-virtual {v5, v3}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 352
    invoke-interface/range {p4 .. p4}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->put(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/InputStreamEntity;JLjava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 353
    .local v10, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p11

    invoke-static {v10, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    return-void

    .line 354
    .end local v5    # "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    .end local v10    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v11    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v2

    .line 355
    .local v2, "e":Ljava/io/FileNotFoundException;
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x138

    invoke-direct {v3, v4, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3
.end method
