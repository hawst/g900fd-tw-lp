.class public Lcom/samsung/android/scloud/backup/model/Mms;
.super Ljava/lang/Object;
.source "Mms.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/model/IModel;


# static fields
.field private static final OEM_AUTHORITY:Ljava/lang/String; = "mms"

.field private static final OEM_CONTENT_URI:Landroid/net/Uri;

.field private static final SOURCE_KEY:Ljava/lang/String; = "MMS"

.field private static final SOURCE_RES_ID:I = 0x7f070004

.field public static final TARGET_ADDR_CONTENT_URI:Landroid/net/Uri;

.field public static final TARGET_PART_CONTENT_URI:Landroid/net/Uri;

.field public static final TARGET_PENDING_CONTENT_URI:Landroid/net/Uri;

.field public static final THREAD_ID_URI:Landroid/net/Uri;

.field public static final m_address_projection:[Ljava/lang/String;

.field public static final m_part_projection:[Ljava/lang/String;

.field public static final m_pending_projection:[Ljava/lang/String;


# instance fields
.field private cid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    const-string v0, "content://mms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->OEM_CONTENT_URI:Landroid/net/Uri;

    .line 46
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->OEM_CONTENT_URI:Landroid/net/Uri;

    const-string v1, "addr"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->TARGET_ADDR_CONTENT_URI:Landroid/net/Uri;

    .line 49
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->OEM_CONTENT_URI:Landroid/net/Uri;

    const-string v1, "part"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->TARGET_PART_CONTENT_URI:Landroid/net/Uri;

    .line 53
    const-string v0, "content://mms-sms/threadID"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->THREAD_ID_URI:Landroid/net/Uri;

    .line 56
    const-string v0, "content://mms-sms/pending"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->TARGET_PENDING_CONTENT_URI:Landroid/net/Uri;

    .line 59
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "msg_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "charset"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->m_address_projection:[Ljava/lang/String;

    .line 63
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "mid"

    aput-object v1, v0, v4

    const-string v1, "chset"

    aput-object v1, v0, v5

    const-string v1, "cid"

    aput-object v1, v0, v6

    const-string v1, "cl"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ct"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "seq"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "text"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->m_part_projection:[Ljava/lang/String;

    .line 78
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "proto_type"

    aput-object v1, v0, v4

    const-string v1, "msg_id"

    aput-object v1, v0, v5

    const-string v1, "msg_type"

    aput-object v1, v0, v6

    const-string v1, "err_code"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "err_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "retry_index"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "last_try"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "due_time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->m_pending_projection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-string v0, "I7o6E6m1Lj"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/Mms;->cid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/Mms;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACKUP_MMS_DETAILS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "detailKey":Ljava/lang/String;
    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const-string v0, "mms"

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->OEM_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getSourceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    const-string v0, "MMS"

    return-object v0
.end method

.method public getSourceResId()I
    .locals 1

    .prologue
    .line 86
    const v0, 0x7f070004

    return v0
.end method

.method public parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 124
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v0

    return-object v0
.end method
