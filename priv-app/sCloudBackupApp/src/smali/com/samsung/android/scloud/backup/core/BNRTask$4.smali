.class final Lcom/samsung/android/scloud/backup/core/BNRTask$4;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$file:Ljava/lang/String;

.field final synthetic val$fileCnt:F

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field final synthetic val$model:Lcom/samsung/android/scloud/backup/model/IModel;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;F)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$ctid:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$file:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    iput p5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$fileCnt:F

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 5
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v1, 0x4

    const-string v2, "BNRTask-"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFile Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$ctid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$file:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x65

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$4;->val$fileCnt:F

    div-float/2addr v3, v4

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 262
    return-void
.end method
