.class public final Lcom/samsung/android/scloud/backup/auth/AuthManager;
.super Ljava/lang/Object;
.source "AuthManager.java"


# static fields
.field private static final INFO_OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "AuthManager"

.field private static sAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mBaseUrlPrimary:Ljava/lang/String;

.field private mGetAPIparms:Ljava/lang/String;

.field private mPutAPIparms:Ljava/lang/String;

.field private mRegistrationID:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 40
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public static declared-synchronized create()Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .locals 2

    .prologue
    .line 65
    const-class v1, Lcom/samsung/android/scloud/backup/auth/AuthManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/samsung/android/scloud/backup/auth/AuthManager;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/auth/AuthManager;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    .line 68
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->sAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleResult(I)V
    .locals 2
    .param p1, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x131

    .line 122
    if-nez p1, :cond_1

    .line 123
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mUserId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 125
    :cond_0
    new-instance v0, Lcom/samsung/android/scloud/backup/common/BNRException;

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v0

    .line 128
    :cond_1
    new-instance v0, Lcom/samsung/android/scloud/backup/common/BNRException;

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v0

    .line 130
    :cond_2
    return-void
.end method


# virtual methods
.method public accountRemoved(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 51
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    return-object v0
.end method

.method public getGetApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGetApiParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "cid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPutApiParamsWithoutCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized updateAuthInformation(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "regId"    # Ljava/lang/String;
    .param p4, "accessToken"    # Ljava/lang/String;
    .param p5, "baseUrl"    # Ljava/lang/String;
    .param p6, "baseUrl2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    const-string v0, "AuthManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAuthInformation : STATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    if-nez p1, :cond_0

    .line 107
    iput-object p5, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 109
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 111
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "did"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 117
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->handleResult(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
