.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showBackupDetailProgressDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 972
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 975
    const-string v0, "ManualRestoreActivity"

    const-string v1, "Cancel Checkpoint [1]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestBackupDetailCancel()V

    .line 977
    const-string v0, "ManualRestoreActivity"

    const-string v1, "Cancel Checkpoint [2]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 979
    const-string v0, "ManualRestoreActivity"

    const-string v1, "Cancel Checkpoint [3]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finishThisActivity(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)V

    .line 981
    const-string v0, "ManualRestoreActivity"

    const-string v1, "Cancel Checkpoint [4]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    return-void
.end method
