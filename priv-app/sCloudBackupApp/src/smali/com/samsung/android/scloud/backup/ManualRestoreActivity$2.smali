.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->val$key:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->val$key:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->val$key:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 332
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;->val$key:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonStatus(Z)V
    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Z)V

    .line 333
    const/4 v0, 0x1

    return v0
.end method
