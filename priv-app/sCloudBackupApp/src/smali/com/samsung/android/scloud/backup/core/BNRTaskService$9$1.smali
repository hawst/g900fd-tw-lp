.class Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;->handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$source:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 276
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 279
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OPERATION_CANCEL 1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 281
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModelManager.getInstance() is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->requestCancel(Ljava/lang/String;)V

    .line 285
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OPERATION_CANCEL 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    return-void

    .line 282
    :cond_1
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 283
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModelManager.getInstance().getBNRManager(context, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;->val$source:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is NULL!!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
