.class public Lcom/samsung/android/scloud/backup/SCloudInternalReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SCloudInternalReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SCloudInternalReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private processAutoBackup(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v1, "SCloudInternalReceiver"

    const-string v2, "AutoBackup Alarm Triggered"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 33
    .local v0, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->checkAutoBackupCondition(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 34
    const-string v1, "AutoBackupReceiver"

    const-string v2, "[processAutoBackup] Start AutoBackupService!! in the sCloudBackupReceiver!"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->startAutoBackupService(Landroid/content/Context;)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string v1, "SCloudInternalReceiver"

    const-string v2, "[processAutoBackup] checkAutoBackupCondition returns false. Reset Auto Backup alarm. "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->resetAutoBackupAlarm(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private processBackupDeleted(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    .line 43
    .local v0, "mMetaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->clearLastBackupTimeMap()V

    .line 44
    const-string v1, "CALLLOGS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 45
    const-string v1, "MMS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 46
    const-string v1, "SMS"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 47
    const-string v1, "HOMESCREEN"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 48
    const-string v1, "VIPLIST"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 49
    const-string v1, "BLACKLIST"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 50
    const-string v1, "SPAM"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 51
    const-string v1, "CALLREJECT"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 53
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 16
    .local v0, "action":Ljava/lang/String;
    const-string v1, "SCloudInternalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[onReceive] action Name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    const-string v1, "com.sec.android.SecBackupApp.AutoBackup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 19
    const-string v1, "SCloudInternalReceiver"

    const-string v2, "Start AutoBackupService"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudInternalReceiver;->processAutoBackup(Landroid/content/Context;)V

    .line 29
    :goto_0
    return-void

    .line 22
    :cond_0
    const-string v1, "com.sec.android.sCloudBackup.BACKUP_DELETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23
    const-string v1, "SCloudInternalReceiver"

    const-string v2, "BACKUP_DELETED"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudInternalReceiver;->processBackupDeleted(Landroid/content/Context;)V

    goto :goto_0

    .line 27
    :cond_1
    const-string v1, "SCloudInternalReceiver"

    const-string v2, "Unknown Intent !!!"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
