.class public Lcom/samsung/android/scloud/backup/data/BNRItem;
.super Ljava/lang/Object;
.source "BNRItem.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BNRItem"


# instance fields
.field private data:Lorg/json/JSONObject;

.field private fileMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private key:Ljava/lang/String;

.field private localId:Ljava/lang/String;

.field private size:J

.field private timeStamp:J


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->key:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "size"    # J

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->key:Ljava/lang/String;

    .line 34
    iput-wide p2, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->size:J

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "localId"    # Ljava/lang/String;
    .param p3, "data"    # Lorg/json/JSONObject;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->key:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    .line 45
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->localId:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONObject;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->key:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    .line 40
    return-void
.end method

.method public static parseToBNRItem(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 1
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 121
    new-instance v0, Lcom/samsung/android/scloud/backup/data/BNRItem;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    return-object v0
.end method

.method public static parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 4
    .param p0, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 112
    new-instance v0, Lcom/samsung/android/scloud/backup/data/BNRItem;

    const-string v1, "key"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 113
    .local v0, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const-string v1, "size"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const-string v1, "size"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 115
    :cond_0
    invoke-virtual {v0, p0}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setData(Lorg/json/JSONObject;)V

    .line 117
    return-object v0
.end method


# virtual methods
.method public getAttachmentFileInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    return-object v0
.end method

.method public getData()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    return-object v0
.end method

.method public getDataWithFilter([Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 9
    .param p1, "filter"    # [Ljava/lang/String;

    .prologue
    .line 94
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 95
    .local v3, "json":Lorg/json/JSONObject;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    if-nez v6, :cond_1

    .line 96
    const/4 v3, 0x0

    .line 107
    .end local v3    # "json":Lorg/json/JSONObject;
    :cond_0
    return-object v3

    .line 97
    .restart local v3    # "json":Lorg/json/JSONObject;
    :cond_1
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 98
    .local v5, "name":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    invoke-virtual {v6, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 100
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    invoke-virtual {v6, v5}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Lorg/json/JSONException;
    const-string v6, "BNRItem"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDataWithFilter err with Name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 103
    new-instance v6, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x130

    invoke-direct {v6, v7, v1}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v6
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->localId:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->size:J

    return-wide v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->timeStamp:J

    return-wide v0
.end method

.method public hasFile()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "checksum"    # Ljava/lang/String;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->fileMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method public setData(Lorg/json/JSONObject;)V
    .locals 0
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->data:Lorg/json/JSONObject;

    .line 67
    return-void
.end method

.method public setSize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->size:J

    .line 91
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 9
    .param p1, "timeStamp"    # J

    .prologue
    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    rsub-int/lit8 v0, v1, 0xd

    .line 54
    .local v0, "len":I
    if-lez v0, :cond_0

    .line 55
    long-to-double v2, p1

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    int-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-long p1, v2

    .line 56
    :cond_0
    iput-wide p1, p0, Lcom/samsung/android/scloud/backup/data/BNRItem;->timeStamp:J

    .line 57
    return-void
.end method
