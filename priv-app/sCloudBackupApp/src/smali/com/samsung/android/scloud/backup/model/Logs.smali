.class public Lcom/samsung/android/scloud/backup/model/Logs;
.super Ljava/lang/Object;
.source "Logs.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/model/IModel;


# static fields
.field private static final OEM_AUTHORITY:Ljava/lang/String; = "logs"

.field private static final OEM_CONTENT_URI:Landroid/net/Uri;

.field private static final SOURCE_KEY:Ljava/lang/String; = "CALLLOGS"

.field private static final SOURCE_RES_ID:I = 0x7f070003

.field public static cid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "content://logs/historys"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Logs;->OEM_CONTENT_URI:Landroid/net/Uri;

    .line 40
    const-string v0, "KNszpw41I3"

    sput-object v0, Lcom/samsung/android/scloud/backup/model/Logs;->cid:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Logs;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACKUP_CALLLOGS_DETAILS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "detailKey":Ljava/lang/String;
    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "logs"

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Logs;->OEM_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getSourceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "CALLLOGS"

    return-object v0
.end method

.method public getSourceResId()I
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f070003

    return v0
.end method

.method public parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 82
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v0

    return-object v0
.end method
