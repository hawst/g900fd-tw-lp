.class public final Lcom/samsung/android/scloud/backup/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_switch_padding:I = 0x7f060000

.field public static final bottom_button_area_paddingLeft:I = 0x7f06006e

.field public static final bottom_button_area_paddingRight:I = 0x7f06006f

.field public static final bottom_button_area_paddingTop:I = 0x7f06006d

.field public static final bottom_button_text_size:I = 0x7f060001

.field public static final bottom_button_text_size_k:I = 0x7f060066

.field public static final bottom_button_text_size_m2:I = 0x7f060002

.field public static final bottom_button_text_size_t:I = 0x7f060067

.field public static final bottom_button_text_size_tabs:I = 0x7f060003

.field public static final bottom_buttons_height:I = 0x7f060004

.field public static final bottom_buttons_height_h:I = 0x7f060005

.field public static final bottom_next_btn_paddingRight:I = 0x7f060006

.field public static final cloud_autobackup_description_font_size:I = 0x7f060007

.field public static final cloud_autobackup_description_margin_left:I = 0x7f060008

.field public static final cloud_autobackup_description_margin_right:I = 0x7f060009

.field public static final cloud_autobackup_description_margin_top:I = 0x7f06000a

.field public static final cloud_autobackup_margin:I = 0x7f06000b

.field public static final cloud_autobackup_padding_bottom:I = 0x7f06000c

.field public static final cloud_autobackup_padding_top:I = 0x7f06000d

.field public static final cloud_list_margin_left:I = 0x7f06000e

.field public static final cloud_list_margin_right:I = 0x7f06000f

.field public static final cloud_restore_description_font_size:I = 0x7f060010

.field public static final cloud_restore_description_margin_top:I = 0x7f060011

.field public static final cloud_restore_margin_left:I = 0x7f060012

.field public static final cloud_restore_margin_right:I = 0x7f060013

.field public static final cloud_restore_restoreitem_height:I = 0x7f060014

.field public static final cloud_restore_restoreitem_margin_top:I = 0x7f060015

.field public static final cloud_restore_summary_font_size:I = 0x7f060016

.field public static final cloud_restore_title_font_size:I = 0x7f060017

.field public static final cloud_wizard_autobackup_checkbox_height:I = 0x7f060018

.field public static final cloud_wizard_autobackup_checkbox_margin_right:I = 0x7f060019

.field public static final cloud_wizard_autobackup_checkbox_width:I = 0x7f06001a

.field public static final cloud_wizard_autobackup_height:I = 0x7f06001b

.field public static final cloud_wizard_autobackup_margin_left:I = 0x7f06001c

.field public static final cloud_wizard_autobackup_margin_right:I = 0x7f06001d

.field public static final cloud_wizard_autobackup_margin_top:I = 0x7f06001e

.field public static final cloud_wizard_autobackup_summary_height:I = 0x7f06001f

.field public static final cloud_wizard_autobackup_summary_margin_left:I = 0x7f060020

.field public static final cloud_wizard_autobackup_summary_margin_top:I = 0x7f060021

.field public static final cloud_wizard_autobackup_summary_text_size:I = 0x7f060022

.field public static final cloud_wizard_autobackup_title_height:I = 0x7f060023

.field public static final cloud_wizard_autobackup_title_margin_left:I = 0x7f060024

.field public static final cloud_wizard_autobackup_title_margin_top:I = 0x7f060025

.field public static final cloud_wizard_autobackup_title_padding_right:I = 0x7f060026

.field public static final cloud_wizard_autobackup_title_text_size:I = 0x7f060027

.field public static final cloud_wizard_back_btn_height:I = 0x7f060028

.field public static final cloud_wizard_back_btn_text_size:I = 0x7f060029

.field public static final cloud_wizard_back_btn_width:I = 0x7f06002a

.field public static final cloud_wizard_below_next_layout_margin_bottom:I = 0x7f060052

.field public static final cloud_wizard_below_next_layout_margin_top:I = 0x7f060053

.field public static final cloud_wizard_btn_area_margin_left:I = 0x7f06002b

.field public static final cloud_wizard_btn_area_margin_right:I = 0x7f06002c

.field public static final cloud_wizard_btn_area_margin_top:I = 0x7f06002d

.field public static final cloud_wizard_btn_area_padding_bottom:I = 0x7f06006c

.field public static final cloud_wizard_learnmore_text_size:I = 0x7f060068

.field public static final cloud_wizard_next_btn_arrow_height:I = 0x7f060054

.field public static final cloud_wizard_next_btn_arrow_width:I = 0x7f060055

.field public static final cloud_wizard_next_btn_height:I = 0x7f06002e

.field public static final cloud_wizard_next_btn_text_size:I = 0x7f06002f

.field public static final cloud_wizard_next_btn_width:I = 0x7f060030

.field public static final cloud_wizard_restore_checkbox_height:I = 0x7f060031

.field public static final cloud_wizard_restore_checkbox_margin_right:I = 0x7f060032

.field public static final cloud_wizard_restore_checkbox_width:I = 0x7f060033

.field public static final cloud_wizard_restore_height:I = 0x7f060034

.field public static final cloud_wizard_restore_layout_height:I = 0x7f060056

.field public static final cloud_wizard_restore_layout_height_k:I = 0x7f060063

.field public static final cloud_wizard_restore_margin_left:I = 0x7f060035

.field public static final cloud_wizard_restore_margin_right:I = 0x7f060036

.field public static final cloud_wizard_restore_margin_top:I = 0x7f060037

.field public static final cloud_wizard_restore_summary_height:I = 0x7f060038

.field public static final cloud_wizard_restore_summary_margin_left:I = 0x7f060039

.field public static final cloud_wizard_restore_summary_margin_top:I = 0x7f06003a

.field public static final cloud_wizard_restore_summary_text_size:I = 0x7f06003b

.field public static final cloud_wizard_restore_title_height:I = 0x7f06003c

.field public static final cloud_wizard_restore_title_margin_left:I = 0x7f06003d

.field public static final cloud_wizard_restore_title_margin_top:I = 0x7f06003e

.field public static final cloud_wizard_restore_title_padding_bottom_h:I = 0x7f060069

.field public static final cloud_wizard_restore_title_padding_right:I = 0x7f06003f

.field public static final cloud_wizard_restore_title_text_size:I = 0x7f060040

.field public static final cloud_wizard_summary_after_check_padding_top:I = 0x7f060057

.field public static final cloud_wizard_summary_margin_left:I = 0x7f060041

.field public static final cloud_wizard_summary_margin_right:I = 0x7f060042

.field public static final cloud_wizard_summary_margin_top:I = 0x7f060043

.field public static final cloud_wizard_summary_padding:I = 0x7f060062

.field public static final cloud_wizard_summary_padding_bottom:I = 0x7f060058

.field public static final cloud_wizard_summary_padding_bottom_k:I = 0x7f060064

.field public static final cloud_wizard_summary_padding_left:I = 0x7f060059

.field public static final cloud_wizard_summary_padding_right:I = 0x7f06005a

.field public static final cloud_wizard_summary_text_size:I = 0x7f060044

.field public static final cloud_wizard_text_size:I = 0x7f06005b

.field public static final cloud_wizard_title_padding_bottom:I = 0x7f06005c

.field public static final cloud_wizard_title_padding_bottom_h:I = 0x7f06005d

.field public static final cloud_wizard_title_padding_bottom_k:I = 0x7f060065

.field public static final cloud_wizard_title_padding_land_bottom:I = 0x7f06005e

.field public static final cloud_wizard_title_padding_land_top:I = 0x7f06005f

.field public static final cloud_wizard_title_padding_top:I = 0x7f060060

.field public static final cloud_wizard_title_padding_top_h:I = 0x7f060061

.field public static final decivelist_date_text_size:I = 0x7f060045

.field public static final decivelist_devicename_margin_bottom:I = 0x7f060046

.field public static final decivelist_devicename_margin_top:I = 0x7f060047

.field public static final decivelist_devicename_text_size:I = 0x7f060048

.field public static final decivelist_linearlayout_margin_left:I = 0x7f060049

.field public static final decivelist_radio_margin_right:I = 0x7f06004a

.field public static final learnmore_linearlayout_margin_left:I = 0x7f06004b

.field public static final learnmore_linearlayout_margin_right:I = 0x7f06004c

.field public static final main_title_font_size:I = 0x7f06004d

.field public static final main_title_font_size_k:I = 0x7f06006a

.field public static final main_title_font_size_t:I = 0x7f06006b

.field public static final next_arrow_paddingLeft:I = 0x7f06004e

.field public static final next_arrow_paddingLeft_chagal:I = 0x7f06004f

.field public static final next_arrow_paddingLeft_h:I = 0x7f060050

.field public static final setupwizard_font_v:I = 0x7f060051


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
