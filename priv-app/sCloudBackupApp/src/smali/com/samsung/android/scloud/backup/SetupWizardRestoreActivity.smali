.class public Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
.super Landroid/preference/PreferenceActivity;
.source "SetupWizardRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;
    }
.end annotation


# static fields
.field private static final SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN:I = 0x400

.field private static final SYSTEM_UI_FLAG_TRANSPARENT_BACKGROUND:I = 0x1000

.field private static final TAG:Ljava/lang/String; = "SetupWizardRestoreActivity"


# instance fields
.field private final REMOTE_BACKUP_DETAIL_NODEVICE:I

.field bSelected:Z

.field private isActivityOn:Z

.field private list:Lorg/json/JSONArray;

.field private mAd:Landroid/app/AlertDialog;

.field private mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

.field private mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

.field private mBackedupDataPreference:Landroid/preference/Preference;

.field private mBackedupDeviceListPreference:Landroid/preference/Preference;

.field private mBackupDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mBackupDetailsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mBlacklist:Landroid/preference/CheckBoxPreference;

.field private mCallReject:Landroid/preference/CheckBoxPreference;

.field private mContext:Landroid/content/Context;

.field public mDownloadRestoreProgressDialog:Landroid/app/ProgressDialog;

.field private mEmail:Landroid/preference/CheckBoxPreference;

.field private final mHandler:Landroid/os/Handler;

.field private mLog:Landroid/preference/CheckBoxPreference;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mMms:Landroid/preference/CheckBoxPreference;

.field private mRestoreDescriptionPreference:Landroid/preference/Preference;

.field private mSelectedDeviceIndex:I

.field private mSms:Landroid/preference/CheckBoxPreference;

.field private mSourceKeyPreference:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/preference/CheckBoxPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDeviceId:Ljava/lang/String;

.field private mValidSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWallpaper:Landroid/preference/CheckBoxPreference;

.field private mviplist:Landroid/preference/CheckBoxPreference;

.field sameDeviceBackupExistsFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 76
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    .line 77
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    .line 85
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 87
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 88
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 89
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;

    .line 90
    iput v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->REMOTE_BACKUP_DETAIL_NODEVICE:I

    .line 91
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->isActivityOn:Z

    .line 94
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    .line 110
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 468
    new-instance v0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$3;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mHandler:Landroid/os/Handler;

    .line 814
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lorg/json/JSONArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initView()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p1, "x1"    # Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->checkRestoreNowButton()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finishThisActivity(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    return-object p1
.end method

.method private declared-synchronized checkRestoreNowButton()V
    .locals 7

    .prologue
    const/16 v6, 0x13

    const/4 v5, 0x1

    .line 709
    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    .line 714
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 715
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    .line 717
    :cond_0
    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    if-nez v3, :cond_a

    .line 718
    const v3, 0x7f090011

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 719
    .local v0, "layout":Landroid/widget/LinearLayout;
    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 721
    const v3, 0x7f090012

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 722
    .local v2, "nextTextView":Landroid/widget/TextView;
    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 723
    const v3, 0x7f090013

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 724
    .local v1, "nextImageView":Landroid/widget/ImageView;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v3

    if-eqz v3, :cond_1

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v3, v6, :cond_1

    .line 726
    const v3, 0x7f020061

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 727
    const/16 v3, 0x66

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 797
    :goto_0
    monitor-exit p0

    return-void

    .line 729
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 731
    const v3, 0x7f020061

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 709
    .end local v0    # "layout":Landroid/widget/LinearLayout;
    .end local v1    # "nextImageView":Landroid/widget/ImageView;
    .end local v2    # "nextTextView":Landroid/widget/TextView;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 733
    .restart local v0    # "layout":Landroid/widget/LinearLayout;
    .restart local v1    # "nextImageView":Landroid/widget/ImageView;
    .restart local v2    # "nextTextView":Landroid/widget/TextView;
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 735
    :cond_3
    const v3, 0x7f020028

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 738
    :cond_4
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-ne v3, v5, :cond_5

    .line 740
    const v3, 0x7f020029

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 742
    :cond_5
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-eq v3, v5, :cond_6

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT01:Z

    if-eqz v3, :cond_7

    .line 744
    :cond_6
    const v3, 0x7f02002a

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 746
    :cond_7
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v3, v5, :cond_8

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v3, v5, :cond_9

    .line 748
    :cond_8
    const v3, 0x7f020027

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 753
    :cond_9
    const v3, 0x7f020026

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 760
    .end local v0    # "layout":Landroid/widget/LinearLayout;
    .end local v1    # "nextImageView":Landroid/widget/ImageView;
    .end local v2    # "nextTextView":Landroid/widget/TextView;
    :cond_a
    const v3, 0x7f090011

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 761
    .restart local v0    # "layout":Landroid/widget/LinearLayout;
    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->bSelected:Z

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 763
    const v3, 0x7f090012

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 764
    .restart local v2    # "nextTextView":Landroid/widget/TextView;
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 765
    const v3, 0x7f090013

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 766
    .restart local v1    # "nextImageView":Landroid/widget/ImageView;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v3

    if-eqz v3, :cond_b

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v3, v6, :cond_b

    .line 768
    const/16 v3, 0xff

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 769
    const v3, 0x7f020061

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 772
    :cond_b
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 774
    const v3, 0x7f020061

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 776
    :cond_c
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v3

    if-nez v3, :cond_d

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v3

    if-nez v3, :cond_d

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 778
    :cond_d
    const v3, 0x7f020060

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 780
    :cond_e
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-ne v3, v5, :cond_f

    .line 782
    const v3, 0x7f020022

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 784
    :cond_f
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-eq v3, v5, :cond_10

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT01:Z

    if-eqz v3, :cond_11

    .line 786
    :cond_10
    const v3, 0x7f020024

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 788
    :cond_11
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v3, v5, :cond_12

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v3, v5, :cond_13

    .line 790
    :cond_12
    const v3, 0x7f020023

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 794
    :cond_13
    const v3, 0x7f020022

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private finishThisActivity(I)V
    .locals 0
    .param p1, "endStatus"    # I

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setResult(I)V

    .line 272
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finish()V

    .line 273
    return-void
.end method

.method private initPref()V
    .locals 11

    .prologue
    const v10, 0x7f070056

    const v9, 0x7f03001d

    const v8, 0x7f03001f

    const/4 v7, 0x1

    .line 345
    const/high16 v5, 0x7f040000

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->addPreferencesFromResource(I)V

    .line 348
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-le v5, v6, :cond_3

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 350
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v5, v7, :cond_2

    .line 351
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    .line 383
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    const-string v6, "screen"

    invoke-virtual {v5, v6}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    .line 384
    .local v2, "prefscreen":Landroid/preference/PreferenceScreen;
    const-string v5, "restore_description"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mRestoreDescriptionPreference:Landroid/preference/Preference;

    .line 386
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-nez v5, :cond_0

    .line 387
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mRestoreDescriptionPreference:Landroid/preference/Preference;

    check-cast v5, Lcom/samsung/android/scloud/backup/CustomSummaryPref;

    invoke-virtual {v2, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 388
    :cond_0
    const-string v5, "backedup_device_list_preference"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    .line 391
    const-string v5, "logcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    .line 392
    const-string v5, "smscheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    .line 393
    const-string v5, "mmscheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    .line 394
    const-string v5, "wallpapercheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    .line 395
    const-string v5, "viplistcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    .line 396
    const-string v5, "blacklistcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    .line 397
    const-string v5, "spamcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    .line 398
    const-string v5, "callrejectcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    .line 401
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 402
    .local v1, "key":Ljava/lang/String;
    const-string v5, "CALLLOGS"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 403
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 353
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_2
    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 357
    :cond_3
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 358
    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 359
    :cond_4
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-ne v5, v7, :cond_5

    .line 360
    const v5, 0x7f030021

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 361
    :cond_5
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-eq v5, v7, :cond_6

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT01:Z

    if-eqz v5, :cond_7

    .line 362
    :cond_6
    const v5, 0x7f030023

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 367
    :cond_7
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v5, v7, :cond_8

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v5, v7, :cond_9

    .line 368
    :cond_8
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 369
    :cond_9
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 370
    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 371
    :cond_a
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 372
    const v5, 0x7f030020

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 373
    :cond_b
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 374
    const v5, 0x7f030022

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    .line 375
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020061

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    goto/16 :goto_0

    .line 380
    :cond_c
    const v5, 0x7f03001c

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 404
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "key":Ljava/lang/String;
    .restart local v2    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_d
    const-string v5, "SMS"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 405
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 406
    :cond_e
    const-string v5, "MMS"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 407
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 408
    :cond_f
    const-string v5, "HOMESCREEN"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 409
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 410
    :cond_10
    const-string v5, "VIPLIST"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 411
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 412
    :cond_11
    const-string v5, "BLACKLIST"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 413
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 414
    :cond_12
    const-string v5, "SPAM"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 415
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 416
    :cond_13
    const-string v5, "CALLREJECT"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 417
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 422
    .end local v1    # "key":Ljava/lang/String;
    :cond_14
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070007

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070004

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 425
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 426
    .restart local v1    # "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    new-instance v6, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;

    invoke-direct {v6, p0, v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2

    .line 436
    .end local v1    # "key":Ljava/lang/String;
    :cond_15
    const-string v5, "screen"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 437
    .local v3, "screen":Landroid/preference/PreferenceScreen;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 438
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 439
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 440
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 441
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 442
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 443
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 444
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 447
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_16

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 449
    .restart local v1    # "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    .line 450
    .local v4, "sourcePreference":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 451
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_3

    .line 454
    .end local v1    # "key":Ljava/lang/String;
    .end local v4    # "sourcePreference":Landroid/preference/CheckBoxPreference;
    :cond_16
    return-void
.end method

.method private initView()V
    .locals 6

    .prologue
    .line 298
    const-string v1, "SetupWizardRestoreActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[initView] : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setLastBackupTime()V

    .line 302
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->updatePref()V

    .line 304
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->checkRestoreNowButton()V

    .line 308
    :cond_0
    return-void

    .line 298
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private networkisAvailable(Z)Z
    .locals 8
    .param p1, "bToast"    # Z

    .prologue
    const/4 v7, 0x0

    .line 647
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v4

    .line 648
    .local v4, "wifiOnly":Z
    const/4 v0, 0x0

    .line 650
    .local v0, "bNetConn":Z
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 651
    .local v1, "connManager":Landroid/net/ConnectivityManager;
    if-eqz v4, :cond_2

    .line 652
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 653
    .local v3, "uWifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 654
    const/4 v0, 0x1

    .line 677
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v0

    .line 656
    .restart local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_1
    if-eqz p1, :cond_0

    .line 657
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    const v6, 0x7f070079

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 665
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_2
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 666
    .local v2, "mNetInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 667
    const/4 v0, 0x1

    goto :goto_0

    .line 669
    :cond_3
    if-eqz p1, :cond_0

    .line 670
    const v5, 0x7f07009b

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private setActivityTheme()V
    .locals 4

    .prologue
    const v3, 0x103012b

    const/4 v2, 0x1

    .line 682
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_2

    .line 684
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setTheme(I)V

    .line 685
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setIndicatorTransparency()V

    .line 688
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-eqz v0, :cond_1

    .line 689
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->setTransGradationMode(Landroid/content/Context;Z)V

    .line 707
    :cond_1
    :goto_0
    return-void

    .line 691
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 693
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v2, :cond_3

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v2, :cond_5

    .line 694
    :cond_3
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setTheme(I)V

    .line 698
    :goto_1
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_GOYA:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v0, v2, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-ne v0, v2, :cond_1

    .line 700
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setIndicatorTransparency()V

    goto :goto_0

    .line 696
    :cond_5
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setTheme(I)V

    goto :goto_1

    .line 704
    :cond_6
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 705
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setIndicatorTransparency()V

    goto :goto_0
.end method

.method private setIndicatorTransparency()V
    .locals 4

    .prologue
    .line 807
    const/16 v0, 0x1400

    .line 808
    .local v0, "visibility":I
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 810
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 811
    .local v1, "wmLp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x4000c00

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 812
    return-void
.end method

.method private setLastBackupTime()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 311
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 312
    .local v3, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 313
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    const v5, 0x7f070042

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 314
    .local v4, "lastTime":Ljava/lang/String;
    const/4 v2, 0x0

    .line 315
    .local v2, "isChecked":Z
    if-nez v0, :cond_1

    .line 316
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5, v3, v10, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 336
    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 338
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 319
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    const v7, 0x7f070041

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 321
    const/4 v2, 0x1

    .line 322
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v3, v6, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 324
    const-string v5, "VIPLIST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "BLACKLIST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 325
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 326
    const/4 v2, 0x0

    .line 327
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5, v3, v10, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 328
    const-string v5, "VIPLIST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 329
    const v5, 0x7f07008e

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 331
    :cond_3
    const v5, 0x7f07008f

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 340
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v2    # "isChecked":Z
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "lastTime":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private showDialogOnActivity(Landroid/app/Dialog;)V
    .locals 3
    .param p1, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 246
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->isActivityOn:Z

    if-eqz v0, :cond_0

    .line 247
    const-string v0, "SetupWizardRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[showDialogOnActivity] Acti:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dialog : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 250
    :cond_0
    return-void
.end method

.method private updatePref()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 457
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 459
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 460
    .local v2, "sourcePreference":Landroid/preference/CheckBoxPreference;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 461
    const-string v3, "VIPLIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "BLACKLIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 463
    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 466
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "sourcePreference":Landroid/preference/CheckBoxPreference;
    :cond_2
    return-void
.end method


# virtual methods
.method public declared-synchronized onBackClicked(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 800
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 801
    monitor-exit p0

    return-void

    .line 800
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x1

    .line 277
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 279
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 280
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 293
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v2, :cond_1

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v2, :cond_2

    .line 294
    :cond_1
    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setContentView(I)V

    .line 295
    :cond_2
    return-void

    .line 282
    :pswitch_0
    const-string v0, "SetupWizardRestoreActivity"

    const-string v1, "onConfigurationChanged : ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 286
    :pswitch_1
    const-string v0, "SetupWizardRestoreActivity"

    const-string v1, "onConfigurationChanged : ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 280
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 144
    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->requestWindowFeature(I)Z

    .line 145
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->setActivityTheme()V

    .line 146
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    .line 151
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 155
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    .line 157
    const-string v2, "SetupWizardRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onCreate] Acti:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initPref()V

    .line 160
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "backupDetailList"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    .line 161
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "JSONArray"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 163
    .local v1, "jsonArray":Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    new-instance v3, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$1;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 176
    const-string v2, "SetupWizardRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[onCreate] Acti:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 179
    :cond_0
    const-string v2, "SetupWizardRestoreActivity"

    const-string v3, "onCreate: No Restore List Found in the DB"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const v2, 0x7f070097

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 183
    invoke-direct {p0, v5}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finishThisActivity(I)V

    .line 202
    :goto_1
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 185
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    iput v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    .line 187
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 189
    const-string v2, "SetupWizardRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[processRestoreReady] mTargetDeviceId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 191
    const-string v2, "SetupWizardRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[processRestoreReady] mBackupDetailsMap : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 194
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 198
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initView()V

    .line 200
    new-instance v2, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$1;)V

    new-array v3, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 267
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 268
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 236
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 241
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 238
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finish()V

    goto :goto_0

    .line 236
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 254
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 255
    const-string v1, "SetupWizardRestoreActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[onPause]  Acti:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mDownloadRestoreProgressDialog : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mDownloadRestoreProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mDownloadRestoreProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->isActivityOn:Z

    .line 258
    return-void

    .line 255
    :cond_0
    const-string v0, "NULL"

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 206
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 207
    const-string v5, "SetupWizardRestoreActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[onResume] Acti:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", mDownloadRestoreProgressDialog : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mDownloadRestoreProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mDownloadRestoreProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->isActivityOn:Z

    .line 210
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 211
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 213
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    .line 214
    const/4 v1, 0x0

    .line 215
    .local v1, "devicePos":I
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 216
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 217
    iput v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    .line 219
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 220
    goto :goto_1

    .line 207
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v1    # "devicePos":I
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    const-string v4, "NULL"

    goto :goto_0

    .line 222
    .restart local v1    # "devicePos":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 224
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 226
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "Dialog_Type"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 227
    .local v2, "dialog":I
    if-eq v2, v7, :cond_3

    .line 228
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showDialog(I)V

    .line 232
    .end local v1    # "devicePos":I
    .end local v2    # "dialog":I
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method public declared-synchronized onSetupWizardRestoreClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    :cond_0
    const-string v0, "No Restore Item"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 629
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finishThisActivity(I)V

    .line 632
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->networkisAvailable(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 633
    const-string v0, "SetupWizardRestoreActivity"

    const-string v1, "Restore to be haapened in background"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceId(Ljava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceIndex(I)V

    .line 637
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finishThisActivity(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 644
    :goto_0
    monitor-exit p0

    return-void

    .line 639
    :cond_2
    const v0, 0x7f07009b

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onSkipClicked(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 803
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->finishThisActivity(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 804
    monitor-exit p0

    return-void

    .line 803
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 262
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 263
    return-void
.end method

.method public declared-synchronized showSelectDataDialog()V
    .locals 9

    .prologue
    .line 567
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->isShowing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_0

    .line 620
    :goto_0
    monitor-exit p0

    return-void

    .line 570
    :cond_0
    :try_start_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 571
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v7, 0x7f07001a

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 573
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 574
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 575
    :cond_1
    const v7, 0x7f070023

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 567
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 581
    .restart local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :cond_2
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v4, "mRestoreSourceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    .line 583
    .local v6, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 584
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 585
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 588
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    if-nez v7, :cond_4

    .line 589
    new-instance v7, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-direct {v7, p0, v4, v8}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/samsung/android/scloud/backup/common/MetaManager;)V

    iput-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    .line 598
    :goto_2
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 599
    const v7, 0x104000a

    new-instance v8, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;

    invoke-direct {v8, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 611
    const/high16 v7, 0x1040000

    new-instance v8, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$8;

    invoke-direct {v8, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$8;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 617
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 619
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-direct {p0, v7}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showDialogOnActivity(Landroid/app/Dialog;)V

    goto/16 :goto_0

    .line 591
    :cond_4
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 592
    .local v5, "selectedData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 593
    .local v3, "key":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v3, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v3, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 595
    .end local v3    # "key":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    invoke-virtual {v7, v5}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->setSelectedData(Ljava/util/HashMap;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized showSelectDeviceDialog()V
    .locals 3

    .prologue
    .line 486
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 563
    :goto_0
    monitor-exit p0

    return-void

    .line 489
    :cond_0
    :try_start_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 491
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070080

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 493
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 494
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 495
    :cond_1
    const v1, 0x7f070023

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 486
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 500
    .restart local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    if-nez v1, :cond_3

    .line 501
    new-instance v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-direct {v1, v2, p0}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    .line 503
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 504
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 505
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iget v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->setfoucsPosition(I)V

    .line 508
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 510
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$4;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 531
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 541
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 542
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 562
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showDialogOnActivity(Landroid/app/Dialog;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
