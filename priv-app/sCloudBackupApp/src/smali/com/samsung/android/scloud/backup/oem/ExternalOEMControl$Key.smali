.class interface abstract Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl$Key;
.super Ljava/lang/Object;
.source "ExternalOEMControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "Key"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final FILE_CHECKSUM_LIST:Ljava/lang/String; = "file_checksum_list"

.field public static final FILE_LIST:Ljava/lang/String; = "file_list"

.field public static final INSERTED_KEY_LIST:Ljava/lang/String; = "inserted_key_list"

.field public static final IS_SUCCESS:Ljava/lang/String; = "is_success"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final MAX_COUNT:Ljava/lang/String; = "max_count"

.field public static final MAX_SIZE:Ljava/lang/String; = "max_size"

.field public static final RESTORED_KEY_LIST:Ljava/lang/String; = "restored_key_list"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final START:Ljava/lang/String; = "start"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TO_DOWNLOAD_FILE_URI:Ljava/lang/String; = "to_download_file_uri"

.field public static final TO_UPLOAD_FILE_URI:Ljava/lang/String; = "to_upload_file_uri"

.field public static final TO_UPLOAD_LIST:Ljava/lang/String; = "to_upload_list"
