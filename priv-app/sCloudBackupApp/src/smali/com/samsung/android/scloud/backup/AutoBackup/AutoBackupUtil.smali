.class public Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
.super Ljava/lang/Object;
.source "AutoBackupUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AutoBackupUtil"

.field private static mBackupAlarmManager:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;


# instance fields
.field private bAutoBackupDebug:Z

.field private bPowerConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->mBackupAlarmManager:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bPowerConnected:Z

    .line 41
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bAutoBackupDebug:Z

    .line 50
    return-void
.end method

.method private getAutoBackupRetryTime(Z)J
    .locals 9
    .param p1, "bSuccess"    # Z

    .prologue
    .line 243
    const-wide/16 v4, 0x0

    .line 245
    .local v4, "triggerTime":J
    const-wide/32 v0, 0x5265c00

    .line 246
    .local v0, "autoBackupInterval":J
    const-wide/32 v2, 0x36ee80

    .line 248
    .local v2, "retryTrigger":J
    iget-boolean v6, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bAutoBackupDebug:Z

    if-eqz v6, :cond_0

    .line 249
    const-wide/32 v0, 0x2bf20

    .line 250
    const-wide/32 v2, 0x1d4c0

    .line 253
    :cond_0
    if-eqz p1, :cond_1

    .line 254
    move-wide v4, v0

    .line 255
    const-string v6, "AutoBackupUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AutoBackupInterval(24hours, debug : 3 minutes) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :goto_0
    const-string v6, "AutoBackupUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AutoBackupRetryTime, After "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    return-wide v4

    .line 257
    :cond_1
    move-wide v4, v2

    .line 258
    const-string v6, "AutoBackupUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "retryTrigger(1hour, debug : 2 minutes) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->mBackupAlarmManager:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->mBackupAlarmManager:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    .line 56
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->mBackupAlarmManager:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    return-object v0
.end method

.method private getNextAutoBackupTriggerTime(J)J
    .locals 13
    .param p1, "lastBackupTime"    # J

    .prologue
    .line 207
    const-wide/16 v8, 0x0

    .line 208
    .local v8, "triggerTime":J
    const-wide/32 v0, 0x5265c00

    .line 209
    .local v0, "autoBackupInterval":J
    const-wide/32 v2, 0x36ee80

    .line 211
    .local v2, "defaultTrigger":J
    iget-boolean v5, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bAutoBackupDebug:Z

    if-eqz v5, :cond_0

    .line 212
    const-wide/32 v0, 0x2bf20

    .line 213
    const-wide/32 v2, 0xea60

    .line 216
    :cond_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 217
    .local v4, "mDate":Ljava/util/Date;
    add-long v6, p1, v0

    .line 218
    .local v6, "nextTriggerTime":J
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    sub-long v8, v6, v10

    .line 224
    cmp-long v5, v8, v2

    if-gez v5, :cond_1

    .line 225
    move-wide v8, v2

    .line 227
    :cond_1
    const-string v5, "AutoBackupUtil"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Next AutoBackupTime, After "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ms"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v5, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-wide v8
.end method

.method private registerAlarm(Landroid/content/Context;JLandroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J
    .param p4, "alarmIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 60
    if-nez p4, :cond_0

    .line 61
    const-string v5, "AutoBackupUtil"

    const-string v8, "AlarmIntent Cannot be null."

    invoke-static {v5, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 66
    .local v3, "mDate":Ljava/util/Date;
    const-string v5, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "====ALARM REGISTER @===: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-wide/16 v10, 0x3e8

    div-long v10, p2, v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    add-long v6, v8, p2

    .line 69
    .local v6, "triggerTime":J
    const-string v5, "com.samsung.android.scloud.backup"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const/high16 v5, 0x10000000

    move-object/from16 v0, p4

    invoke-static {p1, v12, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 73
    .local v4, "mPending":Landroid/app/PendingIntent;
    const-string v5, "alarm"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 74
    .local v2, "mAlarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v2, v12, v6, v7, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private unRegisterAlarm(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "alarmIntent"    # Landroid/content/Intent;

    .prologue
    .line 78
    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {p1, v2, p2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 82
    .local v1, "mPending":Landroid/app/PendingIntent;
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 83
    .local v0, "mAlarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 84
    return-void
.end method


# virtual methods
.method public cancelAutoBackupAlarm(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 193
    const-string v3, "AutoBackupUtil"

    const-string v4, "cancelAutoBackupAlarm is called. Clear autobackup_alarm. "

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.SecBackupApp.AutoBackup"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "alarmIntent":Landroid/content/Intent;
    const/4 v3, 0x0

    const/high16 v4, 0x10000000

    invoke-static {p1, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 200
    .local v2, "mPending":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 201
    .local v1, "mAlarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 202
    return-void
.end method

.method public checkAutoBackupCondition(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    .line 266
    const/4 v6, 0x0

    .line 268
    .local v6, "ret":Z
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    .line 269
    .local v4, "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v0

    .line 271
    .local v0, "bAutoBackupEnable":Z
    const-string v7, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[checkAutoBackupCondition] bAutoBackupEnable : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    if-ne v0, v10, :cond_1

    .line 274
    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isChargerConnected()Z

    move-result v1

    .line 275
    .local v1, "bChargerConnected":Z
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->checkWifiConnection(Landroid/content/Context;)Z

    move-result v3

    .line 276
    .local v3, "bWifiConnected":Z
    const-string v7, "power"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    .line 278
    .local v5, "powerManager":Landroid/os/PowerManager;
    invoke-virtual {v5}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    .line 280
    .local v2, "bScreenOn":Z
    if-ne v1, v10, :cond_0

    if-ne v3, v10, :cond_0

    if-nez v2, :cond_0

    .line 281
    const/4 v6, 0x1

    .line 284
    :cond_0
    const-string v7, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkAutoBackupCondition returns : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v7, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bChargerConnected : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v7, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bWifiConnected : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v7, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bScreenOn : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    .end local v1    # "bChargerConnected":Z
    .end local v2    # "bScreenOn":Z
    .end local v3    # "bWifiConnected":Z
    .end local v5    # "powerManager":Landroid/os/PowerManager;
    :cond_1
    return v6
.end method

.method public checkWifiConnection(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 296
    const/4 v1, 0x0

    .line 298
    .local v1, "ret":Z
    const-string v3, "connectivity"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 299
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 301
    .local v2, "wifi":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 302
    const/4 v1, 0x1

    .line 304
    :cond_0
    return v1
.end method

.method public getChargerStatus()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bPowerConnected:Z

    return v0
.end method

.method public hasItBeen24Hours(J)Z
    .locals 7
    .param p1, "lastBackupTime"    # J

    .prologue
    .line 88
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 90
    .local v0, "mDate":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/32 v4, 0x5265c00

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasItBeenaWeek(J)Z
    .locals 7
    .param p1, "lastURLSuccessTime"    # J

    .prologue
    .line 96
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 97
    .local v0, "mDate":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    const-wide/32 v4, -0x65813800

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetAlarm(Landroid/content/Context;JLandroid/content/Intent;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # J
    .param p4, "alarmIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 101
    if-nez p4, :cond_0

    .line 102
    const-string v5, "AutoBackupUtil"

    const-string v8, "AlarmIntent Cannot be null."

    invoke-static {v5, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :goto_0
    return-void

    .line 106
    :cond_0
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 107
    .local v3, "mDate":Ljava/util/Date;
    const-string v5, "AutoBackupUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "====ALARM REGISTER @===: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-wide/16 v10, 0x3e8

    div-long v10, p2, v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    add-long v6, v8, p2

    .line 110
    .local v6, "triggerTime":J
    const-string v5, "com.samsung.android.scloud.backup"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const/high16 v5, 0x10000000

    move-object/from16 v0, p4

    invoke-static {p1, v12, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 114
    .local v4, "mPending":Landroid/app/PendingIntent;
    const-string v5, "alarm"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    .line 115
    .local v2, "mAlarmManager":Landroid/app/AlarmManager;
    invoke-virtual {v2, v12, v6, v7, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public resetAutoBackupAlarm(Landroid/content/Context;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bSuccess"    # Z

    .prologue
    .line 233
    const-string v1, "AutoBackupUtil"

    const-string v4, "[resetAutoBackupAlarm] "

    invoke-static {v1, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-direct {p0, p2}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getAutoBackupRetryTime(Z)J

    move-result-wide v2

    .line 236
    .local v2, "triggerTime":J
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.SecBackupApp.AutoBackup"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, "alarmIntent":Landroid/content/Intent;
    invoke-direct {p0, p1, v2, v3, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->registerAlarm(Landroid/content/Context;JLandroid/content/Intent;)V

    .line 238
    return-void
.end method

.method public setAutoBackupAlarm(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 180
    const-string v6, "AutoBackupUtil"

    const-string v7, "[setAutoBackupAlarm]"

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->checkAutoBackupCondition(Landroid/content/Context;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 182
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    .line 184
    .local v1, "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastAutoBackupTime()J

    move-result-wide v2

    .line 185
    .local v2, "lastBackupTime":J
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getNextAutoBackupTriggerTime(J)J

    move-result-wide v4

    .line 187
    .local v4, "triggerTime":J
    new-instance v0, Landroid/content/Intent;

    const-string v6, "com.sec.android.SecBackupApp.AutoBackup"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "alarmIntent":Landroid/content/Intent;
    invoke-direct {p0, p1, v4, v5, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->registerAlarm(Landroid/content/Context;JLandroid/content/Intent;)V

    .line 190
    .end local v0    # "alarmIntent":Landroid/content/Intent;
    .end local v1    # "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    .end local v2    # "lastBackupTime":J
    .end local v4    # "triggerTime":J
    :cond_0
    return-void
.end method

.method public setChargerStatus(Z)V
    .locals 0
    .param p1, "bSet"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->bPowerConnected:Z

    .line 145
    return-void
.end method

.method public startAutoBackupManager(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    const-string v1, "AutoBackupUtil"

    const-string v2, "Start AutoBackupManager."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.SecBackupApp.AutoBackupManager.Start"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .local v0, "uIntentService":Landroid/content/Intent;
    const-class v1, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 157
    return-void
.end method

.method public startAutoBackupService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    const-string v2, "AutoBackupUtil"

    const-string v3, "Start AutoBackupService. Send auto backup start."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.sCloudBackupApp.AUTOBACKUP_NOW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    .local v1, "uIntentService":Landroid/content/Intent;
    const-class v2, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 122
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 125
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.SecBackupApp.AutoBackup"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "alarmIntent":Landroid/content/Intent;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->unRegisterAlarm(Landroid/content/Context;Landroid/content/Intent;)V

    .line 127
    return-void
.end method

.method public stopAutoBackupManager(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    const-string v1, "AutoBackupUtil"

    const-string v2, "Stop AutoBackupManager."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .local v0, "uIntentService":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 164
    return-void
.end method

.method public stopAutoBackupService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    const-string v2, "AutoBackupUtil"

    const-string v3, "Auto-backup STOP received..."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    .line 132
    .local v0, "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityAutoBackup()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.sCloudBackupApp.AUTOBACKUP_STOP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    .local v1, "uIntentService":Landroid/content/Intent;
    const-class v2, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 135
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 141
    :goto_0
    return-void

    .line 138
    .end local v1    # "uIntentService":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    .restart local v1    # "uIntentService":Landroid/content/Intent;
    invoke-virtual {p1, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0
.end method
