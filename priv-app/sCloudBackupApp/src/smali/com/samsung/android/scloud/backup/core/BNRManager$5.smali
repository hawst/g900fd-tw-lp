.class Lcom/samsung/android/scloud/backup/core/BNRManager$5;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

.field final synthetic val$cnt:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iput p2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->val$cnt:I

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 7
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 370
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupCommit Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    if-nez p1, :cond_1

    .line 372
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->commitLastBackupTime()V

    .line 373
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setOperationFinished(Ljava/lang/String;Z)V

    .line 374
    const-string v2, "storageinfo"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    :try_start_0
    const-string v2, "storageinfo"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 378
    .local v1, "storageJson":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # invokes: Lcom/samsung/android/scloud/backup/core/BNRManager;->notifyQuota(Lorg/json/JSONObject;)V
    invoke-static {v2, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$800(Lcom/samsung/android/scloud/backup/core/BNRManager;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    .end local v1    # "storageJson":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 379
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 386
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "backupCommit is failed"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->val$cnt:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_2

    .line 389
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "backupCommit sleep for retry - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 390
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 394
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->val$cnt:I

    add-int/lit8 v3, v3, 0x1

    # invokes: Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V
    invoke-static {v2, v3, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$900(Lcom/samsung/android/scloud/backup/core/BNRManager;II)V

    goto :goto_0

    .line 391
    :catch_1
    move-exception v0

    .line 392
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$5;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "backupCommit retry sleep err"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 396
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x13b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "backupCommit failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v2
.end method
