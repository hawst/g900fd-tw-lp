.class Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList$BackupTimeComparator;
.super Ljava/lang/Object;
.source "BackupDetailsList.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BackupTimeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;)I
    .locals 2
    .param p1, "o1"    # Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;
    .param p2, "o2"    # Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;

    .prologue
    .line 67
    invoke-virtual {p2}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;->getLastBackupTime()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;->getLastBackupTime()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 65
    check-cast p1, Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList$BackupTimeComparator;->compare(Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;)I

    move-result v0

    return v0
.end method
