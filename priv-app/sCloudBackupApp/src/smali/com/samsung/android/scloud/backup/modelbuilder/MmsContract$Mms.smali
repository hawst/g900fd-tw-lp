.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms;
.super Ljava/lang/Object;
.source "MmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Mms"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms$Part;,
        Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms$Addr;
    }
.end annotation


# static fields
.field public static final CONTENT_CLASS:Ljava/lang/String; = "ct_cls"

.field public static final CT_L:Ljava/lang/String; = "ct_l"

.field public static final CT_T:Ljava/lang/String; = "ct_t"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DATE_SENT:Ljava/lang/String; = "date_sent"

.field public static final DELIVERY_TIME:Ljava/lang/String; = "d_tm"

.field public static final D_RPT:Ljava/lang/String; = "d_rpt"

.field public static final EXP:Ljava/lang/String; = "exp"

.field public static final LOCKED:Ljava/lang/String; = "locked"

.field public static final MESSAGE_TYPE_DELIVERY_IND:I = 0x86

.field public static final MESSAGE_TYPE_NOTIFICATION_IND:I = 0x82

.field public static final MESSAGE_TYPE_READ_ORIG_IND:I = 0x88

.field public static final MESSAGE_TYPE_READ_REC_IND:I = 0x87

.field public static final MMS:Ljava/lang/String; = "MMS"

.field public static final MSG_BOX:Ljava/lang/String; = "msg_box"

.field public static final M_CLS:Ljava/lang/String; = "m_cls"

.field public static final M_ID:Ljava/lang/String; = "m_id"

.field public static final M_SIZE:Ljava/lang/String; = "m_size"

.field public static final M_TYPE:Ljava/lang/String; = "m_type"

.field public static final PRIORITY:Ljava/lang/String; = "pri"

.field public static final READ:Ljava/lang/String; = "read"

.field public static final READ_STATUS:Ljava/lang/String; = "read_status"

.field public static final REPORT_ALLOWED:Ljava/lang/String; = "rpt_a"

.field public static final RESERVED:Ljava/lang/String; = "reserved"

.field public static final RESPONSE_STATUS:Ljava/lang/String; = "resp_st"

.field public static final RESPONSE_TEXT:Ljava/lang/String; = "resp_txt"

.field public static final RETRIEVE_STATUS:Ljava/lang/String; = "retr_st"

.field public static final RETRIEVE_TEXT:Ljava/lang/String; = "retr_txt"

.field public static final RETRIEVE_TEXT_CHARSET:Ljava/lang/String; = "retr_txt_cs"

.field public static final RR:Ljava/lang/String; = "rr"

.field public static final SEEN:Ljava/lang/String; = "seen"

.field public static final STATUS:Ljava/lang/String; = "st"

.field public static final SUB:Ljava/lang/String; = "sub"

.field public static final SUBJECT_CHARSET:Ljava/lang/String; = "sub_cs"

.field public static final TR_ID:Ljava/lang/String; = "tr_id"

.field public static final V:Ljava/lang/String; = "v"
