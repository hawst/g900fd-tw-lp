.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupSources$OEMAuthorities;
.super Ljava/lang/Object;
.source "BackupSources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OEMAuthorities"
.end annotation


# static fields
.field public static final BLACK_LIST_OEM_AUTHORITY:Ljava/lang/String; = "com.android.email.provider"

.field public static final BLACK_LIST_OEM_CONTENT_URI:Ljava/lang/String; = "content://com.android.email.provider/blacklist"

.field public static final CALL_REJECT_OEM_AUTHORITY:Ljava/lang/String; = "com.android.phone.callsettings"

.field public static final CALL_REJECT_OEM_CONTENT_URI:Ljava/lang/String; = "content://com.android.phone.callsettings/reject_num"

.field public static final LOGS_OEM_AUTHORITY:Ljava/lang/String; = "logs"

.field public static final LOGS_OEM_CONTENT_URI:Ljava/lang/String; = "content://logs/historys"

.field public static final MMS_OEM_AUTHORITY:Ljava/lang/String; = "mms"

.field public static final MMS_OEM_CONTENT_URI:Ljava/lang/String; = "content://mms"

.field public static final SMS_OEM_AUTHORITY:Ljava/lang/String; = "sms"

.field public static final SMS_OEM_CONTENT_URI:Ljava/lang/String; = "content://sms"

.field public static final SPAM_OEM_AUTHORITY:Ljava/lang/String; = "mms-sms"

.field public static final SPAM_OEM_CONTENT_URI:Ljava/lang/String; = "content://mms-sms/spam-filter"

.field public static final VIP_OEM_AUTHORITY:Ljava/lang/String; = "com.android.email.provider"

.field public static final VIP_OEM_CONTENT_URI:Ljava/lang/String; = "content://com.android.email.provider/viplist"

.field public static final WALLPAPER_OEM_AUTHORITY:Ljava/lang/String; = "com.sec.android.sCloudWallpaperBackupProvider"

.field public static final WALLPAPER_OEM_CONTENT_URI:Ljava/lang/String; = "content://com.sec.android.sCloudWallpaperBackupProvider"
