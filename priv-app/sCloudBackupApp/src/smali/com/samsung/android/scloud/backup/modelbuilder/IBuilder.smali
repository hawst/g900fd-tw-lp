.class public abstract Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;
.super Ljava/lang/Object;
.source "IBuilder.java"


# instance fields
.field protected myModel:Lcom/samsung/android/scloud/backup/model/IModel;


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 1
    .param p1, "mModel"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    .line 43
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    .line 44
    return-void
.end method


# virtual methods
.method public abstract backupCleared(Landroid/content/Context;)V
.end method

.method public getIDKey(Ljava/util/List;)[Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p1, "key":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 65
    if-nez p1, :cond_0

    move-object v4, v5

    .line 78
    :goto_0
    return-object v4

    .line 68
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 69
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "Id":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    if-ge v2, v1, :cond_2

    .line 71
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 72
    .local v3, "pos":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 73
    invoke-interface {v0, v2, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 70
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 78
    .end local v3    # "pos":I
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    goto :goto_0
.end method

.method public abstract getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method

.method public getLength(J)I
    .locals 9
    .param p1, "num"    # J

    .prologue
    const-wide/16 v6, 0xa

    .line 100
    const/4 v2, 0x1

    .line 101
    .local v2, "length":I
    move-wide v0, p1

    .line 102
    .local v0, "comp":J
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gtz v3, :cond_0

    .line 103
    const-wide/16 v0, 0xa

    .line 105
    :cond_0
    :goto_0
    cmp-long v3, v0, v6

    if-gez v3, :cond_1

    .line 110
    return v2

    .line 106
    :cond_1
    div-long/2addr v0, v6

    .line 107
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public abstract getOrderByColumnName()Ljava/lang/String;
.end method

.method public abstract getProjection()[Ljava/lang/String;
.end method

.method public getSelectionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "_id IN "

    return-object v0
.end method

.method public abstract getSourceProjection()[Ljava/lang/String;
.end method

.method public abstract getWhere()Ljava/lang/String;
.end method

.method public getWhereKey([Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "keys"    # [Ljava/lang/String;

    .prologue
    .line 83
    const-string v0, ""

    .line 84
    .local v0, "StringKey":Ljava/lang/String;
    if-nez p1, :cond_0

    move-object v1, v0

    .line 95
    .end local v0    # "StringKey":Ljava/lang/String;
    .local v1, "StringKey":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 87
    .end local v1    # "StringKey":Ljava/lang/String;
    .restart local v0    # "StringKey":Ljava/lang/String;
    :cond_0
    move-object v2, p1

    .local v2, "arr$":[Ljava/lang/String;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v4, v2, v3

    .line 88
    .local v4, "key":Ljava/lang/String;
    if-eqz v4, :cond_1

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 87
    :cond_1
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 90
    :cond_2
    const-string v6, ","

    invoke-virtual {v0, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-virtual {v0, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 93
    .end local v4    # "key":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 95
    .end local v0    # "StringKey":Ljava/lang/String;
    .restart local v1    # "StringKey":Ljava/lang/String;
    goto :goto_0
.end method

.method public abstract postOperationOnBackup(Landroid/content/Context;Z)V
.end method

.method public abstract preOperationOnRestore(Landroid/content/Context;)V
.end method

.method public abstract putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method
