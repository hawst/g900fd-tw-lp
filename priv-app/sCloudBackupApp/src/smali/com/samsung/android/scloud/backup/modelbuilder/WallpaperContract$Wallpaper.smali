.class public Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;
.super Ljava/lang/Object;
.source "WallpaperContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Wallpaper"
.end annotation


# static fields
.field static final FILE_UPLOAD_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final VALUE:Ljava/lang/String; = "value"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    .line 46
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    const-string v1, "lockscreen_wallpaper"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    const-string v1, "wallpaper"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    const-string v1, "lockscreen_wallpaper_ripple"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
