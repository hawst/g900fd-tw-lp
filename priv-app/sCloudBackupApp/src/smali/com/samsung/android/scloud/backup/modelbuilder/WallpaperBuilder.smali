.class public Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;
.super Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;
.source "WallpaperBuilder.java"


# static fields
.field private static FILE_WATING_CNT:I = 0x0

.field private static FILE_WATING_TIME:J = 0x0L

.field private static final TAG:Ljava/lang/String; = "WallpaperBuilder"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-wide/16 v0, 0x7d0

    sput-wide v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_TIME:J

    .line 52
    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_CNT:I

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "mModel"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 56
    return-void
.end method

.method private backedLockScreen(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 22
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 314
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const-string v19, "lockscreen_wallpaper_path"

    invoke-static/range {v18 .. v19}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 315
    .local v17, "lockscreenwallpaperPath":Ljava/lang/String;
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "backedLockScreen - location : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const-string v19, "lockscreen_wallpaper"

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v16

    .line 321
    .local v16, "lockscreenWallpaper":I
    if-nez v17, :cond_0

    .line 322
    const-string v17, ""

    .line 324
    :cond_0
    const/16 v18, 0x2

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 325
    const-string v18, "WallpaperBuilder"

    const-string v19, "Lockscreen is Live View"

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "just_lock_livewallpaper_package_name"

    invoke-static/range {v19 .. v20}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "--"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    const-string v20, "just_lock_livewallpaper_class_name"

    invoke-static/range {v19 .. v20}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 330
    :cond_1
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    .line 331
    .local v14, "itemJson":Lorg/json/JSONObject;
    new-instance v15, Lorg/json/JSONObject;

    invoke-direct {v15}, Lorg/json/JSONObject;-><init>()V

    .line 333
    .local v15, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v18, "lockscreen_wallpaper"

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 335
    const-string v18, "value"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 336
    const-string v18, "name"

    const-string v19, "lockscreen_wallpaper"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 339
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->getLockWallPaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 342
    const-string v18, "key"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "BACKUP_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x2

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 344
    const-string v18, "timestamp"

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getLockScreenTimestamp()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v14, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 345
    const-string v18, "value"

    new-instance v19, Lorg/json/JSONObject;

    invoke-direct/range {v19 .. v19}, Lorg/json/JSONObject;-><init>()V

    const-string v20, "HOMESCREEN"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :goto_0
    invoke-static {v14}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v13

    .line 353
    .local v13, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "local Json - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const/4 v4, 0x0

    .line 356
    .local v4, "chksum1":Ljava/lang/String;
    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 357
    new-instance v11, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v18, v0

    check-cast v18, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v13}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "lockscreen_wallpaper"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 358
    .local v11, "file1":Ljava/io/File;
    const/4 v6, 0x0

    .line 359
    .local v6, "cnt1":I
    :goto_1
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_2

    .line 360
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "waiting file - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :try_start_1
    sget-wide v18, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_TIME:J

    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 366
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "cnt1":I
    .local v7, "cnt1":I
    sget v18, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_CNT:I

    move/from16 v0, v18

    if-le v6, v0, :cond_a

    .line 367
    new-instance v18, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v19, 0x138

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "File not found : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v18

    .line 347
    .end local v4    # "chksum1":Ljava/lang/String;
    .end local v7    # "cnt1":I
    .end local v11    # "file1":Ljava/io/File;
    .end local v13    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :catch_0
    move-exception v10

    .line 348
    .local v10, "e":Lorg/json/JSONException;
    const-string v18, "WallpaperBuilder"

    const-string v19, "backedUpWallPaper err "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 363
    .end local v10    # "e":Lorg/json/JSONException;
    .restart local v4    # "chksum1":Ljava/lang/String;
    .restart local v6    # "cnt1":I
    .restart local v11    # "file1":Ljava/io/File;
    .restart local v13    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :catch_1
    move-exception v10

    .line 364
    .local v10, "e":Ljava/lang/InterruptedException;
    new-instance v18, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v19, 0x13a

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v18

    .line 371
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :cond_2
    :try_start_2
    invoke-static {v11}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigest(Ljava/io/File;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v4

    .line 379
    .end local v6    # "cnt1":I
    .end local v11    # "file1":Ljava/io/File;
    :cond_3
    const/4 v5, 0x0

    .line 380
    .local v5, "chksum2":Ljava/lang/String;
    if-lez v16, :cond_5

    .line 381
    new-instance v12, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v18, v0

    check-cast v18, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v13}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "lockscreen_wallpaper_ripple"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 382
    .local v12, "file2":Ljava/io/File;
    const/4 v8, 0x0

    .line 383
    .local v8, "cnt2":I
    :goto_2
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_4

    .line 384
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "waiting file - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :try_start_3
    sget-wide v18, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_TIME:J

    invoke-static/range {v18 .. v19}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    .line 390
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "cnt2":I
    .local v9, "cnt2":I
    sget v18, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_CNT:I

    move/from16 v0, v18

    if-le v8, v0, :cond_9

    .line 392
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "File not found : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v8, v9

    .line 397
    .end local v9    # "cnt2":I
    .restart local v8    # "cnt2":I
    :cond_4
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 399
    :try_start_4
    invoke-static {v12}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigest(Ljava/io/File;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v5

    .line 408
    .end local v8    # "cnt2":I
    .end local v12    # "file2":Ljava/io/File;
    :cond_5
    const-string v18, "WallpaperBuilder"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "checksum lockscreen_wallpaper - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", lockscreen_wallpaper_ripple - "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    if-eqz v4, :cond_6

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getLockScreenChecksum()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 411
    const-string v18, "lockscreen_wallpaper"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0, v4}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_6
    if-eqz v5, :cond_7

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getLockScreenRippleChecksum()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 414
    const-string v18, "lockscreen_wallpaper_ripple"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0, v5}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_7
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->setLockScreenChecksum(Ljava/lang/String;)V

    .line 417
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->setLockScreenRippleChecksum(Ljava/lang/String;)V

    .line 419
    const-string v19, "WallpaperBuilder"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "checksum lockscreen_wallpaper att - "

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;->hasFile()Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-virtual {v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getAttachmentFileInfo()Ljava/util/Map;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    :goto_3
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    return-object v13

    .line 372
    .end local v5    # "chksum2":Ljava/lang/String;
    .restart local v6    # "cnt1":I
    .restart local v11    # "file1":Ljava/io/File;
    :catch_2
    move-exception v10

    .line 373
    .local v10, "e":Ljava/lang/Exception;
    const-string v18, "WallpaperBuilder"

    const-string v19, "backedUpWallPaper err "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 374
    new-instance v18, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v19, 0x13a

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v18

    .line 387
    .end local v6    # "cnt1":I
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "file1":Ljava/io/File;
    .restart local v5    # "chksum2":Ljava/lang/String;
    .restart local v8    # "cnt2":I
    .restart local v12    # "file2":Ljava/io/File;
    :catch_3
    move-exception v10

    .line 388
    .local v10, "e":Ljava/lang/InterruptedException;
    new-instance v18, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v19, 0x13a

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v18

    .line 400
    .end local v10    # "e":Ljava/lang/InterruptedException;
    :catch_4
    move-exception v10

    .line 401
    .local v10, "e":Ljava/lang/Exception;
    const-string v18, "WallpaperBuilder"

    const-string v19, "backedUpWallPaper err "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 402
    new-instance v18, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v19, 0x13a

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v0, v1, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v18

    .line 419
    .end local v8    # "cnt2":I
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v12    # "file2":Ljava/io/File;
    :cond_8
    const-string v18, "no files to upload"

    goto :goto_3

    .restart local v9    # "cnt2":I
    .restart local v12    # "file2":Ljava/io/File;
    :cond_9
    move v8, v9

    .end local v9    # "cnt2":I
    .restart local v8    # "cnt2":I
    goto/16 :goto_2

    .end local v5    # "chksum2":Ljava/lang/String;
    .end local v8    # "cnt2":I
    .end local v12    # "file2":Ljava/io/File;
    .restart local v7    # "cnt1":I
    .restart local v11    # "file1":Ljava/io/File;
    :cond_a
    move v6, v7

    .end local v7    # "cnt1":I
    .restart local v6    # "cnt1":I
    goto/16 :goto_1
.end method

.method private backedUpWallPaper(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v12, 0x13a

    .line 151
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->getWallPaperLocation(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 152
    .local v8, "wallPaperLoc":Ljava/lang/String;
    const-string v9, "WallpaperBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "backedUpWallPaper location : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 155
    .local v6, "itemJson":Lorg/json/JSONObject;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 157
    .local v7, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v9, "name"

    const-string v10, "Home_Screen_WallPaper"

    invoke-virtual {v7, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    const-string v9, "lockscreen_wallpaper"

    const/4 v10, -0x1

    invoke-virtual {v7, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 159
    const-string v9, "lockscreen_wallpaper_ripple"

    const/4 v10, -0x1

    invoke-virtual {v7, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 160
    const-string v9, "value"

    invoke-virtual {v7, v9, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    const-string v9, "/data/system/users/0"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 164
    const-string v9, "WallpaperBuilder"

    const-string v10, "backedUpWallPaper - static Wall Paper"

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->getWallPaper(Landroid/content/Context;)V

    .line 170
    :cond_0
    const-string v9, "key"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "BACKUP_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v11}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 172
    const-string v9, "timestamp"

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getWallpaperTimestamp()J

    move-result-wide v10

    invoke-virtual {v6, v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 173
    const-string v9, "value"

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    const-string v11, "HOMESCREEN"

    invoke-virtual {v10, v11, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v10

    invoke-virtual {v10}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    invoke-static {v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v5

    .line 182
    .local v5, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const-string v9, "/data/system/users/0"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 183
    new-instance v4, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    check-cast v9, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    invoke-interface {v9, p1, v5}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "wallpaper"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 184
    .local v4, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 185
    .local v1, "cnt":I
    :goto_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_1

    .line 186
    const-string v9, "WallpaperBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "waiting file - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :try_start_1
    sget-wide v10, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_TIME:J

    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 192
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "cnt":I
    .local v2, "cnt":I
    sget v9, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->FILE_WATING_CNT:I

    if-le v1, v9, :cond_4

    .line 193
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v10, 0x138

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "File not found : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v9

    .line 175
    .end local v2    # "cnt":I
    .end local v4    # "file":Ljava/io/File;
    .end local v5    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :catch_0
    move-exception v3

    .line 176
    .local v3, "e":Lorg/json/JSONException;
    const-string v9, "WallpaperBuilder"

    const-string v10, "backedUpWallPaper err "

    invoke-static {v9, v10, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 177
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    invoke-direct {v9, v12, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v9

    .line 189
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v1    # "cnt":I
    .restart local v4    # "file":Ljava/io/File;
    .restart local v5    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :catch_1
    move-exception v3

    .line 190
    .local v3, "e":Ljava/lang/InterruptedException;
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    invoke-direct {v9, v12, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v9

    .line 196
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const/4 v0, 0x0

    .line 198
    .local v0, "chksum":Ljava/lang/String;
    :try_start_2
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigest(Ljava/io/File;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    .line 204
    const-string v9, "WallpaperBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checksum wallpaper - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getWallpaperChecksum()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 206
    const-string v9, "wallpaper"

    invoke-virtual {v5, v9, v0}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_2
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->setWallpaperChecksum(Ljava/lang/String;)V

    .line 211
    .end local v0    # "chksum":Ljava/lang/String;
    .end local v1    # "cnt":I
    .end local v4    # "file":Ljava/io/File;
    :cond_3
    return-object v5

    .line 199
    .restart local v0    # "chksum":Ljava/lang/String;
    .restart local v1    # "cnt":I
    .restart local v4    # "file":Ljava/io/File;
    :catch_2
    move-exception v3

    .line 200
    .local v3, "e":Ljava/lang/Exception;
    const-string v9, "WallpaperBuilder"

    const-string v10, "backedUpWallPaper err "

    invoke-static {v9, v10, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 201
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    invoke-direct {v9, v12, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v9

    .end local v0    # "chksum":Ljava/lang/String;
    .end local v1    # "cnt":I
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "cnt":I
    :cond_4
    move v1, v2

    .end local v2    # "cnt":I
    .restart local v1    # "cnt":I
    goto/16 :goto_0
.end method

.method public static createExplicitFromImplicitIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "implicitIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x0

    .line 454
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 455
    .local v4, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, p1, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 458
    .local v5, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_1

    .line 459
    :cond_0
    const/4 v2, 0x0

    .line 474
    :goto_0
    return-object v2

    .line 463
    :cond_1
    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 464
    .local v6, "serviceInfo":Landroid/content/pm/ResolveInfo;
    iget-object v7, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v7, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 465
    .local v3, "packageName":Ljava/lang/String;
    iget-object v7, v6, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 466
    .local v0, "className":Ljava/lang/String;
    new-instance v1, Landroid/content/ComponentName;

    invoke-direct {v1, v3, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    .local v1, "component":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 472
    .local v2, "explicitIntent":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private getLockScreenTravel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 478
    const-string v3, "lockscreen_wallpaper"

    .line 479
    .local v3, "fileName":Ljava/lang/String;
    const-string v10, "WallpaperBuilder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getLockScreenTravel - path : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " -> "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "lockscreen_wallpaper"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const/4 v4, 0x0

    .line 482
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 483
    .local v6, "fos":Ljava/io/FileOutputStream;
    const/4 v2, 0x0

    .line 485
    .local v2, "file":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 487
    .restart local v2    # "file":Ljava/io/File;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 490
    :try_start_0
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "lockscreen_wallpaper"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 493
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .local v5, "fis":Ljava/io/FileInputStream;
    if-eqz v5, :cond_0

    .line 495
    :try_start_2
    invoke-virtual {v5}, Ljava/io/FileInputStream;->available()I

    move-result v9

    .line 496
    .local v9, "size":I
    new-array v0, v9, [B

    .line 499
    .local v0, "buffer":[B
    invoke-virtual {v5, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v8

    .line 500
    .local v8, "len":I
    const/4 v10, 0x0

    invoke-virtual {v7, v0, v10, v8}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 508
    .end local v0    # "buffer":[B
    .end local v8    # "len":I
    .end local v9    # "size":I
    :cond_0
    if-eqz v5, :cond_1

    .line 509
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 516
    :cond_1
    :goto_0
    if-eqz v7, :cond_2

    .line 517
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_2
    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 525
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :cond_3
    :goto_1
    return-void

    .line 510
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 512
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 518
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 520
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 523
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_1

    .line 503
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 504
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v10, "WallpaperBuilder"

    const-string v11, "getLockScreenTravel err"

    invoke-static {v10, v11, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 508
    if-eqz v4, :cond_4

    .line 509
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 516
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_3
    if-eqz v6, :cond_3

    .line 517
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 518
    :catch_3
    move-exception v1

    .line 520
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 510
    .local v1, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    .line 512
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 507
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 508
    :goto_4
    if-eqz v4, :cond_5

    .line 509
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 516
    :cond_5
    :goto_5
    if-eqz v6, :cond_6

    .line 517
    :try_start_9
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 521
    :cond_6
    :goto_6
    throw v10

    .line 510
    :catch_5
    move-exception v1

    .line 512
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 518
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 520
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 507
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v10

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v10

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 503
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v1

    move-object v6, v7

    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "fos":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method private getLockWallPaper(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "wallpaperPath"    # Ljava/lang/String;

    .prologue
    .line 428
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 430
    .local v1, "uIntentService":Landroid/content/Intent;
    const-string v2, "wallpaperPath"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 431
    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 432
    const-string v2, "GET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    const-string v2, "default"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 445
    :cond_0
    :goto_0
    const-string v2, "WallpaperBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting the service - action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-static {p1, v1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->createExplicitFromImplicitIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 447
    .local v0, "finalIntent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 448
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 450
    :cond_1
    return-void

    .line 434
    .end local v0    # "finalIntent":Landroid/content/Intent;
    :cond_2
    const-string v2, "/data/data/com.sec.android.gallery3d/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 435
    const-string v2, "GET_LOCKSCREEN_ACTION_GALLERY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 436
    :cond_3
    const-string v2, "/data/data/com.sec.android.app.wallpaperchooser/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 437
    const-string v2, "GET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    const-string v2, "default"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 439
    :cond_4
    const-string v2, "/data/data/com.samsung.android.service.travel/files/images/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "/data/data/com.samsung.android.keyguardwallpaperupdator/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->getLockScreenTravel(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getWallPaper(Landroid/content/Context;)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 240
    const-string v1, "wallpaper"

    .line 241
    .local v1, "Filename":Ljava/lang/String;
    const/4 v9, 0x0

    .line 243
    .local v9, "fos":Ljava/io/FileOutputStream;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 244
    .local v0, "Dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_0

    .line 245
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v11

    .line 247
    .local v11, "isDirectoriesMade":Z
    const-string v13, "WallpaperBuilder"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "isDirectoriesMade : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    .end local v11    # "isDirectoriesMade":Z
    :cond_0
    new-instance v8, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 251
    .local v8, "fileOp":Ljava/io/File;
    invoke-static/range {p1 .. p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v12

    .line 252
    .local v12, "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v12}, Landroid/app/WallpaperManager;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 254
    .local v5, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v5, :cond_2

    .line 255
    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    .end local v5    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 256
    .local v3, "bmp":Landroid/graphics/Bitmap;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 257
    .local v2, "baf":Ljava/io/ByteArrayOutputStream;
    sget-object v13, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v14, 0x64

    invoke-virtual {v3, v13, v14, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 262
    :try_start_0
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .local v10, "fos":Ljava/io/FileOutputStream;
    move-object v9, v10

    .line 274
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 276
    .local v4, "buffer":[B
    invoke-virtual {v9, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 284
    if-eqz v2, :cond_1

    .line 288
    :try_start_2
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 300
    :cond_1
    :goto_1
    if-eqz v9, :cond_2

    .line 302
    :try_start_3
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 311
    .end local v2    # "baf":Ljava/io/ByteArrayOutputStream;
    .end local v3    # "bmp":Landroid/graphics/Bitmap;
    .end local v4    # "buffer":[B
    :cond_2
    :goto_2
    return-void

    .line 264
    .restart local v2    # "baf":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "bmp":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 266
    .local v7, "e1":Ljava/io/FileNotFoundException;
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to create O-p stream"

    invoke-static {v13, v14, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 290
    .end local v7    # "e1":Ljava/io/FileNotFoundException;
    .restart local v4    # "buffer":[B
    :catch_1
    move-exception v6

    .line 292
    .local v6, "e":Ljava/io/IOException;
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v13, v14, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 304
    .end local v6    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 306
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v13, v14, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 278
    .end local v4    # "buffer":[B
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 280
    .restart local v6    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to open i/p stream or read"

    invoke-static {v13, v14, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 284
    if-eqz v2, :cond_3

    .line 288
    :try_start_5
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 300
    :cond_3
    :goto_3
    if-eqz v9, :cond_2

    .line 302
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 304
    :catch_4
    move-exception v6

    .line 306
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v13, v14, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 290
    :catch_5
    move-exception v6

    .line 292
    const-string v13, "WallpaperBuilder"

    const-string v14, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v13, v14, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 284
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    if-eqz v2, :cond_4

    .line 288
    :try_start_7
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 300
    :cond_4
    :goto_4
    if-eqz v9, :cond_5

    .line 302
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 308
    :cond_5
    :goto_5
    throw v13

    .line 290
    :catch_6
    move-exception v6

    .line 292
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v14, v15, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 304
    .end local v6    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v6

    .line 306
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "getWallPaper: Failed to close stream(s) - "

    invoke-static {v14, v15, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method

.method private getWallPaperLocation(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    invoke-static {p1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    .line 223
    .local v1, "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v1}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v2

    .line 226
    .local v2, "wpi":Landroid/app/WallpaperInfo;
    if-eqz v2, :cond_0

    .line 228
    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "wallPaperPath":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 231
    .end local v0    # "wallPaperPath":Ljava/lang/String;
    :cond_0
    const-string v0, "/data/system/users/0"

    .restart local v0    # "wallPaperPath":Ljava/lang/String;
    goto :goto_0
.end method

.method private static isLiveWallpaperAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallPaperName"    # Ljava/lang/String;

    .prologue
    .line 756
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 757
    .local v5, "uPackageManager":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.service.wallpaper.WallpaperService"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 760
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 761
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v2, 0x0

    .line 763
    .local v2, "info":Landroid/app/WallpaperInfo;
    :try_start_0
    new-instance v2, Landroid/app/WallpaperInfo;

    .end local v2    # "info":Landroid/app/WallpaperInfo;
    invoke-direct {v2, p0, v4}, Landroid/app/WallpaperInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 771
    .restart local v2    # "info":Landroid/app/WallpaperInfo;
    invoke-virtual {v2}, Landroid/app/WallpaperInfo;->getServiceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 772
    const/4 v6, 0x1

    .line 774
    .end local v2    # "info":Landroid/app/WallpaperInfo;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :goto_1
    return v6

    .line 764
    .restart local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v6, "WallpaperBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Skipping wallpaper "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 767
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v0

    .line 768
    .local v0, "e":Ljava/io/IOException;
    const-string v6, "WallpaperBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Skipping wallpaper "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 774
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private static restoreHomeScreenLiveWallPaper(Landroid/content/Context;Ljava/lang/String;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaperPackageClass"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 721
    invoke-static {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->isLiveWallpaperAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 722
    .local v3, "isLiveWallpaperFound":Z
    const-string v9, "WallpaperBuilder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "restoreHomeScreenLiveWallPaper : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", isLiveWallpaperFound : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    if-eqz v3, :cond_0

    .line 724
    new-instance v2, Landroid/content/Intent;

    const-string v9, "android.service.wallpaper.WallpaperService"

    invoke-direct {v2, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 725
    .local v2, "intent":Landroid/content/Intent;
    const-string v9, "\\."

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 726
    .local v7, "split":[Ljava/lang/String;
    array-length v9, v7

    add-int/lit8 v9, v9, -0x1

    aget-object v6, v7, v9

    .line 727
    .local v6, "remove":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    sub-int/2addr v9, v10

    invoke-virtual {p1, v12, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 728
    .local v8, "wallpaperPackageName":Ljava/lang/String;
    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 730
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    .line 734
    .local v4, "mWallpaperManager":Landroid/app/WallpaperManager;
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "getIWallpaperManager"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v9, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 735
    .local v5, "method":Ljava/lang/reflect/Method;
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v5, v4, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 738
    .local v1, "iIWallpaperManager":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    const-string v10, "setWallpaperComponent"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Class;

    const/4 v12, 0x0

    const-class v13, Landroid/content/ComponentName;

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 739
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v1, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    .line 752
    .end local v1    # "iIWallpaperManager":Ljava/lang/Object;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "mWallpaperManager":Landroid/app/WallpaperManager;
    .end local v5    # "method":Ljava/lang/reflect/Method;
    .end local v6    # "remove":Ljava/lang/String;
    .end local v7    # "split":[Ljava/lang/String;
    .end local v8    # "wallpaperPackageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 741
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v4    # "mWallpaperManager":Landroid/app/WallpaperManager;
    .restart local v6    # "remove":Ljava/lang/String;
    .restart local v7    # "split":[Ljava/lang/String;
    .restart local v8    # "wallpaperPackageName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 743
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 744
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v0

    .line 746
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 747
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_2
    move-exception v0

    .line 749
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method private static restoreHomeScreenWallPaper(Landroid/content/Context;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 679
    const-string v0, "wallpaper"

    .line 680
    .local v0, "Filename":Ljava/lang/String;
    const/4 v4, 0x0

    .line 681
    .local v4, "is":Ljava/io/InputStream;
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 682
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    .line 683
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v6

    .line 685
    .local v6, "isDirectoriesMade":Z
    const-string v8, "WallpaperBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Directories Made : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    .end local v6    # "isDirectoriesMade":Z
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 688
    .local v3, "imagePath":Ljava/lang/String;
    const-string v8, "WallpaperBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "restoreHomeScreenWallPaper : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 702
    .end local v4    # "is":Ljava/io/InputStream;
    .local v5, "is":Ljava/io/InputStream;
    if-eqz v3, :cond_1

    .line 703
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v7

    .line 707
    .local v7, "wm":Landroid/app/WallpaperManager;
    :try_start_1
    invoke-virtual {v7, v5}, Landroid/app/WallpaperManager;->setStream(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 713
    .end local v7    # "wm":Landroid/app/WallpaperManager;
    :cond_1
    :goto_0
    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_1
    move-object v4, v5

    .line 718
    .end local v5    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :goto_2
    return-void

    .line 692
    :catch_0
    move-exception v2

    .line 693
    .local v2, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 708
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v7    # "wm":Landroid/app/WallpaperManager;
    :catch_1
    move-exception v2

    .line 709
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "WallpaperBuilder"

    const-string v9, "Inside catch"

    invoke-static {v8, v9, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 714
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "wm":Landroid/app/WallpaperManager;
    :catch_2
    move-exception v2

    .line 715
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private static restoreLockScreenWallPaper(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "wallpaperPath"    # Ljava/lang/String;

    .prologue
    .line 581
    const-string v2, "WallpaperBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreLockScreenWallPaper : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 586
    .local v1, "uIntentService":Landroid/content/Intent;
    const-string v2, "wallpaperPath"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 587
    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 588
    const-string v2, "SET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    :cond_0
    :goto_0
    const-string v2, "WallpaperBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting the service - action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    invoke-static {p0, v1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->createExplicitFromImplicitIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 599
    .local v0, "finalIntent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    .line 600
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 601
    :cond_1
    return-void

    .line 589
    .end local v0    # "finalIntent":Landroid/content/Intent;
    :cond_2
    const-string v2, "/data/data/com.sec.android.gallery3d/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 590
    const-string v2, "SET_LOCKSCREEN_ACTION_GALLERY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 591
    :cond_3
    const-string v2, "/data/data/com.sec.android.app.wallpaperchooser/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 592
    const-string v2, "SET_LOCKSCREEN_ACTION_WALLPAPER_CHOOSER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 593
    :cond_4
    const-string v2, "/data/data/com.samsung.android.service.travel/files/images/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "/data/data/com.samsung.android.keyguardwallpaperupdator/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 594
    :cond_5
    invoke-static {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->setLockScreenTravel(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static setLockScreenTravel(Landroid/content/Context;Ljava/lang/String;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 604
    const-string v4, "lockscreen_wallpaper"

    .line 605
    .local v4, "fileName":Ljava/lang/String;
    const-string v14, "WallpaperBuilder"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "setLockScreenTravel - path : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "lockscreen_wallpaper"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " -> "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    const/4 v5, 0x0

    .line 607
    .local v5, "fis":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 608
    .local v7, "fos":Ljava/io/FileOutputStream;
    const/4 v13, 0x0

    .line 612
    .local v13, "lockScreenPath":Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "lockscreen_wallpaper"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v6, v14}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 614
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v6, "fis":Ljava/io/FileInputStream;
    if-eqz v6, :cond_5

    .line 615
    :try_start_1
    invoke-virtual {v6}, Ljava/io/FileInputStream;->available()I

    move-result v12

    .line 616
    .local v12, "length":I
    if-gtz v12, :cond_2

    .line 617
    const-string v14, "WallpaperBuilder"

    const-string v15, "Corrupt file.. Need to check !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 660
    if-eqz v6, :cond_0

    .line 661
    :try_start_2
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 665
    :goto_0
    if-eqz v7, :cond_1

    .line 666
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    :goto_1
    move-object v5, v6

    .line 674
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v12    # "length":I
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_2
    return-void

    .line 663
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v12    # "length":I
    :cond_0
    const-string v14, "WallpaperBuilder"

    const-string v15, "fis is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 669
    :catch_0
    move-exception v2

    .line 670
    .local v2, "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "file write err "

    invoke-static {v14, v15, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 668
    .end local v2    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_3
    const-string v14, "WallpaperBuilder"

    const-string v15, "fos is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 622
    :cond_2
    move-object/from16 v13, p1

    .line 623
    :try_start_4
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 624
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_3

    .line 625
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v10

    .line 627
    .local v10, "isFileCreated":Z
    const-string v14, "WallpaperBuilder"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "New file created : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    .end local v10    # "isFileCreated":Z
    :cond_3
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Ljava/io/File;->setReadable(ZZ)Z

    .line 632
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v3, v14, v15}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v11

    .line 634
    .local v11, "isWritable":Z
    const-string v14, "WallpaperBuilder"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "isWritable : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 637
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .local v8, "fos":Ljava/io/FileOutputStream;
    :try_start_5
    new-array v1, v12, [B

    .line 638
    .local v1, "buffer":[B
    :goto_3
    invoke-virtual {v6, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v12

    if-lez v12, :cond_4

    .line 639
    const/4 v14, 0x0

    invoke-virtual {v8, v1, v14, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_3

    .line 654
    .end local v1    # "buffer":[B
    :catch_1
    move-exception v2

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .line 655
    .end local v3    # "file":Ljava/io/File;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "isWritable":Z
    .end local v12    # "length":I
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_4
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 660
    if-eqz v5, :cond_8

    .line 661
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 665
    :goto_5
    if-eqz v7, :cond_9

    .line 666
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    .line 669
    :catch_2
    move-exception v2

    .line 670
    .local v2, "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "file write err "

    invoke-static {v14, v15, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v3    # "file":Ljava/io/File;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "isWritable":Z
    .restart local v12    # "length":I
    :cond_4
    move-object v7, v8

    .line 644
    .end local v1    # "buffer":[B
    .end local v3    # "file":Ljava/io/File;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "isWritable":Z
    .end local v12    # "length":I
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    :cond_5
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "lockscreen_wallpaper_path"

    move-object/from16 v0, p1

    invoke-static {v14, v15, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 645
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "lockscreen_wallpaper"

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 648
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "lockscreen_wallpaper_path_ripple"

    move-object/from16 v0, p1

    invoke-static {v14, v15, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 651
    new-instance v9, Landroid/content/Intent;

    const-string v14, "com.sec.android.gallery3d.LOCKSCREEN_IMAGE_CHANGED"

    invoke-direct {v9, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 652
    .local v9, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 660
    if-eqz v6, :cond_6

    .line 661
    :try_start_9
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 665
    :goto_6
    if-eqz v7, :cond_7

    .line 666
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    :goto_7
    move-object v5, v6

    .line 671
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 663
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_6
    const-string v14, "WallpaperBuilder"

    const-string v15, "fis is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_6

    .line 669
    :catch_3
    move-exception v2

    .line 670
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "file write err "

    invoke-static {v14, v15, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v5, v6

    .line 672
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2

    .line 668
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_7
    :try_start_a
    const-string v14, "WallpaperBuilder"

    const-string v15, "fos is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    goto :goto_7

    .line 663
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .end local v9    # "intent":Landroid/content/Intent;
    .local v2, "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :cond_8
    :try_start_b
    const-string v14, "WallpaperBuilder"

    const-string v15, "fis is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 668
    :cond_9
    const-string v14, "WallpaperBuilder"

    const-string v15, "fos is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    goto/16 :goto_2

    .line 656
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v2

    .line 657
    .local v2, "e":Ljava/lang/Exception;
    :goto_8
    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 660
    if-eqz v5, :cond_a

    .line 661
    :try_start_d
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 665
    :goto_9
    if-eqz v7, :cond_b

    .line 666
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5

    goto/16 :goto_2

    .line 669
    :catch_5
    move-exception v2

    .line 670
    .local v2, "e":Ljava/io/IOException;
    const-string v14, "WallpaperBuilder"

    const-string v15, "file write err "

    invoke-static {v14, v15, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 663
    .local v2, "e":Ljava/lang/Exception;
    :cond_a
    :try_start_e
    const-string v14, "WallpaperBuilder"

    const-string v15, "fis is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_9

    .line 668
    :cond_b
    const-string v14, "WallpaperBuilder"

    const-string v15, "fos is NULL !!"

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    goto/16 :goto_2

    .line 659
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    .line 660
    :goto_a
    if-eqz v5, :cond_c

    .line 661
    :try_start_f
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 665
    :goto_b
    if-eqz v7, :cond_d

    .line 666
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    .line 671
    :goto_c
    throw v14

    .line 663
    :cond_c
    :try_start_10
    const-string v15, "WallpaperBuilder"

    const-string v16, "fis is NULL !!"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6

    goto :goto_b

    .line 669
    :catch_6
    move-exception v2

    .line 670
    .local v2, "e":Ljava/io/IOException;
    const-string v15, "WallpaperBuilder"

    const-string v16, "file write err "

    move-object/from16 v0, v16

    invoke-static {v15, v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_c

    .line 668
    .end local v2    # "e":Ljava/io/IOException;
    :cond_d
    :try_start_11
    const-string v15, "WallpaperBuilder"

    const-string v16, "fos is NULL !!"

    invoke-static/range {v15 .. v16}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_6

    goto :goto_c

    .line 659
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v14

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "isWritable":Z
    .restart local v12    # "length":I
    :catchall_2
    move-exception v14

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_a

    .line 656
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "isWritable":Z
    .end local v12    # "length":I
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v2

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "isWritable":Z
    .restart local v12    # "length":I
    :catch_8
    move-exception v2

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto :goto_8

    .line 654
    .end local v3    # "file":Ljava/io/File;
    .end local v11    # "isWritable":Z
    .end local v12    # "length":I
    :catch_9
    move-exception v2

    goto/16 :goto_4

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :catch_a
    move-exception v2

    move-object v5, v6

    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_4
.end method

.method public static setRestoreValues(Landroid/content/Context;Lorg/json/JSONObject;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    const/4 v7, 0x1

    .line 529
    const-string v4, "WallpaperBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setRestoreValues - json : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    const-string v5, "value"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "HOMESCREEN"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 534
    .local v1, "item":Lorg/json/JSONObject;
    if-eqz v1, :cond_1

    .line 535
    const-string v4, "name"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 537
    .local v3, "name":Ljava/lang/String;
    const-string v4, "Home_Screen_WallPaper"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 539
    const-string v4, "value"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "/data/system/users/0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 541
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->restoreHomeScreenWallPaper(Landroid/content/Context;)V

    .line 553
    :cond_0
    :goto_0
    const-string v4, "lockscreen_wallpaper"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 554
    const-string v4, "lockscreen_wallpaper"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 555
    .local v2, "lockscreen_type":I
    const-string v4, "WallpaperBuilder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Lockscreen set Lockscreen type : =  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const-string v4, "lockscreen_wallpaper"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v7, :cond_3

    .line 560
    const-string v4, "value"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->restoreLockScreenWallPaper(Landroid/content/Context;Ljava/lang/String;)V

    .line 577
    .end local v1    # "item":Lorg/json/JSONObject;
    .end local v2    # "lockscreen_type":I
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    :goto_1
    return v7

    .line 545
    .restart local v1    # "item":Lorg/json/JSONObject;
    .restart local v3    # "name":Ljava/lang/String;
    :cond_2
    const-string v4, "value"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->restoreHomeScreenLiveWallPaper(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 574
    .end local v1    # "item":Lorg/json/JSONObject;
    .end local v3    # "name":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "WallpaperBuilder"

    const-string v5, "setRestoreValues err"

    invoke-static {v4, v5, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 562
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v1    # "item":Lorg/json/JSONObject;
    .restart local v2    # "lockscreen_type":I
    .restart local v3    # "name":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v4, "lockscreen_wallpaper"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_1

    .line 564
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "lockscreen_wallpaper"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 566
    const-string v4, "WallpaperBuilder"

    const-string v5, "systemSet : lockscreen_wallpaper = 0"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 798
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->clear()V

    .line 799
    return-void
.end method

.method public getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "start"    # I
    .param p4, "maxCount"    # I
    .param p5, "maxSize"    # J
    .param p7, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 79
    .local p2, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v11, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    if-nez p3, :cond_1

    .line 83
    sget-object v12, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 84
    .local v5, "file":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v2, "deleted":Ljava/io/File;
    const-string v12, "WallpaperBuilder"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Delete file : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", size : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", delete : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 88
    .end local v2    # "deleted":Ljava/io/File;
    .end local v5    # "file":Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->backedUpWallPaper(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v10

    .line 90
    .local v10, "itemWallpaper":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->backedLockScreen(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v9

    .line 92
    .local v9, "itemLockScreen":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    const/4 v6, 0x0

    .line 96
    .local v6, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v7, Ljava/io/FileWriter;

    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p7

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-direct {v7, v12, v13}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    .end local v6    # "fw":Ljava/io/FileWriter;
    .local v7, "fw":Ljava/io/FileWriter;
    :try_start_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v13

    invoke-virtual {v13}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v9}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v13

    invoke-virtual {v13}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 103
    if-eqz v7, :cond_1

    .line 105
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileWriter;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 112
    :goto_1
    :try_start_3
    invoke-virtual {v7}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 123
    .end local v7    # "fw":Ljava/io/FileWriter;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "itemLockScreen":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v10    # "itemWallpaper":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_1
    :goto_2
    return-object v11

    .line 106
    .restart local v7    # "fw":Ljava/io/FileWriter;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "itemLockScreen":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .restart local v10    # "itemWallpaper":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :catch_0
    move-exception v3

    .line 108
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 113
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 115
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 99
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    :catch_2
    move-exception v4

    .line 100
    .local v4, "e1":Ljava/io/IOException;
    :goto_3
    :try_start_4
    new-instance v12, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v13, 0x13a

    invoke-direct {v12, v13, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v12
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 103
    .end local v4    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v12

    :goto_4
    if-eqz v6, :cond_2

    .line 105
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileWriter;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 112
    :goto_5
    :try_start_6
    invoke-virtual {v6}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 116
    :cond_2
    :goto_6
    throw v12

    .line 106
    :catch_3
    move-exception v3

    .line 108
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 113
    .end local v3    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 115
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 103
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v12

    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_4

    .line 99
    .end local v6    # "fw":Ljava/io/FileWriter;
    .restart local v7    # "fw":Ljava/io/FileWriter;
    :catch_5
    move-exception v4

    move-object v6, v7

    .end local v7    # "fw":Ljava/io/FileWriter;
    .restart local v6    # "fw":Ljava/io/FileWriter;
    goto :goto_3
.end method

.method public getOrderByColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 780
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    .line 781
    .local v0, "projection":[Ljava/lang/String;
    return-object v0
.end method

.method public getSourceProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWhere()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public postOperationOnBackup(Landroid/content/Context;Z)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSuccess"    # Z

    .prologue
    .line 785
    const-string v3, "WallpaperBuilder"

    const-string v4, "postOperationOnRestore"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    sget-object v3, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperContract$Wallpaper;->FILE_UPLOAD_LIST:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 788
    .local v1, "file":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 789
    .local v0, "deleted":Ljava/io/File;
    const-string v3, "WallpaperBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete file : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", delete : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 792
    .end local v0    # "deleted":Ljava/io/File;
    .end local v1    # "file":Ljava/lang/String;
    :cond_0
    if-eqz p2, :cond_1

    .line 793
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperProvider$WallpaperDataManager;->commit()V

    .line 795
    :cond_1
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    const-string v0, "WallpaperBuilder"

    const-string v1, "preOperationOnRestore"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 129
    .local p3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p4, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x1

    .line 130
    .local v2, "result":Z
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 131
    .local v1, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->setRestoreValues(Landroid/content/Context;Lorg/json/JSONObject;)Z

    move-result v3

    and-int/2addr v2, v3

    .line 132
    goto :goto_0

    .line 134
    .end local v1    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x66

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v5

    int-to-float v5, v5

    invoke-interface {p2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 135
    const-string v3, "1"

    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    const-string v3, "2"

    invoke-interface {p4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    return v2
.end method
