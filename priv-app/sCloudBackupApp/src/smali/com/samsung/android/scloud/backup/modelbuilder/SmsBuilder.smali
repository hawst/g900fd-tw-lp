.class public Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;
.super Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;
.source "SmsBuilder.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SmsBuilder"


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "mModel"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 77
    return-void
.end method

.method private getDarftAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadId"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 556
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->CONVERSATION_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 558
    .local v1, "adress_uri":Landroid/net/Uri;
    const-string v0, "recipients"

    invoke-static {v1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 559
    const/4 v6, 0x0

    .line 561
    .local v6, "addrCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 563
    if-eqz v6, :cond_0

    .line 564
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 565
    const-string v0, "recipient_ids"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 566
    .local v9, "index":I
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 567
    .local v10, "recipient_id":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 568
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->CANONICAL_ADDRESS_URI:Landroid/net/Uri;

    invoke-static {v0, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 570
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 572
    if-eqz v6, :cond_0

    .line 573
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 574
    const-string v0, "address"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 575
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 576
    .local v7, "address":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    .end local v7    # "address":Ljava/lang/String;
    .end local v9    # "index":I
    .end local v10    # "recipient_id":Ljava/lang/String;
    :goto_0
    return-object v7

    :cond_0
    move-object v7, v11

    .line 580
    goto :goto_0

    .line 581
    :catch_0
    move-exception v8

    .line 582
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "SmsBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception recieved while getting draft address :-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 585
    if-eqz v6, :cond_1

    .line 586
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v7, v11

    .line 588
    goto :goto_0
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 615
    return-void
.end method

.method public getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;
    .locals 41
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "start"    # I
    .param p4, "maxCount"    # I
    .param p5, "maxSize"    # J
    .param p7, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    .local p2, "serverkey":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "SmsBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM() is called!!! :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v8}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v21, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/16 v24, 0x0

    .line 108
    .local v24, "phnDB":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 110
    .local v16, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v17, Ljava/io/FileWriter;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p7

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v7}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    .end local v16    # "fw":Ljava/io/FileWriter;
    .local v17, "fw":Ljava/io/FileWriter;
    :try_start_1
    const-string v4, "["

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 113
    const-wide/16 v34, 0x0

    .line 114
    .local v34, "totalLength":J
    const-wide/16 v10, 0x0

    .line 115
    .local v10, "count":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getSourceProjection()[Ljava/lang/String;

    move-result-object v6

    .line 116
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v4}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 117
    .local v5, "oemUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getOrderByColumnName()Ljava/lang/String;

    move-result-object v25

    .line 118
    .local v25, "sortOrder":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getIDKey(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getWhereKey([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 119
    .local v38, "whereKey":Ljava/lang/String;
    const/16 v23, 0x1f4

    .line 120
    .local v23, "limit":I
    const/16 v19, 0x1

    .line 122
    .local v19, "isNext":Z
    :goto_0
    if-eqz v19, :cond_0

    .line 124
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getSelectionID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v39, " ASC"

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v39, " LIMIT "

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v39, " OFFSET "

    move-object/from16 v0, v39

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 138
    if-nez v24, :cond_3

    .line 140
    const/16 v19, 0x0

    .line 226
    :cond_0
    :goto_1
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 227
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V

    .line 228
    const-string v4, "SmsBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM() is Done : return Item count is :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 237
    if-eqz v24, :cond_1

    .line 238
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 240
    :cond_1
    if-eqz v17, :cond_2

    .line 242
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 248
    :cond_2
    :goto_2
    return-object v21

    .line 144
    :cond_3
    :try_start_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_4

    .line 145
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 146
    const/16 v19, 0x0

    .line 147
    goto :goto_1

    .line 149
    :cond_4
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int p3, p3, v4

    .line 150
    :goto_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 152
    move/from16 v0, p4

    int-to-long v8, v0

    cmp-long v4, v10, v8

    if-ltz v4, :cond_7

    .line 153
    if-eqz v24, :cond_5

    .line 154
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 155
    const/16 v24, 0x0

    .line 157
    :cond_5
    const-string v4, "SmsBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM() is Done : return Item count is :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 161
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 237
    if-eqz v24, :cond_6

    .line 238
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 240
    :cond_6
    if-eqz v17, :cond_2

    .line 242
    :try_start_4
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 243
    :catch_0
    move-exception v14

    .line 245
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 165
    .end local v14    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_5
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v12

    .line 166
    .local v12, "data":Lorg/json/JSONObject;
    const-string v4, "type"

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 167
    .local v33, "typeIndex":I
    move-object/from16 v0, v24

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    .line 168
    .local v36, "type":J
    const-wide/16 v8, 0x3

    cmp-long v4, v36, v8

    if-nez v4, :cond_8

    .line 169
    const-string v4, "thread_id"

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 171
    .local v28, "thread_id":Ljava/lang/String;
    const-string v4, "address"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getDarftAddress(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 173
    .end local v28    # "thread_id":Ljava/lang/String;
    :cond_8
    const-string v4, "_id"

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 177
    .local v18, "idIndex":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BACKUP_SMS_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v24

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 183
    .local v22, "key":Ljava/lang/String;
    const-string v4, "transaction_id"

    const-string v7, "0"

    invoke-virtual {v12, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 185
    new-instance v32, Lorg/json/JSONObject;

    invoke-direct/range {v32 .. v32}, Lorg/json/JSONObject;-><init>()V

    .line 186
    .local v32, "totalData":Lorg/json/JSONObject;
    const-string v4, "value"

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v32

    invoke-virtual {v0, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 187
    const-string v4, "date"

    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 188
    .local v13, "dateIndex":I
    move-object/from16 v0, v24

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 189
    .local v30, "timeStamp":J
    const-wide/16 v8, 0x0

    cmp-long v4, v30, v8

    if-gtz v4, :cond_9

    .line 190
    const-wide v30, 0xe8d4a51000L

    .line 191
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getLength(J)I

    move-result v4

    const/16 v7, 0xd

    if-ge v4, v7, :cond_a

    .line 192
    const-wide/16 v8, 0xa

    mul-long v30, v30, v8

    goto :goto_4

    .line 195
    :cond_a
    const-string v4, "timestamp"

    move-object/from16 v0, v32

    move-wide/from16 v1, v30

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 196
    const-string v4, "key"

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    const-wide/16 v26, 0x0

    .line 198
    .local v26, "subLength":J
    invoke-virtual/range {v32 .. v32}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v29

    .line 199
    .local v29, "toStr":Ljava/lang/String;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v26, v0

    .line 200
    add-long v8, v34, v26

    cmp-long v4, v8, p5

    if-ltz v4, :cond_c

    .line 201
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 202
    const/16 v24, 0x0

    .line 203
    const-string v4, "SmsBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM() is Done : return Item count is :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 207
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 237
    if-eqz v24, :cond_b

    .line 238
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 240
    :cond_b
    if-eqz v17, :cond_2

    .line 242
    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    .line 243
    :catch_1
    move-exception v14

    .line 245
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 210
    .end local v14    # "e":Ljava/io/IOException;
    :cond_c
    add-long v34, v34, v26

    .line 212
    :try_start_7
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_e

    const-wide/16 v8, 0x0

    cmp-long v4, v10, v8

    if-lez v4, :cond_e

    .line 213
    :cond_d
    const-string v4, ","

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 215
    :cond_e
    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 216
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->flush()V

    .line 217
    new-instance v20, Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 218
    .local v20, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    move-object/from16 v0, v20

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 219
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    const-wide/16 v8, 0x1

    add-long/2addr v10, v8

    .line 222
    goto/16 :goto_3

    .line 223
    .end local v12    # "data":Lorg/json/JSONObject;
    .end local v13    # "dateIndex":I
    .end local v18    # "idIndex":I
    .end local v20    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v22    # "key":Ljava/lang/String;
    .end local v26    # "subLength":J
    .end local v29    # "toStr":Ljava/lang/String;
    .end local v30    # "timeStamp":J
    .end local v32    # "totalData":Lorg/json/JSONObject;
    .end local v33    # "typeIndex":I
    .end local v36    # "type":J
    :cond_f
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 224
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 243
    :catch_2
    move-exception v14

    .line 245
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 232
    .end local v5    # "oemUri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":J
    .end local v14    # "e":Ljava/io/IOException;
    .end local v17    # "fw":Ljava/io/FileWriter;
    .end local v19    # "isNext":Z
    .end local v23    # "limit":I
    .end local v25    # "sortOrder":Ljava/lang/String;
    .end local v34    # "totalLength":J
    .end local v38    # "whereKey":Ljava/lang/String;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :catch_3
    move-exception v15

    .line 233
    .local v15, "e1":Ljava/io/IOException;
    :goto_5
    :try_start_8
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x13a

    invoke-direct {v4, v7, v15}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 237
    .end local v15    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_6
    if-eqz v24, :cond_10

    .line 238
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 240
    :cond_10
    if-eqz v16, :cond_11

    .line 242
    :try_start_9
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 246
    :cond_11
    :goto_7
    throw v4

    .line 234
    :catch_4
    move-exception v14

    .line 235
    .local v14, "e":Lorg/json/JSONException;
    :goto_8
    :try_start_a
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x130

    invoke-direct {v4, v7, v14}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 243
    .end local v14    # "e":Lorg/json/JSONException;
    :catch_5
    move-exception v14

    .line 245
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 237
    .end local v14    # "e":Ljava/io/IOException;
    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v17    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object/from16 v16, v17

    .end local v17    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    goto :goto_6

    .line 234
    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v17    # "fw":Ljava/io/FileWriter;
    :catch_6
    move-exception v14

    move-object/from16 v16, v17

    .end local v17    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    goto :goto_8

    .line 232
    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v17    # "fw":Ljava/io/FileWriter;
    :catch_7
    move-exception v15

    move-object/from16 v16, v17

    .end local v17    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    goto :goto_5
.end method

.method public getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "recipient"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 513
    if-nez p2, :cond_0

    move-wide v4, v8

    .line 547
    :goto_0
    return-wide v4

    .line 517
    :cond_0
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    .line 519
    .local v2, "ID_PROJECTION":[Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->THREAD_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    .line 521
    .local v7, "uriBuilder":Landroid/net/Uri$Builder;
    const-string v0, "recipient"

    invoke-virtual {v7, v0, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 523
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 525
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 529
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 533
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 535
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 541
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v4, v8

    .line 547
    goto :goto_0

    .line 541
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getOrderByColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 597
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    .line 598
    .local v0, "projection":[Ljava/lang/String;
    return-object v0
.end method

.method public getSourceProjection()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWhere()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method public postOperationOnBackup(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSuccess"    # Z

    .prologue
    .line 607
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 553
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->setSMSMode(Landroid/content/Context;)V

    .line 554
    return-void
.end method

.method public putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    .locals 34
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 381
    .local p3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p4, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v29, "SmsBuilder"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "putItemToOEM() is called :"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v22

    .line 384
    .local v22, "oemUri":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    .line 385
    .local v20, "mResolver":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 386
    .local v4, "CV":Landroid/content/ContentValues;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v23, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 389
    .local v18, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 390
    if-nez v18, :cond_2

    .line 391
    const-string v29, "SmsBuilder"

    const-string v32, "putDataToOEM(): item is null"

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    .end local v18    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_0
    const/16 v16, 0x0

    .line 434
    .local v16, "insertedResult":[Landroid/content/ContentProviderResult;
    const/16 v26, 0x0

    .line 436
    .local v26, "start":I
    const/16 v21, 0xfa

    .line 438
    .local v21, "maxBatchSize":I
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v25

    .line 440
    .local v25, "size":I
    new-instance v27, Ljava/util/ArrayList;

    const/16 v29, 0xfa

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 442
    .local v27, "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v12, 0x0

    .line 443
    .end local v13    # "i$":Ljava/util/Iterator;
    .local v12, "end":I
    :goto_1
    move/from16 v0, v25

    move/from16 v1, v26

    if-le v0, v1, :cond_8

    .line 444
    move/from16 v0, v26

    add-int/lit16 v12, v0, 0xfa

    .line 445
    move/from16 v0, v25

    if-ge v0, v12, :cond_1

    .line 446
    move/from16 v12, v25

    .line 448
    :cond_1
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->clear()V

    .line 449
    move-object/from16 v0, v23

    move/from16 v1, v26

    invoke-virtual {v0, v1, v12}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 451
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemAuthority()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v16

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v29

    const/16 v32, 0x66

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v33

    move/from16 v0, v33

    int-to-float v0, v0

    move/from16 v33, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v29

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 460
    if-eqz v16, :cond_7

    .line 461
    move-object/from16 v8, v16

    .local v8, "arr$":[Landroid/content/ContentProviderResult;
    array-length v0, v8

    move/from16 v19, v0

    .local v19, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_2
    move/from16 v0, v19

    if-ge v13, v0, :cond_6

    aget-object v24, v8, v13

    .line 462
    .local v24, "result":Landroid/content/ContentProviderResult;
    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    move-object/from16 v17, v0

    .line 463
    .local v17, "insertedUri":Landroid/net/Uri;
    invoke-static/range {v17 .. v17}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v14

    .line 464
    .local v14, "insertedId":J
    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p4

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 461
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 394
    .end local v8    # "arr$":[Landroid/content/ContentProviderResult;
    .end local v12    # "end":I
    .end local v14    # "insertedId":J
    .end local v16    # "insertedResult":[Landroid/content/ContentProviderResult;
    .end local v17    # "insertedUri":Landroid/net/Uri;
    .end local v19    # "len$":I
    .end local v21    # "maxBatchSize":I
    .end local v24    # "result":Landroid/content/ContentProviderResult;
    .end local v25    # "size":I
    .end local v26    # "start":I
    .end local v27    # "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local v13, "i$":Ljava/util/Iterator;
    .restart local v18    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_2
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v10

    .line 395
    .local v10, "data":Lorg/json/JSONObject;
    new-instance v28, Lorg/json/JSONObject;

    const-string v29, "value"

    move-object/from16 v0, v29

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    invoke-direct/range {v28 .. v29}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 397
    .local v28, "totalData":Lorg/json/JSONObject;
    invoke-static/range {v28 .. v28}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v9

    .line 399
    .local v9, "cursor":Landroid/content/ContentValues;
    const-string v29, "type"

    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    .line 400
    .local v30, "type":J
    const-wide/16 v32, 0x3

    cmp-long v29, v30, v32

    if-nez v29, :cond_4

    .line 401
    const-string v29, "address"

    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v6

    .line 403
    .local v6, "ThreadId":J
    const-wide/16 v32, -0x1

    cmp-long v29, v6, v32

    if-eqz v29, :cond_3

    .line 404
    const-string v29, "thread_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 419
    .end local v6    # "ThreadId":J
    :goto_3
    const-string v29, "date"

    const-string v32, "date"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 420
    const-string v29, "reply_path_present"

    const-string v32, "reply_path_present"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 421
    const-string v29, "read"

    const-string v32, "read"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    const-string v29, "body"

    const-string v32, "body"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v29, "locked"

    const-string v32, "locked"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 424
    const-string v29, "type"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    const-string v29, "status"

    const-string v32, "status"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 426
    const-string v29, "pri"

    const-string v32, "pri"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 427
    const-string v29, "reserved"

    const-string v32, "reserved"

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 428
    invoke-static/range {v22 .. v22}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v29

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 406
    .restart local v6    # "ThreadId":J
    :cond_3
    const-string v29, "SmsBuilder"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "putItemToOEM() is failed : THREAD_ID - "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 411
    .end local v6    # "ThreadId":J
    :cond_4
    const-string v29, "address"

    move-object/from16 v0, v29

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 412
    .local v5, "address":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 413
    const-string v29, "address"

    move-object/from16 v0, v29

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 415
    :cond_5
    const-string v29, "SmsBuilder"

    const-string v32, "putItemToOEM() is failed : ADDRESS is null"

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 454
    .end local v5    # "address":Ljava/lang/String;
    .end local v9    # "cursor":Landroid/content/ContentValues;
    .end local v10    # "data":Lorg/json/JSONObject;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v18    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v28    # "totalData":Lorg/json/JSONObject;
    .end local v30    # "type":J
    .restart local v12    # "end":I
    .restart local v16    # "insertedResult":[Landroid/content/ContentProviderResult;
    .restart local v21    # "maxBatchSize":I
    .restart local v25    # "size":I
    .restart local v26    # "start":I
    .restart local v27    # "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catch_0
    move-exception v11

    .line 455
    .local v11, "e":Landroid/os/RemoteException;
    const/16 v29, 0x0

    .line 478
    .end local v11    # "e":Landroid/os/RemoteException;
    :goto_4
    return v29

    .line 456
    :catch_1
    move-exception v11

    .line 457
    .local v11, "e":Landroid/content/OperationApplicationException;
    const/16 v29, 0x0

    goto :goto_4

    .line 466
    .end local v11    # "e":Landroid/content/OperationApplicationException;
    .restart local v8    # "arr$":[Landroid/content/ContentProviderResult;
    .local v13, "i$":I
    .restart local v19    # "len$":I
    :cond_6
    const-string v29, "SmsBuilder"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "putItemToOEM() is Done !!!  "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " inserted item size : "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    move/from16 v26, v12

    goto/16 :goto_1

    .line 471
    .end local v8    # "arr$":[Landroid/content/ContentProviderResult;
    .end local v13    # "i$":I
    .end local v19    # "len$":I
    :cond_7
    const-string v29, "SmsBuilder"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "putItemToOEM() FAIL :"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, " : Not inserted data "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/16 v29, 0x0

    goto :goto_4

    .line 478
    :cond_8
    const/16 v29, 0x1

    goto :goto_4
.end method
