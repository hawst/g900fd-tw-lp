.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupConstants$Action;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Action"
.end annotation


# static fields
.field public static final OPERATION_CANCEL:Ljava/lang/String; = "OPERATION_CANCEL"

.field public static final OPERATION_PAUSE:Ljava/lang/String; = "OPERATION_PAUSE"

.field public static final OPERATION_RESUME:Ljava/lang/String; = "OPERATION_RESUME"

.field public static final REQUEST_BACKUP:Ljava/lang/String; = "REQUEST_BACKUP"

.field public static final REQUEST_BACKUP_CLEARED:Ljava/lang/String; = "REQUEST_BACKUP_CLEARED"

.field public static final REQUEST_BACKUP_DELETE:Ljava/lang/String; = "REQUEST_BACKUP_DELETE"

.field public static final REQUEST_GETDETAIL:Ljava/lang/String; = "REQUEST_GETDETAIL"

.field public static final REQUEST_GETUSAGE:Ljava/lang/String; = "REQUEST_GETUSAGE"

.field public static final REQUEST_RESTORE:Ljava/lang/String; = "REQUEST_RESTORE"
