.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showSelectDeviceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1441
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    const/4 v6, 0x0

    .line 1443
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 1444
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v3

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)I

    .line 1445
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceIndex(I)V

    .line 1447
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v4

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1302(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1449
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v3

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1402(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1450
    const-string v2, "ManualRestoreActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[processRestoreReady] mBackupDetailsMap : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1452
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    invoke-virtual {v2, v1, v6, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto :goto_0

    .line 1453
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 1454
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 1455
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    .line 1457
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method
