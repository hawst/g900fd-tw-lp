.class public Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;
.super Ljava/lang/Object;
.source "BackupDetails.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mClientDeviceID:Ljava/lang/String;

.field private mDeviceID:Ljava/lang/String;

.field private mFlag:Ljava/lang/Boolean;

.field private mLatestBackupDate:Ljava/lang/String;

.field private mModelName:Ljava/lang/String;

.field private mSize:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mDeviceID:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mClientDeviceID:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mLatestBackupDate:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mSize:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mModelName:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "ClientDeviceID"    # Ljava/lang/String;
    .param p3, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mDeviceID:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mClientDeviceID:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mFlag:Ljava/lang/Boolean;

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceID"    # Ljava/lang/String;
    .param p2, "clientDevicdeID"    # Ljava/lang/String;
    .param p3, "lastestBackupDate"    # Ljava/lang/String;
    .param p4, "size"    # Ljava/lang/String;
    .param p5, "modelName"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mDeviceID:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mClientDeviceID:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mLatestBackupDate:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mSize:Ljava/lang/String;

    .line 22
    iput-object p5, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mModelName:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getClientDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mClientDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getFlag()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mFlag:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mModelName:Ljava/lang/String;

    return-object v0
.end method

.method public latestBackupDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mLatestBackupDate:Ljava/lang/String;

    return-object v0
.end method

.method public setFlag(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "flag"    # Ljava/lang/Boolean;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mFlag:Ljava/lang/Boolean;

    .line 74
    return-void
.end method

.method public size()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mSize:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mDeviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mClientDeviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mLatestBackupDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mSize:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->mModelName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    return-void
.end method
