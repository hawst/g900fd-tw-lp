.class public Lcom/samsung/android/scloud/backup/server/BNRServiceManager;
.super Ljava/lang/Object;
.source "BNRServiceManager.java"


# static fields
.field private static LAST_API:Ljava/lang/String; = null

.field private static final MAX_KEY_POST_COUNT:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "BNRServiceManager"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static backupClear(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 341
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupClear!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 345
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 346
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 350
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=clear&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 352
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 353
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string v3, "620"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 358
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "application/json;Charset=utf-8"

    invoke-static {v3, v2, v4, v1, v5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 359
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    new-instance v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager$1;

    invoke-direct {v3, p2}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 365
    return-void
.end method

.method public static backupClearCommit(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 320
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupClearCommit!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 324
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 325
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=clearcommit&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 330
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 331
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    const-string v3, "613"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 336
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "application/json;Charset=utf-8"

    invoke-static {v3, v2, v4, v1, v5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 337
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 338
    return-void
.end method

.method public static backupCommit(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 298
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupCommit!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 302
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 303
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=commit&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 308
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 309
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    const-string v3, "613"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 314
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "application/json;Charset=utf-8"

    invoke-static {v3, v2, v4, v1, v5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 315
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 316
    return-void
.end method

.method public static backupDelete(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;IJLcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "maxCount"    # I
    .param p6, "maxUpload"    # J
    .param p8, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IJ",
            "Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 221
    .local p4, "itemList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v14, "BNRServiceManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "backupDelete!!!!!!!!!! - "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v11

    .line 225
    .local v11, "url":Ljava/lang/String;
    if-nez v11, :cond_0

    .line 226
    const-string v14, "BNRServiceManager"

    const-string v15, "There is NO Base URL."

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    new-instance v14, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v15, 0x131

    invoke-direct {v14, v15}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v14

    .line 230
    :cond_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/bnr/?action=delete&"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 232
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 233
    .local v10, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v14, "client_did"

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-interface {v10, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    const-string v14, "ctid"

    move-object/from16 v0, p2

    invoke-interface {v10, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    const-string v14, "612"

    sput-object v14, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 238
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8}, Lorg/json/JSONArray;-><init>()V

    .line 239
    .local v8, "jsonArr":Lorg/json/JSONArray;
    const-wide/16 v12, 0x5

    .line 240
    .local v12, "size":J
    const/4 v2, 0x0

    .line 241
    .local v2, "count":I
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 243
    .local v6, "item":Ljava/lang/String;
    :try_start_0
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14}, Lorg/json/JSONObject;-><init>()V

    const-string v15, "key"

    invoke-virtual {v14, v15, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v14

    const-string v15, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-virtual/range {v14 .. v17}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v7

    .line 245
    .local v7, "json":Lorg/json/JSONObject;
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    int-to-long v14, v14

    add-long/2addr v12, v14

    .line 246
    cmp-long v14, v12, p6

    if-gez v14, :cond_1

    move/from16 v0, p5

    if-lt v2, v0, :cond_2

    .line 247
    :cond_1
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "application/json;Charset=utf-8"

    move-object/from16 v0, v16

    invoke-static {v14, v11, v15, v10, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 248
    .local v4, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p8

    invoke-static {v4, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 250
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 251
    .end local v8    # "jsonArr":Lorg/json/JSONArray;
    .local v9, "jsonArr":Lorg/json/JSONArray;
    const-wide/16 v12, 0x5

    .line 252
    const/4 v2, 0x0

    move-object v8, v9

    .line 255
    .end local v4    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v9    # "jsonArr":Lorg/json/JSONArray;
    .restart local v8    # "jsonArr":Lorg/json/JSONArray;
    :cond_2
    invoke-virtual {v8, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 256
    add-int/lit8 v2, v2, 0x1

    .line 257
    const-string v14, "BNRServiceManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "payload put["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 259
    .end local v7    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 260
    .local v3, "e":Lorg/json/JSONException;
    const-string v14, "BNRServiceManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "err key : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 261
    new-instance v14, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v15, 0x130

    invoke-direct {v14, v15, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v14

    .line 268
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v6    # "item":Ljava/lang/String;
    :cond_3
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "application/json;Charset=utf-8"

    move-object/from16 v0, v16

    invoke-static {v14, v11, v15, v10, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 269
    .restart local v4    # "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p8

    invoke-static {v4, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 272
    return-void
.end method

.method public static backupDetailSet(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 93
    const-string v11, "BNRServiceManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "backupDetailSet!!!!!!!!!! - "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 97
    .local v8, "time":J
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 99
    .local v2, "detailJson":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 101
    .local v6, "jsonArr":Lorg/json/JSONArray;
    :try_start_0
    const-string v11, "name"

    sget-object v12, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 102
    const-string v11, "date"

    invoke-virtual {v2, v11, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 103
    const-string v11, "operation"

    const/4 v12, 0x3

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 105
    const-string v11, "status"

    const/4 v12, 0x2

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 108
    const-string v11, "software_version"

    sget-object v12, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 109
    const-string v11, "android_version"

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 110
    const-string v11, "did"

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 116
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 117
    .local v5, "json":Lorg/json/JSONObject;
    const-string v11, "key"

    move-object/from16 v0, p3

    invoke-interface {v0, p0}, Lcom/samsung/android/scloud/backup/model/IModel;->getDetailKey(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    const-string v11, "timestamp"

    invoke-virtual {v5, v11, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 119
    const-string v11, "value"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 121
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v10

    .line 131
    .local v10, "url":Ljava/lang/String;
    if-nez v10, :cond_0

    .line 132
    const-string v11, "BNRServiceManager"

    const-string v12, "There is NO Base URL."

    invoke-static {v11, v12}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v11, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v12, 0x131

    invoke-direct {v11, v12}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v11

    .line 124
    .end local v5    # "json":Lorg/json/JSONObject;
    .end local v10    # "url":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 125
    .local v3, "e":Lorg/json/JSONException;
    const-string v11, "BNRServiceManager"

    const-string v12, "backupDetailSet json err "

    invoke-static {v11, v12, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 126
    new-instance v11, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v12, 0x130

    invoke-direct {v11, v12, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v11

    .line 136
    .end local v3    # "e":Lorg/json/JSONException;
    .restart local v5    # "json":Lorg/json/JSONObject;
    .restart local v10    # "url":Ljava/lang/String;
    :cond_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/bnr/?action=set&"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p1, v12}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 138
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 139
    .local v7, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v11, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v7, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v11, "ctid"

    move-object/from16 v0, p2

    invoke-interface {v7, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v11, "622"

    sput-object v11, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 144
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "application/json;Charset=utf-8"

    invoke-static {v11, v10, v12, v7, v13}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 145
    .local v4, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p4

    invoke-static {v4, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 146
    return-void
.end method

.method public static backupReady(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 46
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupReady!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 50
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 51
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 55
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/backupready?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 57
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 58
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v3, "610"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 63
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 64
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 66
    return-void
.end method

.method public static backupRollback(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 275
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupRollback!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 280
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 284
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=rollback&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 286
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 287
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    const-string v3, "614"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 292
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "application/json;Charset=utf-8"

    invoke-static {v3, v2, v4, v1, v5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 293
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 294
    return-void
.end method

.method public static backupSet(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "filePath"    # Ljava/lang/String;
    .param p5, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 151
    const-string v3, "BNRServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "backupSet with File!!!!!!!!!! - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v4

    .line 155
    .local v4, "url":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 156
    const-string v3, "BNRServiceManager"

    const-string v6, "There is NO Base URL."

    invoke-static {v3, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x131

    invoke-direct {v3, v6}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 160
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "/bnr/?action=set&"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 162
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 163
    .local v8, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v8, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    const-string v3, "ctid"

    invoke-interface {v8, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-string v3, "611"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 173
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p4

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 176
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x138

    invoke-direct {v3, v6}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 178
    :cond_1
    const/4 v11, 0x0

    .line 180
    .local v11, "stream":Ljava/io/InputStream;
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    .end local v11    # "stream":Ljava/io/InputStream;
    .local v12, "stream":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object/from16 v0, p5

    invoke-direct {v5, v12, v6, v7, v0}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;-><init>(Ljava/io/InputStream;JLcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    .line 182
    .local v5, "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    const-string v3, "application/json;Charset=utf-8"

    invoke-virtual {v5, v3}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->setContentType(Ljava/lang/String;)V

    .line 183
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static/range {v3 .. v8}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/InputStreamEntity;JLjava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v10

    .line 184
    .local v10, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p6

    invoke-static {v10, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 188
    :try_start_2
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 192
    return-void

    .line 188
    :catch_0
    move-exception v2

    .line 189
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x138

    invoke-direct {v3, v6, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3

    .line 185
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "inputEntity":Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
    .end local v10    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    :catch_1
    move-exception v2

    .line 186
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_0
    :try_start_3
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x138

    invoke-direct {v3, v6, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 188
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    :goto_1
    :try_start_4
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 189
    throw v3

    .line 188
    :catch_2
    move-exception v2

    .line 189
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x138

    invoke-direct {v3, v6, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3

    .line 188
    .end local v2    # "e":Ljava/io/IOException;
    .end local v11    # "stream":Ljava/io/InputStream;
    .restart local v12    # "stream":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v11, v12

    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    goto :goto_1

    .line 185
    .end local v11    # "stream":Ljava/io/InputStream;
    .restart local v12    # "stream":Ljava/io/InputStream;
    :catch_3
    move-exception v2

    move-object v11, v12

    .end local v12    # "stream":Ljava/io/InputStream;
    .restart local v11    # "stream":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public static backupSet(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "content"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 196
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupSet with JSON!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 200
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 201
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 205
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=set&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 207
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 208
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    const-string v3, "611"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 213
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/json;Charset=utf-8"

    invoke-static {v3, v2, p4, v1, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 214
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 215
    return-void
.end method

.method public static deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 5
    .param p0, "did"    # Ljava/lang/String;
    .param p1, "cDid"    # Ljava/lang/String;
    .param p2, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p3, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 579
    const-string v2, "BNRServiceManager"

    const-string v3, "814 : [START]"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v2, "BNRServiceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Client Device id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " DID ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 586
    .local v1, "url":Ljava/lang/StringBuilder;
    if-nez v1, :cond_0

    .line 588
    const-string v2, "BNRServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 594
    :cond_0
    const-string v2, "/bnr/?action=clearall&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 596
    invoke-virtual {p2}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getGetApiParamsWithoutCid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 598
    const-string v2, "&did="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 600
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    const-string v2, "&client_did="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 608
    const-string v2, "621"

    sput-object v2, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 610
    const-string v2, "REQUEST_GETDETAIL"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 613
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 616
    const-string v2, "BNRServiceManager"

    const-string v3, "814 : [END] "

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    return-void
.end method

.method public static getDetails(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 392
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDetails!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 397
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 401
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/details?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithoutCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 403
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 404
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const-string v3, "615"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 409
    const-string v3, "REQUEST_GETDETAIL"

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 410
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 411
    return-void
.end method

.method public static getTotalUsageByUser(Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 4
    .param p0, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p1, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 545
    const-string v2, "BNRServiceManager"

    const-string v3, "813 : [START] "

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    .line 550
    .local v1, "url":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 552
    const-string v2, "BNRServiceManager"

    const-string v3, "There is NO Base URL."

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x131

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 558
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/user/quota?view=user&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getGetApiParamsWithoutCid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 560
    const-string v2, "813"

    sput-object v2, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 562
    const-string v2, "REQUEST_GETDETAIL"

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 565
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 568
    const-string v2, "BNRServiceManager"

    const-string v3, "813 : [END] "

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    return-void
.end method

.method public static releaseCleared(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 69
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "releaseCleared!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 74
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 78
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/?action=releasecleared&"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 81
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v3, "622"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 86
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "application/json;Charset=utf-8"

    invoke-static {v3, v2, v4, v1, v5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 87
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 89
    return-void
.end method

.method public static restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "srcDevice"    # Ljava/lang/String;
    .param p5, "item"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 509
    const-string v7, "BNRServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "restoreItems!!!!!!!!!! - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    .line 513
    .local v6, "url":Ljava/lang/String;
    if-nez v6, :cond_0

    .line 514
    const-string v7, "BNRServiceManager"

    const-string v8, "There is NO Base URL."

    invoke-static {v7, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    new-instance v7, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v8, 0x131

    invoke-direct {v7, v8}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v7

    .line 518
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/bnr/restoreitem?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 520
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 521
    .local v5, "sb":Ljava/lang/StringBuilder;
    const-string v7, "client_did"

    const/4 v8, 0x0

    invoke-static {v5, v7, p4, v8}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 522
    const-string v7, "ctid"

    const/4 v8, 0x0

    invoke-static {v5, v7, p2, v8}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 524
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 527
    .local v1, "arr":Lorg/json/JSONArray;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 528
    .local v4, "json":Lorg/json/JSONObject;
    const-string v7, "key"

    move-object/from16 v0, p5

    invoke-virtual {v4, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 529
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    const-string v7, "618"

    sput-object v7, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 538
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "application/json"

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 539
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p6

    invoke-static {v3, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 540
    return-void

    .line 530
    .end local v3    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v4    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 531
    .local v2, "e":Lorg/json/JSONException;
    const-string v7, "BNRServiceManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "restoreItems JSON err : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p5

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 532
    new-instance v7, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v8, 0x130

    invoke-direct {v7, v8, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v7
.end method

.method public static restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/util/Map;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "srcDevice"    # Ljava/lang/String;
    .param p6, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 465
    .local p5, "items":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const-string v9, "BNRServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "restoreItems!!!!!!!!!! - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v8

    .line 469
    .local v8, "url":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 470
    const-string v9, "BNRServiceManager"

    const-string v10, "There is NO Base URL."

    invoke-static {v9, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v10, 0x131

    invoke-direct {v9, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v9

    .line 474
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/bnr/restoreitem?"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 476
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 477
    .local v7, "sb":Ljava/lang/StringBuilder;
    const-string v9, "client_did"

    const/4 v10, 0x0

    move-object/from16 v0, p4

    invoke-static {v7, v9, v0, v10}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 478
    const-string v9, "ctid"

    const/4 v10, 0x0

    invoke-static {v7, v9, p2, v10}, Lcom/samsung/android/scloud/backup/util/UriTool;->addUrlParameter(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 480
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 482
    .local v1, "arr":Lorg/json/JSONArray;
    invoke-interface/range {p5 .. p5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 483
    .local v5, "item":Ljava/lang/String;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v9

    const/16 v10, 0x7d0

    if-le v9, v10, :cond_1

    .line 484
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/json"

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 485
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p6

    invoke-static {v3, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 486
    new-instance v1, Lorg/json/JSONArray;

    .end local v1    # "arr":Lorg/json/JSONArray;
    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 490
    .end local v3    # "httpResponse":Lorg/apache/http/HttpResponse;
    .restart local v1    # "arr":Lorg/json/JSONArray;
    :cond_1
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 491
    .local v6, "json":Lorg/json/JSONObject;
    const-string v9, "key"

    invoke-virtual {v6, v9, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 492
    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 493
    .end local v6    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 494
    .local v2, "e":Lorg/json/JSONException;
    const-string v9, "BNRServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "restoreItems JSON err : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 495
    new-instance v9, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v10, 0x130

    invoke-direct {v9, v10, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v9

    .line 500
    .end local v2    # "e":Lorg/json/JSONException;
    .end local v5    # "item":Ljava/lang/String;
    :cond_2
    const-string v9, "618"

    sput-object v9, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 502
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/json"

    invoke-static {v9, v10, v11, v12}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 503
    .restart local v3    # "httpResponse":Lorg/apache/http/HttpResponse;
    move-object/from16 v0, p6

    invoke-static {v3, v0}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 504
    return-void
.end method

.method public static restoreKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "startKeyId"    # Ljava/lang/String;
    .param p5, "count"    # I
    .param p6, "srcDevice"    # Ljava/lang/String;
    .param p7, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 439
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "restoreKeys!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 443
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 444
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 448
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/restorekeys?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getGetApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 450
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 451
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-interface {v1, v3, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    const-string v3, "start"

    invoke-interface {v1, v3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    const-string v3, "count"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    const-string v3, "617"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 458
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 459
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p7}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 460
    return-void
.end method

.method public static restoreReady(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "srcDevice"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;

    .prologue
    .line 415
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "restoreReady!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 419
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 420
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 424
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/bnr/restoreready?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getPutApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 426
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 427
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "client_did"

    invoke-interface {v1, v3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    const-string v3, "616"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 432
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 433
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 434
    return-void
.end method

.method public static serviceEnd(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;ZLjava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "isSuc"    # Z
    .param p5, "msg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 653
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serviceEnd!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 657
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 658
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 662
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cloud/end?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getGetApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 664
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 665
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "cdid"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    const-string v3, "did"

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 667
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 668
    const-string v4, "result"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p4, :cond_1

    const-string v3, "r"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 672
    const-string v3, "812"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 674
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 675
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    new-instance v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager$2;

    invoke-direct {v3, p2}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager$2;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 681
    return-void

    .line 668
    .end local v0    # "httpResponse":Lorg/apache/http/HttpResponse;
    :cond_1
    const-string v3, "e"

    goto :goto_0
.end method

.method public static serviceStart(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "trigger"    # Ljava/lang/String;
    .param p5, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 628
    const-string v3, "BNRServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serviceStart!!!!!!!!!! - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 632
    .local v2, "url":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 633
    const-string v3, "BNRServiceManager"

    const-string v4, "There is NO Base URL."

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x131

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 637
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/cloud/start?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getCid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getGetApiParamsWithCid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 639
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 640
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "cdid"

    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    const-string v3, "did"

    invoke-virtual {p1}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 642
    const-string v3, "trigger"

    invoke-interface {v1, v3, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 643
    const-string v3, "ctid"

    invoke-interface {v1, v3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 645
    const-string v3, "811"

    sput-object v3, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->LAST_API:Ljava/lang/String;

    .line 647
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->get(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 648
    .local v0, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-static {v0, p5}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->extractResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 649
    return-void
.end method
