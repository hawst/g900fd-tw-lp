.class Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

.field final synthetic val$holder:Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

.field final synthetic val$posn:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;I)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    iput p3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$posn:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 615
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-boolean v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v0, :cond_0

    .line 618
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 619
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 620
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    iget v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$posn:I

    invoke-virtual {v0, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 621
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    iget v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$posn:I

    aput-boolean v3, v0, v1

    .line 622
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # operator++ for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$108(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    .line 631
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # getter for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$100(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 632
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 638
    :cond_0
    :goto_1
    return-void

    .line 625
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$holder:Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 626
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    iget v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$posn:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 627
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    iget v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->val$posn:I

    aput-boolean v2, v0, v1

    .line 628
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # operator-- for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$110(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    goto :goto_0

    .line 634
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;->this$1:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
