.class public interface abstract Lcom/samsung/android/scloud/backup/model/IModel;
.super Ljava/lang/Object;
.source "IModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    }
.end annotation


# virtual methods
.method public abstract getCid()Ljava/lang/String;
.end method

.method public abstract getDetailKey(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;
.end method

.method public abstract getOemAuthority()Ljava/lang/String;
.end method

.method public abstract getOemContentUri()Landroid/net/Uri;
.end method

.method public abstract getSourceKey()Ljava/lang/String;
.end method

.method public abstract getSourceResId()I
.end method

.method public abstract parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
.end method
