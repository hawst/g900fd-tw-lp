.class public Lcom/samsung/android/scloud/backup/showbackup/BackupList;
.super Lcom/samsung/android/scloud/backup/core/BNRActivity;
.source "BackupList.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;,
        Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;
    }
.end annotation


# static fields
.field private static final MENU_ID_DELETE:I = 0x1

.field private static final MENU_ID_DONE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "BackupList"

.field public static mContext:Landroid/content/Context;

.field private static mProgressDialog:Landroid/app/ProgressDialog;


# instance fields
.field private final DELETE_LIST:I

.field public final GET_UPDATED_LIST:I

.field backupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

.field ck:Landroid/widget/CheckBox;

.field private count:I

.field emptyText:Landroid/widget/TextView;

.field flag:Z

.field private jsonArray:Lorg/json/JSONArray;

.field list:Landroid/widget/ListView;

.field longFlag:Z

.field protected mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

.field mCheckbox:Landroid/widget/CheckBox;

.field mCtid:Ljava/lang/String;

.field mDeleteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field mListSelected:[Z

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mTwDragSelectedItemArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mnu1:Landroid/view/MenuItem;

.field mnu2:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;-><init>()V

    .line 62
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    .line 63
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->longFlag:Z

    .line 74
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    .line 76
    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->DELETE_LIST:I

    .line 78
    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->GET_UPDATED_LIST:I

    .line 80
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    .line 82
    iput v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    .line 89
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    .line 91
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCtid:Ljava/lang/String;

    .line 93
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->emptyText:Landroid/widget/TextView;

    .line 97
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 101
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->jsonArray:Lorg/json/JSONArray;

    .line 674
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/showbackup/BackupList;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/showbackup/BackupList;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->actionSelectallclicked(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    return v0
.end method

.method static synthetic access$108(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    return v0
.end method

.method static synthetic access$110(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backTolist()V

    return-void
.end method

.method private actionSelectallclicked(Ljava/lang/Boolean;)V
    .locals 5
    .param p1, "isChecked"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 490
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 492
    .local v1, "len":I
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 494
    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    .line 495
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 496
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aput-boolean v4, v2, v0

    .line 497
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 495
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 502
    .end local v0    # "i":I
    :cond_0
    iput v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    .line 503
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 504
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aput-boolean v3, v2, v0

    .line 505
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v0, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 503
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 509
    :cond_1
    return-void
.end method

.method private backTolist()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 464
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    .line 465
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->longFlag:Z

    .line 469
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 470
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aget-boolean v1, v1, v0

    if-ne v1, v3, :cond_0

    .line 471
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aput-boolean v2, v1, v0

    .line 472
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 473
    iget v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    .line 469
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 477
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 479
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->invalidateOptionsMenu()V

    .line 480
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 482
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 483
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 485
    return-void
.end method

.method private createMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f070037

    const v8, 0x7f020041

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 293
    const-string v0, "BackupList"

    const-string v1, "createMenu()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-nez v0, :cond_1

    .line 298
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 299
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 300
    const v0, 0x7f070028

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTitle(Ljava/lang/CharSequence;)V

    .line 301
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v6, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    .line 302
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 303
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 304
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    const v1, 0x7f020040

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 306
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v7, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    .line 307
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 309
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 311
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070087

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 317
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5, v7, v5, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    .line 319
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v7}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 323
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->longFlag:Z

    if-nez v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 326
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v8}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 330
    :cond_2
    iget v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    if-nez v0, :cond_3

    .line 331
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 332
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 335
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu2:Landroid/view/MenuItem;

    invoke-interface {v0, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private getDateStringFromTimeSpan(J)Ljava/lang/String;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 726
    sget-object v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 730
    .local v0, "dateFormat":Ljava/text/DateFormat;
    sget-object v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 734
    .local v1, "timeFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 738
    .local v2, "timeStr":Ljava/lang/String;
    return-object v2
.end method

.method private setActivityTheme()V
    .locals 3

    .prologue
    const v2, 0x103012b

    const v1, 0x1030128

    .line 235
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-eqz v0, :cond_1

    .line 238
    :cond_0
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTheme(I)V

    .line 252
    :goto_0
    return-void

    .line 240
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTheme(I)V

    goto :goto_0

    .line 243
    :cond_2
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 244
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTheme(I)V

    goto :goto_0

    .line 249
    :cond_3
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTheme(I)V

    goto :goto_0
.end method

.method private showAllBackup(Lorg/json/JSONArray;)V
    .locals 22
    .param p1, "jsonArray"    # Lorg/json/JSONArray;

    .prologue
    .line 747
    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 748
    .local v11, "bnrJSONobj":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 749
    const-string v4, "bnr"

    invoke-virtual {v11, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 750
    const-string v4, "bnr"

    invoke-virtual {v11, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 752
    .local v12, "dataitemarray":Lorg/json/JSONArray;
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 753
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v14, v4, :cond_6

    .line 754
    invoke-virtual {v12, v14}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v15

    .line 756
    .local v15, "obj":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 757
    .local v5, "Deviceid":Ljava/lang/String;
    const/4 v6, 0x0

    .line 758
    .local v6, "CDeviceId":Ljava/lang/String;
    const-wide/16 v18, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 759
    .local v10, "Usage":Ljava/lang/Long;
    const-wide/16 v18, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 760
    .local v16, "timestamp":Ljava/lang/Long;
    const/4 v9, 0x0

    .line 762
    .local v9, "modelNumber":Ljava/lang/String;
    const-string v4, "did"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 763
    const-string v4, "did"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 765
    :cond_0
    const-string v4, "cdid"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 766
    const-string v4, "cdid"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 767
    :cond_1
    const-string v4, "usage"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 768
    const-string v4, "usage"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 770
    :cond_2
    const-string v4, "date"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 771
    const-string v4, "date"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 778
    :goto_1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v7

    .line 779
    .local v7, "lastBackupTime":Ljava/lang/String;
    const-string v4, "name"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 780
    const-string v4, "name"

    invoke-virtual {v15, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 785
    :goto_2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    const-wide/16 v20, 0x0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    cmp-long v4, v18, v20

    if-lez v4, :cond_3

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v4, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    sget-object v8, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v8, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v4 .. v9}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 753
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 776
    .end local v7    # "lastBackupTime":Ljava/lang/String;
    :cond_4
    const-wide v18, 0x13c710e4150L

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    goto :goto_1

    .line 783
    .restart local v7    # "lastBackupTime":Ljava/lang/String;
    :cond_5
    move-object v9, v6

    goto :goto_2

    .line 795
    .end local v5    # "Deviceid":Ljava/lang/String;
    .end local v6    # "CDeviceId":Ljava/lang/String;
    .end local v7    # "lastBackupTime":Ljava/lang/String;
    .end local v9    # "modelNumber":Ljava/lang/String;
    .end local v10    # "Usage":Ljava/lang/Long;
    .end local v11    # "bnrJSONobj":Lorg/json/JSONObject;
    .end local v12    # "dataitemarray":Lorg/json/JSONArray;
    .end local v14    # "i":I
    .end local v15    # "obj":Lorg/json/JSONObject;
    .end local v16    # "timestamp":Ljava/lang/Long;
    :catch_0
    move-exception v13

    .line 797
    .local v13, "e":Lorg/json/JSONException;
    invoke-virtual {v13}, Lorg/json/JSONException;->printStackTrace()V

    .line 800
    .end local v13    # "e":Lorg/json/JSONException;
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Z

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    .line 801
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 802
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 803
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 804
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCount()I

    move-result v4

    if-lez v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    if-eqz v4, :cond_8

    .line 805
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mnu1:Landroid/view/MenuItem;

    const/4 v8, 0x1

    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 812
    :cond_7
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->invalidateOptionsMenu()V

    .line 813
    return-void

    .line 807
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCount()I

    move-result v4

    if-nez v4, :cond_7

    .line 808
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 809
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->emptyText:Landroid/widget/TextView;

    const v8, 0x7f070059

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method private showBackupDetailProgressDialog(Ljava/lang/String;)V
    .locals 2
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 703
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 704
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 706
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 707
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 708
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 709
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 710
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/scloud/backup/showbackup/BackupList$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$5;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 721
    return-void
.end method


# virtual methods
.method public addDragItemToListArray(Landroid/view/View;I)V
    .locals 2
    .param p1, "currentView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 820
    const-string v0, "BackupList"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 1

    .prologue
    .line 884
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x6a
        0x6b
    .end array-data
.end method

.method public getUsage()V
    .locals 3

    .prologue
    .line 685
    new-instance v0, Landroid/content/Intent;

    const-string v1, "REQUEST_GETUSAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 686
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "TRIGGER"

    const-string v2, "user"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    const-class v1, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 688
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 689
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 355
    const-string v0, "BackupList"

    const-string v1, "onBackPressed ()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v0, :cond_0

    .line 357
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backTolist()V

    .line 360
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 222
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 224
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v0, :cond_0

    .line 226
    const-string v0, "BackupList"

    const-string v1, "flag is true"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_0
    return-void
.end method

.method public onContentChanged()V
    .locals 3

    .prologue
    .line 268
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onContentChanged()V

    .line 269
    const v2, 0x1020004

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 270
    .local v0, "empty":Landroid/view/View;
    const/4 v1, 0x0

    .line 271
    .local v1, "list":Landroid/widget/ListView;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 272
    const v2, 0x7f090043

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "list":Landroid/widget/ListView;
    check-cast v1, Landroid/widget/ListView;

    .line 275
    .restart local v1    # "list":Landroid/widget/ListView;
    :goto_0
    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 277
    :cond_0
    return-void

    .line 274
    :cond_1
    const v2, 0x7f090041

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "list":Landroid/widget/ListView;
    check-cast v1, Landroid/widget/ListView;

    .restart local v1    # "list":Landroid/widget/ListView;
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    .line 113
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onCreate(Landroid/os/Bundle;)V

    .line 114
    const-string v0, "BackupList"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    .line 116
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 117
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setActivityTheme()V

    .line 119
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mTwDragSelectedItemArray:Ljava/util/ArrayList;

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 126
    const v0, 0x7f070070

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->showBackupDetailProgressDialog(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getUsage()V

    .line 128
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 131
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setContentView(I)V

    .line 132
    const v0, 0x7f090043

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    .line 141
    :goto_0
    const v0, 0x7f090042

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->emptyText:Landroid/widget/TextView;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    .line 144
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->generateCTID(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCtid:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 146
    const v0, 0x7f070028

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setTitle(Ljava/lang/CharSequence;)V

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/high16 v1, 0x7f030000

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 149
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    .line 150
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/scloud/backup/showbackup/BackupList$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$1;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    new-instance v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    .line 165
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 166
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->registerForContextMenu(Landroid/view/View;)V

    .line 168
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 205
    return-void

    .line 137
    :cond_2
    const v0, 0x7f030015

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->setContentView(I)V

    .line 138
    const v0, 0x7f090041

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    goto/16 :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 368
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 370
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070036

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070035

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070037

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$3;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 411
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 283
    const-string v0, "BackupList"

    const-string v1, "onCreateOptionsMenu()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 285
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->createMenu(Landroid/view/Menu;)V

    .line 286
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 345
    const-string v0, "BackupList"

    const-string v1, "onDestroy ()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 347
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 348
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 349
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onDestroy()V

    .line 350
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 6
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    const v5, 0x7f07001f

    const/16 v4, 0x131

    const/16 v2, 0xcd

    const/16 v1, 0xc9

    const/4 v3, 0x0

    .line 826
    const/16 v0, 0x6a

    if-ne p1, v0, :cond_4

    .line 827
    if-ne v2, p2, :cond_3

    .line 828
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 830
    const/16 v0, 0x12d

    if-ne p3, v0, :cond_2

    .line 831
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/json/JSONArray;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->jsonArray:Lorg/json/JSONArray;

    .line 832
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->jsonArray:Lorg/json/JSONArray;

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->showAllBackup(Lorg/json/JSONArray;)V

    .line 845
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 846
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 879
    :cond_1
    :goto_1
    return-void

    .line 835
    :cond_2
    if-ne p3, v4, :cond_0

    .line 837
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    const v1, 0x7f070011

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 848
    :cond_3
    if-ne v1, p2, :cond_1

    .line 849
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 852
    :cond_4
    const/16 v0, 0x6b

    if-ne p1, v0, :cond_1

    .line 854
    if-ne v2, p2, :cond_7

    .line 855
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    if-ne p3, v4, :cond_5

    .line 859
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 862
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 863
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_1

    .line 867
    :cond_5
    const/16 v0, 0x13f

    if-ne p3, v0, :cond_6

    .line 868
    sget-object v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 869
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getUsage()V

    goto/16 :goto_1

    .line 875
    :cond_7
    if-ne v1, p2, :cond_1

    .line 876
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 420
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 454
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 425
    :sswitch_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    .line 427
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 428
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 429
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 430
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 432
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070087

    new-array v4, v0, [Ljava/lang/Object;

    iget v5, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 434
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->invalidateOptionsMenu()V

    .line 435
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 436
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupListAdapter:Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->registerForContextMenu(Landroid/view/View;)V

    goto :goto_0

    .line 443
    :sswitch_1
    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v1, :cond_0

    .line 444
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backTolist()V

    goto :goto_0

    .line 446
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->finish()V

    goto :goto_0

    .line 450
    :sswitch_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->showDialog(I)V

    goto :goto_0

    .line 420
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x102002c -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onResume()V

    .line 260
    const-string v0, "BackupList"

    const-string v1, "OnResume "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    return-void
.end method

.method public startDelete(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 693
    .local p1, "mDeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;>;"
    const v1, 0x7f070060

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->showBackupDetailProgressDialog(Ljava/lang/String;)V

    .line 694
    new-instance v0, Landroid/content/Intent;

    const-string v1, "REQUEST_BACKUP_DELETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 695
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "TRIGGER"

    const-string v2, "user"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 696
    const-string v1, "SOURCE_LIST"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 697
    const-class v1, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 698
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 699
    return-void
.end method
