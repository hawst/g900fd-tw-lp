.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants$API;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/server/ServerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "API"
.end annotation


# static fields
.field public static final DELETE_BACKUP_FROM_SERVER_API:Ljava/lang/String; = "814"

.field public static final GET_TOTAL_USAGE_BY_USER_API:Ljava/lang/String; = "813"

.field public static final auth_oauth2_getuserinfo:Ljava/lang/String; = "410"

.field public static final auth_oauth2_getuserinfoweb:Ljava/lang/String; = "411_"

.field public static final bnr_backupready:Ljava/lang/String; = "610"

.field public static final bnr_clear:Ljava/lang/String; = "620"

.field public static final bnr_clearall:Ljava/lang/String; = "621"

.field public static final bnr_commit:Ljava/lang/String; = "613"

.field public static final bnr_delete:Ljava/lang/String; = "612"

.field public static final bnr_details:Ljava/lang/String; = "615"

.field public static final bnr_releasecleared:Ljava/lang/String; = "622"

.field public static final bnr_restoreitem:Ljava/lang/String; = "618"

.field public static final bnr_restoreitems:Ljava/lang/String; = "619"

.field public static final bnr_restorekeys:Ljava/lang/String; = "617"

.field public static final bnr_restoreready:Ljava/lang/String; = "616"

.field public static final bnr_rollback:Ljava/lang/String; = "614"

.field public static final bnr_set:Ljava/lang/String; = "611"

.field public static final cloud_end:Ljava/lang/String; = "812"

.field public static final cloud_prepare:Ljava/lang/String; = "810"

.field public static final cloud_start:Ljava/lang/String; = "811"

.field public static final kvs_delete:Ljava/lang/String; = "217"

.field public static final kvs_item:Ljava/lang/String; = "213"

.field public static final kvs_items:Ljava/lang/String; = "214"

.field public static final kvs_key:Ljava/lang/String; = "210"

.field public static final kvs_keys:Ljava/lang/String; = "211"

.field public static final kvs_set:Ljava/lang/String; = "216"

.field public static final kvs_timestamp:Ljava/lang/String; = "215"

.field public static final kvs_updates:Ljava/lang/String; = "212"

.field public static final ors_copy:Ljava/lang/String; = "317"

.field public static final ors_create:Ljava/lang/String; = "310"

.field public static final ors_delete:Ljava/lang/String; = "311"

.field public static final ors_download:Ljava/lang/String; = "316"

.field public static final ors_get:Ljava/lang/String; = "318"

.field public static final ors_list:Ljava/lang/String; = "314"

.field public static final ors_move:Ljava/lang/String; = "313"

.field public static final ors_rename:Ljava/lang/String; = "312"

.field public static final ors_set:Ljava/lang/String; = "319"

.field public static final ors_upload:Ljava/lang/String; = "315"

.field public static final service_activate:Ljava/lang/String; = "510"

.field public static final service_activateon:Ljava/lang/String; = "511"

.field public static final service_deactivate:Ljava/lang/String; = "512"

.field public static final service_getdevicelist:Ljava/lang/String; = "513"

.field public static final user_quota_all:Ljava/lang/String; = "715"

.field public static final user_quota_appid:Ljava/lang/String; = "711"

.field public static final user_quota_cid:Ljava/lang/String; = "713"

.field public static final user_quota_device:Ljava/lang/String; = "714"

.field public static final user_quota_total:Ljava/lang/String; = "710"

.field public static final user_quota_user:Ljava/lang/String; = "712"
