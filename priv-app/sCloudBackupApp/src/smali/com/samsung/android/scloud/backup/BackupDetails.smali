.class public Lcom/samsung/android/scloud/backup/BackupDetails;
.super Ljava/lang/Object;
.source "BackupDetails.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDeviceId:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mItemChecked:Z

.field public mMydevice:Z

.field private mOperation:I

.field private mTimeStampInTimeFormat:Ljava/lang/String;

.field public mTimestamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/samsung/android/scloud/backup/BackupDetails$1;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/BackupDetails$1;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/BackupDetails;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceName:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mOperation:I

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceId:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mMydevice:Z

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mItemChecked:Z

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimeStampInTimeFormat:Ljava/lang/String;

    .line 47
    return-void

    :cond_0
    move v0, v2

    .line 44
    goto :goto_0

    :cond_1
    move v1, v2

    .line 45
    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "operation"    # I
    .param p3, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceName:Ljava/lang/String;

    .line 35
    iput p2, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mOperation:I

    .line 36
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceId:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public deviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public deviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestampInTimeFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimeStampInTimeFormat:Ljava/lang/String;

    return-object v0
.end method

.method public isItemChecked()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mItemChecked:Z

    return v0
.end method

.method public isMydevice()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mMydevice:Z

    return v0
.end method

.method public operationType()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mOperation:I

    return v0
.end method

.method public setItemChecked(Z)V
    .locals 0
    .param p1, "mItemChecked"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mItemChecked:Z

    .line 79
    return-void
.end method

.method public setMydevice(Z)V
    .locals 0
    .param p1, "mMydevice"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mMydevice:Z

    .line 71
    return-void
.end method

.method public setTimestamp(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTimestamp"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setTimestampInTimeFormat(Ljava/lang/String;)V
    .locals 0
    .param p1, "mTimestamp"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimeStampInTimeFormat:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 105
    iget v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mOperation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mMydevice:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 109
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mItemChecked:Z

    if-eqz v0, :cond_1

    :goto_1
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 110
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimeStampInTimeFormat:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    return-void

    :cond_0
    move v0, v2

    .line 108
    goto :goto_0

    :cond_1
    move v1, v2

    .line 109
    goto :goto_1
.end method
