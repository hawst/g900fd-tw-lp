.class public interface abstract Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
.super Ljava/lang/Object;
.source "IModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/model/IModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IHasFile"
.end annotation


# virtual methods
.method public abstract getAlternatePathParameter()Ljava/lang/String;
.end method

.method public abstract getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;
.end method

.method public abstract getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;
.end method

.method public abstract isAlternateDownloadPathAvailable()Z
.end method
