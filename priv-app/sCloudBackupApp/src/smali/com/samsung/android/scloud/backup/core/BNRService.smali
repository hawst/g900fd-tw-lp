.class public abstract Lcom/samsung/android/scloud/backup/core/BNRService;
.super Landroid/app/Service;
.source "BNRService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# instance fields
.field protected ActivityHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 48
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/core/BNRService$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRService;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRService;->ActivityHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->getTargetEventServiceCodes()[I

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRService;->ActivityHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BackupApp;->setHandlerActivity(Ljava/lang/String;[ILandroid/os/Handler;)V

    .line 38
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestory()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/core/BackupApp;->removeHandlerActivity(Ljava/lang/String;)V

    .line 46
    return-void
.end method
