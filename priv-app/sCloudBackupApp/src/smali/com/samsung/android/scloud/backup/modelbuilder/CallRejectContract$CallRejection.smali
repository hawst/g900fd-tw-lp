.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/CallRejectContract$CallRejection;
.super Ljava/lang/Object;
.source "CallRejectContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/CallRejectContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallRejection"
.end annotation


# static fields
.field public static final CALL_REJECT_LIST:Ljava/lang/String; = "CALLREJECT"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final REJECT_CHECKED:Ljava/lang/String; = "reject_checked"

.field public static final REJECT_MATCH:Ljava/lang/String; = "reject_match"

.field public static final REJECT_NUMBER:Ljava/lang/String; = "reject_number"
