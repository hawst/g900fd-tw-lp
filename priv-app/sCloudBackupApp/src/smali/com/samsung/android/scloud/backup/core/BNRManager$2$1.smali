.class Lcom/samsung/android/scloud/backup/core/BNRManager$2$1;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager$2;->handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$2;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager$2;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$2$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$2;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 5
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$2$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$2;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/core/BNRManager$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$2$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$2;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/core/BNRManager$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "releaseCleared Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$2$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$2;

    iget-object v4, v4, Lcom/samsung/android/scloud/backup/core/BNRManager$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 163
    if-eqz p1, :cond_0

    const/16 v0, 0x4e2a

    if-ne p1, v0, :cond_1

    .line 168
    :cond_0
    return-void

    .line 166
    :cond_1
    new-instance v0, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v1, 0x135

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",releasecleared failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v0
.end method
