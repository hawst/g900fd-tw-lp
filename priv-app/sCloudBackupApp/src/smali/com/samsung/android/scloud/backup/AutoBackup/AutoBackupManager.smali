.class public Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;
.super Landroid/app/Service;
.source "AutoBackupManager.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private final mStatusReceiver:Landroid/content/BroadcastReceiver;

.field private metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 32
    const-string v0, "AutoBackupManager"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->TAG:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mContext:Landroid/content/Context;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 89
    new-instance v0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;-><init>(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->setAutoBackupAlarm(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->disableAutoBackupAlarm(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method private disableAutoBackupAlarm(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 173
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 175
    .local v0, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->stopAutoBackupService(Landroid/content/Context;)V

    .line 176
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->cancelAutoBackupAlarm(Landroid/content/Context;)V

    .line 177
    return-void
.end method

.method private setAutoBackupAlarm(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    const-string v1, "AutoBackupManager"

    const-string v2, "[setAutoBackupAlarm] "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 169
    .local v0, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->setAutoBackupAlarm(Landroid/content/Context;)V

    .line 170
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mContext:Landroid/content/Context;

    .line 67
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 69
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 70
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 71
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    const-string v1, "AutoBackupManager"

    const-string v2, "[onCreate] Create AutoBackupManager.... "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 84
    const-string v0, "AutoBackupManager"

    const-string v1, "[onDestroy] Remove AutoBackupManager.... "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 87
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 45
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 47
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    const-string v1, "AutoBackupManager"

    const-string v2, "AutoBackupManager is called. But AutoBackup is not checked. Destory this Service. "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->stopSelf()V

    .line 50
    const/4 v1, 0x2

    .line 60
    :goto_0
    return v1

    .line 54
    :cond_0
    if-eqz p1, :cond_1

    .line 55
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "action":Ljava/lang/String;
    const-string v1, "AutoBackupManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStartCommand is called. Intent Action name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .end local v0    # "action":Ljava/lang/String;
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 58
    :cond_1
    const-string v1, "AutoBackupManager"

    const-string v2, "[onStartCommand] intent is null. This means this service is restarted by a system. "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
