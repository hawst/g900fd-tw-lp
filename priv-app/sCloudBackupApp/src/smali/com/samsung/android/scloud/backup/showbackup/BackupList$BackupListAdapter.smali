.class Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BackupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BackupListAdapter"
.end annotation


# instance fields
.field mBackupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p2, "c"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 519
    .local p3, "backupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;>;"
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 521
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mContext:Landroid/content/Context;

    .line 523
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    .line 524
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 538
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 545
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 553
    move v2, p1

    .line 554
    .local v2, "posn":I
    move-object v3, p2

    .line 555
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_2

    .line 556
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 557
    .local v1, "layoutInflator":Landroid/view/LayoutInflater;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 558
    const v4, 0x7f03000b

    invoke-virtual {v1, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 561
    :goto_0
    new-instance v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;-><init>()V

    .line 562
    .local v0, "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    const v4, 0x7f090002

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    .line 563
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-boolean v4, v4, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v4, :cond_1

    .line 565
    invoke-virtual {v3, v5}, Landroid/view/View;->setLongClickable(Z)V

    .line 566
    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    .line 567
    invoke-virtual {v3, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 568
    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 577
    :goto_1
    const v4, 0x7f090016

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->deviceName:Landroid/widget/TextView;

    .line 579
    const v4, 0x7f090005

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->latestBackupDate:Landroid/widget/TextView;

    .line 581
    const v4, 0x7f090006

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->size:Landroid/widget/TextView;

    .line 582
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 601
    .end local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :goto_2
    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v5, v5, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aget-boolean v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 602
    iget-object v5, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->deviceName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getModelName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 603
    iget-object v5, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->latestBackupDate:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->latestBackupDate()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    iget-object v5, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->size:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->mBackupList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->size()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    new-instance v4, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$1;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 642
    new-instance v4, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter$2;-><init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 666
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->invalidateOptionsMenu()V

    .line 667
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->notifyDataSetChanged()V

    .line 668
    return-object v3

    .line 560
    .end local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    .restart local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f03000a

    invoke-virtual {v1, v4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_0

    .line 572
    .restart local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    :cond_1
    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 573
    invoke-virtual {v3, v6}, Landroid/view/View;->setLongClickable(Z)V

    .line 574
    invoke-virtual {v3, v5}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_1

    .line 585
    .end local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    .end local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    .line 586
    .restart local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$BackupListAdapter;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-boolean v4, v4, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->flag:Z

    if-eqz v4, :cond_3

    .line 588
    invoke-virtual {v3, v5}, Landroid/view/View;->setLongClickable(Z)V

    .line 589
    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    .line 590
    invoke-virtual {v3, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 591
    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_2

    .line 594
    :cond_3
    iget-object v4, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v4, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 595
    invoke-virtual {v3, v6}, Landroid/view/View;->setLongClickable(Z)V

    .line 596
    invoke-virtual {v3, v5}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_2
.end method
