.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BlackListContract$BList;
.super Ljava/lang/Object;
.source "BlackListContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BlackListContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BList"
.end annotation


# static fields
.field public static final ACCOUNT_ID:Ljava/lang/String; = "accountId"

.field public static final BLACKLIST:Ljava/lang/String; = "BLACKLIST"

.field public static final EMAIL_ADDRESS:Ljava/lang/String; = "emailAddress"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final IS_DOMAIN:Ljava/lang/String; = "isDomain"

.field public static final LAST_ACCESSED_TIME_STAMP:Ljava/lang/String; = "lastAccessedTimeStamp"

.field public static final USER_NAME:Ljava/lang/String; = "userName"
