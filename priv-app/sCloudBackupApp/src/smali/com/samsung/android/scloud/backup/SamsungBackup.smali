.class public Lcom/samsung/android/scloud/backup/SamsungBackup;
.super Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;
.source "SamsungBackup.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;
    }
.end annotation


# static fields
.field private static final ACTION_SAMSUNG_ACCOUNT_VALIDATION:Ljava/lang/String; = "com.msc.action.VALIDATION_CHECK_RESPONSE"

.field private static final MENU_BACKUP_NOW_ID:I = 0x1

.field private static final MENU_ID_DELETE:I = 0x2

.field private static final SA_REQUEST_ID_VALIDATION:I = 0x1

.field static cancelstatus:Z

.field static first:Z

.field private static mMenu:Landroid/view/Menu;

.field private static mProgressDialog:Landroid/app/ProgressDialog;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private alertdialog:Landroid/app/AlertDialog;

.field private backUtil:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

.field backupDataMenuItem:Landroid/view/MenuItem;

.field private backupbutton:Landroid/widget/TextView;

.field private backupbuttonlayout:Landroid/widget/LinearLayout;

.field public backupnowbutton:Landroid/view/MenuItem;

.field checkBox:Landroid/widget/CheckBox;

.field defaultWallpaper:Z

.field private isSupportAidlOnly:Z

.field private mActivity:Landroid/app/Activity;

.field mActivty:Landroid/app/Activity;

.field private mAutoBackup:Landroid/preference/Preference;

.field private mBackupitem:Landroid/preference/PreferenceCategory;

.field private mBlackList:Landroid/preference/CheckBoxPreference;

.field private mCallReject:Landroid/preference/CheckBoxPreference;

.field private mContext:Landroid/content/Context;

.field public mDialog:Landroid/app/Dialog;

.field private mEmail:Landroid/preference/CheckBoxPreference;

.field private mIntentToStartActivity:Landroid/content/Intent;

.field private mLog:Landroid/preference/CheckBoxPreference;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mMms:Landroid/preference/CheckBoxPreference;

.field private mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

.field private mSms:Landroid/preference/CheckBoxPreference;

.field private mSourceKeyPreference:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/preference/CheckBoxPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mVIP:Landroid/preference/CheckBoxPreference;

.field private mValidSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mValidationStatus:I

.field private mWallpaper:Landroid/preference/CheckBoxPreference;

.field noneSelected:Z

.field t:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    sput-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 98
    sput-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 118
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;-><init>()V

    .line 101
    const-string v0, "SamsungBackup"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->TAG:Ljava/lang/String;

    .line 102
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    .line 103
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 104
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->noneSelected:Z

    .line 106
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->defaultWallpaper:Z

    .line 107
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->t:Landroid/widget/Toast;

    .line 108
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    .line 110
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->alertdialog:Landroid/app/AlertDialog;

    .line 111
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backUtil:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    .line 112
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    .line 115
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I

    .line 117
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mIntentToStartActivity:Landroid/content/Intent;

    .line 119
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;

    .line 121
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 122
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z

    .line 1235
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->requestAllCancelBackup()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/scloud/backup/SamsungBackup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->buttonStatus(Z)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1300()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1302(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Landroid/app/ProgressDialog;

    .prologue
    .line 83
    sput-object p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/scloud/backup/SamsungBackup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->registerSamsungAccountReceiver()V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/scloud/backup/SamsungBackup;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/scloud/backup/SamsungBackup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;
    .param p1, "x1"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->unregisterSamsungAccountReceiver()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mIntentToStartActivity:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/scloud/backup/SamsungBackup;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mIntentToStartActivity:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backUtil:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayInitialBackingup()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->requestBackup()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/scloud/backup/SamsungBackup;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    return-object v0
.end method

.method private backupCancelled(Ljava/lang/String;)V
    .locals 7
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1149
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "backupCancelled"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    const/4 v0, 0x0

    .line 1151
    .local v0, "backupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1152
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1153
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 1154
    const/4 v0, 0x1

    goto :goto_0

    .line 1157
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    if-nez v0, :cond_2

    .line 1158
    sput-boolean v6, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1159
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1160
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->enablePreference()V

    .line 1162
    :cond_2
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setLastBackupTimeSource(Ljava/lang/String;Z)V

    .line 1163
    return-void
.end method

.method private backupFailed(Ljava/lang/String;)V
    .locals 8
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 1166
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "backupFailed"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    const/4 v0, 0x0

    .line 1168
    .local v0, "backupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1169
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1170
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 1171
    const/4 v0, 0x1

    goto :goto_0

    .line 1174
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    if-nez v0, :cond_2

    .line 1175
    sput-boolean v7, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1176
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1177
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->enablePreference()V

    .line 1179
    :cond_2
    invoke-direct {p0, p1, v7}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setLastBackupTimeSource(Ljava/lang/String;Z)V

    .line 1180
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 1181
    return-void
.end method

.method private backupSuccess(Ljava/lang/String;)V
    .locals 9
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 1130
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "backupSuccess"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    const/4 v0, 0x0

    .line 1132
    .local v0, "backupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1133
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1134
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_0

    .line 1135
    const/4 v0, 0x1

    goto :goto_0

    .line 1138
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    if-nez v0, :cond_2

    .line 1139
    sput-boolean v5, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1140
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1141
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->enablePreference()V

    .line 1143
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070041

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1144
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v8, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 1145
    return-void
.end method

.method private buttonStatus(Z)V
    .locals 3
    .param p1, "status"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 593
    if-eqz p1, :cond_1

    .line 594
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 595
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setSourceList()V

    .line 599
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->noneSelected:Z

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 601
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private disablePreference()V
    .locals 4

    .prologue
    .line 651
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 652
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 653
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private displayBackingup()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 640
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 641
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_0

    .line 642
    const/4 v2, 0x0

    .line 643
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_1

    .line 644
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v2

    .line 645
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    const v4, 0x7f07001b

    new-array v5, v8, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 648
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "progress":I
    :cond_2
    return-void
.end method

.method private displayCancelling()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 631
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 632
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 633
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const v3, 0x7f07002e

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 637
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private displayInitialBackingup()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 656
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 657
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const v3, 0x7f07001b

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 658
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private enablePreference()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 661
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 662
    .local v2, "key":Ljava/lang/String;
    const-string v4, "HOMESCREEN"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 663
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    .line 664
    .local v3, "wallPaperPreference":Landroid/preference/CheckBoxPreference;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isWallpaperDefault(Landroid/content/Context;)Z

    move-result v4

    if-ne v4, v6, :cond_1

    .line 665
    iput-boolean v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->defaultWallpaper:Z

    .line 666
    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 667
    invoke-virtual {v3, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 669
    :cond_1
    iput-boolean v7, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->defaultWallpaper:Z

    .line 670
    invoke-virtual {v3, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 671
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const-string v5, "HOMESCREEN"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 673
    .end local v3    # "wallPaperPreference":Landroid/preference/CheckBoxPreference;
    :cond_2
    const-string v4, "VIPLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "BLACKLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 674
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 675
    .local v0, "emailPreference":Landroid/preference/CheckBoxPreference;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 676
    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 677
    invoke-virtual {v0, v7}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 679
    :cond_4
    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 680
    const-string v4, "VIPLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 681
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const-string v5, "VIPLIST"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 682
    :cond_5
    const-string v4, "BLACKLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 683
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const-string v5, "BLACKLIST"

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_0

    .line 686
    .end local v0    # "emailPreference":Landroid/preference/CheckBoxPreference;
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 688
    .end local v2    # "key":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method private getOverflowMenu()V
    .locals 5

    .prologue
    .line 224
    :try_start_0
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 225
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v3, Landroid/view/ViewConfiguration;

    const-string v4, "sHasPermanentMenuKey"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 226
    .local v2, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v2, :cond_0

    .line 227
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 228
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v2    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v1

    .line 231
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private initPref()V
    .locals 6

    .prologue
    .line 610
    const-string v4, "screen"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    .line 611
    .local v2, "screen":Landroid/preference/PreferenceScreen;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 612
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 613
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 614
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 615
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mVIP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 616
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBlackList:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 617
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 618
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 621
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 623
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    .line 624
    .local v3, "sourcePreference":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 625
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 628
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "sourcePreference":Landroid/preference/CheckBoxPreference;
    :cond_0
    return-void
.end method

.method private networkisAvailable(Z)Z
    .locals 8
    .param p1, "bToast"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1096
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v4

    .line 1097
    .local v4, "wifiOnly":Z
    const/4 v0, 0x0

    .line 1099
    .local v0, "bNetConn":Z
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1100
    .local v1, "connManager":Landroid/net/ConnectivityManager;
    if-eqz v4, :cond_2

    .line 1101
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1102
    .local v3, "uWifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1103
    const/4 v0, 0x1

    .line 1126
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v0

    .line 1105
    .restart local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_1
    if-eqz p1, :cond_0

    .line 1106
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v6, 0x7f070079

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1114
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_2
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1115
    .local v2, "mNetInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1116
    const/4 v0, 0x1

    goto :goto_0

    .line 1118
    :cond_3
    if-eqz p1, :cond_0

    .line 1119
    const v5, 0x7f07009b

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private registerSamsungAccountReceiver()V
    .locals 3

    .prologue
    .line 1353
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1354
    new-instance v1, Lcom/samsung/android/scloud/backup/SamsungBackup$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$16;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 1419
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1420
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1421
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1422
    return-void
.end method

.method private requestAllCancelBackup()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 916
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_CANCEL"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 917
    .local v5, "service":Landroid/content/Intent;
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 918
    const/4 v1, 0x0

    .line 919
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 920
    .local v0, "cancelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 921
    .local v3, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 922
    add-int/lit8 v1, v1, 0x1

    .line 923
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 926
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    new-array v4, v1, [Ljava/lang/String;

    .line 927
    .local v4, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 928
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 929
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityCancelingBackup()V

    .line 930
    invoke-direct {p0, v8, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startNotificationService(Z[Ljava/lang/String;)V

    .line 931
    return-void
.end method

.method private requestBackup()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 946
    new-instance v3, Landroid/content/Intent;

    const-string v4, "REQUEST_BACKUP"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 947
    .local v3, "service":Landroid/content/Intent;
    const-class v4, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v3, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 948
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v2, v4, [Ljava/lang/String;

    .line 949
    .local v2, "mSourceArr":[Ljava/lang/String;
    const-string v5, "SOURCE_LIST"

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 950
    const-string v4, "TRIGGER"

    const-string v5, "user"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 951
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 952
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 953
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    goto :goto_0

    .line 954
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityBackup()V

    .line 955
    const/4 v4, 0x0

    invoke-direct {p0, v4, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startNotificationService(Z[Ljava/lang/String;)V

    .line 956
    return-void
.end method

.method private setActivityTheme()V
    .locals 4

    .prologue
    const v3, 0x103012b

    const/4 v2, 0x1

    .line 827
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_2

    .line 828
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, " This is a T / K device wih Light theme "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setTheme(I)V

    .line 842
    :cond_1
    :goto_0
    return-void

    .line 832
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 834
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v2, :cond_3

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v2, :cond_4

    .line 835
    :cond_3
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setTheme(I)V

    goto :goto_0

    .line 837
    :cond_4
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setTheme(I)V

    goto :goto_0
.end method

.method private setLastBackupTime()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const v11, 0x7f070042

    const v10, 0x7f070041

    const v9, 0x7f07001d

    const/4 v8, -0x1

    .line 691
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 692
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastOperationStatus(Ljava/lang/String;Z)I

    move-result v2

    .line 693
    .local v2, "lastOperationStatus":I
    const-string v3, "HOMESCREEN"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 694
    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->defaultWallpaper:Z

    if-eqz v3, :cond_0

    .line 695
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    const v4, 0x7f070034

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 696
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v3, v4, v12

    if-nez v3, :cond_2

    .line 697
    if-ne v2, v8, :cond_1

    .line 698
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v11}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 700
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v11}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 702
    :cond_2
    if-ne v2, v8, :cond_3

    .line 703
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 705
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 708
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v3, v4, v12

    if-nez v3, :cond_6

    .line 709
    if-ne v2, v8, :cond_5

    .line 710
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v11}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 712
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v11}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 714
    :cond_6
    if-ne v2, v8, :cond_7

    .line 715
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 717
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 721
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "lastOperationStatus":I
    :cond_8
    return-void
.end method

.method private setLastBackupTimeSource(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bSuccess"    # Z

    .prologue
    const-wide/16 v6, 0x0

    const v4, 0x7f070042

    const v3, 0x7f070041

    const v2, 0x7f07001d

    .line 724
    const-string v0, "HOMESCREEN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 725
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->defaultWallpaper:Z

    if-eqz v0, :cond_0

    .line 726
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const v1, 0x7f070034

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 753
    :goto_0
    return-void

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 728
    if-nez p2, :cond_1

    .line 729
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 731
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 733
    :cond_2
    if-nez p2, :cond_3

    .line 734
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 736
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 740
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-nez v0, :cond_6

    .line 741
    if-nez p2, :cond_5

    .line 742
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 744
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 747
    :cond_6
    if-nez p2, :cond_7

    .line 748
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 750
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private setSourceList()V
    .locals 3

    .prologue
    .line 756
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->noneSelected:Z

    .line 757
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 758
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 760
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->noneSelected:Z

    goto :goto_0

    .line 763
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private showData3gDialog()V
    .locals 5

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 404
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 406
    .local v1, "linearLayout":Landroid/view/View;
    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    .line 407
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/samsung/android/scloud/backup/SamsungBackup$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$6;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 416
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f070010

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/scloud/backup/SamsungBackup$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$9;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/scloud/backup/SamsungBackup$8;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$8;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07002a

    new-instance v4, Lcom/samsung/android/scloud/backup/SamsungBackup$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$7;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->alertdialog:Landroid/app/AlertDialog;

    .line 449
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->alertdialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 450
    return-void
.end method

.method private startNotificationService(Z[Ljava/lang/String;)V
    .locals 3
    .param p1, "isCancelTrue"    # Z
    .param p2, "sourceList"    # [Ljava/lang/String;

    .prologue
    .line 972
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 973
    .local v0, "service":Landroid/content/Intent;
    const-class v1, Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 974
    const-string v1, "SOURCE_LIST"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    const-string v2, "Notification Type"

    if-eqz p1, :cond_0

    const/16 v1, 0x68

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 976
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 977
    return-void

    .line 975
    :cond_0
    const/16 v1, 0x65

    goto :goto_0
.end method

.method private unregisterSamsungAccountReceiver()V
    .locals 2

    .prologue
    .line 1425
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1426
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 1429
    :cond_0
    return-void
.end method

.method private updatePreferenceList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 980
    const-string v1, "auto_backup"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    .line 981
    const-string v1, "logcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mLog:Landroid/preference/CheckBoxPreference;

    .line 982
    const-string v1, "smscheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSms:Landroid/preference/CheckBoxPreference;

    .line 983
    const-string v1, "mmscheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMms:Landroid/preference/CheckBoxPreference;

    .line 984
    const-string v1, "wallpapercheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mWallpaper:Landroid/preference/CheckBoxPreference;

    .line 985
    const-string v1, "viplistcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mVIP:Landroid/preference/CheckBoxPreference;

    .line 986
    const-string v1, "blacklistcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBlackList:Landroid/preference/CheckBoxPreference;

    .line 987
    const-string v1, "spamcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mEmail:Landroid/preference/CheckBoxPreference;

    .line 988
    const-string v1, "callrejectcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mCallReject:Landroid/preference/CheckBoxPreference;

    .line 991
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v0

    .line 992
    .local v0, "activityStatus":I
    const-string v1, "SamsungBackup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updatePreferenceList : activityStatus - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityBackup(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 995
    sput-boolean v4, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 996
    sput-boolean v5, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 997
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 998
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayBackingup()V

    .line 999
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V

    .line 1014
    :goto_0
    return-void

    .line 1001
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingBackup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1002
    sput-boolean v4, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1003
    sput-boolean v4, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 1004
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1005
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V

    .line 1006
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V

    goto :goto_0

    .line 1009
    :cond_1
    sput-boolean v5, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1010
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1011
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->enablePreference()V

    .line 1012
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setLastBackupTime()V

    goto :goto_0
.end method


# virtual methods
.method checkStatus()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1059
    const/4 v3, 0x0

    .line 1061
    .local v3, "value":Z
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1062
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1063
    .local v2, "uWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1065
    .local v1, "uMobile":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1066
    const/4 v3, 0x1

    .line 1092
    :cond_0
    :goto_0
    return v3

    .line 1067
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1069
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getData3gAskStatus()Z

    move-result v3

    .line 1070
    if-nez v3, :cond_0

    .line 1071
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showData3gDialog()V

    goto :goto_0

    .line 1074
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1075
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v5, 0x7f070079

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1089
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 1090
    const/4 v3, 0x0

    goto :goto_0

    .line 1083
    :cond_3
    const v4, 0x7f07009b

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public doNegativeClick(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 1048
    const-string v0, "SamsungBackup"

    const-string v1, "doNegativeClick: On Cancel Click"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 1050
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 1056
    :cond_0
    :goto_0
    return-void

    .line 1051
    :cond_1
    const/16 v0, 0x10

    if-ne p1, v0, :cond_0

    .line 1052
    const-string v0, "SamsungBackup"

    const-string v1, "Do nothing"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    goto :goto_0
.end method

.method public doPositiveClick(I)V
    .locals 5
    .param p1, "type"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1017
    const-string v1, "SamsungBackup"

    const-string v2, "doPositiveClick: On OK Button Click"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    const/16 v1, 0xb

    if-ne p1, v1, :cond_2

    .line 1019
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkStatus()Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 1020
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1021
    const v1, 0x7f070089

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1022
    .local v0, "serverBusyMessage":Ljava/lang/String;
    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1045
    .end local v0    # "serverBusyMessage":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1027
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayInitialBackingup()V

    .line 1028
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V

    .line 1029
    sput-boolean v3, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1030
    sput-boolean v4, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 1031
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 1032
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->requestBackup()V

    goto :goto_0

    .line 1035
    :cond_2
    const/16 v1, 0x10

    if-ne p1, v1, :cond_0

    .line 1036
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityBackup()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1037
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 1038
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->requestAllCancelBackup()V

    .line 1039
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V

    .line 1040
    sput-boolean v3, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 1041
    sput-boolean v3, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 1042
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public getSamsungBackupContext()Landroid/content/Context;
    .locals 0

    .prologue
    .line 145
    return-object p0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1185
    const-string v0, "SamsungBackup"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 3

    .prologue
    .line 1231
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x65

    aput v2, v0, v1

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 1340
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1341
    if-ne p1, v1, :cond_0

    .line 1342
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1343
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I

    .line 1350
    :cond_0
    :goto_0
    return-void

    .line 1347
    :cond_1
    iput v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v6, 0x7f03000f

    const v5, 0x7f03000e

    const/4 v4, 0x0

    const/16 v3, 0x400

    const/4 v2, 0x1

    .line 149
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 150
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 186
    :goto_0
    return-void

    .line 153
    :pswitch_0
    const-string v0, "SamsungBackup"

    const-string v1, "onConfigurationChanged : ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 157
    :cond_0
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-ne v0, v2, :cond_2

    .line 158
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    .line 162
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 164
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 159
    :cond_2
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v0, v2, :cond_1

    .line 160
    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    goto :goto_1

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 171
    :pswitch_1
    const-string v0, "SamsungBackup"

    const-string v1, "onConfigurationChanged : ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 174
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 175
    :cond_4
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-ne v0, v2, :cond_7

    .line 176
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    .line 180
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 181
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 182
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 177
    :cond_7
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v0, v2, :cond_5

    .line 178
    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    goto :goto_2

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f070056

    const/16 v7, 0x400

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 454
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setActivityTheme()V

    .line 456
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 457
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_H:Z

    if-nez v5, :cond_0

    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_J:Z

    if-eqz v5, :cond_5

    .line 458
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 461
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    .line 462
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 464
    iput-object p0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivty:Landroid/app/Activity;

    .line 465
    invoke-direct {p0, v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->networkisAvailable(Z)Z

    move-result v5

    if-nez v5, :cond_1

    .line 467
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    .line 469
    :cond_1
    const v5, 0x7f040001

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->addPreferencesFromResource(I)V

    .line 472
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 473
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 475
    :cond_2
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-ne v5, v8, :cond_6

    .line 476
    const v5, 0x7f03000e

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    .line 481
    :goto_1
    const-string v5, "SamsungBackup"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Intent received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const v5, 0x7f07001c

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setTitle(Ljava/lang/CharSequence;)V

    .line 483
    const-string v5, "screen"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceScreen;

    .line 484
    .local v4, "prefscreen":Landroid/preference/PreferenceScreen;
    const-string v5, "backup_item"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceCategory;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBackupitem:Landroid/preference/PreferenceCategory;

    .line 485
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-nez v5, :cond_3

    .line 486
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBackupitem:Landroid/preference/PreferenceCategory;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 488
    :cond_3
    const-string v5, "auto_backup"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    .line 489
    const-string v5, "logcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mLog:Landroid/preference/CheckBoxPreference;

    .line 490
    const-string v5, "smscheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSms:Landroid/preference/CheckBoxPreference;

    .line 491
    const-string v5, "mmscheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMms:Landroid/preference/CheckBoxPreference;

    .line 492
    const-string v5, "wallpapercheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mWallpaper:Landroid/preference/CheckBoxPreference;

    .line 493
    const-string v5, "viplistcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mVIP:Landroid/preference/CheckBoxPreference;

    .line 494
    const-string v5, "blacklistcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBlackList:Landroid/preference/CheckBoxPreference;

    .line 495
    const-string v5, "spamcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mEmail:Landroid/preference/CheckBoxPreference;

    .line 496
    const-string v5, "callrejectcheckBox"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mCallReject:Landroid/preference/CheckBoxPreference;

    .line 499
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    .line 501
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 502
    .local v3, "key":Ljava/lang/String;
    const-string v5, "CALLLOGS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 503
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 460
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_0

    .line 477
    :cond_6
    sget-boolean v5, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v5, v8, :cond_7

    .line 478
    const v5, 0x7f03000f

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    goto/16 :goto_1

    .line 480
    :cond_7
    const v5, 0x7f030017

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setContentView(I)V

    goto/16 :goto_1

    .line 504
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "key":Ljava/lang/String;
    .restart local v4    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_8
    const-string v5, "SMS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 505
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 506
    :cond_9
    const-string v5, "MMS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 507
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 508
    :cond_a
    const-string v5, "HOMESCREEN"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 509
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 510
    :cond_b
    const-string v5, "VIPLIST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 511
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mVIP:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 512
    :cond_c
    const-string v5, "BLACKLIST"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 513
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mBlackList:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 514
    :cond_d
    const-string v5, "SPAM"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 515
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 516
    :cond_e
    const-string v5, "CALLREJECT"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 517
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 522
    .end local v3    # "key":Ljava/lang/String;
    :cond_f
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSms:Landroid/preference/CheckBoxPreference;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f070007

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 523
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMms:Landroid/preference/CheckBoxPreference;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f070004

    invoke-virtual {p0, v7}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 525
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->initPref()V

    .line 526
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getOverflowMenu()V

    .line 529
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidSourceList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 530
    .restart local v3    # "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    new-instance v6, Lcom/samsung/android/scloud/backup/SamsungBackup$10;

    invoke-direct {v6, p0, v3}, Lcom/samsung/android/scloud/backup/SamsungBackup$10;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_3

    .line 539
    .end local v3    # "key":Ljava/lang/String;
    :cond_10
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    new-instance v6, Lcom/samsung/android/scloud/backup/SamsungBackup$11;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$11;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 559
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v1

    .line 560
    .local v1, "activityStatus":I
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setLastBackupTime()V

    .line 562
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityBackup(I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 563
    sput-boolean v8, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 564
    sput-boolean v9, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 565
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 566
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayBackingup()V

    .line 580
    :goto_4
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getVerificationStatus()I

    move-result v5

    if-eqz v5, :cond_11

    .line 581
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->checkAccountValidation(Landroid/content/Context;)Z

    move-result v0

    .line 582
    .local v0, "accountValid":Z
    if-ne v0, v8, :cond_14

    .line 583
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setVerificationStatus(I)V

    .line 590
    .end local v0    # "accountValid":Z
    :cond_11
    :goto_5
    return-void

    .line 567
    :cond_12
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingBackup()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 568
    sput-boolean v8, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 569
    sput-boolean v8, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 570
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 571
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V

    .line 572
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V

    goto :goto_4

    .line 575
    :cond_13
    sput-boolean v9, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 576
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 577
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setLastBackupTime()V

    goto :goto_4

    .line 586
    .restart local v0    # "accountValid":Z
    :cond_14
    iput-object p0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;

    .line 587
    new-instance v5, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;Lcom/samsung/android/scloud/backup/SamsungBackup$1;)V

    new-array v6, v9, [Ljava/lang/Void;

    invoke-virtual {v5, v6}, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_5
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f07002b

    const/4 v5, 0x2

    const v4, 0x7f07007b

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    .line 239
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupDataMenuItem:Landroid/view/MenuItem;

    .line 240
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 241
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    .line 242
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    .line 243
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v3, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    .line 245
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 248
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/android/scloud/backup/SamsungBackup$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$1;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    :cond_1
    sget-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    if-eqz v0, :cond_8

    .line 283
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 284
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingBackup()Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    if-eqz v0, :cond_7

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 288
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 289
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 309
    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->setSourceList()V

    .line 310
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->noneSelected:Z

    if-eqz v0, :cond_5

    .line 311
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 312
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 313
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 315
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_a

    .line 317
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 318
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 319
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 334
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 293
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 294
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 295
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 300
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 301
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 302
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 303
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 304
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 305
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbutton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 323
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_b

    .line 325
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupbuttonlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 330
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupnowbutton:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 778
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onDestroy()V

    .line 779
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 6
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    .line 1191
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 1193
    packed-switch p2, :pswitch_data_0

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1196
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1199
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    const/16 v0, 0x12d

    if-ne p3, v0, :cond_1

    .line 1201
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupSuccess(Ljava/lang/String;)V

    .line 1207
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1202
    :cond_1
    const/16 v0, 0x132

    if-ne p3, v0, :cond_2

    .line 1203
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupCancelled(Ljava/lang/String;)V

    goto :goto_1

    .line 1206
    :cond_2
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->backupFailed(Ljava/lang/String;)V

    goto :goto_1

    .line 1210
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:203 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1213
    :pswitch_3
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 1214
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_3

    .line 1215
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v1, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const v1, 0x7f07001b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1217
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:202 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1220
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:204 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1193
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 128
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 129
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 132
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    const-string v2, "Dialog_Type"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 134
    .local v1, "dialog":I
    if-eq v1, v3, :cond_0

    .line 135
    const-string v2, "Dialog_Type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showDialogById(I)V

    .line 142
    .end local v1    # "dialog":I
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->updatePreferenceList()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 783
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 822
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 785
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    goto :goto_0

    .line 788
    :sswitch_1
    sget-boolean v2, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    if-eqz v2, :cond_0

    .line 789
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 790
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->requestAllCancelBackup()V

    .line 791
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V

    .line 792
    sput-boolean v1, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 793
    sput-boolean v1, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 794
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    goto :goto_0

    .line 810
    :cond_0
    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showDialogById(I)V

    goto :goto_0

    .line 817
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 818
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 819
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 783
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 768
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onPause()V

    .line 769
    const-string v0, "SamsungBackup"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 774
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 190
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onResume()V

    .line 192
    const-string v2, "SamsungBackup"

    const-string v3, "onResume"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingRestore()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    :cond_0
    const v2, 0x7f07001e

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 196
    .local v1, "serverBusyMessage":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    .line 219
    .end local v1    # "serverBusyMessage":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 204
    :cond_2
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->networkisAvailable(Z)Z

    move-result v2

    if-nez v2, :cond_3

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    .line 208
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->updatePreferenceList()V

    .line 209
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    check-cast v2, Landroid/preference/TwoStatePreference;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    .line 211
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 214
    .local v0, "dialog":I
    if-eq v0, v4, :cond_1

    .line 215
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showDialogById(I)V

    goto :goto_0
.end method

.method showAutoBackupDialog()V
    .locals 7

    .prologue
    .line 339
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;

    check-cast v4, Landroid/preference/TwoStatePreference;

    invoke-virtual {v4}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupCheckBoxStatus()Z

    move-result v4

    if-nez v4, :cond_1

    .line 341
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->backUtil:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    .line 343
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 344
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03000c

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 346
    .local v1, "linearLayout":Landroid/view/View;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v5, 0x7f070013

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 347
    .local v2, "message":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->isChinaDevice(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 348
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v5, 0x7f070014

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 350
    :cond_0
    const v4, 0x7f090017

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 351
    .local v3, "tv":Landroid/widget/TextView;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 352
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    const v4, 0x7f090018

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    .line 354
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 356
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/android/scloud/backup/SamsungBackup$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$2;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 365
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v6, 0x7f070012

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/scloud/backup/SamsungBackup$5;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$5;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;

    const v6, 0x104000a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/samsung/android/scloud/backup/SamsungBackup$4;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$4;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07002a

    new-instance v6, Lcom/samsung/android/scloud/backup/SamsungBackup$3;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$3;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->alertdialog:Landroid/app/AlertDialog;

    .line 396
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->alertdialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 398
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "linearLayout":Landroid/view/View;
    .end local v2    # "message":Ljava/lang/String;
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_1
    return-void
.end method

.method public showDialogById(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    const v2, 0x104000a

    .line 845
    sparse-switch p1, :sswitch_data_0

    .line 898
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not defined dialog ID!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 847
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07007b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070024

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070077

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/SamsungBackup$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$13;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/SamsungBackup$12;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$12;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    .line 900
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 901
    return-void

    .line 869
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070062

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/SamsungBackup$14;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$14;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 884
    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07008c

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07008b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/SamsungBackup$15;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/SamsungBackup$15;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 845
    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0xe -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch
.end method
