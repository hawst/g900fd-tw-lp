.class Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager$4;->handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager$4;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 5
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    const/4 v4, 0x4

    .line 346
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "releaseCleared Finished - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 348
    if-eqz p1, :cond_0

    const/16 v0, 0x4e2a

    if-ne p1, v0, :cond_1

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "firstBackupOperation Finished - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;->this$1:Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void

    .line 351
    :cond_1
    new-instance v0, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v1, 0x135

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",releasecleared failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v0
.end method
