.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;
.super Landroid/os/AsyncTask;
.source "SetupWizardRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChecksameDeviceBackupExists"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0

    .prologue
    .line 814
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;
    .param p2, "x1"    # Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$1;

    .prologue
    .line 814
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;-><init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x1

    .line 818
    const-string v3, "SetupWizardRestoreActivity"

    const-string v4, "[processRestoreReady]"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 821
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 822
    .local v1, "currentDeviceId":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 823
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 824
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 825
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v3, v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$502(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;I)I

    .line 826
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iput-boolean v5, v3, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 832
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3

    .line 822
    .restart local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 814
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 838
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-boolean v0, v0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->sameDeviceBackupExistsFlag:Z

    if-nez v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showSelectDeviceDialog()V

    .line 853
    :goto_0
    return-void

    .line 843
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$702(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 844
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$700(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$902(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 845
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 846
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 850
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initView()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1100(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 814
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$ChecksameDeviceBackupExists;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
