.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$ProgressColumns;
.super Ljava/lang/Object;
.source "BaseBackupContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ProgressColumns"
.end annotation


# static fields
.field public static final LOCAL_PROGRESS:Ljava/lang/String; = "localProgress"

.field public static final LOCAL_STATUS:Ljava/lang/String; = "localStatus"

.field public static final OPERATION:Ljava/lang/String; = "operation"

.field public static final REMOTE_PROGRESS:Ljava/lang/String; = "remoteProgress"

.field public static final REMOTE_STATUS:Ljava/lang/String; = "remoteStatus"
