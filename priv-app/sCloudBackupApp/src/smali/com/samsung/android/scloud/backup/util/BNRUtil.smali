.class public Lcom/samsung/android/scloud/backup/util/BNRUtil;
.super Ljava/lang/Object;
.source "BNRUtil.java"


# static fields
.field public static final ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

.field public static final ACCOUNT_CONTENT_URI:Landroid/net/Uri;

.field public static final ONLY_SUPPORT_AIDL_OF_SAMSUNGACCOUNT_VERSION:I = 0x30d40

.field private static final SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN:I = 0x400

.field private static final SYSTEM_UI_FLAG_TRANSPARENT_BACKGROUND:I = 0x1000

.field private static TAG:Ljava/lang/String; = null

.field private static clientDeviceId:Ljava/lang/String; = null

.field private static final unsupportedLockScreenPath:Ljava/lang/String; = "/data/data/com.samsung.android.keyguardwallpaperupdator/"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    const-string v0, "BNRUtil"

    sput-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    .line 41
    const-string v0, "content://com.msc.openprovider.openContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    .line 43
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->ACCOUNT_AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "tncRequest"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static checkAccountValidation(Landroid/content/Context;)Z
    .locals 15
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v2, 0x0

    .line 183
    const/4 v12, 0x0

    .line 184
    .local v12, "tncState":I
    const/4 v11, 0x0

    .line 185
    .local v11, "nameCheckState":I
    const/4 v9, 0x0

    .line 186
    .local v9, "emailValidationState":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/scloud/backup/util/BNRUtil;->ACCOUNT_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 188
    .local v7, "cursor":Landroid/database/Cursor;
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    .line 190
    .local v10, "manager":Landroid/accounts/AccountManager;
    if-eqz v10, :cond_2

    .line 191
    const-string v0, "com.osp.app.signin"

    invoke-virtual {v10, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 197
    .local v6, "accountArray":[Landroid/accounts/Account;
    array-length v0, v6

    if-lez v0, :cond_4

    .line 201
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 205
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    invoke-interface {v7, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 209
    .local v8, "emailID":Ljava/lang/String;
    invoke-interface {v7, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 211
    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 213
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 215
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "emailID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tncState :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nameCheckState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " emailValidationState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    add-int v0, v12, v11

    add-int/2addr v0, v9

    if-nez v0, :cond_0

    .line 223
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v0, v13

    .line 248
    .end local v6    # "accountArray":[Landroid/accounts/Account;
    .end local v8    # "emailID":Ljava/lang/String;
    :goto_0
    return v0

    .line 233
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_1
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    const-string v1, "Fail To Obtain Cursor"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    .end local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_2
    :goto_1
    if-eqz v7, :cond_3

    .line 246
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v14

    .line 248
    goto :goto_0

    .line 241
    .restart local v6    # "accountArray":[Landroid/accounts/Account;
    :cond_4
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    const-string v1, "Samsung Account Not Logged in"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static checkIfDualSim(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 494
    const/4 v0, 0x0

    .line 495
    .local v0, "numOfSimSlots":I
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getNumofSimSlots(Landroid/content/Context;)I

    move-result v0

    .line 496
    if-ne v1, v0, :cond_0

    .line 497
    const/4 v1, 0x0

    .line 499
    :cond_0
    return v1
.end method

.method public static generateCTID(I)Ljava/lang/String;
    .locals 6
    .param p0, "length"    # I

    .prologue
    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_0

    .line 55
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static declared-synchronized getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const-class v3, Lcom/samsung/android/scloud/backup/util/BNRUtil;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 64
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :goto_0
    monitor-exit v3

    return-object v2

    .line 66
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    .line 67
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 69
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    .line 70
    .local v0, "phoneType":I
    if-eqz v0, :cond_3

    .line 73
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    .line 82
    :goto_1
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_1

    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    :cond_1
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    .line 89
    :cond_2
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;

    goto :goto_0

    .line 78
    :cond_3
    sget-object v2, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    sput-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->clientDeviceId:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 63
    .end local v0    # "phoneType":I
    .end local v1    # "telephonyManager":Landroid/telephony/TelephonyManager;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getCurrentSimID(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 425
    const/4 v1, -0x1

    .line 427
    .local v1, "currSimId":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    const-string v7, "android.os.SystemProperties"

    invoke-virtual {v6, v7}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 429
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v6, 0x2

    new-array v4, v6, [Ljava/lang/Class;

    .line 430
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v4, v6

    .line 431
    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v4, v6

    .line 433
    const-string v6, "getInt"

    invoke-virtual {v0, v6, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 435
    .local v3, "get":Ljava/lang/reflect/Method;
    const/4 v6, 0x2

    new-array v5, v6, [Ljava/lang/Object;

    .line 436
    .local v5, "params":[Ljava/lang/Object;
    const/4 v6, 0x0

    const-string v7, "ril.MSIMM"

    aput-object v7, v5, v6

    .line 437
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 439
    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 441
    if-nez v1, :cond_0

    .line 442
    const/4 v6, 0x2

    new-array v5, v6, [Ljava/lang/Object;

    .line 443
    const/4 v6, 0x0

    const-string v7, "persist.radio.calldefault.simid"

    aput-object v7, v5, v6

    .line 444
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 446
    invoke-virtual {v3, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 455
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v3    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_0
    :goto_0
    const/4 v6, -0x1

    if-ne v6, v1, :cond_1

    .line 456
    const/4 v1, 0x0

    .line 459
    :cond_1
    return v1

    .line 449
    :catch_0
    move-exception v2

    .line 450
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IllegalArgumentException - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 451
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 452
    .local v2, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getIsGT_I9300()Z
    .locals 3

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "ret":Z
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9300"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    const/4 v0, 0x1

    .line 134
    :cond_0
    return v0
.end method

.method public static getIsTablet(Landroid/content/res/Configuration;)Z
    .locals 5
    .param p0, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "isTablet":Z
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_0

    move v1, v0

    .line 118
    .end local v0    # "isTablet":Z
    .local v1, "isTablet":I
    :goto_0
    return v1

    .line 99
    .end local v1    # "isTablet":I
    .restart local v0    # "isTablet":Z
    :cond_0
    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-I9200"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "GT-I9205"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SHV-E310"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SGH-I527"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SCH-R960"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SCH-P729"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "DCH-P729"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SPH-L600"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v4, "SGH-M819N"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v1, v0

    .line 103
    .restart local v1    # "isTablet":I
    goto :goto_0

    .line 106
    .end local v1    # "isTablet":I
    :cond_2
    iget v3, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v3, 0xf

    .line 109
    .local v2, "size":I
    packed-switch v2, :pswitch_data_0

    :goto_1
    move v1, v0

    .line 118
    .restart local v1    # "isTablet":I
    goto :goto_0

    .line 113
    .end local v1    # "isTablet":I
    :pswitch_0
    const/4 v0, 0x1

    .line 114
    goto :goto_1

    .line 109
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getNumofSimSlots(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 463
    const/4 v3, 0x0

    .line 465
    .local v3, "numOfSimSlots":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    const-string v7, "android.os.SystemProperties"

    invoke-virtual {v6, v7}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 467
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v6, 0x2

    new-array v4, v6, [Ljava/lang/Class;

    .line 468
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v4, v6

    .line 469
    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v4, v6

    .line 471
    const-string v6, "getInt"

    invoke-virtual {v0, v6, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 474
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v6, 0x2

    new-array v5, v6, [Ljava/lang/Object;

    .line 475
    .local v5, "params":[Ljava/lang/Object;
    const/4 v6, 0x0

    const-string v7, "ro.multisim.simslotcount"

    aput-object v7, v5, v6

    .line 476
    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 478
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 486
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    :goto_0
    if-nez v3, :cond_0

    .line 487
    const/4 v3, 0x1

    .line 490
    :cond_0
    return v3

    .line 480
    :catch_0
    move-exception v1

    .line 481
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    sget-object v6, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IllegalArgumentException - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 482
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 483
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getVersionOfSamsungAccount(Landroid/content/Context;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 394
    const/4 v3, 0x0

    .line 396
    .local v3, "version":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 397
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.osp.app.signin"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 398
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v3

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got exception in getVersionOfSamsungAccount"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static isCallRejectionTablePresent(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 338
    const-string v0, "content://com.android.phone.callsettings/reject_num"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 339
    .local v1, "mUri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 341
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 345
    if-eqz v6, :cond_0

    .line 346
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v8

    .line 348
    :goto_0
    return v0

    .line 342
    :catch_0
    move-exception v7

    .local v7, "e":Ljava/lang/Exception;
    move v0, v9

    .line 343
    goto :goto_0
.end method

.method public static isEmailLoggedIn(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 307
    const-string v0, "content://com.android.email.provider/account"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 309
    .local v1, "mUri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 311
    .local v7, "c":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 315
    .local v6, "accountCount":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "emailAddress"

    aput-object v4, v2, v3

    const-string v3, "emailAddress!=\'snc@snc.snc\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 324
    if-eqz v7, :cond_0

    .line 325
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 326
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 329
    :cond_0
    if-nez v6, :cond_1

    move v0, v9

    .line 332
    :goto_0
    return v0

    .line 317
    :catch_0
    move-exception v8

    .line 319
    .local v8, "e":Ljava/lang/Exception;
    sget-object v0, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isEmailLoggedIn exception = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 320
    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :cond_1
    move v0, v10

    .line 332
    goto :goto_0
.end method

.method public static isSatusBarHidden()Z
    .locals 1

    .prologue
    .line 417
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-eqz v0, :cond_1

    .line 419
    :cond_0
    const/4 v0, 0x1

    .line 421
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportAidlOnly(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 409
    .local v0, "result":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getVersionOfSamsungAccount(Landroid/content/Context;)I

    move-result v1

    const v2, 0x30d40

    if-lt v1, v2, :cond_0

    .line 410
    const/4 v0, 0x1

    .line 412
    :cond_0
    return v0
.end method

.method public static isViewType_H()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 139
    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_H:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MS01:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MIDAS:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CANE:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_FRESCO:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_J:Z

    if-ne v1, v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViewType_K()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 156
    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_K:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_SLTE:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PACIFIC:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VASTA_OR_MEGA:Z

    if-ne v1, v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViewType_KK()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 165
    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_HEAT:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CORSICAVE:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_YOUNG:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_POCKET:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_AFYON:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIVALTO:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KANAS:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VICTOR:Z

    if-ne v1, v0, :cond_1

    .line 169
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViewType_Light()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 148
    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_K:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_H:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_J:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_E:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PACIFIC:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_SLTE:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v1, v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isViewType_T()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 175
    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_A:Z

    if-ne v1, v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWallpaperBackupNotSupported(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 379
    const-string v1, ""

    .line 380
    .local v1, "lockscreenwallpaperPath":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper_path"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "lockscreen_wallpaper"

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 383
    .local v0, "lockscreen_wallpaper":I
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 384
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    const-string v3, "Locksreen Wallpaper is set to Live View"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "/data/data/com.samsung.android.keyguardwallpaperupdator/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 388
    sget-object v2, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Locksreen backup is not supported, Path is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_1
    return-void
.end method

.method public static isWallpaperDefault(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v5, -0x1

    .line 352
    const-string v1, ""

    .line 367
    .local v1, "lockscreenwallpaperPath":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lockscreen_wallpaper_path"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 369
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "lockscreen_wallpaper"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 371
    .local v0, "lockscreen_wallpaper":I
    if-eq v5, v0, :cond_0

    if-ne v2, v0, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 374
    :cond_0
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static setSMSMode(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    const-string v6, "android.app.AppOpsManager"

    invoke-virtual {v5, v6}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 257
    .local v0, "AppOpsManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x4

    new-array v3, v5, [Ljava/lang/Class;

    .line 258
    .local v3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    .line 259
    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    .line 260
    const/4 v5, 0x2

    const-class v6, Ljava/lang/String;

    aput-object v6, v3, v5

    .line 261
    const/4 v5, 0x3

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    .line 263
    const-string v5, "setMode"

    invoke-virtual {v0, v5, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 266
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v5, 0x4

    new-array v4, v5, [Ljava/lang/Object;

    .line 268
    .local v4, "params":[Ljava/lang/Object;
    const/4 v5, 0x0

    const/16 v6, 0xf

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 269
    const/4 v5, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 270
    const/4 v5, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 271
    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 272
    const-string v5, "appops"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    .end local v0    # "AppOpsManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v3    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v4    # "params":[Ljava/lang/Object;
    :goto_0
    return-void

    .line 273
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got exception in setSMSMode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static setTransGradationMode(Landroid/content/Context;Z)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 504
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    const-string v6, "android.app.StatusBarManager"

    invoke-virtual {v5, v6}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 506
    .local v0, "StatusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/Class;

    .line 507
    .local v3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v3, v5

    .line 509
    const-string v5, "setTransGradationMode"

    invoke-virtual {v0, v5, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 512
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/Object;

    .line 513
    .local v4, "params":[Ljava/lang/Object;
    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    .line 515
    const-string v5, "statusbar"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 520
    .end local v0    # "StatusBarManager":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v3    # "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v4    # "params":[Ljava/lang/Object;
    :goto_0
    return-void

    .line 516
    :catch_0
    move-exception v1

    .line 517
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Got exception in setTransGradationMode"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static supportHeterogeneousDevice()Z
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "ret":Z
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-N7100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-N7105"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    const/4 v0, 0x1

    .line 126
    :cond_1
    return v0
.end method

.method public static updateMessageThreads(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 285
    new-instance v0, Lcom/samsung/android/scloud/backup/util/BNRUtil$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil$1;-><init>(Landroid/content/Context;)V

    .line 302
    .local v0, "runnable":Ljava/lang/Runnable;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 303
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 304
    return-void
.end method
