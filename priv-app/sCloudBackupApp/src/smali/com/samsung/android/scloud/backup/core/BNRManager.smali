.class public Lcom/samsung/android/scloud/backup/core/BNRManager;
.super Ljava/lang/Object;
.source "BNRManager.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IStatusListener;


# static fields
.field private static final PROCESSED_KEY_FILE_NAME:Ljava/lang/String; = "_processedKey"

.field private static final TAG:Ljava/lang/String; = "BNRManager_"


# instance fields
.field private final PAUSE_LOCK:Ljava/lang/Object;

.field private deleteFailed:Ljava/lang/Boolean;

.field private isCanceled:Z

.field private isFailed:I

.field private isInProcess:Z

.field private isPaused:Z

.field private mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mModel:Lcom/samsung/android/scloud/backup/model/IModel;

.field private mProcessedKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceType:I

.field private final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 65
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteFailed:Ljava/lang/Boolean;

    .line 69
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    .line 70
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isInProcess:Z

    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    .line 80
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    .line 82
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 83
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BNRManager_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/scloud/backup/core/BNRManager;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteFailed:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/core/BNRManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->backupReady(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/backup/core/BNRManager;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperationOnRestore(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/backup/core/BNRManager;Lorg/json/JSONObject;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;
    .param p1, "x1"    # Lorg/json/JSONObject;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->notifyQuota(Lorg/json/JSONObject;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/scloud/backup/core/BNRManager;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V

    return-void
.end method

.method private backupReady(I)V
    .locals 5
    .param p1, "cnt"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v4, Lcom/samsung/android/scloud/backup/core/BNRManager$2;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager$2;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupReady(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 185
    return-void
.end method

.method private notifyQuota(Lorg/json/JSONObject;)V
    .locals 4
    .param p1, "storageJson"    # Lorg/json/JSONObject;

    .prologue
    .line 774
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sCloudQuotaApp.QUOTA_WARNING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 775
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "BNRManager_"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Quota warning recieved. Available= :-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "available"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const-string v1, "level"

    const-string v2, "level"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 777
    const-string v1, "quota"

    const-string v2, "quota"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 778
    const-string v1, "usage"

    const-string v2, "usage"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 779
    const-string v1, "available"

    const-string v2, "available"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 780
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 781
    return-void
.end method

.method private postOperation(IILjava/lang/String;Ljava/lang/Object;)V
    .locals 17
    .param p1, "serviceType"    # I
    .param p2, "rCode"    # I
    .param p3, "msg"    # Ljava/lang/String;
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 234
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "postOperation "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const/16 v4, 0x13c

    move/from16 v0, p2

    if-eq v0, v4, :cond_5

    .line 238
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    if-eqz v4, :cond_0

    .line 239
    const/16 p2, 0x132

    .line 241
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    .line 243
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    .line 244
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    .line 246
    const/16 v4, 0x12d

    move/from16 v0, p2

    if-ne v0, v4, :cond_1

    const/4 v8, 0x1

    .line 248
    .local v8, "isSuc":Z
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v9, p3

    invoke-static/range {v4 .. v9}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->serviceEnd(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_1
    const-string v4, ""

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    .line 255
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v11

    .line 256
    .local v11, "dir":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 257
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 258
    .local v12, "dirArray":[Ljava/io/File;
    if-nez v12, :cond_2

    .line 278
    .end local v8    # "isSuc":Z
    .end local v11    # "dir":Ljava/io/File;
    .end local v12    # "dirArray":[Ljava/io/File;
    :goto_2
    return-void

    .line 246
    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    .line 249
    .restart local v8    # "isSuc":Z
    :catch_0
    move-exception v13

    .line 250
    .local v13, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v5, "postOperation-serviceEnd err "

    invoke-static {v4, v5, v13}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 260
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v11    # "dir":Ljava/io/File;
    .restart local v12    # "dirArray":[Ljava/io/File;
    :cond_2
    move-object v10, v12

    .local v10, "arr$":[Ljava/io/File;
    array-length v15, v10

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_3
    if-ge v14, v15, :cond_4

    aget-object v16, v10, v14

    .line 261
    .local v16, "tempFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Files : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v5}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 263
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Delete temp File : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " - size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", delete : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->delete()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 269
    .end local v10    # "arr$":[Ljava/io/File;
    .end local v12    # "dirArray":[Ljava/io/File;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    .end local v16    # "tempFile":Ljava/io/File;
    :cond_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isInProcess:Z

    .line 273
    .end local v8    # "isSuc":Z
    .end local v11    # "dir":Ljava/io/File;
    :cond_5
    if-eqz p4, :cond_6

    .line 274
    const/16 v4, 0xcd

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p4

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(IIILjava/lang/Object;)V

    goto/16 :goto_2

    .line 276
    :cond_6
    const/16 v4, 0xcd

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v4, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    goto/16 :goto_2
.end method

.method private postOperationOnBackup(II)V
    .locals 5
    .param p1, "cnt"    # I
    .param p2, "rCode"    # I

    .prologue
    const/16 v4, 0x12d

    .line 281
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postOperationOnBackup : retryCnt - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    if-ne p2, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, p0, v3, v0}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->postOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Z)V

    .line 285
    const/16 v0, 0x12e

    if-ne p2, v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backup Finished with Do_nothing - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->commitLastBackupTime()V

    .line 424
    :goto_1
    return-void

    .line 283
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 289
    :cond_1
    if-ne p2, v4, :cond_3

    .line 291
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isFirstBackup(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v4, Lcom/samsung/android/scloud/backup/core/BNRManager$4;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager$4;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupClearCommit(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_1

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v4, Lcom/samsung/android/scloud/backup/core/BNRManager$5;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager$5;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupCommit(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_1

    .line 402
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v4, Lcom/samsung/android/scloud/backup/core/BNRManager$6;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager$6;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupRollback(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_1
.end method

.method private postOperationOnRestore(ILjava/lang/String;)V
    .locals 9
    .param p1, "rCode"    # I
    .param p2, "srcDevice"    # Ljava/lang/String;

    .prologue
    const/16 v8, 0x12d

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 428
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v2, "postOperationOnRestore "

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    if-ne p1, v8, :cond_0

    move v5, v7

    :cond_0
    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->postOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Z)V

    .line 435
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 437
    if-ne p1, v8, :cond_1

    .line 438
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v1, "setFirstBackup - true "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setFirstBackup(Ljava/lang/String;Z)V

    .line 441
    :cond_1
    return-void

    .line 429
    :catch_0
    move-exception v6

    .line 430
    .local v6, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result p1

    goto :goto_0
.end method

.method private preOperation(Ljava/lang/String;)V
    .locals 6
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isInProcess:Z

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v1, 0x13c

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v0

    .line 102
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isInProcess:Z

    .line 104
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->generateCTID(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preOperation.. BNR VERSION - 4.01.3.KK_APP, CTID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->initProgress(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v1, "Request auth information to dataRelay... "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v0, Lcom/samsung/android/scloud/backup/auth/AuthControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/auth/AuthControl;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/auth/AuthControl;->getAuthInformation(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    .line 112
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/core/BNRManager$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;)V

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->serviceStart(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 118
    return-void
.end method

.method private preOperationOnBackup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v2, "preOperationOnBackup "

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isFirstBackup(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getOperationFinished(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->backupRollback(I)V

    .line 127
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->backupReady(I)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0, v1, p0, v2}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->preOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 130
    return-void
.end method

.method private preOperationOnRestore(ILjava/lang/String;)V
    .locals 6
    .param p1, "cnt"    # I
    .param p2, "srcDevice"    # Ljava/lang/String;

    .prologue
    .line 188
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preOperationOnRestore : retryCnt - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRManager;->clearPreRestoredData()V

    .line 192
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager$3;

    invoke-direct {v5, p0, p1, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager$3;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;ILjava/lang/String;)V

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->restoreReady(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 230
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0, v1, p0, v2}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->preOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 231
    return-void
.end method


# virtual methods
.method public backupRollback(I)V
    .locals 5
    .param p1, "cnt"    # I

    .prologue
    .line 554
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "preOperationOnBackup backupRollback!! - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isFirstBackup : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v4}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isFirstBackup(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", OperationFinished : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v4}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getOperationFinished(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 557
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v4, Lcom/samsung/android/scloud/backup/core/BNRManager$8;

    invoke-direct {v4, p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager$8;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupRollback(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 590
    return-void
.end method

.method public backupStart(Ljava/lang/String;)V
    .locals 7
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x65

    .line 444
    iput v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    .line 446
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperation(Ljava/lang/String;)V

    .line 447
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperationOnBackup()V

    .line 450
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->initLastBackupTime(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_1

    .line 453
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5, p0}, Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_1 .. :try_end_1} :catch_0

    .line 463
    :goto_0
    return-void

    .line 454
    :catch_0
    move-exception v1

    .line 455
    .local v1, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    :try_start_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 456
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    invoke-virtual {p0, v2, v3, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->throwException(Ljava/lang/String;ILcom/samsung/android/scloud/backup/common/BNRException;)V
    :try_end_2
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 459
    .end local v1    # "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    :catch_1
    move-exception v0

    .line 460
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0, v6, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "logLevel"    # I
    .param p2, "tag"    # Ljava/lang/String;
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x137

    const/4 v6, -0x1

    .line 737
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[TID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p3}, Lcom/samsung/android/scloud/backup/util/LOG;->log(ILjava/lang/String;Ljava/lang/String;)V

    .line 738
    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    if-eq v1, v6, :cond_5

    .line 739
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 740
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 742
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPaused : wait!! [TID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]- paused : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 744
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPaused : notified!! [TID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]- paused : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 750
    :cond_1
    :try_start_2
    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    if-eqz v1, :cond_2

    .line 751
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x132

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v1

    .line 759
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 745
    :catch_0
    move-exception v0

    .line 746
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    invoke-static {v1, p3, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 747
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x134

    invoke-direct {v1, v3, v0}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v1

    .line 753
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    if-eq v1, v6, :cond_4

    .line 754
    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    if-ne v1, v7, :cond_3

    .line 755
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x137

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v1

    .line 757
    :cond_3
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x134

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v1

    .line 759
    :cond_4
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 761
    :cond_5
    return-void
.end method

.method public clearPreRestoredData()V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v1, "clearPreRestoredData"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_processedKey"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/FileSavedList;->load(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/util/FileSavedList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    .line 91
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preOperation. remove restored values in previous failed restoring.. - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    const/4 v5, 0x0

    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->postOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Z)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 96
    :cond_0
    return-void
.end method

.method public deleteBackupUsage(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "trigger"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "mDeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;>;"
    const/16 v4, 0x12d

    const/16 v9, 0x6b

    .line 511
    iput v9, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    .line 512
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteFailed:Ljava/lang/Boolean;

    .line 513
    const/16 v3, 0xc9

    invoke-virtual {p0, v9, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 515
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperation(Ljava/lang/String;)V

    .line 517
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 519
    move v1, v2

    .line 520
    .local v1, "currentIndex":I
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getDeviceID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    new-instance v6, Lcom/samsung/android/scloud/backup/core/BNRManager$7;

    invoke-direct {v6, p0, p2, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager$7;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager;Ljava/util/ArrayList;I)V

    invoke-static {v4, v3, v5, v6}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 517
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 535
    .end local v1    # "currentIndex":I
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteFailed:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_1

    .line 536
    iget v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v4, 0x12d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v7, 0xcd

    const/16 v8, 0x12d

    invoke-static {v6, v7, v8}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    .line 542
    .end local v2    # "i":I
    :goto_1
    return-void

    .line 538
    .restart local v2    # "i":I
    :cond_1
    iget v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v4, 0x13f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v7, 0xcd

    const/16 v8, 0x13f

    invoke-static {v6, v7, v8}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 539
    .end local v2    # "i":I
    :catch_0
    move-exception v0

    .line 540
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {p0, v9, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public getBackedupDetails(Ljava/lang/String;)V
    .locals 6
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0x67

    .line 486
    iput v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    .line 487
    const/16 v1, 0xc9

    const/16 v2, 0x12d

    invoke-virtual {p0, v5, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 489
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperation(Ljava/lang/String;)V

    .line 491
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, p0}, Lcom/samsung/android/scloud/backup/core/BNRTask;->getBackedupHistory(ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :goto_0
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v5, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getBackupUsage(Ljava/lang/String;)V
    .locals 7
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x6a

    .line 499
    iput v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    .line 500
    const/16 v1, 0xc9

    const/16 v2, 0x12d

    invoke-virtual {p0, v5, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 502
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperation(Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, p0}, Lcom/samsung/android/scloud/backup/core/BNRTask;->getAllBackupUsage(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_0
    return-void

    .line 505
    :catch_0
    move-exception v0

    .line 506
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v5, v1, v2, v6}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onFinished(Ljava/lang/String;IILjava/lang/Object;)V
    .locals 10
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "serviceType"    # I
    .param p3, "rCode"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 643
    const/16 v6, 0x12d

    if-eq p3, v6, :cond_0

    const/16 v6, 0x12e

    if-ne p3, v6, :cond_1

    .line 644
    :cond_0
    const/16 v6, 0x67

    if-eq p2, v6, :cond_1

    .line 648
    const/16 v6, 0x65

    if-ne p2, v6, :cond_1

    .line 649
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, p1, v8, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 653
    :cond_1
    const/16 v6, 0x67

    if-eq p2, v6, :cond_2

    const/16 v6, 0x66

    if-ne p2, v6, :cond_7

    .line 654
    :cond_2
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onFinished : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    :goto_0
    const/16 v6, 0x67

    if-eq p2, v6, :cond_d

    const/16 v6, 0x6a

    if-eq p2, v6, :cond_d

    .line 664
    const/4 v3, 0x1

    .line 665
    .local v3, "isFinish":Z
    const/4 v4, 0x1

    .line 666
    .local v4, "isSuc":Z
    const/4 v2, 0x1

    .line 667
    .local v2, "isDoNothing":Z
    const/4 v5, 0x0

    .line 668
    .local v5, "isUserCanceled":Z
    const/4 v6, -0x1

    if-eq p3, v6, :cond_8

    const/4 v6, 0x1

    :goto_1
    and-int/2addr v3, v6

    .line 669
    const/16 v6, 0x12d

    if-ne p3, v6, :cond_9

    const/4 v6, 0x1

    :goto_2
    and-int/2addr v4, v6

    .line 670
    const/16 v6, 0x12e

    if-ne p3, v6, :cond_a

    const/4 v6, 0x1

    :goto_3
    and-int/2addr v2, v6

    .line 671
    const/16 v6, 0x132

    if-ne p3, v6, :cond_b

    const/4 v6, 0x1

    :goto_4
    or-int/2addr v5, v6

    .line 672
    const/16 v6, 0x12d

    if-eq p3, v6, :cond_3

    const/16 v6, 0x12e

    if-eq p3, v6, :cond_3

    const/16 v6, 0x132

    if-eq p3, v6, :cond_3

    .line 675
    if-eqz v5, :cond_3

    .line 676
    const/16 p3, 0x132

    .line 678
    :cond_3
    if-eqz v2, :cond_4

    .line 679
    const/16 p3, 0x12e

    .line 681
    :cond_4
    const/4 v6, 0x0

    :try_start_1
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onFinished : isFinish-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isSuc-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isDoNothing-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", isUserCanceled-"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 684
    if-eqz v3, :cond_6

    .line 685
    const/16 v6, 0x65

    if-ne p2, v6, :cond_c

    .line 686
    const/4 v6, 0x0

    invoke-direct {p0, v6, p3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V

    .line 690
    :cond_5
    :goto_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v7, 0xcd

    invoke-static {p2, v7, p3}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, p2, p3, v6, v7}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_1 .. :try_end_1} :catch_1

    .line 700
    .end local v2    # "isDoNothing":Z
    .end local v3    # "isFinish":Z
    .end local v4    # "isSuc":Z
    .end local v5    # "isUserCanceled":Z
    :cond_6
    :goto_6
    return-void

    .line 656
    :cond_7
    const/4 v6, 0x0

    :try_start_2
    iget-object v7, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onFinished : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 657
    :catch_0
    move-exception v1

    .line 658
    .local v1, "bnre":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result p3

    .line 659
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception : rCode changed -  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 668
    .end local v1    # "bnre":Lcom/samsung/android/scloud/backup/common/BNRException;
    .restart local v2    # "isDoNothing":Z
    .restart local v3    # "isFinish":Z
    .restart local v4    # "isSuc":Z
    .restart local v5    # "isUserCanceled":Z
    :cond_8
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 669
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 670
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 671
    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 687
    :cond_c
    const/16 v6, 0x66

    if-ne p2, v6, :cond_5

    .line 688
    :try_start_3
    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p3, v6}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnRestore(ILjava/lang/String;)V
    :try_end_3
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_5

    .line 697
    .end local v2    # "isDoNothing":Z
    .end local v3    # "isFinish":Z
    .end local v4    # "isSuc":Z
    .end local v5    # "isUserCanceled":Z
    :catch_1
    move-exception v0

    .line 698
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, p2, v6, v7, p4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 695
    .end local v0    # "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    :cond_d
    :try_start_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v7, 0xcd

    invoke-static {p2, v7, p3}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, p2, p3, v6, p4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V
    :try_end_4
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_6
.end method

.method public onProgress(Ljava/lang/String;IF)V
    .locals 6
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "serviceType"    # I
    .param p3, "processed"    # F

    .prologue
    .line 630
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v0

    .line 631
    .local v0, "preProc":I
    if-ltz v0, :cond_0

    .line 632
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1, p3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->addProcessedValue(Ljava/lang/String;F)V

    .line 633
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v1

    .line 634
    .local v1, "progress":I
    if-eq v0, v1, :cond_0

    .line 635
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onProgress : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 636
    const/16 v2, 0xca

    const/16 v3, 0x12d

    invoke-virtual {p0, p2, v2, v3, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(IIII)V

    .line 639
    .end local v1    # "progress":I
    :cond_0
    return-void
.end method

.method public processBackupCleared(Ljava/lang/String;)V
    .locals 4
    .param p1, "trigger"    # Ljava/lang/String;

    .prologue
    .line 545
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    const-string v2, "processBackupCleared!!"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1, v2, p0, v3}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->backupCleared(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    .line 551
    :goto_0
    return-void

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "err processBackupCleared : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public requestCancel(Ljava/lang/String;)V
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 602
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 603
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isInProcess:Z

    if-ne v0, v2, :cond_0

    .line 604
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isCanceled:Z

    .line 605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    .line 607
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;->cancelRequestes(Ljava/lang/String;)V

    .line 609
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 610
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPaused : notify!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :goto_0
    monitor-exit v1

    .line 616
    return-void

    .line 614
    :cond_0
    const-string v0, "BNRManager_"

    const-string v2, "isInProcess is false."

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 615
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestPause()V
    .locals 4

    .prologue
    .line 593
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 594
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    .line 596
    iget v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v2, 0xcb

    const/16 v3, 0x12d

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 597
    monitor-exit v1

    .line 598
    return-void

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public requestResume()V
    .locals 4

    .prologue
    .line 619
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 620
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    .line 621
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->PAUSE_LOCK:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 622
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPaused : notify!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isPaused:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    iget v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    const/16 v2, 0xcc

    const/16 v3, 0x12d

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 624
    monitor-exit v1

    .line 625
    return-void

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public restoreStart(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "trigger"    # Ljava/lang/String;
    .param p2, "srcDevice"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x66

    .line 466
    iput v9, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mServiceType:I

    .line 468
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperation(Ljava/lang/String;)V

    .line 469
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperationOnRestore(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_1

    .line 473
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mProcessedKeyList:Ljava/util/List;

    move-object v4, p2

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/scloud/backup/core/BNRTask;->restore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_1 .. :try_end_1} :catch_0

    .line 483
    :goto_0
    return-void

    .line 474
    :catch_0
    move-exception v8

    .line 475
    .local v8, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 476
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {p0, v0, v1, v8}, Lcom/samsung/android/scloud/backup/core/BNRManager;->throwException(Ljava/lang/String;ILcom/samsung/android/scloud/backup/common/BNRException;)V
    :try_end_2
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 479
    .end local v8    # "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    :catch_1
    move-exception v7

    .line 480
    .local v7, "bnrE":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v7}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v9, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperation(IILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public sendMessageToActivity(III)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I

    .prologue
    .line 703
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLatestStatus(III)V

    .line 704
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v1

    .line 705
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 707
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 708
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 709
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/core/BackupApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 710
    return-void
.end method

.method public sendMessageToActivity(IIII)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "progress"    # I

    .prologue
    .line 726
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v1

    .line 727
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 729
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 730
    iput p4, v0, Landroid/os/Message;->arg1:I

    .line 731
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 732
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/core/BackupApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 733
    return-void
.end method

.method public sendMessageToActivity(IIILjava/lang/Object;)V
    .locals 5
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "data"    # Ljava/lang/Object;

    .prologue
    .line 713
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLatestStatus(III)V

    .line 714
    invoke-static {p1, p2, p3}, Lcom/samsung/android/scloud/backup/common/BackupConstants;->makeMassageCode(III)I

    move-result v1

    .line 715
    .local v1, "msgCode":I
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessageToActivity : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 717
    .local v0, "msg":Landroid/os/Message;
    iput v1, v0, Landroid/os/Message;->what:I

    .line 718
    const/16 v2, 0x67

    if-eq p1, v2, :cond_0

    const/16 v2, 0x6a

    if-ne p1, v2, :cond_1

    .line 719
    :cond_0
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 722
    :goto_0
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/core/BackupApp;->sendMessageToActivities(Landroid/os/Message;)V

    .line 723
    return-void

    .line 721
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0
.end method

.method public throwException(Ljava/lang/String;ILcom/samsung/android/scloud/backup/common/BNRException;)V
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "serviceType"    # I
    .param p3, "e"    # Lcom/samsung/android/scloud/backup/common/BNRException;

    .prologue
    .line 766
    iget v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 767
    invoke-virtual {p3}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager;->isFailed:I

    .line 770
    :cond_0
    invoke-virtual {p3}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/samsung/android/scloud/backup/common/BNRException;->getExceptionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->onFinished(Ljava/lang/String;IILjava/lang/Object;)V

    .line 771
    return-void
.end method
