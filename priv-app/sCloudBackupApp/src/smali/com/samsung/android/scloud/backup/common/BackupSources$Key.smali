.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupSources$Key;
.super Ljava/lang/Object;
.source "BackupSources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Key"
.end annotation


# static fields
.field public static final BLACKLIST:Ljava/lang/String; = "BLACKLIST"

.field public static final CALLREJECT:Ljava/lang/String; = "CALLREJECT"

.field public static final CALL_LOGS:Ljava/lang/String; = "CALLLOGS"

.field public static final MMS:Ljava/lang/String; = "MMS"

.field public static final SMS:Ljava/lang/String; = "SMS"

.field public static final SPAM:Ljava/lang/String; = "SPAM"

.field public static final VIPLIST:Ljava/lang/String; = "VIPLIST"

.field public static final WALLPAPER:Ljava/lang/String; = "HOMESCREEN"
