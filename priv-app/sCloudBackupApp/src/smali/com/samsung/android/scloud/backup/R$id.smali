.class public final Lcom/samsung/android/scloud/backup/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AutoBackupDialogMsg:I = 0x7f090017

.field public static final action_selectall:I = 0x7f090000

.field public static final alertSync_textview:I = 0x7f090032

.field public static final alert_textview:I = 0x7f090020

.field public static final backed_up_data_type:I = 0x7f090003

.field public static final backup_app_icon:I = 0x7f09004b

.field public static final backup_check_box:I = 0x7f09000e

.field public static final backup_list:I = 0x7f090041

.field public static final backup_list_light:I = 0x7f090043

.field public static final backup_list_linear_layout2:I = 0x7f090004

.field public static final backup_restore_warning_message:I = 0x7f090010

.field public static final backup_size:I = 0x7f090006

.field public static final backup_title_textview:I = 0x7f09000f

.field public static final below_next_layout:I = 0x7f090008

.field public static final blackSummary_textview:I = 0x7f09002d

.field public static final black_textview:I = 0x7f09002c

.field public static final blank_linearLayout1:I = 0x7f090014

.field public static final bottom_button_area:I = 0x7f090044

.field public static final calenderSummary_textview:I = 0x7f090035

.field public static final callrejectSummary_textview:I = 0x7f090031

.field public static final callreject_textview:I = 0x7f090030

.field public static final calneder_textview:I = 0x7f090034

.field public static final checkBox:I = 0x7f09001a

.field public static final checkboxAutoBackupDialog:I = 0x7f090018

.field public static final contactSummary_textview:I = 0x7f090037

.field public static final contacts_textview:I = 0x7f090036

.field public static final date:I = 0x7f09001d

.field public static final desc:I = 0x7f090049

.field public static final device_name:I = 0x7f090016

.field public static final devicedetail:I = 0x7f09001e

.field public static final devicename:I = 0x7f09001c

.field public static final dialoglist_backup_summary:I = 0x7f090021

.field public static final dialoglist_sync_summary:I = 0x7f090033

.field public static final dialoglist_top:I = 0x7f09001f

.field public static final empty_text:I = 0x7f090042

.field public static final initial_description_textview:I = 0x7f09000a

.field public static final internetSummary_textview:I = 0x7f090039

.field public static final internet_textview:I = 0x7f090038

.field public static final item_chkbox:I = 0x7f090002

.field public static final latest_backup_date:I = 0x7f090005

.field public static final learnmore_textview:I = 0x7f09000b

.field public static final linearLayout1:I = 0x7f090001

.field public static final logsSummary_textview:I = 0x7f090023

.field public static final logs_textview:I = 0x7f090022

.field public static final memoSummary_textview:I = 0x7f09003f

.field public static final memo_textview:I = 0x7f09003e

.field public static final message:I = 0x7f090019

.field public static final mmsSummary_textview:I = 0x7f090027

.field public static final mms_textview:I = 0x7f090026

.field public static final next_btn_area:I = 0x7f090011

.field public static final next_btn_arrow:I = 0x7f090013

.field public static final next_btn_text:I = 0x7f090012

.field public static final noteSummary_textview:I = 0x7f09003d

.field public static final note_textview:I = 0x7f09003c

.field public static final notification_icon:I = 0x7f09004a

.field public static final prev_btn_area:I = 0x7f090015

.field public static final prev_btn_arrow:I = 0x7f09004d

.field public static final prev_btn_text:I = 0x7f09004e

.field public static final radiobttn:I = 0x7f09001b

.field public static final restore_button:I = 0x7f090045

.field public static final restore_check_box:I = 0x7f09000c

.field public static final restore_title_textview:I = 0x7f09000d

.field public static final scrapSummary_textview:I = 0x7f09003b

.field public static final scrap_textview:I = 0x7f09003a

.field public static final screen_display:I = 0x7f090009

.field public static final skip_btn_text:I = 0x7f09004f

.field public static final smsSummary_textview:I = 0x7f090025

.field public static final sms_textview:I = 0x7f090024

.field public static final spamSummary_textview:I = 0x7f09002f

.field public static final spam_textview:I = 0x7f09002e

.field public static final status_icon:I = 0x7f090046

.field public static final status_text:I = 0x7f09004c

.field public static final time:I = 0x7f090048

.field public static final title:I = 0x7f090047

.field public static final title_text_:I = 0x7f090007

.field public static final vipSummary_textview:I = 0x7f09002b

.field public static final vip_textview:I = 0x7f09002a

.field public static final wallpaperSummary_textview:I = 0x7f090029

.field public static final wallpaper_textview:I = 0x7f090028

.field public static final warningSummary_textview:I = 0x7f090040


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
