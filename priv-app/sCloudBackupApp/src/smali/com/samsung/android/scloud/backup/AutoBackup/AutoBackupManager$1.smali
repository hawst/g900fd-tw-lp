.class Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;
.super Landroid/content/BroadcastReceiver;
.source "AutoBackupManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;->this$0:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 92
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "action":Ljava/lang/String;
    const-string v4, "AutoBackupManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[onReceive] action Name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v4, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 104
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;->this$0:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    # invokes: Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->setAutoBackupAlarm(Landroid/content/Context;)V
    invoke-static {v4, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->access$000(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;Landroid/content/Context;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v1

    .line 114
    .local v1, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v1, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->stopAutoBackupService(Landroid/content/Context;)V

    goto :goto_0

    .line 116
    .end local v1    # "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    :cond_2
    const-string v4, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 117
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;->this$0:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    # invokes: Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->disableAutoBackupAlarm(Landroid/content/Context;)V
    invoke-static {v4, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->access$100(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;Landroid/content/Context;)V

    goto :goto_0

    .line 137
    :cond_3
    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    const-string v4, "plugged"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 143
    .local v3, "plugStatus":I
    if-eq v3, v2, :cond_4

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 146
    .local v2, "bPlugged":Z
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager$1;->this$0:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;

    # getter for: Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->metaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;->access$200(Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setChargerStatus(Z)V

    .line 147
    const-string v4, "AutoBackupManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[onReceive] action Name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v4, "AutoBackupManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[onReceive] chargerStatus : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 143
    .end local v2    # "bPlugged":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method
