.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupConstants$DIALOG;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DIALOG"
.end annotation


# static fields
.field public static final AUTH_OK:I = 0x0

.field public static final BACKUP_FAILED:I = 0xd

.field public static final BACKUP_NOW:I = 0xb

.field public static final DOWNLOAD_RESTORE_PROGRESS:I = 0xc

.field public static final INSUFFICIENT_SPACE:I = 0x13

.field public static final LOSE_BACKUP_DATA_RESTORE_NOT_SELECTED:I = 0x7

.field public static final NONE:I = -0x1

.field public static final NOT_BACKEDUP_5DAYS:I = 0x12

.field public static final QUOTA_FAILED:I = 0xe

.field public static final RESTORE_FAILED_DIALOG:I = 0x9

.field public static final RESTORE_PROGRESS:I = 0x4

.field public static final RESTORE_START_AT_SETUPWIZARD_COMPLETION_ALERT:I = 0x14

.field public static final RETRY_BACKUP:I = 0x8

.field public static final RETRY_BACKUPDETAIL:I = 0xf

.field public static final RETRY_RESTORE:I = 0x5

.field public static final SHOW_PROGRESS:I = 0x1

.field public static final STOP_BACKUP:I = 0x10

.field public static final STOP_RESTORE:I = 0x11
