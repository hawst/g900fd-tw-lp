.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupConstants$ServiceType;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ServiceType"
.end annotation


# static fields
.field public static final ACCOUNT_REMOVED:I = 0x69

.field public static final BACKUP:I = 0x65

.field public static final BACKUP_DELETE:I = 0x6b

.field public static final CANCEL_NOTIFICATION:I = 0x68

.field public static final EXTRACT_KEY:I = 0xf4240

.field public static final GET_DETAILS:I = 0x67

.field public static final GET_USAGE:I = 0x6a

.field public static final NONE:I = 0x64

.field public static final RESTORE:I = 0x66
