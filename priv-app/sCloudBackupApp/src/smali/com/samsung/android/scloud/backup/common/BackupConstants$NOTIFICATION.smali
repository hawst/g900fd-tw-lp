.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupConstants$NOTIFICATION;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "NOTIFICATION"
.end annotation


# static fields
.field public static final BACKUP_FAILED:I = 0x2

.field public static final BACKUP_PROGRESS:I = 0x1

.field public static final INSUFFICIENT_SPACE:I = 0x6

.field public static final QUOTA_FAILED:I = 0x3

.field public static final RESTORE_FAILED:I = 0x5

.field public static final RESTORE_PROGRESS:I = 0x4
