.class public Lcom/samsung/android/scloud/backup/util/ControlRegionalData;
.super Ljava/lang/Object;
.source "ControlRegionalData.java"


# static fields
.field public static final DEVICE_CAMERA:Ljava/lang/String; = "gd1"

.field public static final DEVICE_CAMERA_ATT:Ljava/lang/String; = "gd1att"

.field public static final DEVICE_CAMERA_U0:Ljava/lang/String; = "u0lte"

.field private static final REG_CHN_CMCC:Ljava/lang/String; = "zm"

.field private static final REG_CHN_COUNTRY_ISO:Ljava/lang/String; = "CN"

.field private static final REG_CHN_CTC:Ljava/lang/String; = "tc"

.field private static final REG_CHN_OPEN:Ljava/lang/String; = "zc"

.field private static final REG_EUR_OPEN:Ljava/lang/String; = "xx"

.field private static final REG_JP_DCM:Ljava/lang/String; = "DCM"

.field private static final REG_JP_KDI:Ljava/lang/String; = "KDI"

.field public static final REG_KOR_COUNTRY_CODE:Ljava/lang/String; = "KOREA"

.field private static final REG_US_COUNTRY_ISO:Ljava/lang/String; = "US"

.field private static final TAG:Ljava/lang/String; = "ControlRegionData"

.field public static final TYPE_CAMERA:I = 0x1

.field public static final TYPE_CAMERA_ATT:I = 0x2

.field public static final TYPE_DEFAULT:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCountryCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    const-string v6, "xx"

    .line 231
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    const-string v8, "android.os.SystemProperties"

    invoke-virtual {v7, v8}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 233
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 234
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 236
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 239
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    .line 240
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/String;

    const-string v9, "ro.csc.country_code"

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 242
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 249
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 244
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 245
    .local v3, "iAE":Ljava/lang/IllegalArgumentException;
    const-string v6, "xx"

    .line 248
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 246
    .end local v3    # "iAE":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 247
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "xx"

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getCountryIsoCode(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    const-string v6, "xx"

    .line 206
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    const-string v8, "android.os.SystemProperties"

    invoke-virtual {v7, v8}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 208
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 209
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 211
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 214
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    .line 215
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/String;

    const-string v9, "ro.csc.countryiso_code"

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 217
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 224
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 219
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 220
    .local v3, "iAE":Ljava/lang/IllegalArgumentException;
    const-string v6, "xx"

    .line 223
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 221
    .end local v3    # "iAE":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "xx"

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getDeviceRegionInfo(I)Ljava/lang/String;
    .locals 6
    .param p0, "len"    # I

    .prologue
    .line 161
    const-string v1, "xx"

    .line 163
    .local v1, "ret":Ljava/lang/String;
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    if-nez v3, :cond_0

    move-object v2, v1

    .line 174
    .end local v1    # "ret":Ljava/lang/String;
    .local v2, "ret":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 167
    .end local v2    # "ret":Ljava/lang/String;
    .restart local v1    # "ret":Ljava/lang/String;
    :cond_0
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 168
    .local v0, "length":I
    const/4 v3, 0x3

    if-le v0, v3, :cond_1

    .line 169
    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    sub-int v4, v0, p0

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    const-string v3, "ControlRegionalData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "=========== Region Information :  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v2, v1

    .line 174
    .end local v1    # "ret":Ljava/lang/String;
    .restart local v2    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 172
    .end local v2    # "ret":Ljava/lang/String;
    .restart local v1    # "ret":Ljava/lang/String;
    :cond_1
    const-string v3, "StringPackage"

    const-string v4, "Product Name is too short. Use Opea Value. "

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static getSalesInfo(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    const-string v6, "xx"

    .line 181
    .local v6, "ret":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    const-string v8, "android.os.SystemProperties"

    invoke-virtual {v7, v8}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 183
    .local v0, "SystemProperties":Ljava/lang/Class;
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 184
    .local v4, "paramTypes":[Ljava/lang/Class;
    const/4 v7, 0x0

    const-class v8, Ljava/lang/String;

    aput-object v8, v4, v7

    .line 186
    const-string v7, "get"

    invoke-virtual {v0, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 189
    .local v2, "get":Ljava/lang/reflect/Method;
    const/4 v7, 0x1

    new-array v5, v7, [Ljava/lang/Object;

    .line 190
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    new-instance v8, Ljava/lang/String;

    const-string v9, "ro.csc.sales_code"

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v8, v5, v7

    .line 192
    invoke-virtual {v2, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "ret":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 199
    .end local v0    # "SystemProperties":Ljava/lang/Class;
    .end local v2    # "get":Ljava/lang/reflect/Method;
    .end local v4    # "paramTypes":[Ljava/lang/Class;
    .end local v5    # "params":[Ljava/lang/Object;
    .restart local v6    # "ret":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 194
    .end local v6    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 195
    .local v3, "iAE":Ljava/lang/IllegalArgumentException;
    const-string v6, "xx"

    .line 198
    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 196
    .end local v3    # "iAE":Ljava/lang/IllegalArgumentException;
    .end local v6    # "ret":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 197
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "xx"

    .restart local v6    # "ret":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getStringIdByRegion(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "orgTextId"    # I

    .prologue
    const v0, 0x7f07007a

    .line 35
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->isChinaDevice(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    sparse-switch p1, :sswitch_data_0

    .line 54
    .end local p1    # "orgTextId":I
    :cond_0
    :goto_0
    return p1

    .line 38
    .restart local p1    # "orgTextId":I
    :sswitch_0
    const p1, 0x7f070074

    goto :goto_0

    :sswitch_1
    move p1, v0

    .line 40
    goto :goto_0

    .line 42
    :sswitch_2
    const p1, 0x7f07007d

    goto :goto_0

    .line 44
    :sswitch_3
    const p1, 0x7f070076

    goto :goto_0

    .line 46
    :sswitch_4
    const p1, 0x7f070014

    goto :goto_0

    :sswitch_5
    move p1, v0

    .line 48
    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f070013 -> :sswitch_4
        0x7f070073 -> :sswitch_0
        0x7f070075 -> :sswitch_3
        0x7f070079 -> :sswitch_1
        0x7f07007c -> :sswitch_2
        0x7f070082 -> :sswitch_5
    .end sparse-switch
.end method

.method public static isAvailableList(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 70
    const-string v2, "ControlRegionData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAvailableList is called. sourceKey :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getSalesInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 73
    .local v1, "salesCode":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string v2, "DCM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    const-string v2, "MMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 75
    const/4 v0, 0x0

    .line 87
    :cond_0
    :goto_0
    const-string v2, "ControlRegionData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAvailableList is ended. source result :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return v0

    .line 76
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 78
    :cond_2
    if-eqz v1, :cond_0

    const-string v2, "KDI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    const-string v2, "MMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 80
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_3
    const-string v2, "SMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 82
    const/4 v0, 0x0

    goto :goto_0

    .line 83
    :cond_4
    const-string v2, "SPAM"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 84
    const/4 v0, 0x0

    goto :goto_0

    .line 85
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isCameraDevice()I
    .locals 7

    .prologue
    .line 132
    const/4 v3, 0x0

    .line 133
    .local v3, "ret":I
    const-string v0, "DeviceName_is_NONE"

    .line 134
    .local v0, "deviceName":Ljava/lang/String;
    const-string v1, "DeviceName_is_NONE"

    .line 136
    .local v1, "deviceName2":Ljava/lang/String;
    :try_start_0
    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 137
    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 142
    :goto_0
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This device is ............. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This device is ............. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v5, "gd1att"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 146
    const/4 v3, 0x2

    .line 153
    :goto_1
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This device type is ............. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This PRODUCT(ro.product.name)  is ............. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This device is ............. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    return v3

    .line 138
    :catch_0
    move-exception v2

    .line 139
    .local v2, "ex":Ljava/lang/IndexOutOfBoundsException;
    const-string v4, "ControlRegionData"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ro.product.nama is too short. : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 147
    .end local v2    # "ex":Ljava/lang/IndexOutOfBoundsException;
    :cond_0
    const-string v4, "gd1"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "u0lte"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 148
    :cond_1
    const/4 v3, 0x1

    goto :goto_1

    .line 150
    :cond_2
    const-string v4, "ControlRegionData"

    const-string v5, "This device is not camera device. "

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isChinaDevice(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    const/4 v2, 0x0

    .line 107
    .local v2, "ret":Z
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getDeviceRegionInfo(I)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "regInfo":Ljava/lang/String;
    const-string v3, "zc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "tc"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "zm"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    :cond_0
    const/4 v2, 0x1

    .line 115
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getCountryIsoCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "countryIso":Ljava/lang/String;
    const-string v3, "CN"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 117
    const/4 v2, 0x1

    .line 120
    :cond_2
    return v2
.end method

.method public static isJapanDevice(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 61
    .local v0, "ret":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getSalesInfo(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "salesCode":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "DCM"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "KDI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 63
    :cond_0
    const/4 v0, 0x1

    .line 66
    :cond_1
    return v0
.end method

.method public static isUSDevice(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 95
    .local v1, "ret":Z
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getCountryIsoCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "countryIso":Ljava/lang/String;
    const-string v2, "US"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    const/4 v1, 0x1

    .line 100
    :cond_0
    return v1
.end method
