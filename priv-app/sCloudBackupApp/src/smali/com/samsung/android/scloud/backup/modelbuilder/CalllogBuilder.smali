.class public Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;
.super Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;
.source "CalllogBuilder.java"


# static fields
.field public static final LOG_TYPE_CALL:I = 0x64

.field public static final LOG_TYPE_MMS:I = 0xc8

.field public static final LOG_TYPE_SMS:I = 0x12c

.field public static final LOG_TYPE_VIDEO:I = 0x1f4

.field public static final LOG_TYPE_VOLTE:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "CalllogBuilder"


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "mModel"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 81
    return-void
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 491
    return-void
.end method

.method public getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;
    .locals 39
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "start"    # I
    .param p4, "maxCount"    # I
    .param p5, "maxSize"    # J
    .param p7, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    .local p2, "serverkey":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "CalllogBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM() is called : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v8}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v23, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/16 v26, 0x0

    .line 116
    .local v26, "phnDB":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 119
    .local v16, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v17, Ljava/io/FileWriter;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p7

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-direct {v0, v4, v7}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    .end local v16    # "fw":Ljava/io/FileWriter;
    .local v17, "fw":Ljava/io/FileWriter;
    :try_start_1
    const-string v4, "["

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 122
    const-wide/16 v10, 0x0

    .line 123
    .local v10, "count":J
    const-wide/16 v34, 0x0

    .line 124
    .local v34, "totalLength":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getSourceProjection()[Ljava/lang/String;

    move-result-object v6

    .line 125
    .local v6, "projection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v4}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 126
    .local v5, "oemUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getWhere()Ljava/lang/String;

    move-result-object v36

    .line 127
    .local v36, "where":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getOrderByColumnName()Ljava/lang/String;

    move-result-object v27

    .line 128
    .local v27, "sortOrder":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getIDKey(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getWhereKey([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    .line 129
    .local v37, "whereKey":Ljava/lang/String;
    const/16 v25, 0x1f4

    .line 130
    .local v25, "limit":I
    const/16 v21, 0x1

    .line 132
    .local v21, "isNext":Z
    :goto_0
    if-eqz v21, :cond_0

    .line 134
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getSelectionID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v37

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v38, " ASC"

    move-object/from16 v0, v38

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v38, " LIMIT "

    move-object/from16 v0, v38

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v38, " OFFSET "

    move-object/from16 v0, v38

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 142
    if-nez v26, :cond_3

    .line 143
    const/16 v21, 0x0

    .line 245
    :cond_0
    :goto_1
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 246
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V

    .line 247
    const-string v4, "CalllogBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM is Done !! : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v8}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " returned item count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 253
    if-eqz v26, :cond_1

    .line 254
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_1
    if-eqz v17, :cond_2

    .line 258
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 264
    :cond_2
    :goto_2
    return-object v23

    .line 147
    :cond_3
    :try_start_3
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_4

    .line 148
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 149
    const/16 v21, 0x0

    .line 150
    goto :goto_1

    .line 152
    :cond_4
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int p3, p3, v4

    .line 154
    :goto_3
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 155
    move/from16 v0, p4

    int-to-long v8, v0

    cmp-long v4, v10, v8

    if-ltz v4, :cond_7

    .line 156
    if-eqz v26, :cond_5

    .line 157
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 158
    const/16 v26, 0x0

    .line 160
    :cond_5
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 161
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V

    .line 163
    const-string v4, "CalllogBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM is Done !! : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v8}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " returned item count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 253
    if-eqz v26, :cond_6

    .line 254
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_6
    if-eqz v17, :cond_2

    .line 258
    :try_start_4
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 259
    :catch_0
    move-exception v14

    .line 261
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 170
    .end local v14    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_5
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 173
    .local v12, "data":Lorg/json/JSONObject;
    :try_start_6
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v12

    .line 178
    :try_start_7
    const-string v4, "_id"

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 180
    .local v20, "idIndex":I
    const-wide/16 v18, -0x1

    .line 181
    .local v18, "id":J
    const/4 v4, -0x1

    move/from16 v0, v20

    if-eq v0, v4, :cond_8

    .line 182
    move-object/from16 v0, v26

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 184
    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BACKUP_CALLLOGS_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, "_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v24

    .line 189
    .local v24, "key":Ljava/lang/String;
    :try_start_8
    const-string v4, "transaction_id"

    const-string v7, "0"

    invoke-virtual {v12, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 194
    :try_start_9
    new-instance v33, Lorg/json/JSONObject;

    invoke-direct/range {v33 .. v33}, Lorg/json/JSONObject;-><init>()V

    .line 195
    .local v33, "totalData":Lorg/json/JSONObject;
    const-string v4, "date"

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 196
    .local v13, "dateIndex":I
    move-object/from16 v0, v26

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 197
    .local v30, "timeStamp":J
    const-wide/16 v8, 0x0

    cmp-long v4, v30, v8

    if-gtz v4, :cond_9

    .line 198
    const-wide v30, 0xe8d4a51000L

    .line 199
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->getLength(J)I

    move-result v4

    const/16 v7, 0xd

    if-ge v4, v7, :cond_c

    .line 200
    const-wide/16 v8, 0xa

    mul-long v30, v30, v8

    goto :goto_4

    .line 174
    .end local v13    # "dateIndex":I
    .end local v18    # "id":J
    .end local v20    # "idIndex":I
    .end local v24    # "key":Ljava/lang/String;
    .end local v30    # "timeStamp":J
    .end local v33    # "totalData":Lorg/json/JSONObject;
    :catch_1
    move-exception v15

    .line 175
    .local v15, "e1":Lorg/json/JSONException;
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x130

    invoke-direct {v4, v7, v15}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 250
    .end local v5    # "oemUri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":J
    .end local v12    # "data":Lorg/json/JSONObject;
    .end local v15    # "e1":Lorg/json/JSONException;
    .end local v21    # "isNext":Z
    .end local v25    # "limit":I
    .end local v27    # "sortOrder":Ljava/lang/String;
    .end local v34    # "totalLength":J
    .end local v36    # "where":Ljava/lang/String;
    .end local v37    # "whereKey":Ljava/lang/String;
    :catch_2
    move-exception v15

    move-object/from16 v16, v17

    .line 251
    .end local v17    # "fw":Ljava/io/FileWriter;
    .local v15, "e1":Ljava/io/IOException;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :goto_5
    :try_start_a
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x13a

    invoke-direct {v4, v7, v15}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 253
    .end local v15    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_6
    if-eqz v26, :cond_a

    .line 254
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_a
    if-eqz v16, :cond_b

    .line 258
    :try_start_b
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    .line 262
    :cond_b
    :goto_7
    throw v4

    .line 190
    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v5    # "oemUri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v10    # "count":J
    .restart local v12    # "data":Lorg/json/JSONObject;
    .restart local v17    # "fw":Ljava/io/FileWriter;
    .restart local v18    # "id":J
    .restart local v20    # "idIndex":I
    .restart local v21    # "isNext":Z
    .restart local v24    # "key":Ljava/lang/String;
    .restart local v25    # "limit":I
    .restart local v27    # "sortOrder":Ljava/lang/String;
    .restart local v34    # "totalLength":J
    .restart local v36    # "where":Ljava/lang/String;
    .restart local v37    # "whereKey":Ljava/lang/String;
    :catch_3
    move-exception v14

    .line 191
    .local v14, "e":Lorg/json/JSONException;
    :try_start_c
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x130

    invoke-direct {v4, v7, v14}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 253
    .end local v5    # "oemUri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":J
    .end local v12    # "data":Lorg/json/JSONObject;
    .end local v14    # "e":Lorg/json/JSONException;
    .end local v18    # "id":J
    .end local v20    # "idIndex":I
    .end local v21    # "isNext":Z
    .end local v24    # "key":Ljava/lang/String;
    .end local v25    # "limit":I
    .end local v27    # "sortOrder":Ljava/lang/String;
    .end local v34    # "totalLength":J
    .end local v36    # "where":Ljava/lang/String;
    .end local v37    # "whereKey":Ljava/lang/String;
    :catchall_1
    move-exception v4

    move-object/from16 v16, v17

    .end local v17    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    goto :goto_6

    .line 204
    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v5    # "oemUri":Landroid/net/Uri;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v10    # "count":J
    .restart local v12    # "data":Lorg/json/JSONObject;
    .restart local v13    # "dateIndex":I
    .restart local v17    # "fw":Ljava/io/FileWriter;
    .restart local v18    # "id":J
    .restart local v20    # "idIndex":I
    .restart local v21    # "isNext":Z
    .restart local v24    # "key":Ljava/lang/String;
    .restart local v25    # "limit":I
    .restart local v27    # "sortOrder":Ljava/lang/String;
    .restart local v30    # "timeStamp":J
    .restart local v33    # "totalData":Lorg/json/JSONObject;
    .restart local v34    # "totalLength":J
    .restart local v36    # "where":Ljava/lang/String;
    .restart local v37    # "whereKey":Ljava/lang/String;
    :cond_c
    :try_start_d
    const-string v4, "value"

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 205
    const-string v4, "timestamp"

    move-object/from16 v0, v33

    move-wide/from16 v1, v30

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 206
    const-string v4, "key"

    move-object/from16 v0, v33

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_d
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 211
    :try_start_e
    invoke-virtual/range {v33 .. v33}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v32

    .line 213
    .local v32, "toStr":Ljava/lang/String;
    const-wide/16 v28, 0x0

    .line 214
    .local v28, "subLength":J
    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v28, v0

    .line 216
    add-long v8, v34, v28

    cmp-long v4, v8, p5

    if-ltz v4, :cond_e

    .line 217
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 218
    const/16 v26, 0x0

    .line 219
    const-string v4, "CalllogBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getItemFromOEM is Done !! : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v8}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " returned item count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", totalLength : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v34

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v4, "]"

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 225
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 253
    if-eqz v26, :cond_d

    .line 254
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_d
    if-eqz v17, :cond_2

    .line 258
    :try_start_f
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4

    goto/16 :goto_2

    .line 259
    :catch_4
    move-exception v14

    .line 261
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 207
    .end local v14    # "e":Ljava/io/IOException;
    .end local v28    # "subLength":J
    .end local v32    # "toStr":Ljava/lang/String;
    :catch_5
    move-exception v14

    .line 208
    .local v14, "e":Lorg/json/JSONException;
    :try_start_10
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v7, 0x130

    invoke-direct {v4, v7, v14}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4

    .line 228
    .end local v14    # "e":Lorg/json/JSONException;
    .restart local v28    # "subLength":J
    .restart local v32    # "toStr":Ljava/lang/String;
    :cond_e
    add-long v34, v34, v28

    .line 229
    new-instance v22, Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 230
    .local v22, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    move-object/from16 v0, v22

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 231
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_10

    const-wide/16 v8, 0x0

    cmp-long v4, v10, v8

    if-lez v4, :cond_10

    .line 232
    :cond_f
    const-string v4, ","

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 234
    :cond_10
    move-object/from16 v0, v17

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 235
    invoke-virtual/range {v17 .. v17}, Ljava/io/FileWriter;->flush()V

    .line 236
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    const-wide/16 v8, 0x1

    add-long/2addr v10, v8

    .line 240
    goto/16 :goto_3

    .line 241
    .end local v12    # "data":Lorg/json/JSONObject;
    .end local v13    # "dateIndex":I
    .end local v18    # "id":J
    .end local v20    # "idIndex":I
    .end local v22    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v24    # "key":Ljava/lang/String;
    .end local v28    # "subLength":J
    .end local v30    # "timeStamp":J
    .end local v32    # "toStr":Ljava/lang/String;
    .end local v33    # "totalData":Lorg/json/JSONObject;
    :cond_11
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    .line 242
    const/16 v26, 0x0

    goto/16 :goto_0

    .line 259
    :catch_6
    move-exception v14

    .line 261
    .local v14, "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 259
    .end local v5    # "oemUri":Landroid/net/Uri;
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v10    # "count":J
    .end local v14    # "e":Ljava/io/IOException;
    .end local v17    # "fw":Ljava/io/FileWriter;
    .end local v21    # "isNext":Z
    .end local v25    # "limit":I
    .end local v27    # "sortOrder":Ljava/lang/String;
    .end local v34    # "totalLength":J
    .end local v36    # "where":Ljava/lang/String;
    .end local v37    # "whereKey":Ljava/lang/String;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :catch_7
    move-exception v14

    .line 261
    .restart local v14    # "e":Ljava/io/IOException;
    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    .line 250
    .end local v14    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v15

    goto/16 :goto_5
.end method

.method public getOrderByColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-string v0, "_id"

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 477
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    .line 478
    .local v0, "projection":[Ljava/lang/String;
    return-object v0
.end method

.method public getSourceProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 86
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "number"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "logtype"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "m_subject"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "m_content"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_read"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "new"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getWhere()Ljava/lang/String;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v0, "whereStr":Ljava/lang/StringBuilder;
    const-string v1, "logtype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IN ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public postOperationOnBackup(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSuccess"    # Z

    .prologue
    .line 485
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 472
    return-void
.end method

.method public putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    .locals 29
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 340
    .local p3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p4, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v26

    if-nez v26, :cond_1

    .line 341
    :cond_0
    const-string v26, "CalllogBuilder"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "putItemToOEM() is Done !! : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " : itemList is zero or null"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const/16 v26, 0x0

    .line 438
    :goto_0
    return v26

    .line 345
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    .line 346
    .local v17, "mResolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v19

    .line 347
    .local v19, "oemUri":Landroid/net/Uri;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v20, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 349
    .local v4, "CV":Landroid/content/ContentValues;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 351
    .local v6, "cursor":Landroid/content/ContentValues;
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 352
    .local v15, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 353
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 354
    if-nez v15, :cond_4

    .line 355
    const-string v26, "CalllogBuilder"

    const-string v27, "putDataToOEM(): item is null"

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    .end local v15    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_2
    const/4 v11, 0x0

    .line 394
    .local v11, "insertedResult":[Landroid/content/ContentProviderResult;
    const/16 v23, 0x0

    .line 396
    .local v23, "start":I
    const/16 v18, 0xfa

    .line 398
    .local v18, "maxBatchSize":I
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v22

    .line 400
    .local v22, "size":I
    new-instance v24, Ljava/util/ArrayList;

    const/16 v26, 0xfa

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 402
    .local v24, "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const/4 v9, 0x0

    .line 403
    .end local v10    # "i$":Ljava/util/Iterator;
    .local v9, "end":I
    :goto_2
    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_a

    .line 404
    move/from16 v0, v23

    add-int/lit16 v9, v0, 0xfa

    .line 405
    move/from16 v0, v22

    if-ge v0, v9, :cond_3

    .line 406
    move/from16 v9, v22

    .line 408
    :cond_3
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->clear()V

    .line 409
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1, v9}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 411
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemAuthority()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v11

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v26, v0

    invoke-interface/range {v26 .. v26}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x66

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 420
    if-eqz v11, :cond_9

    .line 421
    move-object v5, v11

    .local v5, "arr$":[Landroid/content/ContentProviderResult;
    array-length v0, v5

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_3
    move/from16 v0, v16

    if-ge v10, v0, :cond_8

    aget-object v21, v5, v10

    .line 422
    .local v21, "result":Landroid/content/ContentProviderResult;
    move-object/from16 v0, v21

    iget-object v14, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 423
    .local v14, "insertedUri":Landroid/net/Uri;
    invoke-static {v14}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v12

    .line 424
    .local v12, "insertedId":J
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p4

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 358
    .end local v5    # "arr$":[Landroid/content/ContentProviderResult;
    .end local v9    # "end":I
    .end local v11    # "insertedResult":[Landroid/content/ContentProviderResult;
    .end local v12    # "insertedId":J
    .end local v14    # "insertedUri":Landroid/net/Uri;
    .end local v16    # "len$":I
    .end local v18    # "maxBatchSize":I
    .end local v21    # "result":Landroid/content/ContentProviderResult;
    .end local v22    # "size":I
    .end local v23    # "start":I
    .end local v24    # "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .local v10, "i$":Ljava/util/Iterator;
    .restart local v15    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_4
    invoke-virtual {v15}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v7

    .line 359
    .local v7, "data":Lorg/json/JSONObject;
    new-instance v25, Lorg/json/JSONObject;

    const-string v26, "value"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 361
    .local v25, "totalData":Lorg/json/JSONObject;
    invoke-static/range {v25 .. v25}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v6

    .line 362
    const-string v26, "number"

    const-string v27, "number"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v26, "date"

    const-string v27, "date"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 364
    const-string v26, "duration"

    const-string v27, "duration"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v26, "name"

    const-string v27, "name"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const-string v26, "type"

    const-string v27, "type"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v26, "logtype"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    if-lez v26, :cond_5

    .line 368
    const-string v26, "logtype"

    const-string v27, "logtype"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 372
    :goto_4
    const-string v26, "m_subject"

    const-string v27, "m_subject"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v26, "m_content"

    const-string v27, "m_content"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v26, "messageid"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 375
    const-string v26, "sp_type"

    const-string v27, " "

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v26, "contactid"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 378
    const-string v26, "is_read"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 379
    const-string v26, "is_read"

    const-string v27, "is_read"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 383
    :goto_5
    const-string v26, "new"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 384
    const-string v26, "new"

    const-string v27, "new"

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 388
    :goto_6
    invoke-static/range {v19 .. v19}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 370
    :cond_5
    const-string v26, "logtype"

    const/16 v27, 0x64

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 381
    :cond_6
    const-string v26, "is_read"

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_5

    .line 386
    :cond_7
    const-string v26, "new"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_6

    .line 414
    .end local v7    # "data":Lorg/json/JSONObject;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v15    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v25    # "totalData":Lorg/json/JSONObject;
    .restart local v9    # "end":I
    .restart local v11    # "insertedResult":[Landroid/content/ContentProviderResult;
    .restart local v18    # "maxBatchSize":I
    .restart local v22    # "size":I
    .restart local v23    # "start":I
    .restart local v24    # "subOperations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    :catch_0
    move-exception v8

    .line 415
    .local v8, "e":Landroid/os/RemoteException;
    const/16 v26, 0x0

    goto/16 :goto_0

    .line 416
    .end local v8    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v8

    .line 417
    .local v8, "e":Landroid/content/OperationApplicationException;
    const/16 v26, 0x0

    goto/16 :goto_0

    .line 426
    .end local v8    # "e":Landroid/content/OperationApplicationException;
    .restart local v5    # "arr$":[Landroid/content/ContentProviderResult;
    .local v10, "i$":I
    .restart local v16    # "len$":I
    :cond_8
    const-string v26, "CalllogBuilder"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "putItemToOEM() is Done !!!  "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " inserted item size : "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    move/from16 v23, v9

    goto/16 :goto_2

    .line 431
    .end local v5    # "arr$":[Landroid/content/ContentProviderResult;
    .end local v10    # "i$":I
    .end local v16    # "len$":I
    :cond_9
    const-string v26, "CalllogBuilder"

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "putItemToOEM() FAIL :"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v28, v0

    invoke-interface/range {v28 .. v28}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " : Not inserted data "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const/16 v26, 0x0

    goto/16 :goto_0

    .line 438
    :cond_a
    const/16 v26, 0x1

    goto/16 :goto_0
.end method
