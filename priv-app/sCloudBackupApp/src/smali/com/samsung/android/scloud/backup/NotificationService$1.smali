.class Lcom/samsung/android/scloud/backup/NotificationService$1;
.super Landroid/os/Handler;
.source "NotificationService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/NotificationService;->initHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/NotificationService;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/NotificationService;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 398
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 400
    :pswitch_0
    const-string v4, "NotificationService"

    const-string v5, "[BACKUP] REMOTE_STATUS_TIMEOUT"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 402
    .local v2, "mCurrentTime":J
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$000(Lcom/samsung/android/scloud/backup/NotificationService;)J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/32 v6, 0x3a980

    cmp-long v4, v4, v6

    if-lez v4, :cond_6

    .line 403
    const-string v4, "NotificationService"

    const-string v5, "[It is time to Cancel Network-Operation. It is not normal!]"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->requestCancel()V

    .line 405
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 406
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # invokes: Lcom/samsung/android/scloud/backup/NotificationService;->removeRemoteTimeoutMessage(I)V
    invoke-static {v4, v8}, Lcom/samsung/android/scloud/backup/NotificationService;->access$200(Lcom/samsung/android/scloud/backup/NotificationService;I)V

    .line 407
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$300(Lcom/samsung/android/scloud/backup/NotificationService;)I

    move-result v4

    const/16 v5, 0x65

    if-ne v4, v5, :cond_3

    .line 408
    const-string v4, "NotificationService"

    const-string v5, "[BACKUP Network-Operation is not normal!]"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 410
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 411
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v9, :cond_1

    .line 412
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 413
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1, v9, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto :goto_1

    .line 415
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/NotificationService;->onBackupFailed(Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 417
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$300(Lcom/samsung/android/scloud/backup/NotificationService;)I

    move-result v4

    const/16 v5, 0x66

    if-ne v4, v5, :cond_0

    .line 418
    const-string v4, "NotificationService"

    const-string v5, "[RESTORE Network-Operation is not normal!]"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityRestoreFailed()V

    .line 420
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 421
    .restart local v1    # "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v9, :cond_4

    .line 422
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 423
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # getter for: Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v4, v1, v8, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto :goto_2

    .line 425
    .end local v1    # "key":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/NotificationService;->onRestoreFailed()V

    goto/16 :goto_0

    .line 428
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_6
    const-string v4, "NotificationService"

    const-string v5, "[handleMessage] Send Remote Message "

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService$1;->this$0:Lcom/samsung/android/scloud/backup/NotificationService;

    # invokes: Lcom/samsung/android/scloud/backup/NotificationService;->sendRemoteTimeoutMessage(I)V
    invoke-static {v4, v8}, Lcom/samsung/android/scloud/backup/NotificationService;->access$400(Lcom/samsung/android/scloud/backup/NotificationService;I)V

    goto/16 :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
