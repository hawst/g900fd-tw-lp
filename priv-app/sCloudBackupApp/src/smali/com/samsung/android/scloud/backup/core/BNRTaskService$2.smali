.class final Lcom/samsung/android/scloud/backup/core/BNRTaskService$2;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 94
    const-string v8, "SOURCE_LIST"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 95
    .local v5, "sourceList":[Ljava/lang/String;
    const-string v8, "TRIGGER"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 96
    .local v7, "trigger":Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 97
    .local v4, "source":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/scloud/backup/core/BNRTaskService$2$1;

    invoke-direct {v3, p0, v4, p1, v7}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$2$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$2;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 110
    .local v3, "runnable":Ljava/lang/Runnable;
    new-instance v6, Ljava/lang/Thread;

    const-string v8, "REQUEST_BACKUP"

    invoke-direct {v6, v3, v8}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 111
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v3    # "runnable":Ljava/lang/Runnable;
    .end local v4    # "source":Ljava/lang/String;
    .end local v6    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
