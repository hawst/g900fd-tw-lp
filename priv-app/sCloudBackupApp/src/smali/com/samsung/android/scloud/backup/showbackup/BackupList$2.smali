.class Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupList;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 172
    move v1, p3

    .line 173
    .local v1, "posn":I
    const-string v2, "BackupList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClick arg1 :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    if-eqz p2, :cond_0

    .line 176
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;

    .line 177
    .local v0, "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    if-eqz v0, :cond_0

    .line 178
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 180
    iget-object v5, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 181
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;->cb:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 182
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # operator++ for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$108(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    .line 183
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v1, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 184
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aput-boolean v3, v2, v1

    .line 190
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # getter for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$100(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    move-result v2

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v5, v5, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v5

    if-ne v2, v5, :cond_3

    .line 191
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 201
    .end local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    :cond_0
    :goto_2
    return-void

    .restart local v0    # "holder":Lcom/samsung/android/scloud/backup/showbackup/BackupList$ViewHolder;
    :cond_1
    move v2, v4

    .line 180
    goto :goto_0

    .line 186
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # operator-- for: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->count:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$110(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)I

    .line 187
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v2, v1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 188
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mListSelected:[Z

    aput-boolean v4, v2, v1

    goto :goto_1

    .line 194
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$2;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v2, v2, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2
.end method
