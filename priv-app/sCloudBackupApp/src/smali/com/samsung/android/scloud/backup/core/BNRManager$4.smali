.class Lcom/samsung/android/scloud/backup/core/BNRManager$4;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

.field final synthetic val$cnt:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager;I)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iput p2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->val$cnt:I

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 7
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 296
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    sparse-switch p1, :sswitch_data_0

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 299
    :sswitch_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->commitLastBackupTime()V

    .line 300
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setFirstBackup(Ljava/lang/String;Z)V

    .line 301
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$300(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setOperationFinished(Ljava/lang/String;Z)V

    .line 302
    const-string v2, "storageinfo"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 305
    :try_start_0
    const-string v2, "storageinfo"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 306
    .local v1, "storageJson":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # invokes: Lcom/samsung/android/scloud/backup/core/BNRManager;->notifyQuota(Lorg/json/JSONObject;)V
    invoke-static {v2, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$800(Lcom/samsung/android/scloud/backup/core/BNRManager;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 307
    .end local v1    # "storageJson":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 316
    .end local v0    # "e":Lorg/json/JSONException;
    :sswitch_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit :  - Out of storage. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x137

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 325
    :sswitch_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit :  - Not ready "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->val$cnt:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    .line 329
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit sleep for retry - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 334
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->val$cnt:I

    add-int/lit8 v3, v3, 0x1

    # invokes: Lcom/samsung/android/scloud/backup/core/BNRManager;->postOperationOnBackup(II)V
    invoke-static {v2, v3, p1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$900(Lcom/samsung/android/scloud/backup/core/BNRManager;II)V

    goto/16 :goto_0

    .line 331
    :catch_1
    move-exception v0

    .line 332
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "backupClearCommit retry sleep err"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 336
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x136

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupClearCommit failed : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 342
    :sswitch_3
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit :  - Not ready. Data was cleared. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$400(Lcom/samsung/android/scloud/backup/core/BNRManager;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$500(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v5}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v5

    new-instance v6, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/core/BNRManager$4$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRManager$4;)V

    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->releaseCleared(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto/16 :goto_0

    .line 357
    :sswitch_4
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backupClearCommit :  - This cdid device doent support clear API. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$4;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x135

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",backupClearCommit failed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v2

    .line 297
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4e23 -> :sswitch_1
        0x4e26 -> :sswitch_2
        0x4e27 -> :sswitch_2
        0x4e28 -> :sswitch_2
        0x4e29 -> :sswitch_3
        0x4e54 -> :sswitch_4
        0x6d60 -> :sswitch_2
        0x6d61 -> :sswitch_2
        0x6d62 -> :sswitch_2
    .end sparse-switch
.end method
