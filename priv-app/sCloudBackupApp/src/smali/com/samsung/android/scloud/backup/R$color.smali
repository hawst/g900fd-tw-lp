.class public final Lcom/samsung/android/scloud/backup/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final auto_backup_summary_color:I = 0x7f050000

.field public static final black:I = 0x7f050001

.field public static final bottom_button_text:I = 0x7f050024

.field public static final bottom_button_text_v:I = 0x7f050025

.field public static final button_backupnow_restorenow:I = 0x7f050026

.field public static final button_text_color:I = 0x7f050022

.field public static final button_text_shadow_color:I = 0x7f050023

.field public static final dark_blue:I = 0x7f050002

.field public static final dark_summary_color:I = 0x7f050003

.field public static final defaultColorDark:I = 0x7f050004

.field public static final defaultColorLight:I = 0x7f050005

.field public static final description_color:I = 0x7f050006

.field public static final description_color_zero:I = 0x7f050007

.field public static final device_detail_dec:I = 0x7f050008

.field public static final device_detail_dec_pressed:I = 0x7f050009

.field public static final gray:I = 0x7f05000a

.field public static final learnmore_color:I = 0x7f05000b

.field public static final learnmore_heading_title_color:I = 0x7f05000c

.field public static final learnmore_heading_title_color_dark:I = 0x7f05000d

.field public static final learnmore_indicator_color:I = 0x7f05000e

.field public static final learnmore_summary_color:I = 0x7f05000f

.field public static final learnmore_summary_color_light:I = 0x7f050010

.field public static final learnmore_title_color:I = 0x7f050011

.field public static final learnmore_title_color_light:I = 0x7f050012

.field public static final list_divider:I = 0x7f050013

.field public static final margin_color_dark:I = 0x7f050014

.field public static final margin_color_light:I = 0x7f050015

.field public static final red:I = 0x7f050016

.field public static final separator:I = 0x7f050017

.field public static final setupwizard_title:I = 0x7f050018

.field public static final text_box_summary:I = 0x7f050019

.field public static final text_box_summary_light_theme:I = 0x7f05001a

.field public static final text_box_summary_secondary:I = 0x7f05001b

.field public static final text_box_summary_tab:I = 0x7f05001c

.field public static final text_box_title:I = 0x7f05001d

.field public static final text_description:I = 0x7f05001e

.field public static final title_area_color:I = 0x7f050021

.field public static final transparent:I = 0x7f05001f

.field public static final white:I = 0x7f050020


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
