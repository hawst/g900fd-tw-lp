.class final Lcom/samsung/android/scloud/backup/core/BNRTask$2;
.super Ljava/lang/Object;
.source "BNRTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$fileCnt:F

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field final synthetic val$model:Lcom/samsung/android/scloud/backup/model/IModel;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;F)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    iput p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$fileCnt:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(JJJ)V
    .locals 5
    .param p1, "now"    # J
    .param p3, "transferred"    # J
    .param p5, "total"    # J

    .prologue
    .line 241
    long-to-float v1, p1

    long-to-float v2, p5

    div-float v0, v1, v2

    .line 242
    .local v0, "per":F
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    iget v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$2;->val$fileCnt:F

    div-float v4, v0, v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 243
    return-void
.end method
