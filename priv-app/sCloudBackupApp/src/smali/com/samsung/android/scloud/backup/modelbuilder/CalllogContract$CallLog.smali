.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/CalllogContract$CallLog;
.super Ljava/lang/Object;
.source "CalllogContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/CalllogContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallLog"
.end annotation


# static fields
.field public static final BACKUP:Ljava/lang/String; = "backuped"

.field public static final CACHED_NAME:Ljava/lang/String; = "name"

.field public static final CALLLOGS:Ljava/lang/String; = "CALLLOGS"

.field public static final CONTACTID:Ljava/lang/String; = "contactid"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final IS_READ:Ljava/lang/String; = "is_read"

.field public static final LOGTYPE:Ljava/lang/String; = "logtype"

.field public static final MESSAGEID:Ljava/lang/String; = "messageid"

.field public static final M_CONTENT:Ljava/lang/String; = "m_content"

.field public static final M_SUBJECT:Ljava/lang/String; = "m_subject"

.field public static final NEW:Ljava/lang/String; = "new"

.field public static final NUMBER:Ljava/lang/String; = "number"

.field public static final SP_TYPE:Ljava/lang/String; = "sp_type"

.field public static final TYPE:Ljava/lang/String; = "type"
