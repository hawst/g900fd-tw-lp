.class public Lcom/samsung/android/scloud/backup/model/Sms;
.super Ljava/lang/Object;
.source "Sms.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/model/IModel;


# static fields
.field private static final OEM_AUTHORITY:Ljava/lang/String; = "sms"

.field private static final SOURCE_KEY:Ljava/lang/String; = "SMS"

.field private static final SOURCE_RES_ID:I = 0x7f070007


# instance fields
.field private final OEM_CONTENT_URI:Landroid/net/Uri;

.field private cid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const-string v0, "content://sms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/Sms;->OEM_CONTENT_URI:Landroid/net/Uri;

    .line 40
    const-string v0, "N0iXqXm9oM"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/Sms;->cid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/Sms;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACKUP_SMS_DETAILS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "detailKey":Ljava/lang/String;
    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 76
    invoke-static {}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string v0, "sms"

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/Sms;->OEM_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getSourceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "SMS"

    return-object v0
.end method

.method public getSourceResId()I
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f070007

    return v0
.end method

.method public parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 1
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 81
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v0

    return-object v0
.end method
