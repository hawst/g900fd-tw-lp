.class Lcom/samsung/android/scloud/backup/SamsungBackup$11;
.super Ljava/lang/Object;
.source "SamsungBackup.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0

    .prologue
    .line 539
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$11;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 544
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$11;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$11;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mAutoBackup:Landroid/preference/Preference;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$500(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/TwoStatePreference;

    invoke-virtual {v0}, Landroid/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 545
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$11;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showAutoBackupDialog()V

    .line 546
    return v1

    .line 544
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
