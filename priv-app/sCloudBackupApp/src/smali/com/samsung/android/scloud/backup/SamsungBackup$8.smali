.class Lcom/samsung/android/scloud/backup/SamsungBackup$8;
.super Ljava/lang/Object;
.source "SamsungBackup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;->showData3gDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v1, 0x1

    .line 432
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setData3gAskStatus(Z)V

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->disablePreference()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$600(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 435
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->displayInitialBackingup()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$700(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 436
    sput-boolean v1, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 437
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 438
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 439
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$8;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->requestBackup()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$800(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 440
    return-void
.end method
