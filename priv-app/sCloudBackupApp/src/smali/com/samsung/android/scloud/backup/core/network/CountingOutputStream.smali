.class public Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;
.super Ljava/io/FilterOutputStream;
.source "CountingOutputStream.java"


# instance fields
.field private final listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

.field private total:J

.field private transferred:J


# direct methods
.method public constructor <init>(JLjava/io/OutputStream;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 3
    .param p1, "size"    # J
    .param p3, "out"    # Ljava/io/OutputStream;
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 18
    invoke-direct {p0, p3}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 19
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    .line 21
    iput-wide p1, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->total:J

    .line 22
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 8
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1

    .line 33
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    .line 35
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    iget-wide v4, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    iget-wide v6, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->total:J

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;->transferred(JJJ)V

    .line 36
    return-void
.end method

.method public write([BII)V
    .locals 8
    .param p1, "b"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 27
    iget-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    .line 28
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    int-to-long v2, p3

    iget-wide v4, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->transferred:J

    iget-wide v6, p0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;->total:J

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;->transferred(JJJ)V

    .line 29
    return-void
.end method
