.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract$Sms;
.super Ljava/lang/Object;
.source "SmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Sms"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DELIVERY_DATE:Ljava/lang/String; = "delivery_date"

.field public static final LOCKED:Ljava/lang/String; = "locked"

.field public static final PRIORITY:Ljava/lang/String; = "pri"

.field public static final READ:Ljava/lang/String; = "read"

.field public static final REPLY_PATH:Ljava/lang/String; = "reply_path_present"

.field public static final RESERVED:Ljava/lang/String; = "reserved"

.field public static final SMS:Ljava/lang/String; = "SMS"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final TYPE:Ljava/lang/String; = "type"
