.class public Lcom/samsung/android/scloud/backup/common/MetaManager;
.super Ljava/lang/Object;
.source "MetaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/common/MetaManager$META;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MetaManager"

.field private static fromAutoBackupActivity:Z

.field private static mSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;


# instance fields
.field private UIStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private bChargerConnection:Z

.field private isCancelled:Z

.field private isPaused:Z

.field private isTimeOutCancelled:Z

.field private lastBackupTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private latestRCode:I

.field private latestServiceType:I

.field private latestStatus:I

.field private mActivityStatus:I

.field private mBackupList:Lorg/json/JSONArray;

.field private mBackupMeta:Landroid/content/SharedPreferences;

.field private mContext:Landroid/content/Context;

.field private mSelectedDeviceIndex:I

.field private progressCalcMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 86
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->fromAutoBackupActivity:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    .line 57
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused:Z

    .line 58
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isCancelled:Z

    .line 59
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled:Z

    .line 61
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 76
    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->bChargerConnection:Z

    .line 78
    iput v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSelectedDeviceIndex:I

    .line 142
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    .line 143
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    const-string v1, "BackupMeta"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    .line 144
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    .line 145
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->lastBackupTimeMap:Ljava/util/Map;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->UIStatusMap:Ljava/util/Map;

    .line 151
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->initValidEntryList()V

    .line 152
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->initUIStatusMap()V

    .line 154
    const-string v0, "sCloudBackupApp"

    const-string v1, "sCloudBackupApp Version Info : 4.01.3.KK_APP"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    return-void
.end method

.method private checkEntryValidation(Ljava/lang/String;)Z
    .locals 4
    .param p1, "sourceEntry"    # Ljava/lang/String;

    .prologue
    .line 840
    const/4 v1, 0x1

    .line 841
    .local v1, "ret":Z
    const/4 v0, 0x0

    .line 848
    .local v0, "devType":I
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->isCameraDevice()I

    move-result v0

    if-lez v0, :cond_5

    .line 855
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 856
    const-string v2, "CALLLOGS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 859
    :cond_0
    const/4 v1, 0x0

    .line 885
    :cond_1
    :goto_0
    sget-boolean v2, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_TMO:Z

    if-eqz v2, :cond_3

    .line 886
    const-string v2, "CALLLOGS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "CALLREJECT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 888
    :cond_2
    const/4 v1, 0x0

    .line 892
    :cond_3
    return v1

    .line 862
    :cond_4
    const-string v2, "CALLLOGS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 863
    const/4 v1, 0x0

    goto :goto_0

    .line 866
    :cond_5
    const-string v2, "com.sec.mms"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isInstalledApplication(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "com.sec.android.mms.kor.provider"

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isInstalledApplication(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 867
    :cond_6
    const-string v2, "SMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "MMS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 869
    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    .line 871
    :cond_8
    const-string v2, "CALLREJECT"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 873
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isCallRejectionTablePresent(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "G9009"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "N9009"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "I959"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 874
    const/4 v1, 0x1

    goto :goto_0

    .line 876
    :cond_9
    const/4 v1, 0x0

    goto :goto_0

    .line 877
    :cond_a
    const-string v2, "SPAM"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 879
    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "G9009"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "N9009"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "I959"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 880
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 882
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    const-class v1, Lcom/samsung/android/scloud/backup/common/MetaManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    if-nez v0, :cond_0

    .line 160
    new-instance v0, Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 162
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->sMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private initUIStatusMap()V
    .locals 4

    .prologue
    .line 810
    sget-object v2, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 811
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->UIStatusMap:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 813
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private initValidEntryList()V
    .locals 5

    .prologue
    .line 787
    const-string v3, "MetaManager"

    const-string v4, "initValidEntryList"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sput-object v3, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    .line 790
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v3, Lcom/samsung/android/scloud/backup/common/BackupSources;->KEYS:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 791
    sget-object v3, Lcom/samsung/android/scloud/backup/common/BackupSources;->KEYS:[Ljava/lang/String;

    aget-object v1, v3, v0

    .line 792
    .local v1, "key":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/scloud/backup/common/BackupSources;->PACKAGES:[Ljava/lang/String;

    aget-object v2, v3, v0

    .line 795
    .local v2, "pac":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->isJapanDevice(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 796
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->isAvailableList(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->checkEntryValidation(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 797
    sget-object v3, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 790
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 801
    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->checkEntryValidation(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isInstalledApplication(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 802
    sget-object v3, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 806
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "pac":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public addProcessedValue(Ljava/lang/String;F)V
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "processed"    # F

    .prologue
    const/4 v2, 0x1

    .line 173
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Float;

    aget-object v1, v0, v2

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v0, v2

    .line 175
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 943
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->clearLastBackupTimeMap()V

    .line 944
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 945
    const-string v0, "MetaManager"

    const-string v1, "MetaManager cleared!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 947
    return-void
.end method

.method public clearLastBackupTimeMap()V
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->lastBackupTimeMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1095
    return-void
.end method

.method public commitLastBackupTime()V
    .locals 8

    .prologue
    .line 525
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->lastBackupTimeMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 526
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LastBackupTime_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->lastBackupTimeMap:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 530
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getActivityStatus()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    return v0
.end method

.method public getAutoBackupCheckBoxStatus()Z
    .locals 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "AutoBackupCheckBox"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAutoBackupStatus()Z
    .locals 3

    .prologue
    .line 480
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "AutoBackup"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getAvailableBackups(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/List;
    .locals 22
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "list"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1029
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 1030
    .local v18, "uBackupDetails":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    const-string v7, "timestamp"

    .line 1031
    .local v7, "TIMESTAMP":Ljava/lang/String;
    const-string v4, "key"

    .line 1032
    .local v4, "KEY":Ljava/lang/String;
    const-string v8, "value"

    .line 1033
    .local v8, "VALUE":Ljava/lang/String;
    const-string v5, "name"

    .line 1034
    .local v5, "NAME":Ljava/lang/String;
    const-string v6, "operation"

    .line 1036
    .local v6, "OPERATION":Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v13, v0, :cond_2

    .line 1037
    const/16 v17, 0x0

    .line 1039
    .local v17, "obj":Lorg/json/JSONObject;
    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 1044
    :goto_1
    if-eqz v17, :cond_1

    .line 1045
    const-string v19, "key"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1046
    const/4 v11, 0x0

    .line 1047
    .local v11, "deviceID":Ljava/lang/String;
    const-string v19, "timestamp"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 1048
    .local v16, "mTimestamp":Ljava/lang/Long;
    const-string v19, "key"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "_"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 1049
    .local v15, "keys":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v19, v0

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 1050
    const/16 v19, 0x3

    aget-object v11, v15, v19

    .line 1052
    :cond_0
    const-string v19, "value"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 1053
    const/4 v9, 0x0

    .line 1055
    .local v9, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :try_start_1
    new-instance v19, Lorg/json/JSONTokener;

    const-string v20, "value"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/json/JSONObject;

    .line 1056
    .local v14, "jsonObject":Lorg/json/JSONObject;
    new-instance v10, Lcom/samsung/android/scloud/backup/BackupDetails;

    const-string v19, "name"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v20, "operation"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v0, v1, v11}, Lcom/samsung/android/scloud/backup/BackupDetails;-><init>(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v9    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .local v10, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    move-object v9, v10

    .line 1064
    .end local v10    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v14    # "jsonObject":Lorg/json/JSONObject;
    .restart local v9    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :goto_2
    if-eqz v9, :cond_1

    .line 1065
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->setTimestamp(Ljava/lang/String;)V

    .line 1066
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->setTimestampInTimeFormat(Ljava/lang/String;)V

    .line 1067
    move-object/from16 v0, v18

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036
    .end local v9    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v11    # "deviceID":Ljava/lang/String;
    .end local v15    # "keys":[Ljava/lang/String;
    .end local v16    # "mTimestamp":Ljava/lang/Long;
    :cond_1
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 1040
    :catch_0
    move-exception v12

    .line 1042
    .local v12, "e":Lorg/json/JSONException;
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_1

    .line 1060
    .end local v12    # "e":Lorg/json/JSONException;
    .restart local v9    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .restart local v11    # "deviceID":Ljava/lang/String;
    .restart local v15    # "keys":[Ljava/lang/String;
    .restart local v16    # "mTimestamp":Ljava/lang/Long;
    :catch_1
    move-exception v12

    .line 1062
    .restart local v12    # "e":Lorg/json/JSONException;
    invoke-virtual {v12}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2

    .line 1073
    .end local v9    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v11    # "deviceID":Ljava/lang/String;
    .end local v12    # "e":Lorg/json/JSONException;
    .end local v15    # "keys":[Ljava/lang/String;
    .end local v16    # "mTimestamp":Ljava/lang/Long;
    .end local v17    # "obj":Lorg/json/JSONObject;
    :cond_2
    return-object v18
.end method

.method public getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;
    .locals 10
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "list"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 912
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 913
    .local v2, "backupItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 914
    const/4 v2, 0x0

    .line 930
    .end local v2    # "backupItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    :cond_0
    return-object v2

    .line 916
    .restart local v2    # "backupItems":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 917
    .local v5, "key":Ljava/lang/String;
    invoke-virtual {p0, v5, p2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackups(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v1

    .line 918
    .local v1, "backupDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    if-eqz v1, :cond_2

    .line 919
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 920
    .local v0, "backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 921
    const-string v7, "MetaManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[getAvailableBackupsByDeviceId] "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " : old "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v6

    :goto_1
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " , new "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 923
    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 921
    :cond_4
    const-string v6, "NULL"

    goto :goto_1

    .line 924
    :cond_5
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_3

    .line 925
    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 20
    .param p1, "list"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 975
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 977
    .local v4, "backupDetailsMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 978
    .local v15, "uBackupDetails":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    const/4 v3, 0x0

    .line 980
    .local v3, "backupDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 981
    .local v11, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v11, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackups(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v3

    .line 982
    if-eqz v3, :cond_0

    .line 983
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 984
    .local v2, "backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    if-nez v16, :cond_2

    .line 985
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 988
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 990
    .local v12, "temp_backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    iget-object v0, v12, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    iget-object v0, v2, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    cmp-long v16, v16, v18

    if-gez v16, :cond_1

    .line 992
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 999
    .end local v2    # "backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "key":Ljava/lang/String;
    .end local v12    # "temp_backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_3
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    .line 1001
    .local v6, "deviceIDSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1002
    .local v5, "deviceID":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 1003
    .restart local v2    # "backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_4

    .line 1004
    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1011
    .end local v2    # "backupDetail":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v5    # "deviceID":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    :cond_5
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v0, v16

    if-ge v7, v0, :cond_8

    .line 1012
    add-int/lit8 v10, v7, 0x1

    .local v10, "j":I
    :goto_3
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v16

    move/from16 v0, v16

    if-ge v10, v0, :cond_7

    .line 1013
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/backup/BackupDetails;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/scloud/backup/BackupDetails;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupDetails;->mTimestamp:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v16

    cmp-long v16, v18, v16

    if-gez v16, :cond_6

    .line 1015
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 1016
    .local v13, "temp_backupDetail_i":Lcom/samsung/android/scloud/backup/BackupDetails;
    invoke-interface {v15, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 1017
    .local v14, "temp_backupDetail_j":Lcom/samsung/android/scloud/backup/BackupDetails;
    invoke-interface {v15, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1018
    invoke-interface {v15, v7, v14}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1019
    invoke-interface {v15, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1020
    invoke-interface {v15, v10, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1012
    .end local v13    # "temp_backupDetail_i":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v14    # "temp_backupDetail_j":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 1011
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1024
    .end local v10    # "j":I
    :cond_8
    return-object v15
.end method

.method public getBackupList()Lorg/json/JSONArray;
    .locals 7

    .prologue
    .line 950
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupList:Lorg/json/JSONArray;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupList:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 951
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupList:Lorg/json/JSONArray;

    .line 963
    :cond_0
    :goto_0
    return-object v0

    .line 954
    :cond_1
    const/4 v0, 0x0

    .line 955
    .local v0, "arrayList":Lorg/json/JSONArray;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v5, "BACKUP_LIST"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 956
    .local v3, "list":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 958
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "arrayList":Lorg/json/JSONArray;
    .local v1, "arrayList":Lorg/json/JSONArray;
    move-object v0, v1

    .line 962
    .end local v1    # "arrayList":Lorg/json/JSONArray;
    .restart local v0    # "arrayList":Lorg/json/JSONArray;
    goto :goto_0

    .line 959
    :catch_0
    move-exception v2

    .line 961
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public getData3gAskStatus()Z
    .locals 3

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "data3gAsk"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getData3gAskStatusRestore()Z
    .locals 3

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "data3gAskRestore"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getDateStringFromTimeSpan(J)Ljava/lang/String;
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 691
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 692
    .local v0, "dateFormat":Ljava/text/DateFormat;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 693
    .local v1, "timeFormat":Ljava/text/DateFormat;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 696
    .local v2, "timeStr":Ljava/lang/String;
    return-object v2
.end method

.method public getFromAutoBackupActivityValue()Z
    .locals 1

    .prologue
    .line 622
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->fromAutoBackupActivity:Z

    return v0
.end method

.method public getLastAutoBackupTime()J
    .locals 4

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "LastAutoBackupTime"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastBackupTime()J
    .locals 6

    .prologue
    .line 533
    const-wide/16 v0, 0x0

    .line 534
    .local v0, "finalTime":J
    sget-object v4, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 535
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-lez v4, :cond_0

    .line 536
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 538
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    return-wide v0
.end method

.method public getLastBackupTime(Ljava/lang/String;)J
    .locals 4
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LastBackupTime_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastOperationStatus(Ljava/lang/String;Z)I
    .locals 4
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "bBackup"    # Z

    .prologue
    const/4 v3, 0x0

    .line 514
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LAST_STATUS_BACKUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 517
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LAST_STATUS_RESTORE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method public getLatestRCode()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestRCode:I

    return v0
.end method

.method public getLatestServiceType()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestServiceType:I

    return v0
.end method

.method public getLatestStatus()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestStatus:I

    return v0
.end method

.method public getOperationFinished(Ljava/lang/String;)Z
    .locals 3
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OPERATION_FINISHED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getProgress(Ljava/lang/String;)I
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 166
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Float;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 167
    :cond_0
    const/4 v0, -0x1

    .line 169
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Float;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, v0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Float;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v1, v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public getSelected(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "bBackup"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 640
    if-ne p2, v3, :cond_6

    .line 641
    const-string v1, "HOMESCREEN"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 642
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isWallpaperDefault(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 673
    :cond_0
    :goto_0
    return v0

    .line 647
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SELECTED_BACKUP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 652
    :cond_2
    const-string v1, "VIPLIST"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "BLACKLIST"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 653
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 658
    :cond_4
    const-string v1, "CALLLOGS"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 660
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SELECTED_BACKUP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 664
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECTED_BACKUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 667
    :cond_6
    const-string v1, "VIPLIST"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "BLACKLIST"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 668
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 673
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECTED_RESTORE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public getSelectedDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 606
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "SELECTED_DEVICE"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedDeviceIndex()I
    .locals 1

    .prologue
    .line 934
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSelectedDeviceIndex:I

    return v0
.end method

.method public getSelectedList(Z)Ljava/util/ArrayList;
    .locals 5
    .param p1, "bBackup"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 679
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 681
    .local v3, "sourceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    .line 682
    .local v2, "keySet":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 683
    .local v1, "key":Ljava/lang/String;
    invoke-virtual {p0, v1, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 684
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 686
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method public getUIStatus(Ljava/lang/String;)I
    .locals 1
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 820
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->UIStatusMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getValidSourceList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 824
    sget-object v0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSourceList:Ljava/util/List;

    return-object v0
.end method

.method public getVerificationStatus()I
    .locals 3

    .prologue
    .line 598
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "verificationOk"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getWallpaperFirstDefaultCheck()Z
    .locals 3

    .prologue
    .line 590
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "WALLPAPER_FIRST_DEFAULT_CHANGE_CHECK"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public initLastBackupTime(Ljava/lang/String;)V
    .locals 2
    .param p1, "sourceKey"    # Ljava/lang/String;

    .prologue
    .line 521
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastBackupTime(Ljava/lang/String;J)V

    .line 522
    return-void
.end method

.method public initProgress(Ljava/lang/String;)V
    .locals 5
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Float;

    const/4 v2, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    return-void
.end method

.method public initProgress([Ljava/lang/String;)V
    .locals 9
    .param p1, "sources"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 186
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    .line 187
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 188
    .local v3, "src":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Float;

    const/4 v6, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 189
    .end local v3    # "src":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public isActivityAutoBackup()Z
    .locals 2

    .prologue
    .line 369
    const/4 v0, 0x2

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityBackup()Z
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityBackup(I)Z
    .locals 1
    .param p1, "activityStatus"    # I

    .prologue
    .line 419
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityBackupDetail()Z
    .locals 2

    .prologue
    .line 394
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityBackupDetail(I)Z
    .locals 1
    .param p1, "activityStatus"    # I

    .prologue
    .line 424
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityCancelingBackup()Z
    .locals 2

    .prologue
    .line 379
    const/4 v0, 0x6

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityCancelingRestore()Z
    .locals 2

    .prologue
    .line 384
    const/4 v0, 0x7

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityInSetupWizard()Z
    .locals 2

    .prologue
    .line 404
    const/16 v0, 0x8

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityNone()Z
    .locals 2

    .prologue
    .line 359
    const/4 v0, -0x1

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityNone(I)Z
    .locals 1
    .param p1, "activityStatus"    # I

    .prologue
    .line 414
    const/4 v0, -0x1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityPauseBackup()Z
    .locals 2

    .prologue
    .line 399
    const/4 v0, 0x4

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityPauseRestore(I)Z
    .locals 1
    .param p1, "activityStatus"    # I

    .prologue
    .line 429
    const/4 v0, 0x5

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityRestore()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 374
    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityRestoreFailed()Z
    .locals 2

    .prologue
    .line 409
    const/16 v0, 0x9

    iget v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActivityRestoreFailed(I)Z
    .locals 1
    .param p1, "activityStatus"    # I

    .prologue
    .line 434
    const/16 v0, 0x9

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBatteryOk()Z
    .locals 3

    .prologue
    .line 439
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    const-string v1, "BatteryStatus"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isCancelled:Z

    return v0
.end method

.method public isChargerConnected()Z
    .locals 1

    .prologue
    .line 542
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->bChargerConnection:Z

    return v0
.end method

.method public isFirstBackup(Ljava/lang/String;)Z
    .locals 3
    .param p1, "src"    # Ljava/lang/String;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIRST_BACKUP_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isInstalledApplication(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 828
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 830
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 831
    const-string v2, "MetaManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package Name is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is installed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "MetaManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Package Name is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in NOT installed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused:Z

    return v0
.end method

.method public isTimeOutCancelled()Z
    .locals 1

    .prologue
    .line 234
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled:Z

    return v0
.end method

.method public isWiFiOnly()Z
    .locals 3

    .prologue
    .line 700
    const/4 v0, 0x1

    .line 701
    .local v0, "bWifi":Z
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getCountryCode(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "KOREA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 702
    const/4 v0, 0x0

    .line 704
    :cond_0
    return v0
.end method

.method public setActivityAutoBackup()V
    .locals 4

    .prologue
    .line 275
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityAutoBackup"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityAutoBackup\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 280
    return-void
.end method

.method public setActivityBackup()V
    .locals 4

    .prologue
    .line 267
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityBackup"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityBackup\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 272
    return-void
.end method

.method public setActivityBackupDetail()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 283
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityBackupDetail"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityBackupDetail\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iput v3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 288
    return-void
.end method

.method public setActivityCancelingBackup()V
    .locals 4

    .prologue
    .line 322
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityCancelingBackup"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityCancelingBackup\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 327
    return-void
.end method

.method public setActivityCancelingRestore()V
    .locals 4

    .prologue
    .line 330
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityCancelingRestore"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityCancelingRestore\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const/4 v0, 0x7

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 335
    return-void
.end method

.method public setActivityInSetupWizard()V
    .locals 4

    .prologue
    .line 338
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityInSetupWizard"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityInSetupWizard\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 343
    return-void
.end method

.method public setActivityNone()V
    .locals 4

    .prologue
    .line 259
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityNone"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityNone\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 264
    return-void
.end method

.method public setActivityPauseBackup()V
    .locals 4

    .prologue
    .line 299
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityPauseBackup"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityPauseBackup\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 304
    return-void
.end method

.method public setActivityPauseRemote()V
    .locals 4

    .prologue
    .line 307
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityPauseRemote"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityPauseRemote\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 312
    return-void
.end method

.method public setActivityRestore()V
    .locals 4

    .prologue
    .line 291
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityRestore"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityRestore\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 296
    return-void
.end method

.method public setActivityRestoreFailed()V
    .locals 4

    .prologue
    .line 346
    const-string v0, "MetaManager"

    const-string v1, "MetaManager - setActivityRestoreFailed"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setActivityRestoreFailed\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const/16 v0, 0x9

    iput v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mActivityStatus:I

    .line 351
    return-void
.end method

.method public setAutoBackupCheckBoxStatus(Z)V
    .locals 2
    .param p1, "bStatus"    # Z

    .prologue
    .line 582
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AutoBackupCheckBox"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 583
    return-void
.end method

.method public setAutoBackupStatus(Z)V
    .locals 4
    .param p1, "isFirstBackup"    # Z

    .prologue
    .line 472
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setAutoBackupStatus\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AutoBackup"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 477
    return-void
.end method

.method public setBackupList(Lorg/json/JSONArray;)V
    .locals 3
    .param p1, "backupList"    # Lorg/json/JSONArray;

    .prologue
    .line 969
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupList:Lorg/json/JSONArray;

    .line 970
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "BACKUP_LIST"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 972
    return-void
.end method

.method public setBatteryOk(Z)V
    .locals 2
    .param p1, "isBatteryOk"    # Z

    .prologue
    .line 443
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "BatteryStatus"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 444
    return-void
.end method

.method public setCanceled(Z)V
    .locals 4
    .param p1, "isCanceled"    # Z

    .prologue
    .line 226
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setCanceled\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isCancelled:Z

    .line 231
    return-void
.end method

.method public setChargerStatus(Z)V
    .locals 0
    .param p1, "bSet"    # Z

    .prologue
    .line 546
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->bChargerConnection:Z

    .line 547
    return-void
.end method

.method public setData3gAskStatus(Z)V
    .locals 2
    .param p1, "bStatus"    # Z

    .prologue
    .line 447
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "data3gAsk"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 448
    return-void
.end method

.method public setData3gAskStatusRestore(Z)V
    .locals 2
    .param p1, "bStatus"    # Z

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "data3gAskRestore"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 456
    return-void
.end method

.method public setFirstBackup(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "isFirstBackup"    # Z

    .prologue
    .line 463
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIRST_BACKUP_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 464
    return-void
.end method

.method public setFromAutoBackupActivityValue(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 618
    sput-boolean p1, Lcom/samsung/android/scloud/backup/common/MetaManager;->fromAutoBackupActivity:Z

    .line 619
    return-void
.end method

.method public setLastAutoBackupTime(J)V
    .locals 3
    .param p1, "autoBackupTime"    # J

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LastAutoBackupTime"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1091
    return-void
.end method

.method public setLastBackupTime(Ljava/lang/String;J)V
    .locals 2
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 492
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->lastBackupTimeMap:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    return-void
.end method

.method public setLastOperationStatus(Ljava/lang/String;ZI)V
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "bBackup"    # Z
    .param p3, "status"    # I

    .prologue
    .line 500
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LAST_STATUS_BACKUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 511
    :goto_0
    return-void

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "LAST_STATUS_RESTORE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setLatestStatus(III)V
    .locals 0
    .param p1, "latestServiceType"    # I
    .param p2, "status"    # I
    .param p3, "rcode"    # I

    .prologue
    .line 200
    iput p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestServiceType:I

    .line 201
    iput p2, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestStatus:I

    .line 202
    iput p3, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->latestRCode:I

    .line 203
    return-void
.end method

.method public setOperationFinished(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "finished"    # Z

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OPERATION_FINISHED_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1079
    return-void
.end method

.method public setPaused(Z)V
    .locals 0
    .param p1, "isPaused"    # Z

    .prologue
    .line 218
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused:Z

    .line 219
    return-void
.end method

.method public setSelected(Ljava/lang/String;ZZ)V
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "bSelected"    # Z
    .param p3, "bBackup"    # Z

    .prologue
    .line 626
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECTED_BACKUP"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 637
    :goto_0
    return-void

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SELECTED_RESTORE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setSelectedDeviceId(Ljava/lang/String;)V
    .locals 2
    .param p1, "selectedDeviceId"    # Ljava/lang/String;

    .prologue
    .line 602
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SELECTED_DEVICE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 603
    return-void
.end method

.method public setSelectedDeviceIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 938
    iput p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mSelectedDeviceIndex:I

    .line 939
    return-void
.end method

.method public setTimeOutCancelled(Z)V
    .locals 4
    .param p1, "isTimeOutCancelled"    # Z

    .prologue
    .line 238
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MetaManager - setTimeOutCanceled\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled:Z

    .line 240
    return-void
.end method

.method public setTotalValue(Ljava/lang/String;F)V
    .locals 3
    .param p1, "sourceKey"    # Ljava/lang/String;
    .param p2, "total"    # F

    .prologue
    .line 178
    const-string v0, "MetaManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgress : Total value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->progressCalcMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Float;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    .line 183
    :goto_0
    return-void

    .line 182
    :cond_0
    const-string v0, "MetaManager"

    const-string v1, "Total value should not be 0..!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setUIStatus(Ljava/lang/String;I)V
    .locals 2
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 816
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->UIStatusMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    return-void
.end method

.method public setVerificationStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 594
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "verificationOk"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 595
    return-void
.end method

.method public setWallpaperFirstDefaultCheck(Z)V
    .locals 2
    .param p1, "status"    # Z

    .prologue
    .line 586
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/common/MetaManager;->mBackupMeta:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "WALLPAPER_FIRST_DEFAULT_CHANGE_CHECK"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 587
    return-void
.end method
