.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;
.super Ljava/lang/Object;
.source "SetupWizardRestoreActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->initPref()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

.field final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->val$key:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 429
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->val$key:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->val$key:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 430
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$2;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->checkRestoreNowButton()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$300(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V

    .line 431
    const/4 v0, 0x1

    return v0
.end method
