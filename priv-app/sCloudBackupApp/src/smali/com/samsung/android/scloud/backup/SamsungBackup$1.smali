.class Lcom/samsung/android/scloud/backup/SamsungBackup$1;
.super Ljava/lang/Object;
.source "SamsungBackup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;->onCreateOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 254
    sget-boolean v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 256
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->requestAllCancelBackup()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$100(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 257
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->displayCancelling()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$200(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 258
    sput-boolean v2, Lcom/samsung/android/scloud/backup/SamsungBackup;->cancelstatus:Z

    .line 259
    sput-boolean v2, Lcom/samsung/android/scloud/backup/SamsungBackup;->first:Z

    .line 260
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->invalidateOptionsMenu()V

    .line 280
    :goto_0
    return-void

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$1;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->showDialogById(I)V

    goto :goto_0
.end method
