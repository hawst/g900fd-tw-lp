.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showSelectDeviceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1491
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 1495
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 1496
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 1497
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->setfoucsPosition(I)V

    .line 1498
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 1499
    return-void
.end method
