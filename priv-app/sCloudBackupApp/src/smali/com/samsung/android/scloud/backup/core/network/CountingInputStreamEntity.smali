.class public Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;
.super Lorg/apache/http/entity/InputStreamEntity;
.source "CountingInputStreamEntity.java"


# instance fields
.field private listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;JLcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "instream"    # Ljava/io/InputStream;
    .param p2, "length"    # J
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 17
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .line 19
    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->getContentLength()J

    move-result-wide v2

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/network/CountingInputStreamEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    invoke-direct {v0, v2, v3, p1, v1}, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;-><init>(JLjava/io/OutputStream;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    invoke-super {p0, v0}, Lorg/apache/http/entity/InputStreamEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 25
    return-void
.end method
