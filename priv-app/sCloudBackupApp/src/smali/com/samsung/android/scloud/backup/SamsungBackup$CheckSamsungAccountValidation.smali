.class Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;
.super Landroid/os/AsyncTask;
.source "SamsungBackup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckSamsungAccountValidation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;Lcom/samsung/android/scloud/backup/SamsungBackup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/backup/SamsungBackup;
    .param p2, "x1"    # Lcom/samsung/android/scloud/backup/SamsungBackup$1;

    .prologue
    .line 1235
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;-><init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 11
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v10, 0x1

    .line 1250
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v8

    const-string v9, "doInBackground...SamsungAccount Validation"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1251
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSupportAidlOnly(Landroid/content/Context;)Z

    move-result v9

    # setter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z
    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1402(Lcom/samsung/android/scloud/backup/SamsungBackup;Z)Z

    .line 1252
    const-string v0, "tj9u972o46"

    .line 1253
    .local v0, "clientId":Ljava/lang/String;
    const-string v1, ""

    .line 1254
    .local v1, "clientSecret":Ljava/lang/String;
    const-string v7, "com.samsung.android.scloud.backup"

    .line 1255
    .local v7, "packageName":Ljava/lang/String;
    const-string v6, "OSP_02"

    .line 1256
    .local v6, "ospVersion":Ljava/lang/String;
    const-string v5, "VALIDATION_CHECK"

    .line 1257
    .local v5, "mode":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1258
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.msc.action.samsungaccount.REQUEST_CHECKLIST_VALIDATION"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1259
    .local v4, "intent":Landroid/content/Intent;
    const-string v8, "client_id"

    const-string v9, "tj9u972o46"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1260
    const-string v8, "client_secret"

    const-string v9, ""

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1261
    const-string v8, "progress_theme"

    const-string v9, "invisible"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1262
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v8, v4, v10}, Lcom/samsung/android/scloud/backup/SamsungBackup;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1278
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 1279
    .local v2, "count":I
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1600(Lcom/samsung/android/scloud/backup/SamsungBackup;)I

    move-result v8

    const/4 v9, -0x1

    if-ne v8, v9, :cond_1

    .line 1281
    const/16 v8, 0x3c

    if-le v2, v8, :cond_5

    .line 1282
    :try_start_0
    const-string v8, "SamsungBackup"

    const-string v9, "no response from Samsung account"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1292
    :cond_1
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1293
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->unregisterSamsungAccountReceiver()V
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1700(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 1295
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 1296
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I
    invoke-static {v9}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1600(Lcom/samsung/android/scloud/backup/SamsungBackup;)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setVerificationStatus(I)V

    .line 1299
    :cond_3
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mValidationStatus:I
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1600(Lcom/samsung/android/scloud/backup/SamsungBackup;)I

    move-result v8

    if-nez v8, :cond_6

    .line 1300
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 1302
    :goto_2
    return-object v8

    .line 1265
    .end local v2    # "count":I
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_4
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.msc.action.VALIDATION_CHECK_REQUEST"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1266
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v8, "client_id"

    const-string v9, "tj9u972o46"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1267
    const-string v8, "client_secret"

    const-string v9, ""

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1268
    const-string v8, "mypackage"

    const-string v9, "com.samsung.android.scloud.backup"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1269
    const-string v8, "OSP_VER"

    const-string v9, "OSP_02"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1270
    const-string v8, "MODE"

    const-string v9, "VALIDATION_CHECK"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1271
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->registerSamsungAccountReceiver()V
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1500(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 1272
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 1273
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1285
    .restart local v2    # "count":I
    :cond_5
    const-wide/16 v8, 0x3e8

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1286
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1287
    :catch_0
    move-exception v3

    .line 1288
    .local v3, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    .line 1302
    .end local v3    # "e":Ljava/lang/InterruptedException;
    :cond_6
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1235
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 1331
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->isSupportAidlOnly:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1332
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # invokes: Lcom/samsung/android/scloud/backup/SamsungBackup;->unregisterSamsungAccountReceiver()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1700(Lcom/samsung/android/scloud/backup/SamsungBackup;)V

    .line 1333
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 1334
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "validated"    # Ljava/lang/Boolean;

    .prologue
    .line 1307
    const-string v1, "SamsungBackup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Samsung account Validated :? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1309
    :try_start_0
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1310
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1311
    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1302(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1313
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1314
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mIntentToStartActivity:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1800(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1315
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mIntentToStartActivity:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1800(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1319
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    .line 1328
    :cond_1
    :goto_1
    return-void

    .line 1317
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f07003d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1321
    :catch_0
    move-exception v0

    .line 1322
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 1323
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    goto :goto_1

    .line 1324
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1325
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1326
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->finish()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1235
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPreExecute...SamsungAccount Validation"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1200(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1241
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1200(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1302(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 1242
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$CheckSamsungAccountValidation;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1200(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f07003e

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1243
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1244
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1245
    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$1300()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1247
    :cond_0
    return-void
.end method
