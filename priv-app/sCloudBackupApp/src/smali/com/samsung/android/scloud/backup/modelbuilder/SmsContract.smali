.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;
.super Ljava/lang/Object;
.source "SmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract$Sms;,
        Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract$BaseColumns;
    }
.end annotation


# static fields
.field public static final CANONICAL_ADDRESS_URI:Landroid/net/Uri;

.field public static final CONVERSATION_URI:Landroid/net/Uri;

.field public static final RECIPIENT:Ljava/lang/String; = "recipient"

.field public static final RECIPIENTS:Ljava/lang/String; = "recipients"

.field public static final RECIPIENT_IDS:Ljava/lang/String; = "recipient_ids"

.field public static final TARGET_URI:Landroid/net/Uri;

.field public static final THREAD_ID_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "content://sms"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->TARGET_URI:Landroid/net/Uri;

    .line 33
    const-string v0, "content://mms-sms/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->CONVERSATION_URI:Landroid/net/Uri;

    .line 34
    const-string v0, "content://mms-sms/canonical-address"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->CANONICAL_ADDRESS_URI:Landroid/net/Uri;

    .line 35
    const-string v0, "content://mms-sms/threadID"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/SmsContract;->THREAD_ID_URI:Landroid/net/Uri;

    return-void
.end method
