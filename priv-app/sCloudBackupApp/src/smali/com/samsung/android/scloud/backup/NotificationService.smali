.class public Lcom/samsung/android/scloud/backup/NotificationService;
.super Lcom/samsung/android/scloud/backup/core/BNRService;
.source "NotificationService.java"


# instance fields
.field private final MESSAGE_DELAYED_TIME:J

.field private final REMOTE_PROGRESS_TIMEOUT_VALUE:I

.field private final REMOTE_STATUS_TIMEOUT:I

.field private final TAG:Ljava/lang/String;

.field private builder:Landroid/app/Notification$Builder;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsCancelled:Z

.field private mLastProgressUpdateTime:J

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mNotifationFlags:I

.field private mNotification:Landroid/app/Notification;

.field private mNotificationIntent:Landroid/content/Intent;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mQuotafull:Z

.field private mRemoteViewsError:Landroid/widget/RemoteViews;

.field private mRemoteViewsProgress:Landroid/widget/RemoteViews;

.field private mServiceType:I

.field private serviceTypeDef:[I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;-><init>()V

    .line 34
    const-string v0, "NotificationService"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->TAG:Ljava/lang/String;

    .line 35
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotification:Landroid/app/Notification;

    .line 36
    iput v3, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotifationFlags:I

    .line 37
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mRemoteViewsProgress:Landroid/widget/RemoteViews;

    .line 38
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mRemoteViewsError:Landroid/widget/RemoteViews;

    .line 39
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 40
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 41
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    .line 42
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 44
    iput v3, p0, Lcom/samsung/android/scloud/backup/NotificationService;->REMOTE_STATUS_TIMEOUT:I

    .line 45
    const v0, 0x3a980

    iput v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->REMOTE_PROGRESS_TIMEOUT_VALUE:I

    .line 46
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->MESSAGE_DELAYED_TIME:J

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J

    .line 48
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mQuotafull:Z

    .line 49
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->serviceTypeDef:[I

    .line 55
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mContext:Landroid/content/Context;

    return-void

    .line 52
    nop

    :array_0
    .array-data 4
        0x65
        0x66
    .end array-data
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/NotificationService;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/NotificationService;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/NotificationService;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/NotificationService;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/NotificationService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/NotificationService;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/NotificationService;->removeRemoteTimeoutMessage(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/NotificationService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/NotificationService;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/NotificationService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/NotificationService;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/NotificationService;->sendRemoteTimeoutMessage(I)V

    return-void
.end method

.method private createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "mPendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 478
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f02003a

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x7f07005b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    .line 488
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 490
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 492
    return-void
.end method

.method private createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "summary"    # Ljava/lang/String;
    .param p3, "mPendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 497
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f02003a

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    const v1, 0x7f07005b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    .line 507
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 509
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 511
    return-void
.end method

.method private initHandler()V
    .locals 1

    .prologue
    .line 395
    new-instance v0, Lcom/samsung/android/scloud/backup/NotificationService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/NotificationService$1;-><init>(Lcom/samsung/android/scloud/backup/NotificationService;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mHandler:Landroid/os/Handler;

    .line 438
    return-void
.end method

.method private removeRemoteTimeoutMessage(I)V
    .locals 3
    .param p1, "mMessgeType"    # I

    .prologue
    .line 448
    const-string v0, "NotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[2. removeRemoteTimeoutMessage]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 450
    return-void
.end method

.method private sendRemoteTimeoutMessage(I)V
    .locals 4
    .param p1, "mMessgeType"    # I

    .prologue
    .line 442
    const-string v0, "NotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[1. sendRemoteTimeoutMessage] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 445
    return-void
.end method

.method private showQuotaNotification()V
    .locals 3

    .prologue
    .line 470
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getTag()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Send Quota full notification broadcast"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mQuotafull:Z

    .line 472
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sCloudQuotaApp.QUOTA_FULL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 473
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 474
    return-void
.end method


# virtual methods
.method public cancelnotification()V
    .locals 2

    .prologue
    .line 283
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 285
    return-void
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    const-string v0, "NotificationService"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->serviceTypeDef:[I

    return-object v0
.end method

.method public onBackupCompleted()V
    .locals 3

    .prologue
    .line 270
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 271
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 274
    :cond_0
    return-void
.end method

.method public onBackupFailed(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "isQuotaFull"    # Ljava/lang/Boolean;

    .prologue
    const/16 v3, 0xd

    .line 292
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 294
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 295
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 296
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 297
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 298
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->showNotification(I)V

    .line 299
    :cond_0
    return-void
.end method

.method public onBackupFailed(Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 289
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 191
    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v3, 0x7f07005b

    .line 245
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRService;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 247
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 248
    .local v0, "text":Ljava/lang/String;
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f02003a

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    .line 251
    return-void
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const v3, 0x7f07005b

    .line 255
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->onCreate()V

    .line 256
    const-string v1, "NotificationService"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->initHandler()V

    .line 258
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mContext:Landroid/content/Context;

    .line 260
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 262
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 263
    .local v0, "text":Ljava/lang/String;
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f02003a

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    .line 264
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 265
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 515
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->onDestroy()V

    .line 516
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 8
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    .line 69
    packed-switch p2, :pswitch_data_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 72
    :pswitch_0
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,msg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg1 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , obj : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J

    .line 74
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->sendRemoteTimeoutMessage(I)V

    goto :goto_0

    .line 77
    :pswitch_1
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,msg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg1 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , obj : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const/16 v4, 0x65

    if-ne p1, v4, :cond_6

    .line 79
    const/16 v4, 0x12d

    if-ne p3, v4, :cond_3

    .line 80
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 81
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 118
    :cond_1
    :goto_1
    const/4 v3, 0x1

    .line 119
    .local v3, "noOperation":Z
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 120
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 121
    const/4 v3, 0x0

    goto :goto_2

    .line 83
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "noOperation":Z
    :cond_3
    const/16 v4, 0x132

    if-ne p3, v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v4

    if-nez v4, :cond_4

    .line 84
    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->onOperationCancelled(Ljava/lang/String;)V

    .line 85
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    goto :goto_1

    .line 87
    :cond_4
    const/16 v4, 0x132

    if-ne p3, v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 88
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 89
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 90
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto :goto_1

    .line 93
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 94
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, -0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 95
    const/16 v4, 0x137

    if-ne p3, v4, :cond_1

    .line 96
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mQuotafull:Z

    goto/16 :goto_1

    .line 100
    :cond_6
    const/16 v4, 0x12d

    if-ne p3, v4, :cond_7

    .line 101
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 102
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto/16 :goto_1

    .line 104
    :cond_7
    const/16 v4, 0x132

    if-ne p3, v4, :cond_8

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v4

    if-nez v4, :cond_8

    .line 105
    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->onOperationCancelled(Ljava/lang/String;)V

    .line 106
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    goto/16 :goto_1

    .line 108
    :cond_8
    const/16 v4, 0x132

    if-ne p3, v4, :cond_9

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 109
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 110
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 111
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto/16 :goto_1

    .line 114
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 115
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto/16 :goto_1

    .line 122
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "noOperation":Z
    :cond_a
    if-eqz v3, :cond_0

    .line 123
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->removeRemoteTimeoutMessage(I)V

    .line 124
    const/4 v1, 0x1

    .line 125
    .local v1, "isSuccess":Z
    const/16 v4, 0x65

    if-ne p1, v4, :cond_f

    .line 126
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 127
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_b
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 128
    .restart local v2    # "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_b

    .line 129
    const/4 v1, 0x0

    goto :goto_3

    .line 131
    .end local v2    # "key":Ljava/lang/String;
    :cond_c
    if-eqz v1, :cond_e

    .line 132
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    if-nez v4, :cond_d

    .line 133
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->onBackupCompleted()V

    .line 163
    :cond_d
    :goto_4
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    goto/16 :goto_0

    .line 136
    :cond_e
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mQuotafull:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->onBackupFailed(Ljava/lang/Boolean;)V

    .line 137
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mQuotafull:Z

    if-eqz v4, :cond_d

    .line 138
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->showQuotaNotification()V

    goto :goto_4

    .line 140
    :cond_f
    const/16 v4, 0x66

    if-ne p1, v4, :cond_d

    .line 141
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setCanceled(Z)V

    .line 142
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 143
    .restart local v2    # "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_10

    .line 144
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityRestoreFailed()V

    .line 145
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->onRestoreFailed()V

    .line 149
    .end local v2    # "key":Ljava/lang/String;
    :cond_11
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 150
    .restart local v2    # "key":Ljava/lang/String;
    const-string v4, "SMS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, "MMS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 151
    :cond_13
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_12

    .line 152
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->updateMessageThreads(Landroid/content/Context;)V

    .line 157
    .end local v2    # "key":Ljava/lang/String;
    :cond_14
    if-eqz v1, :cond_d

    .line 158
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 159
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mIsCancelled:Z

    if-nez v4, :cond_d

    .line 160
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->onRestoreCompleted()V

    goto/16 :goto_4

    .line 168
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "isSuccess":Z
    .end local v3    # "noOperation":Z
    :pswitch_2
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status:203 onEventReceived :  ,rcode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,msg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg1 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , obj : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->removeRemoteTimeoutMessage(I)V

    goto/16 :goto_0

    .line 173
    :pswitch_3
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status:202 onEventReceived :  ,rcode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,msg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg1 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , obj : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J

    goto/16 :goto_0

    .line 178
    :pswitch_4
    const-string v4, "NotificationService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status:204 onEventReceived :  ,rcode"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ,msg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg1 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", arg2 : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , obj : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mLastProgressUpdateTime:J

    .line 180
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->sendRemoteTimeoutMessage(I)V

    goto/16 :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public onOperationCancelled(Ljava/lang/String;)V
    .locals 3
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 278
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, p1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 279
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 280
    return-void
.end method

.method public onRestoreCompleted()V
    .locals 3

    .prologue
    .line 302
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 303
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v1, :cond_0

    .line 305
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 306
    :cond_0
    return-void
.end method

.method public onRestoreFailed()V
    .locals 4

    .prologue
    const/16 v3, 0xd

    .line 313
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 314
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 316
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 317
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/NotificationService;->showNotification(I)V

    .line 318
    return-void
.end method

.method public onRestoreFailed(Ljava/lang/String;)V
    .locals 2
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v1, -0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 310
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/high16 v5, 0x24000000

    const/4 v4, 0x1

    const/16 v3, 0xd

    .line 196
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/scloud/backup/core/BNRService;->onStartCommand(Landroid/content/Intent;II)I

    .line 197
    if-eqz p1, :cond_0

    .line 198
    const-string v0, "Notification Type"

    const/16 v1, 0x64

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    .line 200
    const-string v0, "NotificationService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[OnStartCommand] ServiceType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    .line 202
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    .line 203
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 206
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->showNotification(I)V

    .line 207
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/samsung/android/scloud/backup/NotificationService;->startForeground(ILandroid/app/Notification;)V

    .line 239
    :cond_0
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 211
    :cond_1
    iget v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_2

    .line 212
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    .line 213
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 216
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/NotificationService;->showNotification(I)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/samsung/android/scloud/backup/NotificationService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0

    .line 221
    :cond_2
    iget v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_3

    .line 222
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 223
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 224
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 227
    :cond_3
    iget v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mServiceType:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_5

    .line 228
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_4

    .line 229
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/NotificationService;->stopForeground(Z)V

    .line 230
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 232
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->stopSelf()V

    goto :goto_0

    .line 235
    :cond_5
    const-string v0, "NotificationService"

    const-string v1, "Unknown Service Type"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestCancel()V
    .locals 8

    .prologue
    .line 453
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_CANCEL"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 454
    .local v5, "service":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 455
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 456
    .local v0, "cancelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 457
    .local v3, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 458
    add-int/lit8 v1, v1, 0x1

    .line 459
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 462
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    new-array v4, v1, [Ljava/lang/String;

    .line 463
    .local v4, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 465
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/NotificationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 466
    return-void
.end method

.method protected showNotification(I)V
    .locals 8
    .param p1, "id"    # I

    .prologue
    const v7, 0x7f07009d

    const/16 v5, 0xe

    const/4 v4, -0x1

    const/high16 v6, 0x10000000

    .line 323
    const-string v0, ""

    .line 324
    .local v0, "summary":Ljava/lang/String;
    const-string v1, ""

    .line 326
    .local v1, "title":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 392
    :goto_0
    return-void

    .line 328
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 329
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 331
    const v2, 0x7f07009e

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 332
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 337
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 341
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v0, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 347
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 348
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 351
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 357
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07009c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 358
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v3, "Dialog_Type"

    const/16 v4, 0x9

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 360
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 362
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v0, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    .line 367
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 369
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 370
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 373
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v0, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    .line 379
    :pswitch_5
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 380
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07008a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 381
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    const-string v3, "Dialog_Type"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 382
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/NotificationService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v3, v4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mNotificationIntent:Landroid/content/Intent;

    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 384
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/NotificationService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-direct {p0, v1, v0, v2}, Lcom/samsung/android/scloud/backup/NotificationService;->createNotification(Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto/16 :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
