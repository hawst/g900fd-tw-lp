.class public final Lcom/samsung/android/scloud/backup/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final BLACKLIST:I = 0x7f070000

.field public static final BLACKLIST_ZERO:I = 0x7f070001

.field public static final CALLREJECTION:I = 0x7f070002

.field public static final LOGS:I = 0x7f070003

.field public static final MMS:I = 0x7f070004

.field public static final MessageMMS:I = 0x7f070005

.field public static final MessageSMS:I = 0x7f070006

.field public static final SMS:I = 0x7f070007

.field public static final SPAM:I = 0x7f070008

.field public static final SPAM_ZERO:I = 0x7f070009

.field public static final VIPLIST:I = 0x7f07000a

.field public static final VIPLIST_ZERO:I = 0x7f07000b

.field public static final WALLPAPER:I = 0x7f07000c

.field public static final WEARABLE:I = 0x7f07000d

.field public static final accessibility_settings:I = 0x7f07000e

.field public static final alert:I = 0x7f07000f

.field public static final attention:I = 0x7f070010

.field public static final auth_token_toast:I = 0x7f070011

.field public static final auto_backup:I = 0x7f070012

.field public static final auto_backup_popup_msg:I = 0x7f070013

.field public static final auto_backup_popup_msg_wlan:I = 0x7f070014

.field public static final auto_backup_summary:I = 0x7f070015

.field public static final auto_backup_summary_zero:I = 0x7f070016

.field public static final auto_backup_summary_zero_wlan:I = 0x7f070017

.field public static final autobackup_turnoff_toast:I = 0x7f070018

.field public static final back_up_without_restoring:I = 0x7f070019

.field public static final backed_up_data:I = 0x7f07001a

.field public static final backingup_pref:I = 0x7f07001b

.field public static final backup_Menu:I = 0x7f07001c

.field public static final backup_backupfailed:I = 0x7f07001d

.field public static final backup_cannot_backup_while_restoring:I = 0x7f07001e

.field public static final backup_delete_failed_message:I = 0x7f07001f

.field public static final backup_failed:I = 0x7f070020

.field public static final backup_item:I = 0x7f070021

.field public static final backup_item_zero:I = 0x7f070022

.field public static final backup_not_found_before:I = 0x7f070023

.field public static final backup_popup:I = 0x7f070024

.field public static final backup_restore:I = 0x7f070025

.field public static final backup_restore_warning_messgae:I = 0x7f070026

.field public static final backup_success:I = 0x7f070027

.field public static final backupdata:I = 0x7f070028

.field public static final calender:I = 0x7f070029

.field public static final cancel:I = 0x7f07002a

.field public static final cancel_backup:I = 0x7f07002b

.field public static final checkbox_text:I = 0x7f07002c

.field public static final clock:I = 0x7f07002d

.field public static final com_pop_cancelling:I = 0x7f07002e

.field public static final com_pop_downloadfail:I = 0x7f07002f

.field public static final com_pop_downloadfail_space:I = 0x7f070030

.field public static final com_pop_restoring:I = 0x7f070031

.field public static final contacts:I = 0x7f070032

.field public static final continue_setup:I = 0x7f070033

.field public static final default_wallpaper_cannot_be_backed_up:I = 0x7f070034

.field public static final delete_backup_dialog_message:I = 0x7f070035

.field public static final delete_backup_dialog_title:I = 0x7f070036

.field public static final delete_text:I = 0x7f070037

.field public static final do_not_show_again:I = 0x7f070038

.field public static final done:I = 0x7f070039

.field public static final download_data_3g:I = 0x7f07003a

.field public static final downloading_text:I = 0x7f07003b

.field public static final email:I = 0x7f07003c

.field public static final failed_to_connect_to_server:I = 0x7f07003d

.field public static final getting_restore_list_text:I = 0x7f07003e

.field public static final home:I = 0x7f07003f

.field public static final internet:I = 0x7f070040

.field public static final last_backup:I = 0x7f070041

.field public static final last_backup_never_string:I = 0x7f070042

.field public static final later_text:I = 0x7f070043

.field public static final learn_more_blacklist:I = 0x7f070044

.field public static final learn_more_calender:I = 0x7f070045

.field public static final learn_more_calllog:I = 0x7f070046

.field public static final learn_more_callreject:I = 0x7f070047

.field public static final learn_more_contacts:I = 0x7f070048

.field public static final learn_more_internet:I = 0x7f070049

.field public static final learn_more_memo:I = 0x7f07004a

.field public static final learn_more_mms:I = 0x7f07004b

.field public static final learn_more_note:I = 0x7f07004c

.field public static final learn_more_scrap:I = 0x7f07004d

.field public static final learn_more_sms:I = 0x7f07004e

.field public static final learn_more_spam:I = 0x7f07004f

.field public static final learn_more_viplist:I = 0x7f070050

.field public static final learn_more_wallpaper:I = 0x7f070051

.field public static final learn_more_warning:I = 0x7f070052

.field public static final lose_backup_data_description_restore_not_selected:I = 0x7f070053

.field public static final memo:I = 0x7f070054

.field public static final message:I = 0x7f070055

.field public static final messages:I = 0x7f070056

.field public static final miscellaneous:I = 0x7f070057

.field public static final next_button:I = 0x7f070058

.field public static final no_backup_data:I = 0x7f070059

.field public static final note:I = 0x7f07005a

.field public static final notification_ticker_text:I = 0x7f07005b

.field public static final notification_ticker_text_zero:I = 0x7f07005c

.field public static final notification_upload_failed_title:I = 0x7f07005d

.field public static final phone:I = 0x7f07005e

.field public static final previous:I = 0x7f07005f

.field public static final processing_dialog_string:I = 0x7f070060

.field public static final quota_failed_notification_message:I = 0x7f070061

.field public static final quota_failed_notification_title:I = 0x7f070062

.field public static final restore:I = 0x7f070063

.field public static final restore_before_backup:I = 0x7f070064

.field public static final restore_cannot_backup_while_backingup:I = 0x7f070065

.field public static final restore_complete:I = 0x7f070066

.field public static final restore_data:I = 0x7f070067

.field public static final restore_description:I = 0x7f070068

.field public static final restore_description_zero:I = 0x7f070069

.field public static final restore_failed:I = 0x7f07006a

.field public static final restore_failed_title:I = 0x7f07006b

.field public static final restore_restorefailed_description:I = 0x7f07006c

.field public static final restore_stoprestore_title:I = 0x7f07006d

.field public static final restore_zero:I = 0x7f07006e

.field public static final restoring_pref:I = 0x7f07006f

.field public static final retrieving_data_from_samsung_account:I = 0x7f070070

.field public static final retry_restore:I = 0x7f070071

.field public static final retry_text:I = 0x7f070072

.field public static final samsung_autobackup_data_summary:I = 0x7f070073

.field public static final samsung_autobackup_data_summary_chn:I = 0x7f070074

.field public static final samsung_autobackup_description:I = 0x7f070075

.field public static final samsung_autobackup_description_chn:I = 0x7f070076

.field public static final samsung_back_up:I = 0x7f070077

.field public static final samsung_backup_options_summary:I = 0x7f070078

.field public static final samsung_backup_require_wificonnection:I = 0x7f070079

.field public static final samsung_backup_require_wificonnection_chn:I = 0x7f07007a

.field public static final samsung_backupnow_title:I = 0x7f07007b

.field public static final samsung_cannotbackup5days_notification_description:I = 0x7f07007c

.field public static final samsung_cannotbackup5days_notification_description_chn:I = 0x7f07007d

.field public static final samsung_manualrestore_description:I = 0x7f07007e

.field public static final samsung_manualrestore_restorenow:I = 0x7f07007f

.field public static final samsung_manualrestore_selectdevice:I = 0x7f070080

.field public static final samsung_manualrestore_selectdevice_zero:I = 0x7f070081

.field public static final samsung_restore_require_wificonnection:I = 0x7f070082

.field public static final samsung_restore_title:I = 0x7f070083

.field public static final samsung_resume:I = 0x7f070084

.field public static final scrap:I = 0x7f070085

.field public static final selecet_backed_up_data:I = 0x7f070086

.field public static final select_all_title:I = 0x7f070087

.field public static final select_data:I = 0x7f070088

.field public static final server_busy_message:I = 0x7f070089

.field public static final server_notification_desc:I = 0x7f07008a

.field public static final server_notification_desc_popup:I = 0x7f07008b

.field public static final server_notification_title:I = 0x7f07008c

.field public static final set_up_email_to_restore_email_application_data:I = 0x7f07008d

.field public static final set_up_email_to_restore_priority_senders:I = 0x7f07008e

.field public static final set_up_email_to_restore_spam_addresses:I = 0x7f07008f

.field public static final setupWizardRestore_zero:I = 0x7f070090

.field public static final setup_wizard_alert_message:I = 0x7f070091

.field public static final setup_wizard_backup_and_restore_main_string:I = 0x7f070092

.field public static final setup_wizard_backup_and_restore_main_string_zero:I = 0x7f070093

.field public static final setup_wizard_backup_and_restore_summary_string:I = 0x7f070094

.field public static final setup_wizard_learn_more:I = 0x7f070095

.field public static final setup_wizard_learn_more_title:I = 0x7f070096

.field public static final setup_wizard_no_restore_item:I = 0x7f070097

.field public static final skip:I = 0x7f070098

.field public static final stop_restoring_desc:I = 0x7f070099

.field public static final sync:I = 0x7f07009a

.field public static final toast_text_data_connectivity_unavaliable:I = 0x7f07009b

.field public static final unable_to_restore:I = 0x7f07009c

.field public static final unknown_error:I = 0x7f07009d

.field public static final upload_in_progress:I = 0x7f07009e

.field public static final wallpaper:I = 0x7f07009f

.field public static final warning:I = 0x7f0700a0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
