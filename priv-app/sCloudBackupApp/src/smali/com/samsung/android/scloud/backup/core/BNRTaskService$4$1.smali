.class Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;->handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$trigger:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->val$trigger:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 152
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_GETDETAIL 1: "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 154
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModelManager.getInstance() is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->val$context:Landroid/content/Context;

    const-string v2, "REQUEST_GETDETAIL"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->val$trigger:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->getBackedupDetails(Ljava/lang/String;)V

    .line 158
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_GETDETAIL 2: "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    return-void

    .line 155
    :cond_1
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4$1;->val$context:Landroid/content/Context;

    const-string v2, "REQUEST_GETDETAIL"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 156
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModelManager.getInstance().getBNRManager(context, BackupConstants.Action.REQUEST_GETDETAIL) is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
