.class final Lcom/samsung/android/scloud/backup/core/BNRTask$12;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/Map;Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field final synthetic val$model:Lcom/samsung/android/scloud/backup/model/IModel;

.field final synthetic val$processedKeyList:Ljava/util/List;

.field final synthetic val$serverFileMap:Ljava/util/Map;

.field final synthetic val$srcDevice:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/Map;Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$serverFileMap:Ljava/util/Map;

    iput-object p5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    iput-object p6, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iput-object p7, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$srcDevice:Ljava/lang/String;

    iput-object p8, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$processedKeyList:Ljava/util/List;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 21
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 565
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x4

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "restoreItems Finished - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 567
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 568
    .local v20, "toRestoreItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual/range {p3 .. p3}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_5

    .line 569
    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v19

    .line 571
    .local v19, "json":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Lcom/samsung/android/scloud/backup/model/IModel;->parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v18

    .line 572
    .local v18, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 574
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$serverFileMap:Ljava/util/Map;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 575
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$serverFileMap:Ljava/util/Map;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map;

    .line 576
    .local v14, "fileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 577
    .local v12, "file":Ljava/lang/String;
    invoke-interface {v14, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v2}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 581
    .end local v12    # "file":Ljava/lang/String;
    .end local v14    # "fileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->hasFile()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 582
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    check-cast v15, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    .line 583
    .local v15, "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getAttachmentFileInfo()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    int-to-float v13, v2

    .line 585
    .local v13, "fileCnt":F
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getAttachmentFileInfo()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .restart local v17    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 586
    .restart local v12    # "file":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x4

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : Download File - key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 589
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-interface {v15, v6, v0}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$srcDevice:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v15, v7, v0}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    new-instance v9, Lcom/samsung/android/scloud/backup/core/BNRTask$12$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1, v13}, Lcom/samsung/android/scloud/backup/core/BNRTask$12$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTask$12;Lcom/samsung/android/scloud/backup/data/BNRItem;F)V

    invoke-static/range {v2 .. v9}, Lcom/samsung/android/scloud/backup/server/ORSServiceManager;->downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 610
    :catch_0
    move-exception v10

    .line 611
    .local v10, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v10}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\"rcode\":32002"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v15}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->isAlternateDownloadPathAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x0

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : Download from alternate path !!!"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 615
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-interface {v15, v6, v0}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v15}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getAlternatePathParameter()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-interface {v15, v7, v0}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    new-instance v9, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1, v13}, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTask$12;Lcom/samsung/android/scloud/backup/data/BNRItem;F)V

    invoke-static/range {v2 .. v9}, Lcom/samsung/android/scloud/backup/server/ORSServiceManager;->downloadFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 636
    :catch_1
    move-exception v11

    .line 637
    .local v11, "e1":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual {v11}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\"rcode\":32002"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 638
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x0

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : File does not exist in the alternath path. key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", file : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 640
    :cond_1
    throw v11

    .line 644
    .end local v11    # "e1":Lcom/samsung/android/scloud/backup/common/BNRException;
    :cond_2
    invoke-virtual {v10}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\"rcode\":32002"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 645
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x0

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : File does not exists. key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", file : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 647
    :cond_3
    throw v10

    .line 654
    .end local v10    # "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    .end local v12    # "file":Ljava/lang/String;
    .end local v13    # "fileCnt":F
    .end local v15    # "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    .end local v17    # "i$":Ljava/util/Iterator;
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x66

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 568
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 657
    .end local v18    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v19    # "json":Lorg/json/JSONObject;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$processedKeyList:Ljava/util/List;

    move-object/from16 v6, v20

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->putDataToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 658
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x139

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 660
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v3, 0x4

    const-string v4, "BNRTask-"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v6}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$ctid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") : Restored to OEM DB - count : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 664
    return-void
.end method
