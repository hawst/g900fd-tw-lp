.class final Lcom/samsung/android/scloud/backup/util/BNRUtil$1;
.super Ljava/lang/Object;
.source "BNRUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/util/BNRUtil;->updateMessageThreads(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/util/BNRUtil$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 288
    # getter for: Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "updateMessageThreads"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :try_start_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 291
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "content://mms-sms/update_threads"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 292
    .local v2, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/util/BNRUtil$1;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 293
    .local v1, "resolver":Landroid/content/ContentResolver;
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 294
    # getter for: Lcom/samsung/android/scloud/backup/util/BNRUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "updateMessageThreads called"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 300
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "values":Landroid/content/ContentValues;
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v0}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0
.end method
