.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;
.super Landroid/os/Handler;
.source "ManualRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1373
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 1376
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1392
    :goto_0
    return-void

    .line 1378
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const v2, 0x7f070023

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1381
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finishThisActivity(I)V
    invoke-static {v0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)V

    goto :goto_0

    .line 1385
    :pswitch_1
    const-string v0, "ManualRestoreActivity"

    const-string v1, "REMOTE_BACKUP_DETAIL_TIMEOUT"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->processBackupDetailRequestFail()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    goto :goto_0

    .line 1376
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
