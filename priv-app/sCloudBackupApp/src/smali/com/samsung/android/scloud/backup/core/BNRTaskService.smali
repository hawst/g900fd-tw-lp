.class public Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.super Landroid/app/Service;
.source "BNRTaskService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;
    }
.end annotation


# static fields
.field private static final BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;",
            ">;"
        }
    .end annotation
.end field

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-string v0, "BNRTaskService"

    sput-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;

    .line 87
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    .line 91
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_BACKUP"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$2;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$2;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_RESTORE"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$3;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$3;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_GETDETAIL"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$4;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_GETUSAGE"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$5;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$5;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_BACKUP_DELETE"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "OPERATION_PAUSE"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$7;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$7;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "OPERATION_RESUME"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$8;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$8;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "OPERATION_CANCEL"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    const-string v1, "REQUEST_BACKUP_CLEARED"

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$10;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$10;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->mContext:Landroid/content/Context;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/core/BNRTaskService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 42
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;

    const-string v1, "onCreate : "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->mContext:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 49
    sget-object v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 56
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "action":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onStartCommand : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget-object v4, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->BNRServiceHandler_Map:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v4, v0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;

    .line 60
    .local v1, "handler":Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;
    if-eqz v0, :cond_0

    .line 61
    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService$1;

    invoke-direct {v2, p0, v1, p1}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService;Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;Landroid/content/Intent;)V

    .line 69
    .local v2, "serviceTask":Ljava/lang/Runnable;
    new-instance v3, Ljava/lang/Thread;

    const-string v4, "SERVICE_THREAD"

    invoke-direct {v3, v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 70
    .local v3, "serviceThread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 74
    .end local v2    # "serviceTask":Ljava/lang/Runnable;
    .end local v3    # "serviceThread":Ljava/lang/Thread;
    :cond_0
    const/4 v4, 0x2

    return v4
.end method
