.class final Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 199
    const-string v4, "TRIGGER"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 200
    .local v3, "trigger":Ljava/lang/String;
    const-string v4, "SOURCE_LIST"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 202
    .local v0, "mDeleteList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;>;"
    new-instance v1, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;

    invoke-direct {v1, p0, p1, v3, v0}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 215
    .local v1, "runnable":Ljava/lang/Runnable;
    new-instance v2, Ljava/lang/Thread;

    const-string v4, "REQUEST_BACKUP_DELETE"

    invoke-direct {v2, v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 216
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 217
    return-void
.end method
