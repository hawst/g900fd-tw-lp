.class Lcom/samsung/android/scloud/backup/SamsungBackup$4;
.super Ljava/lang/Object;
.source "SamsungBackup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SamsungBackup;->showAutoBackupDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SamsungBackup;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v2, 0x1

    .line 382
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 383
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->backUtil:Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$400(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$300(Lcom/samsung/android/scloud/backup/SamsungBackup;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->startAutoBackupManager(Landroid/content/Context;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/SamsungBackup;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SamsungBackup$4;->this$0:Lcom/samsung/android/scloud/backup/SamsungBackup;

    # getter for: Lcom/samsung/android/scloud/backup/SamsungBackup;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SamsungBackup;->access$000(Lcom/samsung/android/scloud/backup/SamsungBackup;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupCheckBoxStatus(Z)V

    .line 386
    :cond_0
    return-void
.end method
