.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$SourceColumns;
.super Ljava/lang/Object;
.source "BaseBackupContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SourceColumns"
.end annotation


# static fields
.field public static final BACKUP:Ljava/lang/String; = "backuped"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DIRTY:Ljava/lang/String; = "dirty"

.field public static final KEY:Ljava/lang/String; = "key"
