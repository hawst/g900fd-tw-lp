.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;
.super Ljava/lang/Object;
.source "SetupWizardRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showSelectDataDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 602
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapterData:Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->getSelectedData()Ljava/util/HashMap;

    move-result-object v2

    .line 603
    .local v2, "selectedData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 604
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mValidSourceList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 605
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$7;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$200(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 607
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 608
    return-void
.end method
