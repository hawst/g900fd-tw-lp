.class public abstract Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.super Ljava/lang/Object;
.source "KVSResponseHandler.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;


# static fields
.field private static final TAG:Ljava/lang/String; = "KVSResponsHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method

.method public handleResponse(ILjava/lang/String;)V
    .locals 9
    .param p1, "statusCode"    # I
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 20
    const/4 v4, -0x1

    .line 21
    .local v4, "rCode":I
    const/4 v3, 0x0

    .line 22
    .local v3, "list":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .line 24
    .local v1, "jsonObj":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 25
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    .local v2, "jsonObj":Lorg/json/JSONObject;
    :try_start_1
    const-string v5, "rcode"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 26
    const-string v5, "rcode"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 28
    const/16 v5, 0x4e23

    if-eq v4, v5, :cond_0

    const v5, 0x98c7

    if-ne v4, v5, :cond_1

    .line 29
    :cond_0
    new-instance v5, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x137

    invoke-direct {v5, v6, p2}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 44
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 45
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_0
    const-string v5, "KVSResponsHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handle KVS Response : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    new-instance v5, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x130

    invoke-direct {v5, v6, v0}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v5

    .line 32
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    .restart local v2    # "jsonObj":Lorg/json/JSONObject;
    :cond_1
    const/16 v5, 0xc8

    if-eq p1, v5, :cond_2

    .line 33
    :try_start_2
    new-instance v5, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x12f

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "status error : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", response ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v5

    .line 36
    :cond_2
    const-string v5, "list"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 37
    const-string v5, "list"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 38
    const-string v5, "list"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 41
    :cond_3
    const-string v6, "KVSResponsHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RCODE ="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " / LIST Count : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    :goto_1
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, v4, v2, v3}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;->handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 48
    return-void

    .line 41
    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 44
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    .restart local v1    # "jsonObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    goto/16 :goto_0
.end method
