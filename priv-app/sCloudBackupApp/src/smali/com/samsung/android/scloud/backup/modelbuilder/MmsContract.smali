.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;
.super Ljava/lang/Object;
.source "MmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Addr;,
        Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$PendingMMS;,
        Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms;
    }
.end annotation


# static fields
.field public static final ADDR_COLUMNS:[Ljava/lang/String;

.field public static final BACKUP:Ljava/lang/String; = "backuped"

.field public static final CONTENT_CLASS:Ljava/lang/String; = "ct_cls"

.field public static final CT_L:Ljava/lang/String; = "ct_l"

.field public static final CT_T:Ljava/lang/String; = "ct_t"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DATE_SENT:Ljava/lang/String; = "date_sent"

.field public static final DELIVERY_TIME:Ljava/lang/String; = "d_tm"

.field public static final D_RPT:Ljava/lang/String; = "d_rpt"

.field public static final EXP:Ljava/lang/String; = "exp"

.field public static final KEY_ADDR:Ljava/lang/String; = "ADDRESS"

.field public static final KEY_DATA:Ljava/lang/String; = "DataValue"

.field public static final KEY_MMS:Ljava/lang/String; = "MMS"

.field public static final KEY_PART:Ljava/lang/String; = "PART"

.field public static final KEY_PENDING:Ljava/lang/String; = "PENDING"

.field public static final LOCKED:Ljava/lang/String; = "locked"

.field public static final MMS_COLUMNS:[Ljava/lang/String;

.field public static final MSG_BOX:Ljava/lang/String; = "msg_box"

.field public static final M_CLS:Ljava/lang/String; = "m_cls"

.field public static final M_ID:Ljava/lang/String; = "m_id"

.field public static final M_SIZE:Ljava/lang/String; = "m_size"

.field public static final M_TYPE:Ljava/lang/String; = "m_type"

.field public static final PART_COLUMNS:[Ljava/lang/String;

.field public static final PENDING_COLUMNS:[Ljava/lang/String;

.field public static final PRIORITY:Ljava/lang/String; = "pri"

.field public static final READ:Ljava/lang/String; = "read"

.field public static final READ_STATUS:Ljava/lang/String; = "read_status"

.field public static final REPORT_ALLOWED:Ljava/lang/String; = "rpt_a"

.field public static final RESERVED:Ljava/lang/String; = "reserved"

.field public static final RESPONSE_STATUS:Ljava/lang/String; = "resp_st"

.field public static final RESPONSE_TEXT:Ljava/lang/String; = "resp_txt"

.field public static final RETRIEVE_STATUS:Ljava/lang/String; = "retr_st"

.field public static final RETRIEVE_TEXT:Ljava/lang/String; = "retr_txt"

.field public static final RETRIEVE_TEXT_CHARSET:Ljava/lang/String; = "retr_txt_cs"

.field public static final RR:Ljava/lang/String; = "rr"

.field public static final SEEN:Ljava/lang/String; = "seen"

.field public static final STATUS:Ljava/lang/String; = "st"

.field public static final SUB:Ljava/lang/String; = "sub"

.field public static final SUBJECT_CHARSET:Ljava/lang/String; = "sub_cs"

.field public static final THREAD_ID:Ljava/lang/String; = "thread_id"

.field public static final TR_ID:Ljava/lang/String; = "tr_id"

.field public static final V:Ljava/lang/String; = "v"


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 275
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "source_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v4

    const-string v1, "msg_box"

    aput-object v1, v0, v5

    const-string v1, "read"

    aput-object v1, v0, v6

    const-string v1, "m_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ct_t"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ct_l"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "exp"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "m_cls"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "m_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "rr"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "tr_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "locked"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "date_sent"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "rpt_a"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "resp_st"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "retr_st"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "retr_txt"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "retr_txt_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "read_status"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "ct_cls"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "d_tm"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "resp_txt"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "seen"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;->MMS_COLUMNS:[Ljava/lang/String;

    .line 331
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "msg_id"

    aput-object v1, v0, v3

    const-string v1, "address"

    aput-object v1, v0, v4

    const-string v1, "charset"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "transaction_id"

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;->ADDR_COLUMNS:[Ljava/lang/String;

    .line 349
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mid"

    aput-object v1, v0, v3

    const-string v1, "chset"

    aput-object v1, v0, v4

    const-string v1, "cid"

    aput-object v1, v0, v5

    const-string v1, "cl"

    aput-object v1, v0, v6

    const-string v1, "ct"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "seq"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "text"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "transaction_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;->PART_COLUMNS:[Ljava/lang/String;

    .line 377
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "msg_id"

    aput-object v1, v0, v3

    const-string v1, "msg_type"

    aput-object v1, v0, v4

    const-string v1, "err_code"

    aput-object v1, v0, v5

    const-string v1, "err_type"

    aput-object v1, v0, v6

    const-string v1, "retry_index"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "last_try"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "due_time"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "proto_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;->PENDING_COLUMNS:[Ljava/lang/String;

    return-void
.end method
