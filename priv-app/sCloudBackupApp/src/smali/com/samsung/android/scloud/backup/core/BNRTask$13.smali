.class final Lcom/samsung/android/scloud/backup/core/BNRTask$13;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->getBackedupHistory(ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

.field final synthetic val$cnt:I

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;)V
    .locals 0

    .prologue
    .line 671
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$ctid:Ljava/lang/String;

    iput p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$cnt:I

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$context:Landroid/content/Context;

    iput-object p5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 6
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    const/4 v5, 0x4

    .line 674
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const-string v2, "BNRTask-"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDetails Finished - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$ctid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v5, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 675
    if-nez p1, :cond_0

    .line 676
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const-string v2, "REQUEST_GETDETAIL"

    const/16 v3, 0x67

    const/16 v4, 0x12d

    invoke-interface {v1, v2, v3, v4, p3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onFinished(Ljava/lang/String;IILjava/lang/Object;)V

    .line 696
    :goto_0
    return-void

    .line 680
    :cond_0
    const/16 v1, 0x4e26

    if-ne p1, v1, :cond_1

    .line 681
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const-string v2, "BNRTask-"

    const-string v3, "Wating for DETAILS from server"

    invoke-interface {v1, v5, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_1
    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$cnt:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 687
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "restoreReady sleep for retry - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$ctid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 688
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    :goto_1
    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$cnt:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$auth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$ctid:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/BNRTask;->getBackedupHistory(ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V

    goto :goto_0

    .line 689
    :catch_0
    move-exception v0

    .line 690
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "BNRTask-"

    const-string v2, "restoreReady retry sleep err"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 694
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v2, 0x136

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v1
.end method
