.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$TYPE;
.super Ljava/lang/Object;
.source "BaseBackupContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TYPE"
.end annotation


# static fields
.field public static final FIELD_TYPE_BLOB:I = 0x4

.field public static final FIELD_TYPE_FLOAT:I = 0x2

.field public static final FIELD_TYPE_INTEGER:I = 0x1

.field public static final FIELD_TYPE_NULL:I = 0x0

.field public static final FIELD_TYPE_STRING:I = 0x3
