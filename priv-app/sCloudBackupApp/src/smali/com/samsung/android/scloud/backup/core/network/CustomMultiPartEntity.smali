.class public Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;
.super Lorg/apache/http/entity/mime/MultipartEntity;
.source "CustomMultiPartEntity.java"


# static fields
.field private static final FILE_MULTIPART:Ljava/lang/String; = "file"


# instance fields
.field private final listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

.field private size:J


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .line 41
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .line 46
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .line 47
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 0
    .param p1, "mode"    # Lorg/apache/http/entity/mime/HttpMultipartMode;
    .param p2, "boundary"    # Ljava/lang/String;
    .param p3, "charset"    # Ljava/nio/charset/Charset;
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;Ljava/lang/String;Ljava/nio/charset/Charset;)V

    .line 52
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .line 53
    return-void
.end method


# virtual methods
.method public addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "contentBody"    # Lorg/apache/http/entity/mime/content/ContentBody;

    .prologue
    .line 65
    invoke-super {p0, p1, p2}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 67
    const-string v0, "file"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p2, Lorg/apache/http/entity/mime/content/FileBody;

    if-eqz v0, :cond_0

    .line 68
    invoke-interface {p2}, Lorg/apache/http/entity/mime/content/ContentBody;->getContentLength()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->size:J

    .line 69
    :cond_0
    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 4
    .param p1, "outstream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;

    iget-wide v2, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->size:J

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/network/CustomMultiPartEntity;->listener:Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    invoke-direct {v0, v2, v3, p1, v1}, Lcom/samsung/android/scloud/backup/core/network/CountingOutputStream;-><init>(JLjava/io/OutputStream;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    invoke-super {p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 59
    return-void
.end method
