.class public Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BackupUpDataListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mRestoreSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mValidSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/samsung/android/scloud/backup/common/MetaManager;)V
    .locals 5
    .param p1, "c"    # Landroid/content/Context;
    .param p3, "metaManager"    # Lcom/samsung/android/scloud/backup/common/MetaManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/android/scloud/backup/common/MetaManager;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "restoreSourceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 34
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 24
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mRestoreSourceList:Ljava/util/List;

    .line 26
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    .line 27
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    .line 35
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mRestoreSourceList:Ljava/util/List;

    .line 37
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 38
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    .line 40
    const/4 v1, 0x0

    .line 41
    .local v1, "key":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 42
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "key":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 43
    .restart local v1    # "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mRestoreSourceList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 45
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_0
    const-string v2, "VIPLIST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "BLACKLIST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 48
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 49
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v1, v2, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 65
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 70
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedData()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const v5, 0x7f070056

    const/4 v3, 0x1

    .line 77
    if-nez p2, :cond_1

    .line 78
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 79
    .local v1, "layoutInflator":Landroid/view/LayoutInflater;
    new-instance v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;-><init>()V

    .line 80
    .local v0, "holder":Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;
    const v2, 0x7f030001

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/RelativeLayout;

    .line 81
    .restart local p2    # "convertView":Landroid/view/View;
    const v2, 0x7f090002

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->cb:Landroid/widget/CheckBox;

    .line 82
    const v2, 0x7f090003

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    .line 83
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 87
    .end local v1    # "layoutInflator":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p2, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 88
    invoke-virtual {p2, v3}, Landroid/view/View;->setClickable(Z)V

    .line 90
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "CALLLOGS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 91
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 107
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mRestoreSourceList:Ljava/util/List;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 114
    new-instance v2, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$1;-><init>(Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;ILcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v3, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->cb:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v3, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    return-object p2

    .line 85
    .end local v0    # "holder":Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;
    goto :goto_0

    .line 92
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "SMS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 93
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070007

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 94
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "MMS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 95
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 96
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "HOMESCREEN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 97
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const v3, 0x7f07000c

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 98
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "VIPLIST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 99
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const v3, 0x7f07000a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 100
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "BLACKLIST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 101
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 102
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "SPAM"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 103
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const v3, 0x7f070008

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 104
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "CALLREJECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, v0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter$ViewHolder;->backedUpDataType:Landroid/widget/TextView;

    const v3, 0x7f070002

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1
.end method

.method public setSelectedData(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "selectedData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupUpDataListAdapter;->mSelectedData:Ljava/util/HashMap;

    .line 57
    return-void
.end method
