.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;
.super Ljava/lang/Object;
.source "BaseBackupContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseColumns"
.end annotation


# static fields
.field public static final DATE:Ljava/lang/String; = "date"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TRANSACTION_ID:Ljava/lang/String; = "transaction_id"

.field public static final VALUE:Ljava/lang/String; = "value"

.field public static final _ID:Ljava/lang/String; = "_id"
