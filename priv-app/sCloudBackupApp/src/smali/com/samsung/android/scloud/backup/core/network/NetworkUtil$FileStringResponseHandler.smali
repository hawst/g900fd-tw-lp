.class public abstract Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileStringResponseHandler;
.super Ljava/lang/Object;
.source "NetworkUtil.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "FileStringResponseHandler"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract handleFileStringResponse(Ljava/lang/String;)V
.end method

.method public handleResponse(Ljava/lang/String;JLjava/io/InputStream;)V
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "size"    # J
    .param p4, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 278
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, p4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 279
    .local v0, "br":Ljava/io/BufferedReader;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 281
    .local v3, "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    .end local v3    # "str":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 286
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "NetworkUtil"

    const-string v5, "handleFileStringResponse err "

    invoke-static {v4, v5, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v5, 0x138

    invoke-direct {v4, v5, v1}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4

    .line 283
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    .restart local v3    # "str":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileStringResponseHandler;->handleFileStringResponse(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 289
    return-void
.end method
