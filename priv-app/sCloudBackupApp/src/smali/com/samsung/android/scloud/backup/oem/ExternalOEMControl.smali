.class public Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;
.super Ljava/lang/Object;
.source "ExternalOEMControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IOEMControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl$Key;,
        Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl$METHOD;
    }
.end annotation


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl; = null

.field private static final TAG:Ljava/lang/String; = "ExternalIOEMControl"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl;

    return-object v0
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 230
    return-void
.end method

.method public getDataFromOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;IIJLjava/util/List;Ljava/lang/String;)Z
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "start"    # I
    .param p6, "maxCount"    # I
    .param p7, "maxSize"    # J
    .param p10, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 98
    .local p4, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p9, "output":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/4 v13, 0x4

    const-string v14, "ExternalIOEMControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDataFromOEM : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", keys : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", start : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p5

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14, v15}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v13

    move/from16 v0, p5

    if-ne v0, v13, :cond_0

    .line 100
    const/4 v13, 0x0

    .line 135
    :goto_0
    return v13

    .line 102
    :cond_0
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 103
    .local v7, "input":Landroid/os/Bundle;
    const-string v14, "to_upload_list"

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v13

    new-array v13, v13, [Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-interface {v0, v13}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    invoke-virtual {v7, v14, v13}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 104
    const-string v13, "start"

    move/from16 v0, p5

    invoke-virtual {v7, v13, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v13, "max_count"

    move/from16 v0, p6

    invoke-virtual {v7, v13, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    const-string v13, "max_size"

    move-wide/from16 v0, p7

    invoke-virtual {v7, v13, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 107
    const-string v13, "to_upload_file_uri"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "content://com.sec.android.sCloudBackupProvider"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, ""

    move-object/from16 v0, p10

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v14

    const-string v15, "getDataFromOEM"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v13, v14, v15, v0, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v11

    .line 111
    .local v11, "result":Landroid/os/Bundle;
    const-string v13, "key"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 112
    .local v10, "key":[Ljava/lang/String;
    const-string v13, "size"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v12

    .line 113
    .local v12, "size":[J
    const-string v13, "file_list"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 114
    .local v5, "fileList":[Ljava/lang/String;
    const-string v13, "file_checksum_list"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "checksumList":[Ljava/lang/String;
    if-nez v12, :cond_1

    .line 117
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 119
    :cond_1
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    array-length v13, v10

    if-ge v6, v13, :cond_3

    .line 120
    new-instance v8, Lcom/samsung/android/scloud/backup/data/BNRItem;

    aget-object v13, v10, v6

    invoke-direct {v8, v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 121
    .local v8, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    aget-wide v14, v12, v6

    invoke-virtual {v8, v14, v15}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 122
    aget-object v13, v5, v6

    if-eqz v13, :cond_2

    .line 123
    const/4 v13, 0x4

    const-string v14, "ExternalIOEMControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDataFromOEM ["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] file_list : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v5, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14, v15}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 124
    aget-object v13, v5, v6

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 125
    .local v4, "fileArr":[Ljava/lang/String;
    aget-object v13, v3, v6

    const-string v14, ","

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 126
    .local v2, "checksumArr":[Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    array-length v13, v4

    if-ge v9, v13, :cond_2

    .line 127
    const/4 v13, 0x4

    const-string v14, "ExternalIOEMControl"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDataFromOEM ["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "] file : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    aget-object v16, v4, v9

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v13, v14, v15}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 128
    aget-object v13, v4, v9

    aget-object v14, v2, v9

    invoke-virtual {v8, v13, v14}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 132
    .end local v2    # "checksumArr":[Ljava/lang/String;
    .end local v4    # "fileArr":[Ljava/lang/String;
    .end local v9    # "j":I
    :cond_2
    move-object/from16 v0, p9

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 135
    .end local v8    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_3
    const/4 v13, 0x1

    goto/16 :goto_0
.end method

.method public getTotalKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)Ljava/util/List;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x4

    .line 73
    const-string v6, "ExternalIOEMControl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getTotalKeys : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v9, v6, v7}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v7

    const-string v8, "getTotalKeys"

    invoke-virtual {v6, v7, v8, v10, v10}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 77
    .local v4, "result":Landroid/os/Bundle;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const-string v6, "key"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 79
    .local v2, "key":[Ljava/lang/String;
    const-string v6, "timestamp"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v5

    .line 81
    .local v5, "timestamp":[J
    if-nez v5, :cond_1

    .line 91
    :cond_0
    return-object v3

    .line 84
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v2

    if-ge v0, v6, :cond_0

    .line 85
    new-instance v1, Lcom/samsung/android/scloud/backup/data/BNRItem;

    aget-object v6, v2, v0

    invoke-direct {v1, v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 86
    .local v1, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    aget-wide v6, v5, v0

    invoke-virtual {v1, v6, v7}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setTimeStamp(J)V

    .line 87
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    const-string v6, "ExternalIOEMControl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Keys : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v2, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p2, v9, v6, v7}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public postOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "isSuccess"    # Z

    .prologue
    .line 141
    const/4 v3, 0x4

    const-string v4, "ExternalIOEMControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "postOperationOnBackup : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 142
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "postOperationOnBackup"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 147
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 149
    .local v1, "oemResult":Z
    if-nez v1, :cond_0

    .line 150
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x13d

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 152
    :cond_0
    return-void
.end method

.method public postOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "isSuccess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p4, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x4

    const-string v4, "ExternalIOEMControl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "postOperationOnRestore : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", success : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 215
    .local v0, "input":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v0, v3, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 216
    const-string v4, "restored_key_list"

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {p4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "postOperationOnRestore"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 220
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "is_success"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 222
    .local v1, "oemResult":Z
    if-nez v1, :cond_0

    .line 223
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x13d

    invoke-direct {v3, v4}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v3

    .line 224
    :cond_0
    return-void
.end method

.method public preOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    const/4 v6, 0x0

    .line 59
    const/4 v2, 0x4

    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preOperationOnBackup : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "preOperationOnBackup"

    invoke-virtual {v2, v3, v4, v6, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 63
    .local v1, "result":Landroid/os/Bundle;
    const-string v2, "is_success"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 65
    .local v0, "isSuccess":Z
    if-nez v0, :cond_0

    .line 66
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x13d

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 68
    :cond_0
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    const/4 v6, 0x0

    .line 156
    const/4 v2, 0x4

    const-string v3, "ExternalIOEMControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "preOperationOnRestore : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "preOperationOnRestore"

    invoke-virtual {v2, v3, v4, v6, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 160
    .local v1, "result":Landroid/os/Bundle;
    const-string v2, "is_success"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 162
    .local v0, "oemResult":Z
    if-nez v0, :cond_0

    .line 163
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x13d

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v2

    .line 166
    :cond_0
    return-void
.end method

.method public putDataToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Ljava/util/List;)Z
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 171
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p5, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v18, 0x4

    const-string v19, "ExternalIOEMControl"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "putDataToOEM : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", items : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p2

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 174
    :try_start_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v6, v0, [Ljava/lang/String;

    .line 175
    .local v6, "data":[Ljava/lang/String;
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v8, v0, [Ljava/lang/String;

    .line 177
    .local v8, "file":[Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_2

    .line 178
    move-object/from16 v0, p4

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 179
    .local v13, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v6, v9

    .line 180
    invoke-virtual {v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;->hasFile()Z

    move-result v18

    if-eqz v18, :cond_1

    .line 181
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 182
    .local v17, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v13}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getAttachmentFileInfo()Ljava/util/Map;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 183
    .local v5, "att":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 202
    .end local v5    # "att":Ljava/lang/String;
    .end local v6    # "data":[Ljava/lang/String;
    .end local v8    # "file":[Ljava/lang/String;
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v13    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v17    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v7

    .line 203
    .local v7, "e":Ljava/lang/Exception;
    const-string v18, "ExternalIOEMControl"

    const-string v19, "putDataToOEM oem err"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 204
    const/16 v18, 0x0

    .line 207
    .end local v7    # "e":Ljava/lang/Exception;
    :goto_2
    return v18

    .line 186
    .restart local v6    # "data":[Ljava/lang/String;
    .restart local v8    # "file":[Ljava/lang/String;
    .restart local v9    # "i":I
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v13    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .restart local v17    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v8, v9

    .line 177
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v17    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 190
    .end local v13    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_2
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 191
    .local v11, "input":Landroid/os/Bundle;
    const-string v18, "data"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v6}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 192
    const-string v18, "file_list"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v8}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 193
    const-string v18, "to_download_file_uri"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "content://com.sec.android.sCloudBackupProvider/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v19

    const-string v20, "putDataToOEM"

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3, v11}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v16

    .line 197
    .local v16, "result":Landroid/os/Bundle;
    const-string v18, "inserted_key_list"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 199
    .local v12, "insertedKey":[Ljava/lang/String;
    move-object v4, v12

    .local v4, "arr$":[Ljava/lang/String;
    array-length v15, v4

    .local v15, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_3
    if-ge v10, v15, :cond_3

    aget-object v14, v4, v10

    .line 200
    .local v14, "k":Ljava/lang/String;
    move-object/from16 v0, p5

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 199
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 207
    .end local v14    # "k":Ljava/lang/String;
    :cond_3
    const/16 v18, 0x1

    goto :goto_2
.end method
