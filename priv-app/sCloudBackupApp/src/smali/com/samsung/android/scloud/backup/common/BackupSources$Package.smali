.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupSources$Package;
.super Ljava/lang/Object;
.source "BackupSources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupSources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Package"
.end annotation


# static fields
.field public static final BLACKLIST:Ljava/lang/String; = "com.android.email"

.field public static final CALLREJECT:Ljava/lang/String; = "com.android.mms"

.field public static final CALL_LOGS:Ljava/lang/String; = "com.android.mms"

.field public static final MMS:Ljava/lang/String; = "com.android.mms"

.field public static final SMS:Ljava/lang/String; = "com.android.mms"

.field public static final SPAM:Ljava/lang/String; = "com.android.mms"

.field public static final VIPLIST:Ljava/lang/String; = "com.android.email"

.field public static final WALLPAPER:Ljava/lang/String; = "com.samsung.android.scloud.backup"
