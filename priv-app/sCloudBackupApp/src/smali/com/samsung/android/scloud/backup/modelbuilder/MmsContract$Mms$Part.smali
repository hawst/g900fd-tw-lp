.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms$Part;
.super Ljava/lang/Object;
.source "MmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Part"
.end annotation


# static fields
.field public static final CHSET:Ljava/lang/String; = "chset"

.field public static final CID:Ljava/lang/String; = "cid"

.field public static final CL:Ljava/lang/String; = "cl"

.field public static final CT:Ljava/lang/String; = "ct"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MSG_ID:Ljava/lang/String; = "mid"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SEQ:Ljava/lang/String; = "seq"

.field public static final TEXT:Ljava/lang/String; = "text"
