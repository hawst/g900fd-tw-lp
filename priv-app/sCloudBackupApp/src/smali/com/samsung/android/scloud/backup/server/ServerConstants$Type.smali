.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants$Type;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/server/ServerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Type"
.end annotation


# static fields
.field public static final AUTO_BACKUP:I = 0x6

.field public static final BACKUP:I = 0x1

.field public static final DELETE:I = 0x4

.field public static final DOWNLOAD:I = 0x5

.field public static final NONE:I = 0x0

.field public static final RESTORE:I = 0x2

.field public static final UPLOAD:I = 0x3
