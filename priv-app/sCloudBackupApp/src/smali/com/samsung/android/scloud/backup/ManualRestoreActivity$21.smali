.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showData3gDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1348
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1351
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setData3gAskStatusRestore(Z)V

    .line 1352
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1353
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    .line 1354
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayInitialRestoring()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1800(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    .line 1355
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceId(Ljava/lang/String;)V

    .line 1356
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startRestore()V

    .line 1360
    :goto_0
    return-void

    .line 1359
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    goto :goto_0
.end method
