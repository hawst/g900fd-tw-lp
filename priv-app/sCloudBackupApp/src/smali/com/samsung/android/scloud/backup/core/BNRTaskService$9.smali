.class final Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 273
    const-string v7, "SOURCE_LIST"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 274
    .local v5, "sourceList":[Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 275
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 276
    .local v4, "source":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;

    invoke-direct {v3, p0, v4, p1}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$9$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$9;Ljava/lang/String;Landroid/content/Context;)V

    .line 289
    .local v3, "runnable":Ljava/lang/Runnable;
    new-instance v6, Ljava/lang/Thread;

    const-string v7, "OPERATION_CANCEL"

    invoke-direct {v6, v3, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 290
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 275
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 294
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "runnable":Ljava/lang/Runnable;
    .end local v4    # "source":Ljava/lang/String;
    .end local v6    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
