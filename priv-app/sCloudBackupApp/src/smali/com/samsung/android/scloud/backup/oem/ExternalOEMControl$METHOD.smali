.class interface abstract Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl$METHOD;
.super Ljava/lang/Object;
.source "ExternalOEMControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/oem/ExternalOEMControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "METHOD"
.end annotation


# static fields
.field public static final GET_DATA_FROM_OEM:Ljava/lang/String; = "getDataFromOEM"

.field public static final GET_TOTAL_KEYS:Ljava/lang/String; = "getTotalKeys"

.field public static final POST_OPERATION_ON_BACKUP:Ljava/lang/String; = "postOperationOnBackup"

.field public static final POST_OPERATION_ON_RESTORE:Ljava/lang/String; = "postOperationOnRestore"

.field public static final PRE_OPERATION_ON_BACKUP:Ljava/lang/String; = "preOperationOnBackup"

.field public static final PRE_OPERATION_ON_RESTORE:Ljava/lang/String; = "preOperationOnRestore"

.field public static final PUT_DATA_TO_OEM:Ljava/lang/String; = "putDataToOEM"
