.class public Lcom/samsung/android/scloud/backup/auth/AuthControl;
.super Ljava/lang/Object;
.source "AuthControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/auth/IAuthControl;


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.DataRelayProvider"

.field private static final METHOD:Ljava/lang/String; = "getAuthInformation"

.field private static final RELAY_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "AuthControlForNewDataRelay"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "content://com.sec.android.DataRelayProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/auth/AuthControl;->RELAY_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public getAuthInformation(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ctid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/scloud/backup/common/BNRException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/samsung/android/scloud/backup/auth/AuthControl;->RELAY_URI:Landroid/net/Uri;

    const-string v10, "getAuthInformation"

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, p2, v11}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v7

    .line 43
    .local v7, "authBundle":Landroid/os/Bundle;
    invoke-static {}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->create()Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-result-object v0

    .line 45
    .local v0, "auth":Lcom/samsung/android/scloud/backup/auth/AuthManager;
    if-nez v7, :cond_0

    .line 46
    const-string v8, "AuthControlForNewDataRelay"

    const-string v9, "AuthBundle is NULL"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v8, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v9, 0x131

    invoke-direct {v8, v9}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(I)V

    throw v8

    .line 51
    :cond_0
    const-string v8, "status"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 52
    .local v1, "status":I
    const-string v8, "userId"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "userId":Ljava/lang/String;
    const-string v8, "regId"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "regId":Ljava/lang/String;
    const-string v8, "accessToken"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "accessToken":Ljava/lang/String;
    const-string v8, "baseUrl"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "baseUrl":Ljava/lang/String;
    const-string v8, "baseUrl2"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 58
    .local v6, "baseUrl2":Ljava/lang/String;
    const-string v8, "AuthControlForNewDataRelay"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "AuthInformation From New DataRelay : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/scloud/backup/auth/AuthManager;->updateAuthInformation(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-object v0
.end method
