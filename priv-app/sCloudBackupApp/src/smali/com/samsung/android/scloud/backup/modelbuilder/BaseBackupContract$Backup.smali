.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$Backup;
.super Ljava/lang/Object;
.source "BaseBackupContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Backup"
.end annotation


# static fields
.field public static final ANDROID_VER:Ljava/lang/String; = "android_version"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DID:Ljava/lang/String; = "did"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OPERATION:Ljava/lang/String; = "operation"

.field public static final SOFTWARE_VER:Ljava/lang/String; = "software_version"

.field public static final STATUS:Ljava/lang/String; = "status"
