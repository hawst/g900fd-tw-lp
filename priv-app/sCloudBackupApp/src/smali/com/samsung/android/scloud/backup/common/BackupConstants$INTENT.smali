.class public interface abstract Lcom/samsung/android/scloud/backup/common/BackupConstants$INTENT;
.super Ljava/lang/Object;
.source "BackupConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/BackupConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "INTENT"
.end annotation


# static fields
.field public static final ACTION_QUOTA_STORAGE_FULL:Ljava/lang/String; = "com.sec.android.sCloudQuotaApp.QUOTA_FULL"

.field public static final ACTION_QUOTA_WARNING_INTENT:Ljava/lang/String; = "com.sec.android.sCloudQuotaApp.QUOTA_WARNING"

.field public static final ACTION_SAMSUNG_ACCOUNT_SIGN_IN:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

.field public static final ACTION_SAMSUNG_ACCOUNT_SIGN_OUT:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

.field public static final ACTION_SERVER_BUSY_FINISHED:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.ACTION_SERVER_BUSY_FINISHED"

.field public static final ACTION_SETUP_WIZARD_COMPLETE:Ljava/lang/String; = "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

.field public static final AUTOBACKUP_ALARM:Ljava/lang/String; = "com.sec.android.SecBackupApp.AutoBackup"

.field public static final AUTO_BACKUP_START:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.AUTOBACKUP_NOW"

.field public static final AUTO_BACKUP_STOP:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.AUTOBACKUP_STOP"

.field public static final DATASYNC_AUTO_BACKUP_START:Ljava/lang/String; = "com.sec.android.sCloudDataSync.AUTOBACKUP_NOW"

.field public static final DATASYNC_AUTO_BACKUP_STOP:Ljava/lang/String; = "com.sec.android.sCloudDataSync.AUTOBACKUP_STOP"

.field public static final REQUEST_RESTORE:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.REQUEST_RESTORE"

.field public static final RESTORE_READY:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.RESTORE_READY"

.field public static final START_AUTOBACKUP_MANAGER:Ljava/lang/String; = "com.sec.android.SecBackupApp.AutoBackupManager.Start"

.field public static final SYNC_BACKUP:Ljava/lang/String; = "com.sec.android.sCloudSync.Backup"

.field public static final SYNC_BACKUP_REDIRECT:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.Redirect"

.field public static final SYNC_FAILED:Ljava/lang/String; = "com.sec.android.sCloudBackupApp.SYNC_FAILED"
