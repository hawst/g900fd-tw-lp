.class public abstract Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;
.super Ljava/lang/Object;
.source "NetworkUtil.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/network/NetworkUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "JSONResponseHandler"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract handleJSONResponse(Lorg/json/JSONObject;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation
.end method

.method public handleResponse(ILjava/lang/String;)V
    .locals 4
    .param p1, "statusCode"    # I
    .param p2, "body"    # Ljava/lang/String;

    .prologue
    .line 263
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 264
    .local v1, "json":Lorg/json/JSONObject;
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$JSONResponseHandler;->handleJSONResponse(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    return-void

    .line 265
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "NetworkUtil"

    const-string v3, "handleJSONResponse err "

    invoke-static {v2, v3, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 267
    new-instance v2, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v3, 0x130

    invoke-direct {v2, v3, v0}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v2
.end method
