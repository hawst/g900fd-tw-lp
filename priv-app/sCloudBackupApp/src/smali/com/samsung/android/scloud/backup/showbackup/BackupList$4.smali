.class Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;
.super Ljava/lang/Object;
.source "BackupList.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/showbackup/BackupList;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 383
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCount()I

    move-result v2

    .line 384
    .local v2, "len":I
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 385
    .local v0, "checked":Landroid/util/SparseBooleanArray;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 386
    const-string v3, "BackupList"

    const-string v4, "DeleteList should be cleared before creating it"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 389
    :cond_0
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Length of List :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Length of SparseBoolenArray :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 392
    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 393
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Posiiton is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v4, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getDeviceID()Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v3, v3, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backupList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {v5, v6, v3, v7}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 397
    :cond_2
    const-string v3, "BackupList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Delete List Size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v5, v5, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    iget-object v4, v4, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->startDelete(Ljava/util/ArrayList;)V

    .line 399
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/showbackup/BackupList$4;->this$0:Lcom/samsung/android/scloud/backup/showbackup/BackupList;

    # invokes: Lcom/samsung/android/scloud/backup/showbackup/BackupList;->backTolist()V
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/showbackup/BackupList;->access$200(Lcom/samsung/android/scloud/backup/showbackup/BackupList;)V

    .line 400
    return-void
.end method
