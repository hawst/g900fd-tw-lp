.class Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;->handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$mDeleteList:Ljava/util/ArrayList;

.field final synthetic val$trigger:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTaskService$6;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$trigger:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$mDeleteList:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 205
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_BACKUPDELETE 1: "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    if-nez v0, :cond_1

    .line 207
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModelManager.getInstance() is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$context:Landroid/content/Context;

    const-string v2, "REQUEST_BACKUP_DELETE"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$trigger:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$mDeleteList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteBackupUsage(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 211
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REQUEST_BACKUPDELETE 2: "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    return-void

    .line 208
    :cond_1
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$6$1;->val$context:Landroid/content/Context;

    const-string v2, "REQUEST_BACKUP_DELETE"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 209
    # getter for: Lcom/samsung/android/scloud/backup/core/BNRTaskService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/scloud/backup/core/BNRTaskService;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ModelManager.getInstance().getBNRManager(context, BackupConstants.Action.REQUEST_BACKUP_DELETE) is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
