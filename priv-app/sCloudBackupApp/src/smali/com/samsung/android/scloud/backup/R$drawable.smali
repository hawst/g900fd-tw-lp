.class public final Lcom/samsung/android/scloud/backup/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_bar_button:I = 0x7f020000

.field public static final auto_bg:I = 0x7f020001

.field public static final backed_up:I = 0x7f020002

.field public static final bottom_button_text_color_v:I = 0x7f020003

.field public static final example:I = 0x7f020004

.field public static final fontcolor_selector_desc:I = 0x7f020005

.field public static final fontcolor_selector_desc_dark:I = 0x7f020006

.field public static final fontcolor_selector_title:I = 0x7f020007

.field public static final fontcolor_selector_title_dark:I = 0x7f020008

.field public static final fontcolor_selector_title_t:I = 0x7f020009

.field public static final ic_settings_cloud:I = 0x7f02000a

.field public static final list_bg:I = 0x7f02000b

.field public static final list_bg_dark:I = 0x7f02000c

.field public static final list_bg_k:I = 0x7f02000d

.field public static final list_bg_t_light:I = 0x7f02000e

.field public static final list_bg_v:I = 0x7f02000f

.field public static final quick_notify_bg:I = 0x7f020010

.field public static final setupwizard_bg_dark:I = 0x7f020011

.field public static final setupwizard_bg_h:I = 0x7f020012

.field public static final setupwizard_bg_k:I = 0x7f020013

.field public static final setupwizard_bg_kk:I = 0x7f020014

.field public static final setupwizard_bg_land_dark:I = 0x7f020015

.field public static final setupwizard_bg_land_h:I = 0x7f020016

.field public static final setupwizard_bg_land_k:I = 0x7f020017

.field public static final setupwizard_bg_land_kk:I = 0x7f020018

.field public static final setupwizard_bg_tablet:I = 0x7f020019

.field public static final setupwizard_bg_tablet_land:I = 0x7f02001a

.field public static final setupwizard_bg_v:I = 0x7f02001b

.field public static final setupwizard_bg_v_land:I = 0x7f02001c

.field public static final setupwizard_bottom_button_bg:I = 0x7f02001d

.field public static final setupwizard_bottom_button_divider:I = 0x7f02001e

.field public static final setupwizard_navibar_bg:I = 0x7f02001f

.field public static final setupwizard_navibar_divider:I = 0x7f020020

.field public static final setupwizard_next:I = 0x7f020021

.field public static final setupwizard_next_arrow:I = 0x7f020022

.field public static final setupwizard_next_arrow_dark:I = 0x7f020023

.field public static final setupwizard_next_arrow_v:I = 0x7f020024

.field public static final setupwizard_next_dark:I = 0x7f020025

.field public static final setupwizard_next_dim:I = 0x7f020026

.field public static final setupwizard_next_dim_dark:I = 0x7f020027

.field public static final setupwizard_next_dim_h:I = 0x7f020028

.field public static final setupwizard_next_dim_n1:I = 0x7f020029

.field public static final setupwizard_next_dim_v:I = 0x7f02002a

.field public static final setupwizard_next_press:I = 0x7f02002b

.field public static final setupwizard_next_press_v:I = 0x7f02002c

.field public static final setupwizard_next_v:I = 0x7f02002d

.field public static final setupwizard_prev_arrow:I = 0x7f02002e

.field public static final setupwizard_prev_arrow_dark:I = 0x7f02002f

.field public static final setupwizard_prev_arrow_v:I = 0x7f020030

.field public static final setupwizard_prev_dark:I = 0x7f020031

.field public static final setupwizard_prev_dim_h:I = 0x7f020032

.field public static final setupwizard_prev_press_v:I = 0x7f020033

.field public static final setupwizard_prev_v:I = 0x7f020034

.field public static final setupwizard_previous:I = 0x7f020035

.field public static final setupwizard_previous_dim:I = 0x7f020036

.field public static final setupwizard_previous_press:I = 0x7f020037

.field public static final setupwizard_title_bg:I = 0x7f020038

.field public static final setupwizard_title_bg_h:I = 0x7f020039

.field public static final stat_notify_cloud:I = 0x7f02003a

.field public static final tw_ab_bottom_transparent_dark_holo:I = 0x7f02003b

.field public static final tw_ab_bottom_transparent_light_holo:I = 0x7f02003c

.field public static final tw_ab_bottom_transparent_light_holo_t:I = 0x7f02003d

.field public static final tw_ab_bottom_transparent_mtrl:I = 0x7f02003e

.field public static final tw_ab_transparent_light:I = 0x7f02003f

.field public static final tw_action_bar_icon_delete_disabled_holo_dark:I = 0x7f020040

.field public static final tw_action_bar_icon_delete_holo_dark:I = 0x7f020041

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f020042

.field public static final tw_action_item_background_pressed_holo_dark:I = 0x7f020043

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f020044

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f020045

.field public static final tw_bm_left_focused_holo_dark:I = 0x7f020046

.field public static final tw_bm_left_focused_holo_light:I = 0x7f020047

.field public static final tw_bm_left_focused_holo_light_v:I = 0x7f020048

.field public static final tw_bm_left_normal_holo_dark:I = 0x7f020049

.field public static final tw_bm_left_normal_holo_light:I = 0x7f02004a

.field public static final tw_bm_left_pressed_holo_dark:I = 0x7f02004b

.field public static final tw_bm_left_pressed_holo_light:I = 0x7f02004c

.field public static final tw_bm_left_pressed_holo_light_v:I = 0x7f02004d

.field public static final tw_bm_left_selected_holo_dark:I = 0x7f02004e

.field public static final tw_bm_left_selected_holo_light:I = 0x7f02004f

.field public static final tw_bm_left_selected_holo_light_v:I = 0x7f020050

.field public static final tw_bm_right_focused_holo_dark:I = 0x7f020051

.field public static final tw_bm_right_focused_holo_light:I = 0x7f020052

.field public static final tw_bm_right_focused_holo_light_v:I = 0x7f020053

.field public static final tw_bm_right_normal_holo_dark:I = 0x7f020054

.field public static final tw_bm_right_normal_holo_light:I = 0x7f020055

.field public static final tw_bm_right_normal_holo_light_v:I = 0x7f020056

.field public static final tw_bm_right_pressed_holo_dark:I = 0x7f020057

.field public static final tw_bm_right_pressed_holo_light:I = 0x7f020058

.field public static final tw_bm_right_pressed_holo_light_v:I = 0x7f020059

.field public static final tw_bm_right_selected_holo_dark:I = 0x7f02005a

.field public static final tw_bm_right_selected_holo_light:I = 0x7f02005b

.field public static final tw_bm_right_selected_holo_light_v:I = 0x7f02005c

.field public static final tw_bottom_bg_p:I = 0x7f02005d

.field public static final tw_btn_default_focused_holo_dark_h:I = 0x7f02005e

.field public static final tw_btn_default_pressed_holo_dark_h:I = 0x7f02005f

.field public static final tw_btn_icon_next_holo_dark:I = 0x7f020060

.field public static final tw_btn_icon_next_holo_light_t:I = 0x7f020061

.field public static final tw_btn_icon_previous_holo_dark:I = 0x7f020062

.field public static final tw_btn_next_1btn_default_holo_dark:I = 0x7f020063

.field public static final tw_btn_next_default_holo_dark:I = 0x7f020064

.field public static final tw_btn_next_default_holo_dark_h:I = 0x7f020065

.field public static final tw_btn_next_default_holo_dark_k:I = 0x7f020066

.field public static final tw_btn_previous_default_holo_dark:I = 0x7f020067

.field public static final tw_btn_previous_default_holo_dark_h:I = 0x7f020068

.field public static final tw_btn_previous_default_holo_dark_k:I = 0x7f020069

.field public static final tw_dialog_list_section_divider_holo_light:I = 0x7f02006a

.field public static final tw_divider_ab_holo_light:I = 0x7f02006b

.field public static final tw_fullscreen_background_dark:I = 0x7f02006c

.field public static final tw_fullscreen_background_light:I = 0x7f02006d

.field public static final tw_list_disabled_holo_dark:I = 0x7f02006e

.field public static final tw_list_divider_holo_dark:I = 0x7f02006f

.field public static final tw_list_divider_holo_light:I = 0x7f020070

.field public static final tw_list_focused_holo:I = 0x7f020071

.field public static final tw_list_focused_holo_dark:I = 0x7f020072

.field public static final tw_list_focused_holo_dark_k:I = 0x7f020073

.field public static final tw_list_focused_holo_light:I = 0x7f020074

.field public static final tw_list_pressed_holo_dark:I = 0x7f020075

.field public static final tw_list_pressed_holo_dark_k:I = 0x7f020076

.field public static final tw_list_pressed_holo_light:I = 0x7f020077

.field public static final tw_list_pressed_holo_light_v:I = 0x7f020078

.field public static final tw_list_section_divider_focused_holo_dark:I = 0x7f020079

.field public static final tw_list_section_divider_focused_holo_dark_h:I = 0x7f02007a

.field public static final tw_list_section_divider_focused_holo_light:I = 0x7f02007b

.field public static final tw_list_section_divider_holo_dark:I = 0x7f02007c

.field public static final tw_list_section_divider_holo_light:I = 0x7f02007d

.field public static final tw_list_section_divider_pressed_holo_dark:I = 0x7f02007e

.field public static final tw_list_section_divider_pressed_holo_dark_h:I = 0x7f02007f

.field public static final tw_list_section_divider_pressed_holo_light:I = 0x7f020080

.field public static final tw_list_section_divider_selected_holo_dark:I = 0x7f020081

.field public static final tw_list_section_divider_selected_holo_dark_h:I = 0x7f020082

.field public static final tw_list_section_divider_selected_holo_light:I = 0x7f020083

.field public static final tw_list_selected_holo_light:I = 0x7f020084

.field public static final tw_list_selector_disabled_holo_dark:I = 0x7f020085

.field public static final tw_list_selector_disabled_holo_light:I = 0x7f020086

.field public static final tw_preference_list_divider_holo_light:I = 0x7f020087

.field public static final tw_setupwizard_next_button:I = 0x7f020088

.field public static final tw_setupwizard_next_button_dark:I = 0x7f020089

.field public static final tw_setupwizard_next_button_h:I = 0x7f02008a

.field public static final tw_setupwizard_next_button_k:I = 0x7f02008b

.field public static final tw_setupwizard_next_button_kk:I = 0x7f02008c

.field public static final tw_setupwizard_next_button_t:I = 0x7f02008d

.field public static final tw_setupwizard_next_button_v:I = 0x7f02008e

.field public static final tw_setupwizard_prev_button:I = 0x7f02008f

.field public static final tw_setupwizard_prev_button_dark:I = 0x7f020090

.field public static final tw_setupwizard_prev_button_v:I = 0x7f020091

.field public static final tw_setupwizard_previous_button:I = 0x7f020092

.field public static final tw_setupwizard_previous_button_h:I = 0x7f020093

.field public static final tw_setupwizard_previous_button_k:I = 0x7f020094

.field public static final tw_setupwizard_previous_button_kk:I = 0x7f020095

.field public static final tw_setupwizard_previous_button_t:I = 0x7f020096

.field public static final tw_setupwizard_previous_button_v:I = 0x7f020097


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
