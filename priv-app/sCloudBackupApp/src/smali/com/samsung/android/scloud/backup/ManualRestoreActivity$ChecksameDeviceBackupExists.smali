.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;
.super Landroid/os/AsyncTask;
.source "ManualRestoreActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChecksameDeviceBackupExists"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method private constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1656
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p2, "x1"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;

    .prologue
    .line 1656
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x1

    .line 1660
    const-string v3, "ManualRestoreActivity"

    const-string v4, "[processRestoreReady]"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 1663
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$2300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1664
    .local v1, "currentDeviceId":Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1665
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 1666
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1667
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v3, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)I

    .line 1668
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iput-boolean v5, v3, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 1674
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    return-object v3

    .line 1664
    .restart local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1656
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-boolean v0, v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->sameDeviceBackupExistsFlag:Z

    if-nez v0, :cond_0

    .line 1681
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showSelectDeviceDialog()V

    .line 1692
    :goto_0
    return-void

    .line 1684
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1302(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1685
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceIndex(I)V

    .line 1686
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1402(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1687
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1688
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1690
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # invokes: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1656
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
