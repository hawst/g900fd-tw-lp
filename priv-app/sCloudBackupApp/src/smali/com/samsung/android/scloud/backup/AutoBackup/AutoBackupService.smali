.class public Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;
.super Lcom/samsung/android/scloud/backup/core/BNRService;
.source "AutoBackupService.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field mSourceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSourceStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetServiceType:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;-><init>()V

    .line 43
    const-string v0, "AutoBackupService"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->TAG:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    .line 47
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 50
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x65

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mTargetServiceType:[I

    return-void
.end method

.method private autoBackupCancelled(Ljava/lang/String;)V
    .locals 6
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 180
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AutobackupCancelled"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "backupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 184
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 185
    const/4 v0, 0x1

    goto :goto_0

    .line 188
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    if-nez v0, :cond_2

    .line 189
    invoke-direct {p0, v5}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->finishAutoBackup(Z)V

    .line 190
    :cond_2
    return-void
.end method

.method private autoBackupFailed(Ljava/lang/String;)V
    .locals 7
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 193
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AutobackupFailed"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "backupStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 197
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 198
    const/4 v0, 0x1

    goto :goto_0

    .line 201
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v6, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 202
    if-nez v0, :cond_2

    .line 203
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->finishAutoBackup(Z)V

    .line 204
    :cond_2
    return-void
.end method

.method private autoBackupSuccess(Ljava/lang/String;)V
    .locals 8
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "AutobackupSuccess"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "backupStatus":Z
    const/4 v3, 0x1

    .line 161
    .local v3, "success":Z
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 163
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v6, :cond_1

    .line 164
    const/4 v0, 0x1

    .line 166
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 167
    const/4 v3, 0x0

    goto :goto_0

    .line 170
    .end local v2    # "key":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, p1, v6, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 171
    if-nez v0, :cond_3

    .line 172
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->finishAutoBackup(Z)V

    .line 173
    if-eqz v3, :cond_3

    .line 174
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastBackupTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastAutoBackupTime(J)V

    .line 176
    :cond_3
    return-void
.end method

.method private finishAutoBackup(Z)V
    .locals 2
    .param p1, "bSuccess"    # Z

    .prologue
    .line 138
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 139
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 140
    .local v0, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->resetAutoBackupAlarm(Landroid/content/Context;Z)V

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->stopSelf()V

    .line 142
    return-void
.end method

.method private initMapValues()V
    .locals 4

    .prologue
    .line 151
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 152
    .local v1, "source":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 154
    .end local v1    # "source":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private requestAllCancelBackup()V
    .locals 8

    .prologue
    .line 207
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_CANCEL"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v5, "service":Landroid/content/Intent;
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 209
    const/4 v1, 0x0

    .line 210
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, "cancelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 212
    .local v3, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 213
    add-int/lit8 v1, v1, 0x1

    .line 214
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    if-lez v1, :cond_2

    .line 218
    new-array v4, v1, [Ljava/lang/String;

    .line 219
    .local v4, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 222
    .end local v4    # "mSourceArr":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private requestBackup()V
    .locals 5

    .prologue
    .line 226
    new-instance v1, Landroid/content/Intent;

    const-string v2, "REQUEST_BACKUP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v1, "service":Landroid/content/Intent;
    const-class v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 228
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [Ljava/lang/String;

    .line 229
    .local v0, "mSourceArr":[Ljava/lang/String;
    const-string v3, "SOURCE_LIST"

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    const-string v2, "TRIGGER"

    const-string v3, "system"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 231
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 232
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->initMapValues()V

    .line 233
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityAutoBackup()V

    .line 234
    return-void
.end method


# virtual methods
.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, "AutoBackupService"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mTargetServiceType:[I

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->onCreate()V

    .line 132
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    .line 133
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    .line 135
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "############ Auto-Backup service stopped. ############## "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRService;->onDestroy()V

    .line 148
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 3
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    .line 61
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 62
    packed-switch p2, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_1
    const/16 v0, 0x12d

    if-ne p3, v0, :cond_1

    .line 69
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->autoBackupSuccess(Ljava/lang/String;)V

    .line 75
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 70
    :cond_1
    const/16 v0, 0x132

    if-ne p3, v0, :cond_2

    .line 71
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->autoBackupCancelled(Ljava/lang/String;)V

    goto :goto_1

    .line 74
    :cond_2
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->autoBackupFailed(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:203 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 82
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:202 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:204 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 104
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/scloud/backup/core/BNRService;->onStartCommand(Landroid/content/Intent;II)I

    .line 106
    if-eqz p1, :cond_2

    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Auto-Backup service started. And its intent Action is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    .line 109
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.sCloudBackupApp.AUTOBACKUP_NOW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SourceList is empty!!!"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.sec.android.sCloudBackupApp.AUTOBACKUP_STOP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 120
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->getTag()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Stop Auto Backup Service"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mSourceStatusMap:Ljava/util/Map;

    if-eqz v1, :cond_2

    .line 122
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->requestAllCancelBackup()V

    .line 126
    :cond_2
    const/4 v1, 0x2

    return v1

    .line 114
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    .local v0, "uIntentService":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupService;->requestBackup()V

    goto :goto_0
.end method
