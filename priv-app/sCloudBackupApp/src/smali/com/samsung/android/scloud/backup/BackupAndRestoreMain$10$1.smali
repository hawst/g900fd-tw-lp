.class Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;
.super Ljava/lang/Object;
.source "BackupAndRestoreMain.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;)V
    .locals 0

    .prologue
    .line 695
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;->this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 698
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;->this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->removeRemoteTimeoutMessage(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$400(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;I)V

    .line 699
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;->this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;->this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # getter for: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$100(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10$1;->this$1:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;->this$0:Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    # invokes: Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startRestoreActivity()V
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->access$500(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    .line 703
    :cond_0
    return-void
.end method
