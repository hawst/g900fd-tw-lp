.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants$STATUS;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/server/ServerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "STATUS"
.end annotation


# static fields
.field public static final FAILED:I = 0x1

.field public static final NONE:I = 0x0

.field public static final PROGRESS:I = 0x4

.field public static final QUOTA_CLEARING:I = 0x7

.field public static final QUOTA_FAILED:I = 0x5

.field public static final SERVER_BUSY:I = 0x6

.field public static final START:I = 0x3

.field public static final SUCCESS:I = 0x2
