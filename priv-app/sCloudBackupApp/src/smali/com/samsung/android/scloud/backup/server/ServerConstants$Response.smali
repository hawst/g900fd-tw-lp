.class public interface abstract Lcom/samsung/android/scloud/backup/server/ServerConstants$Response;
.super Ljava/lang/Object;
.source "ServerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/server/ServerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Response"
.end annotation


# static fields
.field public static final AVAILABLE:Ljava/lang/String; = "available"

.field public static final BNR:Ljava/lang/String; = "bnr"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DEVICENAME:Ljava/lang/String; = "name"

.field public static final FILE_LIST:Ljava/lang/String; = "file_list"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final LEVEL:Ljava/lang/String; = "level"

.field public static final LIST:Ljava/lang/String; = "list"

.field public static final MAX_TIMESTAMP:Ljava/lang/String; = "maxTimestamp"

.field public static final NEXTKEY:Ljava/lang/String; = "nextKey"

.field public static final QUOTA:Ljava/lang/String; = "quota"

.field public static final RCODE:Ljava/lang/String; = "rcode"

.field public static final RMSG:Ljava/lang/String; = "rmsg"

.field public static final SERVER_TIMESTAMP:Ljava/lang/String; = "serverTimestamp"

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final STORAGEINFO:Ljava/lang/String; = "storageinfo"

.field public static final TIMESTAMP:Ljava/lang/String; = "timestamp"

.field public static final TIMESTAMPDATE:Ljava/lang/String; = "date"

.field public static final USAGE:Ljava/lang/String; = "usage"

.field public static final VALUE:Ljava/lang/String; = "value"
