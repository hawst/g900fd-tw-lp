.class public Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SCloudBackupReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$1;,
        Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SCloudBackupReceiver"


# instance fields
.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 172
    return-void
.end method

.method private handleAutoBackup(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountExist"    # Z

    .prologue
    const/4 v2, 0x1

    .line 125
    if-ne p2, v2, :cond_1

    .line 126
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 127
    const-string v1, "SCloudBackupReceiver"

    const-string v2, "AutoBackupStatus is set as true. Start Auto Backup Manager"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 129
    .local v0, "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->startAutoBackupManager(Landroid/content/Context;)V

    .line 138
    .end local v0    # "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    :goto_0
    return-void

    .line 131
    :cond_0
    const-string v1, "SCloudBackupReceiver"

    const-string v2, "AutoBackupStatus is set as false."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_1
    const-string v1, "SCloudBackupReceiver"

    const-string v2, "Stop Auto Backup Manager"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-static {}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->getBackupUtilHandler()Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;

    move-result-object v0

    .line 136
    .restart local v0    # "alarmManager":Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;
    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/AutoBackup/AutoBackupUtil;->stopAutoBackupManager(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private processAccountRemoveAction(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const-string v4, "SCloudBackupReceiver"

    const-string v5, "Account Removed..."

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    .line 105
    .local v2, "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v1

    .line 107
    .local v1, "mValidSourceList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Landroid/content/Intent;

    const-string v4, "REQUEST_BACKUP_CLEARED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 108
    .local v3, "service":Landroid/content/Intent;
    const-class v4, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 109
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    new-array v0, v4, [Ljava/lang/String;

    .line 110
    .local v0, "mSourceArr":[Ljava/lang/String;
    const-string v5, "SOURCE_LIST"

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v4, "TRIGGER"

    const-string v5, "ACCOUNT_REMOVED"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 112
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 114
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->clear()V

    .line 116
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "service":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 117
    .restart local v3    # "service":Landroid/content/Intent;
    const-class v4, Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v3, p1, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 118
    const-string v5, "SOURCE_LIST"

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    const-string v4, "Notification Type"

    const/16 v5, 0x69

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 120
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 121
    return-void
.end method

.method private processStartAutoRestore(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 142
    const-string v5, "SCloudBackupReceiver"

    const-string v6, "[processStartAutoRestore]"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    .line 144
    .local v1, "metaManager":Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, "target":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 147
    const-string v5, "connectivity"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 148
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 149
    .local v4, "uWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 151
    .local v3, "uMobile":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 152
    :cond_1
    const-string v5, "SCloudBackupReceiver"

    const-string v6, "[processStartAutoRestore][startRestore]"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->startAutoRestore(Landroid/content/Context;)V

    .line 164
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    .end local v3    # "uMobile":Landroid/net/NetworkInfo;
    .end local v4    # "uWifi":Landroid/net/NetworkInfo;
    :cond_2
    :goto_0
    return-void

    .line 155
    .restart local v0    # "connManager":Landroid/net/ConnectivityManager;
    .restart local v3    # "uMobile":Landroid/net/NetworkInfo;
    .restart local v4    # "uWifi":Landroid/net/NetworkInfo;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f070079

    invoke-static {p1, v6}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private startAutoRestore(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    const-string v1, "SCloudBackupReceiver"

    const-string v2, "[startAutoRestore]  thread"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    new-instance v0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$1;)V

    .line 169
    .local v0, "receiverThread":Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;
    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver$ReceiverThread;->start()V

    .line 170
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    .line 48
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "action":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 51
    const-string v5, "SCloudBackupReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[onReceive] action Name : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    if-eqz v2, :cond_1

    .line 53
    const-string v5, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 54
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 55
    .local v4, "manager":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 57
    .local v0, "accountArr":[Landroid/accounts/Account;
    if-eqz v4, :cond_0

    .line 58
    const-string v5, "com.osp.app.signin"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 60
    :cond_0
    if-eqz v0, :cond_2

    array-length v5, v0

    if-lez v5, :cond_2

    .line 96
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v4    # "manager":Landroid/accounts/AccountManager;
    :cond_1
    :goto_0
    return-void

    .line 65
    .restart local v0    # "accountArr":[Landroid/accounts/Account;
    .restart local v4    # "manager":Landroid/accounts/AccountManager;
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->processAccountRemoveAction(Landroid/content/Context;)V

    .line 66
    const/4 v5, 0x0

    invoke-direct {p0, p1, v5}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->handleAutoBackup(Landroid/content/Context;Z)V

    goto :goto_0

    .line 68
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v4    # "manager":Landroid/accounts/AccountManager;
    :cond_3
    const-string v5, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 69
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    .line 70
    .restart local v4    # "manager":Landroid/accounts/AccountManager;
    const/4 v0, 0x0

    .line 71
    .restart local v0    # "accountArr":[Landroid/accounts/Account;
    if-eqz v4, :cond_4

    .line 72
    const-string v5, "com.osp.app.signin"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 74
    :cond_4
    if-eqz v0, :cond_1

    array-length v5, v0

    if-lez v5, :cond_1

    .line 76
    invoke-direct {p0, p1, v8}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->handleAutoBackup(Landroid/content/Context;Z)V

    goto :goto_0

    .line 78
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v4    # "manager":Landroid/accounts/AccountManager;
    :cond_5
    const-string v5, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 79
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 80
    .local v3, "am":Landroid/accounts/AccountManager;
    if-eqz v3, :cond_1

    .line 81
    const-string v5, "com.osp.app.signin"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 82
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v5, v1

    if-eqz v5, :cond_6

    .line 83
    const-string v5, "SCloudBackupReceiver"

    const-string v6, "Account exist, Start AutoBackup Manager"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0, p1, v8}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->handleAutoBackup(Landroid/content/Context;Z)V

    .line 86
    :cond_6
    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/samsung/android/scloud/backup/model/ModelManager;->initBNRManager(Landroid/content/Context;)V

    goto :goto_0

    .line 89
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v3    # "am":Landroid/accounts/AccountManager;
    :cond_7
    const-string v5, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 90
    const-string v5, "SCloudBackupReceiver"

    const-string v6, "setup wizard completed, start restore and AutoBackup"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/SCloudBackupReceiver;->processStartAutoRestore(Landroid/content/Context;)V

    goto :goto_0

    .line 94
    :cond_8
    const-string v5, "SCloudBackupReceiver"

    const-string v6, "Other Intent"

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
