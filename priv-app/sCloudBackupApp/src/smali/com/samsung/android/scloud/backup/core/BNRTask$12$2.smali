.class Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;
.super Ljava/lang/Object;
.source "BNRTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$FileResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask$12;->handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

.field final synthetic val$fileCnt:F

.field final synthetic val$item:Lcom/samsung/android/scloud/backup/data/BNRItem;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRTask$12;Lcom/samsung/android/scloud/backup/data/BNRItem;F)V
    .locals 0

    .prologue
    .line 618
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->val$item:Lcom/samsung/android/scloud/backup/data/BNRItem;

    iput p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->val$fileCnt:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleResponse(Ljava/lang/String;JLjava/io/InputStream;)V
    .locals 8
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "size"    # J
    .param p4, "stream"    # Ljava/io/InputStream;

    .prologue
    const/4 v6, 0x4

    .line 622
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const-string v2, "BNRTask-"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile Finished from alternate path - key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->val$item:Lcom/samsung/android/scloud/backup/data/BNRItem;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 623
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    iget-object v0, v1, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    check-cast v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    .line 624
    .local v0, "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    new-instance v1, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;)V

    invoke-static {p4, p2, p3, p1, v1}, Lcom/samsung/android/scloud/backup/util/FileTool;->writeToFile(Ljava/io/InputStream;JLjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V

    .line 633
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    iget-object v1, v1, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const-string v2, "BNRTask-"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadFile fileWrite Finished from alternate path - key : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->val$item:Lcom/samsung/android/scloud/backup/data/BNRItem;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->this$0:Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    iget-object v4, v4, Lcom/samsung/android/scloud/backup/core/BNRTask$12;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$12$2;->val$item:Lcom/samsung/android/scloud/backup/data/BNRItem;

    invoke-interface {v0, v4, v5}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v6, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 634
    return-void
.end method
