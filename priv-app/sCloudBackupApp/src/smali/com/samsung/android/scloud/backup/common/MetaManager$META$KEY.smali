.class final Lcom/samsung/android/scloud/backup/common/MetaManager$META$KEY;
.super Ljava/lang/Object;
.source "MetaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/common/MetaManager$META;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "KEY"
.end annotation


# static fields
.field private static final AUTO_BACKUP:Ljava/lang/String; = "AutoBackup"

.field private static final AUTO_BACKUP_CHECKBOX:Ljava/lang/String; = "AutoBackupCheckBox"

.field private static final AUTO_BACKUP_SWITCH_CONTROL:Ljava/lang/String; = "AutoBackupSwitchControl"

.field private static final AUTO_RESTORE:Ljava/lang/String; = "AUTO_RESTORE"

.field private static final AutoBackupOnFromSetupWizard:Ljava/lang/String; = "AutoBackupOnFromSetupWizard"

.field private static final BACKUPLIST:Ljava/lang/String; = "BACKUP_LIST"

.field private static final BACKUPPROGRESS:Ljava/lang/String; = "BackupProgress"

.field private static final BATTERY_STATUS:Ljava/lang/String; = "BatteryStatus"

.field private static final COMING_FROM_SETUPWIZARD:Ljava/lang/String; = "COMING_FROM_SETUPWIZARD"

.field private static final DATA_3G_ASK:Ljava/lang/String; = "data3gAsk"

.field private static final DATA_3G_ASK_RESTORE:Ljava/lang/String; = "data3gAskRestore"

.field private static final FIRST_BACKUP:Ljava/lang/String; = "FIRST_BACKUP_"

.field private static final LAST_AUTOBACKUP_TIME:Ljava/lang/String; = "LastAutoBackupTime"

.field private static final LAST_BACKUP_TIME:Ljava/lang/String; = "LastBackupTime_"

.field private static final LAST_RESTORE_TIME:Ljava/lang/String; = "LastRestoreTime"

.field private static final LAST_STATUS_BACKUP:Ljava/lang/String; = "LAST_STATUS_BACKUP"

.field private static final LAST_STATUS_RESTORE:Ljava/lang/String; = "LAST_STATUS_RESTORE"

.field private static final OPERATION_FINISHED:Ljava/lang/String; = "OPERATION_FINISHED_"

.field private static final RESTORED:Ljava/lang/String; = "RestoredAlready"

.field private static final RESTORETIMELABEL:Ljava/lang/String; = "RestoreTimeLabel"

.field private static final RESTORE_PROGRESS:Ljava/lang/String; = "RestoreProgress"

.field private static final SELECTED_BACKUP:Ljava/lang/String; = "SELECTED_BACKUP"

.field private static final SELECTED_DEVICE:Ljava/lang/String; = "SELECTED_DEVICE"

.field private static final SELECTED_RESTORE:Ljava/lang/String; = "SELECTED_RESTORE"

.field private static final SHOW_URGENT:Ljava/lang/String; = "SHOW_URGENT"

.field private static final SWITCH_STATE:Ljava/lang/String; = "SWITCH_STATE"

.field private static final TRANSACTION_ID:Ljava/lang/String; = "ID"

.field private static final VERIFICATION_OK:Ljava/lang/String; = "verificationOk"

.field private static final WALLPAPER_FIRST_DEFAULT_CHANGE_CHECK:Ljava/lang/String; = "WALLPAPER_FIRST_DEFAULT_CHANGE_CHECK"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
