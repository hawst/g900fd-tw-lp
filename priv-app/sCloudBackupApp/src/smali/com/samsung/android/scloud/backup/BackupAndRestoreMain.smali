.class public Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;
.super Lcom/samsung/android/scloud/backup/core/BNRActivity;
.source "BackupAndRestoreMain.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# static fields
.field private static final RESULT_SETUP_WIZARD_RESTORE:I = 0x3ed

.field private static final SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN:I = 0x400

.field private static final SYSTEM_UI_FLAG_TRANSPARENT_BACKGROUND:I = 0x1000

.field private static final TAG:Ljava/lang/String; = "BackupAndRestoreMain"

.field private static final WIFI_SETUP_ACTIVITY:I = 0x3ec


# instance fields
.field private final MESSAGE_DELAYED_TIME:J

.field private final REMOTE_BACKUP_DETAIL_TIMEOUT:I

.field private bRestoreFailed:Z

.field private list:Lorg/json/JSONArray;

.field mAutoBackupCheckBox:Landroid/widget/CheckBox;

.field public mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

.field private mBackupDetailProgressDialogCancelled:Z

.field private mBackupDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field public mDialog:Landroid/app/Dialog;

.field private mHandler:Landroid/os/Handler;

.field mLearnMore:Landroid/widget/TextView;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field mRestoreCheckBox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;-><init>()V

    .line 75
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    .line 78
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    .line 79
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    .line 80
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    .line 81
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;

    .line 84
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 87
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->bRestoreFailed:Z

    .line 88
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z

    .line 89
    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mDialog:Landroid/app/Dialog;

    .line 90
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;

    .line 94
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->REMOTE_BACKUP_DETAIL_TIMEOUT:I

    .line 95
    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->MESSAGE_DELAYED_TIME:J

    .line 781
    new-instance v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$11;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private SetViewToactivity()V
    .locals 10

    .prologue
    const v9, 0x7f030005

    const v8, 0x7f030003

    const/high16 v7, 0x420c0000    # 35.0f

    const/high16 v6, 0x41b00000    # 22.0f

    const/4 v5, 0x1

    .line 129
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v3, v5, :cond_1

    .line 131
    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    .line 133
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eqz v3, :cond_0

    .line 134
    const v3, 0x7f09000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 135
    .local v1, "initialDescriptionTextview":Landroid/widget/TextView;
    const v3, 0x7f090010

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    .local v0, "backupRestoreWarningMessage":Landroid/widget/TextView;
    const v3, 0x7f090007

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 137
    .local v2, "titleText":Landroid/widget/TextView;
    invoke-virtual {v1, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 138
    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 139
    invoke-virtual {v2, v5, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 189
    .end local v0    # "backupRestoreWarningMessage":Landroid/widget/TextView;
    .end local v1    # "initialDescriptionTextview":Landroid/widget/TextView;
    .end local v2    # "titleText":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-le v3, v4, :cond_2

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 144
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto :goto_0

    .line 149
    :cond_2
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 151
    const v3, 0x7f030004

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto :goto_0

    .line 153
    :cond_3
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-ne v3, v5, :cond_4

    .line 155
    const v3, 0x7f030007

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto :goto_0

    .line 157
    :cond_4
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-eq v3, v5, :cond_5

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT01:Z

    if-eqz v3, :cond_6

    .line 159
    :cond_5
    const v3, 0x7f030009

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto :goto_0

    .line 161
    :cond_6
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v3, v5, :cond_7

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v3, v5, :cond_8

    .line 163
    :cond_7
    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    .line 165
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eqz v3, :cond_0

    .line 166
    const v3, 0x7f09000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 167
    .restart local v1    # "initialDescriptionTextview":Landroid/widget/TextView;
    const v3, 0x7f090010

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 168
    .restart local v0    # "backupRestoreWarningMessage":Landroid/widget/TextView;
    const v3, 0x7f090007

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 169
    .restart local v2    # "titleText":Landroid/widget/TextView;
    invoke-virtual {v1, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 170
    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 171
    invoke-virtual {v2, v5, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    goto/16 :goto_0

    .line 175
    .end local v0    # "backupRestoreWarningMessage":Landroid/widget/TextView;
    .end local v1    # "initialDescriptionTextview":Landroid/widget/TextView;
    .end local v2    # "titleText":Landroid/widget/TextView;
    :cond_8
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 177
    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto/16 :goto_0

    .line 179
    :cond_9
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 180
    const v3, 0x7f030008

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto/16 :goto_0

    .line 182
    :cond_a
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 184
    const v3, 0x7f030006

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto/16 :goto_0

    .line 186
    :cond_b
    const v3, 0x7f030002

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setContentView(I)V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)Lorg/json/JSONArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->removeRemoteTimeoutMessage(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startRestoreActivity()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->processBackupDetailRequestFail()V

    return-void
.end method

.method private backupDetailFailed()V
    .locals 2

    .prologue
    .line 711
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "[backupDetailFailed]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 714
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->createAlertDialog(I)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 716
    :cond_0
    return-void
.end method

.method private displayAlert()V
    .locals 14

    .prologue
    const v13, 0x7f070056

    const v12, 0x7f05000d

    const v11, 0x7f05000c

    .line 278
    const v8, 0x7f070096

    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 281
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f030014

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 282
    .local v6, "v":Landroid/view/View;
    const v8, 0x7f090024

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 283
    .local v5, "tvSms":Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070007

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    const v8, 0x7f090026

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 285
    .local v4, "tvMms":Landroid/widget/TextView;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070004

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x104000a

    invoke-virtual {p0, v9}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$2;

    invoke-direct {v10, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$2;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mDialog:Landroid/app/Dialog;

    .line 297
    const v8, 0x7f090020

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .local v0, "alert":Landroid/widget/TextView;
    const v8, 0x7f090032

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 298
    .local v1, "alertsync":Landroid/widget/TextView;
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v8

    if-nez v8, :cond_0

    sget-boolean v8, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_K:Z

    if-nez v8, :cond_0

    sget-boolean v8, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_H:Z

    if-eqz v8, :cond_1

    .line 300
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 301
    .local v7, "windowManager":Landroid/view/WindowManager$LayoutParams;
    const v8, 0x3f19999a    # 0.6f

    iput v8, v7, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 302
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getWindow()Landroid/view/Window;

    move-result-object v8

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/view/Window;->addFlags(I)V

    .line 303
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v0, v8, v11}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 304
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v1, v8, v11}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 311
    .end local v7    # "windowManager":Landroid/view/WindowManager$LayoutParams;
    :goto_0
    iget-object v8, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->show()V

    .line 313
    return-void

    .line 308
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v0, v8, v12}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v1, v8, v12}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private finishBackupAndRestore(I)V
    .locals 0
    .param p1, "endStatus"    # I

    .prologue
    .line 382
    invoke-virtual {p0, p1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setResult(I)V

    .line 383
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->finish()V

    .line 384
    return-void
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 405
    const v1, 0x7f09000c

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    .line 407
    const v1, 0x7f09000e

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    .line 410
    const v1, 0x7f09000b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    .line 411
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 412
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setClickable(Z)V

    .line 413
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mLearnMore:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v2

    or-int/lit8 v2, v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 414
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 417
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 424
    .local v0, "nextbuttonArrow":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 422
    .end local v0    # "nextbuttonArrow":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .restart local v0    # "nextbuttonArrow":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method private processBackupDetailRequestFail()V
    .locals 2

    .prologue
    .line 809
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "[processBackupDetailRequestFail]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->requestCancel()V

    .line 813
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->backupDetailFailed()V

    .line 814
    return-void
.end method

.method private processRestoreReady()V
    .locals 2

    .prologue
    .line 685
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "[processRestoreReady]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 687
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$10;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 708
    return-void
.end method

.method private removeRemoteTimeoutMessage(I)V
    .locals 3
    .param p1, "mMessgeType"    # I

    .prologue
    .line 804
    const-string v0, "BackupAndRestoreMain"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[2. removeRemoteTimeoutMessage]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 806
    return-void
.end method

.method private sendRemoteTimeoutMessage(I)V
    .locals 4
    .param p1, "mMessgeType"    # I

    .prologue
    .line 798
    const-string v0, "BackupAndRestoreMain"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[1. sendRemoteTimeoutMessage] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 801
    return-void
.end method

.method private setActivityTheme()V
    .locals 4

    .prologue
    const v3, 0x103012b

    const/4 v1, 0x1

    .line 317
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v0, v2, :cond_2

    .line 319
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setTheme(I)V

    .line 320
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setIndicatorTransparency()V

    .line 323
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-eqz v0, :cond_1

    .line 324
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->setTransGradationMode(Landroid/content/Context;Z)V

    .line 344
    :cond_1
    :goto_0
    return-void

    .line 326
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 328
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v1, :cond_3

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v1, :cond_5

    .line 329
    :cond_3
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setTheme(I)V

    .line 333
    :goto_1
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_LT03:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_GOYA:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_PICASSO:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MONDRIAN:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_V2:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MILLET_OR_DEGAS:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_MATISSE:Z

    if-eq v0, v1, :cond_4

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KONA:Z

    if-ne v0, v1, :cond_1

    .line 335
    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setIndicatorTransparency()V

    goto :goto_0

    .line 331
    :cond_5
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setTheme(I)V

    goto :goto_1

    .line 333
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 340
    :cond_7
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_H()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_KK()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setIndicatorTransparency()V

    goto :goto_0
.end method

.method private setIndicatorTransparency()V
    .locals 4

    .prologue
    .line 348
    const/16 v0, 0x1400

    .line 350
    .local v0, "visibility":I
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 352
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 353
    .local v1, "wmLp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, 0x4000c00

    or-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 355
    return-void
.end method

.method private showBackupDetailProgressDialog()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 459
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07003b

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 461
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07003e

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 463
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 465
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 466
    new-instance v0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$3;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    .line 474
    .local v0, "mCancelListener":Landroid/content/DialogInterface$OnClickListener;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, -0x2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 476
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$4;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 485
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 486
    return-void
.end method

.method private startRestoreActivity()V
    .locals 5

    .prologue
    .line 719
    const/16 v2, 0x3ed

    .line 720
    .local v2, "requestCode":I
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 722
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 723
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "backupDetailList"

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailsList:Ljava/util/List;

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 725
    const-string v3, "JSONArray"

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;

    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setBackupList(Lorg/json/JSONArray;)V

    .line 727
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 728
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startActivityForResult(Landroid/content/Intent;I)V

    .line 729
    return-void
.end method


# virtual methods
.method public createAlertDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "dialogType"    # I

    .prologue
    .line 489
    sparse-switch p1, :sswitch_data_0

    .line 545
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 491
    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07006b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070071

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070072

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$6;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$6;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070043

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$5;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 510
    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070064

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070033

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$8;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$7;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$7;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 531
    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07000f

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070091

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$9;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 489
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_1
        0xf -> :sswitch_0
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method public doNegativeClick(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 560
    const/16 v0, 0xf

    if-ne p1, v0, :cond_0

    .line 561
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 562
    :cond_0
    return-void
.end method

.method public doPositiveClick(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 549
    const/16 v0, 0xf

    if-ne p2, v0, :cond_1

    .line 550
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getDetail()V

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    const/4 v0, 0x7

    if-ne p2, v0, :cond_0

    .line 552
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->bRestoreFailed:Z

    if-eqz v0, :cond_2

    .line 553
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->bRestoreFailed:Z

    .line 555
    :cond_2
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->finishBackupAndRestore(I)V

    goto :goto_0
.end method

.method protected getClientDeviceId()Ljava/lang/String;
    .locals 5

    .prologue
    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, "clientDeviceId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 392
    .local v2, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v1

    .line 393
    .local v1, "phoneType":I
    if-nez v1, :cond_0

    .line 395
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 400
    :goto_0
    return-object v0

    .line 398
    :cond_0
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDetail()V
    .locals 3

    .prologue
    .line 440
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z

    .line 441
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityBackupDetail()V

    .line 442
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->showBackupDetailProgressDialog()V

    .line 443
    new-instance v0, Landroid/content/Intent;

    const-string v1, "REQUEST_GETDETAIL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 444
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "TRIGGER"

    const-string v2, "user"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 445
    const-class v1, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 446
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 447
    return-void
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 733
    const-string v0, "BackupAndRestoreMain"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 3

    .prologue
    .line 778
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x67

    aput v2, v0, v1

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 639
    packed-switch p1, :pswitch_data_0

    .line 682
    :cond_0
    :goto_0
    return-void

    .line 641
    :pswitch_0
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 642
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 644
    .local v2, "uWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 647
    .local v1, "uMobile":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v3

    if-nez v3, :cond_3

    .line 650
    :cond_2
    const/16 v3, 0x14

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->createAlertDialog(I)Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 654
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 655
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 656
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    const v4, 0x7f070079

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 664
    :cond_4
    const v3, 0x7f07009b

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 673
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    .end local v1    # "uMobile":Landroid/net/NetworkInfo;
    .end local v2    # "uWifi":Landroid/net/NetworkInfo;
    :pswitch_1
    const/4 v3, -0x1

    if-ne p2, v3, :cond_5

    .line 674
    invoke-direct {p0, p2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->finishBackupAndRestore(I)V

    goto :goto_0

    .line 675
    :cond_5
    if-nez p2, :cond_0

    .line 676
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 639
    :pswitch_data_0
    .packed-switch 0x3ec
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized onBackClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 631
    monitor-enter p0

    :try_start_0
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "[onBackClicked] RESULT_BACK... 0 returned. "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->finishBackupAndRestore(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634
    monitor-exit p0

    return-void

    .line 631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onBackupClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 565
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 570
    :goto_0
    monitor-exit p0

    return-void

    .line 568
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 565
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 111
    const/4 v0, 0x0

    .line 113
    .local v0, "isAutobackupChk":Z
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 114
    .local v1, "isRestoreChk":Z
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 117
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->SetViewToactivity()V

    .line 120
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->initViews()V

    .line 122
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 125
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 245
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->requestWindowFeature(I)Z

    .line 246
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->setActivityTheme()V

    .line 248
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onCreate(Landroid/os/Bundle;)V

    .line 251
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "[BackupAndRestoreMain][onCreate]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    .line 254
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 255
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityInSetupWizard()V

    .line 257
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->SetViewToactivity()V

    .line 259
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->initViews()V

    .line 261
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 262
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 264
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain$1;-><init>(Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 273
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    .line 275
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 378
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onDestroy()V

    .line 379
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 4
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 740
    const/16 v0, 0x67

    if-ne p1, v0, :cond_0

    .line 741
    const/16 v0, 0xcd

    if-ne v0, p2, :cond_0

    .line 742
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->removeRemoteTimeoutMessage(I)V

    .line 747
    const/16 v0, 0x12d

    if-ne p3, v0, :cond_2

    .line 748
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lorg/json/JSONArray;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->list:Lorg/json/JSONArray;

    .line 749
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->processRestoreReady()V

    .line 767
    :cond_0
    :goto_0
    const/16 v0, 0xc9

    if-ne v0, p2, :cond_1

    .line 768
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,msg"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", arg2 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , obj : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->sendRemoteTimeoutMessage(I)V

    .line 774
    :cond_1
    return-void

    .line 750
    :cond_2
    const/16 v0, 0x132

    if-ne p3, v0, :cond_3

    .line 751
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Request cancelled by the user"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 752
    :cond_3
    const/16 v0, 0x13c

    if-ne p3, v0, :cond_4

    .line 753
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Previous operation is in progress"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialogCancelled:Z

    .line 755
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 756
    const v0, 0x7f070089

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 762
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getTag()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Backup Detail Failed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->backupDetailFailed()V

    goto/16 :goto_0
.end method

.method public declared-synchronized onLearnMoreClicked(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 427
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->displayAlert()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    monitor-exit p0

    return-void

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 103
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "action":Ljava/lang/String;
    const-string v1, "BackupAndRestoreMain"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[BackupAndRestoreMain] onNewIntent is arrived. : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public onNextClicked(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 574
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 575
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 576
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v8}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 578
    .local v3, "uWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 580
    .local v2, "uMobile":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v5

    if-nez v5, :cond_2

    .line 583
    :cond_1
    const/16 v5, 0x14

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->createAlertDialog(I)Landroid/app/Dialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 628
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    .end local v2    # "uMobile":Landroid/net/NetworkInfo;
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :goto_0
    return-void

    .line 587
    .restart local v0    # "connManager":Landroid/net/ConnectivityManager;
    .restart local v2    # "uMobile":Landroid/net/NetworkInfo;
    .restart local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_2
    const-string v5, "wifi"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    .line 588
    .local v4, "wifiManager":Landroid/net/wifi/WifiManager;
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 589
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mContext:Landroid/content/Context;

    const v6, 0x7f070079

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 602
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 603
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "extra_prefs_show_button_bar"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 604
    const-string v5, "extra_prefs_set_back_text"

    const v6, 0x7f07005f

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    const-string v5, "wifi_enable_next_on_connect"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 607
    const-string v5, "show_dialog_once"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 608
    const/16 v5, 0x3ec

    invoke-virtual {p0, v1, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 597
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    const v5, 0x7f07009b

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 612
    .end local v0    # "connManager":Landroid/net/ConnectivityManager;
    .end local v2    # "uMobile":Landroid/net/NetworkInfo;
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    .end local v4    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mAutoBackupCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 613
    const/4 v5, 0x7

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->createAlertDialog(I)Landroid/app/Dialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 617
    :cond_5
    const-string v5, "BackupAndRestoreMain"

    const-string v6, "[onNextClicked] RESULT_OK... -1 returned. "

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 620
    iget-boolean v5, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->bRestoreFailed:Z

    if-eqz v5, :cond_6

    .line 621
    iput-boolean v7, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->bRestoreFailed:Z

    .line 623
    :cond_6
    const/4 v5, -0x1

    invoke-direct {p0, v5}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->finishBackupAndRestore(I)V

    goto/16 :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 193
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 197
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 195
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 366
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "onPause..."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onPause()V

    .line 368
    return-void
.end method

.method public declared-synchronized onRestoreClicked(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    :goto_0
    monitor-exit p0

    return-void

    .line 435
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->mRestoreCheckBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 360
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "onResume..."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onResume()V

    .line 362
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 372
    const-string v0, "BackupAndRestoreMain"

    const-string v1, "onStop..."

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRActivity;->onStop()V

    .line 374
    return-void
.end method

.method public requestCancel()V
    .locals 4

    .prologue
    .line 450
    new-instance v1, Landroid/content/Intent;

    const-string v2, "OPERATION_CANCEL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 451
    .local v1, "service":Landroid/content/Intent;
    const-class v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 452
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    .line 453
    .local v0, "mSourceArr":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "REQUEST_GETDETAIL"

    aput-object v3, v0, v2

    .line 454
    const-string v2, "SOURCE_LIST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/BackupAndRestoreMain;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 456
    return-void
.end method
