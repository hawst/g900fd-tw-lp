.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$PendingMMS;
.super Ljava/lang/Object;
.source "MmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PendingMMS"
.end annotation


# static fields
.field public static final DUE_TIME:Ljava/lang/String; = "due_time"

.field public static final ERR_CODE:Ljava/lang/String; = "err_code"

.field public static final ERR_TYPE:Ljava/lang/String; = "err_type"

.field public static final LAST_TRY:Ljava/lang/String; = "last_try"

.field public static final MSG_ID:Ljava/lang/String; = "msg_id"

.field public static final MSG_TYPE:Ljava/lang/String; = "msg_type"

.field public static final PROTO_TPYE:Ljava/lang/String; = "proto_type"

.field public static final RETRY_INDEX:Ljava/lang/String; = "retry_index"

.field public static final _ID:Ljava/lang/String; = "_id"
