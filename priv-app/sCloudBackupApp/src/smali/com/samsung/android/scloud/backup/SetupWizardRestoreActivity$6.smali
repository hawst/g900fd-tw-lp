.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;
.super Ljava/lang/Object;
.source "SetupWizardRestoreActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showSelectDeviceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0

    .prologue
    .line 542
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    .line 548
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;

    .line 549
    .local v2, "viewholder":Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;
    iget-object v1, v2, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->rdbtn:Landroid/widget/RadioButton;

    .line 550
    .local v1, "rdbtn":Landroid/widget/RadioButton;
    invoke-virtual {v1}, Landroid/widget/RadioButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 552
    .local v0, "device":Lcom/samsung/android/scloud/backup/BackupDetails;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v4

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 553
    invoke-virtual {v0, v5}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 555
    invoke-virtual {v1, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 557
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->setfoucsPosition(I)V

    .line 558
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$6;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->notifyDataSetChanged()V

    .line 560
    return-void
.end method
