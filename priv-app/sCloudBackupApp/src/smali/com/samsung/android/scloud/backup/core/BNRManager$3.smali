.class Lcom/samsung/android/scloud/backup/core/BNRManager$3;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperationOnRestore(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

.field final synthetic val$cnt:I

.field final synthetic val$srcDevice:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iput p2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->val$cnt:I

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->val$srcDevice:Ljava/lang/String;

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 6
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    .line 195
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "restoreReady Finished - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 197
    if-nez p1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    const/16 v2, 0x66

    const/16 v3, 0xc9

    const/16 v4, 0x12d

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->sendMessageToActivity(III)V

    .line 200
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$400(Lcom/samsung/android/scloud/backup/core/BNRManager;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;
    invoke-static {v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$200(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->preOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 202
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "preOperationOnRestore finished.. "

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRManager;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 227
    :goto_0
    return-void

    .line 206
    :cond_0
    const/16 v1, 0x4e26

    if-ne p1, v1, :cond_2

    .line 207
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "waiting server - restoreReady."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_1
    :goto_1
    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->val$cnt:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_4

    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restoreReady sleep for retry - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mCtid:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$100(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->val$cnt:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->val$srcDevice:Ljava/lang/String;

    # invokes: Lcom/samsung/android/scloud/backup/core/BNRManager;->preOperationOnRestore(ILjava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$700(Lcom/samsung/android/scloud/backup/core/BNRManager;ILjava/lang/String;)V

    goto :goto_0

    .line 209
    :cond_2
    const/16 v1, 0x4e27

    if-ne p1, v1, :cond_3

    .line 210
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "waiting server - migration."

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212
    :cond_3
    if-eqz p1, :cond_1

    goto :goto_1

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$3;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mTAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$000(Lcom/samsung/android/scloud/backup/core/BNRManager;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "restoreReady retry sleep err"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 225
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_4
    new-instance v1, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v2, 0x136

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/String;)V

    throw v1
.end method
