.class public Lcom/samsung/android/scloud/backup/core/BNRTask;
.super Ljava/lang/Object;
.source "BNRTask.java"


# static fields
.field private static final MAX_COUNT:I = 0x3e8

.field private static final MAX_DOWNLOAD:J = 0x100000L

.field private static final MAX_KEY_GET_COUNT:I = 0x1f4

.field private static final MAX_UPLOAD:J = 0x4ffff6L

.field private static final TAG:Ljava/lang/String; = "BNRTask-"

.field private static final THRESHOLD_LIMIT:J = 0xaL

.field private static final mBackupDetailsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/scloud/backup/showbackup/BackupDetailsList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/core/BNRTask;->mBackupDetailsList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 57
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p2, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;

    .prologue
    .line 64
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : backup start !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v6, 0x1

    new-array v10, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, ""

    aput-object v7, v10, v6

    .line 69
    .local v10, "startKey":[Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .local v11, "serverKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isFirstBackup(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 73
    :cond_0
    const/4 v6, 0x0

    aget-object v16, v10, v6

    const/16 v17, 0x1f4

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    new-instance v6, Lcom/samsung/android/scloud/backup/core/BNRTask$1;

    move-object/from16 v7, p4

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    invoke-direct/range {v6 .. v11}, Lcom/samsung/android/scloud/backup/core/BNRTask$1;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;)V

    move-object/from16 v12, p0

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p1

    move-object/from16 v19, v6

    invoke-static/range {v12 .. v19}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->restoreKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 103
    const/4 v6, 0x0

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : get server keys - count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 104
    const/4 v6, 0x0

    aget-object v6, v10, v6

    if-eqz v6, :cond_1

    const-string v6, ""

    const/4 v7, 0x0

    aget-object v7, v10, v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 110
    :cond_1
    :goto_0
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : get server keys ends !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    invoke-interface {v6, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->getTotalKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)Ljava/util/List;

    move-result-object v48

    .line 114
    .local v48, "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v17, "toUploadList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v54, Ljava/util/ArrayList;

    invoke-direct/range {v54 .. v54}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v54, "toDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v48 .. v48}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v41

    .local v41, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 119
    .local v47, "local":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 106
    .end local v17    # "toUploadList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v41    # "i$":Ljava/util/Iterator;
    .end local v47    # "local":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v48    # "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .end local v54    # "toDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    const/4 v6, 0x0

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : First backup - do not call restoreKeys"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    .restart local v17    # "toUploadList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v41    # "i$":Ljava/util/Iterator;
    .restart local v48    # "localKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .restart local v54    # "toDeleteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v41

    .end local v41    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 123
    .local v50, "server":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const/16 v43, 0x0

    .line 124
    .local v43, "isInLocal":Z
    invoke-interface/range {v48 .. v48}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v42

    .local v42, "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v47

    check-cast v47, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 125
    .restart local v47    # "local":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 128
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getTimeStamp()J

    move-result-wide v6

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getTimeStamp()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_6

    .line 129
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 130
    :cond_6
    const/16 v43, 0x1

    .line 134
    .end local v47    # "local":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_7
    if-nez v43, :cond_4

    .line 135
    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v54

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 138
    .end local v42    # "i$":Ljava/util/Iterator;
    .end local v43    # "isInLocal":Z
    .end local v50    # "server":Lcom/samsung/android/scloud/backup/data/BNRItem;
    :cond_8
    const/4 v6, 0x0

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : Compare end - local : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v48 .. v48}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", server : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", toUpload : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", toDelete : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v6

    int-to-float v6, v6

    const v7, 0x3fa66666    # 1.3f

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v56, v6, v7

    .line 142
    .local v56, "totalProc":F
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v6, :cond_9

    .line 143
    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v6, v6

    add-float v56, v56, v6

    .line 145
    :cond_9
    const/4 v6, 0x0

    cmpg-float v6, v56, v6

    if-gtz v6, :cond_f

    .line 146
    const/4 v6, 0x0

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : there is no items to backup !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 155
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_18

    .line 156
    const/16 v18, 0x0

    .line 158
    .local v18, "idx":I
    new-instance v51, Ljava/util/HashMap;

    invoke-direct/range {v51 .. v51}, Ljava/util/HashMap;-><init>()V

    .line 159
    .local v51, "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v49, Ljava/util/HashMap;

    invoke-direct/range {v49 .. v49}, Ljava/util/HashMap;-><init>()V

    .line 161
    .local v49, "nextServerFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v6, :cond_a

    .line 190
    :cond_a
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v22, "toUploadItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 192
    .local v23, "tmpFile":Ljava/lang/String;
    :goto_4
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v13

    const/16 v19, 0x3e8

    const-wide/32 v20, 0x4ffff6

    move-object/from16 v14, p0

    move-object/from16 v15, p4

    move-object/from16 v16, p1

    invoke-interface/range {v13 .. v23}, Lcom/samsung/android/scloud/backup/core/IOEMControl;->getDataFromOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;IIJLjava/util/List;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 194
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v6

    add-int v18, v18, v6

    .line 196
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x65

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v8

    int-to-float v8, v8

    const v9, 0x3e99999a    # 0.3f

    mul-float/2addr v8, v9

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 198
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v41

    :cond_b
    :goto_5
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_15

    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 199
    .local v44, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v6, :cond_b

    move-object/from16 v40, p1

    .line 200
    check-cast v40, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    .line 201
    .local v40, "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getAttachmentFileInfo()Ljava/util/Map;

    move-result-object v45

    .line 203
    .local v45, "itemFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v51

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Ljava/util/Map;

    .line 205
    .local v52, "serverFileMapByKey":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v55, Ljava/util/ArrayList;

    invoke-direct/range {v55 .. v55}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v55, "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v53, Ljava/util/ArrayList;

    invoke-direct/range {v53 .. v53}, Ljava/util/ArrayList;-><init>()V

    .line 207
    .local v53, "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v52, :cond_c

    invoke-interface/range {v52 .. v52}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_c

    .line 208
    invoke-interface/range {v52 .. v52}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    move-object/from16 v0, v53

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 210
    :cond_c
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    .line 211
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v49

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    :cond_d
    if-eqz v45, :cond_11

    .line 214
    invoke-interface/range {v45 .. v45}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v42

    .restart local v42    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 215
    .local v27, "file":Ljava/lang/String;
    move-object/from16 v0, v53

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v49

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    move-object/from16 v0, v45

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object/from16 v0, v27

    invoke-interface {v6, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    if-eqz v52, :cond_e

    move-object/from16 v0, v52

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    move-object/from16 v0, v52

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object/from16 v0, v45

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 220
    :cond_e
    move-object/from16 v0, v55

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 151
    .end local v18    # "idx":I
    .end local v22    # "toUploadItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .end local v23    # "tmpFile":Ljava/lang/String;
    .end local v27    # "file":Ljava/lang/String;
    .end local v40    # "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    .end local v42    # "i$":Ljava/util/Iterator;
    .end local v44    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v45    # "itemFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v49    # "nextServerFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v51    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v52    # "serverFileMapByKey":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v53    # "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v55    # "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_f
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : Total Count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v56

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v6

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    move/from16 v0, v56

    invoke-virtual {v6, v7, v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTotalValue(Ljava/lang/String;F)V

    goto/16 :goto_3

    .line 222
    .restart local v18    # "idx":I
    .restart local v22    # "toUploadItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .restart local v23    # "tmpFile":Ljava/lang/String;
    .restart local v27    # "file":Ljava/lang/String;
    .restart local v40    # "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    .restart local v42    # "i$":Ljava/util/Iterator;
    .restart local v44    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .restart local v45    # "itemFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v49    # "nextServerFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v51    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v52    # "serverFileMapByKey":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v53    # "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v55    # "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_10
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "This file already be in server : - key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", file : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 228
    .end local v27    # "file":Ljava/lang/String;
    .end local v42    # "i$":Ljava/util/Iterator;
    :cond_11
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->size()I

    move-result v6

    invoke-interface/range {v53 .. v53}, Ljava/util/List;->size()I

    move-result v7

    add-int/2addr v6, v7

    int-to-float v0, v6

    move/from16 v39, v0

    .line 229
    .local v39, "fileCnt":F
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : upload - key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", toUploadFileList : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v55 .. v55}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", toDeleteFileList : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v53 .. v53}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uploadFile start - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-interface/range {v55 .. v55}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v42

    .restart local v42    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_12

    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    .line 234
    .local v38, "file":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    move-object/from16 v2, v44

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v40

    move-object/from16 v1, v44

    invoke-interface {v0, v7, v1}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    const/16 v29, 0x0

    const/16 v30, 0x0

    new-instance v32, Lcom/samsung/android/scloud/backup/core/BNRTask$2;

    move-object/from16 v0, v32

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    move/from16 v3, v39

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRTask$2;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;F)V

    new-instance v33, Lcom/samsung/android/scloud/backup/core/BNRTask$3;

    move-object/from16 v0, v33

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    move-object/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/core/BNRTask$3;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v24, p0

    move-object/from16 v25, p2

    move-object/from16 v26, p1

    move-object/from16 v31, p3

    invoke-static/range {v24 .. v33}, Lcom/samsung/android/scloud/backup/server/ORSServiceManager;->uploadFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_7

    .line 253
    .end local v38    # "file":Ljava/lang/String;
    :cond_12
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "deleteFile start - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-interface/range {v53 .. v53}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v42

    :goto_8
    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_13

    invoke-interface/range {v42 .. v42}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 255
    .restart local v27    # "file":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v40

    move-object/from16 v1, v44

    invoke-interface {v0, v7, v1}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x0

    new-instance v24, Lcom/samsung/android/scloud/backup/core/BNRTask$4;

    move-object/from16 v25, p4

    move-object/from16 v26, p3

    move-object/from16 v28, p1

    move/from16 v29, v39

    invoke-direct/range {v24 .. v29}, Lcom/samsung/android/scloud/backup/core/BNRTask$4;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;F)V

    move-object/from16 v28, p0

    move-object/from16 v29, p2

    move-object/from16 v30, p1

    move-object/from16 v33, p3

    move-object/from16 v34, v24

    invoke-static/range {v28 .. v34}, Lcom/samsung/android/scloud/backup/server/ORSServiceManager;->deleteFile(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_8

    .line 266
    .end local v27    # "file":Ljava/lang/String;
    :cond_13
    const/4 v6, 0x0

    cmpl-float v6, v39, v6

    if-nez v6, :cond_14

    .line 267
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uploadFile : no file to upload or delete - key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x65

    const/high16 v8, 0x3f800000    # 1.0f

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 272
    :cond_14
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "uploadFile Finished All - key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 278
    .end local v39    # "fileCnt":F
    .end local v40    # "fileModel":Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
    .end local v42    # "i$":Ljava/util/Iterator;
    .end local v44    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v45    # "itemFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v52    # "serverFileMapByKey":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v53    # "toDeleteFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v55    # "toUploadFileList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_15
    const/4 v6, 0x0

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : BNR set - count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v33, Lcom/samsung/android/scloud/backup/core/BNRTask$5;

    move-object/from16 v0, v33

    move-object/from16 v1, v22

    move-object/from16 v2, p4

    move-object/from16 v3, p3

    move-object/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRTask$5;-><init>(Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;)V

    new-instance v34, Lcom/samsung/android/scloud/backup/core/BNRTask$6;

    move-object/from16 v0, v34

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    move-object/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRTask$6;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;)V

    move-object/from16 v28, p0

    move-object/from16 v29, p2

    move-object/from16 v30, p3

    move-object/from16 v31, p1

    move-object/from16 v32, v23

    invoke-static/range {v28 .. v34}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupSet(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 298
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->clear()V

    goto/16 :goto_4

    .line 303
    :cond_16
    move-object/from16 v0, p1

    instance-of v6, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v6, :cond_17

    .line 342
    :cond_17
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : backup set - end !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 348
    .end local v18    # "idx":I
    .end local v22    # "toUploadItems":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .end local v23    # "tmpFile":Ljava/lang/String;
    .end local v49    # "nextServerFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v51    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_18
    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1c

    .line 349
    invoke-interface/range {v54 .. v54}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v41

    .restart local v41    # "i$":Ljava/util/Iterator;
    :cond_19
    :goto_9
    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1b

    invoke-interface/range {v41 .. v41}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Ljava/lang/String;

    .line 350
    .local v46, "key":Ljava/lang/String;
    invoke-static/range {v46 .. v46}, Lcom/samsung/android/scloud/backup/data/BNRItem;->parseToBNRItem(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-result-object v44

    .line 351
    .restart local v44    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->hasFile()Z

    move-result v6

    if-eqz v6, :cond_19

    .line 352
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : delete server File - key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 354
    :try_start_0
    move-object/from16 v0, p1

    check-cast v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    move-object v6, v0

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v44

    invoke-interface {v6, v7, v0}, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;->getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x0

    new-instance v34, Lcom/samsung/android/scloud/backup/core/BNRTask$7;

    move-object/from16 v0, v34

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    move-object/from16 v3, v46

    move-object/from16 v4, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRTask$7;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;)V

    move-object/from16 v28, p0

    move-object/from16 v29, p2

    move-object/from16 v30, p1

    move-object/from16 v33, p3

    invoke-static/range {v28 .. v34}, Lcom/samsung/android/scloud/backup/server/ORSServiceManager;->deleteFolder(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    .line 363
    :catch_0
    move-exception v37

    .line 364
    .local v37, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    invoke-virtual/range {v37 .. v37}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\"rcode\":31002"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 365
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : Already deleted folder. key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v44 .. v44}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 367
    :cond_1a
    throw v37

    .line 374
    .end local v37    # "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    .end local v44    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v46    # "key":Ljava/lang/String;
    :cond_1b
    const/16 v33, 0x3e8

    const-wide/32 v34, 0x4ffff6

    new-instance v36, Lcom/samsung/android/scloud/backup/core/BNRTask$8;

    move-object/from16 v0, v36

    move-object/from16 v1, p4

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BNRTask$8;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;)V

    move-object/from16 v28, p0

    move-object/from16 v29, p2

    move-object/from16 v30, p3

    move-object/from16 v31, p1

    move-object/from16 v32, v54

    invoke-static/range {v28 .. v36}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupDelete(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;IJLcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 380
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x65

    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v8

    int-to-float v8, v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 383
    .end local v41    # "i$":Ljava/util/Iterator;
    :cond_1c
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : backup delete end !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 385
    new-instance v6, Lcom/samsung/android/scloud/backup/core/BNRTask$9;

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-direct {v6, v0, v1}, Lcom/samsung/android/scloud/backup/core/BNRTask$9;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p1

    invoke-static {v0, v1, v2, v3, v6}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->backupDetailSet(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 392
    const/4 v6, 0x4

    const-string v7, "BNRTask-"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") : backup detail set end !!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x65

    const/16 v8, 0x12d

    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v6, v7, v8, v9}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onFinished(Ljava/lang/String;IILjava/lang/Object;)V

    .line 396
    return-void
.end method

.method public static getAllBackupUsage(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p2, "ctid"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 406
    .local p3, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x4

    const-string v1, "BNRTask-"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GET USAGE BY USER]("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : GET USAGE BY USER !!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p4, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 408
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRTask$10;

    invoke-direct {v0, p4, p2}, Lcom/samsung/android/scloud/backup/core/BNRTask$10;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->getTotalUsageByUser(Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 431
    return-void
.end method

.method public static getBackedupHistory(ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 6
    .param p0, "cnt"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;

    .prologue
    .line 671
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRTask$13;

    move-object v1, p4

    move-object v2, p3

    move v3, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/core/BNRTask$13;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;ILandroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;)V

    invoke-static {p1, p2, p3, v0}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->getDetails(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 698
    return-void
.end method

.method public static restore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p2, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "srcDevice"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 438
    .local p5, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore start !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 440
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : get server data !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 442
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v6, v2

    .line 443
    .local v6, "startKey":[Ljava/lang/String;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 446
    .local v7, "serverKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    :cond_0
    const/4 v2, 0x0

    aget-object v12, v6, v2

    const/16 v13, 0x1f4

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRTask$11;

    move-object/from16 v3, p6

    move-object/from16 v4, p1

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/scloud/backup/core/BNRTask$11;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;)V

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p1

    move-object/from16 v14, p4

    move-object v15, v2

    invoke-static/range {v8 .. v15}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->restoreKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;ILjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 473
    const/4 v2, 0x0

    aget-object v2, v6, v2

    if-eqz v2, :cond_1

    const-string v2, ""

    const/4 v3, 0x0

    aget-object v3, v6, v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 476
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v20, v0

    .line 478
    .local v20, "keySize":J
    const-wide/16 v2, 0x2

    mul-long v4, v2, v20

    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v2, :cond_5

    move-wide/from16 v2, v20

    :goto_0
    add-long/2addr v2, v4

    long-to-float v2, v2

    move-wide/from16 v0, v20

    long-to-float v3, v0

    const/high16 v4, 0x41100000    # 9.0f

    div-float/2addr v3, v4

    add-float v19, v2, v3

    .line 479
    .local v19, "totalProc":F
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTotalValue(Ljava/lang/String;F)V

    .line 482
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore items !! Count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 485
    const-wide/16 v22, 0x0

    .line 486
    .local v22, "size":J
    const/4 v2, 0x0

    cmpl-float v2, v19, v2

    if-lez v2, :cond_8

    .line 487
    new-instance v15, Ljava/util/HashMap;

    invoke-direct {v15}, Ljava/util/HashMap;-><init>()V

    .line 489
    .local v15, "toDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 491
    .local v16, "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;

    if-eqz v2, :cond_2

    .line 520
    :cond_2
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore items start !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 523
    .local v18, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v2

    const/16 v3, 0x1f4

    if-ge v2, v3, :cond_3

    const-wide/32 v2, 0x100000

    cmp-long v2, v22, v2

    if-ltz v2, :cond_4

    .line 524
    :cond_3
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore items1 - size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cnt :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    .line 525
    invoke-static/range {v8 .. v16}, Lcom/samsung/android/scloud/backup/core/BNRTask;->restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/Map;Ljava/util/Map;)V

    .line 526
    const-wide/16 v22, 0x0

    .line 527
    invoke-interface {v15}, Ljava/util/Map;->clear()V

    .line 529
    :cond_4
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getKey()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-interface {v15, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getSize()J

    move-result-wide v2

    add-long v22, v22, v2

    .line 532
    goto/16 :goto_1

    .line 478
    .end local v15    # "toDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .end local v16    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v19    # "totalProc":F
    .end local v22    # "size":J
    :cond_5
    const-wide/16 v2, 0x0

    goto/16 :goto_0

    .line 534
    .restart local v15    # "toDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .restart local v16    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v19    # "totalProc":F
    .restart local v22    # "size":J
    :cond_6
    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 535
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore items2 - size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cnt :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    move-object/from16 v14, p6

    .line 536
    invoke-static/range {v8 .. v16}, Lcom/samsung/android/scloud/backup/core/BNRTask;->restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/Map;Ljava/util/Map;)V

    .line 539
    :cond_7
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore items finished !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 547
    .end local v15    # "toDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .end local v16    # "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v17    # "i$":Ljava/util/Iterator;
    :goto_2
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : restore ends !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x66

    const/16 v4, 0x12d

    move-object/from16 v0, p6

    move-object/from16 v1, p4

    invoke-interface {v0, v2, v3, v4, v1}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onFinished(Ljava/lang/String;IILjava/lang/Object;)V

    .line 552
    return-void

    .line 543
    :cond_8
    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") : there is no backed up data !!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p6

    invoke-interface {v0, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/Map;Ljava/util/Map;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p2, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p3, "ctid"    # Ljava/lang/String;
    .param p4, "srcDevice"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 561
    .local p5, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p7, "toDownloadList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p8, "serverFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRTask$12;

    move-object v1, p6

    move-object v2, p3

    move-object v3, p1

    move-object/from16 v4, p8

    move-object v5, p0

    move-object v6, p2

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/scloud/backup/core/BNRTask$12;-><init>(Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/Map;Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/util/List;)V

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    move-object/from16 v6, p7

    move-object v7, v0

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->restoreItems(Landroid/content/Context;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/lang/String;Ljava/util/Map;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    .line 666
    return-void
.end method
