.class final Lcom/samsung/android/scloud/backup/core/BNRTask$5;
.super Ljava/lang/Object;
.source "BNRTask.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctid:Ljava/lang/String;

.field final synthetic val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field final synthetic val$model:Lcom/samsung/android/scloud/backup/model/IModel;

.field final synthetic val$toUploadItems:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/lang/String;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$toUploadItems:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$ctid:Ljava/lang/String;

    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferred(JJJ)V
    .locals 7
    .param p1, "now"    # J
    .param p3, "transferred"    # J
    .param p5, "total"    # J

    .prologue
    .line 284
    long-to-float v1, p1

    long-to-float v2, p5

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$toUploadItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const v2, 0x3f4ccccd    # 0.8f

    mul-float v0, v1, v2

    .line 285
    .local v0, "per":F
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    const/4 v2, 0x4

    const-string v3, "BNRTask-"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backupSet transferred - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$ctid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$listener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRTask$5;->val$model:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x65

    invoke-interface {v1, v2, v3, v0}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 288
    return-void
.end method
