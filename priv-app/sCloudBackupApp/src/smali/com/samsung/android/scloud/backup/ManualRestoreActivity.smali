.class public Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
.super Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;
.source "ManualRestoreActivity.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/ManualRestoreActivity$CheckSamsungAccountValidation;,
        Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;
    }
.end annotation


# static fields
.field private static final ACTION_SAMSUNG_ACCOUNT_VALIDATION:Ljava/lang/String; = "com.msc.action.VALIDATION_CHECK_RESPONSE"

.field private static final MENU_RESTORE_ID:I = 0x2

.field private static final SA_REQUEST_ID_VALIDATION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ManualRestoreActivity"

.field static cancelstatus:Z

.field static first:Z

.field private static mMenu:Landroid/view/Menu;

.field private static mProgressDialog:Landroid/app/ProgressDialog;


# instance fields
.field private final MESSAGE_DELAYED_TIME:J

.field private final REMOTE_BACKUP_DETAIL_NODEVICE:I

.field private final REMOTE_BACKUP_DETAIL_TIMEOUT:I

.field public alertDialog:Landroid/app/AlertDialog;

.field public alertdialog:Landroid/app/Dialog;

.field private bRestoreFailed:Z

.field bSelected:Z

.field public buttonrestore:Landroid/view/MenuItem;

.field checkBox:Landroid/widget/CheckBox;

.field dialog:Landroid/app/Dialog;

.field private isSupportAidlOnly:Z

.field private list:Lorg/json/JSONArray;

.field private mActivity:Landroid/app/Activity;

.field private mAd:Landroid/app/AlertDialog;

.field private mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

.field private mBackedupDeviceListPreference:Landroid/preference/Preference;

.field public mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

.field private mBackupDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mBackupDetailsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mBlacklist:Landroid/preference/CheckBoxPreference;

.field private mCallReject:Landroid/preference/CheckBoxPreference;

.field private mContext:Landroid/content/Context;

.field public mDialog:Landroid/app/Dialog;

.field private mEmail:Landroid/preference/CheckBoxPreference;

.field private mHandler:Landroid/os/Handler;

.field private mIntentToStartActivity:Landroid/content/Intent;

.field private mIsBackupDetailFailed:Z

.field private mIsDialogActiveFlag:Z

.field private mLog:Landroid/preference/CheckBoxPreference;

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private mMms:Landroid/preference/CheckBoxPreference;

.field private mRestoreDescriptionPreference:Landroid/preference/Preference;

.field private mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

.field private mSelectedDeviceChange:Ljava/lang/Boolean;

.field private mSelectedDeviceIndex:I

.field private mSms:Landroid/preference/CheckBoxPreference;

.field private mSourceKeyPreference:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/preference/CheckBoxPreference;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDeviceId:Ljava/lang/String;

.field private mValidSourceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mValidationStatus:I

.field private mWallpaper:Landroid/preference/CheckBoxPreference;

.field private mviplist:Landroid/preference/CheckBoxPreference;

.field newActivity:Z

.field noneSelected:Z

.field private restorebutton:Landroid/widget/TextView;

.field private restorebuttonlayout:Landroid/widget/LinearLayout;

.field sameDeviceBackupExistsFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 122
    sput-boolean v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 123
    sput-boolean v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 135
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;-><init>()V

    .line 99
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    .line 100
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 103
    iput v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    .line 105
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 106
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 107
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 108
    iput v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->REMOTE_BACKUP_DETAIL_TIMEOUT:I

    .line 109
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->REMOTE_BACKUP_DETAIL_NODEVICE:I

    .line 110
    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->MESSAGE_DELAYED_TIME:J

    .line 111
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->bRestoreFailed:Z

    .line 114
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->bSelected:Z

    .line 115
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->newActivity:Z

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    .line 118
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    .line 119
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->dialog:Landroid/app/Dialog;

    .line 120
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    .line 121
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsBackupDetailFailed:Z

    .line 124
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    .line 125
    iput-boolean v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    .line 126
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->sameDeviceBackupExistsFlag:Z

    .line 127
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertdialog:Landroid/app/Dialog;

    .line 128
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->checkBox:Landroid/widget/CheckBox;

    .line 129
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertDialog:Landroid/app/AlertDialog;

    .line 132
    iput v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidationStatus:I

    .line 134
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIntentToStartActivity:Landroid/content/Intent;

    .line 136
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mActivity:Landroid/app/Activity;

    .line 138
    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 139
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->isSupportAidlOnly:Z

    .line 140
    iput-boolean v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsDialogActiveFlag:Z

    .line 1373
    new-instance v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$23;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;

    .line 1695
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lorg/json/JSONArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsBackupDetailFailed:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayInitialRestoring()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->processBackupDetailRequestFail()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonStatus(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2500()Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$2502(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Landroid/app/ProgressDialog;

    .prologue
    .line 85
    sput-object p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method static synthetic access$2600(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->isSupportAidlOnly:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->isSupportAidlOnly:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->registerSamsungAccountReceiver()V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidationStatus:I

    return v0
.end method

.method static synthetic access$2802(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidationStatus:I

    return p1
.end method

.method static synthetic access$2900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->unregisterSamsungAccountReceiver()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->removeRemoteTimeoutMessage(I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIntentToStartActivity:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIntentToStartActivity:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsDialogActiveFlag:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsDialogActiveFlag:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # I

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finishThisActivity(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setSourceList()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/ManualRestoreActivity;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    return-object p1
.end method

.method private backupDetailFailed()V
    .locals 2

    .prologue
    .line 1091
    const-string v0, "ManualRestoreActivity"

    const-string v1, "[backupDetailFailed]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1093
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1094
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    .line 1096
    :cond_0
    return-void
.end method

.method private buttonStatus(Z)V
    .locals 3
    .param p1, "status"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 737
    if-eqz p1, :cond_1

    .line 738
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 739
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setSourceList()V

    .line 743
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 746
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method private disablePreference()V
    .locals 4

    .prologue
    .line 788
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 789
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 790
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private displayCancelling()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1218
    iput-boolean v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->bSelected:Z

    .line 1219
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1220
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1221
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const v3, 0x7f07002e

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1222
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 1225
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v2, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1226
    return-void
.end method

.method private displayInitialRestoring()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 793
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 794
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    const v3, 0x7f07006f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 795
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 796
    return-void
.end method

.method private displayRestoring()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 776
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 777
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_0

    .line 778
    const/4 v2, 0x0

    .line 779
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_1

    .line 780
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v2

    .line 781
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    const v4, 0x7f07006f

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 784
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "progress":I
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v3, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 785
    return-void
.end method

.method private enablePreference()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 799
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 800
    .local v3, "key":Ljava/lang/String;
    const-string v4, "VIPLIST"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "BLACKLIST"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 801
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    .line 802
    .local v1, "emailPreference":Landroid/preference/CheckBoxPreference;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 803
    invoke-virtual {v1, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 804
    invoke-virtual {v1, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 806
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 807
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-nez v0, :cond_2

    .line 808
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 811
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v1, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 810
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1

    .line 814
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v1    # "emailPreference":Landroid/preference/CheckBoxPreference;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 815
    .restart local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-nez v0, :cond_4

    .line 816
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 818
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 822
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v3    # "key":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private finishThisActivity(I)V
    .locals 0
    .param p1, "endStatus"    # I

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    .line 652
    return-void
.end method

.method private initPref()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 753
    const-string v4, "screen"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceScreen;

    .line 754
    .local v2, "screen":Landroid/preference/PreferenceScreen;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 755
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 756
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 757
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 758
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 759
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 760
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 761
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 764
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 766
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    .line 767
    .local v3, "sourcePreference":Landroid/preference/CheckBoxPreference;
    invoke-virtual {v2, v3}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 768
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v1, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 769
    const-string v4, "VIPLIST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "BLACKLIST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 770
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 771
    invoke-virtual {v3, v5}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 773
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "sourcePreference":Landroid/preference/CheckBoxPreference;
    :cond_2
    return-void
.end method

.method private initView()V
    .locals 6

    .prologue
    .line 655
    const-string v1, "ManualRestoreActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[initView] : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 659
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setLastBackupTime()V

    .line 660
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    .line 661
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->updatePref()V

    .line 664
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 665
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 667
    :cond_0
    return-void

    .line 655
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private initViewOnProgress()V
    .locals 6

    .prologue
    .line 670
    const-string v1, "ManualRestoreActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[initView] : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " Count : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 673
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    .line 674
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->updatePref()V

    .line 677
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 678
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 680
    :cond_0
    return-void

    .line 670
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private networkisAvailable(Z)Z
    .locals 8
    .param p1, "bToast"    # Z

    .prologue
    const/4 v7, 0x0

    .line 1526
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v4

    .line 1527
    .local v4, "wifiOnly":Z
    const/4 v0, 0x0

    .line 1529
    .local v0, "bNetConn":Z
    const-string v5, "connectivity"

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 1530
    .local v1, "connManager":Landroid/net/ConnectivityManager;
    if-eqz v4, :cond_2

    .line 1531
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1532
    .local v3, "uWifi":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1533
    const/4 v0, 0x1

    .line 1556
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return v0

    .line 1535
    .restart local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_1
    if-eqz p1, :cond_0

    .line 1536
    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v6, 0x7f070082

    invoke-static {v5, v6}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1544
    .end local v3    # "uWifi":Landroid/net/NetworkInfo;
    :cond_2
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1545
    .local v2, "mNetInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1546
    const/4 v0, 0x1

    goto :goto_0

    .line 1548
    :cond_3
    if-eqz p1, :cond_0

    .line 1549
    const v5, 0x7f07009b

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private processBackupDetailRequestFail()V
    .locals 2

    .prologue
    .line 1407
    const-string v0, "ManualRestoreActivity"

    const-string v1, "[processBackupDetailRequestFail]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 1411
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestBackupDetailCancel()V

    .line 1412
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->backupDetailFailed()V

    .line 1413
    return-void
.end method

.method private processRestoreReady()V
    .locals 2

    .prologue
    .line 1058
    const-string v0, "ManualRestoreActivity"

    const-string v1, "[processRestoreReady]"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 1062
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1088
    :cond_0
    return-void
.end method

.method private registerSamsungAccountReceiver()V
    .locals 3

    .prologue
    .line 1818
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1819
    new-instance v1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$28;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$28;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 1884
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1885
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.msc.action.VALIDATION_CHECK_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1886
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1887
    return-void
.end method

.method private removeRemoteTimeoutMessage(I)V
    .locals 3
    .param p1, "mMessgeType"    # I

    .prologue
    .line 1402
    const-string v0, "ManualRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[2. removeRemoteTimeoutMessage]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1404
    return-void
.end method

.method private restoreCancelled(Ljava/lang/String;)V
    .locals 7
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1020
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "restoreCancelled"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    const/4 v2, 0x0

    .line 1022
    .local v2, "restoreStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1023
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1024
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 1025
    const/4 v2, 0x1

    goto :goto_0

    .line 1028
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    if-nez v2, :cond_2

    .line 1029
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setCanceled(Z)V

    .line 1030
    sput-boolean v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 1031
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 1032
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->enablePreference()V

    .line 1033
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1035
    :cond_2
    invoke-direct {p0, p1, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setLastBackupTimeSource(Ljava/lang/String;Z)V

    .line 1036
    return-void
.end method

.method private restoreFailed(Ljava/lang/String;)V
    .locals 8
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 1039
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "restoreFailed"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    const/4 v2, 0x0

    .line 1041
    .local v2, "restoreStatus":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1042
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1043
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v7, :cond_0

    .line 1044
    const/4 v2, 0x1

    goto :goto_0

    .line 1047
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    if-nez v2, :cond_2

    .line 1048
    sput-boolean v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 1049
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 1050
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->enablePreference()V

    .line 1051
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v3, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1053
    :cond_2
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setLastBackupTimeSource(Ljava/lang/String;Z)V

    .line 1054
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 1055
    return-void
.end method

.method private sendRemoteTimeoutMessage(I)V
    .locals 4
    .param p1, "mMessgeType"    # I

    .prologue
    .line 1396
    const-string v0, "ManualRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[1. sendRemoteTimeoutMessage] : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1397
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1399
    return-void
.end method

.method private setActivityTheme()V
    .locals 3

    .prologue
    const v2, 0x103012b

    const/4 v1, 0x1

    .line 1560
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1561
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eq v0, v1, :cond_0

    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-ne v0, v1, :cond_4

    .line 1562
    :cond_0
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setTheme(I)V

    .line 1566
    :cond_1
    :goto_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_Light()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-le v0, v1, :cond_3

    .line 1567
    :cond_2
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setTheme(I)V

    .line 1570
    :cond_3
    return-void

    .line 1564
    :cond_4
    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setTheme(I)V

    goto :goto_0
.end method

.method private setLastBackupTime()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 683
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 684
    .local v3, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getLastOperationStatus(Ljava/lang/String;Z)I

    move-result v4

    .line 685
    .local v4, "lastOperationStatus":I
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 686
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    const v6, 0x7f070042

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 687
    .local v5, "lastTime":Ljava/lang/String;
    const/4 v2, 0x0

    .line 688
    .local v2, "isChecked":Z
    if-nez v0, :cond_1

    .line 689
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3, v10, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 709
    :cond_0
    :goto_1
    const/4 v6, -0x1

    if-ne v4, v6, :cond_5

    .line 710
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/preference/CheckBoxPreference;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f07006a

    invoke-virtual {p0, v8}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 713
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0

    .line 692
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v8, 0x7f070041

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 694
    const/4 v2, 0x1

    .line 695
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-ne v6, v11, :cond_2

    .line 696
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3, v11, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 698
    :cond_2
    const-string v6, "VIPLIST"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "BLACKLIST"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 699
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 700
    const/4 v2, 0x0

    .line 701
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3, v10, v10}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 702
    const-string v6, "VIPLIST"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 703
    const v6, 0x7f07008e

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 705
    :cond_4
    const v6, 0x7f07008f

    invoke-virtual {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 712
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v6, v5}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 715
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v2    # "isChecked":Z
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "lastOperationStatus":I
    .end local v5    # "lastTime":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method private setLastBackupTimeSource(Ljava/lang/String;Z)V
    .locals 8
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bSuccess"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 718
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLastBackupTimeSource "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    const-string v2, " Not null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 720
    .local v0, "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    const v2, 0x7f070042

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 721
    .local v1, "lastTime":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 722
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1, v6, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    .line 730
    :cond_0
    :goto_1
    if-nez p2, :cond_3

    .line 731
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f07006a

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 734
    :goto_2
    return-void

    .line 718
    .end local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .end local v1    # "lastTime":Ljava/lang/String;
    :cond_1
    const-string v2, " Null"

    goto :goto_0

    .line 725
    .restart local v0    # "bd":Lcom/samsung/android/scloud/backup/BackupDetails;
    .restart local v1    # "lastTime":Ljava/lang/String;
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f070041

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestamp()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getDateStringFromTimeSpan(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 727
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-ne v2, v7, :cond_0

    .line 728
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2, p1, v7, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelected(Ljava/lang/String;ZZ)V

    goto :goto_1

    .line 733
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private setSourceList()V
    .locals 3

    .prologue
    .line 617
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    .line 618
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 619
    .local v1, "key":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 620
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    goto :goto_0

    .line 623
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private showBackupDetailProgressDialog()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 967
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07003b

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 968
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f07003e

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 969
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 970
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 972
    new-instance v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$8;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    .line 984
    .local v0, "mCancelListener":Landroid/content/DialogInterface$OnClickListener;
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, -0x2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const/high16 v4, 0x1040000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 987
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$9;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$9;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 995
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 996
    return-void
.end method

.method private showData3gDialog()V
    .locals 5

    .prologue
    .line 1321
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1322
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1324
    .local v1, "linearLayout":Landroid/view/View;
    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->checkBox:Landroid/widget/CheckBox;

    .line 1325
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->checkBox:Landroid/widget/CheckBox;

    new-instance v3, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$19;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$19;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1334
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f070010

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$22;

    invoke-direct {v3, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$22;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v4, 0x104000a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$21;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07002a

    new-instance v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$20;

    invoke-direct {v4, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$20;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertDialog:Landroid/app/AlertDialog;

    .line 1370
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1371
    return-void
.end method

.method private startNotificationService(Z[Ljava/lang/String;)V
    .locals 3
    .param p1, "isCancelTrue"    # Z
    .param p2, "sourceList"    # [Ljava/lang/String;

    .prologue
    .line 959
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 960
    .local v0, "service":Landroid/content/Intent;
    const-class v1, Lcom/samsung/android/scloud/backup/NotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 961
    const-string v1, "SOURCE_LIST"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 962
    const-string v2, "Notification Type"

    if-eqz p1, :cond_0

    const/16 v1, 0x68

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 963
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 964
    return-void

    .line 962
    :cond_0
    const/16 v1, 0x66

    goto :goto_0
.end method

.method private unregisterSamsungAccountReceiver()V
    .locals 2

    .prologue
    .line 1890
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 1891
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSamsungAccountReceiver:Landroid/content/BroadcastReceiver;

    .line 1894
    :cond_0
    return-void
.end method

.method private updatePref()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 825
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 827
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    .line 828
    .local v2, "sourcePreference":Landroid/preference/CheckBoxPreference;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelected(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 829
    const-string v3, "VIPLIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "BLACKLIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 830
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isEmailLoggedIn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 831
    invoke-virtual {v2, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 834
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "sourcePreference":Landroid/preference/CheckBoxPreference;
    :cond_2
    return-void
.end method

.method private updatePreferenceList()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 165
    const-string v1, "logcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    .line 166
    const-string v1, "smscheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    .line 167
    const-string v1, "mmscheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    .line 168
    const-string v1, "wallpapercheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    .line 169
    const-string v1, "viplistcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    .line 170
    const-string v1, "blacklistcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    .line 171
    const-string v1, "spamcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    .line 172
    const-string v1, "callrejectcheckBox"

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    .line 175
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v0

    .line 176
    .local v0, "activityStatus":I
    const-string v1, "ManualRestoreActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updatePreferenceList : activityStatus - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 179
    sput-boolean v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 180
    sput-boolean v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 181
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 182
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    .line 185
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayRestoring()V

    .line 186
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    .line 201
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingRestore()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 189
    :cond_2
    sput-boolean v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 190
    sput-boolean v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 191
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 192
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayCancelling()V

    .line 193
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    goto :goto_0

    .line 196
    :cond_3
    sput-boolean v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 197
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 198
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->enablePreference()V

    .line 199
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setLastBackupTime()V

    goto :goto_0
.end method


# virtual methods
.method public doNegativeClick(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 1271
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1

    .line 1272
    invoke-direct {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finishThisActivity(I)V

    .line 1277
    :cond_0
    :goto_0
    return-void

    .line 1273
    :cond_1
    const/16 v0, 0x11

    if-ne p1, v0, :cond_0

    .line 1274
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 1275
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestResume()V

    goto :goto_0
.end method

.method public doPositiveClick(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const v3, 0x7f070018

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1229
    const/16 v0, 0xf

    if-ne p1, v0, :cond_1

    .line 1230
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getDetail()V

    .line 1268
    :cond_0
    :goto_0
    return-void

    .line 1231
    :cond_1
    const/4 v0, 0x7

    if-ne p1, v0, :cond_3

    .line 1232
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->bRestoreFailed:Z

    if-eqz v0, :cond_2

    .line 1233
    iput-boolean v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->bRestoreFailed:Z

    .line 1235
    :cond_2
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finishThisActivity(I)V

    goto :goto_0

    .line 1236
    :cond_3
    const/16 v0, 0x9

    if-ne p1, v0, :cond_4

    .line 1237
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    .line 1238
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setFromAutoBackupActivityValue(Z)V

    .line 1241
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1246
    :cond_4
    const/16 v0, 0x11

    if-ne p1, v0, :cond_0

    .line 1247
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 1252
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setCanceled(Z)V

    .line 1253
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestAllCancelRestore()V

    .line 1254
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayCancelling()V

    .line 1255
    sput-boolean v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 1256
    sput-boolean v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 1257
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 1259
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAutoBackupStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setAutoBackupStatus(Z)V

    .line 1261
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setFromAutoBackupActivityValue(Z)V

    .line 1262
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public getDetail()V
    .locals 4

    .prologue
    .line 837
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 838
    const v2, 0x7f070089

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 839
    .local v0, "serverBusyMessage":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 843
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    .line 851
    .end local v0    # "serverBusyMessage":Ljava/lang/String;
    :goto_0
    return-void

    .line 846
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showBackupDetailProgressDialog()V

    .line 847
    new-instance v1, Landroid/content/Intent;

    const-string v2, "REQUEST_GETDETAIL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 848
    .local v1, "service":Landroid/content/Intent;
    const-string v2, "TRIGGER"

    const-string v3, "user"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 849
    const-class v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 850
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1574
    const-string v0, "ManualRestoreActivity"

    return-object v0
.end method

.method public getTargetEventServiceCodes()[I
    .locals 1

    .prologue
    .line 1653
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x67
        0x66
    .end array-data
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 1805
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1806
    if-ne p1, v1, :cond_0

    .line 1807
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 1808
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidationStatus:I

    .line 1815
    :cond_0
    :goto_0
    return-void

    .line 1812
    :cond_1
    iput v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidationStatus:I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 205
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getIsTablet(Landroid/content/res/Configuration;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    packed-switch v0, :pswitch_data_0

    .line 231
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 234
    :cond_1
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-eqz v0, :cond_5

    .line 235
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setContentView(I)V

    .line 240
    :cond_2
    :goto_1
    return-void

    .line 209
    :pswitch_0
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onConfigurationChanged : ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 212
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 220
    :pswitch_1
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onConfigurationChanged : ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 223
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 236
    :cond_5
    sget-boolean v0, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-eqz v0, :cond_2

    .line 237
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setContentView(I)V

    goto :goto_1

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f070056

    const/16 v7, 0x400

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 244
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setActivityTheme()V

    .line 245
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 248
    iput-boolean v8, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->newActivity:Z

    .line 250
    sget-boolean v4, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_H:Z

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_J:Z

    if-eqz v4, :cond_5

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 257
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    .line 258
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 259
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getValidSourceList()Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    .line 261
    const-string v4, "ManualRestoreActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[onCreate] Acti:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getActivityStatus()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-direct {p0, v8}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->networkisAvailable(Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 264
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    .line 266
    :cond_1
    const/high16 v4, 0x7f040000

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->addPreferencesFromResource(I)V

    .line 268
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isSatusBarHidden()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 269
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 271
    :cond_2
    sget-boolean v4, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_VIENNA:Z

    if-ne v4, v8, :cond_6

    .line 272
    const v4, 0x7f03000e

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setContentView(I)V

    .line 278
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const-string v5, "screen"

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceScreen;

    .line 279
    .local v3, "prefscreen":Landroid/preference/PreferenceScreen;
    const-string v4, "restore_description"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mRestoreDescriptionPreference:Landroid/preference/Preference;

    .line 280
    sget-boolean v4, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_T:Z

    if-nez v4, :cond_3

    .line 281
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mRestoreDescriptionPreference:Landroid/preference/Preference;

    check-cast v4, Lcom/samsung/android/scloud/backup/CustomSummaryPref;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 282
    :cond_3
    const-string v4, "backedup_device_list_preference"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    .line 283
    const-string v4, "logcheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    .line 284
    const-string v4, "smscheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    .line 285
    const-string v4, "mmscheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    .line 286
    const-string v4, "wallpapercheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    .line 287
    const-string v4, "viplistcheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    .line 288
    const-string v4, "blacklistcheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    .line 289
    const-string v4, "spamcheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    .line 290
    const-string v4, "callrejectcheckBox"

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    .line 293
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 294
    .local v2, "key":Ljava/lang/String;
    const-string v4, "CALLLOGS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 295
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mLog:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 253
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    goto/16 :goto_0

    .line 273
    :cond_6
    sget-boolean v4, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-ne v4, v8, :cond_7

    .line 274
    const v4, 0x7f03000f

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setContentView(I)V

    goto/16 :goto_1

    .line 277
    :cond_7
    const v4, 0x7f030017

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setContentView(I)V

    goto/16 :goto_1

    .line 296
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "key":Ljava/lang/String;
    .restart local v3    # "prefscreen":Landroid/preference/PreferenceScreen;
    :cond_8
    const-string v4, "SMS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 297
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 298
    :cond_9
    const-string v4, "MMS"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 299
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 300
    :cond_a
    const-string v4, "HOMESCREEN"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 301
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mWallpaper:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 302
    :cond_b
    const-string v4, "VIPLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 303
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mviplist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 304
    :cond_c
    const-string v4, "BLACKLIST"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 305
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBlacklist:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 306
    :cond_d
    const-string v4, "SPAM"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 307
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mEmail:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 308
    :cond_e
    const-string v4, "CALLREJECT"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 309
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mCallReject:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 314
    .end local v2    # "key":Ljava/lang/String;
    :cond_f
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSms:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070007

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMms:Landroid/preference/CheckBoxPreference;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 317
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initPref()V

    .line 319
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    new-instance v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 327
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 328
    .restart local v2    # "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/preference/CheckBoxPreference;

    new-instance v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;

    invoke-direct {v5, p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$2;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_3

    .line 338
    .end local v2    # "key":Ljava/lang/String;
    :cond_10
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v4

    if-nez v4, :cond_11

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestoreFailed()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 339
    :cond_11
    sput-boolean v8, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 340
    sput-boolean v9, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 341
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 342
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 343
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getBackupList()Lorg/json/JSONArray;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    .line 344
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 345
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    .line 346
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceIndex()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    .line 347
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V

    .line 348
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 349
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayRestoring()V

    .line 350
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    .line 373
    :goto_4
    new-instance v4, Landroid/app/ProgressDialog;

    invoke-direct {v4, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    .line 374
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    new-instance v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$3;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 385
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getVerificationStatus()I

    move-result v4

    if-eqz v4, :cond_12

    .line 386
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->checkAccountValidation(Landroid/content/Context;)Z

    move-result v0

    .line 387
    .local v0, "accountValid":Z
    if-ne v0, v8, :cond_16

    .line 388
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v9}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setVerificationStatus(I)V

    .line 395
    .end local v0    # "accountValid":Z
    :cond_12
    :goto_5
    return-void

    .line 353
    :cond_13
    const/16 v4, 0x9

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    goto :goto_4

    .line 354
    :cond_14
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingRestore()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 355
    sput-boolean v8, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 356
    sput-boolean v8, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 357
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 358
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 359
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getBackupList()Lorg/json/JSONArray;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    .line 360
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 361
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    .line 362
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceIndex()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    .line 363
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V

    .line 364
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayCancelling()V

    .line 365
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    goto :goto_4

    .line 368
    :cond_15
    sput-boolean v9, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 369
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    goto :goto_4

    .line 391
    .restart local v0    # "accountValid":Z
    :cond_16
    iput-object p0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mActivity:Landroid/app/Activity;

    .line 392
    new-instance v4, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$CheckSamsungAccountValidation;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$CheckSamsungAccountValidation;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;)V

    new-array v5, v9, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$CheckSamsungAccountValidation;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_5
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const v4, 0x7f07007f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 498
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setActivityTheme()V

    .line 500
    sput-object p1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMenu:Landroid/view/Menu;

    .line 501
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    .line 502
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    .line 503
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    .line 504
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;

    invoke-direct {v1, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$7;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 527
    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 528
    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v5, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    .line 529
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 530
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 533
    sget-boolean v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    if-eqz v0, :cond_9

    .line 534
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    const v1, 0x7f07006d

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 535
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 536
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    const v1, 0x7f07006d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 538
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingRestore()Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    if-eqz v0, :cond_7

    .line 539
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 540
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 541
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 542
    :cond_4
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onCreateOptionsMenu ,cancelstatus = true, setenable = false"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 560
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setSourceList()V

    .line 561
    iget-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    if-eqz v0, :cond_5

    .line 562
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 563
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 564
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 567
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v5, :cond_c

    .line 569
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_6

    .line 570
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 571
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 584
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 545
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 546
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 547
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 548
    :cond_8
    const-string v0, "ManualRestoreActivity"

    const-string v1, "  onCreateOptionsMenu, cancelstatus = true, setenable = true"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 552
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 553
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_a

    .line 554
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 555
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 556
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    .line 557
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 558
    :cond_b
    const-string v0, "ManualRestoreActivity"

    const-string v1, "  onCreateOptionsMenu ,cancelstatus = false, setenable = true"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 575
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebutton:Landroid/widget/TextView;

    if-eqz v0, :cond_d

    .line 577
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restorebuttonlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 578
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 581
    :cond_d
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->buttonrestore:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 643
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onDestroy()V

    .line 644
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 646
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    .line 648
    :cond_0
    return-void
.end method

.method public onEventReceived(IIILandroid/os/Message;)V
    .locals 10
    .param p1, "serviceType"    # I
    .param p2, "status"    # I
    .param p3, "rCode"    # I
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v9, 0x12d

    const/4 v5, -0x1

    const/4 v8, 0x0

    const/16 v7, 0x132

    const/4 v6, 0x1

    .line 1580
    const/16 v3, 0x66

    if-ne p1, v3, :cond_9

    .line 1581
    packed-switch p2, :pswitch_data_0

    .line 1649
    :cond_0
    :goto_0
    return-void

    .line 1583
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1586
    :pswitch_1
    const/4 v2, 0x1

    .line 1587
    .local v2, "noneRestoring":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1588
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v1}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 1589
    const/4 v2, 0x0

    goto :goto_1

    .line 1591
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1592
    if-ne v9, p3, :cond_3

    .line 1593
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restoreSuccess(Ljava/lang/String;)V

    .line 1603
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1594
    :cond_3
    if-ne v7, p3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1595
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restoreCancelled(Ljava/lang/String;)V

    goto :goto_2

    .line 1596
    :cond_4
    if-ne p3, v7, :cond_5

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1597
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 1598
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1599
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v6, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto/16 :goto_2

    .line 1602
    :cond_5
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->restoreFailed(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1606
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "noneRestoring":Z
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:203 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1609
    :pswitch_3
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isPaused()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isCanceled()Z

    move-result v3

    if-nez v3, :cond_8

    .line 1610
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_8

    .line 1611
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSourceKeyPreference:Ljava/util/HashMap;

    iget-object v4, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    const v4, 0x7f07006f

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v7, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getProgress(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1613
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:202 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1616
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:204 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1622
    :cond_9
    const/16 v3, 0x67

    if-ne p1, v3, :cond_0

    .line 1623
    const/16 v3, 0xcd

    if-ne v3, p2, :cond_a

    .line 1624
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:205 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1625
    invoke-direct {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->removeRemoteTimeoutMessage(I)V

    .line 1626
    if-ne p3, v9, :cond_b

    .line 1627
    iget-object v3, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lorg/json/JSONArray;

    iput-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    .line 1628
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setBackupList(Lorg/json/JSONArray;)V

    .line 1629
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->processRestoreReady()V

    .line 1644
    :cond_a
    :goto_3
    const/16 v3, 0xc9

    if-ne v3, p2, :cond_0

    .line 1645
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "status:201 onEventReceived :  ,rcode"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ,msg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg1 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", arg2 : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p4, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , obj : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1646
    invoke-direct {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->sendRemoteTimeoutMessage(I)V

    goto/16 :goto_0

    .line 1631
    :cond_b
    if-ne p3, v7, :cond_c

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1632
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v8}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setTimeOutCancelled(Z)V

    .line 1633
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Request cancelled after timeout"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1635
    :cond_c
    if-ne p3, v7, :cond_d

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isTimeOutCancelled()Z

    move-result v3

    if-nez v3, :cond_d

    .line 1636
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Request cancelled by the user"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1640
    :cond_d
    iput-boolean v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsBackupDetailFailed:Z

    .line 1641
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->backupDetailFailed()V

    goto/16 :goto_3

    .line 1581
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 147
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 148
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 150
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 151
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 152
    const-string v2, "Dialog_Type"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 153
    .local v1, "dialog":I
    if-eq v1, v3, :cond_0

    .line 154
    const-string v2, "Dialog_Type"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "dialog from onNewIntent!!!!!!!!!!!!!!!!!!!"

    invoke-static {v2, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    .line 162
    .end local v1    # "dialog":I
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->updatePreferenceList()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 589
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 612
    invoke-super {p0, p1}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 591
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    goto :goto_0

    .line 595
    :sswitch_1
    sget-boolean v1, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    if-eqz v1, :cond_0

    .line 596
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->requestPause()V

    .line 597
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setPaused(Z)V

    .line 598
    const/16 v1, 0x11

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    goto :goto_0

    .line 600
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setSourceList()V

    .line 601
    iget-boolean v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->noneSelected:Z

    if-eqz v1, :cond_1

    .line 603
    const-string v1, "ManualRestoreActivity"

    const-string v2, "onOptionsItemSelected - noneSelected = true"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 606
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->onRestoreClicked()V

    goto :goto_0

    .line 589
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 627
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onPause()V

    .line 628
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getVerificationStatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->newActivity:Z

    .line 631
    :cond_0
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 634
    :cond_1
    return-void
.end method

.method public declared-synchronized onRestoreClicked()V
    .locals 2

    .prologue
    .line 1505
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->networkisAvailable(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1506
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startRestoreProcess()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1521
    :goto_0
    monitor-exit p0

    return-void

    .line 1508
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1509
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v1, 0x7f070082

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1515
    :cond_1
    const v0, 0x7f07009b

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 399
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onResume()V

    .line 401
    const-string v4, "ManualRestoreActivity"

    const-string v5, "onResume"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    invoke-direct {p0, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->networkisAvailable(Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 404
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityBackup()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingBackup()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 409
    :cond_2
    const-string v4, "ManualRestoreActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mMetaManager.isActivityBackup(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityBackup()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const v4, 0x7f070065

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 411
    .local v3, "serverBusyMessage":Ljava/lang/String;
    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 415
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    goto :goto_0

    .line 419
    .end local v3    # "serverBusyMessage":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_4

    .line 420
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 422
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "Dialog_Type"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 423
    .local v0, "dialog":I
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "Dialog_Type"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 424
    if-eq v0, v8, :cond_4

    .line 425
    const/16 v4, 0x9

    if-ne v0, v4, :cond_7

    .line 426
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestoreFailed()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 427
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "dialog from onResume!!!!!!!!!!!!!!!!!!!"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    .line 439
    .end local v0    # "dialog":I
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getVerificationStatus()I

    move-result v4

    if-nez v4, :cond_0

    .line 440
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestore()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityRestoreFailed()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isActivityCancelingRestore()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 441
    :cond_5
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->newActivity:Z

    if-nez v4, :cond_6

    .line 442
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    .line 443
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getBackupList()Lorg/json/JSONArray;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    .line 444
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    .line 445
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;

    invoke-virtual {v4, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    .line 446
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedDeviceIndex()I

    move-result v4

    iput v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    .line 447
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->initView()V

    .line 449
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->updatePreferenceList()V

    goto/16 :goto_0

    .line 433
    .restart local v0    # "dialog":I
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v4

    const-string v5, "dialog from onResume!!!!!!!!!!!!!!!!!!!"

    invoke-static {v4, v5}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-virtual {p0, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    goto :goto_1

    .line 450
    .end local v0    # "dialog":I
    :cond_8
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_0

    .line 451
    :cond_9
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->newActivity:Z

    if-ne v4, v6, :cond_b

    .line 452
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityBackupDetail()V

    .line 453
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mValidSourceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 454
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v2, v7, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    goto :goto_2

    .line 455
    .end local v2    # "key":Ljava/lang/String;
    :cond_a
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceChange:Ljava/lang/Boolean;

    .line 456
    iput-boolean v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsDialogActiveFlag:Z

    .line 457
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f070067

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07007e

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$5;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$5;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07002a

    new-instance v6, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$4;

    invoke-direct {v6, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$4;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertdialog:Landroid/app/Dialog;

    .line 476
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertdialog:Landroid/app/Dialog;

    new-instance v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$6;

    invoke-direct {v5, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$6;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 484
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->alertdialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto/16 :goto_0

    .line 488
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_b
    iget-boolean v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mIsBackupDetailFailed:Z

    if-ne v4, v6, :cond_0

    .line 489
    const/16 v4, 0xf

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showDialogById(I)V

    goto/16 :goto_0
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 638
    invoke-super {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->onStop()V

    .line 639
    return-void
.end method

.method public requestAllCancelRestore()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 931
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_CANCEL"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 932
    .local v5, "service":Landroid/content/Intent;
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 933
    const/4 v1, 0x0

    .line 934
    .local v1, "count":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 935
    .local v0, "cancelList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 936
    .local v3, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v6

    if-ne v6, v8, :cond_0

    .line 937
    add-int/lit8 v1, v1, 0x1

    .line 938
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 941
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    new-array v4, v1, [Ljava/lang/String;

    .line 942
    .local v4, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 943
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityCancelingRestore()V

    .line 944
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 945
    invoke-direct {p0, v8, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startNotificationService(Z[Ljava/lang/String;)V

    .line 946
    return-void
.end method

.method public requestBackupDetailCancel()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 920
    new-instance v1, Landroid/content/Intent;

    const-string v2, "OPERATION_CANCEL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 921
    .local v1, "service":Landroid/content/Intent;
    const-class v2, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 922
    new-array v0, v4, [Ljava/lang/String;

    .line 923
    .local v0, "mSourceArr":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v3, "REQUEST_GETDETAIL"

    aput-object v3, v0, v2

    .line 924
    const-string v2, "SOURCE_LIST"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 925
    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 926
    invoke-direct {p0, v4, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startNotificationService(Z[Ljava/lang/String;)V

    .line 927
    return-void
.end method

.method public requestPause()V
    .locals 8

    .prologue
    .line 887
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_PAUSE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 888
    .local v5, "service":Landroid/content/Intent;
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 889
    const/4 v0, 0x0

    .line 890
    .local v0, "count":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 891
    .local v4, "pauseList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 892
    .local v2, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 893
    add-int/lit8 v0, v0, 0x1

    .line 894
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 897
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    new-array v3, v0, [Ljava/lang/String;

    .line 898
    .local v3, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 899
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 900
    return-void
.end method

.method public requestResume()V
    .locals 8

    .prologue
    .line 903
    new-instance v5, Landroid/content/Intent;

    const-string v6, "OPERATION_RESUME"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 904
    .local v5, "service":Landroid/content/Intent;
    const-class v6, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 905
    const/4 v0, 0x0

    .line 906
    .local v0, "count":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 907
    .local v4, "resumeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 908
    .local v2, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v6, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 909
    add-int/lit8 v0, v0, 0x1

    .line 910
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 913
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    new-array v3, v0, [Ljava/lang/String;

    .line 914
    .local v3, "mSourceArr":[Ljava/lang/String;
    const-string v7, "SOURCE_LIST"

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 915
    invoke-virtual {p0, v5}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 916
    return-void
.end method

.method public restoreSuccess(Ljava/lang/String;)V
    .locals 7
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 999
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getTag()Ljava/lang/String;

    move-result-object v3

    const-string v4, "restoreSuccess"

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    const/4 v1, 0x0

    .line 1001
    .local v1, "isRestoring":Z
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    .line 1002
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1003
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getUIStatus(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 1004
    const/4 v1, 0x1

    goto :goto_0

    .line 1007
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    if-nez v1, :cond_2

    .line 1008
    sput-boolean v5, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 1009
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 1010
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->enablePreference()V

    .line 1011
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v3, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1015
    :cond_2
    invoke-direct {p0, p1, v6}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->setLastBackupTimeSource(Ljava/lang/String;Z)V

    .line 1016
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3, p1, v5, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setLastOperationStatus(Ljava/lang/String;ZI)V

    .line 1017
    return-void
.end method

.method public showDialogById(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    const v4, 0x7f07006d

    const/4 v3, 0x0

    const v2, 0x104000a

    .line 1099
    packed-switch p1, :pswitch_data_0

    .line 1197
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not defined dialog ID!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1101
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07006b

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070071

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070072

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$12;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$12;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070043

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$11;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$11;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    .line 1120
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    check-cast v0, Landroid/app/AlertDialog;

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 1121
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1213
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1214
    return-void

    .line 1125
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0700a0

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070053

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$14;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$14;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$13;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 1146
    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07002f

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070030

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$15;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$15;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 1160
    :pswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070099

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$17;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$17;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f070084

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$16;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$16;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 1183
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f07009c

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07006c

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$18;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$18;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 1099
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public declared-synchronized showSelectDeviceDialog()V
    .locals 3

    .prologue
    .line 1417
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1502
    :goto_0
    monitor-exit p0

    return-void

    .line 1420
    :cond_0
    :try_start_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1422
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f070080

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1424
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 1425
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1426
    :cond_1
    const v1, 0x7f070023

    invoke-virtual {p0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1417
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1431
    .restart local v0    # "alert":Landroid/app/AlertDialog$Builder;
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    if-nez v1, :cond_3

    .line 1432
    new-instance v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    invoke-direct {v1, v2, p0}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    .line 1434
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    .line 1435
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;

    iget v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 1436
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iget v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->setfoucsPosition(I)V

    .line 1439
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1441
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$24;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1460
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$25;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$25;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1470
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    .line 1471
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$26;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$26;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1491
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    new-instance v2, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;

    invoke-direct {v2, p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$27;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1501
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mAd:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public startRestore()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 854
    sput-boolean v7, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->cancelstatus:Z

    .line 855
    sput-boolean v6, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->first:Z

    .line 856
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->invalidateOptionsMenu()V

    .line 857
    new-instance v3, Landroid/content/Intent;

    const-string v4, "REQUEST_RESTORE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 858
    .local v3, "service":Landroid/content/Intent;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v2, v4, [Ljava/lang/String;

    .line 859
    .local v2, "mSourceArr":[Ljava/lang/String;
    const-string v5, "SOURCE_LIST"

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 860
    const-string v4, "TRIGGER"

    const-string v5, "user"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 861
    const-string v4, "SOURCE_DEVICE"

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 862
    const-class v4, Lcom/samsung/android/scloud/backup/core/BNRTaskService;

    invoke-virtual {v3, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 863
    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 864
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v6}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getSelectedList(Z)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 865
    .local v1, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4, v1, v7}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setUIStatus(Ljava/lang/String;I)V

    goto :goto_0

    .line 866
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityRestore()V

    .line 867
    invoke-direct {p0, v6, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startNotificationService(Z[Ljava/lang/String;)V

    .line 868
    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackedupDeviceListPreference:Landroid/preference/Preference;

    invoke-virtual {v4, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 869
    return-void
.end method

.method public startRestoreProcess()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1280
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1281
    .local v0, "mConnectivityManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1282
    .local v2, "mWifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1284
    .local v1, "mMobile":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1285
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 1286
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startRestore()V

    .line 1287
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceId(Ljava/lang/String;)V

    .line 1288
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayInitialRestoring()V

    .line 1289
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    .line 1317
    :goto_0
    return-void

    .line 1292
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->finish()V

    goto :goto_0

    .line 1293
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1294
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getData3gAskStatusRestore()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1295
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->showData3gDialog()V

    goto :goto_0

    .line 1297
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->disablePreference()V

    .line 1298
    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->displayInitialRestoring()V

    .line 1299
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setSelectedDeviceId(Ljava/lang/String;)V

    .line 1300
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->startRestore()V

    goto :goto_0

    .line 1303
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->isWiFiOnly()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1304
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mContext:Landroid/content/Context;

    const v4, 0x7f070082

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/ControlRegionalData;->getStringIdByRegion(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1315
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    invoke-virtual {v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->setActivityNone()V

    goto :goto_0

    .line 1310
    :cond_4
    const v3, 0x7f07009b

    invoke-virtual {p0, v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
