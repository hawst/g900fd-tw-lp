.class public Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;
.super Ljava/lang/Object;
.source "InternalOEMControl.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IOEMControl;


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mBuilderMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "OEMControl"

    sput-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    .line 57
    new-instance v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    .line 63
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "CALLLOGS"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "CALLLOGS"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/CalllogBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "SMS"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "SMS"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/SmsBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "MMS"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "MMS"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "HOMESCREEN"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "HOMESCREEN"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/WallpaperBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "VIPLIST"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/VIPBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "VIPLIST"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/VIPBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "BLACKLIST"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/BlackListBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "BLACKLIST"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/BlackListBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "SPAM"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/SpamBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "SPAM"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/SpamBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    const-string v1, "CALLREJECT"

    new-instance v2, Lcom/samsung/android/scloud/backup/modelbuilder/CallRejectBuilder;

    invoke-static {}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;

    move-result-object v3

    const-string v4, "CALLREJECT"

    invoke-virtual {v3, v4}, Lcom/samsung/android/scloud/backup/model/ModelManager;->getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/CallRejectBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void
.end method

.method private static delete(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Landroid/net/Uri;Ljava/util/List;ZLjava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p2, "oemUri"    # Landroid/net/Uri;
    .param p4, "isSuccess"    # Z
    .param p5, "sourceKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "insertedList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 190
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 191
    .local v1, "mResolver":Landroid/content/ContentResolver;
    if-nez p3, :cond_0

    .line 207
    :goto_0
    return-void

    .line 194
    :cond_0
    sget-object v2, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "oemDelete - start : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v6, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "cnt":I
    if-nez p4, :cond_2

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 197
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id IN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getWhereKey(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 204
    :cond_1
    :goto_1
    const/16 v2, 0x66

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x41100000    # 9.0f

    div-float/2addr v3, v4

    invoke-interface {p1, p5, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 205
    sget-object v2, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "oemDelete - end   : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v6, v2, v3}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 198
    :cond_2
    if-eqz p4, :cond_3

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_3

    .line 199
    invoke-virtual {v1, p2, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 201
    :cond_3
    if-eqz p4, :cond_1

    .line 202
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id < "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1
.end method

.method public static getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->INSTANCE:Lcom/samsung/android/scloud/backup/core/IOEMControl;

    return-object v0
.end method

.method private static getLength(J)I
    .locals 8
    .param p0, "num"    # J

    .prologue
    const-wide/16 v6, 0xa

    .line 80
    const/4 v2, 0x1

    .line 81
    .local v2, "length":I
    move-wide v0, p0

    .line 82
    .local v0, "comp":J
    const-wide/16 v4, 0x0

    cmp-long v3, p0, v4

    if-nez v3, :cond_0

    .line 83
    const-wide/16 v0, 0xa

    .line 85
    :cond_0
    :goto_0
    cmp-long v3, v0, v6

    if-gez v3, :cond_1

    .line 90
    return v2

    .line 86
    :cond_1
    div-long/2addr v0, v6

    .line 87
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static getWhereKey(Ljava/util/List;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, ""

    .line 135
    .local v2, "where":Ljava/lang/String;
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 143
    .end local v2    # "where":Ljava/lang/String;
    .local v3, "where":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 138
    .end local v3    # "where":Ljava/lang/String;
    .restart local v2    # "where":Ljava/lang/String;
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 139
    .local v1, "key":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 140
    goto :goto_1

    .line 141
    .end local v1    # "key":Ljava/lang/String;
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 142
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 143
    .end local v2    # "where":Ljava/lang/String;
    .restart local v3    # "where":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 222
    sget-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "backupCleared  : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->backupCleared(Landroid/content/Context;)V

    .line 224
    return-void
.end method

.method public getDataFromOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;IIJLjava/util/List;Ljava/lang/String;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "start"    # I
    .param p6, "maxCount"    # I
    .param p7, "maxSize"    # J
    .param p10, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 149
    .local p4, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p9, "output":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/4 v3, 0x4

    sget-object v4, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDataFromOEM() is called!!! :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 151
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    move-object v4, p1

    move-object v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    move-object/from16 v10, p10

    invoke-virtual/range {v3 .. v10}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p9

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    const/4 v3, 0x0

    sget-object v4, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDataFromOEM() "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " : Done!!! : output size is : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v3, v4, v5}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-interface/range {p9 .. p9}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 158
    const/4 v3, 0x1

    .line 161
    :goto_1
    return v3

    .line 153
    :catch_0
    move-exception v2

    .line 154
    .local v2, "e":Lorg/json/JSONException;
    sget-object v3, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    const-string v4, "getDataFromOEM() : output.addAll(model.getBuilder().getItemFromOEM(context, keys, start, maxSize)) is Error"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 161
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getTotalKeys(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)Ljava/util/List;
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v12, "totalKeys":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->getProjection()[Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "projection":[Ljava/lang/String;
    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 98
    .local v1, "oemUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->getWhere()Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "where":Ljava/lang/String;
    const/4 v9, 0x0

    .line 100
    .local v9, "phnDB":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 101
    .local v8, "key":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v13, 0x0

    aget-object v13, v2, v13

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v13, " ASC"

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BACKUP_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface/range {p3 .. p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 106
    .local v6, "backupKey":Ljava/lang/String;
    if-nez v9, :cond_1

    .line 124
    :goto_0
    return-object v12

    .line 118
    .local v7, "dateIndex":I
    .local v10, "timeStamp":J
    :cond_0
    invoke-virtual {v8, v10, v11}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setTimeStamp(J)V

    .line 119
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    const/4 v8, 0x0

    .line 107
    .end local v7    # "dateIndex":I
    .end local v10    # "timeStamp":J
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 108
    new-instance v8, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .end local v8    # "key":Lcom/samsung/android/scloud/backup/data/BNRItem;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v4, 0x0

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 109
    .restart local v8    # "key":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const-string v0, "date"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 110
    .restart local v7    # "dateIndex":I
    const-wide/16 v10, 0x0

    .line 111
    .restart local v10    # "timeStamp":J
    const/4 v0, -0x1

    if-eq v7, v0, :cond_2

    .line 112
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 113
    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v0, v10, v4

    if-gtz v0, :cond_3

    .line 114
    const-wide v10, 0xe8d4a51000L

    .line 115
    :cond_3
    :goto_1
    invoke-static {v10, v11}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getLength(J)I

    move-result v0

    const/16 v4, 0xd

    if-ge v0, v4, :cond_0

    .line 116
    const-wide/16 v4, 0xa

    mul-long/2addr v10, v4

    goto :goto_1

    .line 122
    .end local v7    # "dateIndex":I
    .end local v10    # "timeStamp":J
    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 123
    const/4 v9, 0x0

    .line 124
    goto :goto_0
.end method

.method public postOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "isSuccess"    # Z

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v0, p1, p4}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->postOperationOnBackup(Landroid/content/Context;Z)V

    .line 218
    return-void
.end method

.method public postOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Z)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p5, "isSuccess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p4, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 183
    sget-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postOperationOnRestoreOEM - start : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v6, v0, v1}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    move-object v1, p2

    move-object v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->delete(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Landroid/net/Uri;Ljava/util/List;ZLjava/lang/String;)V

    .line 186
    sget-object v0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postOperationOnRestoreOEM - end : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v6, v0, v1}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method public preOperationOnBackup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 212
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->preOperationOnRestore(Landroid/content/Context;)V

    .line 179
    return-void
.end method

.method public putDataToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Lcom/samsung/android/scloud/backup/model/IModel;Ljava/util/List;Ljava/util/List;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 166
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p5, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x4

    sget-object v3, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "putDataToOEM() is called!!! :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v1, 0x0

    .line 169
    .local v1, "result":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->mBuilderMap:Ljava/util/HashMap;

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;

    invoke-virtual {v2, p1, p2, p4, p5}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;->putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 173
    :goto_0
    const/4 v2, 0x0

    sget-object v3, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "putDataToOEM() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p3}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is Done!!! output Size :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p2, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->checkAndLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 174
    return v1

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->TAG:Ljava/lang/String;

    const-string v3, "putDataFromOEM() : mBnrModel.getBuilder().putDataToOEM(context, items) is Error"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
