.class public final Lcom/samsung/android/scloud/backup/util/FileTool;
.super Ljava/lang/Object;
.source "FileTool.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "FileTool"

.field private static mBytes:[B

.field private static mMessageDigest:Ljava/security/MessageDigest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 38
    const/16 v0, 0x2000

    new-array v0, v0, [B

    sput-object v0, Lcom/samsung/android/scloud/backup/util/FileTool;->mBytes:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getByteArr(Ljava/io/InputStream;)[B
    .locals 6
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    const-class v4, Lcom/samsung/android/scloud/backup/util/FileTool;

    monitor-enter v4

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 191
    .local v1, "byteOpStream":Ljava/io/ByteArrayOutputStream;
    const/16 v3, 0x400

    new-array v0, v3, [B

    .line 192
    .local v0, "buff":[B
    :goto_0
    const/4 v3, 0x0

    const/16 v5, 0x400

    invoke-virtual {p0, v0, v3, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, "len":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 193
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 190
    .end local v0    # "buff":[B
    .end local v1    # "byteOpStream":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "len":I
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    .line 194
    .restart local v0    # "buff":[B
    .restart local v1    # "byteOpStream":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "len":I
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 195
    monitor-exit v4

    return-object v0
.end method

.method public static declared-synchronized getMessageDigest(Ljava/io/File;)Ljava/lang/String;
    .locals 11
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 46
    const-class v8, Lcom/samsung/android/scloud/backup/util/FileTool;

    monitor-enter v8

    const/4 v1, 0x0

    .line 48
    .local v1, "bHex":I
    :try_start_0
    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    if-nez v7, :cond_0

    .line 49
    const-string v7, "MD5"

    invoke-static {v7}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    sput-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 53
    :goto_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 56
    .local v5, "len":I
    :goto_1
    :try_start_1
    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mBytes:[B

    invoke-virtual {v3, v7}, Ljava/io/FileInputStream;->read([B)I

    move-result v5

    if-lez v5, :cond_1

    .line 57
    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    sget-object v9, Lcom/samsung/android/scloud/backup/util/FileTool;->mBytes:[B

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10, v5}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 59
    :catch_0
    move-exception v2

    .line 60
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 61
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 46
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "len":I
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 51
    :cond_0
    :try_start_3
    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v7}, Ljava/security/MessageDigest;->reset()V

    goto :goto_0

    .line 63
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "len":I
    :cond_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 64
    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v7}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    .line 65
    .local v6, "md5Data":[B
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .local v0, "UniqueID":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v7, v6

    if-ge v4, v7, :cond_3

    .line 68
    aget-byte v7, v6, v4

    and-int/lit16 v1, v7, 0xff

    .line 69
    const/16 v7, 0xf

    if-gt v1, v7, :cond_2

    .line 71
    const-string v7, "0"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 75
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v7

    monitor-exit v8

    return-object v7
.end method

.method public static declared-synchronized getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "file"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41
    const-class v1, Lcom/samsung/android/scloud/backup/util/FileTool;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigest(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getMessageDigestFileInputStream(Ljava/io/FileInputStream;)Ljava/lang/String;
    .locals 9
    .param p0, "fis"    # Ljava/io/FileInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 80
    const-class v6, Lcom/samsung/android/scloud/backup/util/FileTool;

    monitor-enter v6

    const/4 v1, 0x0

    .line 82
    .local v1, "bHex":I
    :try_start_0
    sget-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    if-nez v5, :cond_0

    .line 83
    const-string v5, "MD5"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    .line 87
    :goto_0
    const/4 v3, 0x0

    .line 88
    .local v3, "len":I
    :goto_1
    sget-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mBytes:[B

    invoke-virtual {p0, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_1

    .line 89
    sget-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    sget-object v7, Lcom/samsung/android/scloud/backup/util/FileTool;->mBytes:[B

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8, v3}, Ljava/security/MessageDigest;->update([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 80
    .end local v3    # "len":I
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    .line 85
    :cond_0
    :try_start_1
    sget-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v5}, Ljava/security/MessageDigest;->reset()V

    goto :goto_0

    .line 91
    .restart local v3    # "len":I
    :cond_1
    invoke-virtual {p0}, Ljava/io/FileInputStream;->close()V

    .line 92
    sget-object v5, Lcom/samsung/android/scloud/backup/util/FileTool;->mMessageDigest:Ljava/security/MessageDigest;

    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    .line 93
    .local v4, "md5Data":[B
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v0, "UniqueID":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v5, v4

    if-ge v2, v5, :cond_3

    .line 96
    aget-byte v5, v4, v2

    and-int/lit16 v1, v5, 0xff

    .line 97
    const/16 v5, 0xf

    if-gt v1, v5, :cond_2

    .line 99
    const-string v5, "0"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 103
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    monitor-exit v6

    return-object v5
.end method

.method public static isSameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 125
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigest(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 132
    .local v0, "UniqueID":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    const/4 v2, 0x1

    .line 135
    .end local v0    # "UniqueID":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 126
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0

    .line 128
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 129
    .local v1, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static isSameFileInputStream(Ljava/io/FileInputStream;Ljava/lang/String;)Z
    .locals 4
    .param p0, "fis"    # Ljava/io/FileInputStream;
    .param p1, "checkSum"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 109
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/scloud/backup/util/FileTool;->getMessageDigestFileInputStream(Ljava/io/FileInputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 116
    .local v0, "UniqueID":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 117
    const/4 v2, 0x1

    .line 119
    .end local v0    # "UniqueID":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 110
    :catch_0
    move-exception v1

    .line 111
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    goto :goto_0

    .line 112
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 113
    .local v1, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static writeToFile(Ljava/io/InputStream;JLjava/lang/String;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;)V
    .locals 19
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "size"    # J
    .param p3, "filepath"    # Ljava/lang/String;
    .param p4, "handler"    # Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;

    .prologue
    .line 139
    const/4 v13, 0x0

    .line 141
    .local v13, "fileOpStream":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v3, "FileTool"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "writeToFile - start Write with stream : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v3, "/"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 144
    .local v18, "split":[Ljava/lang/String;
    move-object/from16 v0, v18

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v12, v18, v3

    .line 145
    .local v12, "fileName":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 147
    .local v15, "folderPath":Ljava/lang/String;
    const/high16 v3, 0x20000

    new-array v2, v3, [B

    .line 149
    .local v2, "buffer":[B
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 151
    const-string v3, "FileTool"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Creating folder : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    move-result v17

    .line 154
    .local v17, "result":Z
    if-nez v17, :cond_1

    .line 156
    const-string v3, "FileTool"

    const-string v4, "ORSMetaResponse.fromBinaryFile(): Can not create directory. "

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3}, Ljava/io/IOException;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    .end local v2    # "buffer":[B
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v15    # "folderPath":Ljava/lang/String;
    .end local v17    # "result":Z
    .end local v18    # "split":[Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 173
    .local v10, "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    const-string v3, "FileTool"

    const-string v4, "writeToFile"

    invoke-static {v3, v4, v10}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    new-instance v3, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v4, 0x133

    invoke-direct {v3, v4, v10}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_1
    if-eqz v13, :cond_0

    .line 178
    :try_start_2
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 182
    :cond_0
    :goto_2
    throw v3

    .line 162
    .restart local v2    # "buffer":[B
    .restart local v11    # "file":Ljava/io/File;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v15    # "folderPath":Ljava/lang/String;
    .restart local v18    # "split":[Ljava/lang/String;
    :cond_1
    :try_start_3
    new-instance v14, Ljava/io/FileOutputStream;

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-direct {v14, v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 163
    .end local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    .local v14, "fileOpStream":Ljava/io/FileOutputStream;
    const/16 v16, 0x0

    .line 164
    .local v16, "len":I
    const-wide/16 v6, 0x0

    .line 165
    .local v6, "sum":J
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v16

    if-lez v16, :cond_3

    .line 166
    move/from16 v0, v16

    int-to-long v4, v0

    add-long/2addr v6, v4

    .line 168
    if-eqz p4, :cond_2

    .line 169
    move/from16 v0, v16

    int-to-long v4, v0

    move-object/from16 v3, p4

    move-wide/from16 v8, p1

    invoke-interface/range {v3 .. v9}, Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$PDMProgressListener;->transferred(JJJ)V

    .line 170
    :cond_2
    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v2, v3, v0}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    .line 172
    :catch_1
    move-exception v10

    move-object v13, v14

    .end local v14    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 176
    .end local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v14    # "fileOpStream":Ljava/io/FileOutputStream;
    :cond_3
    if-eqz v14, :cond_4

    .line 178
    :try_start_5
    invoke-virtual {v14}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 184
    :cond_4
    :goto_4
    return-void

    .line 179
    :catch_2
    move-exception v10

    .line 181
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 179
    .end local v2    # "buffer":[B
    .end local v6    # "sum":J
    .end local v10    # "e":Ljava/io/IOException;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v14    # "fileOpStream":Ljava/io/FileOutputStream;
    .end local v15    # "folderPath":Ljava/lang/String;
    .end local v16    # "len":I
    .end local v18    # "split":[Ljava/lang/String;
    .restart local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v10

    .line 181
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 176
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v2    # "buffer":[B
    .restart local v6    # "sum":J
    .restart local v11    # "file":Ljava/io/File;
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v14    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v15    # "folderPath":Ljava/lang/String;
    .restart local v16    # "len":I
    .restart local v18    # "split":[Ljava/lang/String;
    :catchall_1
    move-exception v3

    move-object v13, v14

    .end local v14    # "fileOpStream":Ljava/io/FileOutputStream;
    .restart local v13    # "fileOpStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
