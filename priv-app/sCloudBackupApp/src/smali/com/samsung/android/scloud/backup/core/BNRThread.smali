.class public Lcom/samsung/android/scloud/backup/core/BNRThread;
.super Ljava/lang/Thread;
.source "BNRThread.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BNRThread-"


# instance fields
.field private mAuth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

.field private mContext:Landroid/content/Context;

.field private mCtid:Ljava/lang/String;

.field private mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

.field private mModel:Lcom/samsung/android/scloud/backup/model/IModel;

.field private mProcessedKeyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSrcDevice:Ljava/lang/String;

.field private serviceType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceType"    # I
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    .line 36
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->serviceType:I

    .line 38
    iput-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mContext:Landroid/content/Context;

    .line 49
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mContext:Landroid/content/Context;

    .line 50
    iput p2, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->serviceType:I

    .line 51
    iput-object p3, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    .line 52
    iput-object p6, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    .line 53
    iput-object p4, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mAuth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    .line 54
    iput-object p5, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mCtid:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceType"    # I
    .param p3, "model"    # Lcom/samsung/android/scloud/backup/model/IModel;
    .param p4, "auth"    # Lcom/samsung/android/scloud/backup/auth/AuthManager;
    .param p5, "ctid"    # Ljava/lang/String;
    .param p6, "srcDevice"    # Ljava/lang/String;
    .param p8, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            "Lcom/samsung/android/scloud/backup/auth/AuthManager;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p7, "processedKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/scloud/backup/core/BNRThread;-><init>(Landroid/content/Context;ILcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V

    .line 61
    iput-object p6, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mSrcDevice:Ljava/lang/String;

    .line 62
    iput-object p7, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mProcessedKeyList:Ljava/util/List;

    .line 63
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BNRThread-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BNRThread Started !!  - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :try_start_0
    iget v0, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->serviceType:I

    packed-switch v0, :pswitch_data_0

    .line 93
    :goto_0
    :pswitch_0
    return-void

    .line 72
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mAuth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mCtid:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/core/BNRTask;->backup(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_0
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v7

    .line 90
    .local v7, "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BNRThread-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lcom/samsung/android/scloud/backup/common/BNRException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->serviceType:I

    invoke-interface {v0, v1, v2, v7}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->throwException(Ljava/lang/String;ILcom/samsung/android/scloud/backup/common/BNRException;)V

    goto :goto_0

    .line 77
    .end local v7    # "e":Lcom/samsung/android/scloud/backup/common/BNRException;
    :pswitch_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mModel:Lcom/samsung/android/scloud/backup/model/IModel;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mAuth:Lcom/samsung/android/scloud/backup/auth/AuthManager;

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mCtid:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mSrcDevice:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mProcessedKeyList:Ljava/util/List;

    iget-object v6, p0, Lcom/samsung/android/scloud/backup/core/BNRThread;->mListener:Lcom/samsung/android/scloud/backup/core/IStatusListener;

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/scloud/backup/core/BNRTask;->restore(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;Lcom/samsung/android/scloud/backup/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/samsung/android/scloud/backup/core/IStatusListener;)V
    :try_end_1
    .catch Lcom/samsung/android/scloud/backup/common/BNRException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
