.class public Lcom/samsung/android/scloud/backup/common/BackupSources;
.super Ljava/lang/Object;
.source "BackupSources.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/common/BackupSources$Cids;,
        Lcom/samsung/android/scloud/backup/common/BackupSources$OEMAuthorities;,
        Lcom/samsung/android/scloud/backup/common/BackupSources$Package;,
        Lcom/samsung/android/scloud/backup/common/BackupSources$ResId;,
        Lcom/samsung/android/scloud/backup/common/BackupSources$Key;
    }
.end annotation


# static fields
.field public static final KEYS:[Ljava/lang/String;

.field public static final PACKAGES:[Ljava/lang/String;

.field public static final ResIdArr:[I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 7
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "CALLLOGS"

    aput-object v1, v0, v4

    const-string v1, "MMS"

    aput-object v1, v0, v5

    const-string v1, "SMS"

    aput-object v1, v0, v6

    const-string v1, "HOMESCREEN"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "VIPLIST"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "BLACKLIST"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SPAM"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CALLREJECT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/common/BackupSources;->KEYS:[Ljava/lang/String;

    .line 19
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/scloud/backup/common/BackupSources;->ResIdArr:[I

    .line 31
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "com.android.mms"

    aput-object v1, v0, v4

    const-string v1, "com.android.mms"

    aput-object v1, v0, v5

    const-string v1, "com.android.mms"

    aput-object v1, v0, v6

    const-string v1, "com.samsung.android.scloud.backup"

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const-string v2, "com.android.email"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.android.email"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.android.mms"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.mms"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/scloud/backup/common/BackupSources;->PACKAGES:[Ljava/lang/String;

    return-void

    .line 19
    :array_0
    .array-data 4
        0x7f070003
        0x7f070004
        0x7f070007
        0x7f07000c
        0x7f07000a
        0x7f070000
        0x7f070008
        0x7f070002
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method
