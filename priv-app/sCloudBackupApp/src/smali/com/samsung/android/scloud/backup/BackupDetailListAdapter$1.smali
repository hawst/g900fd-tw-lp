.class Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;
.super Ljava/lang/Object;
.source "BackupDetailListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

.field final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;I)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iput p2, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->val$pos:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 117
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->access$000(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->access$100(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->access$000(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 120
    .local v0, "device":Lcom/samsung/android/scloud/backup/BackupDetails;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 123
    .end local v0    # "device":Lcom/samsung/android/scloud/backup/BackupDetails;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    # getter for: Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->access$100(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)Ljava/util/List;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->val$pos:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 124
    .restart local v0    # "device":Lcom/samsung/android/scloud/backup/BackupDetails;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 125
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    iget v2, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->val$pos:I

    # setter for: Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I
    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->access$002(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;I)I

    .line 126
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;->this$0:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->notifyDataSetChanged()V

    .line 127
    return-void
.end method
