.class public Lcom/samsung/android/scloud/backup/model/ModelManager;
.super Ljava/lang/Object;
.source "ModelManager.java"


# static fields
.field private static INSTANCE:Lcom/samsung/android/scloud/backup/model/ModelManager;


# instance fields
.field private mBNRManagerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/core/BNRManager;",
            ">;"
        }
    .end annotation
.end field

.field private mModelMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/scloud/backup/model/IModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    .line 31
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "CALLLOGS"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Logs;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Logs;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "SMS"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Sms;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Sms;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "MMS"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Mms;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Mms;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "HOMESCREEN"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/WallPaper;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/WallPaper;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "VIPLIST"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/VIPList;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/VIPList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "BLACKLIST"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/BlackList;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/BlackList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "SPAM"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Spam;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Spam;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "CALLREJECT"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/CallReject;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/CallReject;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "REQUEST_GETDETAIL"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Logs;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Logs;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "REQUEST_GETUSAGE"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Logs;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Logs;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v1, "REQUEST_BACKUP_DELETE"

    new-instance v2, Lcom/samsung/android/scloud/backup/model/Logs;

    invoke-direct {v2}, Lcom/samsung/android/scloud/backup/model/Logs;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 47
    const-string v0, "ModelManager"

    const-string v1, "mModelMap is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 49
    const-string v0, "ModelManager"

    const-string v1, "mBNRManagerMap is NULL!!!"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_1
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/scloud/backup/model/ModelManager;
    .locals 3

    .prologue
    .line 19
    const-class v1, Lcom/samsung/android/scloud/backup/model/ModelManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/scloud/backup/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/backup/model/ModelManager;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/samsung/android/scloud/backup/model/ModelManager;

    invoke-direct {v0}, Lcom/samsung/android/scloud/backup/model/ModelManager;-><init>()V

    sput-object v0, Lcom/samsung/android/scloud/backup/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/backup/model/ModelManager;

    .line 22
    :cond_0
    sget-object v0, Lcom/samsung/android/scloud/backup/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/backup/model/ModelManager;

    if-nez v0, :cond_1

    .line 23
    const-string v0, "ModelManager"

    const-string v2, "INSTANCE is NULL!!!"

    invoke-static {v0, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    :cond_1
    sget-object v0, Lcom/samsung/android/scloud/backup/model/ModelManager;->INSTANCE:Lcom/samsung/android/scloud/backup/model/ModelManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized getBNRManager(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/android/scloud/backup/core/BNRManager;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v2, p1, v0}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 61
    const-string v0, "ModelManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mBNRManagerMap.get("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is NULL!!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    new-instance v2, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v2, p1, v0}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/core/BNRManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getModel(Ljava/lang/String;)Lcom/samsung/android/scloud/backup/model/IModel;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/model/IModel;

    return-object v0
.end method

.method public initBNRManager(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "CALLLOGS"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 70
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "CALLLOGS"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "CALLLOGS"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "SMS"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 72
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "SMS"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "SMS"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "MMS"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 74
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "MMS"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "MMS"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "HOMESCREEN"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 76
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "HOMESCREEN"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "HOMESCREEN"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "VIPLIST"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 78
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "VIPLIST"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "VIPLIST"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "BLACKLIST"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 80
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "BLACKLIST"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "BLACKLIST"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "SPAM"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 82
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "SPAM"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "SPAM"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v3, "CALLREJECT"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 84
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    const-string v4, "CALLREJECT"

    new-instance v5, Lcom/samsung/android/scloud/backup/core/BNRManager;

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mModelMap:Ljava/util/Map;

    const-string v6, "CALLREJECT"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-direct {v5, p1, v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;-><init>(Landroid/content/Context;Lcom/samsung/android/scloud/backup/model/IModel;)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/scloud/backup/model/ModelManager;->mBNRManagerMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/core/BNRManager;

    .line 90
    .local v1, "mgr":Lcom/samsung/android/scloud/backup/core/BNRManager;
    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->clearPreRestoredData()V

    goto :goto_0

    .line 91
    .end local v1    # "mgr":Lcom/samsung/android/scloud/backup/core/BNRManager;
    :cond_8
    return-void
.end method
