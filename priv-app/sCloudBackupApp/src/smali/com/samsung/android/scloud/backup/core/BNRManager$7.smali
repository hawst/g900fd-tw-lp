.class Lcom/samsung/android/scloud/backup/core/BNRManager$7;
.super Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;
.source "BNRManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteBackupUsage(Ljava/lang/String;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

.field final synthetic val$currentIndex:I

.field final synthetic val$mDeleteList:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/core/BNRManager;Ljava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    iput-object p2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$mDeleteList:Ljava/util/ArrayList;

    iput p3, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$currentIndex:I

    invoke-direct {p0}, Lcom/samsung/android/scloud/backup/server/KVSResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleKVSResponse(ILorg/json/JSONObject;Lorg/json/JSONArray;)V
    .locals 3
    .param p1, "rCode"    # I
    .param p2, "data"    # Lorg/json/JSONObject;
    .param p3, "list"    # Lorg/json/JSONArray;

    .prologue
    const/4 v2, 0x1

    .line 524
    if-nez p1, :cond_0

    .line 525
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$mDeleteList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$currentIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->setFlag(Ljava/lang/Boolean;)V

    .line 532
    :goto_0
    return-void

    .line 526
    :cond_0
    const/16 v0, 0x4e28

    if-ne p1, v0, :cond_1

    .line 527
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$mDeleteList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$currentIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getModelName()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$mDeleteList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$currentIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->getClientDeviceID()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    # getter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->mAuthManager:Lcom/samsung/android/scloud/backup/auth/AuthManager;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$500(Lcom/samsung/android/scloud/backup/core/BNRManager;)Lcom/samsung/android/scloud/backup/auth/AuthManager;

    move-result-object v2

    invoke-static {v1, v0, v2, p0}, Lcom/samsung/android/scloud/backup/server/BNRServiceManager;->deleteBackupFromServer(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/scloud/backup/auth/AuthManager;Lcom/samsung/android/scloud/backup/core/network/NetworkUtil$StringResponseHandler;)V

    goto :goto_0

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$mDeleteList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->val$currentIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/showbackup/BackupDetails;->setFlag(Ljava/lang/Boolean;)V

    .line 530
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRManager$7;->this$0:Lcom/samsung/android/scloud/backup/core/BNRManager;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/core/BNRManager;->deleteFailed:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/core/BNRManager;->access$1002(Lcom/samsung/android/scloud/backup/core/BNRManager;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0
.end method
