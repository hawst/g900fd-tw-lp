.class Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;
.super Ljava/lang/Object;
.source "ManualRestoreActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->processRestoreReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)V
    .locals 0

    .prologue
    .line 1062
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1064
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsForBackedUpDeviceList(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$902(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/List;)Ljava/util/List;

    .line 1065
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1066
    :cond_0
    const-string v0, "ManualRestoreActivity"

    const-string v1, "onCreate: No Restore List Found in the DB"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1085
    :goto_0
    return-void

    .line 1070
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v0, v4}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1202(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;I)I

    .line 1072
    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$900(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1200(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceID()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1302(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1074
    const-string v0, "ManualRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[processRestoreReady] mTargetDeviceId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mTargetDeviceId:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1300(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->list:Lorg/json/JSONArray;
    invoke-static {v3}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1000(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getAvailableBackupsByDeviceId(Ljava/lang/String;Lorg/json/JSONArray;)Ljava/util/HashMap;

    move-result-object v1

    # setter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1402(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 1076
    const-string v0, "ManualRestoreActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[processRestoreReady] mBackupDetailsMap : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1078
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1080
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mBackupDetailsMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1400(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 1081
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity;->access$1100(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1083
    :cond_3
    new-instance v0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$10;->this$0:Lcom/samsung/android/scloud/backup/ManualRestoreActivity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;-><init>(Lcom/samsung/android/scloud/backup/ManualRestoreActivity;Lcom/samsung/android/scloud/backup/ManualRestoreActivity$1;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/ManualRestoreActivity$ChecksameDeviceBackupExists;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method
