.class final Lcom/samsung/android/scloud/backup/core/BNRTaskService$3;
.super Ljava/lang/Object;
.source "BNRTaskService.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/BNRTaskService$BNRServiceHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/core/BNRTaskService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleServiceAction(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    const-string v1, "SOURCE_LIST"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 122
    .local v9, "sourceList":[Ljava/lang/String;
    const-string v1, "TRIGGER"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 123
    .local v4, "trigger":Ljava/lang/String;
    const-string v1, "SOURCE_DEVICE"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 124
    .local v5, "srcDevice":Ljava/lang/String;
    move-object v6, v9

    .local v6, "arr$":[Ljava/lang/String;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v2, v6, v7

    .line 125
    .local v2, "source":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRTaskService$3$1;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/scloud/backup/core/BNRTaskService$3$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRTaskService$3;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .local v0, "runnable":Ljava/lang/Runnable;
    new-instance v10, Ljava/lang/Thread;

    const-string v1, "REQUEST_RESTORE"

    invoke-direct {v10, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 139
    .local v10, "thread":Ljava/lang/Thread;
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 124
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 142
    .end local v0    # "runnable":Ljava/lang/Runnable;
    .end local v2    # "source":Ljava/lang/String;
    .end local v10    # "thread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method
