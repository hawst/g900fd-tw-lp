.class public interface abstract Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms$Addr;
.super Ljava/lang/Object;
.source "MmsContract.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/modelbuilder/BaseBackupContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/scloud/backup/modelbuilder/MmsContract$Mms;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Addr"
.end annotation


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "address"

.field public static final CHARSET:Ljava/lang/String; = "charset"

.field public static final MSG_ID:Ljava/lang/String; = "msg_id"

.field public static final TYPE:Ljava/lang/String; = "type"
