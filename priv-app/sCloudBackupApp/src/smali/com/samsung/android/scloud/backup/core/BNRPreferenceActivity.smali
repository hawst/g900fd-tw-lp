.class public abstract Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "BNRPreferenceActivity.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/core/IBNRContext;


# instance fields
.field protected ActivityHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 65
    new-instance v0, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity$1;-><init>(Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;)V

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->ActivityHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onDestory()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/scloud/backup/core/BackupApp;->removeHandlerActivity(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 43
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->getTargetEventServiceCodes()[I

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/core/BNRPreferenceActivity;->ActivityHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/BackupApp;->setHandlerActivity(Ljava/lang/String;[ILandroid/os/Handler;)V

    .line 47
    return-void
.end method
