.class Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;
.super Ljava/lang/Object;
.source "SetupWizardRestoreActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->showSelectDeviceDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 534
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->getCurPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 535
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mBackupDetailsList:Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$800(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetails;->setItemChecked(Z)V

    .line 536
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mAdapter:Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    invoke-static {v0}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$600(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity$5;->this$0:Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;

    # getter for: Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->mSelectedDeviceIndex:I
    invoke-static {v1}, Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;->access$500(Lcom/samsung/android/scloud/backup/SetupWizardRestoreActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->setfoucsPosition(I)V

    .line 537
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    .line 538
    return-void
.end method
