.class public Lcom/samsung/android/scloud/backup/model/WallPaper;
.super Ljava/lang/Object;
.source "WallPaper.java"

# interfaces
.implements Lcom/samsung/android/scloud/backup/model/IModel$IHasFile;
.implements Lcom/samsung/android/scloud/backup/model/IModel;


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.sec.android.sCloudWallpaperBackupProvider"

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final SOURCE_KEY:Ljava/lang/String; = "HOMESCREEN"


# instance fields
.field private SOURCE_RES_ID:I

.field private cid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "content://com.sec.android.sCloudWallpaperBackupProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/scloud/backup/model/WallPaper;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const v0, 0x7f07000c

    iput v0, p0, Lcom/samsung/android/scloud/backup/model/WallPaper;->SOURCE_RES_ID:I

    .line 42
    const-string v0, "zMxn0UENnJ"

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/model/WallPaper;->cid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAlternatePathParameter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    const-string v0, ""

    return-object v0
.end method

.method public getCid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/model/WallPaper;->cid:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailKey(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BACKUP_HOMESCREEN_DETAILS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "detailKey":Ljava/lang/String;
    return-object v0
.end method

.method public getLocalFilePathPrefix(Landroid/content/Context;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/samsung/android/scloud/backup/data/BNRItem;

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOEMControl()Lcom/samsung/android/scloud/backup/core/IOEMControl;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/samsung/android/scloud/backup/oem/InternalOEMControl;->getInstance()Lcom/samsung/android/scloud/backup/core/IOEMControl;

    move-result-object v0

    return-object v0
.end method

.method public getOemAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const-string v0, "com.sec.android.sCloudWallpaperBackupProvider"

    return-object v0
.end method

.method public getOemContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/scloud/backup/model/WallPaper;->CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method public getServerFilePathPrefix(Ljava/lang/String;Lcom/samsung/android/scloud/backup/data/BNRItem;)Ljava/lang/String;
    .locals 2
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/android/scloud/backup/data/BNRItem;

    .prologue
    .line 79
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const-string v0, "/mnt/sdcard/Wallpaper/"

    .line 82
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/mnt/sdcard/Wallpaper/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSourceKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    const-string v0, "HOMESCREEN"

    return-object v0
.end method

.method public getSourceResId()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/android/scloud/backup/model/WallPaper;->SOURCE_RES_ID:I

    return v0
.end method

.method public isAlternateDownloadPathAvailable()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    return v0
.end method

.method public parseToBNRItem(Lorg/json/JSONObject;)Lcom/samsung/android/scloud/backup/data/BNRItem;
    .locals 7
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    const/4 v6, 0x0

    .line 110
    new-instance v1, Lcom/samsung/android/scloud/backup/data/BNRItem;

    const-string v3, "key"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 111
    .local v1, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    const-string v3, "size"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    const-string v3, "size"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 115
    :goto_0
    invoke-virtual {v1, p1}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setData(Lorg/json/JSONObject;)V

    .line 117
    const/4 v2, 0x0

    .line 119
    .local v2, "name":Ljava/lang/String;
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    const-string v4, "value"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "HOMESCREEN"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "name"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 124
    :goto_1
    const-string v3, "Home_Screen_WallPaper"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 125
    const-string v3, "wallpaper"

    invoke-virtual {v1, v3, v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_0
    :goto_2
    return-object v1

    .line 114
    .end local v2    # "name":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    goto :goto_0

    .line 120
    .restart local v2    # "name":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {p0}, Lcom/samsung/android/scloud/backup/model/WallPaper;->getSourceKey()Ljava/lang/String;

    move-result-object v3

    const-string v4, "parseToBNRItem err"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 126
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    const-string v3, "lockscreen_wallpaper"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    const-string v3, "lockscreen_wallpaper"

    invoke-virtual {v1, v3, v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v3, "lockscreen_wallpaper_ripple"

    invoke-virtual {v1, v3, v6}, Lcom/samsung/android/scloud/backup/data/BNRItem;->putAttachmentFileInfo(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
