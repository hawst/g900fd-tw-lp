.class public Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BackupDetailListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private ctxt:Landroid/content/Context;

.field private elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

.field private prevpos:I


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctxt"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/BackupDetails;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "elements":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/BackupDetails;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I

    .line 53
    iput-object p1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;

    .line 54
    iput-object p2, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->ctxt:Landroid/content/Context;

    .line 55
    invoke-static {p2}, Lcom/samsung/android/scloud/backup/common/MetaManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/scloud/backup/common/MetaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->mMetaManager:Lcom/samsung/android/scloud/backup/common/MetaManager;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurPosition()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 78
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 83
    move v2, p1

    .line 84
    .local v2, "pos":I
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->elements:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/scloud/backup/BackupDetails;

    .line 87
    .local v0, "device":Lcom/samsung/android/scloud/backup/BackupDetails;
    if-nez p2, :cond_4

    .line 88
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_K()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 89
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->ctxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030012

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/RelativeLayout;

    .line 101
    .restart local p2    # "convertView":Landroid/view/View;
    :goto_0
    new-instance v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;-><init>()V

    .line 102
    .local v1, "holder":Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;
    const v3, 0x7f09001c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->devicename:Landroid/widget/TextView;

    .line 103
    const v3, 0x7f09001d

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->date:Landroid/widget/TextView;

    .line 104
    const v3, 0x7f09001b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->rdbtn:Landroid/widget/RadioButton;

    .line 105
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 110
    :goto_1
    invoke-virtual {p2, v6}, Landroid/view/View;->setClickable(Z)V

    .line 111
    invoke-virtual {p2, v6, v5}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 112
    iget-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->rdbtn:Landroid/widget/RadioButton;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 113
    iget-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->rdbtn:Landroid/widget/RadioButton;

    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 114
    new-instance v3, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;

    invoke-direct {v3, p0, v2}, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$1;-><init>(Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;I)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->devicename:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->deviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->date:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->getTimestampInTimeFormat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v3, v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;->rdbtn:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Lcom/samsung/android/scloud/backup/BackupDetails;->isItemChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 134
    return-object p2

    .line 91
    .end local v1    # "holder":Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;
    :cond_0
    invoke-static {}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->isViewType_T()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->ctxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030013

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/RelativeLayout;

    .restart local p2    # "convertView":Landroid/view/View;
    goto :goto_0

    .line 95
    :cond_1
    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_CHAGALL:Z

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/samsung/android/scloud/backup/common/BackupConstants$DeviceList;->IS_DEVICE_KLIMT:Z

    if-eqz v3, :cond_3

    .line 96
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->ctxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030011

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/RelativeLayout;

    .restart local p2    # "convertView":Landroid/view/View;
    goto/16 :goto_0

    .line 99
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->ctxt:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030010

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    check-cast p2, Landroid/widget/RelativeLayout;

    .restart local p2    # "convertView":Landroid/view/View;
    goto/16 :goto_0

    .line 108
    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;

    .restart local v1    # "holder":Lcom/samsung/android/scloud/backup/BackupDetailListAdapter$ViewHolder;
    goto :goto_1
.end method

.method public setfoucsPosition(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/samsung/android/scloud/backup/BackupDetailListAdapter;->prevpos:I

    .line 74
    return-void
.end method
