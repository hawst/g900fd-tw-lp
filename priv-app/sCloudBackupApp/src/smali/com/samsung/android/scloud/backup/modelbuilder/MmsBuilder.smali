.class public Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;
.super Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;
.source "MmsBuilder.java"


# static fields
.field private static final RECIPIENT:Ljava/lang/String; = "recipient"

.field static final TAG:Ljava/lang/String; = "MMSBuilder"


# direct methods
.method public constructor <init>(Lcom/samsung/android/scloud/backup/model/IModel;)V
    .locals 0
    .param p1, "mModel"    # Lcom/samsung/android/scloud/backup/model/IModel;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/samsung/android/scloud/backup/modelbuilder/IBuilder;-><init>(Lcom/samsung/android/scloud/backup/model/IModel;)V

    .line 83
    return-void
.end method

.method private getJSONFromMmsAttachment(Landroid/content/Context;Landroid/database/Cursor;)Lorg/json/JSONObject;
    .locals 20
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "partCursor"    # Landroid/database/Cursor;

    .prologue
    .line 276
    const-string v18, "MMSBuilder"

    const-string v19, "getJSONFromMmsAttachment() is called "

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v18, "_id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 278
    .local v8, "idIndex":I
    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 279
    .local v14, "partId":J
    const/4 v13, 0x0

    .line 281
    .local v13, "partTable":Lorg/json/JSONObject;
    :try_start_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v18

    if-eqz v18, :cond_0

    .line 282
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 289
    :cond_0
    :goto_0
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 290
    .local v11, "jsonObj":Lorg/json/JSONObject;
    const/4 v10, 0x0

    .line 293
    .local v10, "is":Ljava/io/InputStream;
    :try_start_1
    const-string v18, "_data"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    if-nez v18, :cond_2

    .line 295
    const/4 v11, 0x0

    .line 348
    .end local v11    # "jsonObj":Lorg/json/JSONObject;
    if-eqz v10, :cond_1

    .line 350
    :try_start_2
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 356
    :cond_1
    :goto_1
    const/4 v10, 0x0

    .line 359
    :goto_2
    return-object v11

    .line 284
    .end local v10    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v6

    .line 286
    .local v6, "e1":Lorg/json/JSONException;
    invoke-virtual {v6}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 351
    .end local v6    # "e1":Lorg/json/JSONException;
    .restart local v10    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v5

    .line 353
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 297
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v11    # "jsonObj":Lorg/json/JSONObject;
    :cond_2
    :try_start_3
    const-string v18, "_data"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v4

    .line 299
    .local v4, "datapath":Ljava/lang/String;
    if-nez v4, :cond_4

    .line 301
    const/4 v11, 0x0

    .line 348
    .end local v11    # "jsonObj":Lorg/json/JSONObject;
    if-eqz v10, :cond_3

    .line 350
    :try_start_4
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 356
    :cond_3
    :goto_3
    const/4 v10, 0x0

    goto :goto_2

    .line 351
    :catch_2
    move-exception v5

    .line 353
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 303
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v11    # "jsonObj":Lorg/json/JSONObject;
    :cond_4
    :try_start_5
    const-string v18, "PART_"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v9

    .line 305
    .local v9, "index":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v9, v0, :cond_6

    .line 307
    const/4 v11, 0x0

    .line 348
    .end local v11    # "jsonObj":Lorg/json/JSONObject;
    if-eqz v10, :cond_5

    .line 350
    :try_start_6
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 356
    :cond_5
    :goto_4
    const/4 v10, 0x0

    goto :goto_2

    .line 351
    :catch_3
    move-exception v5

    .line 353
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 309
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v11    # "jsonObj":Lorg/json/JSONObject;
    :cond_6
    :try_start_7
    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "Filename":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "content://mms/part/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 313
    .local v16, "partURI":Landroid/net/Uri;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v10

    .line 315
    new-instance v17, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 317
    .local v17, "totalbuffer":Ljava/io/ByteArrayOutputStream;
    const/16 v18, 0x100

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 319
    .local v3, "buffer":[B
    invoke-virtual {v10, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    .line 321
    .local v12, "len":I
    :goto_5
    if-ltz v12, :cond_7

    .line 323
    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1, v12}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 325
    invoke-virtual {v10, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    goto :goto_5

    .line 328
    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 330
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v18

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    .line 333
    .local v7, "encodedata":Ljava/lang/String;
    invoke-virtual {v11, v2, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 348
    if-eqz v10, :cond_8

    .line 350
    :try_start_8
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 356
    :cond_8
    :goto_6
    const/4 v10, 0x0

    .line 357
    goto/16 :goto_2

    .line 351
    :catch_4
    move-exception v5

    .line 353
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 336
    .end local v2    # "Filename":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "datapath":Ljava/lang/String;
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "encodedata":Ljava/lang/String;
    .end local v9    # "index":I
    .end local v12    # "len":I
    .end local v16    # "partURI":Landroid/net/Uri;
    .end local v17    # "totalbuffer":Ljava/io/ByteArrayOutputStream;
    :catch_5
    move-exception v5

    .line 338
    .local v5, "e":Ljava/io/FileNotFoundException;
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 348
    if-eqz v10, :cond_9

    .line 350
    :try_start_a
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 356
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :cond_9
    :goto_7
    const/4 v10, 0x0

    .line 357
    goto/16 :goto_2

    .line 351
    .restart local v5    # "e":Ljava/io/FileNotFoundException;
    :catch_6
    move-exception v5

    .line 353
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 340
    .end local v5    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v5

    .line 342
    .restart local v5    # "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 348
    if-eqz v10, :cond_a

    .line 350
    :try_start_c
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    .line 356
    :cond_a
    :goto_8
    const/4 v10, 0x0

    .line 357
    goto/16 :goto_2

    .line 351
    :catch_8
    move-exception v5

    .line 353
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 344
    .end local v5    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v5

    .line 346
    .local v5, "e":Lorg/json/JSONException;
    :try_start_d
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 348
    if-eqz v10, :cond_b

    .line 350
    :try_start_e
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 356
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_b
    :goto_9
    const/4 v10, 0x0

    .line 357
    goto/16 :goto_2

    .line 351
    .restart local v5    # "e":Lorg/json/JSONException;
    :catch_a
    move-exception v5

    .line 353
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 348
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    if-eqz v10, :cond_c

    .line 350
    :try_start_f
    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 356
    :cond_c
    :goto_a
    const/4 v10, 0x0

    throw v18

    .line 351
    :catch_b
    move-exception v5

    .line 353
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_a
.end method

.method private getMmsAttachment(Landroid/content/Context;ILjava/lang/String;Ljava/util/HashMap;)V
    .locals 10
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "partId"    # I
    .param p3, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 585
    .local p4, "mmsDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v8, "MMSBuilder"

    const-string v9, "getMmsAttachment() is called"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "content://mms/part/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 590
    .local v7, "partURI":Landroid/net/Uri;
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 593
    :cond_1
    const/4 v6, 0x0

    .line 595
    .local v6, "os":Ljava/io/OutputStream;
    const-string v8, "PART_"

    invoke-virtual {p3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 597
    .local v5, "index":I
    const/4 v8, -0x1

    if-eq v5, v8, :cond_0

    .line 601
    invoke-virtual {p3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 603
    .local v0, "Filename":Ljava/lang/String;
    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 607
    .local v4, "encodedata":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 611
    const/4 v8, 0x0

    :try_start_0
    invoke-static {v4, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 613
    .local v1, "buffer":[B
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v6

    .line 615
    invoke-virtual {v6, v1}, Ljava/io/OutputStream;->write([B)V

    .line 617
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 621
    .end local v1    # "buffer":[B
    :catch_0
    move-exception v3

    .line 622
    .local v3, "e1":Ljava/io/FileNotFoundException;
    const-string v8, "MMSBuilder"

    const-string v9, "getMmsAttachment() : fileNotfound.."

    invoke-static {v8, v9, v3}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 624
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 626
    .end local v3    # "e1":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 628
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "MMSBuilder"

    const-string v9, "getMmsAttachment() : IOException.."

    invoke-static {v8, v9, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 629
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private hasPendingEntry(Lorg/json/JSONArray;JLandroid/content/ContentValues;)Z
    .locals 6
    .param p1, "pendingJson"    # Lorg/json/JSONArray;
    .param p2, "backedupMsgId"    # J
    .param p4, "CV"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1000
    if-eqz p1, :cond_0

    .line 1002
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    .line 1003
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 1004
    invoke-virtual {p1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 1005
    .local v1, "obj":Lorg/json/JSONObject;
    const-string v2, "msg_type"

    const-string v3, "msg_type"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    const-string v2, "proto_type"

    const-string v3, "proto_type"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1011
    const-string v2, "err_code"

    const-string v3, "err_code"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1014
    const-string v2, "err_type"

    const-string v3, "err_type"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1017
    const-string v2, "due_time"

    const-string v3, "due_time"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1020
    const-string v2, "retry_index"

    const-string v3, "retry_index"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1023
    const-string v2, "last_try"

    const-string v3, "last_try"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p4, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1025
    const/4 v2, 0x1

    .line 1028
    .end local v0    # "count":I
    .end local v1    # "obj":Lorg/json/JSONObject;
    :cond_0
    return v2
.end method

.method private insert(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Z
    .locals 11
    .param p1, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p3, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 1033
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    .line 1034
    .local v6, "size":I
    const-string v8, "MMSBuilder"

    const-string v9, "insert() is called  "

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v6, :cond_3

    .line 1036
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/scloud/backup/data/BNRItem;

    .line 1037
    .local v2, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    invoke-virtual {v2}, Lcom/samsung/android/scloud/backup/data/BNRItem;->getData()Lorg/json/JSONObject;

    move-result-object v5

    .line 1038
    .local v5, "jsonData":Lorg/json/JSONObject;
    const/4 v3, 0x0

    .line 1041
    .local v3, "json":Lorg/json/JSONObject;
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    const-string v8, "value"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "json":Lorg/json/JSONObject;
    .local v4, "json":Lorg/json/JSONObject;
    move-object v3, v4

    .line 1048
    .end local v4    # "json":Lorg/json/JSONObject;
    .restart local v3    # "json":Lorg/json/JSONObject;
    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONObject;->length()I

    move-result v8

    if-nez v8, :cond_1

    .line 1049
    :cond_0
    const-string v8, "MMSBuilder"

    const-string v9, "insert() VALUE is null or 0"

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    .end local v2    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v3    # "json":Lorg/json/JSONObject;
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :goto_2
    return v7

    .line 1043
    .restart local v2    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .restart local v3    # "json":Lorg/json/JSONObject;
    .restart local v5    # "jsonData":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 1045
    .local v0, "e":Lorg/json/JSONException;
    const-string v8, "MMSBuilder"

    const-string v9, "JsonObject.getJSONArray(MmsContract.Mms.VALUE)"

    invoke-static {v8, v9, v0}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1046
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1

    .line 1052
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    invoke-virtual {p0, v3, p1, p3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->restoreBackedDataToPhone(Lorg/json/JSONObject;Landroid/content/Context;Ljava/util/List;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1053
    const-string v8, "MMSBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "restoreBackedDataToPhone() is failed : item index :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1058
    :cond_2
    const-string v8, "MMSBuilder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "restoreBackedDataToPhone() is SUCCESS : item index :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1061
    .end local v2    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v3    # "json":Lorg/json/JSONObject;
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :cond_3
    const/4 v7, 0x1

    goto :goto_2
.end method

.method private parse(Landroid/content/Context;Landroid/database/Cursor;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 1
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "mmsCursor"    # Landroid/database/Cursor;
    .param p3, "parsedJSON"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 127
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->parseMMS(Landroid/database/Cursor;Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "msgId":Ljava/lang/String;
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->parseAddr(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 133
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->parsePart(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 136
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->parsePending(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 138
    return-object p3
.end method

.method private parseAddr(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 11
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "msgId"    # Ljava/lang/String;
    .param p3, "parsedJSON"    # Lorg/json/JSONObject;

    .prologue
    const/4 v2, 0x0

    .line 158
    const/4 v6, 0x0

    .line 159
    .local v6, "addrCursor":Landroid/database/Cursor;
    const-string v0, "MMSBuilder"

    const-string v1, "parseAddr() is called "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 161
    .local v7, "addrList":Lorg/json/JSONArray;
    const-string v10, "msg_id= ? "

    .line 162
    .local v10, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    .line 163
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v1}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "addr"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "msg_id= ? "

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 168
    if-eqz v6, :cond_0

    .line 169
    const/4 v8, 0x0

    .line 173
    .local v8, "addrTable":Lorg/json/JSONObject;
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 174
    invoke-static {v6}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 175
    const-string v0, "transaction_id"

    const-string v1, "0"

    invoke-virtual {v8, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 177
    invoke-virtual {v8}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 178
    const-string v0, "MMSBuilder"

    const-string v1, "GetReminders: Unable to parse "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 185
    :catch_0
    move-exception v9

    .line 187
    .local v9, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v0, "MMSBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==getAddress: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 193
    .end local v8    # "addrTable":Lorg/json/JSONObject;
    .end local v9    # "e":Lorg/json/JSONException;
    :cond_0
    :goto_1
    return-void

    .line 180
    .restart local v8    # "addrTable":Lorg/json/JSONObject;
    :cond_1
    :try_start_2
    invoke-virtual {v7, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 183
    :cond_2
    :try_start_3
    const-string v0, "ADDRESS"

    invoke-virtual {p3, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private parseMMS(Landroid/database/Cursor;Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 5
    .param p1, "mmsCursor"    # Landroid/database/Cursor;
    .param p2, "parsedJSON"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 143
    const-string v3, "MMSBuilder"

    const-string v4, "parseMMS() is called "

    invoke-static {v3, v4}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v0

    .line 146
    .local v0, "MMSTable":Lorg/json/JSONObject;
    const-string v3, "transaction_id"

    const-string v4, "0"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 147
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 148
    .local v1, "idIndex":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "sourceId":Ljava/lang/String;
    const-string v3, "source_id"

    invoke-virtual {v0, v3, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 152
    const-string v3, "MMS"

    invoke-virtual {p2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 153
    return-object v2
.end method

.method private parsePart(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 14
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "msgId"    # Ljava/lang/String;
    .param p3, "parsedJSON"    # Lorg/json/JSONObject;

    .prologue
    .line 196
    const-string v1, "MMSBuilder"

    const-string v2, "parsePart() is called "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const/4 v10, 0x0

    .line 198
    .local v10, "partCursor":Landroid/database/Cursor;
    const-string v13, "mid= ?"

    .line 199
    .local v13, "selection":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v5, v1

    .line 200
    .local v5, "selectionArgs":[Ljava/lang/String;
    new-instance v11, Lorg/json/JSONArray;

    invoke-direct {v11}, Lorg/json/JSONArray;-><init>()V

    .line 201
    .local v11, "partList":Lorg/json/JSONArray;
    new-instance v7, Lorg/json/JSONArray;

    invoke-direct {v7}, Lorg/json/JSONArray;-><init>()V

    .line 203
    .local v7, "datalist":Lorg/json/JSONArray;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v2}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "part"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "mid= ?"

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 208
    if-eqz v10, :cond_1

    .line 209
    const/4 v12, 0x0

    .line 212
    .local v12, "partTable":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 214
    invoke-static {v10}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v12

    .line 215
    const-string v1, "transaction_id"

    const-string v2, "0"

    invoke-virtual {v12, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    invoke-virtual {v12}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 218
    const-string v1, "MMSBuilder"

    const-string v2, "GetReminders: Unable to parse "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v8

    .line 234
    .local v8, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v1, "MMSBuilder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "==getPART: Unable to parse: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "===="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v8}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 237
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 240
    .end local v8    # "e":Lorg/json/JSONException;
    .end local v12    # "partTable":Lorg/json/JSONObject;
    :cond_1
    :goto_1
    return-void

    .line 220
    .restart local v12    # "partTable":Lorg/json/JSONObject;
    :cond_2
    :try_start_2
    invoke-virtual {v11, v12}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 221
    const/4 v9, 0x0

    .line 222
    .local v9, "jsondata":Lorg/json/JSONObject;
    invoke-direct {p0, p1, v10}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getJSONFromMmsAttachment(Landroid/content/Context;Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v9

    .line 224
    if-eqz v9, :cond_0

    .line 225
    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 226
    const-string v1, "MMSBuilder"

    const-string v2, "dataValue is inserting "

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 237
    .end local v9    # "jsondata":Lorg/json/JSONObject;
    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    .line 230
    :cond_3
    :try_start_3
    const-string v1, "PART"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    const-string v1, "DataValue"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 237
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private parsePending(Landroid/content/Context;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "msgId"    # Ljava/lang/String;
    .param p3, "parsedJSON"    # Lorg/json/JSONObject;

    .prologue
    const/4 v2, 0x0

    .line 244
    const-string v0, "MMSBuilder"

    const-string v1, "parsePending() is called "

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const/4 v8, 0x0

    .line 246
    .local v8, "pendingJson":Lorg/json/JSONObject;
    const/4 v7, 0x0

    .line 248
    .local v7, "pendingCursor":Landroid/database/Cursor;
    const-string v9, "msg_id =? "

    .line 250
    .local v9, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v4, v0

    .line 252
    .local v4, "selectionArgs":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/scloud/backup/model/Mms;->TARGET_PENDING_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "msg_id =? "

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 256
    if-eqz v7, :cond_1

    .line 258
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    invoke-static {v7}, Lcom/samsung/android/scloud/backup/modelbuilder/JSONParser;->toJSON(Landroid/database/Cursor;)Lorg/json/JSONObject;

    move-result-object v8

    .line 260
    invoke-virtual {v8}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 261
    const-string v0, "PENDING"

    invoke-virtual {p3, v0, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 272
    :cond_1
    :goto_0
    return-void

    .line 264
    :catch_0
    move-exception v6

    .line 265
    .local v6, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v0, "MMSBuilder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "==Get Pending: Unable to parse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v6    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public backupCleared(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1086
    return-void
.end method

.method public getItemFromOEM(Landroid/content/Context;Ljava/util/List;IIJLjava/lang/String;)Ljava/util/List;
    .locals 37
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "start"    # I
    .param p4, "maxCount"    # I
    .param p5, "maxSize"    # J
    .param p7, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IIJ",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 368
    .local p2, "serverkey":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "MMSBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getItemFromOEM() is called!!! : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v20, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    const/16 v23, 0x0

    .line 371
    .local v23, "mmsCursor":Landroid/database/Cursor;
    const/4 v15, 0x0

    .line 373
    .local v15, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v16, Ljava/io/FileWriter;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p7

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v6}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 374
    .end local v15    # "fw":Ljava/io/FileWriter;
    .local v16, "fw":Ljava/io/FileWriter;
    :try_start_1
    const-string v4, "["

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 376
    const-wide/16 v10, 0x0

    .line 377
    .local v10, "count":J
    const-wide/16 v32, 0x0

    .line 378
    .local v32, "totalLength":J
    const/16 v22, 0x1f4

    .line 379
    .local v22, "limit":I
    const/16 v18, 0x1

    .line 381
    .local v18, "isNext":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v4}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 382
    .local v5, "oemUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getOrderByColumnName()Ljava/lang/String;

    move-result-object v25

    .line 383
    .local v25, "sortOrder":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getIDKey(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getWhereKey([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 384
    .local v34, "whereKey":Ljava/lang/String;
    :cond_0
    :goto_0
    if-eqz v18, :cond_1

    .line 385
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getSelectionID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v34

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v35, " ASC"

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v35, " LIMIT "

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v35, " OFFSET "

    move-object/from16 v0, v35

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 392
    if-eqz v23, :cond_0

    .line 393
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_4

    .line 394
    const/16 v18, 0x0

    .line 395
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_1
    const-string v4, "]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 474
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V

    .line 475
    const-string v4, "MMSBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getItemFromOEM is Done !! : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " returned item count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", totalLength : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 486
    if-eqz v23, :cond_2

    .line 487
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_2
    if-eqz v16, :cond_3

    .line 490
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 496
    :cond_3
    :goto_1
    return-object v20

    .line 398
    :cond_4
    :try_start_3
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int p3, p3, v4

    .line 400
    :goto_2
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 401
    move/from16 v0, p4

    int-to-long v6, v0

    cmp-long v4, v10, v6

    if-ltz v4, :cond_7

    .line 402
    if-eqz v23, :cond_5

    .line 403
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 404
    const/16 v23, 0x0

    .line 406
    :cond_5
    const-string v4, "MMSBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getItemFromOEM is Done !! : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v7}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " returned item count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", totalLength : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const-string v4, "]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 412
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 486
    if-eqz v23, :cond_6

    .line 487
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_6
    if-eqz v16, :cond_3

    .line 490
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 491
    :catch_0
    move-exception v13

    .line 493
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 416
    .end local v13    # "e":Ljava/io/IOException;
    :cond_7
    :try_start_5
    new-instance v24, Lorg/json/JSONObject;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONObject;-><init>()V

    .line 417
    .local v24, "parsedJSON":Lorg/json/JSONObject;
    const-string v4, "_id"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 419
    .local v17, "idIndex":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->parse(Landroid/content/Context;Landroid/database/Cursor;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 421
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BACKUP_MMS_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->getClientDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 426
    .local v21, "itemkey":Ljava/lang/String;
    const-string v4, "transaction_id"

    const-string v6, "0"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 427
    new-instance v31, Lorg/json/JSONObject;

    invoke-direct/range {v31 .. v31}, Lorg/json/JSONObject;-><init>()V

    .line 428
    .local v31, "totalData":Lorg/json/JSONObject;
    const-string v4, "value"

    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 430
    const-string v4, "date"

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 431
    .local v12, "dateIndex":I
    move-object/from16 v0, v23

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 432
    .local v28, "timeStamp":J
    const-wide/16 v6, 0x0

    cmp-long v4, v28, v6

    if-gtz v4, :cond_8

    .line 433
    const-wide v28, 0xe8d4a51000L

    .line 434
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getLength(J)I

    move-result v4

    const/16 v6, 0xd

    if-ge v4, v6, :cond_9

    .line 435
    const-wide/16 v6, 0xa

    mul-long v28, v28, v6

    goto :goto_3

    .line 438
    :cond_9
    const-string v4, "timestamp"

    move-object/from16 v0, v31

    move-wide/from16 v1, v28

    invoke-virtual {v0, v4, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 439
    const-string v4, "key"

    move-object/from16 v0, v31

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 440
    const-wide/16 v26, 0x0

    .line 441
    .local v26, "subLength":J
    invoke-virtual/range {v31 .. v31}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v30

    .line 442
    .local v30, "toStr":Ljava/lang/String;
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v26, v0

    .line 444
    add-long v6, v32, v26

    cmp-long v4, v6, p5

    if-ltz v4, :cond_b

    .line 445
    const-string v4, "MMSBuilder"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getItemFromOEM() is Done : return Item count is :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " , totalLength : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v4, "]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 450
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V

    .line 451
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 452
    const/16 v23, 0x0

    .line 486
    if-eqz v23, :cond_a

    .line 487
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_a
    if-eqz v16, :cond_3

    .line 490
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_1

    .line 491
    :catch_1
    move-exception v13

    .line 493
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 457
    .end local v13    # "e":Ljava/io/IOException;
    :cond_b
    add-long v32, v32, v26

    .line 458
    :try_start_7
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_d

    const-wide/16 v6, 0x0

    cmp-long v4, v10, v6

    if-lez v4, :cond_d

    .line 459
    :cond_c
    const-string v4, ","

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 461
    :cond_d
    move-object/from16 v0, v16

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 462
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileWriter;->flush()V

    .line 463
    new-instance v19, Lcom/samsung/android/scloud/backup/data/BNRItem;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/samsung/android/scloud/backup/data/BNRItem;-><init>(Ljava/lang/String;)V

    .line 464
    .local v19, "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    move-object/from16 v0, v19

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/data/BNRItem;->setSize(J)V

    .line 465
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    const-wide/16 v6, 0x1

    add-long/2addr v10, v6

    .line 468
    goto/16 :goto_2

    .line 469
    .end local v12    # "dateIndex":I
    .end local v17    # "idIndex":I
    .end local v19    # "item":Lcom/samsung/android/scloud/backup/data/BNRItem;
    .end local v21    # "itemkey":Ljava/lang/String;
    .end local v24    # "parsedJSON":Lorg/json/JSONObject;
    .end local v26    # "subLength":J
    .end local v28    # "timeStamp":J
    .end local v30    # "toStr":Ljava/lang/String;
    .end local v31    # "totalData":Lorg/json/JSONObject;
    :cond_e
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 470
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 491
    :catch_2
    move-exception v13

    .line 493
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 480
    .end local v5    # "oemUri":Landroid/net/Uri;
    .end local v10    # "count":J
    .end local v13    # "e":Ljava/io/IOException;
    .end local v16    # "fw":Ljava/io/FileWriter;
    .end local v18    # "isNext":Z
    .end local v22    # "limit":I
    .end local v25    # "sortOrder":Ljava/lang/String;
    .end local v32    # "totalLength":J
    .end local v34    # "whereKey":Ljava/lang/String;
    .restart local v15    # "fw":Ljava/io/FileWriter;
    :catch_3
    move-exception v14

    .line 481
    .local v14, "e1":Ljava/io/IOException;
    :goto_4
    :try_start_8
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x13a

    invoke-direct {v4, v6, v14}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 486
    .end local v14    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_5
    if-eqz v23, :cond_f

    .line 487
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_f
    if-eqz v15, :cond_10

    .line 490
    :try_start_9
    invoke-virtual {v15}, Ljava/io/FileWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 494
    :cond_10
    :goto_6
    throw v4

    .line 482
    :catch_4
    move-exception v13

    .line 483
    .local v13, "e":Lorg/json/JSONException;
    :goto_7
    :try_start_a
    new-instance v4, Lcom/samsung/android/scloud/backup/common/BNRException;

    const/16 v6, 0x130

    invoke-direct {v4, v6, v13}, Lcom/samsung/android/scloud/backup/common/BNRException;-><init>(ILjava/lang/Throwable;)V

    throw v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 491
    .end local v13    # "e":Lorg/json/JSONException;
    :catch_5
    move-exception v13

    .line 493
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 486
    .end local v13    # "e":Ljava/io/IOException;
    .end local v15    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object/from16 v15, v16

    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v15    # "fw":Ljava/io/FileWriter;
    goto :goto_5

    .line 482
    .end local v15    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :catch_6
    move-exception v13

    move-object/from16 v15, v16

    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v15    # "fw":Ljava/io/FileWriter;
    goto :goto_7

    .line 480
    .end local v15    # "fw":Ljava/io/FileWriter;
    .restart local v16    # "fw":Ljava/io/FileWriter;
    :catch_7
    move-exception v14

    move-object/from16 v15, v16

    .end local v16    # "fw":Ljava/io/FileWriter;
    .restart local v15    # "fw":Ljava/io/FileWriter;
    goto :goto_4
.end method

.method public getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .local p2, "recipients":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 961
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    .line 963
    .local v2, "ID_PROJECTION":[Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/scloud/backup/model/Mms;->THREAD_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    .line 965
    .local v9, "uriBuilder":Landroid/net/Uri$Builder;
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 967
    .local v8, "recipient":Ljava/lang/String;
    const-string v0, "recipient"

    invoke-virtual {v9, v0, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 971
    .end local v8    # "recipient":Ljava/lang/String;
    :cond_0
    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 973
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 977
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    .line 981
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 983
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 989
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 994
    :goto_1
    return-wide v4

    .line 989
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 994
    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 989
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public getOrderByColumnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "_id"

    return-object v0
.end method

.method public getProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1072
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    .line 1073
    .local v0, "projection":[Ljava/lang/String;
    return-object v0
.end method

.method public getSourceProjection()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 87
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "read"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "m_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ct_t"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ct_l"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "exp"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "m_cls"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "m_type"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "v"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "pri"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "rr"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "tr_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "d_rpt"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "locked"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "reserved"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "date_sent"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "rpt_a"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "resp_st"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "retr_st"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "retr_txt"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "retr_txt_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "read_status"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "ct_cls"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "d_tm"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "resp_txt"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "seen"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getWhere()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public postOperationOnBackup(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSuccess"    # Z

    .prologue
    .line 1080
    return-void
.end method

.method public preOperationOnRestore(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1066
    invoke-static {p1}, Lcom/samsung/android/scloud/backup/util/BNRUtil;->setSMSMode(Landroid/content/Context;)V

    .line 1067
    return-void
.end method

.method public putItemToOEM(Landroid/content/Context;Lcom/samsung/android/scloud/backup/core/IStatusListener;Ljava/util/List;Ljava/util/List;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/scloud/backup/core/IStatusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/samsung/android/scloud/backup/core/IStatusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/scloud/backup/data/BNRItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .local p3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/scloud/backup/data/BNRItem;>;"
    .local p4, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 565
    const-string v1, "MMSBuilder"

    const-string v2, "putDataToOem() is called !!!"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 567
    :cond_0
    const-string v1, "MMSBuilder"

    const-string v2, "putDataToOem() is Done : inputData is null or 0"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :goto_0
    return v0

    .line 570
    :cond_1
    invoke-direct {p0, p1, p3, p4}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->insert(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 571
    const-string v1, "MMSBuilder"

    const-string v2, "insert() is failed : MMS data is not inserted"

    invoke-static {v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 574
    :cond_2
    const-string v0, "MMSBuilder"

    const-string v1, "insert() is Done : All data is inserted"

    invoke-static {v0, v1}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    iget-object v0, p0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    invoke-interface {v0}, Lcom/samsung/android/scloud/backup/model/IModel;->getSourceKey()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x66

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    int-to-float v2, v2

    invoke-interface {p2, v0, v1, v2}, Lcom/samsung/android/scloud/backup/core/IStatusListener;->onProgress(Ljava/lang/String;IF)V

    .line 576
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected restoreBackedDataToPhone(Lorg/json/JSONObject;Landroid/content/Context;Ljava/util/List;)Z
    .locals 54
    .param p1, "jsonob"    # Lorg/json/JSONObject;
    .param p2, "mContext"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 638
    .local p3, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v51, "MMSBuilder"

    const-string v52, "restoreBackedDataToPhone() is called"

    invoke-static/range {v51 .. v52}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    const-string v51, "MMS"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v27

    .line 641
    .local v27, "mmsJson":Lorg/json/JSONObject;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 643
    .local v6, "CR":Landroid/content/ContentResolver;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 649
    .local v7, "CV":Landroid/content/ContentValues;
    new-instance v43, Ljava/util/LinkedHashSet;

    invoke-direct/range {v43 .. v43}, Ljava/util/LinkedHashSet;-><init>()V

    .line 651
    .local v43, "recipientsList":Ljava/util/LinkedHashSet;, "Ljava/util/LinkedHashSet<Ljava/lang/String;>;"
    new-instance v41, Landroid/content/ContentValues;

    invoke-direct/range {v41 .. v41}, Landroid/content/ContentValues;-><init>()V

    .line 659
    .local v41, "pendingValues":Landroid/content/ContentValues;
    :try_start_0
    const-string v51, "source_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 661
    .local v18, "backedupMsgId":J
    const-string v51, "m_type"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v32

    .line 663
    .local v32, "msg_type":J
    const/16 v16, 0x1

    .line 665
    .local v16, "bIsMessageTypeSent":Z
    const-string v51, "ADDRESS"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v13

    .line 667
    .local v13, "addrJson":Lorg/json/JSONArray;
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v51

    if-lez v51, :cond_6

    .line 669
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v12

    .line 671
    .local v12, "addrCount":I
    const-string v51, "msg_box"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v51

    const/16 v52, 0x1

    move/from16 v0, v51

    move/from16 v1, v52

    if-ne v0, v1, :cond_0

    .line 673
    const/16 v16, 0x0

    .line 677
    :cond_0
    const-wide/16 v52, 0x86

    cmp-long v51, v32, v52

    if-eqz v51, :cond_1

    const-wide/16 v52, 0x87

    cmp-long v51, v32, v52

    if-nez v51, :cond_2

    .line 681
    :cond_1
    const/16 v16, 0x1

    .line 683
    :cond_2
    const/16 v42, 0x0

    .local v42, "pos":I
    :goto_0
    move/from16 v0, v42

    if-ge v0, v12, :cond_6

    .line 684
    move/from16 v0, v42

    invoke-virtual {v13, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v34

    .line 686
    .local v34, "obj":Lorg/json/JSONObject;
    const-string v51, "type"

    move-object/from16 v0, v34

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v51

    const/16 v52, 0x89

    move/from16 v0, v51

    move/from16 v1, v52

    if-ne v0, v1, :cond_4

    if-nez v16, :cond_4

    .line 689
    const-string v51, "address"

    move-object/from16 v0, v34

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, v43

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 683
    :cond_3
    :goto_1
    add-int/lit8 v42, v42, 0x1

    goto :goto_0

    .line 692
    :cond_4
    const-string v51, "type"

    move-object/from16 v0, v34

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v51

    const/16 v52, 0x89

    move/from16 v0, v51

    move/from16 v1, v52

    if-eq v0, v1, :cond_3

    const/16 v51, 0x1

    move/from16 v0, v16

    move/from16 v1, v51

    if-ne v0, v1, :cond_3

    .line 695
    const-string v51, "address"

    move-object/from16 v0, v34

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, v43

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 950
    .end local v12    # "addrCount":I
    .end local v13    # "addrJson":Lorg/json/JSONArray;
    .end local v16    # "bIsMessageTypeSent":Z
    .end local v18    # "backedupMsgId":J
    .end local v32    # "msg_type":J
    .end local v34    # "obj":Lorg/json/JSONObject;
    .end local v42    # "pos":I
    :catch_0
    move-exception v22

    .line 951
    .local v22, "e":Ljava/lang/Exception;
    const-string v51, "MMSBuilder"

    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    const-string v53, "RestoreBackedDataToPhone: Exception received: "

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v53

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/samsung/android/scloud/backup/util/LOG;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 956
    .end local v22    # "e":Ljava/lang/Exception;
    :cond_5
    const/16 v51, 0x1

    :goto_2
    return v51

    .line 701
    .restart local v13    # "addrJson":Lorg/json/JSONArray;
    .restart local v16    # "bIsMessageTypeSent":Z
    .restart local v18    # "backedupMsgId":J
    .restart local v32    # "msg_type":J
    :cond_6
    :try_start_1
    invoke-virtual/range {v41 .. v41}, Landroid/content/ContentValues;->clear()V

    .line 703
    const-string v51, "PENDING"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v40

    .line 706
    .local v40, "pendingJson":Lorg/json/JSONArray;
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-wide/from16 v2, v18

    move-object/from16 v4, v41

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->hasPendingEntry(Lorg/json/JSONArray;JLandroid/content/ContentValues;)Z

    move-result v15

    .line 709
    .local v15, "bHasPending":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v8

    .line 711
    .local v8, "ThreadId":J
    invoke-virtual/range {v43 .. v43}, Ljava/util/LinkedHashSet;->clear()V

    .line 713
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 715
    const-string v51, "thread_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 717
    const-string v51, "date"

    const-string v52, "date"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 719
    const-string v51, "msg_box"

    const-string v52, "msg_box"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 722
    const-string v51, "read"

    const-string v52, "read"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 724
    const-string v51, "m_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_7

    .line 725
    const-string v51, "m_id"

    const-string v52, "m_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    :cond_7
    const-string v51, "sub"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_8

    .line 727
    const-string v51, "sub"

    const-string v52, "sub"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    :cond_8
    const-string v51, "ct_t"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_9

    .line 729
    const-string v51, "ct_t"

    const-string v52, "ct_t"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    :cond_9
    const-string v51, "ct_l"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_a

    .line 731
    const-string v51, "ct_l"

    const-string v52, "ct_l"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_a
    const-string v51, "exp"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_b

    .line 733
    const-string v51, "exp"

    const-string v52, "exp"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 734
    :cond_b
    const-string v51, "m_cls"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_c

    .line 735
    const-string v51, "m_cls"

    const-string v52, "m_cls"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_c
    if-eqz v15, :cond_23

    .line 741
    const-string v51, "m_type"

    const/16 v52, 0x82

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 749
    :goto_3
    const-string v51, "v"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_d

    .line 750
    const-string v51, "v"

    const-string v52, "v"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 751
    :cond_d
    const-string v51, "m_size"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_e

    .line 752
    const-string v51, "m_size"

    const-string v52, "m_size"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 753
    :cond_e
    const-string v51, "reserved"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_f

    .line 754
    const-string v51, "reserved"

    const-string v52, "reserved"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 756
    :cond_f
    const-wide/16 v44, 0x0

    .line 757
    .local v44, "priority":J
    const-string v51, "pri"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_10

    .line 758
    const-string v51, "pri"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v44

    .line 760
    :cond_10
    const-wide/16 v52, 0x0

    cmp-long v51, v44, v52

    if-eqz v51, :cond_11

    .line 762
    const-string v51, "pri"

    invoke-static/range {v44 .. v45}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 764
    :cond_11
    const-string v51, "rr"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v46

    .line 766
    .local v46, "rr":J
    const-wide/16 v52, 0x0

    cmp-long v51, v46, v52

    if-eqz v51, :cond_12

    .line 768
    const-string v51, "rr"

    invoke-static/range {v46 .. v47}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 769
    :cond_12
    const-string v51, "tr_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_13

    .line 770
    const-string v51, "tr_id"

    const-string v52, "tr_id"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :cond_13
    const-string v51, "d_rpt"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 774
    .local v20, "d_rpt":J
    const-wide/16 v52, 0x0

    cmp-long v51, v20, v52

    if-eqz v51, :cond_14

    .line 775
    const-string v51, "d_rpt"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 776
    :cond_14
    const-string v51, "locked"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_15

    .line 777
    const-string v51, "locked"

    const-string v52, "locked"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 778
    :cond_15
    const-string v51, "date_sent"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_16

    .line 779
    const-string v51, "date_sent"

    const-string v52, "date_sent"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_16
    const-string v51, "sub_cs"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_17

    .line 782
    const-string v51, "sub_cs"

    const-string v52, "sub_cs"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 784
    :cond_17
    const-string v51, "rpt_a"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_18

    .line 785
    const-string v51, "rpt_a"

    const-string v52, "rpt_a"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    :cond_18
    const-string v51, "resp_st"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_19

    .line 788
    const-string v51, "resp_st"

    const-string v52, "resp_st"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    :cond_19
    const-string v51, "st"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1a

    .line 791
    const-string v51, "st"

    const-string v52, "st"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    :cond_1a
    const-string v51, "retr_st"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1b

    .line 794
    const-string v51, "retr_st"

    const-string v52, "retr_st"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    :cond_1b
    const-string v51, "retr_txt"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1c

    .line 797
    const-string v51, "retr_txt"

    const-string v52, "retr_txt"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    :cond_1c
    const-string v51, "retr_txt_cs"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1d

    .line 800
    const-string v51, "retr_txt_cs"

    const-string v52, "retr_txt_cs"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 802
    :cond_1d
    const-string v51, "read_status"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1e

    .line 803
    const-string v51, "read_status"

    const-string v52, "read_status"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    :cond_1e
    const-string v51, "ct_cls"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_1f

    .line 806
    const-string v51, "ct_cls"

    const-string v52, "ct_cls"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    :cond_1f
    const-string v51, "d_tm"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_20

    .line 809
    const-string v51, "d_tm"

    const-string v52, "d_tm"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    :cond_20
    const-string v51, "resp_txt"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_21

    .line 812
    const-string v51, "resp_txt"

    const-string v52, "resp_txt"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    :cond_21
    const-string v51, "seen"

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_22

    .line 815
    const-string v51, "seen"

    const-string v52, "seen"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v51, v0

    invoke-interface/range {v51 .. v51}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v51

    move-object/from16 v0, v51

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v29

    .line 819
    .local v29, "msgUri":Landroid/net/Uri;
    if-nez v29, :cond_24

    .line 821
    const-string v51, "MMSBuilder"

    const-string v52, "Failed to insert into mms table"

    invoke-static/range {v51 .. v52}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const/16 v51, 0x0

    goto/16 :goto_2

    .line 746
    .end local v20    # "d_rpt":J
    .end local v29    # "msgUri":Landroid/net/Uri;
    .end local v44    # "priority":J
    .end local v46    # "rr":J
    :cond_23
    const-string v51, "m_type"

    const-string v52, "m_type"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto/16 :goto_3

    .line 826
    .restart local v20    # "d_rpt":J
    .restart local v29    # "msgUri":Landroid/net/Uri;
    .restart local v44    # "priority":J
    .restart local v46    # "rr":J
    :cond_24
    invoke-static/range {v29 .. v29}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v30

    .line 827
    .local v30, "msg_id":J
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p3

    move-object/from16 v1, v51

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 829
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 831
    if-eqz v15, :cond_25

    .line 833
    const-string v51, "m_type"

    const-string v52, "m_type"

    move-object/from16 v0, v27

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->myModel:Lcom/samsung/android/scloud/backup/model/IModel;

    move-object/from16 v51, v0

    invoke-interface/range {v51 .. v51}, Lcom/samsung/android/scloud/backup/model/IModel;->getOemContentUri()Landroid/net/Uri;

    move-result-object v51

    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    const-string v53, "_id="

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    move-object/from16 v0, v52

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    const/16 v53, 0x0

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    move-object/from16 v2, v53

    invoke-virtual {v6, v0, v7, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 838
    sget-object v51, Lcom/samsung/android/scloud/backup/model/Mms;->TARGET_PENDING_CONTENT_URI:Landroid/net/Uri;

    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    const-string v53, "msg_id="

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    move-object/from16 v0, v52

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    const/16 v53, 0x0

    move-object/from16 v0, v51

    move-object/from16 v1, v41

    move-object/from16 v2, v52

    move-object/from16 v3, v53

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 841
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 845
    :cond_25
    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "content://mms/"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v51

    const-string v52, "/addr"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v51 .. v51}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v48

    .line 847
    .local v48, "systemAddrUri":Landroid/net/Uri;
    new-instance v51, Ljava/lang/StringBuilder;

    invoke-direct/range {v51 .. v51}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "content://mms/"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    move-object/from16 v0, v51

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v51

    const-string v52, "/part"

    invoke-virtual/range {v51 .. v52}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v51

    invoke-virtual/range {v51 .. v51}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v51

    invoke-static/range {v51 .. v51}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v49

    .line 849
    .local v49, "systemPartUri":Landroid/net/Uri;
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v51

    if-lez v51, :cond_2a

    .line 851
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v11

    .line 853
    .local v11, "addrCnt":I
    const/16 v42, 0x0

    .restart local v42    # "pos":I
    :goto_4
    move/from16 v0, v42

    if-ge v0, v11, :cond_2a

    .line 854
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 856
    move/from16 v0, v42

    invoke-virtual {v13, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 857
    .local v10, "addr":Lorg/json/JSONObject;
    const-string v51, "msg_id"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 858
    const-string v51, "address"

    move-object/from16 v0, v51

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_26

    .line 859
    const-string v51, "address"

    const-string v52, "address"

    move-object/from16 v0, v52

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    :cond_26
    const-string v51, "type"

    move-object/from16 v0, v51

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_27

    .line 861
    const-string v51, "type"

    const-string v52, "type"

    move-object/from16 v0, v52

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 862
    :cond_27
    const-string v51, "charset"

    move-object/from16 v0, v51

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_28

    .line 863
    const-string v51, "charset"

    const-string v52, "charset"

    move-object/from16 v0, v52

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v52

    invoke-static/range {v52 .. v53}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 865
    :cond_28
    const/4 v14, 0x0

    .line 866
    .local v14, "addrUri":Landroid/net/Uri;
    move-object/from16 v0, v48

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v14

    .line 867
    if-nez v14, :cond_29

    .line 868
    const-string v51, "MMSBuilder"

    const-string v52, "restoreBackupedDataToPhone() : insert fail in ADDRESS table"

    invoke-static/range {v51 .. v52}, Lcom/samsung/android/scloud/backup/util/LOG;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const/16 v51, 0x0

    goto/16 :goto_2

    .line 853
    :cond_29
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_4

    .line 878
    .end local v10    # "addr":Lorg/json/JSONObject;
    .end local v11    # "addrCnt":I
    .end local v14    # "addrUri":Landroid/net/Uri;
    .end local v42    # "pos":I
    :cond_2a
    const-string v51, "PART"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v36

    .line 879
    .local v36, "partArray":Lorg/json/JSONArray;
    invoke-virtual/range {v36 .. v36}, Lorg/json/JSONArray;->length()I

    move-result v51

    if-lez v51, :cond_5

    .line 880
    invoke-virtual/range {v36 .. v36}, Lorg/json/JSONArray;->length()I

    move-result v37

    .line 882
    .local v37, "partCnt":I
    const/16 v42, 0x0

    .restart local v42    # "pos":I
    :goto_5
    move/from16 v0, v42

    move/from16 v1, v37

    if-ge v0, v1, :cond_5

    .line 883
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 884
    move-object/from16 v0, v36

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v35

    .line 886
    .local v35, "part":Lorg/json/JSONObject;
    const-string v51, "mid"

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 888
    const-string v51, "seq"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_2b

    .line 889
    const-string v51, "seq"

    const-string v52, "seq"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 891
    :cond_2b
    const-string v51, "ct"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_2c

    .line 892
    const-string v51, "ct"

    const-string v52, "ct"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    :cond_2c
    const-string v51, "name"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_2d

    .line 895
    const-string v51, "name"

    const-string v52, "name"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    :cond_2d
    const-string v51, "chset"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_2e

    .line 898
    const-string v51, "chset"

    const-string v52, "chset"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v52

    invoke-static/range {v52 .. v52}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 900
    :cond_2e
    const-string v51, "cid"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_2f

    .line 901
    const-string v51, "cid"

    const-string v52, "cid"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    :cond_2f
    const-string v51, "cl"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_30

    .line 904
    const-string v51, "cl"

    const-string v52, "cl"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    :cond_30
    const-string v51, "_data"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 911
    .local v17, "data":Ljava/lang/String;
    const-string v51, "text"

    move-object/from16 v0, v35

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v51

    if-eqz v51, :cond_31

    .line 912
    const-string v51, "text"

    const-string v52, "text"

    move-object/from16 v0, v35

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v51

    move-object/from16 v1, v52

    invoke-virtual {v7, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    :cond_31
    move-object/from16 v0, v49

    invoke-virtual {v6, v0, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v39

    .line 916
    .local v39, "partUri":Landroid/net/Uri;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v51

    if-lez v51, :cond_33

    .line 918
    invoke-static/range {v39 .. v39}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v52

    move-wide/from16 v0, v52

    long-to-int v0, v0

    move/from16 v38, v0

    .line 919
    .local v38, "partId":I
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    .line 920
    .local v26, "mmsDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v28, 0x0

    .line 922
    .local v28, "mmsdata":Lorg/json/JSONArray;
    const-string v51, "DataValue"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v28

    .line 923
    if-eqz v28, :cond_33

    .line 924
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_6
    invoke-virtual/range {v28 .. v28}, Lorg/json/JSONArray;->length()I

    move-result v51

    move/from16 v0, v25

    move/from16 v1, v51

    if-ge v0, v1, :cond_33

    .line 925
    const/16 v50, 0x0

    .line 926
    .local v50, "temp":Lorg/json/JSONObject;
    move-object/from16 v0, v28

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v50

    .line 927
    if-eqz v50, :cond_32

    .line 928
    invoke-virtual/range {v50 .. v50}, Lorg/json/JSONObject;->names()Lorg/json/JSONArray;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v24

    .line 930
    .local v24, "filenames":Lorg/json/JSONArray;
    const/16 v23, 0x0

    .line 931
    .local v23, "filename":Ljava/lang/String;
    if-eqz v24, :cond_32

    .line 933
    const/16 v51, 0x0

    :try_start_2
    move-object/from16 v0, v24

    move/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v23

    .line 938
    :goto_7
    :try_start_3
    move-object/from16 v0, v50

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, v26

    move-object/from16 v1, v23

    move-object/from16 v2, v51

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v38

    move-object/from16 v3, v17

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/scloud/backup/modelbuilder/MmsBuilder;->getMmsAttachment(Landroid/content/Context;ILjava/lang/String;Ljava/util/HashMap;)V

    .line 924
    .end local v23    # "filename":Ljava/lang/String;
    .end local v24    # "filenames":Lorg/json/JSONArray;
    :cond_32
    add-int/lit8 v25, v25, 0x1

    goto :goto_6

    .line 934
    .restart local v23    # "filename":Ljava/lang/String;
    .restart local v24    # "filenames":Lorg/json/JSONArray;
    :catch_1
    move-exception v22

    .line 936
    .local v22, "e":Lorg/json/JSONException;
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_7

    .line 882
    .end local v22    # "e":Lorg/json/JSONException;
    .end local v23    # "filename":Ljava/lang/String;
    .end local v24    # "filenames":Lorg/json/JSONArray;
    .end local v25    # "i":I
    .end local v26    # "mmsDataMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v28    # "mmsdata":Lorg/json/JSONArray;
    .end local v38    # "partId":I
    .end local v50    # "temp":Lorg/json/JSONObject;
    :cond_33
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_5
.end method
