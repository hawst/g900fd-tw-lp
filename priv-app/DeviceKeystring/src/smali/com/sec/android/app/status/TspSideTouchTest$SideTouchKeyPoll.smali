.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;
.super Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SideTouchKeyPoll"
.end annotation


# instance fields
.field protected final _keyEventColor:[I

.field protected _keyEventColorIdx:I

.field protected _maxValue:I

.field protected _touchMaxValueView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 1121
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    .line 1122
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 1116
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    .line 1117
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    .line 1118
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColor:[I

    .line 1119
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColorIdx:I

    .line 1123
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1124
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1125
    return-void

    .line 1118
    :array_0
    .array-data 4
        0x7f060005
        0x7f060004
    .end array-data
.end method


# virtual methods
.method public addView(Landroid/widget/FrameLayout;)V
    .locals 4
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 1163
    invoke-super {p0, p1}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->addView(Landroid/widget/FrameLayout;)V

    .line 1165
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1167
    .local v0, "lParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1168
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_lLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1169
    return-void
.end method

.method public onKey(ILandroid/view/KeyEvent;)V
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1129
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyCode:I

    if-eq p1, v0, :cond_1

    .line 1153
    :cond_0
    :goto_0
    return-void

    .line 1133
    :cond_1
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", keyCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", getRepeatCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1139
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColor:[I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColorIdx:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColorIdx:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1141
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColorIdx:I

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColor:[I

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyEventColorIdx:I

    .line 1142
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->playKeySound(Landroid/content/Context;)V

    goto :goto_0

    .line 1147
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 1137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onReset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1157
    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    .line 1158
    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    .line 1159
    return-void
.end method

.method public onStatusChanged(Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;)V
    .locals 4
    .param p1, "status"    # Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .prologue
    .line 1229
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStatusChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-ne p1, v0, :cond_0

    .line 1231
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1233
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->updateView()V

    .line 1234
    return-void
.end method

.method public onUpdate([Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # [Ljava/lang/String;

    .prologue
    .line 1209
    if-eqz p1, :cond_0

    .line 1211
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_param:I

    .line 1212
    .local v1, "index":I
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 1213
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onUpdate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    :try_start_0
    aget-object v3, p1, v1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1217
    .local v2, "sensitivity":I
    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->setSensitivity(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1224
    .end local v1    # "index":I
    .end local v2    # "sensitivity":I
    :cond_0
    :goto_0
    return-void

    .line 1219
    .restart local v1    # "index":I
    :catch_0
    move-exception v0

    .line 1220
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected setSensitivity(I)V
    .locals 5
    .param p1, "sensitivity"    # I

    .prologue
    .line 1194
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    .line 1195
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    if-le v1, v2, :cond_0

    .line 1196
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setSensitivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_maxValue: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    iput v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    .line 1200
    :cond_0
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_specMin:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    if-gt v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_specMax:I

    if-gt v1, v2, :cond_1

    const/4 v0, 0x1

    .line 1201
    .local v0, "bSpecIn":Z
    :goto_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setSensitivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensitivity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bSpecIn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->updateView()V

    .line 1205
    return-void

    .line 1200
    .end local v0    # "bSpecIn":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x11

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1173
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1174
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchNameView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1175
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1176
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1177
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1179
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_sensitivity:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1180
    .local v0, "sensitivity":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1181
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1182
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1183
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1185
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_maxValue:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1186
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1187
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1188
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1189
    return-void
.end method
