.class Lcom/sec/android/app/status/PCMDump$3;
.super Ljava/lang/Object;
.source "PCMDump.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/PCMDump;->startRecording()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/PCMDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/PCMDump;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioRecord;->startRecording()V

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    if-nez v1, :cond_5

    .line 304
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$800(Lcom/sec/android/app/status/PCMDump;)Ljava/io/FileOutputStream;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 305
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$800(Lcom/sec/android/app/status/PCMDump;)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/PCMDump;->access$802(Lcom/sec/android/app/status/PCMDump;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 312
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioRecord;->stop()V

    .line 316
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/AudioRecord;->release()V

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # setter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v1, v6}, Lcom/sec/android/app/status/PCMDump;->access$402(Lcom/sec/android/app/status/PCMDump;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;

    .line 320
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # setter for: Lcom/sec/android/app/status/PCMDump;->mIsRecording:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/status/PCMDump;->access$502(Lcom/sec/android/app/status/PCMDump;Z)Z

    .line 322
    :cond_4
    return-void

    .line 288
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/status/PCMDump;->mIsRecording:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/status/PCMDump;->access$502(Lcom/sec/android/app/status/PCMDump;Z)Z

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v2}, Lcom/sec/android/app/status/PCMDump;->access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    iget-object v3, v3, Lcom/sec/android/app/status/PCMDump;->buffer:[B

    iget-object v4, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mBufferSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/PCMDump;->access$700(Lcom/sec/android/app/status/PCMDump;)I

    move-result v4

    invoke-virtual {v2, v3, v5, v4}, Landroid/media/AudioRecord;->read([BII)I

    move-result v2

    # setter for: Lcom/sec/android/app/status/PCMDump;->mReadBytes:I
    invoke-static {v1, v2}, Lcom/sec/android/app/status/PCMDump;->access$602(Lcom/sec/android/app/status/PCMDump;I)I

    .line 290
    const/4 v1, -0x3

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mReadBytes:I
    invoke-static {v2}, Lcom/sec/android/app/status/PCMDump;->access$600(Lcom/sec/android/app/status/PCMDump;)I

    move-result v2

    if-eq v1, v2, :cond_7

    .line 292
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$800(Lcom/sec/android/app/status/PCMDump;)Ljava/io/FileOutputStream;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$800(Lcom/sec/android/app/status/PCMDump;)Ljava/io/FileOutputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    iget-object v2, v2, Lcom/sec/android/app/status/PCMDump;->buffer:[B

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mReadBytes:I
    invoke-static {v4}, Lcom/sec/android/app/status/PCMDump;->access$600(Lcom/sec/android/app/status/PCMDump;)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 301
    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump$3;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;
    invoke-static {v1}, Lcom/sec/android/app/status/PCMDump;->access$900(Lcom/sec/android/app/status/PCMDump;)Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 299
    .end local v0    # "e":Ljava/io/IOException;
    :cond_7
    const-string v1, "PCMDump"

    const-string v2, "ModuleAudioService, Audio recording failed!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 308
    :catch_1
    move-exception v0

    .line 309
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1
.end method
