.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;
.super Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
.source "TspSideTouchTest.java"

# interfaces
.implements Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SideTouchKeyMultiverse"
.end annotation


# instance fields
.field protected _preState:I

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    .line 1024
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    .line 1025
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 1022
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_preState:I

    .line 1026
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1027
    return-void
.end method


# virtual methods
.method public onKey(ILandroid/view/KeyEvent;)V
    .locals 0
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1032
    return-void
.end method

.method public onReset()V
    .locals 1

    .prologue
    .line 1042
    invoke-super {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->onReset()V

    .line 1043
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_preState:I

    .line 1044
    return-void
.end method

.method protected setSensitivity(I)V
    .locals 5
    .param p1, "sensitivity"    # I

    .prologue
    .line 1049
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_sensitivity:I

    .line 1050
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_specMin:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_sensitivity:I

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_sensitivity:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_specMax:I

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 1051
    .local v0, "bSpecIn":Z
    :goto_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setSensitivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sensitivity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bSpecIn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    if-eqz v0, :cond_1

    .line 1055
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1082
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->updateView()V

    .line 1083
    return-void

    .line 1050
    .end local v0    # "bSpecIn":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1057
    .restart local v0    # "bSpecIn":Z
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1058
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V
    invoke-static {v1}, Lcom/sec/android/app/status/TspSideTouchTest;->access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V

    goto :goto_1

    .line 1061
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1062
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V
    invoke-static {v1}, Lcom/sec/android/app/status/TspSideTouchTest;->access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V

    goto :goto_1

    .line 1070
    :cond_1
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 1072
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    goto :goto_1

    .line 1055
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 1070
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    .locals 1
    .param p1, "param"    # I

    .prologue
    .line 1036
    invoke-static {p1}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->getThreshold(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->_threshold:Ljava/lang/String;

    .line 1037
    return-object p0
.end method
