.class public Lcom/sec/android/app/status/Proximity_offset_JB;
.super Landroid/app/Activity;
.source "Proximity_offset_JB.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;
    }
.end annotation


# instance fields
.field private mAdcView:Landroid/widget/TextView;

.field private mBackview:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

.field private mIsSupportSensorhub:Z

.field private mIsThresholdChecked:Z

.field private mOffsetBtn:Landroid/widget/Button;

.field private mOffsetResult:Landroid/widget/TextView;

.field private mOffsetValue:I

.field private mOffsetView:Landroid/widget/TextView;

.field private mResetBtn:Landroid/widget/Button;

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThreshold:Landroid/widget/EditText;

.field private mThresholdChangeBtn:Landroid/widget/Button;

.field private mTimer:Ljava/util/Timer;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkView:Landroid/widget/TextView;

.field private mbTMD27723:Z

.field private mnOffsetPassState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    .line 65
    iput v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    .line 70
    iput v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetValue:I

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsThresholdChecked:Z

    .line 73
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsSupportSensorhub:Z

    .line 75
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset_JB$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/Proximity_offset_JB$1;-><init>(Lcom/sec/android/app/status/Proximity_offset_JB;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    .line 371
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/Proximity_offset_JB;)Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mAdcView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/Proximity_offset_JB;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/Proximity_offset_JB;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mBackview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/Proximity_offset_JB;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->startVibrate()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/Proximity_offset_JB;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->stopVibrate()V

    return-void
.end method

.method private startVibrate()V
    .locals 3

    .prologue
    .line 185
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 186
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 187
    return-void

    .line 185
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibrate()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 191
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 259
    const/4 v1, 0x0

    .line 260
    .local v1, "fw":Ljava/io/FileWriter;
    const/4 v3, 0x0

    .line 262
    .local v3, "result":Z
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 264
    const/4 v3, 0x1

    .line 269
    if-eqz v2, :cond_2

    .line 271
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 277
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return v3

    .line 272
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "Proximity_offset_JB"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 274
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 265
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 266
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException filepath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Reason: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 269
    if-eqz v1, :cond_0

    .line 271
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 272
    :catch_2
    move-exception v0

    .line 273
    const-string v4, "Proximity_offset_JB"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 269
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_1

    .line 271
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 274
    :cond_1
    :goto_3
    throw v4

    .line 272
    :catch_3
    move-exception v0

    .line 273
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "Proximity_offset_JB"

    const-string v6, "IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 269
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 265
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method public IsOffsetPass()I
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 308
    const/4 v0, 0x0

    .line 309
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 311
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/sys/class/sensors/proximity_sensor/prox_offset_pass"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 313
    .local v4, "temp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 314
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 316
    :cond_0
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IsOffsetPass: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    if-eqz v0, :cond_7

    .line 319
    const-string v6, "1"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    if-eqz v6, :cond_3

    .line 320
    const/4 v5, 0x1

    .line 330
    if-eqz v3, :cond_1

    .line 332
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    move-object v2, v3

    .line 338
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "temp":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    return v5

    .line 333
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 334
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 321
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_3
    const-string v6, "2"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v6

    if-eqz v6, :cond_5

    .line 322
    const/4 v5, 0x2

    .line 330
    if-eqz v3, :cond_4

    .line 332
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    :goto_2
    move-object v2, v3

    .line 335
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 333
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .line 334
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 330
    .end local v1    # "e":Ljava/io/IOException;
    :cond_5
    if-eqz v3, :cond_6

    .line 332
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_6
    :goto_3
    move-object v2, v3

    .line 335
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 333
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    .line 334
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 330
    .end local v1    # "e":Ljava/io/IOException;
    :cond_7
    if-eqz v3, :cond_9

    .line 332
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    move-object v2, v3

    .line 335
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 333
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_3
    move-exception v1

    .line 334
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 335
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1

    .line 327
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "temp":Ljava/lang/String;
    :catch_4
    move-exception v1

    .line 328
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ProximitySensor IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 330
    if-eqz v2, :cond_2

    .line 332
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_1

    .line 333
    :catch_5
    move-exception v1

    .line 334
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 330
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_5
    if-eqz v2, :cond_8

    .line 332
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 335
    :cond_8
    :goto_6
    throw v5

    .line 333
    :catch_6
    move-exception v1

    .line 334
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 330
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 327
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_4

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :cond_9
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method public ReadSensorModelName()V
    .locals 9

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 345
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v6, "/sys/class/sensors/proximity_sensor/name"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 347
    .local v4, "temp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 348
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 349
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SensorModelName: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :cond_0
    if-eqz v0, :cond_2

    .line 352
    const-string v5, "TMD27723"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "TMD2672X"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "TMD26723"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 353
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 360
    :cond_2
    if-eqz v3, :cond_5

    .line 362
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 368
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "temp":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-void

    .line 363
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 365
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 357
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 358
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ProximitySensor IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 360
    if-eqz v2, :cond_3

    .line 362
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 363
    :catch_2
    move-exception v1

    .line 364
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 360
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_4

    .line 362
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 365
    :cond_4
    :goto_3
    throw v5

    .line 363
    :catch_3
    move-exception v1

    .line 364
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 360
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 357
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :cond_5
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v9, -0xffff01

    const/4 v8, 0x1

    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0a0167

    if-ne v4, v5, :cond_3

    .line 196
    const-string v4, "Proximity_offset_JB"

    const-string v5, "offset"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const-string v4, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v5, "0"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 198
    const-string v4, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v5, "1"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->readOffset()Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "OffsetValue":[Ljava/lang/String;
    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetValue:I

    .line 201
    aget-object v4, v0, v8

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "temp":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    double-to-int v3, v4

    .line 204
    .local v3, "threshold":I
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 206
    iget-boolean v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    if-ne v4, v8, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->IsOffsetPass()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    .line 209
    iget v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    if-lez v4, :cond_1

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    const-string v5, "Pass"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 247
    .end local v0    # "OffsetValue":[Ljava/lang/String;
    .end local v2    # "temp":Ljava/lang/String;
    .end local v3    # "threshold":I
    :cond_0
    :goto_0
    return-void

    .line 213
    .restart local v0    # "OffsetValue":[Ljava/lang/String;
    .restart local v2    # "temp":Ljava/lang/String;
    .restart local v3    # "threshold":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    const-string v5, "Fail"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 217
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    const-string v5, "Pass"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 221
    .end local v0    # "OffsetValue":[Ljava/lang/String;
    .end local v2    # "temp":Ljava/lang/String;
    .end local v3    # "threshold":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0a0168

    if-ne v4, v5, :cond_5

    .line 222
    const-string v4, "Proximity_offset_JB"

    const-string v5, "reset"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const-string v4, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v5, "0"

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 226
    iget-boolean v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    if-ne v4, v8, :cond_4

    .line 227
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->IsOffsetPass()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    .line 231
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 234
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    const v5, 0x7f0a0165

    if-ne v4, v5, :cond_0

    .line 235
    const-string v4, "Proximity_offset_JB"

    const-string v5, "Change threshold "

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const/4 v1, 0x0

    .line 237
    .local v1, "newthresh":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mThreshold:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 238
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "New Thresh: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    if-eqz v1, :cond_6

    .line 240
    const-string v4, "/sys/class/sensors/proximity_sensor/prox_thresh"

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 244
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x1

    .line 94
    const-string v0, "Proximity_offset_JB"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->ReadSensorModelName()V

    .line 98
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    if-ne v0, v5, :cond_3

    .line 99
    const-string v0, "Proximity_offset_JB"

    const-string v1, "This is TMD27723 or TMD2672X sensors"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const v0, 0x7f030055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->setContentView(I)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->IsOffsetPass()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I

    .line 106
    :goto_0
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 107
    .local v8, "productModel":Ljava/lang/String;
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v8, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 108
    const-string v0, "Proximity_offset_JB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "productModel= "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v0, "ro.product.device"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 110
    .local v7, "productDevice":Ljava/lang/String;
    const-string v0, "Proximity_offset_JB"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "productDevice= "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const-string v0, "i9300"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "c1vzw"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "c1att"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "c1spr"

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "m3"

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsThresholdChecked:Z

    .line 118
    :cond_1
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mVibrator:Landroid/os/Vibrator;

    .line 119
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensorManager:Landroid/hardware/SensorManager;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensor:Landroid/hardware/Sensor;

    .line 122
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mBackview:Landroid/view/View;

    .line 123
    const v0, 0x7f0a0166

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;

    .line 124
    const v0, 0x7f0a015e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mAdcView:Landroid/widget/TextView;

    .line 125
    const v0, 0x7f0a0170

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetView:Landroid/widget/TextView;

    .line 126
    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetResult:Landroid/widget/TextView;

    .line 128
    const v0, 0x7f0a0167

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetBtn:Landroid/widget/Button;

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v0, 0x7f0a0168

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mResetBtn:Landroid/widget/Button;

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v0, 0x7f0a0171

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mThreshold:Landroid/widget/EditText;

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mThreshold:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 135
    const v0, 0x7f0a0165

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mThresholdChangeBtn:Landroid/widget/Button;

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mThresholdChangeBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mTimer:Ljava/util/Timer;

    .line 138
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset_JB;Lcom/sec/android/app/status/Proximity_offset_JB$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    .line 140
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z

    if-ne v0, v5, :cond_4

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    const-wide/16 v4, 0x5dc

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 146
    :goto_1
    new-instance v6, Ljava/io/File;

    const-string v0, "/sys/class/sensors/ssp_sensor/mcu_test"

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 147
    .local v6, "nodeSensorhub":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsSupportSensorhub:Z

    .line 149
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsSupportSensorhub:Z

    if-eqz v0, :cond_2

    .line 150
    const-string v0, "/sys/class/sensors/proximity_sensor/prox_avg"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 152
    :cond_2
    return-void

    .line 103
    .end local v6    # "nodeSensorhub":Ljava/io/File;
    .end local v7    # "productDevice":Ljava/lang/String;
    .end local v8    # "productModel":Ljava/lang/String;
    :cond_3
    const v0, 0x7f030053

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->setContentView(I)V

    goto/16 :goto_0

    .line 143
    .restart local v7    # "productDevice":Ljava/lang/String;
    .restart local v8    # "productModel":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 176
    const-string v0, "Proximity_offset_JB"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 179
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mIsSupportSensorhub:Z

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "/sys/class/sensors/proximity_sensor/prox_avg"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/Proximity_offset_JB;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 182
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 166
    const-string v0, "Proximity_offset_JB"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 169
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB;->stopVibrate()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->access$500(Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;)V

    .line 172
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 156
    const-string v0, "Proximity_offset_JB"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->access$400(Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 162
    return-void
.end method

.method public readOffset()Ljava/lang/String;
    .locals 9

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 284
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 286
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v6, "sys/class/sensors/proximity_sensor/prox_cal"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 289
    .local v4, "temp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 290
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 292
    :cond_0
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 296
    if-eqz v3, :cond_4

    .line 298
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 304
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "temp":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    const-string v0, "NONE"

    .end local v0    # "data":Ljava/lang/String;
    :cond_2
    return-object v0

    .line 299
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 300
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v3

    .line 301
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 293
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 294
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ProximitySensor IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 296
    if-eqz v2, :cond_1

    .line 298
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 299
    :catch_2
    move-exception v1

    .line 300
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_3

    .line 298
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 301
    :cond_3
    :goto_3
    throw v5

    .line 299
    :catch_3
    move-exception v1

    .line 300
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "Proximity_offset_JB"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 296
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 293
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :cond_4
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method
