.class Lcom/sec/android/app/version/SimpleVersionTMO$1;
.super Ljava/lang/Object;
.source "SimpleVersionTMO.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/version/SimpleVersionTMO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/version/SimpleVersionTMO;


# direct methods
.method constructor <init>(Lcom/sec/android/app/version/SimpleVersionTMO;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 150
    const-string v0, "SimpleVersion"

    const-string v2, "onServiceConnected"

    const-string v3, "onServiceConnected()"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$102(Lcom/sec/android/app/version/SimpleVersionTMO;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v3, 0x1002

    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    invoke-static {v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v2, v1

    move v5, v4

    # invokes: Lcom/sec/android/app/version/SimpleVersionTMO;->SendData(CCCCC)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$300(Lcom/sec/android/app/version/SimpleVersionTMO;CCCCC)V

    .line 155
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 157
    const-string v0, "SimpleVersion"

    const-string v1, "onServiceDisconnected"

    const-string v2, "onServiceDisconnected()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$1;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mServiceMessenger:Landroid/os/Messenger;
    invoke-static {v0, v1}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$102(Lcom/sec/android/app/version/SimpleVersionTMO;Landroid/os/Messenger;)Landroid/os/Messenger;

    .line 159
    return-void
.end method
