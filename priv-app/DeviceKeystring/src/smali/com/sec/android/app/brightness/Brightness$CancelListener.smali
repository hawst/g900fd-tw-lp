.class public Lcom/sec/android/app/brightness/Brightness$CancelListener;
.super Ljava/lang/Object;
.source "Brightness.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/brightness/Brightness;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CancelListener"
.end annotation


# instance fields
.field private brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

.field final synthetic this$0:Lcom/sec/android/app/brightness/Brightness;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0
    .param p2, "dialog"    # Lcom/sec/android/app/brightness/miniDlg;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/brightness/Brightness$CancelListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/sec/android/app/brightness/Brightness$CancelListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    .line 49
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$CancelListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->restoreSystemBrightness()V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$CancelListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->dismiss()V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$CancelListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/Brightness;->finish()V

    .line 55
    return-void
.end method
