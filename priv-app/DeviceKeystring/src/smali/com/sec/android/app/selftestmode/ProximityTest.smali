.class public Lcom/sec/android/app/selftestmode/ProximityTest;
.super Landroid/app/Activity;
.source "ProximityTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/ProximityTest$1;,
        Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private info:Landroid/widget/TextView;

.field private mBackgroudLayout:Landroid/widget/LinearLayout;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mVibrator:Landroid/os/Vibrator;

.field private mlistner:Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;

.field private working:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->working:Z

    .line 41
    const-string v0, "Proximity Test"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->CLASS_NAME:Ljava/lang/String;

    .line 116
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/selftestmode/ProximityTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/ProximityTest;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->startVibration()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/selftestmode/ProximityTest;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/ProximityTest;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->working:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/selftestmode/ProximityTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/ProximityTest;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->isWorking()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/selftestmode/ProximityTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/ProximityTest;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->stopVibration()V

    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 69
    const v1, 0x7f0a010c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/ProximityTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->info:Landroid/widget/TextView;

    .line 70
    new-instance v1, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;-><init>(Lcom/sec/android/app/selftestmode/ProximityTest;Lcom/sec/android/app/selftestmode/ProximityTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mlistner:Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;

    .line 71
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/ProximityTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 73
    .local v0, "sensor":Landroid/hardware/Sensor;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mlistner:Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 74
    const v1, 0x7f0a010b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/ProximityTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    .line 75
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/ProximityTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mVibrator:Landroid/os/Vibrator;

    .line 76
    return-void
.end method

.method private isWorking()V
    .locals 3

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->working:Z

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->info:Landroid/widget/TextView;

    const v1, 0x7f070099

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 82
    const-string v0, "Proximity Test"

    const-string v1, "isWorking"

    const-string v2, "Working"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->info:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->info:Landroid/widget/TextView;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 87
    const-string v0, "Proximity Test"

    const-string v1, "isWorking"

    const-string v2, "Release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startVibration()V
    .locals 3

    .prologue
    .line 105
    const-string v0, "Proximity Test"

    const-string v1, "startVibration"

    const-string v2, "Vibration start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mVibrator:Landroid/os/Vibrator;

    const/4 v1, 0x2

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 109
    return-void

    .line 106
    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 113
    const-string v0, "Proximity Test"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v0, 0x7f030050

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/ProximityTest;->setContentView(I)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/ProximityTest;->init()V

    .line 50
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 66
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/ProximityTest;->mlistner:Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 60
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 61
    const-string v0, "Proximity Test"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 54
    const-string v0, "Proximity Test"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method
