.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;
.super Landroid/os/Handler;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;)V
    .locals 0

    .prologue
    .line 1505
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;->this$0:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1507
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 1523
    :cond_0
    return-void

    .line 1509
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1510
    .local v2, "status":Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;->this$0:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    iget-object v4, v4, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;

    .line 1511
    .local v1, "listener":Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;
    invoke-interface {v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;->onStatusChanged(Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;)V

    goto :goto_0

    .line 1515
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;
    .end local v2    # "status":Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
    :pswitch_1
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    move-object v3, v4

    check-cast v3, [Ljava/lang/String;

    .line 1516
    .local v3, "value":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;->this$0:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    iget-object v4, v4, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;

    .line 1517
    .restart local v1    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;
    invoke-interface {v1, v3}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;->onUpdate([Ljava/lang/String;)V

    goto :goto_1

    .line 1507
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
