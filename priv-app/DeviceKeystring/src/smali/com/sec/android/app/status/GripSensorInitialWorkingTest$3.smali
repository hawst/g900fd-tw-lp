.class Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;
.super Landroid/os/Handler;
.source "GripSensorInitialWorkingTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 1

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 224
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->count:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 226
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 250
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->workingGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1200(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->releaseGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    goto :goto_0

    .line 236
    :pswitch_2
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "thread logs"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "log"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1300(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1100(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1300(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1100(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 226
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
