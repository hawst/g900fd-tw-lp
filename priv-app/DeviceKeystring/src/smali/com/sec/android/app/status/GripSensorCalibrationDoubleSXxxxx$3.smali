.class Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;
.super Landroid/os/Handler;
.source "GripSensorCalibrationDoubleSXxxxx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V
    .locals 0

    .prologue
    .line 583
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x31

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 585
    const/4 v5, 0x0

    .line 586
    .local v5, "result":Z
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 648
    :cond_0
    :goto_0
    return-void

    .line 588
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->calibration(B)Z
    invoke-static {v6, v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;B)Z

    move-result v5

    .line 589
    if-nez v5, :cond_0

    .line 590
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "onClick"

    const-string v8, "gripsensor_cal_fail"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    const/16 v7, 0x30

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->calibration(B)Z
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;B)Z

    move-result v5

    .line 595
    if-nez v5, :cond_0

    .line 596
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "onClick"

    const-string v8, "gripsensor_calerase_fail"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 600
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/hardware/SensorManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;
    invoke-static {v7}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 601
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    const-string v7, "/sys/class/sensors/grip_sensor/reset"

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$800(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Ljava/lang/String;B)Z

    goto :goto_0

    .line 604
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Z)V

    .line 605
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "MSG_UPDATE_PROGRESS"

    const-string v8, "Doing Cal.."

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v6, v10}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Z)V

    goto :goto_0

    .line 611
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 612
    .local v0, "caldatas":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 613
    .local v1, "caldatas2":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "Waiting"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 614
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "Waiting"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v6, v10}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Z)V

    .line 616
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 617
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "MSG_UPDATE_UI"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCalSuccess() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess()Z
    invoke-static {v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Y("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v0, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 623
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess2()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 624
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "MSG_UPDATE_UI"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCalSuccess() : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess2()Z
    invoke-static {v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Y("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v1, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v1, v11

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 621
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "N(0, 0)"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 628
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;

    move-result-object v6

    const-string v7, "N(0, 0)"

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 632
    .end local v0    # "caldatas":[Ljava/lang/String;
    .end local v1    # "caldatas2":[Ljava/lang/String;
    :pswitch_6
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v2

    .line 633
    .local v2, "iscalsuccess":Z
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess2()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v3

    .line 634
    .local v3, "iscalsuccess2":Z
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isSpecIn()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1800(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z

    move-result v4

    .line 635
    .local v4, "isspecin":Z
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "MSG_UPDATE_RESULT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isSpecIn(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    const-string v6, "GripSensorCalibrationDoubleSXxxxx"

    const-string v7, "MSG_UPDATE_RESULT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCalSuccess(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3eb

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 642
    .end local v2    # "iscalsuccess":Z
    .end local v3    # "iscalsuccess2":Z
    .end local v4    # "isspecin":Z
    :pswitch_7
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/hardware/SensorManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;
    invoke-static {v7}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$1900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/hardware/Sensor;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v11}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto/16 :goto_0

    .line 586
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method
