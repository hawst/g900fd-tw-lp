.class public Lcom/sec/android/app/status/melody_test;
.super Landroid/app/Activity;
.source "melody_test.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mp:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private mediaPlayStop()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    .line 135
    :cond_0
    return-void
.end method

.method private receiverMax()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 100
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 110
    return-void
.end method

.method private receiverMin()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 115
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 122
    return-void
.end method

.method private speakerOff()V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 127
    return-void
.end method

.method private stereoMax()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 73
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 83
    return-void
.end method

.method private stereoMin()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 88
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mp:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 95
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_0
    return-void

    .line 43
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->stereoMax()V

    goto :goto_0

    .line 46
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->receiverMax()V

    goto :goto_0

    .line 49
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->stereoMin()V

    goto :goto_0

    .line 52
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->receiverMin()V

    goto :goto_0

    .line 55
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->speakerOff()V

    goto :goto_0

    .line 58
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/status/melody_test;->finish()V

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x7f0a014b
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v6, 0x7f03004c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->setContentView(I)V

    .line 25
    const-string v6, "audio"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/sec/android/app/status/melody_test;->mAudioManager:Landroid/media/AudioManager;

    .line 26
    const v6, 0x7f0a014b

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 27
    .local v5, "buttonspeaker":Landroid/view/View;
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    const v6, 0x7f0a014c

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 29
    .local v4, "buttonreceiver":Landroid/view/View;
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    const v6, 0x7f0a014d

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 31
    .local v2, "buttonheadset":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    const v6, 0x7f0a014e

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "buttonReceiverHeadset":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    const v6, 0x7f0a014f

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 35
    .local v3, "buttonoff":Landroid/view/View;
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v6, 0x7f0a0150

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/melody_test;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 37
    .local v1, "buttonexit":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/status/melody_test;->mediaPlayStop()V

    .line 67
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 68
    return-void
.end method
