.class public Lcom/sec/android/app/status/PowerStatus;
.super Landroid/app/Activity;
.source "PowerStatus.java"


# instance fields
.field private mDumpSysResult:Ljava/lang/StringBuffer;

.field private mHandler:Landroid/os/Handler;

.field private mKWakeLockResult:Ljava/lang/StringBuffer;

.field private mPSResult:Ljava/lang/StringBuffer;

.field private mPidList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTextDumpSysView:Landroid/widget/TextView;

.field private mTextPStatusView:Landroid/widget/TextView;

.field private final runnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    .line 26
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    .line 27
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    .line 29
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    .line 71
    new-instance v0, Lcom/sec/android/app/status/PowerStatus$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/PowerStatus$1;-><init>(Lcom/sec/android/app/status/PowerStatus;)V

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/PowerStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PowerStatus;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/status/PowerStatus;->updateStatus()V

    return-void
.end method

.method private analyzeDumpsys()V
    .locals 20

    .prologue
    .line 137
    const/4 v11, 0x0

    .line 138
    .local v11, "reader":Ljava/io/BufferedReader;
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v2, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    const-string v18, "/system/bin/sh"

    aput-object v18, v2, v17

    const/16 v17, 0x1

    const-string v18, "-c"

    aput-object v18, v2, v17

    const/16 v17, 0x2

    const-string v18, "dumpsys power"

    aput-object v18, v2, v17

    .line 143
    .local v2, "command":[Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v9

    .line 144
    .local v9, "p":Ljava/lang/Process;
    new-instance v12, Ljava/io/BufferedReader;

    new-instance v17, Ljava/io/InputStreamReader;

    invoke-virtual {v9}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7

    .line 145
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .local v12, "reader":Ljava/io/BufferedReader;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    const-string v18, "**** dumpsys power ****\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 150
    const/4 v14, 0x0

    .line 151
    .local v14, "searchStarted":Z
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v12}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "line":Ljava/lang/String;
    if-eqz v6, :cond_9

    .line 154
    if-nez v14, :cond_1

    const-string v17, "Wake Locks.*"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 155
    const/4 v14, 0x1

    .line 158
    :cond_1
    if-eqz v14, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_2

    .line 159
    const/4 v14, 0x0

    .line 161
    :cond_2
    if-eqz v14, :cond_0

    .line 164
    const-string v17, ".*pid=.*"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 165
    const-string v17, "DumpsysPowerStatus"

    const-string v18, "found the matching line"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const-string v13, "pid=.*"

    .line 167
    .local v13, "regex":Ljava/lang/String;
    invoke-static {v13}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v10

    .line 168
    .local v10, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v10, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 170
    .local v7, "matcher":Ljava/util/regex/Matcher;
    :cond_3
    :goto_1
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 171
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v16

    .line 172
    .local v16, "str":Ljava/lang/String;
    const-string v17, "DumpsysPowerStatus"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "found the matching string "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const-string v17, "[=\\(\\) ,]"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 175
    .local v15, "ss":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-gt v0, v1, :cond_7

    .line 176
    const-string v17, "DumpsysPowerStatus"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "invalid parsing"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    array-length v0, v15

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    .end local v7    # "matcher":Ljava/util/regex/Matcher;
    .end local v10    # "pattern":Ljava/util/regex/Pattern;
    .end local v13    # "regex":Ljava/lang/String;
    .end local v15    # "ss":[Ljava/lang/String;
    .end local v16    # "str":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    const-string v18, "\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 189
    .end local v6    # "line":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 190
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v17, "DumpsysPowerStatus"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 193
    if-eqz v12, :cond_5

    .line 195
    :try_start_4
    invoke-virtual {v12}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5

    .end local v3    # "e":Ljava/lang/Exception;
    :cond_5
    :goto_2
    move-object v11, v12

    .line 209
    .end local v9    # "p":Ljava/lang/Process;
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .end local v14    # "searchStarted":Z
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    const-string v18, " **** suspicious PM wakelock owner ****\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    const-string v17, "DumpsysPowerStatus"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "pidList is = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 213
    .local v8, "num":I
    if-nez v8, :cond_a

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    move-object/from16 v17, v0

    const-string v18, " NONE\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    :cond_6
    return-void

    .line 180
    .end local v8    # "num":I
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v7    # "matcher":Ljava/util/regex/Matcher;
    .restart local v9    # "p":Ljava/lang/Process;
    .restart local v10    # "pattern":Ljava/util/regex/Pattern;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    .restart local v13    # "regex":Ljava/lang/String;
    .restart local v14    # "searchStarted":Z
    .restart local v15    # "ss":[Ljava/lang/String;
    .restart local v16    # "str":Ljava/lang/String;
    :cond_7
    :try_start_5
    const-string v17, "DumpsysPowerStatus"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "found pid = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x1

    aget-object v19, v15, v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    const/16 v17, 0x1

    aget-object v17, v15, v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/PowerStatus;->isInteger(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aget-object v18, v15, v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 193
    .end local v6    # "line":Ljava/lang/String;
    .end local v7    # "matcher":Ljava/util/regex/Matcher;
    .end local v10    # "pattern":Ljava/util/regex/Pattern;
    .end local v13    # "regex":Ljava/lang/String;
    .end local v15    # "ss":[Ljava/lang/String;
    .end local v16    # "str":Ljava/lang/String;
    :catchall_0
    move-exception v17

    if-eqz v12, :cond_8

    .line 195
    :try_start_6
    invoke-virtual {v12}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    .line 198
    :cond_8
    :goto_4
    :try_start_7
    throw v17
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 201
    .end local v14    # "searchStarted":Z
    :catch_1
    move-exception v4

    move-object v11, v12

    .line 202
    .end local v9    # "p":Ljava/lang/Process;
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .local v4, "exception":Ljava/io/IOException;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :goto_5
    const-string v17, "DumpsysPowerStatus"

    const-string v18, "IOException"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 193
    .end local v4    # "exception":Ljava/io/IOException;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v9    # "p":Ljava/lang/Process;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    .restart local v14    # "searchStarted":Z
    :cond_9
    if-eqz v12, :cond_5

    .line 195
    :try_start_8
    invoke-virtual {v12}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_2

    .line 196
    :catch_2
    move-exception v3

    .line 197
    .local v3, "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    goto/16 :goto_2

    .line 203
    .end local v3    # "e":Ljava/io/IOException;
    .end local v6    # "line":Ljava/lang/String;
    .end local v14    # "searchStarted":Z
    :catch_3
    move-exception v4

    move-object v11, v12

    .line 204
    .end local v9    # "p":Ljava/lang/Process;
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .local v4, "exception":Ljava/lang/SecurityException;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :goto_6
    const-string v17, "DumpsysPowerStatus"

    const-string v18, "SecurityException"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 196
    .end local v4    # "exception":Ljava/lang/SecurityException;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v9    # "p":Ljava/lang/Process;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    .restart local v14    # "searchStarted":Z
    :catch_4
    move-exception v3

    .line 197
    .local v3, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    goto/16 :goto_2

    .line 205
    .end local v3    # "e":Ljava/io/IOException;
    .end local v14    # "searchStarted":Z
    :catch_5
    move-exception v3

    move-object v11, v12

    .line 206
    .end local v9    # "p":Ljava/lang/Process;
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :goto_7
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 196
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "p":Ljava/lang/Process;
    .restart local v12    # "reader":Ljava/io/BufferedReader;
    .restart local v14    # "searchStarted":Z
    :catch_6
    move-exception v3

    .line 197
    .local v3, "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_4

    .line 216
    .end local v3    # "e":Ljava/io/IOException;
    .end local v9    # "p":Ljava/lang/Process;
    .end local v12    # "reader":Ljava/io/BufferedReader;
    .end local v14    # "searchStarted":Z
    .restart local v8    # "num":I
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :cond_a
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_8
    if-ge v5, v8, :cond_6

    .line 217
    const-string v18, "DumpsysPowerStatus"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "pid = "

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/PowerStatus;->execPS(Ljava/lang/String;)V

    .line 216
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 205
    .end local v5    # "i":I
    .end local v8    # "num":I
    :catch_7
    move-exception v3

    goto :goto_7

    .line 203
    :catch_8
    move-exception v4

    goto :goto_6

    .line 201
    :catch_9
    move-exception v4

    goto/16 :goto_5
.end method

.method private analyzeKernelWakelocks()V
    .locals 18

    .prologue
    .line 225
    const/4 v7, 0x0

    .line 226
    .local v7, "reader":Ljava/io/BufferedReader;
    const/4 v10, 0x0

    .line 228
    .local v10, "wakeup_sources":Z
    const-string v11, "DumpsysPowerStatus"

    const-string v12, "analyze kernel wakelock "

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    const-string v11, "/proc/wakelocks"

    invoke-direct {v5, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_d
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    .local v5, "is":Ljava/io/FileInputStream;
    :goto_0
    :try_start_1
    new-instance v8, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    invoke-direct {v11, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v8, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .local v8, "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    :try_start_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "line":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 247
    const-string v11, "name.*"

    invoke-virtual {v6, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 248
    const-string v11, "[\\t]+"

    invoke-virtual {v6, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 249
    .local v9, "ss":[Ljava/lang/String;
    array-length v11, v9

    const/4 v12, 0x1

    if-gt v11, v12, :cond_3

    .line 250
    const-string v11, "DumpsysPowerStatus"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "invalid parsing"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v9

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    .end local v9    # "ss":[Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 282
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    const-string v12, " **** kernel wakelock ****\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 283
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 292
    if-eqz v8, :cond_6

    .line 294
    :try_start_3
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    move-object v7, v8

    .line 300
    .end local v5    # "is":Ljava/io/FileInputStream;
    .end local v6    # "line":Ljava/lang/String;
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_2
    return-void

    .line 233
    :catch_0
    move-exception v2

    .line 235
    .local v2, "e":Ljava/io/FileNotFoundException;
    :try_start_4
    const-string v11, "DumpsysPowerStatus"

    const-string v12, "file not found /proc/wakelocks"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    new-instance v5, Ljava/io/FileInputStream;

    const-string v11, "/sys/kernel/debug/wakeup_sources"

    invoke-direct {v5, v11}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_c
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 237
    .restart local v5    # "is":Ljava/io/FileInputStream;
    const/4 v10, 0x1

    goto :goto_0

    .line 238
    .end local v5    # "is":Ljava/io/FileInputStream;
    :catch_1
    move-exception v3

    .line 292
    .local v3, "e2":Ljava/io/FileNotFoundException;
    if-eqz v7, :cond_2

    .line 294
    :try_start_5
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    .line 295
    :catch_2
    move-exception v2

    .line 296
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 254
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "e2":Ljava/io/FileNotFoundException;
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "ss":[Ljava/lang/String;
    :cond_3
    if-nez v10, :cond_4

    .line 255
    :try_start_6
    const-string v11, "0"

    const/4 v12, 0x4

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 256
    const-string v11, "DumpsysPowerStatus"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "found active wakelock_name = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v13, v9, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " active_since = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x4

    aget-object v13, v9, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v11, "DumpsysPowerStatus"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "active_since = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x4

    aget-object v13, v9, v13

    invoke-static {v13}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const-wide/32 v16, 0x3b9aca00

    div-long v14, v14, v16

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 262
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const-string v12, " is active for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 263
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const/4 v12, 0x4

    aget-object v12, v9, v12

    invoke-static {v12}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    const-wide/32 v14, 0xf4240

    div-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 264
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const-string v12, "ms\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 285
    .end local v6    # "line":Ljava/lang/String;
    .end local v9    # "ss":[Ljava/lang/String;
    :catch_3
    move-exception v4

    move-object v7, v8

    .line 286
    .end local v5    # "is":Ljava/io/FileInputStream;
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .local v4, "exception":Ljava/io/IOException;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    :goto_3
    :try_start_7
    const-string v11, "DumpsysPowerStatus"

    const-string v12, "IOException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 292
    if-eqz v7, :cond_2

    .line 294
    :try_start_8
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_2

    .line 295
    :catch_4
    move-exception v2

    .line 296
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 267
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "exception":Ljava/io/IOException;
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "ss":[Ljava/lang/String;
    :cond_4
    :try_start_9
    const-string v11, "0"

    const/4 v12, 0x5

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 268
    const-string v11, "DumpsysPowerStatus"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "found active wakelock_name = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v13, v9, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " active_since = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x5

    aget-object v13, v9, v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const-string v11, "DumpsysPowerStatus"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "active_since = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/4 v13, 0x5

    aget-object v13, v9, v13

    invoke-static {v13}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const/4 v12, 0x0

    aget-object v12, v9, v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 273
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const-string v12, " is active for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 274
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const/4 v12, 0x5

    aget-object v12, v9, v12

    invoke-static {v12}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 275
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    const-string v12, "ms\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto/16 :goto_1

    .line 287
    .end local v6    # "line":Ljava/lang/String;
    .end local v9    # "ss":[Ljava/lang/String;
    :catch_5
    move-exception v4

    move-object v7, v8

    .line 288
    .end local v5    # "is":Ljava/io/FileInputStream;
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .local v4, "exception":Ljava/lang/SecurityException;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    :goto_4
    :try_start_a
    const-string v11, "DumpsysPowerStatus"

    const-string v12, "SecurityException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 292
    if-eqz v7, :cond_2

    .line 294
    :try_start_b
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto/16 :goto_2

    .line 295
    :catch_6
    move-exception v2

    .line 296
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 295
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "exception":Ljava/lang/SecurityException;
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catch_7
    move-exception v2

    .line 296
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v7, v8

    .line 297
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 289
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "is":Ljava/io/FileInputStream;
    .end local v6    # "line":Ljava/lang/String;
    :catch_8
    move-exception v2

    .line 290
    .local v2, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_c
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 292
    if-eqz v7, :cond_2

    .line 294
    :try_start_d
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    goto/16 :goto_2

    .line 295
    :catch_9
    move-exception v2

    .line 296
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 292
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_6
    if-eqz v7, :cond_5

    .line 294
    :try_start_e
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    .line 297
    :cond_5
    :goto_7
    throw v11

    .line 295
    :catch_a
    move-exception v2

    .line 296
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 292
    .end local v2    # "e":Ljava/io/IOException;
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v11

    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto :goto_6

    .line 289
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catch_b
    move-exception v2

    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 287
    .end local v5    # "is":Ljava/io/FileInputStream;
    :catch_c
    move-exception v4

    goto :goto_4

    .line 285
    :catch_d
    move-exception v4

    goto/16 :goto_3

    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "is":Ljava/io/FileInputStream;
    .restart local v6    # "line":Ljava/lang/String;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :cond_6
    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_2
.end method

.method private execPS(Ljava/lang/String;)V
    .locals 10
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 93
    const/4 v5, 0x0

    .line 94
    .local v5, "reader":Ljava/io/BufferedReader;
    const/4 v7, 0x3

    new-array v0, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "/system/bin/sh"

    aput-object v8, v0, v7

    const/4 v7, 0x1

    const-string v8, "-c"

    aput-object v8, v0, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ps -p "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v7

    .line 97
    .local v0, "command":[Ljava/lang/String;
    const-string v7, "DumpsysPowerStatus"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "execPS : ps -p "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v4

    .line 101
    .local v4, "p":Ljava/lang/Process;
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .local v6, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 105
    const-string v7, "DumpsysPowerStatus"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ": ps "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v7, p0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    iget-object v7, p0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 109
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v5, v6

    .line 110
    .end local v4    # "p":Ljava/lang/Process;
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .local v2, "exception":Ljava/io/IOException;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    const-string v7, "DumpsysPowerStatus"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 116
    if-eqz v5, :cond_0

    .line 118
    :try_start_3
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 124
    .end local v2    # "exception":Ljava/io/IOException;
    :cond_0
    :goto_2
    return-void

    .line 116
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "p":Ljava/lang/Process;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :cond_1
    if-eqz v6, :cond_3

    .line 118
    :try_start_4
    invoke-virtual {v6}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v5, v6

    .line 121
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 119
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .line 120
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 121
    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 119
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "p":Ljava/lang/Process;
    .restart local v2    # "exception":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 120
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 111
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "exception":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 112
    .local v2, "exception":Ljava/lang/SecurityException;
    :goto_3
    :try_start_5
    const-string v7, "DumpsysPowerStatus"

    const-string v8, "SecurityException"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 116
    if-eqz v5, :cond_0

    .line 118
    :try_start_6
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 119
    :catch_4
    move-exception v1

    .line 120
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 113
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "exception":Ljava/lang/SecurityException;
    :catch_5
    move-exception v1

    .line 114
    .local v1, "e":Ljava/lang/Exception;
    :goto_4
    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 116
    if-eqz v5, :cond_0

    .line 118
    :try_start_8
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_2

    .line 119
    :catch_6
    move-exception v1

    .line 120
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 116
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_5
    if-eqz v5, :cond_2

    .line 118
    :try_start_9
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 121
    :cond_2
    :goto_6
    throw v7

    .line 119
    :catch_7
    move-exception v1

    .line 120
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 116
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "p":Ljava/lang/Process;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 113
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catch_8
    move-exception v1

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 111
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :catch_9
    move-exception v2

    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 109
    .end local v4    # "p":Ljava/lang/Process;
    :catch_a
    move-exception v2

    goto :goto_1

    .end local v5    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "p":Ljava/lang/Process;
    .restart local v6    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v5, v6

    .end local v6    # "reader":Ljava/io/BufferedReader;
    .restart local v5    # "reader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private isInteger(Ljava/lang/String;)Z
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 128
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateStatus()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mKWakeLockResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/status/PowerStatus;->analyzeDumpsys()V

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/status/PowerStatus;->analyzeKernelWakelocks()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextDumpSysView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextDumpSysView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->mDumpSysResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextPStatusView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextPStatusView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->mPSResult:Ljava/lang/StringBuffer;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 89
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PowerStatus;->setContentView(I)V

    .line 65
    const v0, 0x7f0a015a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PowerStatus;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextPStatusView:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextPStatusView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 67
    const v0, 0x7f0a015b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PowerStatus;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextDumpSysView:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mTextDumpSysView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 58
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mPidList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 47
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 37
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/PowerStatus;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/PowerStatus;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 40
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 41
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 32
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onStart"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 34
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 51
    const-string v0, "DumpsysPowerStatus"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 53
    return-void
.end method
