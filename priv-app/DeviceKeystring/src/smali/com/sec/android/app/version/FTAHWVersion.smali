.class public Lcom/sec/android/app/version/FTAHWVersion;
.super Landroid/app/Activity;
.source "FTAHWVersion.java"


# instance fields
.field ftaHwvertv:Landroid/widget/TextView;

.field public mReceiver:Landroid/content/BroadcastReceiver;

.field result:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 17
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/version/FTAHWVersion;->result:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/version/FTAHWVersion;->ftaHwvertv:Landroid/widget/TextView;

    .line 37
    new-instance v0, Lcom/sec/android/app/version/FTAHWVersion$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/version/FTAHWVersion$1;-><init>(Lcom/sec/android/app/version/FTAHWVersion;)V

    iput-object v0, p0, Lcom/sec/android/app/version/FTAHWVersion;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private startReceiver()V
    .locals 2

    .prologue
    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 33
    .local v0, "mFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.GET_FTA_RESPONSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/sec/android/app/version/FTAHWVersion;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/version/FTAHWVersion;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 35
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v1, 0x7f030018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/version/FTAHWVersion;->setContentView(I)V

    .line 25
    const v1, 0x7f0a0068

    invoke-virtual {p0, v1}, Lcom/sec/android/app/version/FTAHWVersion;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/version/FTAHWVersion;->ftaHwvertv:Landroid/widget/TextView;

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/version/FTAHWVersion;->startReceiver()V

    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_FTA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/version/FTAHWVersion;->sendBroadcast(Landroid/content/Intent;)V

    .line 29
    return-void
.end method
