.class Lcom/sec/android/app/status/touch_firmware$8;
.super Ljava/lang/Object;
.source "touch_firmware.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/touch_firmware;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/touch_firmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/touch_firmware;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    .line 253
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/hlte_cypress_tkey.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 254
    .local v1, "Cypress_tkey":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/hlte_coreriver_tkey.bin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 256
    .local v0, "Coreriver_tkey":Ljava/io/File;
    const-string v2, "SUPPORT_TSK_FW_UPDATE_INTERNAL_FOR_H"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 257
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->startNewThread(I)Z
    invoke-static {v2, v4}, Lcom/sec/android/app/status/touch_firmware;->access$300(Lcom/sec/android/app/status/touch_firmware;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v2, v2, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    .line 270
    :cond_1
    :goto_0
    return-void

    .line 262
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const-string v3, "Not Found File :\n/hlte_cypress_tkey.bin or\n/hlte_coreriver_tkey.bin"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 266
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->startNewThread(I)Z
    invoke-static {v2, v4}, Lcom/sec/android/app/status/touch_firmware;->access$300(Lcom/sec/android/app/status/touch_firmware;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware$8;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v2, v2, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0
.end method
