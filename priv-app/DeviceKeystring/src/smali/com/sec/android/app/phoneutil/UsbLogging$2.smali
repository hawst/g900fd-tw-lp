.class Lcom/sec/android/app/phoneutil/UsbLogging$2;
.super Ljava/lang/Object;
.source "UsbLogging.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/phoneutil/UsbLogging;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/phoneutil/UsbLogging;


# direct methods
.method constructor <init>(Lcom/sec/android/app/phoneutil/UsbLogging;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/phoneutil/UsbLogging$2;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    const/4 v5, 0x0

    .line 106
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$2;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    # getter for: Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$000(Lcom/sec/android/app/phoneutil/UsbLogging;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "usblogging.preferences_name"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 108
    .local v2, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 110
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    packed-switch p2, :pswitch_data_0

    .line 156
    :goto_0
    return-void

    .line 114
    :pswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$2;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    const-string v4, "CPONLY"

    # invokes: Lcom/sec/android/app/phoneutil/UsbLogging;->loggingstatus(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$200(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V

    .line 116
    const-string v3, "CPONLY"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 117
    const-string v3, "APONLY"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 118
    const-string v3, "CPAP"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 119
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$2;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    const-string v4, "APONLY"

    # invokes: Lcom/sec/android/app/phoneutil/UsbLogging;->loggingstatus(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$200(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V

    .line 130
    const-string v3, "CPONLY"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 131
    const-string v3, "APONLY"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 132
    const-string v3, "CPAP"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 133
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 134
    :catch_1
    move-exception v0

    .line 135
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 142
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$2;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    const-string v4, "CPAP"

    # invokes: Lcom/sec/android/app/phoneutil/UsbLogging;->loggingstatus(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$200(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V

    .line 144
    const-string v3, "CPONLY"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 145
    const-string v3, "APONLY"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 146
    const-string v3, "CPAP"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 147
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 148
    :catch_2
    move-exception v0

    .line 149
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a02af
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
