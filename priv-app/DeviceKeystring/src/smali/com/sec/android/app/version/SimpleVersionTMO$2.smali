.class Lcom/sec/android/app/version/SimpleVersionTMO$2;
.super Landroid/os/Handler;
.source "SimpleVersionTMO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/version/SimpleVersionTMO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/version/SimpleVersionTMO;


# direct methods
.method constructor <init>(Lcom/sec/android/app/version/SimpleVersionTMO;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 204
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "error"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 206
    .local v1, "error":I
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 208
    :pswitch_0
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    const-string v8, "in QUERT_SERVM_DONE"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    if-eqz v1, :cond_2

    .line 211
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    const-string v8, "Exception Occur!!!"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # invokes: Lcom/sec/android/app/version/SimpleVersionTMO;->UpdateFTAVersion()V
    invoke-static {v6}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$500(Lcom/sec/android/app/version/SimpleVersionTMO;)V

    goto :goto_0

    .line 213
    :cond_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "response"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    check-cast v0, [B

    .line 215
    .local v0, "buf":[B
    if-eqz v0, :cond_0

    .line 219
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "size of result : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    array-length v6, v0

    div-int/lit8 v6, v6, 0x22

    add-int/lit8 v4, v6, 0x1

    .line 224
    .local v4, "numOfLine":I
    iget-object v6, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$400(Lcom/sec/android/app/version/SimpleVersionTMO;)[Ljava/lang/String;

    move-result-object v6

    array-length v6, v6

    if-le v4, v6, :cond_3

    .line 225
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Too many lines. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v6, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$400(Lcom/sec/android/app/version/SimpleVersionTMO;)[Ljava/lang/String;

    move-result-object v6

    array-length v4, v6

    .line 229
    :cond_3
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_1

    .line 230
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 232
    .local v5, "rowString":Ljava/lang/StringBuffer;
    const/4 v3, 0x2

    .local v3, "j":I
    :goto_2
    const/16 v6, 0x22

    if-ge v3, v6, :cond_4

    .line 233
    mul-int/lit8 v6, v2, 0x22

    add-int/2addr v6, v3

    array-length v7, v0

    if-lt v6, v7, :cond_5

    .line 234
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    const-string v8, "unexpected end of line"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_4
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$400(Lcom/sec/android/app/version/SimpleVersionTMO;)[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 247
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    iget-object v8, p0, Lcom/sec/android/app/version/SimpleVersionTMO$2;->this$0:Lcom/sec/android/app/version/SimpleVersionTMO;

    # getter for: Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/version/SimpleVersionTMO;->access$400(Lcom/sec/android/app/version/SimpleVersionTMO;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v2

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 238
    :cond_5
    mul-int/lit8 v6, v2, 0x22

    add-int/2addr v6, v3

    aget-byte v6, v0, v6

    if-nez v6, :cond_6

    .line 239
    const-string v6, "SimpleVersion"

    const-string v7, "handleMessage"

    const-string v8, "break"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 243
    :cond_6
    mul-int/lit8 v6, v2, 0x22

    add-int/2addr v6, v3

    aget-byte v6, v0, v6

    int-to-char v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 232
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 206
    nop

    :pswitch_data_0
    .packed-switch 0x3f0
        :pswitch_0
    .end packed-switch
.end method
