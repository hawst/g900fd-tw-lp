.class public Lcom/sec/android/app/selftestmode/LightSensorTest;
.super Landroid/app/Activity;
.source "LightSensorTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/LightSensorTest$1;,
        Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private iluxval:I

.field private info:Landroid/widget/TextView;

.field private isVibrationOn:Z

.field private level:I

.field private mBackgroudLayout:Landroid/widget/LinearLayout;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mVibrator:Landroid/os/Vibrator;

.field private mlistner:Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const-string v0, "LightSensorTest"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->CLASS_NAME:Ljava/lang/String;

    .line 153
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/selftestmode/LightSensorTest;Landroid/hardware/SensorEvent;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/LightSensorTest;
    .param p1, "x1"    # Landroid/hardware/SensorEvent;
    .param p2, "x2"    # F

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/selftestmode/LightSensorTest;->changeLightSensor(Landroid/hardware/SensorEvent;F)V

    return-void
.end method

.method private changeLightSensor(Landroid/hardware/SensorEvent;F)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;
    .param p2, "value"    # F

    .prologue
    const/16 v5, 0x96

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 85
    float-to-int v0, p2

    iput v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    .line 87
    const-string v0, "IS_LIGHT_SENSOR_LEVEL5"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 92
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    const/16 v1, 0xf

    if-gt v0, v1, :cond_2

    .line 93
    iput v4, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    .line 117
    :cond_0
    :goto_0
    const-string v0, "LightSensorTest"

    const-string v1, "changeLightSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "iluxval : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    if-ne v0, v4, :cond_9

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->info:Landroid/widget/TextView;

    const v1, 0x7f07023e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 123
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->isVibrationOn:Z

    if-nez v0, :cond_1

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->startVibration()V

    .line 132
    :cond_1
    :goto_1
    return-void

    .line 94
    :cond_2
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    if-gt v0, v5, :cond_3

    .line 95
    iput v2, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 96
    :cond_3
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    const/16 v1, 0x5dc

    if-gt v0, v1, :cond_4

    .line 97
    iput v3, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 98
    :cond_4
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    const/16 v1, 0x3a98

    if-gt v0, v1, :cond_5

    .line 99
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 100
    :cond_5
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    const v1, 0x989680

    if-gt v0, v1, :cond_0

    .line 101
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 108
    :cond_6
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_7

    .line 109
    iput v4, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 110
    :cond_7
    iget v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->iluxval:I

    if-ge v0, v5, :cond_8

    .line 111
    iput v2, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 113
    :cond_8
    iput v3, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->level:I

    goto :goto_0

    .line 127
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->info:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->info:Landroid/widget/TextView;

    const v1, 0x7f07023f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 130
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->stopVibration()V

    goto :goto_1
.end method

.method private init()V
    .locals 4

    .prologue
    .line 74
    const v1, 0x7f0a010c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/LightSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->info:Landroid/widget/TextView;

    .line 75
    new-instance v1, Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;-><init>(Lcom/sec/android/app/selftestmode/LightSensorTest;Lcom/sec/android/app/selftestmode/LightSensorTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mlistner:Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;

    .line 76
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/LightSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 78
    .local v0, "sensor":Landroid/hardware/Sensor;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mlistner:Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 79
    const v1, 0x7f0a010b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/LightSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mBackgroudLayout:Landroid/widget/LinearLayout;

    .line 80
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/selftestmode/LightSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mVibrator:Landroid/os/Vibrator;

    .line 81
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->isVibrationOn:Z

    .line 82
    return-void
.end method

.method private startVibration()V
    .locals 3

    .prologue
    .line 141
    const-string v0, "LightSensorTest"

    const-string v1, "startVibration"

    const-string v2, "Vibration start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mVibrator:Landroid/os/Vibrator;

    const/4 v1, 0x2

    new-array v1, v1, [J

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->isVibrationOn:Z

    .line 146
    return-void

    .line 142
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibration()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->isVibrationOn:Z

    .line 151
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v0, 0x7f030040

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->setContentView(I)V

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/LightSensorTest;->init()V

    .line 54
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->isVibrationOn:Z

    .line 71
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest;->mlistner:Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 64
    const-string v0, "LightSensorTest"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 66
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 58
    const-string v0, "LightSensorTest"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method
