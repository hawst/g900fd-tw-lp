.class Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTestDouble.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 98
    const-wide/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOn()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$900(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 104
    const-wide/16 v2, 0xc8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 108
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold2()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData2()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResetButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$500(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 113
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 105
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 106
    .restart local v0    # "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method
