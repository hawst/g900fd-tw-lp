.class public Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;
.super Ljava/lang/Object;
.source "LoopbackTest2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/LoopbackTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudioRecording"
.end annotation


# instance fields
.field private audioSource:I

.field buffer:[B

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBufferSize:I

.field private mHandler:Landroid/os/Handler;

.field private mIsRecording:Z

.field private mReadBytes:I

.field private mRecorder:Landroid/media/AudioRecord;

.field public mThread:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 469
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 454
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->audioSource:I

    .line 459
    iput v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    .line 460
    iput-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->buffer:[B

    .line 462
    iput-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;

    .line 463
    iput v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mReadBytes:I

    .line 464
    iput-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    .line 465
    iput-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mIsRecording:Z

    .line 470
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;

    .line 471
    iput-object p2, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mHandler:Landroid/os/Handler;

    .line 472
    return-void
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;
    .param p1, "x1"    # I

    .prologue
    .line 450
    iput p1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mReadBytes:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .prologue
    .line 450
    iget v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;
    .param p1, "x1"    # Landroid/media/AudioRecord;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;

    return-object p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;
    .param p1, "x1"    # Z

    .prologue
    .line 450
    iput-boolean p1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mIsRecording:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 450
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->getParameter(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getParameter(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p1, "keys"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 518
    const-string v9, "LoopbackTest2.AudioRecording"

    const-string v10, "MicTestRecoder.getParameter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "keys: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-string v9, "LoopbackTest2.AudioRecording"

    const-string v10, "MicTestRecoder.getParameter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "target: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 522
    :cond_0
    const/4 v6, -0x1

    .line 547
    :cond_1
    :goto_0
    return v6

    .line 525
    :cond_2
    const-string v5, "-1"

    .line 526
    .local v5, "param":Ljava/lang/String;
    const/4 v6, -0x1

    .line 528
    .local v6, "rms":I
    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 529
    const-string v0, ";"

    .line 530
    .local v0, "DELIMITER":Ljava/lang/String;
    const-string v9, ";"

    invoke-virtual {p1, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 531
    .local v8, "values":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_4

    aget-object v7, v1, v2

    .line 532
    .local v7, "value":Ljava/lang/String;
    invoke-virtual {v7, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 531
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 535
    :cond_3
    const-string v9, ""

    invoke-virtual {v7, p2, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 539
    .end local v0    # "DELIMITER":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v7    # "value":Ljava/lang/String;
    .end local v8    # "values":[Ljava/lang/String;
    :cond_4
    const-string v9, "LoopbackTest2.AudioRecording"

    const-string v10, "MicTestRecoder.getParameter"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "target param: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    if-eqz v5, :cond_1

    .line 542
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    goto :goto_0

    .line 544
    :catch_0
    move-exception v4

    .line 545
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public getRMS()Ljava/lang/String;
    .locals 5

    .prologue
    .line 568
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "rms_value"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 569
    .local v0, "value":Ljava/lang/String;
    const-string v1, "LoopbackTest2.AudioRecording"

    const-string v2, "getRMS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    return-object v0
.end method

.method protected initInputSetting(Ljava/lang/String;)Z
    .locals 7
    .param p1, "inputDevice"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x3e80

    const/16 v3, 0xc

    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 475
    const-string v0, "sub"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=sub"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 482
    :cond_0
    :goto_0
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    .line 483
    iget v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    .line 484
    iget v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->buffer:[B

    .line 487
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->audioSource:I

    iget v5, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;

    .line 490
    iput v6, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mReadBytes:I

    .line 492
    return v6

    .line 477
    :cond_1
    const-string v0, "main"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=main"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startRecording()Z
    .locals 3

    .prologue
    .line 575
    const-string v0, "LoopbackTest2.AudioRecording"

    const-string v1, "startRecording"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;-><init>(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 626
    const-string v0, "LoopbackTest2.AudioRecording"

    const-string v1, "startRecording"

    const-string v2, "Started"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const/4 v0, 0x1

    return v0
.end method

.method public stopRecording()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 631
    const-string v1, "LoopbackTest2.AudioRecording"

    const-string v2, "stopRecording"

    invoke-static {v1, v2, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 634
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mIsRecording:Z

    if-eqz v1, :cond_0

    .line 636
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 637
    :catch_0
    move-exception v0

    .line 638
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 642
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    iput-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    .line 643
    const-string v1, "LoopbackTest2.AudioRecording"

    const-string v2, "stopRecording"

    const-string v3, "Done: "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    const/4 v1, 0x1

    return v1
.end method
