.class public Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
.super Landroid/app/Activity;
.source "GripSensorInitialWorkingTestDouble.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;
    }
.end annotation


# instance fields
.field private Grip_status_ON_1:Z

.field private Grip_status_ON_2:Z

.field private gripSensorName:Ljava/lang/String;

.field private gripSensorName2:Ljava/lang/String;

.field private mBackgroundLinearLayout:Landroid/widget/LinearLayout;

.field private mBackgroundLinearLayout2:Landroid/widget/LinearLayout;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field public mInitialThreshold:I

.field public mInitialThreshold2:I

.field public mIsWorking:Z

.field public mIsWorking2:Z

.field private mMode:Ljava/lang/String;

.field private mMode2:Ljava/lang/String;

.field private mResetButton:Landroid/widget/Button;

.field private mResultTextView:Landroid/widget/TextView;

.field private mResultTextView2:Landroid/widget/TextView;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThreadGetData:Ljava/lang/Thread;

.field private mThreadGetData2:Ljava/lang/Thread;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    .line 56
    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    .line 57
    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    .line 58
    iput v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mInitialThreshold:I

    .line 59
    iput v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mInitialThreshold2:I

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mGripSensor:Landroid/hardware/Sensor;

    .line 64
    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z

    .line 65
    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z

    .line 325
    new-instance v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    .line 494
    return-void
.end method

.method private Init()V
    .locals 5

    .prologue
    .line 374
    const-string v2, "GRIP_SENSOR_NAME"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    .line 375
    const-string v2, "GRIP_SENSOR_NAME_2ND"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    const-string v3, "SX9500"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    const-string v3, "SX9306"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    const-string v3, "AD7146"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 377
    :cond_0
    const-string v2, "GRIP_SENSOR_MODE"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "gripMode":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 379
    const-string v2, "Initial"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    .line 383
    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    if-eqz v2, :cond_5

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Working"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 391
    .end local v0    # "gripMode":Ljava/lang/String;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Release"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 393
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    const-string v3, "SX9500"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    const-string v3, "SX9306"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    const-string v3, "AD7146"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 394
    :cond_2
    const-string v2, "GRIP_SENSOR_MODE_2ND"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 395
    .local v1, "gripMode2":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 396
    const-string v2, "Initial"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    .line 400
    :cond_3
    :goto_2
    iget-boolean v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    if-eqz v2, :cond_8

    .line 401
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Working"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    .end local v1    # "gripMode2":Ljava/lang/String;
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Release"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    return-void

    .line 380
    .restart local v0    # "gripMode":Ljava/lang/String;
    :cond_4
    const-string v2, "1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 381
    const-string v2, "Normal"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    goto/16 :goto_0

    .line 386
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Release"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 388
    .end local v0    # "gripMode":Ljava/lang/String;
    :cond_6
    const-string v2, "Initial"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    goto/16 :goto_1

    .line 397
    .restart local v1    # "gripMode2":Ljava/lang/String;
    :cond_7
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 398
    const-string v2, "Normal"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    goto :goto_2

    .line 403
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Release"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 405
    .end local v1    # "gripMode2":Ljava/lang/String;
    :cond_9
    const-string v2, "Initial"

    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    goto :goto_3
.end method

.method private UpdateThreshold()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 146
    const-string v3, "GRIP_SENSOR_THRESHOLD"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "threshold":Ljava/lang/String;
    const-string v3, "GripSensorInitialWorkingTestDouble"

    const-string v4, "UpdateThreshold"

    invoke-static {v3, v4, v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    if-eqz v1, :cond_0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "thresholds":[Ljava/lang/String;
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mInitialThreshold:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_1
    return-void

    .line 148
    .end local v2    # "thresholds":[Ljava/lang/String;
    :cond_0
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "0"

    aput-object v4, v2, v3

    goto :goto_0

    .line 151
    .restart local v2    # "thresholds":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private UpdateThreshold2()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 156
    const-string v3, "GRIP_SENSOR_THRESHOLD_2ND"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "threshold2":Ljava/lang/String;
    const-string v3, "GripSensorInitialWorkingTestDouble"

    const-string v4, "UpdateThreshold2"

    invoke-static {v3, v4, v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    if-eqz v1, :cond_0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 160
    .local v2, "thresholds2":[Ljava/lang/String;
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mInitialThreshold2:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_1
    return-void

    .line 158
    .end local v2    # "thresholds2":[Ljava/lang/String;
    :cond_0
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "0"

    aput-object v4, v2, v3

    goto :goto_0

    .line 161
    .restart local v2    # "thresholds2":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 162
    .local v0, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold2()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData2()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->gripSensorName2:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->workingGripSensor()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->workingGripSensor2()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor2()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResetButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData2()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOff()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOn()V

    return-void
.end method

.method private releaseGripSensor()V
    .locals 4

    .prologue
    .line 428
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "releaseGripSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z

    if-nez v0, :cond_0

    .line 433
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopVibration()V

    .line 435
    :cond_0
    return-void
.end method

.method private releaseGripSensor2()V
    .locals 4

    .prologue
    .line 437
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "releaseGripSensor2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z

    if-nez v0, :cond_0

    .line 442
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopVibration()V

    .line 444
    :cond_0
    return-void
.end method

.method private sensorOff()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 466
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    .line 467
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    .line 468
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mGripSensor:Landroid/hardware/Sensor;

    .line 469
    return-void
.end method

.method private sensorOn()V
    .locals 5

    .prologue
    .line 447
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    if-nez v1, :cond_0

    .line 448
    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    .line 450
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 451
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    .line 453
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 454
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mGripSensor:Landroid/hardware/Sensor;

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 458
    .local v0, "result":Z
    const-string v1, "GripSensorInitialWorkingTestDouble"

    const-string v2, "sensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method private startThreadGettingRawData()V
    .locals 4

    .prologue
    .line 193
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "startThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData2()V

    .line 195
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$2;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 242
    return-void
.end method

.method private startThreadGettingRawData2()V
    .locals 4

    .prologue
    .line 244
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "startThreadGettingRawData2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData2()V

    .line 246
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$3;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 294
    return-void
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 167
    const-string v1, "GripSensorInitialWorkingTestDouble"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 173
    .local v0, "pattern":[J
    if-nez p1, :cond_2

    .line 174
    aput-wide v6, v0, v4

    .line 175
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 181
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 183
    .end local v0    # "pattern":[J
    :cond_1
    return-void

    .line 176
    .restart local v0    # "pattern":[J
    :cond_2
    if-ne p1, v5, :cond_0

    .line 177
    aput-wide v6, v0, v4

    .line 178
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 169
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopThreadGettingRawData()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 297
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 304
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 307
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    .line 308
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    return-void
.end method

.method private stopThreadGettingRawData2()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    .line 311
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "stopThreadGettingRawData2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 314
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 318
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 321
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    .line 322
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "stopThreadGettingRawData2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mThreadGetData2:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    return-void
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 186
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 190
    :cond_0
    return-void
.end method

.method private workingGripSensor()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 413
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "workingGripSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startVibration(I)V

    .line 418
    return-void
.end method

.method private workingGripSensor2()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 420
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "workingGripSensor2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startVibration(I)V

    .line 425
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 472
    const-string v4, "GripSensorInitialWorkingTestDouble"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/4 v3, 0x0

    .line 474
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 476
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 478
    const/4 v3, 0x1

    .line 483
    if-eqz v2, :cond_0

    .line 484
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 490
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorInitialWorkingTestDouble"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    return v3

    .line 486
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorInitialWorkingTestDouble"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 489
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 479
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 480
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorInitialWorkingTestDouble"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 483
    if-eqz v1, :cond_1

    .line 484
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 486
    :catch_2
    move-exception v0

    .line 487
    const-string v4, "GripSensorInitialWorkingTestDouble"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 482
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 483
    :goto_2
    if-eqz v1, :cond_2

    .line 484
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 488
    :cond_2
    :goto_3
    throw v4

    .line 486
    :catch_3
    move-exception v0

    .line 487
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorInitialWorkingTestDouble"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 482
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 479
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->setContentView(I)V

    .line 72
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mVibrator:Landroid/os/Vibrator;

    .line 74
    const v0, 0x7f0a000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    .line 75
    const v0, 0x7f0a00bd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mBackgroundLinearLayout2:Landroid/widget/LinearLayout;

    .line 76
    const v0, 0x7f0a00ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0a00bf

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0a00bb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResetButton:Landroid/widget/Button;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 141
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 143
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 131
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOff()V

    .line 133
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData()V

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData2()V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopVibration()V

    .line 136
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 137
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 120
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOn()V

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold()V

    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->UpdateThreshold2()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData()V

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->startThreadGettingRawData2()V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Init()V

    .line 127
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method
