.class Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;
.super Ljava/lang/Object;
.source "AccelerometerSensorTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field private final DOWN:I

.field private final LEFT:I

.field private final MAX_DIRECTION:I

.field private final RIGHT:I

.field private final UP:I

.field private curr_orientation:I

.field private mImageAccResource:[I

.field private mPhoneType:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 81
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 72
    iput v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->UP:I

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->DOWN:I

    .line 74
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->LEFT:I

    .line 75
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->RIGHT:I

    .line 76
    iput v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->MAX_DIRECTION:I

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mPhoneType:Ljava/lang/String;

    .line 79
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    .line 83
    iput v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->initDirection()V

    .line 85
    const-string v0, "DEVICE_TYPE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mDeviceType:Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->access$002(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;Ljava/lang/String;)Ljava/lang/String;

    .line 86
    const-string v0, "SUPPORT_PHONE_FUNCTION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mPhoneType:Ljava/lang/String;

    .line 88
    const-string v0, "tablet"

    # getter for: Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mDeviceType:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->access$000(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "false"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mPhoneType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->initTabletDirection()V

    .line 91
    :cond_0
    return-void
.end method

.method private initDirection()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x0

    const v2, 0x7f020003

    aput v2, v0, v1

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x1

    const/high16 v2, 0x7f020000

    aput v2, v0, v1

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x2

    const v2, 0x7f020001

    aput v2, v0, v1

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x3

    const v2, 0x7f020002

    aput v2, v0, v1

    .line 98
    return-void
.end method

.method private initTabletDirection()V
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x0

    const v2, 0x7f020002

    aput v2, v0, v1

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x1

    const v2, 0x7f020001

    aput v2, v0, v1

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x2

    const v2, 0x7f020003

    aput v2, v0, v1

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    const/4 v1, 0x3

    const/high16 v2, 0x7f020000

    aput v2, v0, v1

    .line 105
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 108
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 12
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const-wide/high16 v10, -0x3fbd000000000000L    # -38.0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 111
    iget-object v2, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v2}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 112
    new-array v0, v8, [I

    .line 113
    .local v0, "data":[I
    const/16 v1, 0x18

    .line 114
    .local v1, "m_Rev_Convert":I
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v4

    float-to-int v2, v2

    neg-int v3, v1

    mul-int/2addr v2, v3

    aput v2, v0, v4

    .line 115
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v6

    float-to-int v2, v2

    neg-int v3, v1

    mul-int/2addr v2, v3

    aput v2, v0, v6

    .line 116
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v7

    float-to-int v2, v2

    neg-int v3, v1

    mul-int/2addr v2, v3

    aput v2, v0, v7

    .line 118
    aget v2, v0, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    aget v3, v0, v6

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v2, v3, :cond_2

    .line 119
    aget v2, v0, v4

    int-to-double v2, v2

    cmpg-double v2, v2, v10

    if-gez v2, :cond_1

    .line 120
    iget v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    if-eq v2, v7, :cond_0

    .line 121
    iput v7, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    iget-object v2, v2, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    aget v3, v3, v8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 123
    const-string v2, "AccelerometerSensorTest"

    const-string v3, "onSensorChanged"

    const-string v4, "browser activity rotation test ----> TURN LANDSCAPE1------- "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .end local v0    # "data":[I
    .end local v1    # "m_Rev_Convert":I
    :cond_0
    :goto_0
    return-void

    .line 126
    .restart local v0    # "data":[I
    .restart local v1    # "m_Rev_Convert":I
    :cond_1
    aget v2, v0, v4

    int-to-double v2, v2

    const-wide/high16 v4, 0x4043000000000000L    # 38.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 127
    iget v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    if-eq v2, v8, :cond_0

    .line 128
    iput v8, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    iget-object v2, v2, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    aget v3, v3, v7

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 130
    const-string v2, "AccelerometerSensorTest"

    const-string v3, "onSensorChanged"

    const-string v4, "browser activity rotation test ----> TURN PORTRAIT2------- "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_2
    aget v2, v0, v6

    int-to-double v2, v2

    cmpg-double v2, v2, v10

    if-gez v2, :cond_3

    .line 136
    iget v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    if-eqz v2, :cond_0

    .line 137
    iput v4, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    iget-object v2, v2, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 139
    const-string v2, "AccelerometerSensorTest"

    const-string v3, "onSensorChanged"

    const-string v4, "browser activity rotation test ----> TURN LANDSCAPE3------- "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_3
    aget v2, v0, v6

    int-to-double v2, v2

    const-wide/high16 v4, 0x4043000000000000L    # 38.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 143
    iget v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    if-eq v2, v6, :cond_0

    .line 144
    iput v6, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->curr_orientation:I

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->this$0:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    iget-object v2, v2, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;->mImageAccResource:[I

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 146
    const-string v2, "AccelerometerSensorTest"

    const-string v3, "onSensorChanged"

    const-string v4, "browser activity rotation test ----> TURN PORTRAIT4------- "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
