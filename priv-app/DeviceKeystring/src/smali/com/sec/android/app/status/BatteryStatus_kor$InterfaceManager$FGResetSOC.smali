.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FGResetSOC"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1063
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;)V

    .line 1064
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1113
    const-string v0, "FG Reset SOC"

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1080
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1081
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1082
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1069
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "FG Reset SOC"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nAre you sure you want to continue?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Enable"

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Disable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1075
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    .line 1087
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v1

    .line 1088
    .local v1, "ifm":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->sendMessage(ILjava/lang/Object;)V

    .line 1090
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1094
    :goto_0
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->FG_RESET_SOC:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1095
    .local v2, "path":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1096
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v5, "1\n"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->writeFile(Ljava/io/File;Ljava/lang/String;Z)Z

    .line 1100
    :goto_1
    new-instance v3, Ljava/util/Timer;

    invoke-direct {v3}, Ljava/util/Timer;-><init>()V

    .line 1101
    .local v3, "timer":Ljava/util/Timer;
    new-instance v4, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC$1;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC$1;-><init>(Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;)V

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v4, v6, v7}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1108
    return-void

    .line 1091
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "timer":Ljava/util/Timer;
    :catch_0
    move-exception v0

    .line 1092
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1098
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    const-string v4, "BatteryStatus"

    const-string v5, "BATT_RESET_SOC is not set!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
