.class public Lcom/sec/android/app/status/LightsensorRead;
.super Landroid/app/Activity;
.source "LightsensorRead.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;
    }
.end annotation


# static fields
.field private static nOriginalStateLightSensor:I


# instance fields
.field private final MAX_BUFFER_SIZE:I

.field private final SECOND:I

.field private brightness_flag:I

.field private mBackButton:Landroid/widget/Button;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private mLightSensor:Landroid/hardware/Sensor;

.field private mLuxMsg:Landroid/widget/TextView;

.field private mLuxMsg2:Landroid/widget/TextView;

.field private mSensorListner:Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorValue:F

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/status/LightsensorRead;->nOriginalStateLightSensor:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/status/LightsensorRead;->SECOND:I

    .line 38
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/status/LightsensorRead;->MAX_BUFFER_SIZE:I

    .line 40
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mHandler:Landroid/os/Handler;

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/LightsensorRead;->brightness_flag:I

    .line 87
    new-instance v0, Lcom/sec/android/app/status/LightsensorRead$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/LightsensorRead$2;-><init>(Lcom/sec/android/app/status/LightsensorRead;)V

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mClicked:Landroid/view/View$OnClickListener;

    .line 212
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/LightsensorRead;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LightsensorRead;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/status/LightsensorRead;->printCurrentLuxInfo()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/LightsensorRead;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LightsensorRead;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/LightsensorRead;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LightsensorRead;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mBackButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/LightsensorRead;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LightsensorRead;
    .param p1, "x1"    # F

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/LightsensorRead;->setSensorValue(F)V

    return-void
.end method

.method private getValueFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x64

    const/4 v9, 0x0

    .line 157
    const/4 v4, 0x0

    .line 158
    .local v4, "in":Ljava/io/InputStream;
    new-array v0, v10, [B

    .line 159
    .local v0, "buffer":[B
    const/4 v7, 0x0

    .line 160
    .local v7, "value":Ljava/lang/String;
    const/4 v6, 0x0

    .line 162
    .local v6, "length":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v10, :cond_0

    .line 163
    aput-byte v9, v0, v3

    .line 162
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 167
    :cond_0
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    .end local v4    # "in":Ljava/io/InputStream;
    .local v5, "in":Ljava/io/InputStream;
    if-eqz v5, :cond_2

    .line 170
    :try_start_1
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 172
    if-eqz v6, :cond_1

    .line 173
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    add-int/lit8 v10, v6, -0x1

    invoke-direct {v8, v0, v9, v10}, Ljava/lang/String;-><init>([BII)V

    .end local v7    # "value":Ljava/lang/String;
    .local v8, "value":Ljava/lang/String;
    move-object v7, v8

    .line 176
    .end local v8    # "value":Ljava/lang/String;
    .restart local v7    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 185
    :cond_2
    if-eqz v5, :cond_5

    .line 187
    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v4, v5

    .line 194
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :cond_3
    :goto_1
    return-object v7

    .line 188
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/io/IOException;
    const-string v9, "LightSensorRead"

    const-string v10, "getValueFromFile"

    const-string v11, "File Close error"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v5

    .line 190
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_1

    .line 178
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 179
    .local v2, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 180
    const-string v9, "LightSensorRead"

    const-string v10, "getValueFromFile"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FileNotFoundException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 185
    if-eqz v4, :cond_3

    .line 187
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 188
    :catch_2
    move-exception v1

    .line 189
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v9, "LightSensorRead"

    const-string v10, "getValueFromFile"

    const-string v11, "File Close error"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 181
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v1

    .line 182
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 183
    const-string v9, "LightSensorRead"

    const-string v10, "getValueFromFile"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 185
    if-eqz v4, :cond_3

    .line 187
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 188
    :catch_4
    move-exception v1

    .line 189
    const-string v9, "LightSensorRead"

    const-string v10, "getValueFromFile"

    const-string v11, "File Close error"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 185
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_4
    if-eqz v4, :cond_4

    .line 187
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 190
    :cond_4
    :goto_5
    throw v9

    .line 188
    :catch_5
    move-exception v1

    .line 189
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v10, "LightSensorRead"

    const-string v11, "getValueFromFile"

    const-string v12, "File Close error"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 185
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 181
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v1

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 178
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :cond_5
    move-object v4, v5

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    goto/16 :goto_1
.end method

.method private printCurrentLuxInfo()V
    .locals 8

    .prologue
    .line 100
    const/4 v3, 0x0

    .line 101
    .local v3, "luxVal":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 103
    .local v2, "luxDisplay":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 106
    .local v1, "lightlevel":I
    :try_start_0
    const-string v4, "/sys/class/sensors/light_sensor/lux"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/LightsensorRead;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 112
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Lux"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    float-to-double v4, v4

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_1

    .line 119
    const/4 v1, 0x1

    .line 130
    :cond_0
    :goto_1
    packed-switch v1, :pswitch_data_0

    .line 154
    :goto_2
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e1":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 120
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_1
    iget v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    float-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_2

    .line 121
    const/4 v1, 0x2

    goto :goto_1

    .line 122
    :cond_2
    iget v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    float-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_3

    .line 123
    const/4 v1, 0x3

    goto :goto_1

    .line 124
    :cond_3
    iget v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    float-to-double v4, v4

    const-wide v6, 0x40c3880000000000L    # 10000.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_4

    .line 125
    const/4 v1, 0x4

    goto :goto_1

    .line 126
    :cond_4
    iget v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    float-to-double v4, v4

    const-wide v6, 0x40cf400000000000L    # 16000.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_0

    .line 127
    const/4 v1, 0x5

    goto :goto_1

    .line 132
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const-string v5, "Level 1"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const/high16 v5, -0x10000

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 136
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const-string v5, "Level 2"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const/16 v5, -0x100

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 140
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const-string v5, "Level 3"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const v5, -0xff0100

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 144
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const-string v5, "Level 4"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const v5, -0xff0001

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 148
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const-string v5, "Level 5"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    const v5, -0xffff01

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 130
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setSensorValue(F)V
    .locals 0
    .param p1, "sensorValue"    # F

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorValue:F

    .line 97
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v0, 0x7f030041

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LightsensorRead;->setContentView(I)V

    .line 60
    const v0, 0x7f0a010d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LightsensorRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0a010e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LightsensorRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mLuxMsg2:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0a0067

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LightsensorRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mBackButton:Landroid/widget/Button;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mBackButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/LightsensorRead;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LightsensorRead;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorManager:Landroid/hardware/SensorManager;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mLightSensor:Landroid/hardware/Sensor;

    .line 68
    new-instance v0, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;-><init>(Lcom/sec/android/app/status/LightsensorRead;Lcom/sec/android/app/status/LightsensorRead$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorListner:Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

    .line 69
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorListner:Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 202
    const-string v0, "LightSensorRead"

    const-string v1, "onDestroy"

    const-string v2, "ondestroyend"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 204
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorListner:Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 209
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 210
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LightsensorRead;->mSensorListner:Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;

    iget-object v2, p0, Lcom/sec/android/app/status/LightsensorRead;->mLightSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 75
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mTimer:Ljava/util/Timer;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/status/LightsensorRead$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/LightsensorRead$1;-><init>(Lcom/sec/android/app/status/LightsensorRead;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 85
    return-void
.end method
