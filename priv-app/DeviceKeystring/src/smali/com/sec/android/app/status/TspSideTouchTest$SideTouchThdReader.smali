.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SideTouchThdReader"
.end annotation


# static fields
.field private static _readOnceFromSysfs:Z

.field private static _threshold:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1586
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_readOnceFromSysfs:Z

    .line 1587
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    return-void
.end method

.method private static getSavedThreshold(I)Ljava/lang/String;
    .locals 5
    .param p0, "param"    # I

    .prologue
    .line 1624
    const-string v0, "NA"

    .line 1625
    .local v0, "ret":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    array-length v1, v1

    if-le v1, p0, :cond_0

    .line 1626
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    aget-object v0, v1, p0

    .line 1628
    :cond_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "getSavedThreshold"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "param: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ret: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1630
    return-object v0
.end method

.method public static getThreshold(I)Ljava/lang/String;
    .locals 1
    .param p0, "param"    # I

    .prologue
    .line 1595
    sget-boolean v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_readOnceFromSysfs:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1596
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->getSavedThreshold(I)Ljava/lang/String;

    move-result-object v0

    .line 1600
    :goto_0
    return-object v0

    .line 1599
    :cond_0
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->updateThreshold()V

    .line 1600
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->getSavedThreshold(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static parseResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1638
    if-nez p0, :cond_0

    move-object p0, v2

    .line 1649
    .end local p0    # "value":Ljava/lang/String;
    .local v0, "SPLIT_CMD_RESULT":Ljava/lang/String;
    .local v1, "split":[Ljava/lang/String;
    :goto_0
    return-object p0

    .line 1642
    .end local v0    # "SPLIT_CMD_RESULT":Ljava/lang/String;
    .end local v1    # "split":[Ljava/lang/String;
    .restart local p0    # "value":Ljava/lang/String;
    :cond_0
    const-string v0, ":"

    .line 1643
    .restart local v0    # "SPLIT_CMD_RESULT":Ljava/lang/String;
    const-string v3, ":"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1644
    .restart local v1    # "split":[Ljava/lang/String;
    array-length v3, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    .line 1645
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "parseResult"

    const-string v5, "split.length < 2"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object p0, v2

    .line 1646
    goto :goto_0

    .line 1648
    :cond_1
    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 1649
    goto :goto_0
.end method

.method public static release()V
    .locals 1

    .prologue
    .line 1634
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    .line 1635
    return-void
.end method

.method public static updateThreshold()V
    .locals 10

    .prologue
    .line 1604
    const-string v3, "get_sidekey_threshold"

    .line 1606
    .local v3, "RUN_CMD":Ljava/lang/String;
    const-string v1, "/sys/class/sec/tsp/cmd"

    .line 1607
    .local v1, "PATH_COMMAND":Ljava/lang/String;
    const-string v2, "/sys/class/sec/tsp/cmd_result"

    .line 1609
    .local v2, "PATH_RESULT":Ljava/lang/String;
    const-string v6, "/sys/class/sec/tsp/cmd"

    const-string v7, "get_sidekey_threshold"

    invoke-static {v6, v7}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1610
    const-string v6, "/sys/class/sec/tsp/cmd_result"

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 1611
    .local v4, "result":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->parseResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1612
    .local v5, "value":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "updateThreshold"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "value: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1613
    if-nez v5, :cond_0

    .line 1614
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "updateThreshold"

    const-string v8, "value is null"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1615
    const/4 v6, 0x0

    sput-object v6, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    .line 1621
    :goto_0
    return-void

    .line 1618
    :cond_0
    const-string v0, "[,\\s]+"

    .line 1619
    .local v0, "COMMA_OR_SPACE":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "[,\\s]+"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    .line 1620
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v6

    const-string v7, "updateThreshold"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_threshold.length: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->_threshold:[Ljava/lang/String;

    array-length v9, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
