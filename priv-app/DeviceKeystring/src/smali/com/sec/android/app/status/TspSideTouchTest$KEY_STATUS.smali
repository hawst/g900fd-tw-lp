.class final enum Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
.super Ljava/lang/Enum;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "KEY_STATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_CANCEL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_PEADING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field public static final enum STATUS_READING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;


# instance fields
.field private _resId:I

.field private _text:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1681
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_INIT"

    const v2, 0x7f060002

    const-string v3, ""

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1682
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_PASS"

    const v2, 0x7f060005

    const-string v3, "PASS"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1683
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_FAIL"

    const v2, 0x7f060003

    const-string v3, "FAIL"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1684
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_PEADING"

    const v2, 0x7f060019

    const-string v3, "PENDING"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PEADING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1685
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_READING"

    const v2, 0x7f060004

    const-string v3, "READING"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_READING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1686
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_POLL"

    const/4 v2, 0x5

    const v3, 0x7f060002

    const-string v4, ""

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1687
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    const-string v1, "STATUS_CANCEL"

    const/4 v2, 0x6

    const v3, 0x7f060006

    const-string v4, "CANCEL"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_CANCEL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1680
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PEADING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_READING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_CANCEL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->$VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p3, "colorId"    # I
    .param p4, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1693
    iput p3, p0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->_resId:I

    .line 1694
    iput-object p4, p0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->_text:Ljava/lang/String;

    .line 1695
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1680
    const-class v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
    .locals 1

    .prologue
    .line 1680
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->$VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    return-object v0
.end method


# virtual methods
.method public getResourceId()I
    .locals 1

    .prologue
    .line 1698
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->_resId:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1702
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->_text:Ljava/lang/String;

    return-object v0
.end method
