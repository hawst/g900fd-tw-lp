.class Lcom/sec/android/app/brightness/miniDlg$3;
.super Ljava/lang/Object;
.source "miniDlg.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/brightness/miniDlg;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/brightness/miniDlg;


# direct methods
.method constructor <init>(Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # setter for: Lcom/sec/android/app/brightness/miniDlg;->mButtonPushing:Z
    invoke-static {v0, v7}, Lcom/sec/android/app/brightness/miniDlg;->access$302(Lcom/sec/android/app/brightness/miniDlg;Z)Z

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/brightness/miniDlg;->mRepeatType:I
    invoke-static {v0, v1}, Lcom/sec/android/app/brightness/miniDlg;->access$402(Lcom/sec/android/app/brightness/miniDlg;I)I

    .line 132
    iget-object v6, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    new-instance v0, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    const-wide/16 v2, 0x9c4

    const-wide/16 v4, 0x96

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;-><init>(Lcom/sec/android/app/brightness/miniDlg;JJ)V

    # setter for: Lcom/sec/android/app/brightness/miniDlg;->timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    invoke-static {v6, v0}, Lcom/sec/android/app/brightness/miniDlg;->access$502(Lcom/sec/android/app/brightness/miniDlg;Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;)Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg$3;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # getter for: Lcom/sec/android/app/brightness/miniDlg;->timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    invoke-static {v0}, Lcom/sec/android/app/brightness/miniDlg;->access$500(Lcom/sec/android/app/brightness/miniDlg;)Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->start()Landroid/os/CountDownTimer;

    .line 134
    return v7
.end method
