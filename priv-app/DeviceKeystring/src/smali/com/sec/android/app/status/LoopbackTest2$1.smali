.class Lcom/sec/android/app/status/LoopbackTest2$1;
.super Landroid/os/Handler;
.source "LoopbackTest2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/LoopbackTest2;->startMicTest(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/LoopbackTest2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/LoopbackTest2;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 227
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 229
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    .line 230
    .local v0, "rmsL":I
    iget v1, p1, Landroid/os/Message;->arg2:I

    .line 231
    .local v1, "rmsR":I
    const-string v3, "LoopbackTest2"

    const-string v4, "startMicTest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rmsL="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", rmsR="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;
    invoke-static {v3}, Lcom/sec/android/app/status/LoopbackTest2;->access$000(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/lcdtest/view/Graph;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;
    invoke-static {v3}, Lcom/sec/android/app/status/LoopbackTest2;->access$000(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/lcdtest/view/Graph;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->floatValue()F

    move-result v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->floatValue()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/lcdtest/view/Graph;->addValue(FF)V

    .line 235
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v3}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v3}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->setMicArray(II)Z

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/LoopbackTest2;->access$200(Lcom/sec/android/app/status/LoopbackTest2;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 238
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 240
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2;->access$200(Lcom/sec/android/app/status/LoopbackTest2;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIC1\n Real : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    AVG : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getAVG(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    MIN : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getMIN(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    MAX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getMAX(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/status/LoopbackTest2;->drawTextView(Landroid/widget/TextView;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/status/LoopbackTest2;->access$300(Lcom/sec/android/app/status/LoopbackTest2;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 244
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2;->access$400(Lcom/sec/android/app/status/LoopbackTest2;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MIC2\nReal : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    AVG : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getAVG(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    MIN : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getMIN(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    MAX : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    invoke-static {v6}, Lcom/sec/android/app/status/LoopbackTest2;->access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    move-result-object v6

    invoke-virtual {v6, v8}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->getMAX(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/status/LoopbackTest2;->drawTextView(Landroid/widget/TextView;Ljava/lang/String;)V
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/status/LoopbackTest2;->access$300(Lcom/sec/android/app/status/LoopbackTest2;Landroid/widget/TextView;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
