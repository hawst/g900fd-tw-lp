.class public Lcom/sec/android/app/brightness/Brightness$KeycodeListener;
.super Ljava/lang/Object;
.source "Brightness.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/brightness/Brightness;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "KeycodeListener"
.end annotation


# instance fields
.field private brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

.field final synthetic this$0:Lcom/sec/android/app/brightness/Brightness;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0
    .param p2, "dialog"    # Lcom/sec/android/app/brightness/miniDlg;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p2, p0, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    .line 63
    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 66
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->restoreSystemBrightness()V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->dismiss()V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/Brightness;->finish()V

    .line 72
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
