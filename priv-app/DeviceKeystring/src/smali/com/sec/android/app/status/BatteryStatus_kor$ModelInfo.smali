.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
.super Ljava/lang/Object;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ModelInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

.field private static mIsCreated:Z


# instance fields
.field private mAP:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

.field private mExternalStoragePath:Ljava/lang/String;

.field private mMode:I

.field private mModuleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$Module;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mRevision:Ljava/lang/String;

.field private mSettingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    .line 215
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mIsCreated:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    .line 233
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mSettingMap:Ljava/util/HashMap;

    .line 234
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mRevision:Ljava/lang/String;

    .line 235
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mName:Ljava/lang/String;

    .line 236
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mMode:I

    .line 237
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->NONE:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mAP:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    .line 238
    const-string v0, "/storage/sdcard0"

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mExternalStoragePath:Ljava/lang/String;

    .line 239
    return-void
.end method

.method public static create()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    .line 213
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 207
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ModelInfo is null!!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    return-object v0
.end method

.method public static getIsCreated()Z
    .locals 1

    .prologue
    .line 217
    sget-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mIsCreated:Z

    return v0
.end method

.method public static setIsCreated(Z)V
    .locals 0
    .param p0, "isCreated"    # Z

    .prologue
    .line 220
    sput-boolean p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mIsCreated:Z

    .line 221
    return-void
.end method


# virtual methods
.method public addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;)V
    .locals 1
    .param p1, "module"    # Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    return-void
.end method

.method public addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V
    .locals 1
    .param p1, "module"    # Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 250
    invoke-virtual {p1, p2}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->setPath(Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    return-void
.end method

.method public addSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;Ljava/lang/Object;)V
    .locals 2
    .param p1, "key"    # Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 285
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mSettingMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 286
    .local v0, "obj":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mSettingMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mSettingMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    return-void
.end method

.method public checkModule()V
    .locals 3

    .prologue
    .line 274
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 275
    .local v1, "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    iget v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mMode:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->checkLineStatus(I)V

    goto :goto_0

    .line 277
    .end local v1    # "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    :cond_0
    return-void
.end method

.method public getAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mAP:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    return-object v0
.end method

.method public getExternalStoragePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mExternalStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 313
    iget v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mMode:I

    return v0
.end method

.method public getModule(Ljava/lang/String;)Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 257
    if-eqz p1, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 264
    :goto_0
    return-object v1

    .line 259
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 260
    .local v1, "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v1    # "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    :cond_3
    move-object v1, v2

    .line 264
    goto :goto_0
.end method

.method public getModuleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$Module;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getRevision()Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mRevision:Ljava/lang/String;

    return-object v0
.end method

.method public getSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mSettingMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setAP(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;)V
    .locals 0
    .param p1, "ap"    # Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mAP:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    .line 318
    return-void
.end method

.method public setExternalStoragePath(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mExternalStoragePath:Ljava/lang/String;

    .line 325
    const-string v0, "BatteryStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ExternalStoragePath = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return-void
.end method

.method public setMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 310
    iput p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mMode:I

    .line 311
    return-void
.end method

.method public setModule()V
    .locals 3

    .prologue
    .line 279
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mModuleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 280
    .local v1, "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->setModule()V

    goto :goto_0

    .line 282
    .end local v1    # "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    :cond_0
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mName:Ljava/lang/String;

    .line 304
    return-void
.end method

.method public setRevision(Ljava/lang/String;)V
    .locals 0
    .param p1, "rev"    # Ljava/lang/String;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mRevision:Ljava/lang/String;

    .line 297
    return-void
.end method

.method public showInfo()V
    .locals 5

    .prologue
    .line 242
    const-string v0, "BatteryStatus"

    const-string v1, "Name(%s), AP(%s), Rev(%s), Mode(0x%x), ExPath(%s)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mName:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mAP:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mRevision:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mMode:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->mExternalStoragePath:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    return-void
.end method
