.class public Lcom/sec/android/app/status/ModuleAudioService;
.super Landroid/app/Service;
.source "ModuleAudioService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/ModuleAudioService$LocalBinder;
    }
.end annotation


# instance fields
.field private audioSource:I

.field buffer:[B

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioTrack:Landroid/media/AudioTrack;

.field private final mBinder:Landroid/os/IBinder;

.field private mBufferSize:I

.field private mConnectionMode:Ljava/lang/String;

.field private mCurrentState:I

.field public mHandler:Landroid/os/Handler;

.field private mIsHeadsetPlugged:Z

.field private mIsPlaying:Z

.field private mIsRecording:Z

.field private mIsRunningThread:Z

.field private mReadBytes:I

.field private mRecorder:Landroid/media/AudioRecord;

.field private mThread:Ljava/lang/Thread;

.field private mWrittenBytes:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->audioSource:I

    .line 31
    iput v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mCurrentState:I

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    .line 34
    iput-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsHeadsetPlugged:Z

    .line 37
    iput-boolean v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRecording:Z

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsPlaying:Z

    .line 39
    iput-boolean v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRunningThread:Z

    .line 40
    iput v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mReadBytes:I

    iput v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mWrittenBytes:I

    .line 42
    iput v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    .line 43
    iput-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->buffer:[B

    .line 44
    iput-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mThread:Ljava/lang/Thread;

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mConnectionMode:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/sec/android/app/status/ModuleAudioService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/ModuleAudioService$LocalBinder;-><init>(Lcom/sec/android/app/status/ModuleAudioService;)V

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBinder:Landroid/os/IBinder;

    .line 61
    new-instance v0, Lcom/sec/android/app/status/ModuleAudioService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/ModuleAudioService$1;-><init>(Lcom/sec/android/app/status/ModuleAudioService;)V

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/ModuleAudioService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/ModuleAudioService;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRunningThread:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/ModuleAudioService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/ModuleAudioService;Landroid/media/AudioTrack;)Landroid/media/AudioTrack;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/ModuleAudioService;
    .param p1, "x1"    # Landroid/media/AudioTrack;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioRecord;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/ModuleAudioService;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/status/ModuleAudioService;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/ModuleAudioService;
    .param p1, "x1"    # Landroid/media/AudioRecord;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    return-object p1
.end method

.method private setStreamMusicVolume(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 230
    if-le p1, v1, :cond_0

    move p1, v1

    .line 231
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 232
    :cond_1
    const-string v1, "ModuleAudioService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setStreamMusicVolume volme="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    if-nez p1, :cond_2

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 239
    :goto_0
    return-void

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v4, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private setStreamMusicVolumeMax()V
    .locals 1

    .prologue
    .line 214
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/ModuleAudioService;->setStreamMusicVolume(I)V

    .line 215
    return-void
.end method

.method private setStreamVolumeMax()V
    .locals 2

    .prologue
    .line 195
    const-string v0, "ModuleAudioService"

    const-string v1, "setVolumeMax"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/status/ModuleAudioService;->setStreamMusicVolumeMax()V

    .line 198
    return-void
.end method


# virtual methods
.method public InitLoopBack()V
    .locals 7

    .prologue
    const v2, 0xac44

    const/4 v4, 0x2

    const/4 v3, 0x3

    .line 94
    const-string v0, "ModuleAudioService"

    const-string v1, "InitLoopBack"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/ModuleAudioService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioManager:Landroid/media/AudioManager;

    .line 98
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    .line 99
    iget v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    .line 100
    iget v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->buffer:[B

    .line 102
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->audioSource:I

    iget v5, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    .line 106
    new-instance v0, Landroid/media/AudioTrack;

    iget v5, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    const/4 v6, 0x1

    move v1, v3

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    .line 107
    return-void
.end method

.method public StartLoopBack()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 145
    const-string v0, "ModuleAudioService"

    const-string v1, "StartLoopBack"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/status/ModuleAudioService;->setStreamVolumeMax()V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 150
    iput-boolean v5, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRecording:Z

    .line 151
    iput-boolean v5, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRunningThread:Z

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_3

    .line 184
    :cond_1
    :goto_0
    iput-boolean v4, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRunningThread:Z

    .line 186
    :cond_2
    return-void

    .line 159
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;

    iget-object v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->buffer:[B

    iget v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    invoke-virtual {v0, v1, v4, v2}, Landroid/media/AudioRecord;->read([BII)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mReadBytes:I

    .line 161
    const/4 v0, -0x3

    iget v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mReadBytes:I

    if-eq v0, v1, :cond_5

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_1

    .line 166
    iget v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mWrittenBytes:I

    iget-object v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    iget-object v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->buffer:[B

    iget v3, p0, Lcom/sec/android/app/status/ModuleAudioService;->mReadBytes:I

    invoke-virtual {v1, v2, v4, v3}, Landroid/media/AudioTrack;->write([BII)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mWrittenBytes:I

    .line 167
    const-string v0, "ModuleAudioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Audio recorder written bytes = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mWrittenBytes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsPlaying:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mWrittenBytes:I

    iget v1, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBufferSize:I

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_4

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_6

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 175
    const-string v0, "ModuleAudioService"

    const-string v1, "AudioTrack start playing..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 177
    iput-boolean v5, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsPlaying:Z

    .line 183
    :cond_4
    :goto_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRecording:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 169
    :cond_5
    const-string v0, "ModuleAudioService"

    const-string v1, "Audio recording failed!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 180
    :cond_6
    const-string v0, "ModuleAudioService"

    const-string v1, "AudioTrack create fail"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public StopLoopBack()V
    .locals 4

    .prologue
    .line 189
    const-string v0, "ModuleAudioService"

    const-string v1, "StopLoopBack"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mIsRecording:Z

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 192
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    const-string v0, "ModuleAudioService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 116
    const-string v0, "ModuleAudioService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/status/ModuleAudioService;->StopLoopBack()V

    .line 111
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 112
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x1

    .line 120
    const-string v0, "ModuleAudioService"

    const-string v1, "onStartCommand"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    if-eqz p1, :cond_0

    .line 122
    const-string v0, "STATE"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mCurrentState:I

    .line 123
    const-string v0, "ModuleAudioService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/status/ModuleAudioService;->mCurrentState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/ModuleAudioService;->InitLoopBack()V

    .line 126
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/ModuleAudioService$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/ModuleAudioService$2;-><init>(Lcom/sec/android/app/status/ModuleAudioService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mThread:Ljava/lang/Thread;

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 132
    return v3
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
