.class Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;
.super Ljava/lang/Object;
.source "AP_chip_test_Qualcomm.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->updateGUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/16 v14, 0x8

    const/4 v13, 0x1

    const/4 v12, -0x1

    const/4 v11, 0x0

    .line 46
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    const-string v8, "/sys/devices/system/soc/soc0/soc_pvs"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 47
    .local v5, "pvs":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    const-string v8, "/sys/devices/system/soc/soc0/soc_iddq"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "iddq":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "QCOMM : Raw data PSV="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", IDDQ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v2, -0x1

    .local v2, "iPvs":I
    const/4 v1, -0x1

    .line 53
    .local v1, "iIddq":I
    if-eqz v5, :cond_0

    .line 55
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 61
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 63
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v14, :cond_5

    .line 64
    const-string v6, "-1"

    .line 65
    .local v6, "temp":Ljava/lang/String;
    const/4 v7, 0x6

    const/16 v8, 0x8

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 66
    const/16 v7, 0x10

    invoke-static {v6, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 74
    .end local v6    # "temp":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "handleCommand"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "QCOMM : PSV="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", IDDQ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v0, "F"

    .line 77
    .local v0, "cpuGrade":Ljava/lang/String;
    const-string v7, "MSM8974"

    iget-object v8, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    # getter for: Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->access$000(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "MSM8274"

    iget-object v8, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    # getter for: Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->access$000(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "MSM8674"

    iget-object v8, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    # getter for: Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->access$000(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 78
    :cond_2
    if-ne v1, v12, :cond_3

    .line 79
    const-string v0, "F"

    .line 81
    :cond_3
    packed-switch v2, :pswitch_data_0

    .line 106
    const-string v0, "F"

    .line 132
    :goto_2
    const-string v7, "A"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "B"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 133
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->group_TextView:Landroid/widget/TextView;

    const v8, -0xffff01

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 138
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->invalidate()V

    .line 140
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data1_TextView:Landroid/widget/TextView;

    const-string v8, "PVS : "

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data1Result_TextView:Landroid/widget/TextView;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data1_TextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->invalidate()V

    .line 143
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data1Result_TextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->invalidate()V

    .line 144
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data2_TextView:Landroid/widget/TextView;

    const-string v8, "IDDQ : "

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data2Result_TextView:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data2_TextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->invalidate()V

    .line 147
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data2Result_TextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->invalidate()V

    .line 148
    return-void

    .line 56
    .end local v0    # "cpuGrade":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 57
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 68
    .end local v4    # "ne":Ljava/lang/NumberFormatException;
    :cond_5
    const/16 v7, 0x10

    :try_start_2
    invoke-static {v3, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v1

    goto/16 :goto_1

    .line 70
    :catch_1
    move-exception v4

    .line 71
    .restart local v4    # "ne":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 86
    .end local v4    # "ne":Ljava/lang/NumberFormatException;
    .restart local v0    # "cpuGrade":Ljava/lang/String;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    aget-object v7, v7, v2

    aget v7, v7, v11

    if-ge v1, v7, :cond_6

    .line 87
    const-string v0, "A"

    goto/16 :goto_2

    .line 88
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    aget-object v7, v7, v2

    aget v7, v7, v13

    if-ge v1, v7, :cond_7

    .line 89
    const-string v0, "B"

    goto/16 :goto_2

    .line 91
    :cond_7
    const-string v0, "C"

    .line 93
    goto/16 :goto_2

    .line 96
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    aget-object v7, v7, v2

    aget v7, v7, v11

    if-ge v1, v7, :cond_8

    .line 97
    const-string v0, "B"

    goto/16 :goto_2

    .line 99
    :cond_8
    const-string v0, "C"

    .line 101
    goto/16 :goto_2

    .line 103
    :pswitch_2
    const-string v0, "C"

    .line 104
    goto/16 :goto_2

    .line 110
    :cond_9
    packed-switch v2, :pswitch_data_1

    .line 127
    const-string v0, "F"

    goto/16 :goto_2

    .line 116
    :pswitch_3
    if-ne v1, v12, :cond_a

    .line 117
    const-string v0, "F"

    goto/16 :goto_2

    .line 118
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    add-int/lit8 v8, v2, -0x1

    aget-object v7, v7, v8

    aget v7, v7, v11

    if-gt v1, v7, :cond_b

    .line 119
    const-string v0, "A"

    goto/16 :goto_2

    .line 120
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    add-int/lit8 v8, v2, -0x1

    aget-object v7, v7, v8

    aget v7, v7, v13

    if-gt v1, v7, :cond_c

    .line 121
    const-string v0, "B"

    goto/16 :goto_2

    .line 123
    :cond_c
    const-string v0, "C"

    .line 125
    goto/16 :goto_2

    .line 135
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;->this$0:Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    iget-object v7, v7, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->group_TextView:Landroid/widget/TextView;

    const/high16 v8, -0x10000

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 81
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 110
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
