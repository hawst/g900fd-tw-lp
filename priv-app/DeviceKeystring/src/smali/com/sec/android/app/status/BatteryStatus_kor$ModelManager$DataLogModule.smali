.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;
.super Lcom/sec/android/app/status/BatteryStatus_kor$Module;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DataLogModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 615
    const-string v0, "Data Log"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;-><init>(Ljava/lang/String;Z)V

    .line 616
    return-void
.end method


# virtual methods
.method public getPath()Ljava/lang/String;
    .locals 7

    .prologue
    .line 636
    const-string v3, "time \t"

    .line 637
    .local v3, "ret":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;->getLineStatusList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;

    .line 638
    .local v2, "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->getStatusList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 639
    .local v4, "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 640
    goto :goto_0

    .line 642
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    .end local v4    # "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_1
    return-object v3
.end method

.method protected setModule()V
    .locals 4

    .prologue
    .line 619
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/batt_read_raw_soc"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/capacity"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/voltage_now"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/current_now"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/present"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/temp"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/batt_temp_adc"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/chg_temp"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/online"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/batt_charging_source"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/health"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/status"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "/sys/class/power_supply/battery/siop_level"

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 632
    return-void
.end method

.method public updateLineStatus(I)Ljava/lang/String;
    .locals 7
    .param p1, "mode"    # I

    .prologue
    .line 647
    const-string v3, ""

    .line 648
    .local v3, "ret":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;->getLineStatusList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;

    .line 649
    .local v2, "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->getStatusList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 650
    .local v4, "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 651
    goto :goto_0

    .line 653
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    .end local v4    # "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_1
    return-object v3
.end method
