.class public Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;
.super Landroid/app/Activity;
.source "FuelGaugeRegister.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final FILE_PATH_READ:Ljava/lang/String;

.field private final FILE_PATH_WRITE1:Ljava/lang/String;

.field private final FILE_PATH_WRITE2:Ljava/lang/String;

.field private mCurrentFuelGauge:Landroid/widget/EditText;

.field private mNewFuelGauge1:Landroid/widget/EditText;

.field private mNewFuelGauge2:Landroid/widget/EditText;

.field private mReadFuelGauge:Landroid/widget/Button;

.field private mWriteFuelGauge1:Landroid/widget/Button;

.field private mWriteFuelGauge2:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 20
    const-string v0, "filepath1"

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->FILE_PATH_READ:Ljava/lang/String;

    .line 21
    const-string v0, "filepath2"

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->FILE_PATH_WRITE1:Ljava/lang/String;

    .line 22
    const-string v0, "filepath3"

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->FILE_PATH_WRITE2:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge1:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge1:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge2:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge2:Landroid/widget/Button;

    return-object v0
.end method

.method private static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 125
    const/4 v3, 0x0

    .line 126
    .local v3, "value":Ljava/lang/String;
    const/4 v1, 0x0

    .line 129
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    if-eqz v2, :cond_0

    .line 132
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 133
    if-eqz v3, :cond_0

    .line 134
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 140
    :cond_0
    if-eqz v2, :cond_3

    .line 142
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 150
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return-object v3

    .line 143
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 145
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 137
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 140
    if-eqz v1, :cond_1

    .line 142
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 143
    :catch_2
    move-exception v0

    .line 144
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 140
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_2

    .line 142
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 145
    :cond_2
    :goto_3
    throw v4

    .line 143
    :catch_3
    move-exception v0

    .line 144
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 140
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 137
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private readFuelGaugeReg()V
    .locals 2

    .prologue
    .line 94
    const-string v1, "filepath1"

    invoke-static {v1}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "fuelGaugeReg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mCurrentFuelGauge:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

.method private static write(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v1, 0x1

    .line 104
    .local v1, "res":Z
    const/4 v2, 0x0

    .line 107
    .local v2, "writer":Ljava/io/FileWriter;
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, p0}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    .end local v2    # "writer":Ljava/io/FileWriter;
    .local v3, "writer":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 113
    if-eqz v3, :cond_0

    .line 114
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 121
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    return v1

    .line 116
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    move-object v2, v3

    .line 119
    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_0

    .line 109
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 110
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const/4 v1, 0x0

    .line 113
    if-eqz v2, :cond_1

    .line 114
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 116
    :catch_2
    move-exception v0

    .line 117
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    .line 119
    goto :goto_0

    .line 112
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 113
    :goto_2
    if-eqz v2, :cond_2

    .line 114
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 118
    :cond_2
    :goto_3
    throw v4

    .line 116
    :catch_3
    move-exception v0

    .line 117
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_3

    .line 112
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_2

    .line 109
    .end local v2    # "writer":Ljava/io/FileWriter;
    .restart local v3    # "writer":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/FileWriter;
    .restart local v2    # "writer":Ljava/io/FileWriter;
    goto :goto_1
.end method

.method private writeFuelGaugeReg(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-static {p1, p2}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 100
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 91
    :goto_0
    :pswitch_0
    return-void

    .line 80
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->readFuelGaugeReg()V

    goto :goto_0

    .line 83
    :pswitch_2
    const-string v0, "filepath2"

    iget-object v1, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge1:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->writeFuelGaugeReg(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :pswitch_3
    const-string v0, "filepath3"

    iget-object v1, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge2:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->writeFuelGaugeReg(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :pswitch_data_0
    .packed-switch 0x7f0a006b
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->setContentView(I)V

    .line 35
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mReadFuelGauge:Landroid/widget/Button;

    .line 36
    const v0, 0x7f0a006d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge1:Landroid/widget/Button;

    .line 37
    const v0, 0x7f0a006f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge2:Landroid/widget/Button;

    .line 38
    const v0, 0x7f0a006a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mCurrentFuelGauge:Landroid/widget/EditText;

    .line 39
    const v0, 0x7f0a006c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge1:Landroid/widget/EditText;

    .line 40
    const v0, 0x7f0a006e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge2:Landroid/widget/EditText;

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge1:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;-><init>(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge2:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$2;-><init>(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mReadFuelGauge:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge1:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge2:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->readFuelGaugeReg()V

    .line 74
    return-void
.end method
