.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DataLog"
.end annotation


# static fields
.field private static mFile:Ljava/io/File;

.field private static mIsRunning:Z

.field private static mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 925
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    .line 926
    sput-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    .line 927
    sput-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 928
    sput-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 931
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;)V

    .line 932
    return-void
.end method

.method private createDataLogFile()Ljava/io/File;
    .locals 8

    .prologue
    .line 935
    const/4 v0, 0x0

    .line 936
    .local v0, "count":I
    const-string v3, ""

    .line 937
    .local v3, "path":Ljava/lang/String;
    const/4 v2, 0x0

    .line 939
    .local v2, "file":Ljava/io/File;
    :goto_0
    const-string v4, "%s/%s_%d.txt"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getExternalStoragePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "battonlog"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 941
    new-instance v2, Ljava/io/File;

    .end local v2    # "file":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 942
    .restart local v2    # "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x3e8

    if-lt v1, v4, :cond_1

    .line 943
    :cond_0
    return-object v2

    :cond_1
    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_0
.end method

.method private getLogData()Ljava/lang/String;
    .locals 6

    .prologue
    .line 974
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 975
    .local v0, "ret":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    if-eqz v1, :cond_0

    .line 976
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->updateLineStatus(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 978
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1025
    const-string v0, "%s: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Data Log"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-boolean v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 984
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    if-nez v0, :cond_0

    .line 985
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v0

    const-string v1, "Data Log"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getModule(Ljava/lang/String;)Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 987
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    if-nez v0, :cond_1

    .line 988
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->startThread()V

    .line 992
    :goto_0
    check-cast p1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->setText(Ljava/lang/CharSequence;)V

    .line 993
    return-void

    .line 990
    .restart local p1    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->stopThread()V

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 998
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 999
    .local v1, "line":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->writeFile(Ljava/io/File;Ljava/lang/String;Z)Z

    .line 1000
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    if-eqz v2, :cond_3

    .line 1002
    :try_start_0
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1003
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->getLogData()Ljava/lang/String;

    move-result-object v1

    .line 1004
    if-eqz v1, :cond_0

    .line 1005
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->writeFile(Ljava/io/File;Ljava/lang/String;Z)Z

    .line 1007
    :cond_0
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1008
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1010
    :cond_1
    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1011
    :catch_0
    move-exception v0

    .line 1012
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_2

    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1014
    sget-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1016
    :cond_2
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    .line 1017
    const/4 v2, 0x0

    sput-object v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    goto :goto_0

    .line 1020
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    return-void
.end method

.method protected startThread()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 948
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->createDataLogFile()Ljava/io/File;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    .line 949
    sget-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 950
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 951
    .local v0, "context":Landroid/content/Context;
    const-string v3, "power"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    const/4 v4, 0x6

    const-string v5, "Data Log"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 953
    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 954
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 955
    const/4 v3, 0x1

    sput-boolean v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 963
    .end local v0    # "context":Landroid/content/Context;
    .end local v2    # "thread":Ljava/lang/Thread;
    :cond_0
    :goto_0
    return-void

    .line 957
    :catch_0
    move-exception v1

    .line 958
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "BatteryStatus"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    .line 960
    sput-object v6, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 961
    sput-object v6, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mFile:Ljava/io/File;

    goto :goto_0
.end method

.method protected stopThread()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 965
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 968
    :cond_0
    sput-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 969
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mIsRunning:Z

    .line 970
    sput-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;->mModule:Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 971
    return-void
.end method
