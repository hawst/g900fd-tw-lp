.class public Lcom/sec/android/app/status/AP_chip_test_Qualcomm;
.super Lcom/sec/android/app/status/AP_chip_test;
.source "AP_chip_test_Qualcomm.java"


# instance fields
.field public IDDQ_TABLE:[[I

.field private final PATH_APCHIP_DATA_QCOMM_IDDQ:Ljava/lang/String;

.field private final PATH_APCHIP_DATA_QCOMM_PVS:Ljava/lang/String;

.field private mAPName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/app/status/AP_chip_test;-><init>()V

    .line 11
    const-string v0, "/sys/devices/system/soc/soc0/soc_pvs"

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->PATH_APCHIP_DATA_QCOMM_PVS:Ljava/lang/String;

    .line 12
    const-string v0, "/sys/devices/system/soc/soc0/soc_iddq"

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->PATH_APCHIP_DATA_QCOMM_IDDQ:Ljava/lang/String;

    .line 13
    const-string v0, "ro.chipname"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    .line 19
    const-string v0, "AP_chip_test_Qualcomm"

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->CLASS_NAME:Ljava/lang/String;

    .line 20
    invoke-super {p0, p1}, Lcom/sec/android/app/status/AP_chip_test;->onCreate(Landroid/os/Bundle;)V

    .line 21
    iget-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->data3_TextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 22
    const-string v0, "MSM8974"

    iget-object v1, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MSM8274"

    iget-object v1, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MSM8674"

    iget-object v1, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->mAPName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    :cond_0
    const/4 v0, 0x7

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v2

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v6

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    .line 41
    :goto_0
    return-void

    .line 33
    :cond_1
    const/4 v0, 0x5

    new-array v0, v0, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v2

    new-array v1, v3, [I

    fill-array-data v1, :array_8

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_a

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_b

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->IDDQ_TABLE:[[I

    goto :goto_0

    .line 23
    nop

    :array_0
    .array-data 4
        0x2b
        0x33
    .end array-data

    :array_1
    .array-data 4
        0x35
        0x3d
    .end array-data

    :array_2
    .array-data 4
        0x41
        0x4a
    .end array-data

    :array_3
    .array-data 4
        0x4f
        0x59
    .end array-data

    :array_4
    .array-data 4
        0x6a
        0x6a
    .end array-data

    :array_5
    .array-data 4
        0x64
        0x64
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 33
    :array_7
    .array-data 4
        0x23
        0x46
    .end array-data

    :array_8
    .array-data 4
        0x37
        0x5a
    .end array-data

    :array_9
    .array-data 4
        0x41
        0x69
    .end array-data

    :array_a
    .array-data 4
        0x50
        0x69
    .end array-data

    :array_b
    .array-data 4
        0x5a
        0x69
    .end array-data
.end method

.method public updateGUI()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm$1;-><init>(Lcom/sec/android/app/status/AP_chip_test_Qualcomm;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 150
    return-void
.end method
