.class Lcom/sec/android/app/version/FTASWVersion$1;
.super Landroid/content/BroadcastReceiver;
.source "FTASWVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/version/FTASWVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/version/FTASWVersion;


# direct methods
.method constructor <init>(Lcom/sec/android/app/version/FTASWVersion;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/version/FTASWVersion$1;->this$0:Lcom/sec/android/app/version/FTASWVersion;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 52
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "mAction":Ljava/lang/String;
    const-string v1, "android.intent.action.GET_FTA_RESPONSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/version/FTASWVersion$1;->this$0:Lcom/sec/android/app/version/FTASWVersion;

    const-string v2, "fta_sw_ver"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/version/FTASWVersion;->result:Ljava/lang/String;

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/version/FTASWVersion$1;->this$0:Lcom/sec/android/app/version/FTASWVersion;

    iget-object v1, v1, Lcom/sec/android/app/version/FTASWVersion;->ftaSwvertv:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FTA SW VERSION: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/version/FTASWVersion$1;->this$0:Lcom/sec/android/app/version/FTASWVersion;

    iget-object v3, v3, Lcom/sec/android/app/version/FTASWVersion;->result:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    :cond_0
    return-void
.end method
