.class public Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FactoryKeyStringBroadcastReceiver.java"


# static fields
.field public static mWl:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mCountryCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 38
    const-string v0, "ro.csc.country_code"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mCountryCode:Ljava/lang/String;

    .line 41
    return-void
.end method

.method private getCoverCount()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 613
    const-string v3, "/efs/FactoryApp/CoverCount"

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 614
    .local v1, "frontCoverCount":Ljava/lang/String;
    const-string v3, "/efs/FactoryApp/BackCoverCount"

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 615
    .local v2, "rearCoverCount":Ljava/lang/String;
    move-object v0, v1

    .line 616
    .local v0, "countG":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 617
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 620
    :cond_0
    return-object v0
.end method

.method private installSelfTestApp(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 653
    const-string v0, "http://mc.sec.net/self/MobileCare_Self_Phone"

    .line 654
    .local v0, "PHONE_URL":Ljava/lang/String;
    const-string v1, "http://mc.sec.net/self/MobileCare_Self_Table"

    .line 655
    .local v1, "TABLET_URL":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->setNonMarketAppsAllowed(Landroid/content/Context;Z)V

    .line 656
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v4, "http://mc.sec.net/self/MobileCare_Self_Phone"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 659
    .local v2, "browserIntent":Landroid/content/Intent;
    const/high16 v3, 0x14000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 660
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 661
    return-void
.end method

.method private isSharpNonADCChipset()Z
    .locals 10

    .prologue
    .line 664
    const/4 v0, 0x0

    .line 665
    .local v0, "data":Ljava/lang/String;
    const/4 v3, 0x0

    .line 666
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v2, 0x0

    .line 669
    .local v2, "isSharp":Z
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v6, "/sys/class/sensors/proximity_sensor/name"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 670
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    .line 673
    const-string v5, "FactoryKeyStringBroadcastReceiver"

    const-string v6, "isSharpNonADCChipset"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SensorModelName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    const-string v5, "GP2AP002"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    if-eqz v5, :cond_0

    .line 676
    const/4 v2, 0x1

    .line 683
    :cond_0
    if-eqz v4, :cond_3

    .line 685
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 693
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return v2

    .line 686
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 687
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "FactoryKeyStringBroadcastReceiver"

    const-string v6, "isSharpNonADCChipset"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 689
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 679
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 680
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "FactoryKeyStringBroadcastReceiver"

    const-string v6, "isSharpNonADCChipset"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ProximitySensor IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 683
    if-eqz v3, :cond_1

    .line 685
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 686
    :catch_2
    move-exception v1

    .line 687
    const-string v5, "FactoryKeyStringBroadcastReceiver"

    const-string v6, "isSharpNonADCChipset"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 683
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v3, :cond_2

    .line 685
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 689
    :cond_2
    :goto_3
    throw v5

    .line 686
    :catch_3
    move-exception v1

    .line 687
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "FactoryKeyStringBroadcastReceiver"

    const-string v7, "isSharpNonADCChipset"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File close exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 683
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 679
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method private loadEPenCounter()J
    .locals 14

    .prologue
    .line 408
    const-wide/16 v8, 0x0

    .line 409
    .local v8, "result":J
    const/4 v0, 0x0

    .line 410
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 413
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/FactoryApp/epen_count"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 416
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 417
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 419
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 420
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 433
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 434
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_1
    if-eqz v1, :cond_2

    .line 438
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 445
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 440
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 441
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 443
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 423
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 424
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 433
    if-eqz v5, :cond_4

    .line 434
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_4
    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 440
    :catch_2
    move-exception v3

    .line 441
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 425
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 426
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 433
    if-eqz v5, :cond_5

    .line 434
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_5
    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 440
    :catch_4
    move-exception v3

    .line 441
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 428
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 433
    if-eqz v5, :cond_6

    .line 434
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_6
    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 440
    :catch_6
    move-exception v3

    .line 441
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 430
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 433
    if-eqz v5, :cond_7

    .line 434
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_7
    if-eqz v0, :cond_3

    .line 438
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 440
    :catch_8
    move-exception v3

    .line 441
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEPenCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 432
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 433
    :goto_5
    if-eqz v5, :cond_8

    .line 434
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 437
    :cond_8
    if-eqz v0, :cond_9

    .line 438
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 442
    :cond_9
    :goto_6
    throw v10

    .line 440
    :catch_9
    move-exception v3

    .line 441
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadEPenCounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 432
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 429
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 427
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 425
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 423
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private loadEarjackCounter()J
    .locals 14

    .prologue
    .line 449
    const-wide/16 v8, 0x0

    .line 450
    .local v8, "result":J
    const/4 v0, 0x0

    .line 451
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 454
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/FactoryApp/earjack_count"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 455
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 457
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 458
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 460
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 461
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 474
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 475
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_1
    if-eqz v1, :cond_2

    .line 479
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 486
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 481
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 482
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 484
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 464
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 465
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 474
    if-eqz v5, :cond_4

    .line 475
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_4
    if-eqz v0, :cond_3

    .line 479
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 481
    :catch_2
    move-exception v3

    .line 482
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 466
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 467
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 474
    if-eqz v5, :cond_5

    .line 475
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_5
    if-eqz v0, :cond_3

    .line 479
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 481
    :catch_4
    move-exception v3

    .line 482
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 468
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 469
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 474
    if-eqz v5, :cond_6

    .line 475
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_6
    if-eqz v0, :cond_3

    .line 479
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 481
    :catch_6
    move-exception v3

    .line 482
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 470
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 471
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 474
    if-eqz v5, :cond_7

    .line 475
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_7
    if-eqz v0, :cond_3

    .line 479
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 481
    :catch_8
    move-exception v3

    .line 482
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadEarjackCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 473
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 474
    :goto_5
    if-eqz v5, :cond_8

    .line 475
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 478
    :cond_8
    if-eqz v0, :cond_9

    .line 479
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 483
    :cond_9
    :goto_6
    throw v10

    .line 481
    :catch_9
    move-exception v3

    .line 482
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadEarjackCounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 473
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 470
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 468
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 466
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 464
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private loadFlashPopupCounter()J
    .locals 14

    .prologue
    .line 531
    const-wide/16 v8, 0x0

    .line 532
    .local v8, "result":J
    const/4 v0, 0x0

    .line 533
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 536
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/FactoryApp/strobeCount"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 539
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 540
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 542
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 543
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 556
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 557
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_1
    if-eqz v1, :cond_2

    .line 561
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 568
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 563
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 564
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 566
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 546
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 547
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 556
    if-eqz v5, :cond_4

    .line 557
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_4
    if-eqz v0, :cond_3

    .line 561
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 563
    :catch_2
    move-exception v3

    .line 564
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 548
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 549
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 556
    if-eqz v5, :cond_5

    .line 557
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_5
    if-eqz v0, :cond_3

    .line 561
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 563
    :catch_4
    move-exception v3

    .line 564
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 550
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 551
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 556
    if-eqz v5, :cond_6

    .line 557
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_6
    if-eqz v0, :cond_3

    .line 561
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 563
    :catch_6
    move-exception v3

    .line 564
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 552
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 553
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 556
    if-eqz v5, :cond_7

    .line 557
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_7
    if-eqz v0, :cond_3

    .line 561
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 563
    :catch_8
    move-exception v3

    .line 564
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadFlashPopupCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 555
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 556
    :goto_5
    if-eqz v5, :cond_8

    .line 557
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 560
    :cond_8
    if-eqz v0, :cond_9

    .line 561
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 565
    :cond_9
    :goto_6
    throw v10

    .line 563
    :catch_9
    move-exception v3

    .line 564
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadFlashPopupCounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 555
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 552
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 550
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 548
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 546
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private loadQwertyCounter()J
    .locals 14

    .prologue
    .line 490
    const-wide/16 v8, 0x0

    .line 491
    .local v8, "result":J
    const/4 v0, 0x0

    .line 492
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 495
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/SlideCount"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 498
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 499
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 501
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 502
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 515
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 516
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_1
    if-eqz v1, :cond_2

    .line 520
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 527
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 522
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 523
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 525
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 505
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 506
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 515
    if-eqz v5, :cond_4

    .line 516
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_4
    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 522
    :catch_2
    move-exception v3

    .line 523
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 507
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 508
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 515
    if-eqz v5, :cond_5

    .line 516
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_5
    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 522
    :catch_4
    move-exception v3

    .line 523
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 509
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 510
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 515
    if-eqz v5, :cond_6

    .line 516
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_6
    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 522
    :catch_6
    move-exception v3

    .line 523
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 511
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 512
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 515
    if-eqz v5, :cond_7

    .line 516
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_7
    if-eqz v0, :cond_3

    .line 520
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 522
    :catch_8
    move-exception v3

    .line 523
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadQwertyCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 514
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 515
    :goto_5
    if-eqz v5, :cond_8

    .line 516
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 519
    :cond_8
    if-eqz v0, :cond_9

    .line 520
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 524
    :cond_9
    :goto_6
    throw v10

    .line 522
    :catch_9
    move-exception v3

    .line 523
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadQwertyCounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 514
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 511
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 509
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 507
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 505
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private loadTACounter()J
    .locals 14

    .prologue
    .line 367
    const-wide/16 v8, 0x0

    .line 368
    .local v8, "result":J
    const/4 v0, 0x0

    .line 369
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 372
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/FactoryApp/batt_cable_count"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 375
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 376
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 378
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 379
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 392
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 393
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_1
    if-eqz v1, :cond_2

    .line 397
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 404
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 399
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 400
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 402
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 382
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 383
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 392
    if-eqz v5, :cond_4

    .line 393
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_4
    if-eqz v0, :cond_3

    .line 397
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 399
    :catch_2
    move-exception v3

    .line 400
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 384
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 385
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 392
    if-eqz v5, :cond_5

    .line 393
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_5
    if-eqz v0, :cond_3

    .line 397
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 399
    :catch_4
    move-exception v3

    .line 400
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 386
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 387
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 392
    if-eqz v5, :cond_6

    .line 393
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_6
    if-eqz v0, :cond_3

    .line 397
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 399
    :catch_6
    move-exception v3

    .line 400
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 388
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 389
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 392
    if-eqz v5, :cond_7

    .line 393
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_7
    if-eqz v0, :cond_3

    .line 397
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 399
    :catch_8
    move-exception v3

    .line 400
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTACounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 391
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 392
    :goto_5
    if-eqz v5, :cond_8

    .line 393
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 396
    :cond_8
    if-eqz v0, :cond_9

    .line 397
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 401
    :cond_9
    :goto_6
    throw v10

    .line 399
    :catch_9
    move-exception v3

    .line 400
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadTACounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 391
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 388
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 386
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 384
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 382
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private loadTMBCounter()J
    .locals 14

    .prologue
    .line 572
    const-wide/16 v8, 0x0

    .line 573
    .local v8, "result":J
    const/4 v0, 0x0

    .line 574
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 577
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v6, Ljava/io/FileReader;

    const-string v10, "/efs/FactoryApp/tdmb_det_count"

    invoke-direct {v6, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    .end local v5    # "fr":Ljava/io/FileReader;
    .local v6, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v10, 0x1fa0

    invoke-direct {v1, v6, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 580
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 581
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 583
    .local v2, "counter":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 584
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v8

    .line 597
    .end local v2    # "counter":Ljava/lang/String;
    :cond_0
    if-eqz v6, :cond_1

    .line 598
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_1
    if-eqz v1, :cond_2

    .line 602
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 609
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-wide v8

    .line 604
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v3

    .line 605
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 607
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 587
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 588
    .local v4, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_4
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "FileNotFoundException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 597
    if-eqz v5, :cond_4

    .line 598
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_4
    if-eqz v0, :cond_3

    .line 602
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 604
    :catch_2
    move-exception v3

    .line 605
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 589
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 590
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_6
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 597
    if-eqz v5, :cond_5

    .line 598
    :try_start_7
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_5
    if-eqz v0, :cond_3

    .line 602
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 604
    :catch_4
    move-exception v3

    .line 605
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 591
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 592
    .local v3, "e":Ljava/lang/NullPointerException;
    :goto_3
    :try_start_8
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "NullPointerException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 597
    if-eqz v5, :cond_6

    .line 598
    :try_start_9
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_6
    if-eqz v0, :cond_3

    .line 602
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_0

    .line 604
    :catch_6
    move-exception v3

    .line 605
    .local v3, "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 593
    .end local v3    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 594
    .local v7, "ne":Ljava/lang/NumberFormatException;
    :goto_4
    :try_start_a
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "NumberFormatException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 597
    if-eqz v5, :cond_7

    .line 598
    :try_start_b
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_7
    if-eqz v0, :cond_3

    .line 602
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_0

    .line 604
    :catch_8
    move-exception v3

    .line 605
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "FactoryKeyStringBroadcastReceiver"

    const-string v11, "loadTMBCounter"

    const-string v12, "IOException"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 596
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "ne":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v10

    .line 597
    :goto_5
    if-eqz v5, :cond_8

    .line 598
    :try_start_c
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 601
    :cond_8
    if-eqz v0, :cond_9

    .line 602
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 606
    :cond_9
    :goto_6
    throw v10

    .line 604
    :catch_9
    move-exception v3

    .line 605
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v11, "FactoryKeyStringBroadcastReceiver"

    const-string v12, "loadTMBCounter"

    const-string v13, "IOException"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 596
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_5

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 593
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_a
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_b
    move-exception v7

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 591
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_c
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_d
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 589
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_e
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_f
    move-exception v3

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_2

    .line 587
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_10
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    goto/16 :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "fr":Ljava/io/FileReader;
    :catch_11
    move-exception v4

    move-object v5, v6

    .end local v6    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method private saveQwertyCounter(J)V
    .locals 7
    .param p1, "counter"    # J

    .prologue
    .line 624
    const/4 v1, 0x0

    .line 627
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    const-string v3, "/efs/SlideCount"

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 628
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 633
    if-eqz v2, :cond_2

    .line 635
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 642
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 636
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 637
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "FactoryKeyStringBroadcastReceiver"

    const-string v4, "saveQwertyCounter"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 639
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 629
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 630
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "FactoryKeyStringBroadcastReceiver"

    const-string v4, "saveQwertyCounter"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 633
    if-eqz v1, :cond_0

    .line 635
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 636
    :catch_2
    move-exception v0

    .line 637
    const-string v3, "FactoryKeyStringBroadcastReceiver"

    const-string v4, "saveQwertyCounter"

    const-string v5, "IOException"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 633
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    .line 635
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 639
    :cond_1
    :goto_3
    throw v3

    .line 636
    :catch_3
    move-exception v0

    .line 637
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "FactoryKeyStringBroadcastReceiver"

    const-string v5, "saveQwertyCounter"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 633
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 629
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method

.method private setNonMarketAppsAllowed(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "enabled"    # Z

    .prologue
    .line 648
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_non_market_apps"

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 650
    return-void

    .line 648
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    const-string v27, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3c

    .line 46
    new-instance v20, Landroid/content/Intent;

    const-string v26, "android.intent.action.MAIN"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    .local v20, "i":Landroid/content/Intent;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v19

    .line 48
    .local v19, "host":Ljava/lang/String;
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "host is "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v26

    if-nez v26, :cond_0

    .line 51
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 54
    :cond_0
    const-string v26, "07"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 55
    const/16 v18, 0x0

    .line 58
    .local v18, "enable":I
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v26

    const-string v27, "country_cert_info_enable"

    invoke-static/range {v26 .. v27}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    .line 63
    :goto_0
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "ShowCountryCertInfo : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const/16 v26, 0x1

    move/from16 v0, v18

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    .line 66
    const-class v26, Lcom/sec/android/app/status/ShowCountryCertInfo;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 67
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 68
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 364
    .end local v18    # "enable":I
    .end local v19    # "host":Ljava/lang/String;
    .end local v20    # "i":Landroid/content/Intent;
    :cond_1
    :goto_1
    return-void

    .line 59
    .restart local v18    # "enable":I
    .restart local v19    # "host":Ljava/lang/String;
    .restart local v20    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v17

    .line 60
    .local v17, "e":Ljava/lang/Exception;
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Failed to get country_cert_info_enable to Settings."

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v18    # "enable":I
    :cond_2
    const-string v26, "22888973"

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 71
    const-class v26, Lcom/sec/android/app/status/BattType;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 72
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 73
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 74
    :cond_3
    const-string v26, "0228"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 75
    const-string v26, "KOREA"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mCountryCode:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 76
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "CSC Country code is KOR"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-class v26, Lcom/sec/android/app/status/BatteryStatus_kor;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 82
    :goto_2
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 83
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 79
    :cond_4
    const-class v26, Lcom/sec/android/app/status/BatteryStatus;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_2

    .line 84
    :cond_5
    const-string v26, "02288"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 85
    const-class v26, Lcom/sec/android/app/status/BatteryStatusEng;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 86
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 87
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 88
    :cond_6
    const-string v26, "0589"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 89
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "0589"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-class v26, Lcom/sec/android/app/status/LightsensorRead;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 91
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 93
    :cond_7
    const-string v26, "80"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 94
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "80"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-class v26, Lcom/sec/android/app/status/TouchNoise;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 96
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 97
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 98
    :cond_8
    const-string v26, "251"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 99
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    if-nez v26, :cond_9

    .line 100
    const-string v26, "power"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/os/PowerManager;

    .line 101
    .local v22, "pm":Landroid/os/PowerManager;
    const/16 v26, 0x1a

    const-string v27, "by KeyString"

    move-object/from16 v0, v22

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v26

    sput-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 105
    .end local v22    # "pm":Landroid/os/PowerManager;
    :cond_9
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual/range {v26 .. v26}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v26

    if-nez v26, :cond_a

    .line 106
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual/range {v26 .. v26}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 107
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "acquire WakeLock by KeyString"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 109
    :cond_a
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "already acquired WakeLock by KeyString"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 111
    :cond_b
    const-string v26, "250"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 112
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    if-eqz v26, :cond_c

    .line 113
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual/range {v26 .. v26}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v26

    if-eqz v26, :cond_1

    .line 114
    sget-object v26, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual/range {v26 .. v26}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 115
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "release WakeLock by KeyString"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 118
    :cond_c
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "mWl is null!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 120
    :cond_d
    const-string v26, "350"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 121
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "*#350# start"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v24, Landroid/content/Intent;

    const-string v26, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 123
    .local v24, "startIntent":Landroid/content/Intent;
    const-string v26, "android.intent.action.REBOOT"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v26, "android.intent.extra.KEY_CONFIRM"

    const/16 v27, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 125
    const/high16 v26, 0x10000000

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 126
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 127
    .end local v24    # "startIntent":Landroid/content/Intent;
    :cond_e
    const-string v26, "5238973"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 128
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "LCD_TYPE Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-class v26, Lcom/sec/android/app/status/LcdTypeInfo;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 130
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 131
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 132
    :cond_f
    const-string v26, "4327"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 133
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "HDCP Test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-class v26, Lcom/sec/android/app/status/HdcpCheckDialog;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 135
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 136
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 137
    :cond_10
    const-string v26, "22235"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 138
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Acceler sensor cal data  Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-class v26, Lcom/sec/android/app/status/AccSensorCalibration;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 140
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 141
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 142
    :cond_11
    const-string v26, "4378873"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 143
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "GestureSensor sensor Test  Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-class v26, Lcom/sec/android/app/status/GestureSensor;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 145
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 146
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 147
    :cond_12
    const-string v26, "7736"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_13

    .line 148
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "S-Pen detection Test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-class v26, Lcom/sec/android/app/status/SPenDetectionTest;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 150
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 151
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 152
    :cond_13
    const-string v26, "77368378"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 153
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "SPentestUI Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-class v26, Lcom/sec/android/app/status/SPenTestUI;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 155
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 156
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 157
    :cond_14
    const-string v26, "77361379"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_15

    .line 158
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "SPentestMode Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-class v26, Lcom/sec/android/app/status/SPenTestMode;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 160
    const/16 v26, 0x20

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 161
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 162
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 163
    :cond_15
    const-string v26, "3736"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_16

    .line 164
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "EPentestMain Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-class v26, Lcom/sec/android/app/status/EPenTestMain;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 166
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 167
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 168
    :cond_16
    const-string v26, "0782"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_17

    .line 169
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "RCTTime Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-class v26, Lcom/sec/android/app/status/RTCTimeRead;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 171
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 172
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 173
    :cond_17
    const-string v26, "86824"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_18

    .line 174
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Touch_key_sensitivity Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-class v26, Lcom/sec/android/app/status/touch_key_sensitivity;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 176
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 177
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 178
    :cond_18
    const-string v26, "0842"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_19

    .line 179
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "vibration_test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-class v26, Lcom/sec/android/app/status/vibration_test;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 181
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 182
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 183
    :cond_19
    const-string v26, "0673"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1a

    .line 184
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "melody_test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-class v26, Lcom/sec/android/app/status/melody_test;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 186
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 187
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 188
    :cond_1a
    const-string v26, "0289"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1b

    .line 189
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "melody_test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-class v26, Lcom/sec/android/app/status/melody_test;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 191
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 193
    :cond_1b
    const-string v26, "2663"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1c

    .line 194
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "touch_firmware Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-class v26, Lcom/sec/android/app/status/touch_firmware;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 196
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 197
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 198
    :cond_1c
    const-string v26, "2664"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1d

    .line 199
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "PointerLocation Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-class v26, Lcom/sec/android/app/status/PointerLocation;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 201
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 202
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 203
    :cond_1d
    const-string v26, "0588"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1e

    .line 204
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Acceler sensor cal data  Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-class v26, Lcom/sec/android/app/status/Proximity_test;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 206
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 207
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 208
    :cond_1e
    const-string v26, "3264"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1f

    .line 209
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "ram_check Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-class v26, Lcom/sec/android/app/status/ram_check;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 211
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 212
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 213
    :cond_1f
    const-string v26, "7780"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_20

    .line 214
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "MasterClear Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v26, "com.android.settings"

    const-string v27, "com.android.settings.MasterClear"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 217
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 218
    :cond_20
    const-string v26, "1004"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_21

    .line 219
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Sound Source Test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-class v26, Lcom/sec/android/app/status/SoundSourceTest;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 221
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 222
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 223
    :cond_21
    const-string v26, "7769"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_24

    .line 224
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Proximity_offse Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->isSharpNonADCChipset()Z

    move-result v26

    if-eqz v26, :cond_22

    .line 227
    const-class v26, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 235
    :goto_3
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 236
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 229
    :cond_22
    const-string v26, "IS_PROXIOFFSET_JB_CONCEPT"

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    if-eqz v26, :cond_23

    .line 230
    const-class v26, Lcom/sec/android/app/status/Proximity_offset_JB;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_3

    .line 232
    :cond_23
    const-class v26, Lcom/sec/android/app/status/Proximity_offset;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_3

    .line 237
    :cond_24
    const-string v26, "87976633"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_25

    .line 239
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Change Keystring block to user mode"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/status/ChangeUserMode;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/status/ChangeUserMode;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/status/ChangeUserMode;->changeToUserMode()Z

    goto/16 :goto_1

    .line 241
    :cond_25
    const-string v26, "9998*3288"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_28

    .line 242
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadTACounter()J

    move-result-wide v4

    .line 243
    .local v4, "countA":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadEPenCounter()J

    move-result-wide v6

    .line 244
    .local v6, "countB":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadEarjackCounter()J

    move-result-wide v8

    .line 245
    .local v8, "countC":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadQwertyCounter()J

    move-result-wide v10

    .line 246
    .local v10, "countD":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadFlashPopupCounter()J

    move-result-wide v12

    .line 247
    .local v12, "countE":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->loadTMBCounter()J

    move-result-wide v14

    .line 248
    .local v14, "countF":J
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->getCoverCount()Ljava/lang/String;

    move-result-object v26

    if-eqz v26, :cond_26

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "\nG : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->getCoverCount()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 250
    .local v16, "countG":Ljava/lang/String;
    :goto_4
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "A : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\nB : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\nC : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\nD : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\nE : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\nF : "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    if-eqz v16, :cond_27

    .end local v16    # "countG":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 248
    :cond_26
    const/16 v16, 0x0

    goto :goto_4

    .line 250
    .restart local v16    # "countG":Ljava/lang/String;
    :cond_27
    const-string v16, ""

    goto :goto_5

    .line 254
    .end local v4    # "countA":J
    .end local v6    # "countB":J
    .end local v8    # "countC":J
    .end local v10    # "countD":J
    .end local v12    # "countE":J
    .end local v14    # "countF":J
    .end local v16    # "countG":Ljava/lang/String;
    :cond_28
    const-string v26, "767*2878"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_29

    .line 255
    const-wide/16 v26, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->saveQwertyCounter(J)V

    .line 256
    const-string v26, "Qwerty slide counter is reset"

    const/16 v27, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 257
    :cond_29
    const-string v26, "0283"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2d

    .line 258
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Packet Loopback Test Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v26, "SUPPORT_COMPLEX_LOOPBACK_TEST"

    const/16 v27, 0x0

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    if-nez v26, :cond_2a

    const-string v26, "SUPPORT_COMPLEX_LOOPBACK_PATH"

    invoke-static/range {v26 .. v26}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_2b

    .line 262
    :cond_2a
    const-class v26, Lcom/sec/android/app/status/LoopbackTest2;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 269
    :goto_6
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 270
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 263
    :cond_2b
    const-string v26, "SUPPORT_PIEZO_RCV"

    const/16 v27, 0x1

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_2c

    .line 264
    const-class v26, Lcom/sec/android/app/status/LoopbackTestDual;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_6

    .line 266
    :cond_2c
    const-class v26, Lcom/sec/android/app/status/LoopbackTest;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_6

    .line 271
    :cond_2d
    const-string v26, "7328735824"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2e

    .line 272
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Lot ID status Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-class v26, Lcom/sec/android/app/status/SecureJT;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 274
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 275
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 276
    :cond_2e
    const-string v26, "22228378"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_2f

    .line 277
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "CABC Mode Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-class v26, Lcom/sec/android/app/status/cabctest;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 279
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 280
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 281
    :cond_2f
    const-string v26, "1105"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_30

    .line 282
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "display failhist"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-class v26, Lcom/sec/android/app/status/FailHistRead;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 284
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 285
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 286
    :cond_30
    const-string v26, "09070907"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_31

    .line 287
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Install Selftest Content"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/entry/FactoryKeyStringBroadcastReceiver;->installSelfTestApp(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 289
    :cond_31
    const-string v26, "726387"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_32

    .line 290
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run PCM Dump"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const-class v26, Lcom/sec/android/app/status/PCMDump;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 292
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 293
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 294
    :cond_32
    const-string v26, "8778378"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_33

    .line 295
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run TSP Test mode"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const-class v26, Lcom/sec/android/app/status/TspTestMode;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 297
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 298
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 299
    :cond_33
    const-string v26, "46837"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_34

    .line 300
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run setting app for glove touch"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const-class v26, Lcom/sec/android/app/status/GloveSetting;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 302
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 303
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 304
    :cond_34
    const-string v26, "26732875"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_35

    .line 305
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run Core CTRL"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const-class v26, Lcom/sec/android/app/status/CoreCTRL;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 307
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 308
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 309
    :cond_35
    const-string v26, "435517"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_36

    .line 310
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run Grip calibration"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-class v26, Lcom/sec/android/app/status/GripSensorCalibrationEntry;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 312
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 313
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 314
    :cond_36
    const-string v26, "1106"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_37

    .line 315
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run Grip On/Off"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-class v26, Lcom/sec/android/app/status/GripSensorCalibrationEntry;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 317
    const-string v26, "host"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 319
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 320
    :cond_37
    const-string v26, "272447"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_39

    .line 321
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    const-string v28, "run AP Chip test"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const-string v26, "qualcomm"

    const-string v27, "CHIPSET_MANUFACTURE"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v21

    .line 324
    .local v21, "isQualcomm":Z
    if-eqz v21, :cond_38

    .line 325
    const-class v26, Lcom/sec/android/app/status/AP_chip_test_Qualcomm;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 330
    :goto_7
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 331
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 327
    :cond_38
    const-class v26, Lcom/sec/android/app/status/AP_chip_test;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_7

    .line 332
    .end local v21    # "isQualcomm":Z
    :cond_39
    const-string v26, "6364366633"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3a

    .line 333
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "MDNIE Mode Start!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-class v26, Lcom/sec/android/app/status/MdnieTest;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 335
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 336
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 337
    :cond_3a
    const-string v26, "877"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3b

    .line 338
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Show result of TSP!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-class v26, Lcom/sec/android/app/status/TspResult;

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 340
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 341
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 342
    :cond_3b
    const-string v26, "0775"

    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    const-string v26, "factory"

    const-string v27, "BINARY_TYPE"

    invoke-static/range {v27 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Properties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 343
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive"

    const-string v28, "Show receiverSpeakerTest!!"

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const-string v26, "com.sec.android.app.factorykeystring"

    const-string v27, "com.sec.android.app.status.receiverSpeakerTest"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const/high16 v26, 0x10000000

    move-object/from16 v0, v20

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 347
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 351
    .end local v19    # "host":Ljava/lang/String;
    .end local v20    # "i":Landroid/content/Intent;
    :cond_3c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v26

    const-string v27, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 352
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v26

    if-nez v26, :cond_3d

    .line 353
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 356
    :cond_3d
    const-string v26, "pref_hardReset"

    const/16 v27, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v23

    .line 357
    .local v23, "preferences":Landroid/content/SharedPreferences;
    const-string v26, "mode_hardReset"

    const-string v27, "N"

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 358
    .local v25, "value":Ljava/lang/String;
    const-string v26, "FactoryKeyStringBroadcastReceiver"

    const-string v27, "onReceive, "

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "pref_hardReset : "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v26, "ON"

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 361
    const-string v26, "RESET_ENABLED"

    const-string v27, "1"

    invoke-static/range {v26 .. v27}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_1
.end method
