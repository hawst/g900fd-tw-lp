.class Lcom/sec/android/app/status/ModuleAudioService$1;
.super Landroid/os/Handler;
.source "ModuleAudioService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/ModuleAudioService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/ModuleAudioService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/ModuleAudioService;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    .line 63
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mIsRunningThread:Z
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$000(Lcom/sec/android/app/status/ModuleAudioService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "ModuleAudioService"

    const-string v1, "STOP_LOOP : Audio Loopback thread is running"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    iget-object v0, v0, Lcom/sec/android/app/status/ModuleAudioService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioTrack;->flush()V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$100(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioTrack;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # setter for: Lcom/sec/android/app/status/ModuleAudioService;->mAudioTrack:Landroid/media/AudioTrack;
    invoke-static {v0, v5}, Lcom/sec/android/app/status/ModuleAudioService;->access$102(Lcom/sec/android/app/status/ModuleAudioService;Landroid/media/AudioTrack;)Landroid/media/AudioTrack;

    .line 79
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$200(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioRecord;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$200(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$200(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 83
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # getter for: Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0}, Lcom/sec/android/app/status/ModuleAudioService;->access$200(Lcom/sec/android/app/status/ModuleAudioService;)Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/ModuleAudioService$1;->this$0:Lcom/sec/android/app/status/ModuleAudioService;

    # setter for: Lcom/sec/android/app/status/ModuleAudioService;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v0, v5}, Lcom/sec/android/app/status/ModuleAudioService;->access$202(Lcom/sec/android/app/status/ModuleAudioService;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
