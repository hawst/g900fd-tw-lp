.class Lcom/sec/android/app/status/Proximity_offset_JB$1;
.super Landroid/os/Handler;
.source "Proximity_offset_JB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/Proximity_offset_JB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/Proximity_offset_JB;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/Proximity_offset_JB;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 77
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 79
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mAdcView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$100(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "   :  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;
    invoke-static {v2}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$000(Lcom/sec/android/app/status/Proximity_offset_JB;)Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->getADC()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 82
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$200(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    invoke-virtual {v2}, Lcom/sec/android/app/status/Proximity_offset_JB;->readOffset()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 85
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mOffsetView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$200(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ":  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_JB$1;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    invoke-virtual {v2}, Lcom/sec/android/app/status/Proximity_offset_JB;->readOffset()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
