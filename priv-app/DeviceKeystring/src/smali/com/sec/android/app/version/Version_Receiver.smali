.class public Lcom/sec/android/app/version/Version_Receiver;
.super Landroid/content/BroadcastReceiver;
.source "Version_Receiver.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/version/Version_Receiver;->mContext:Landroid/content/Context;

    .line 38
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 39
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "host":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 43
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 46
    :cond_0
    const-string v2, "12580*369"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 47
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "12580*369"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-class v2, Lcom/sec/android/app/version/MainVersion;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 76
    :goto_0
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 79
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    return-void

    .line 49
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v1    # "i":Landroid/content/Intent;
    :cond_2
    const-string v2, "1234"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 50
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "1234"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-class v2, Lcom/sec/android/app/version/SimpleVersion;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 52
    :cond_3
    const-string v2, "99732"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 53
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "99732"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-class v2, Lcom/sec/android/app/version/BuildInfo;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 55
    :cond_4
    const-string v2, "9999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 56
    const-class v2, Lcom/sec/android/app/version/SimpleVersionTMO;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 57
    :cond_5
    const-string v2, "4943"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 58
    const-class v2, Lcom/sec/android/app/version/HWIDInfo;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 63
    :cond_6
    const-string v2, "1111_WIFI"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 64
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "1111_WIFI"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-class v2, Lcom/sec/android/app/version/FTASWVersion;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 66
    :cond_7
    const-string v2, "2222_WIFI"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 67
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "2222_WIFI"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-class v2, Lcom/sec/android/app/version/FTAHWVersion;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    .line 69
    :cond_8
    const-string v2, "278837"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    const-string v2, "Version_Receiver"

    const-string v3, "onReceive"

    const-string v4, "CPU_VERSION : 278837"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-class v2, Lcom/sec/android/app/version/CpuVersion;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0
.end method
