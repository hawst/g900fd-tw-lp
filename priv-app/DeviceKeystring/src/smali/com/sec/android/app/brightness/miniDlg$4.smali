.class Lcom/sec/android/app/brightness/miniDlg$4;
.super Ljava/lang/Object;
.source "miniDlg.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/brightness/miniDlg;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/brightness/miniDlg;


# direct methods
.method constructor <init>(Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg$4;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 140
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 142
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 151
    :cond_0
    :goto_0
    return v2

    .line 144
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$4;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/brightness/miniDlg;->isButtonPushing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$4;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # getter for: Lcom/sec/android/app/brightness/miniDlg;->timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    invoke-static {v1}, Lcom/sec/android/app/brightness/miniDlg;->access$500(Lcom/sec/android/app/brightness/miniDlg;)Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->cancel()V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$4;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # setter for: Lcom/sec/android/app/brightness/miniDlg;->mButtonPushing:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/brightness/miniDlg;->access$302(Lcom/sec/android/app/brightness/miniDlg;Z)Z

    goto :goto_0

    .line 142
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
