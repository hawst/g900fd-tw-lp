.class public Lcom/sec/android/app/status/FailHistRead;
.super Landroid/app/Activity;
.source "FailHistRead.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;
    }
.end annotation


# static fields
.field private static final FAIL_HISTORY_TITLE:[Ljava/lang/String;


# instance fields
.field private mBackButton:Landroid/widget/Button;

.field private mClicked:Landroid/view/View$OnClickListener;

.field private mEraseButton:Landroid/widget/Button;

.field private mFailHistItem:Landroid/widget/TextView;

.field private mFailHistProcess:Landroid/widget/TextView;

.field private mFailHistSpec:Landroid/widget/TextView;

.field private mFailHistTime:Landroid/widget/TextView;

.field private mFailHistValue:Landroid/widget/TextView;

.field private mFailHistVersion:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field private mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "1. PROCESS : "

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "2. ITEM : "

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "3. SPEC : "

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "4. VALUE : "

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "5. DATE: "

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    .line 65
    new-instance v0, Lcom/sec/android/app/status/FailHistRead$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/FailHistRead$1;-><init>(Lcom/sec/android/app/status/FailHistRead;)V

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mHandler:Landroid/os/Handler;

    .line 79
    new-instance v0, Lcom/sec/android/app/status/FailHistRead$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/FailHistRead$2;-><init>(Lcom/sec/android/app/status/FailHistRead;)V

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mClicked:Landroid/view/View$OnClickListener;

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/FailHistRead;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/FailHistRead;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/status/FailHistRead;->printFailHist()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/FailHistRead;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FailHistRead;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mBackButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/FailHistRead;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FailHistRead;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/FailHistRead;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FailHistRead;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method private printFailHist()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 94
    const-string v0, "NONE"

    .line 95
    .local v0, "failhist":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistVersion:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    const-string v4, "V1"

    iget-object v5, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 97
    const-string v4, "persist.sys.factory.failhist"

    const-string v5, "NONE"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_0
    :goto_0
    const-string v4, "FailHistRead"

    const-string v5, "printFailHist"

    invoke-static {v4, v5, v0}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v4, "NONE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v0, :cond_3

    .line 103
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistProcess:Landroid/widget/TextView;

    const-string v5, "N"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistItem:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistSpec:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistValue:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistTime:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    :goto_1
    return-void

    .line 98
    :cond_2
    const-string v4, "V2"

    iget-object v5, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 99
    const-string v4, "/efs/FactoryApp/SSFH"

    invoke-static {v4, v7}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_3
    const/4 v4, 0x5

    new-array v1, v4, [Ljava/lang/String;

    .line 111
    .local v1, "history":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    const/4 v4, 0x5

    if-ge v2, v4, :cond_4

    .line 112
    const-string v4, ""

    aput-object v4, v1, v2

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 115
    :cond_4
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "tempHist":[Ljava/lang/String;
    const/4 v2, 0x0

    :goto_3
    array-length v4, v3

    if-ge v2, v4, :cond_5

    .line 117
    aget-object v4, v3, v2

    aput-object v4, v1, v2

    .line 116
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 120
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistProcess:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistItem:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v9

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v9

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistSpec:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v10

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v10

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistValue:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v11

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v11

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const-string v4, "V1"

    iget-object v5, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 126
    const-string v4, "persist.sys.factory.fh.time"

    const-string v5, "NONE"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v8

    .line 128
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistTime:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/status/FailHistRead;->FAIL_HISTORY_TITLE:[Ljava/lang/String;

    aget-object v6, v6, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    invoke-virtual {v4, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 49
    :cond_0
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->setContentView(I)V

    .line 50
    const v0, 0x7f0a0060

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistVersion:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0a0061

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistProcess:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0a0062

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistItem:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f0a0063

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistSpec:Landroid/widget/TextView;

    .line 54
    const v0, 0x7f0a0064

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistValue:Landroid/widget/TextView;

    .line 55
    const v0, 0x7f0a0065

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mFailHistTime:Landroid/widget/TextView;

    .line 56
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 58
    const v0, 0x7f0a0067

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/FailHistRead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mBackButton:Landroid/widget/Button;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mBackButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead;->mClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const-string v0, "FAILHIST_VERSION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x44c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 141
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 142
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 134
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/status/FailHistRead;->printFailHist()V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/status/FailHistRead;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x44c

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 137
    return-void
.end method
