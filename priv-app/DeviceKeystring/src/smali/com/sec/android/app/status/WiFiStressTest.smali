.class public Lcom/sec/android/app/status/WiFiStressTest;
.super Landroid/app/Activity;
.source "WiFiStressTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;,
        Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;
    }
.end annotation


# instance fields
.field private OFF_TIME:I

.field private ON_TIME:I

.field private REPEAT_TIME:I

.field private mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

.field public mHandler:Landroid/os/Handler;

.field private mIsRemoteCall:Z

.field private mOffTime:Landroid/widget/TextView;

.field private mOnTime:Landroid/widget/TextView;

.field private mPM:Landroid/os/PowerManager;

.field private mRepeat:Landroid/widget/TextView;

.field private mResultText:Landroid/widget/TextView;

.field private mStartButton:Landroid/widget/Button;

.field private mTestCount:I

.field private mWakeUp:Landroid/os/PowerManager$WakeLock;

.field private mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

.field private mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiSwitch:Landroid/widget/Switch;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mIsRemoteCall:Z

    .line 64
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    .line 65
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    .line 66
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWakeUp:Landroid/os/PowerManager$WakeLock;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iput-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 73
    iput v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    .line 81
    sget-object v0, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->DISABLED:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    iput-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .line 97
    new-instance v0, Lcom/sec/android/app/status/WiFiStressTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/WiFiStressTest$1;-><init>(Lcom/sec/android/app/status/WiFiStressTest;)V

    iput-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$008(Lcom/sec/android/app/status/WiFiStressTest;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/WiFiStressTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/status/WiFiStressTest;->canceltAlarm()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/WiFiStressTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/status/WiFiStressTest;->setAlarm()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/status/WiFiStressTest;Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;)Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;
    .param p1, "x1"    # Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/WiFiStressTest;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    return-object v0
.end method

.method private canceltAlarm()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 382
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 385
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 386
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.status.WakeUp"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 387
    const-string v3, "isAcquireWakelock"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 390
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 391
    return-void
.end method

.method private disableSwitch(Landroid/widget/CompoundButton;)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;

    .prologue
    const/4 v0, 0x0

    .line 520
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setActivated(Z)V

    .line 521
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setClickable(Z)V

    .line 522
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 523
    return-void
.end method

.method private setAlarm()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 370
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "alarm"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 373
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 374
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.status.WakeUp"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    const-string v3, "isAcquireWakelock"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 376
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v8, v1, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 378
    .local v2, "sender":Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v0, v8, v4, v5, v2}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 379
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 438
    const-string v0, "WiFiStressTest"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 394
    const-string v0, "WiFiStressTest"

    const-string v1, "onClick"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "v = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 434
    :goto_0
    :pswitch_0
    return-void

    .line 397
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    if-ne v0, v1, :cond_0

    .line 398
    sget-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->START:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->startTest()V

    goto :goto_0

    .line 402
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->stopTest()V

    goto :goto_0

    .line 408
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    add-int/lit16 v0, v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOnTime:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 412
    :pswitch_3
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOnTime:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 416
    :pswitch_4
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    add-int/lit16 v0, v0, -0x3e8

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOffTime:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 420
    :pswitch_5
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOffTime:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    div-int/lit16 v1, v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 424
    :pswitch_6
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mRepeat:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto/16 :goto_0

    .line 428
    :pswitch_7
    iget v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mRepeat:Landroid/widget/TextView;

    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    goto/16 :goto_0

    .line 395
    :pswitch_data_0
    .packed-switch 0x7f0a02ca
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 147
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "remote"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mIsRemoteCall:Z

    .line 151
    :cond_0
    const-string v3, "WiFiStressTest"

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mIsRemoteCall = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/status/WiFiStressTest;->mIsRemoteCall:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const v3, 0x7f030074

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->setContentView(I)V

    .line 155
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    const-string v3, "WiFiStressTest"

    const-string v4, "onCreate"

    const-string v5, "alarm on"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 166
    :cond_1
    const-string v3, "WiFiStressTest"

    const-string v4, "onCreate()"

    const-string v5, "inform start to sysfs"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v3, "/sys/power/pm_wifi_test"

    const-string v4, "1"

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 170
    new-instance v3, Lcom/sec/android/app/status/WiFiStressTest$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/WiFiStressTest$2;-><init>(Lcom/sec/android/app/status/WiFiStressTest;)V

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 184
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/status/WiFiStressTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/16 v4, 0x1a

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    const/16 v4, 0xbb

    invoke-static {v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mPM:Landroid/os/PowerManager;

    .line 192
    const v3, 0x7f0a02c8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;

    const-string v4, "WIFI"

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->disableSwitch(Landroid/widget/CompoundButton;)V

    .line 197
    const v3, 0x7f0a02c9

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    .line 199
    const v3, 0x7f0a02ca

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    sget-object v4, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 204
    const v3, 0x7f0a02cb

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOnTime:Landroid/widget/TextView;

    .line 205
    const v3, 0x7f0a02ce

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOffTime:Landroid/widget/TextView;

    .line 206
    const v3, 0x7f0a02d1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mRepeat:Landroid/widget/TextView;

    .line 208
    const v3, 0x7f0a02cc

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    const v3, 0x7f0a02cd

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v3, 0x7f0a02cf

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const v3, 0x7f0a02d0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    const v3, 0x7f0a02d2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    const v3, 0x7f0a02d3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOnTime:Landroid/widget/TextView;

    iget v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    div-int/lit16 v4, v4, 0x3e8

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mOffTime:Landroid/widget/TextView;

    iget v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->OFF_TIME:I

    div-int/lit16 v4, v4, 0x3e8

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mRepeat:Landroid/widget/TextView;

    iget v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/status/WiFiStressTest;->setText(Landroid/widget/TextView;I)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "wifi_scan_always_enabled"

    invoke-static {v3, v4, v8}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 221
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/WiFiStressTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 223
    .local v1, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    sget-object v3, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->ENBLED:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v8}, Landroid/widget/Switch;->setChecked(Z)V

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v4, "WiFi already enabled"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    :goto_0
    return-void

    .line 230
    :cond_2
    sget-object v3, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->ENABLING:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .line 231
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v4, "Turning on WiFi..."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setActivated(Z)V

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setClickable(Z)V

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 235
    invoke-virtual {v1, v8}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v2

    .line 236
    .local v2, "wifiRequestOn":Z
    const-string v3, "WiFiStressTest"

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wifiRequestOn = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    new-instance v3, Lcom/sec/android/app/status/WiFiStressTest$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/WiFiStressTest$3;-><init>(Lcom/sec/android/app/status/WiFiStressTest;)V

    iput-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 268
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 269
    .local v0, "intentfilter":Landroid/content/IntentFilter;
    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 270
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/status/WiFiStressTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 332
    const-string v2, "WiFiStressTest"

    const-string v3, "onDestroy"

    const-string v4, "onDestroy"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiStatusBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/WiFiStressTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 337
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mAlarmManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/WiFiStressTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 341
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 345
    :cond_2
    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/WiFiStressTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 346
    .local v1, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v0

    .line 347
    .local v0, "result":Z
    const-string v2, "WiFiStressTest"

    const-string v3, "onCreate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "turn off wifi = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "wifi_scan_always_enabled"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 350
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 352
    const-string v2, "WiFiStressTest"

    const-string v3, "onDestroy"

    const-string v4, "alarm off"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/WiFiStressTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 362
    :cond_3
    const-string v2, "WiFiStressTest"

    const-string v3, "onDestroy()"

    const-string v4, "inform end to sysfs"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v2, "/sys/power/pm_wifi_test"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 366
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 367
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 511
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->finish()V

    .line 516
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 513
    :cond_1
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 514
    invoke-virtual {p0}, Lcom/sec/android/app/status/WiFiStressTest;->finish()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 326
    const-string v0, "WiFiStressTest"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 328
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 276
    const-string v1, "WiFiStressTest"

    const-string v2, "onResume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWifiState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    sget-object v2, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->ENABLING:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    if-ne v1, v2, :cond_1

    .line 280
    const-string v1, "WiFiStressTest"

    const-string v2, "onResume"

    const-string v3, "WIFI_STATE.ENABLING"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    sget-object v2, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->DISABLED:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    if-ne v1, v2, :cond_2

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v2, "Error : WIFI ON FAILED "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 289
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    if-ne v1, v2, :cond_3

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 298
    :cond_3
    const-string v1, "WiFiStressTest"

    const-string v2, "onResume()"

    const-string v3, "inform to sysfs on testing"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v1, "/sys/power/pm_wifi_test"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 302
    iget v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    iget v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->REPEAT_TIME:I

    if-ge v1, v2, :cond_4

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Test #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/WiFiStressTest;->ON_TIME:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 306
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v2, "PASS"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 310
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.aporiented.athandler.AtWifitest.WIFI_STRESS_TEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 311
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "result"

    const-string v2, "OK"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 312
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/WiFiStressTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 313
    iget-boolean v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mIsRemoteCall:Z

    if-eqz v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/status/WiFiStressTest$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/WiFiStressTest$4;-><init>(Lcom/sec/android/app/status/WiFiStressTest;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public setText(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "n"    # I

    .prologue
    .line 486
    const-string v0, "WiFiStressTest"

    const-string v1, "setText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/widget/TextView;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 488
    return-void
.end method

.method public startTest()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 456
    const-string v1, "WiFiStressTest"

    const-string v2, "startTest"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    iput v4, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 460
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v2, "STARTED"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/WiFiStressTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 464
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 466
    :cond_0
    const-string v1, "WiFiStressTest"

    const-string v2, "startTest"

    const-string v3, "WIFI ON"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 473
    return-void

    .line 468
    :cond_1
    const-string v1, "WiFiStressTest"

    const-string v2, "startTest"

    const-string v3, "SET WIFI ON"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    goto :goto_0
.end method

.method public stopTest()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;

    const-string v1, "STOPPED"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    iput v2, p0, Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I

    .line 483
    return-void
.end method
