.class public Lcom/sec/android/app/status/FactoryTestPhone;
.super Ljava/lang/Object;
.source "FactoryTestPhone.java"


# static fields
.field private static mQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private IS_CAL_TEST_PASS:Z

.field private IS_FINAL_TEST_PASS:Z

.field private IS_LTECAL_TEST_PASS:Z

.field private IS_LTEFINAL_TEST_PASS:Z

.field private IS_PBA_TEST_PASS:Z

.field private IS_SMD_TEST_PASS:Z

.field private isConnected:Z

.field mContext:Landroid/content/Context;

.field private mData:[B

.field public mHandler:Landroid/os/Handler;

.field private final mNVKeyToItemIDHashmap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private mResponse:Landroid/os/Message;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mSvcModeMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "FactoryTestPhone"

    iput-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    .line 34
    iput-object v2, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->isConnected:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_SMD_TEST_PASS:Z

    .line 52
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_PBA_TEST_PASS:Z

    .line 53
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_CAL_TEST_PASS:Z

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_FINAL_TEST_PASS:Z

    .line 55
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_LTECAL_TEST_PASS:Z

    .line 56
    iput-boolean v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->IS_LTEFINAL_TEST_PASS:Z

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mNVKeyToItemIDHashmap:Ljava/util/HashMap;

    .line 147
    new-instance v0, Lcom/sec/android/app/status/FactoryTestPhone$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/FactoryTestPhone$1;-><init>(Lcom/sec/android/app/status/FactoryTestPhone;)V

    iput-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 354
    new-instance v0, Lcom/sec/android/app/status/FactoryTestPhone$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/FactoryTestPhone$2;-><init>(Lcom/sec/android/app/status/FactoryTestPhone;)V

    iput-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mContext:Landroid/content/Context;

    .line 67
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mSvcModeMessenger:Landroid/os/Messenger;

    .line 68
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    .line 69
    iput-object v2, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mData:[B

    .line 70
    iput-object v2, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mResponse:Landroid/os/Message;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/FactoryTestPhone;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/FactoryTestPhone;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/status/FactoryTestPhone;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->isConnected:Z

    return p1
.end method

.method static synthetic access$300()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/FactoryTestPhone;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mData:[B

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/FactoryTestPhone;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mResponse:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/FactoryTestPhone;[BLandroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/FactoryTestPhone;
    .param p1, "x1"    # [B
    .param p2, "x2"    # Landroid/os/Message;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/FactoryTestPhone;->sendMessageToRil([BLandroid/os/Message;)V

    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "invokeOemRilRequestRaw"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    sget-object v0, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "invokeOemRilRequestRaw : failed offer a item"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->isConnected:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "isConnected == true"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :goto_1
    sget-object v0, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 207
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/FactoryTestPhone;->sendMessageToRil([BLandroid/os/Message;)V

    goto :goto_1

    .line 210
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->isConnected:Z

    goto :goto_0

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "invokeOemRilRequestRaw"

    const-string v2, "isConnected == false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/status/FactoryTestPhone;->bindSecPhoneService()V

    goto :goto_0
.end method

.method private sendMessageToRil([BLandroid/os/Message;)V
    .locals 5
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 165
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 168
    .local v0, "req":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessage() - current Queue size before : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget-object v1, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 171
    sget-object v1, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "data":[B
    check-cast p1, [B

    .line 172
    .restart local p1    # "data":[B
    const-string v1, "request"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 173
    new-instance p2, Landroid/os/Message;

    .end local p2    # "response":Landroid/os/Message;
    invoke-direct {p2}, Landroid/os/Message;-><init>()V

    .line 174
    .restart local p2    # "response":Landroid/os/Message;
    invoke-virtual {p2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v1, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    if-eqz v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage() to RIL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/status/FactoryTestPhone;->unbindSecPhoneService()V

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendMessage() - current Queue size after : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/status/FactoryTestPhone;->mQueue:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    return-void

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "sendMessageToRil"

    const-string v3, "sendMessage : mQueue is empty!!!"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public bindSecPhoneService()V
    .locals 4

    .prologue
    .line 74
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "bindSecPhoneService"

    const-string v3, "bindSecPhoneService()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 76
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 78
    return-void
.end method

.method public requestGripSensorOn(Z)V
    .locals 12
    .param p1, "isOn"    # Z

    .prologue
    const/4 v11, 0x4

    .line 280
    iget-object v7, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "requestGripSensorOn"

    const-string v9, "requestGripSensorOn()"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 282
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 283
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0x8

    .line 284
    .local v4, "fileSize":I
    const/16 v5, 0x12

    .line 285
    .local v5, "mainCmd":B
    const/16 v6, 0x9

    .line 286
    .local v6, "subCmd":B
    const/4 v1, 0x0

    .line 287
    .local v1, "data":[B
    iget-object v7, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "requestGripSensorOn"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RILSUPPORTBR_____ requestGripSensorOn() ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "MODEL_NAME"

    invoke-static {v10}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]_____RILSUPPORTBR"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    if-eqz p1, :cond_1

    .line 290
    new-array v1, v11, [B

    .end local v1    # "data":[B
    fill-array-data v1, :array_0

    .line 300
    .restart local v1    # "data":[B
    :goto_0
    const/16 v7, 0x12

    :try_start_0
    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 301
    const/16 v7, 0x9

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 302
    const/16 v7, 0x8

    invoke-virtual {v2, v7}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 303
    invoke-virtual {v2, v1}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    if-eqz v2, :cond_0

    .line 309
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 316
    :cond_0
    :goto_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mData:[B

    .line 317
    iget-object v7, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x65

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mResponse:Landroid/os/Message;

    .line 318
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mHandler:Landroid/os/Handler;

    const/16 v9, 0xcd

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/status/FactoryTestPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 319
    return-void

    .line 294
    :cond_1
    new-array v1, v11, [B

    .end local v1    # "data":[B
    fill-array-data v1, :array_1

    .restart local v1    # "data":[B
    goto :goto_0

    .line 310
    :catch_0
    move-exception v3

    .line 311
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 304
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 305
    .local v3, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 307
    if-eqz v2, :cond_0

    .line 309
    :try_start_3
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 310
    :catch_2
    move-exception v3

    .line 311
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 307
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v2, :cond_2

    .line 309
    :try_start_4
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 312
    :cond_2
    :goto_2
    throw v7

    .line 310
    :catch_3
    move-exception v3

    .line 311
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 290
    nop

    :array_0
    .array-data 1
        0x2t
        0x0t
        0xbt
        0x1t
    .end array-data

    .line 294
    :array_1
    .array-data 1
        0x2t
        0x0t
        0xbt
        0x0t
    .end array-data
.end method

.method public unbindSecPhoneService()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "unbindSecPhoneService"

    const-string v2, "unbindSecPhoneService()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/status/FactoryTestPhone;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/FactoryTestPhone;->isConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    goto :goto_0
.end method
