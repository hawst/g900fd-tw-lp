.class public Lcom/sec/android/app/lcdtest/view/Graph;
.super Landroid/view/View;
.source "Graph.java"


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private final GRAPH_SCALING_COOR:I

.field private final INCREASING_COOR:I

.field private final INIT_COOR_X:I

.field private INIT_COOR_Y:I

.field private LIST_SIZE:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPathX:Landroid/graphics/Path;

.field private mPathY:Landroid/graphics/Path;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValueX:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueY:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mXPaint:Landroid/graphics/Paint;

.field private mYPaint:Landroid/graphics/Paint;

.field private mZPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_X:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    .line 22
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INCREASING_COOR:I

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->GRAPH_SCALING_COOR:I

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    .line 40
    const-string v0, "GyroSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/lcdtest/view/Graph;->init(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_X:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    .line 22
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INCREASING_COOR:I

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->GRAPH_SCALING_COOR:I

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    .line 40
    const-string v0, "GyroSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/lcdtest/view/Graph;->init(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_X:I

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    .line 22
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INCREASING_COOR:I

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->GRAPH_SCALING_COOR:I

    .line 30
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    .line 31
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    .line 40
    const-string v0, "GyroSensorGraph"

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/lcdtest/view/Graph;->init(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v7, -0x1000000

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mContext:Landroid/content/Context;

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 60
    .local v0, "mWm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenWidth:I

    .line 61
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenHeight:I

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "init"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mScreenWidth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mScreenHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenHeight:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    .line 64
    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    div-int/lit8 v1, v1, 0x14

    iput v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->LIST_SIZE:I

    .line 65
    new-instance v1, Landroid/graphics/CornerPathEffect;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-direct {v1, v2}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mEffects:Landroid/graphics/PathEffect;

    .line 66
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 71
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 76
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mZPaint:Landroid/graphics/Paint;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mZPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mZPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mZPaint:Landroid/graphics/Paint;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mZPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 81
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 86
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/lcdtest/view/Graph;->invalidate()V

    .line 92
    return-void
.end method

.method private setPath()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 148
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 150
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v3, v1

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v3, v1

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v3, v1

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 157
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x14

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    mul-int/lit8 v1, v0, 0x14

    add-int/lit8 v1, v1, 0xa

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/lcdtest/view/Graph;->invalidate()V

    .line 167
    return-void
.end method


# virtual methods
.method public addValue(FF)V
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->LIST_SIZE:I

    if-le v0, v1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 142
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/lcdtest/view/Graph;->setPath()V

    .line 143
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v1, 0x42f00000    # 120.0f

    const/high16 v11, 0x428c0000    # 70.0f

    const/high16 v10, 0x42480000    # 50.0f

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v9, 0x41200000    # 10.0f

    .line 96
    const/4 v7, 0x0

    .line 97
    .local v7, "textx":F
    const/4 v8, 0x0

    .line 100
    .local v8, "texty":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 112
    :goto_0
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 113
    const-string v0, "MIC1 :"

    iget-object v3, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v10, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 114
    const/high16 v3, 0x435c0000    # 220.0f

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 115
    invoke-static {v7}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 116
    const-string v0, "MIC2 :"

    const/high16 v1, 0x43960000    # 300.0f

    iget-object v3, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v10, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 117
    const/high16 v1, 0x43c80000    # 400.0f

    const/high16 v3, 0x43fa0000    # 500.0f

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 118
    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x43c80000    # 400.0f

    iget-object v2, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v11, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 124
    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    move v3, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 125
    iget v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->INIT_COOR_Y:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathX:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mXPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mPathY:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mYPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 129
    return-void

    .line 106
    :catch_0
    move-exception v6

    .line 107
    .local v6, "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onDraw"

    const-string v4, "get value fail"

    invoke-static {v0, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onDraw"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Size of X, Y : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueX:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->mValueY:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/view/Graph;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "onDraw"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LIST_SIZE value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/lcdtest/view/Graph;->LIST_SIZE:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
