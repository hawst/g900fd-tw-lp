.class public Lcom/sec/android/app/status/GripSensorTest;
.super Landroid/app/Activity;
.source "GripSensorTest.java"


# static fields
.field private static IS_GRIP_COUNT:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private final VIBRATE_TIME:I

.field private info1:Landroid/widget/TextView;

.field private info2:Landroid/widget/TextView;

.field private mBackgroudLayout1:Landroid/widget/LinearLayout;

.field private mBackgroudLayout2:Landroid/widget/LinearLayout;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentTime:J

.field private mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

.field private mIsPressedBackkey:Z

.field private mVibrator:Landroid/os/Vibrator;

.field mWl:Landroid/os/PowerManager$WakeLock;

.field private pass1:Z

.field private pass2:Z

.field private txtgripsensor1:Landroid/widget/TextView;

.field private txtgripsensor2:Landroid/widget/TextView;

.field private working:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest;->working:Z

    .line 37
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest;->pass1:Z

    .line 38
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest;->pass2:Z

    .line 40
    const v0, 0xffff

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest;->VIBRATE_TIME:I

    .line 42
    const-string v0, "GripSensor Test"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->CLASS_NAME:Ljava/lang/String;

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mIsPressedBackkey:Z

    .line 46
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mCurrentTime:J

    .line 190
    new-instance v0, Lcom/sec/android/app/status/GripSensorTest$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorTest$2;-><init>(Lcom/sec/android/app/status/GripSensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private ActiveVibrate(I)V
    .locals 4
    .param p1, "intensity"    # I

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorTest;->startVibration(I)V

    .line 183
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 184
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/sec/android/app/status/GripSensorTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorTest$1;-><init>(Lcom/sec/android/app/status/GripSensorTest;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 188
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->stopVibration()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorTest;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->sendToRilGripSensorStop()V

    return-void
.end method

.method private init()V
    .locals 6

    .prologue
    const v3, 0x7f070096

    const/16 v5, 0x8

    .line 120
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 121
    .local v0, "pm":Landroid/os/PowerManager;
    const/16 v1, 0x1a

    const-string v2, "My Lock Tag"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 122
    const v1, 0x7f0a0093

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->txtgripsensor1:Landroid/widget/TextView;

    .line 123
    const v1, 0x7f0a0096

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->txtgripsensor2:Landroid/widget/TextView;

    .line 124
    const v1, 0x7f0a0094

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;

    .line 125
    const v1, 0x7f0a0097

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 128
    const v1, 0x7f0a0092

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    .line 129
    const v1, 0x7f0a0095

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    .line 130
    const-string v1, "GRIPSENSOR_GRIP_COUNT"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sput v1, Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I

    .line 132
    const-string v1, "GripSensor Test"

    const-string v2, "init"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IS_GRIP_COUNT : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    sget v1, Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->txtgripsensor1:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->txtgripsensor2:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 141
    :cond_0
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    .line 142
    return-void
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 103
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 104
    .local v0, "gripSensorData":Landroid/content/IntentFilter;
    const-string v1, "com.sec.android.app.factorytest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    const-string v1, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/status/GripSensorTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 107
    return-void
.end method

.method private sendToRilGripSensorStart()V
    .locals 3

    .prologue
    .line 110
    const-string v0, "GripSensor Test"

    const-string v1, "sendToRilGripSensorStart"

    const-string v2, "sendToRilGripSensorStart="

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 112
    return-void
.end method

.method private sendToRilGripSensorStop()V
    .locals 3

    .prologue
    .line 115
    const-string v0, "GripSensor Test"

    const-string v1, "sendToRilGripSensorStop"

    const-string v2, "sendToRilGripSensorStop="

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 117
    return-void
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 162
    const-string v1, "GripSensor Test"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 165
    .local v0, "pattern":[J
    if-nez p1, :cond_1

    .line 166
    aput-wide v6, v0, v4

    .line 167
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 173
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 174
    return-void

    .line 168
    :cond_1
    if-ne p1, v5, :cond_0

    .line 169
    aput-wide v6, v0, v4

    .line 170
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 163
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 178
    const-string v0, "GripSensor Test"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const v0, 0x7f030026

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest;->setContentView(I)V

    .line 69
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->init()V

    .line 70
    const-string v0, "GripSensor Test"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/status/FactoryTestPhone;->bindSecPhoneService()V

    .line 73
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 94
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/status/FactoryTestPhone;->unbindSecPhoneService()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    .line 100
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->sendToRilGripSensorStop()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 89
    const-string v0, "GripSensor Test"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 77
    const-string v0, "GripSensor Test"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->registerReceiver()V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest;->sendToRilGripSensorStart()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 81
    return-void
.end method

.method public setBackgroundColor(Landroid/widget/LinearLayout;I)V
    .locals 1
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "id"    # I

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 146
    return-void
.end method
