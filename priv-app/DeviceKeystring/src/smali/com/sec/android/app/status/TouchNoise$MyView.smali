.class public Lcom/sec/android/app/status/TouchNoise$MyView;
.super Landroid/view/View;
.source "TouchNoise.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TouchNoise;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field protected mDrawHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/status/TouchNoise;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TouchNoise;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 888
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    .line 889
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 849
    new-instance v0, Lcom/sec/android/app/status/TouchNoise$MyView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TouchNoise$MyView$1;-><init>(Lcom/sec/android/app/status/TouchNoise$MyView;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mDrawHandler:Landroid/os/Handler;

    .line 890
    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_width:I
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoise;->access$1000(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v0

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_height:I
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoise;->access$1100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 891
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    # setter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {p1, v0}, Lcom/sec/android/app/status/TouchNoise;->access$1202(Lcom/sec/android/app/status/TouchNoise;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 892
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 893
    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 895
    # getter for: Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoise;->access$100(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare_melfas2()V

    .line 900
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare()V

    goto :goto_0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/TouchNoise$MyView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise$MyView;

    .prologue
    .line 839
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare_melfas2()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/TouchNoise$MyView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise$MyView;

    .prologue
    .line 839
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare()V

    return-void
.end method

.method private drawSquare()V
    .locals 15

    .prologue
    const/high16 v14, 0x40400000    # 3.0f

    const/high16 v13, 0x40800000    # 4.0f

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    .line 905
    const/4 v6, 0x0

    .line 906
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 907
    .local v7, "ColY":I
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_width:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1000(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_height:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1300(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    mul-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 909
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 911
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 914
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    mul-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    const/high16 v2, 0x41200000    # 10.0f

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 919
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v2, v13

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v4, v13

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 921
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v2, v13

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v4, v13

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 926
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    add-int/lit8 v5, v5, -0x3

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    add-int/lit8 v5, v5, -0x3

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 940
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 943
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 944
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    int-to-float v1, v8

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 946
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 947
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    int-to-float v1, v9

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 948
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_width:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1000(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v7

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_height:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 946
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 943
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 953
    .end local v9    # "j":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "%d,%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchX:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$2100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchY:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$2200(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    const/high16 v3, 0x41200000    # 10.0f

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v4, 0x41200000    # 10.0f

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 976
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Threshold:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$2400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->thresholdValue:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 977
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Ref:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$2600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 978
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Ref:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$2800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 979
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Ref:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$3000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 980
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Ref:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$3200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 981
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Ref:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$3400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 983
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$300(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 984
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Intensity:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$3600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 985
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Intensity:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$3800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 986
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Intensity:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$4000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 987
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Intensity:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$4200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 988
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Intensity:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$4400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1013
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->thresholdValue:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1019
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$300(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1027
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1035
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1063
    :goto_3
    return-void

    .line 990
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Delta:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_delta_0:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$4600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_5:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 991
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Delta:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_delta_1:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$4800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_6:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 992
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Delta:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_delta_2:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$5000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_7:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 993
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Delta:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_delta_3:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$5200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_8:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 994
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "Delta:%s"

    new-array v2, v12, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$5400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_9:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    .line 1038
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_5:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1039
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_6:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    mul-float/2addr v3, v13

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_7:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_8:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->deltaValue_9:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3
.end method

.method private drawSquare_melfas2()V
    .locals 11

    .prologue
    .line 1066
    const/4 v6, 0x0

    .line 1067
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 1068
    .local v7, "ColY":I
    const/4 v10, 0x5

    .line 1069
    .local v10, "margin":I
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_width:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1000(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_height:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1300(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    const/high16 v4, 0x40400000    # 3.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1078
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1081
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 1082
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    int-to-float v1, v8

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 1084
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    int-to-float v1, v9

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_width:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1000(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v7

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->lcd_height:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1100(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1084
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1081
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1097
    .end local v9    # "j":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$2600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1098
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$2800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_1:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1099
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$3000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_2:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1100
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$3200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_3:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$3400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_4:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$5802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_5:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$5900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1103
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_6:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_7:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$2902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_8:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_9:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1107
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_10:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_10:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$6402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_11:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_11:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$6602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_12:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$6900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_12:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$6802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1110
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_13:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$7100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_13:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1111
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "R:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_refer_14:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$7300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_14:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7202(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_1:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_2:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_3:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1116
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_4:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$5800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$2900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1126
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_10:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$6400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_11:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$6600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_12:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$6800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_13:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1136
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->touchValue_14:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1138
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$3600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_0:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$3800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_1:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$4000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_2:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$4200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_3:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1142
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$4400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_4:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$7802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_5:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$7900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_6:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_7:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$3902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_8:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_9:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$4302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_10:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_10:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$8402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_11:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_11:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$8602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1150
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_12:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$8900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_12:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$8802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_13:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$9100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_13:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$9002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    const-string v1, "I:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_14:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$9300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_14:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoise;->access$9202(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_0:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_1:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1155
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_2:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1156
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_3:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1158
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_4:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$7800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1162
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1164
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$3900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1168
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$4300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_10:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$8400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_11:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$8600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1174
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_12:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$8800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_13:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$9000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->intensityValue_14:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$9200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoise;->access$1500(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1600(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->font_size:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1800(Lcom/sec/android/app/status/TouchNoise;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoise;->access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1180
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1185
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise$MyView;->startTimer()V

    .line 1186
    return-void
.end method

.method protected startTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 881
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoise;->access$100(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mDrawHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 886
    :goto_0
    return-void

    .line 884
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise$MyView;->mDrawHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
