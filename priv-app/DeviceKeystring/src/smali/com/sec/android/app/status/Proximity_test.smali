.class public Lcom/sec/android/app/status/Proximity_test;
.super Landroid/app/Activity;
.source "Proximity_test.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/Proximity_test$1;,
        Lcom/sec/android/app/status/Proximity_test$handlersensor;
    }
.end annotation


# instance fields
.field private Bacckview:Landroid/view/View;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mVibrator:Landroid/os/Vibrator;

.field private mlistner:Lcom/sec/android/app/status/Proximity_test$handlersensor;

.field private txtproximity:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/Proximity_test;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_test;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->txtproximity:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/Proximity_test;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_test;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->Bacckview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/Proximity_test;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_test;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_test;->startVibrate()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/Proximity_test;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_test;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_test;->stopVibrate()V

    return-void
.end method

.method private startVibrate()V
    .locals 3

    .prologue
    .line 54
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 57
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_test;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 58
    return-void

    .line 54
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibrate()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 62
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_test;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->mVibrator:Landroid/os/Vibrator;

    .line 31
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_test;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->mSensorManager:Landroid/hardware/SensorManager;

    .line 32
    new-instance v0, Lcom/sec/android/app/status/Proximity_test$handlersensor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/Proximity_test$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_test;Lcom/sec/android/app/status/Proximity_test$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->mlistner:Lcom/sec/android/app/status/Proximity_test$handlersensor;

    .line 33
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_test;->setContentView(I)V

    .line 34
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->Bacckview:Landroid/view/View;

    .line 35
    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->txtproximity:Landroid/widget/TextView;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->txtproximity:Landroid/widget/TextView;

    const-string v1, "Close to activiate!!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_test;->mlistner:Lcom/sec/android/app/status/Proximity_test$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_test;->stopVibrate()V

    .line 50
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 51
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 41
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_test;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    .line 43
    .local v0, "sensor":Landroid/hardware/Sensor;
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_test;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_test;->mlistner:Lcom/sec/android/app/status/Proximity_test$handlersensor;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v0, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 44
    return-void
.end method
