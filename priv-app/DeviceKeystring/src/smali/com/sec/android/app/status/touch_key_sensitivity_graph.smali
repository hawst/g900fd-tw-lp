.class public Lcom/sec/android/app/status/touch_key_sensitivity_graph;
.super Landroid/app/Activity;
.source "touch_key_sensitivity_graph.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    }
.end annotation


# static fields
.field private static final EXTRA_BUTTON_EVENT:Ljava/lang/String;

.field private static mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

.field private static run_cmd_status:Z


# instance fields
.field private INDEX_1KEY:I

.field private INDEX_2KEY:I

.field private INDEX_3KEY:I

.field private INDEX_4KEY:I

.field private INDEX_5KEY:I

.field private INDEX_6KEY:I

.field private INDEX_BACK:I

.field private INDEX_HOME:I

.field private INDEX_MENU:I

.field private INDEX_RECENT:I

.field private final PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_DUMMY1:Ljava/lang/String;

.field private final PATH_NOISE_TSK_DUMMY3:Ljava/lang/String;

.field private final PATH_NOISE_TSK_DUMMY4:Ljava/lang/String;

.field private final PATH_NOISE_TSK_DUMMY5:Ljava/lang/String;

.field private final PATH_NOISE_TSK_DUMMY6:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HOME:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HOME_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_MENU:Ljava/lang/String;

.field private final PATH_NOISE_TSK_MENU_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_RAWDATA:Ljava/lang/String;

.field private final PATH_RECOMMEND_VERSION:Ljava/lang/String;

.field private final PATH_THD_SPEC:Ljava/lang/String;

.field private final PATH_TSK_UPDATE:Ljava/lang/String;

.field private final PATH_TSK_VERSION:Ljava/lang/String;

.field private backKeyValue:I

.field private dummy1KeyValue:I

.field private dummy3KeyValue:I

.field private dummy4KeyValue:I

.field private dummy5KeyValue:I

.field private dummy6KeyValue:I

.field private homeKeyValue:I

.field private isDummy1key:Z

.field private isDummy3key:Z

.field private isDummy4key:Z

.field private isDummy5key:Z

.field private isDummy6key:Z

.field private isExistsBack:Z

.field private isExistsHome:Z

.field private isExistsMenu:Z

.field private isExistsRecent:Z

.field private isExistsThres:Z

.field private isFiveKeyModel:Z

.field private isFourKeyModel:Z

.field private isSixKeyModel:Z

.field private mContext:Landroid/content/Context;

.field private mWl:Landroid/os/PowerManager$WakeLock;

.field private menuKeyValue:I

.field private recentKeyValue:I

.field private thresholdValue:I

.field private thresholdValue0:I

.field private thresholdValue1:I

.field private thresholdValue2:I

.field private thresholdValue3:I

.field private thresholdValue4:I

.field private thresholdValue5:I

.field private thresholdValueStr:Ljava/lang/String;

.field private timer:Ljava/util/Timer;

.field private timerTask:Ljava/util/TimerTask;

.field private touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "EXTRA_BUTTON_EVENT"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->EXTRA_BUTTON_EVENT:Ljava/lang/String;

    .line 126
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->run_cmd_status:Z

    .line 151
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    sput-object v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    const-string v0, "/sys/class/sec/sec_touchkey/touch_sensitivity"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

    .line 38
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_THD_SPEC:Ljava/lang/String;

    .line 39
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_firm_version_panel"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_TSK_VERSION:Ljava/lang/String;

    .line 40
    const-string v0, "sys/class/sec/sec_touchkey/touchkey_firm_version_phone"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_RECOMMEND_VERSION:Ljava/lang/String;

    .line 41
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_firm_update"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_TSK_UPDATE:Ljava/lang/String;

    .line 42
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_raw_data0"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_BACK_RAWDATA:Ljava/lang/String;

    .line 43
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_raw_data1"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_HOME_RAWDATA:Ljava/lang/String;

    .line 44
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_raw_data2"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_RECENT_RAWDATA:Ljava/lang/String;

    .line 45
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_raw_data3"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_MENU_RAWDATA:Ljava/lang/String;

    .line 50
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_back"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_BACK:Ljava/lang/String;

    .line 51
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_home"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_HOME:Ljava/lang/String;

    .line 52
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_recent"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_RECENT:Ljava/lang/String;

    .line 53
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_menu"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_MENU:Ljava/lang/String;

    .line 55
    const-string v0, "PATH_NOISE_TSK_DUMMY1"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY1:Ljava/lang/String;

    .line 56
    const-string v0, "PATH_NOISE_TSK_DUMMY3"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY3:Ljava/lang/String;

    .line 57
    const-string v0, "PATH_NOISE_TSK_DUMMY4"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY4:Ljava/lang/String;

    .line 58
    const-string v0, "PATH_NOISE_TSK_DUMMY5"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY5:Ljava/lang/String;

    .line 59
    const-string v0, "PATH_NOISE_TSK_DUMMY6"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY6:Ljava/lang/String;

    .line 74
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I

    .line 75
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I

    .line 76
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I

    .line 77
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I

    .line 79
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_MENU:I

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_HOME:I

    .line 81
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_BACK:I

    .line 82
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_RECENT:I

    .line 83
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_1KEY:I

    .line 84
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_2KEY:I

    .line 85
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_3KEY:I

    .line 86
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_4KEY:I

    .line 87
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_5KEY:I

    .line 88
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_6KEY:I

    .line 90
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I

    .line 91
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I

    .line 92
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I

    .line 93
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I

    .line 94
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I

    .line 96
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I

    .line 98
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I

    .line 99
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I

    .line 100
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I

    .line 101
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I

    .line 102
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I

    .line 103
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsThres:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy4key:Z

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy5key:Z

    .line 119
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy6key:Z

    .line 128
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Lcom/sec/android/app/status/touch_key_sensitivity_view;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_BACK:I

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_HOME:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_RECENT:I

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_MENU:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_1KEY:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_3KEY:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy4key:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_4KEY:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy5key:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_5KEY:I

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy6key:Z

    return v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_6KEY:I

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsThres:Z

    return v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I

    return p1
.end method

.method private drawGraph()V
    .locals 7

    .prologue
    .line 282
    new-instance v6, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)V

    .line 305
    .local v6, "handler":Landroid/os/Handler;
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timer:Ljava/util/Timer;

    .line 306
    new-instance v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;

    invoke-direct {v0, p0, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timerTask:Ljava/util/TimerTask;

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x32

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 395
    return-void
.end method

.method private fileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 425
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private fileRead(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "size"    # I

    .prologue
    .line 398
    const-string v0, ""

    .line 399
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 402
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4, p2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    .end local v2    # "in":Ljava/io/BufferedReader;
    .local v3, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 404
    if-nez v0, :cond_0

    .line 405
    const-string v0, "0"
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 412
    :cond_0
    if-eqz v3, :cond_3

    .line 414
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 421
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 415
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 416
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 417
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_0

    .line 407
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 408
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 412
    if-eqz v2, :cond_1

    .line 414
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 415
    :catch_2
    move-exception v1

    .line 416
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 409
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 410
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 412
    if-eqz v2, :cond_1

    .line 414
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 415
    :catch_4
    move-exception v1

    .line 416
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 412
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v2, :cond_2

    .line 414
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 417
    :cond_2
    :goto_4
    throw v4

    .line 415
    :catch_5
    move-exception v1

    .line 416
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 412
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_3

    .line 409
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_2

    .line 407
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :cond_3
    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method public static getMaxData(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 445
    sget-object v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    aget-object v0, v0, p0

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I
    invoke-static {v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$4900(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 12

    .prologue
    const/4 v4, 0x1

    .line 219
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/os/PowerManager;

    .line 220
    .local v10, "pm":Landroid/os/PowerManager;
    const v0, 0x3000001a

    const-string v1, "touch_key_sensitivity_graph"

    invoke-virtual {v10, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 224
    :try_start_0
    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v11

    .line 226
    .local v11, "windowManager":Landroid/view/IWindowManager;
    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v11, v0, v1, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z

    .line 227
    const/16 v0, 0xbb

    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v11, v0, v1, v2}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v11    # "windowManager":Landroid/view/IWindowManager;
    :goto_0
    const v0, 0x7f0a02a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    .line 233
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_back"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileExists(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    .line 234
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_home"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileExists(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    .line 235
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_recent"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileExists(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    .line 236
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_menu"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileExists(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    .line 237
    const-string v0, "/sys/class/sec/sec_touchkey/touchkey_threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileExists(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsThres:Z

    .line 240
    const-string v0, "IS_FOUR_KEY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    .line 241
    const-string v0, "IS_FIVE_KEY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    .line 242
    const-string v0, "IS_SIX_KEY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    .line 244
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    if-eqz v0, :cond_1

    .line 245
    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy6key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy4key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    .line 252
    :cond_0
    :goto_1
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "init"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isFiveKeyModel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isSixKeyModel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    if-eqz v0, :cond_3

    .line 255
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    iget-boolean v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    iget-boolean v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    iget-boolean v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    iget-boolean v7, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy5key:Z

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabledFiveKey(ZZZZZZZ)V

    .line 273
    :goto_2
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sgh-s730g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setReduction(F)V

    .line 279
    :goto_3
    return-void

    .line 228
    :catch_0
    move-exception v9

    .line 229
    .local v9, "e":Ljava/lang/Exception;
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "init"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Home Key Block Exception"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 246
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    if-eqz v0, :cond_2

    .line 247
    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy5key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    goto/16 :goto_1

    .line 248
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    if-eqz v0, :cond_0

    .line 249
    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    goto/16 :goto_1

    .line 259
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    if-eqz v0, :cond_4

    .line 260
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    iget-boolean v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    iget-boolean v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    iget-boolean v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    iget-boolean v7, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy4key:Z

    iget-boolean v8, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy6key:Z

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabledSixKey(ZZZZZZZZ)V

    goto :goto_2

    .line 264
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    if-eqz v0, :cond_5

    .line 265
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    iget-boolean v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    iget-boolean v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z

    iget-boolean v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabledFourKey(ZZZZZZ)V

    goto/16 :goto_2

    .line 270
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z

    iget-boolean v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z

    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z

    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabled(ZZZZ)V

    goto/16 :goto_2

    .line 277
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;

    const-string v1, "TOUCH_KEY_GRAPH_REDUCTION"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setReduction(F)V

    goto/16 :goto_3
.end method

.method private initMinMaxIfon()V
    .locals 4

    .prologue
    .line 438
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 439
    sget-object v1, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    new-instance v2, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;)V

    aput-object v2, v1, v0

    .line 438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 441
    :cond_0
    return-void
.end method

.method private runTSPReferenceStartcmd()V
    .locals 2

    .prologue
    .line 430
    sget-boolean v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->run_cmd_status:Z

    if-nez v0, :cond_0

    const-string v0, "IS_ATMEL_TSP_IC_TOUCHKEY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->run_cmd_status:Z

    .line 432
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    move-result-object v0

    const-string v1, "run_reference_read"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 433
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->run_cmd_status:Z

    .line 435
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 157
    const v0, 0x7f03006b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->setContentView(I)V

    .line 158
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iput-object p0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mContext:Landroid/content/Context;

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->init()V

    .line 161
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->runTSPReferenceStartcmd()V

    .line 162
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->initMinMaxIfon()V

    .line 163
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    const-string v0, "EXTRA_BUTTON_EVENT"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 193
    :cond_1
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "onDestroy"

    const-string v2, "onDestory"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 195
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 212
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 213
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->runTSPReferenceStartcmd()V

    .line 215
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 200
    packed-switch p1, :pswitch_data_0

    .line 206
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 202
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->finish()V

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 177
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 178
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 184
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 169
    const-string v0, "touch_key_sensitivity_graph"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->drawGraph()V

    .line 172
    return-void
.end method
