.class public Lcom/sec/android/app/phoneutil/UsbLogging;
.super Landroid/app/Activity;
.source "UsbLogging.java"


# instance fields
.field private final LOGGING_ENABLE_PATH:Ljava/lang/String;

.field private final LOGGING_SEL_PATH:Ljava/lang/String;

.field private final aponly:[B

.field private final cpap:[B

.field private final cponly:[B

.field private final disable:[B

.field private final enable:[B

.field private mContext:Landroid/content/Context;

.field private mUartRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 198
    const-string v0, "/sys/class/sec/switch/DMport"

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->LOGGING_ENABLE_PATH:Ljava/lang/String;

    .line 199
    const-string v0, "/sys/class/sec/switch/DMlog"

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->LOGGING_SEL_PATH:Ljava/lang/String;

    .line 200
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->enable:[B

    .line 203
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->disable:[B

    .line 206
    new-array v0, v1, [B

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->cponly:[B

    .line 209
    new-array v0, v1, [B

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->aponly:[B

    .line 212
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->cpap:[B

    return-void

    .line 200
    nop

    :array_0
    .array-data 1
        0x45t
        0x4et
        0x41t
        0x42t
        0x4ct
        0x45t
        0x0t
    .end array-data

    .line 203
    :array_1
    .array-data 1
        0x44t
        0x49t
        0x53t
        0x41t
        0x42t
        0x4ct
        0x45t
        0x0t
    .end array-data

    .line 206
    :array_2
    .array-data 1
        0x43t
        0x50t
        0x4ft
        0x4et
        0x4ct
        0x59t
        0x0t
    .end array-data

    .line 209
    :array_3
    .array-data 1
        0x41t
        0x50t
        0x4ft
        0x4et
        0x4ct
        0x59t
        0x0t
    .end array-data

    .line 212
    :array_4
    .array-data 1
        0x43t
        0x50t
        0x41t
        0x50t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/phoneutil/UsbLogging;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/phoneutil/UsbLogging;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/phoneutil/UsbLogging;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/phoneutil/UsbLogging;->loggingenable(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/phoneutil/UsbLogging;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/app/phoneutil/UsbLogging;->loggingstatus(Ljava/lang/String;)V

    return-void
.end method

.method private getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;

    const-string v2, "usblogging.preferences_name"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 195
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, "0"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V
    .locals 5
    .param p1, "func"    # Landroid/widget/RadioGroup;
    .param p2, "type"    # Landroid/widget/RadioGroup;

    .prologue
    const v4, 0x7f0a02b0

    const/4 v3, 0x1

    .line 163
    const-string v2, "persist.enablestatus"

    invoke-direct {p0, v2}, Lcom/sec/android/app/phoneutil/UsbLogging;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "uartString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 166
    const-string v2, "enable"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 167
    const v2, 0x7f0a02ac

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 173
    :cond_0
    :goto_0
    const-string v2, "persist.logselection"

    invoke-direct {p0, v2}, Lcom/sec/android/app/phoneutil/UsbLogging;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "usbString":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 176
    const-string v2, "cp"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_3

    .line 177
    const v2, 0x7f0a02af

    invoke-virtual {p2, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 184
    :cond_1
    :goto_1
    return-void

    .line 168
    .end local v1    # "usbString":Ljava/lang/String;
    :cond_2
    const-string v2, "disable"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_0

    .line 169
    const v2, 0x7f0a02ad

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 178
    .restart local v1    # "usbString":Ljava/lang/String;
    :cond_3
    const-string v2, "ap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_4

    .line 179
    invoke-virtual {p2, v4}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1

    .line 180
    :cond_4
    const-string v2, "cpap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-ne v2, v3, :cond_1

    .line 181
    invoke-virtual {p2, v4}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method

.method private loggingenable(Ljava/lang/String;)V
    .locals 3
    .param p1, "logenable"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 217
    new-instance v1, Ljava/io/FileOutputStream;

    const-string v2, "/sys/class/sec/switch/DMport"

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 220
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v2, "YES"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 221
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->enable:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 230
    :goto_1
    return-void

    .line 222
    :cond_1
    :try_start_1
    const-string v2, "NO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->disable:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 228
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v2
.end method

.method private loggingstatus(Ljava/lang/String;)V
    .locals 3
    .param p1, "sel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 233
    new-instance v1, Ljava/io/FileOutputStream;

    const-string v2, "/sys/class/sec/switch/DMlog"

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 236
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v2, "CPONLY"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->cponly:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 248
    :goto_1
    return-void

    .line 238
    :cond_1
    :try_start_1
    const-string v2, "APONLY"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->aponly:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    goto :goto_1

    .line 240
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_3
    const-string v2, "CPAP"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->cpap:[B

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    iput-object p0, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;

    .line 49
    const v2, 0x7f030071

    invoke-virtual {p0, v2}, Lcom/sec/android/app/phoneutil/UsbLogging;->setContentView(I)V

    .line 50
    const v2, 0x7f0a02ab

    invoke-virtual {p0, v2}, Lcom/sec/android/app/phoneutil/UsbLogging;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUartRadioGroup:Landroid/widget/RadioGroup;

    .line 51
    const v2, 0x7f0a02ae

    invoke-virtual {p0, v2}, Lcom/sec/android/app/phoneutil/UsbLogging;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 53
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;

    const-string v3, "usblogging.preferences_name"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 56
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "Enable"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUartRadioGroup:Landroid/widget/RadioGroup;

    const v3, 0x7f0a02ac

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    .line 68
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUartRadioGroup:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/android/app/phoneutil/UsbLogging$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/phoneutil/UsbLogging$1;-><init>(Lcom/sec/android/app/phoneutil/UsbLogging;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/android/app/phoneutil/UsbLogging$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/phoneutil/UsbLogging$2;-><init>(Lcom/sec/android/app/phoneutil/UsbLogging;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUartRadioGroup:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/phoneutil/UsbLogging;->initialSetting(Landroid/widget/RadioGroup;Landroid/widget/RadioGroup;)V

    .line 160
    return-void

    .line 58
    :cond_1
    const-string v2, "Disable"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v5, :cond_2

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUartRadioGroup:Landroid/widget/RadioGroup;

    const v3, 0x7f0a02ad

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 60
    :cond_2
    const-string v2, "CPONLY"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v5, :cond_3

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    const v3, 0x7f0a02af

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 62
    :cond_3
    const-string v2, "APONLY"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v5, :cond_4

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    const v3, 0x7f0a02b0

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 64
    :cond_4
    const-string v2, "CPAP"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-ne v2, v5, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/phoneutil/UsbLogging;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    const v3, 0x7f0a02b1

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method
