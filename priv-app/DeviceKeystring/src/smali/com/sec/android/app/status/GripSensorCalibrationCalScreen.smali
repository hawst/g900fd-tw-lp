.class public Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;
.super Landroid/app/Activity;
.source "GripSensorCalibrationCalScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mChipName:Ljava/lang/String;

.field private mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x10000000

    .line 62
    const-string v3, "GripSensorCalibrationCalScreen"

    const-string v4, "onClick"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v2, p1

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 65
    :pswitch_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 66
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mChipName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 68
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 70
    const-string v2, "grip_sensor_name"

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mChipName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 65
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 79
    .end local v0    # "i":I
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/status/GripSensorTest2;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v2, "grip_sensor_name"

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mChipName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 82
    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a00b9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    const-string v0, "GripSensorCalibrationCalScreen"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->setContentView(I)V

    .line 31
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "grip_sensor_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mChipName:Ljava/lang/String;

    .line 34
    const v0, 0x7f0a00a2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mChipName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    const v0, 0x7f0a00ba

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    invoke-static {}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->values()[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 40
    return-void

    .line 31
    :cond_0
    const-string v0, "Unknown Sensor"

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 56
    const-string v0, "GripSensorCalibrationCalScreen"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 58
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 50
    const-string v0, "GripSensorCalibrationCalScreen"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 52
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 44
    const-string v0, "GripSensorCalibrationCalScreen"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 46
    return-void
.end method
