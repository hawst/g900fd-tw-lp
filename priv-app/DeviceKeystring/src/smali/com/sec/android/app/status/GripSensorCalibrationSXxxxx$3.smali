.class Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;
.super Landroid/os/Handler;
.source "GripSensorCalibrationSXxxxx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0x31

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 440
    const/4 v3, 0x0

    .line 441
    .local v3, "result":Z
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 443
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->calibration(B)Z
    invoke-static {v4, v6}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;B)Z

    move-result v3

    .line 444
    if-nez v3, :cond_0

    .line 445
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "onClick"

    const-string v6, "gripsensor_cal_fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 449
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    const/16 v5, 0x30

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->calibration(B)Z
    invoke-static {v4, v5}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;B)Z

    move-result v3

    .line 450
    if-nez v3, :cond_0

    .line 451
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "onClick"

    const-string v6, "gripsensor_calerase_fail"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/hardware/SensorManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 456
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    const-string v5, "/sys/class/sensors/grip_sensor/reset"

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$800(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Ljava/lang/String;B)Z

    goto :goto_0

    .line 459
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    const/4 v5, 0x0

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v4, v5}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Z)V

    .line 460
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "MSG_UPDATE_PROGRESS"

    const-string v6, "Doing Cal.."

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v4, v8}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Z)V

    goto :goto_0

    .line 466
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, "caldatas":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/widget/TextView;

    move-result-object v4

    const-string v5, "Waiting"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->toggleButtonClickable(Z)V
    invoke-static {v4, v8}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Z)V

    .line 469
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isCalSuccess()Z
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 470
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "MSG_UPDATE_UI"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isCalSuccess() : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isCalSuccess()Z
    invoke-static {v7}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Y("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 474
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/widget/TextView;

    move-result-object v4

    const-string v5, "N(0, 0)"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 478
    .end local v0    # "caldatas":[Ljava/lang/String;
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isCalSuccess()Z
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z

    move-result v1

    .line 479
    .local v1, "iscalsuccess":Z
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isSpecIn()Z
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z

    move-result v2

    .line 480
    .local v2, "isspecin":Z
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "MSG_UPDATE_RESULT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSpecIn(): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "MSG_UPDATE_RESULT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isCalSuccess(): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x3eb

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 487
    .end local v1    # "iscalsuccess":Z
    .end local v2    # "isspecin":Z
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/hardware/SensorManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$1500(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/hardware/Sensor;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v9}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto/16 :goto_0

    .line 441
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method
