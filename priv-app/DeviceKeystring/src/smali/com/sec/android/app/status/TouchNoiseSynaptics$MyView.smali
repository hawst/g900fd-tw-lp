.class public Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;
.super Landroid/view/View;
.source "TouchNoiseSynaptics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TouchNoiseSynaptics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field public mHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TouchNoiseSynaptics;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 413
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .line 414
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 398
    new-instance v0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView$1;-><init>(Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mHandler:Landroid/os/Handler;

    .line 415
    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v0

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 416
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    # setter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {p1, v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$502(Lcom/sec/android/app/status/TouchNoiseSynaptics;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 417
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    .line 418
    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {p1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 419
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->drawSquare()V

    .line 420
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->drawSquare()V

    return-void
.end method

.method private drawSquare()V
    .locals 15

    .prologue
    const/high16 v14, 0x40400000    # 3.0f

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/high16 v11, 0x40800000    # 4.0f

    const/high16 v10, 0x40000000    # 2.0f

    .line 423
    const/4 v6, 0x0

    .line 424
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 426
    .local v7, "ColY":I
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mbackgroundPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$600(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    mul-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, 0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    mul-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    const/high16 v2, 0x41200000    # 10.0f

    sub-float/2addr v1, v2

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v2, v11

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v4, v11

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v2, v11

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v4, v11

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/lit8 v5, v5, -0x1

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    mul-float/2addr v3, v14

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    add-int/lit8 v5, v5, -0x3

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    sub-float/2addr v2, v10

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v5

    add-int/lit8 v5, v5, -0x3

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v1, v1, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v10

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v4, v4, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 457
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    int-to-float v1, v8

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .line 460
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v0, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    int-to-float v1, v9

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v7

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v5}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 460
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 457
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 468
    .end local v9    # "j":I
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    const-string v1, "%d,%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentX:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentY:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mTouchPoint:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1302(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    const-string v1, "Threshold:%s"

    new-array v2, v13, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdRaw:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1602(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    const-string v1, "Threshold_glove:%s"

    new-array v2, v13, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdGlove:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue_glove:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1802(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;

    .line 472
    const/4 v8, 0x0

    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v8, v0, :cond_2

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "rawcap:%s"

    new-array v2, v13, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v8

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 472
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 476
    :cond_2
    const/4 v8, 0x0

    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-ge v8, v0, :cond_3

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "delta:%s"

    new-array v2, v13, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v3

    aget-object v3, v3, v8

    aput-object v3, v2, v12

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    .line 476
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 481
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mTouchPoint:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    const/high16 v3, 0x41200000    # 10.0f

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    const/high16 v4, 0x41200000    # 10.0f

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1600(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue_glove:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v12

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v3, v11

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v13

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v3, v11

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v12

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v3, v11

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v13

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    mul-float/2addr v3, v11

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v2, v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I
    invoke-static {v3}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget v3, v3, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I

    move-result v4

    add-int/lit8 v4, v4, -0x3

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;
    invoke-static {v4}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 504
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 509
    return-void
.end method
