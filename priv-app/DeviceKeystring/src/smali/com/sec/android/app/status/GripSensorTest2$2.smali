.class Lcom/sec/android/app/status/GripSensorTest2$2;
.super Ljava/lang/Object;
.source "GripSensorTest2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorTest2;->startThreadGettingRawData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorTest2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorTest2;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorTest2$2;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 128
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorTest2$2;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorTest2;->access$300(Lcom/sec/android/app/status/GripSensorTest2;)Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_0

    .line 130
    const-string v6, "GRIP_SENSOR_RAWDATA"

    invoke-static {v6, v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 131
    .local v5, "rawData":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorTest2$2;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorTest2;->access$400(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 132
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 133
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 134
    iput v8, v2, Landroid/os/Message;->what:I

    .line 137
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 138
    .local v3, "nRawData":I
    const-string v6, "RAW_DATA_KEY"

    invoke-virtual {v0, v6, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 139
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorTest2$2;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorTest2;->access$400(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 146
    const-wide/16 v6, 0x64

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v1

    .line 148
    .local v1, "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 152
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "nRawData":I
    .end local v5    # "rawData":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 140
    .restart local v0    # "data":Landroid/os/Bundle;
    .restart local v2    # "msg":Landroid/os/Message;
    .restart local v5    # "rawData":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 141
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method
