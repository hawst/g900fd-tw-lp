.class public Lcom/sec/android/app/status/receiverSpeakerTest;
.super Landroid/app/Activity;
.source "receiverSpeakerTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field public CLASS_NAME:Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private r1000Hz:Landroid/widget/ToggleButton;

.field private r200Hz:Landroid/widget/ToggleButton;

.field private r500Hz:Landroid/widget/ToggleButton;

.field private s1000Hz:Landroid/widget/ToggleButton;

.field private s200Hz:Landroid/widget/ToggleButton;

.field private s500Hz:Landroid/widget/ToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 22
    const-string v0, "receiverSpeakerTest"

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    .line 41
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->AUDIO_PATH:[Ljava/lang/String;

    .line 47
    return-void
.end method

.method private release()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 226
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 228
    :cond_0
    return-void
.end method

.method private startMedia(Landroid/content/res/AssetFileDescriptor;I)V
    .locals 9
    .param p1, "afd"    # Landroid/content/res/AssetFileDescriptor;
    .param p2, "device"    # I

    .prologue
    .line 183
    iget-object v1, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startMedia"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "afd : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", device : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->AUDIO_PATH:[Ljava/lang/String;

    array-length v0, v0

    if-lt v0, p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v0, v0, p2

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0, p2}, Lcom/sec/android/app/status/receiverSpeakerTest;->setAudioPath(I)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->release()V

    .line 186
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    .line 204
    :goto_1
    return-void

    .line 183
    :cond_0
    const-string v0, "Unknown device"

    goto :goto_0

    .line 194
    :catch_0
    move-exception v8

    .line 195
    .local v8, "ise":Ljava/lang/IllegalStateException;
    invoke-static {v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->release()V

    goto :goto_1

    .line 197
    .end local v8    # "ise":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v7

    .line 198
    .local v7, "ie":Ljava/io/IOException;
    invoke-static {v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 199
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->release()V

    goto :goto_1

    .line 200
    .end local v7    # "ie":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 201
    .local v6, "iae":Ljava/lang/IllegalArgumentException;
    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->release()V

    goto :goto_1
.end method

.method private stopMedia()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 219
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->release()V

    .line 221
    :cond_0
    return-void
.end method

.method private updateMedia(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 116
    move-object v4, p1

    check-cast v4, Landroid/widget/ToggleButton;

    .line 117
    .local v4, "toggle":Landroid/widget/ToggleButton;
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->stopMedia()V

    .line 119
    invoke-virtual {v4}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 120
    const/4 v3, 0x0

    .line 121
    .local v3, "srcName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 123
    .local v1, "device":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "updateMedia"

    const-string v7, "Unknown Button clicked"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :goto_0
    if-nez v3, :cond_1

    .line 151
    iget-object v5, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "updateMedia"

    const-string v7, "Unknown media file"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .end local v1    # "device":I
    .end local v3    # "srcName":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 125
    .restart local v1    # "device":I
    .restart local v3    # "srcName":Ljava/lang/String;
    :pswitch_0
    const-string v3, "lf_200hz.wav"

    .line 126
    const/4 v1, 0x1

    .line 127
    goto :goto_0

    .line 129
    :pswitch_1
    const-string v3, "mp3_500hz.wav"

    .line 130
    const/4 v1, 0x1

    .line 131
    goto :goto_0

    .line 133
    :pswitch_2
    const-string v3, "mp3_1khz.wav"

    .line 134
    const/4 v1, 0x1

    .line 135
    goto :goto_0

    .line 138
    :pswitch_3
    const-string v3, "lf_200hz.wav"

    .line 139
    goto :goto_0

    .line 141
    :pswitch_4
    const-string v3, "mp3_500hz.wav"

    .line 142
    goto :goto_0

    .line 144
    :pswitch_5
    const-string v3, "mp3_1khz.wav"

    .line 145
    goto :goto_0

    .line 155
    :cond_1
    const/4 v0, 0x0

    .line 157
    .local v0, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 164
    if-nez v0, :cond_2

    .line 165
    iget-object v5, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "updateMedia"

    const-string v7, "Unknown media file afd=null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 158
    :catch_0
    move-exception v2

    .line 159
    .local v2, "ie":Ljava/io/IOException;
    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 160
    iget-object v5, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "updateMedia"

    const-string v7, "Unknown media file"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 168
    .end local v2    # "ie":Ljava/io/IOException;
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/receiverSpeakerTest;->startMedia(Landroid/content/res/AssetFileDescriptor;I)V

    goto :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x7f0a0174
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateToggle(I)V
    .locals 3
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r200Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0a0174

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r500Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0a0175

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r1000Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0a0176

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r1000Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s200Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0a0177

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s500Hz:Landroid/widget/ToggleButton;

    const v0, 0x7f0a0178

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s1000Hz:Landroid/widget/ToggleButton;

    const v2, 0x7f0a0179

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s1000Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 113
    return-void

    :cond_1
    move v0, v1

    .line 106
    goto :goto_0

    :cond_2
    move v0, v1

    .line 107
    goto :goto_1

    :cond_3
    move v0, v1

    .line 108
    goto :goto_2

    :cond_4
    move v0, v1

    .line 110
    goto :goto_3

    :cond_5
    move v0, v1

    .line 111
    goto :goto_4
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->updateToggle(I)V

    .line 102
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/receiverSpeakerTest;->updateMedia(Landroid/view/View;)V

    .line 103
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v0, 0x7f030057

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->setContentView(I)V

    .line 54
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const-string v0, "Not Support Keystring"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onCreate"

    const-string v2, "Not Support Keystring"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->finish()V

    .line 60
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mAudioManager:Landroid/media/AudioManager;

    .line 61
    const v0, 0x7f0a0174

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r200Hz:Landroid/widget/ToggleButton;

    .line 62
    const v0, 0x7f0a0175

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r500Hz:Landroid/widget/ToggleButton;

    .line 63
    const v0, 0x7f0a0176

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r1000Hz:Landroid/widget/ToggleButton;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->r1000Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const v0, 0x7f0a0177

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s200Hz:Landroid/widget/ToggleButton;

    .line 69
    const v0, 0x7f0a0178

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s500Hz:Landroid/widget/ToggleButton;

    .line 70
    const v0, 0x7f0a0179

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s1000Hz:Landroid/widget/ToggleButton;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s200Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s500Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->s1000Hz:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 86
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/status/receiverSpeakerTest;->stopMedia()V

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onPause"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/receiverSpeakerTest;->setAudioPath(I)V

    .line 97
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/receiverSpeakerTest;->setAudioPath(I)V

    .line 82
    return-void
.end method

.method public setAudioPath(I)V
    .locals 4
    .param p1, "path"    # I

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setMode"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioPath : factory_test_route="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_route="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/receiverSpeakerTest;->AUDIO_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 233
    return-void
.end method
