.class public Lcom/sec/android/app/status/LoopbackTestDual;
.super Landroid/app/Activity;
.source "LoopbackTestDual.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field final LOOPBACK_PATH:[Ljava/lang/String;

.field final LOOPBACK_PATH_EAR_EAR:I

.field final LOOPBACK_PATH_MIC2_SPK:I

.field final LOOPBACK_PATH_MIC3_SPK:I

.field final LOOPBACK_PATH_MIC_EAR:I

.field final LOOPBACK_PATH_MIC_PIEZO_RCV:I

.field final LOOPBACK_PATH_MIC_RCV:I

.field final LOOPBACK_PATH_MIC_SPK:I

.field final LOOPBACK_PATH_NONE:I

.field final LOOPBACK_TYPE:[Ljava/lang/String;

.field final LOOPBACK_TYPE_CODEC:I

.field final LOOPBACK_TYPE_PACKET:I

.field final LOOPBACK_TYPE_PCM:I

.field final LOOPBACK_TYPE_REALTIME:I

.field private _am:Landroid/media/AudioManager;

.field private mIsDoingLoopback:Z

.field product:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    const-string v0, "LoopbackTestDual"

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->CLASS_NAME:Ljava/lang/String;

    .line 27
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->product:Ljava/lang/String;

    .line 30
    iput-boolean v3, p0, Lcom/sec/android/app/status/LoopbackTestDual;->mIsDoingLoopback:Z

    .line 153
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_NONE:I

    .line 154
    iput v3, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC_RCV:I

    .line 155
    iput v4, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC_SPK:I

    .line 156
    iput v5, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC3_SPK:I

    .line 157
    iput v6, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC_EAR:I

    .line 158
    iput v7, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC2_SPK:I

    .line 159
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_EAR_EAR:I

    .line 160
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH_MIC_PIEZO_RCV:I

    .line 161
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mic_rcv;"

    aput-object v1, v0, v3

    const-string v1, "mic_spk;"

    aput-object v1, v0, v4

    const-string v1, "mic3_spk;"

    aput-object v1, v0, v5

    const-string v1, "mic_ear;"

    aput-object v1, v0, v6

    const-string v1, "mic2_spk;"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ear_ear;"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mic_rcv;"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH:[Ljava/lang/String;

    .line 165
    iput v3, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE_PACKET:I

    .line 166
    iput v4, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE_PCM:I

    .line 167
    iput v5, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE_REALTIME:I

    .line 168
    iput v6, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE_CODEC:I

    .line 169
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "packet;"

    aput-object v1, v0, v3

    const-string v1, "pcm;"

    aput-object v1, v0, v4

    const-string v1, "realtime"

    aput-object v1, v0, v5

    const-string v1, "codec"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isVoiceCapable()Z
    .locals 6

    .prologue
    .line 231
    const/4 v1, 0x1

    .line 232
    .local v1, "r":Z
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 234
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 235
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1120044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 239
    :cond_0
    const-string v2, "LoopbackTestDual"

    const-string v3, "isVoiceCapable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isVoiceCapable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    return v1
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 7
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 94
    const/4 v0, -0x1

    .line 96
    .local v0, "path":I
    sparse-switch p2, :sswitch_data_0

    .line 116
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 117
    iget-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTestDual;->mIsDoingLoopback:Z

    if-nez v1, :cond_1

    .line 118
    const-string v1, "LoopbackTestDual"

    const-string v2, "onCheckedChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "product = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTestDual;->product:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->setPiezoRCV(I)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTestDual;->startLoopback(II)V

    .line 137
    :goto_1
    return-void

    .line 98
    :sswitch_0
    const/4 v0, 0x0

    .line 99
    goto :goto_0

    .line 101
    :sswitch_1
    const/4 v0, 0x6

    .line 102
    goto :goto_0

    .line 104
    :sswitch_2
    const/4 v0, 0x1

    .line 105
    goto :goto_0

    .line 107
    :sswitch_3
    const/4 v0, 0x5

    .line 108
    goto :goto_0

    .line 110
    :sswitch_4
    const/4 v0, -0x1

    .line 111
    goto :goto_0

    .line 124
    :cond_0
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTestDual;->startLoopback(II)V

    goto :goto_1

    .line 127
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->setPiezoRCV(I)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 129
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTestDual;->setLoopbackPath(II)V

    goto :goto_1

    .line 131
    :cond_2
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTestDual;->setLoopbackPath(II)V

    goto :goto_1

    .line 135
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->stopLoopback()V

    goto :goto_1

    .line 96
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0110 -> :sswitch_0
        0x7f0a0111 -> :sswitch_2
        0x7f0a0112 -> :sswitch_3
        0x7f0a0113 -> :sswitch_4
        0x7f0a0120 -> :sswitch_1
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 143
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->finish()V

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x7f0a00a4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    const-string v0, "LoopbackTestDual"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->setContentView(I)V

    .line 37
    const v0, 0x7f0a010f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 38
    const v0, 0x7f0a00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    .line 40
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 64
    const-string v0, "LoopbackTestDual"

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    packed-switch p1, :pswitch_data_0

    .line 74
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 68
    :pswitch_0
    const-string v0, "LoopbackTestDual"

    const-string v1, "ignore"

    const-string v2, " KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 79
    const-string v0, "UIBase"

    const-string v1, "onKeyUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    packed-switch p1, :pswitch_data_0

    .line 89
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 83
    :pswitch_0
    const-string v0, "LoopbackTestDual"

    const-string v1, "ignore"

    const-string v2, "KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const/4 v0, 0x1

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 57
    const-string v0, "LoopbackTestDual"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 59
    const v0, 0x7f0a0113

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 60
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 44
    const-string v1, "LoopbackTestDual"

    const-string v2, "onResume"

    const-string v3, "onResume"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 46
    const v1, 0x7f0a0113

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    const v1, 0x7f0a0110

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTestDual;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 50
    .local v0, "RcvON_RadioButton":Landroid/widget/RadioButton;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 51
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    .line 53
    .end local v0    # "RcvON_RadioButton":Landroid/widget/RadioButton;
    :cond_0
    return-void
.end method

.method public setLoopbackPath(II)V
    .locals 4
    .param p1, "path"    # I
    .param p2, "type"    # I

    .prologue
    .line 220
    const-string v0, "LoopbackTestDual"

    const-string v1, "setLoopbackPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLoopbackPath path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->setStreamMusicVolumeMax()V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public setPiezoRCV(I)V
    .locals 3
    .param p1, "path"    # I

    .prologue
    .line 174
    const-string v0, "LoopbackTestDual"

    const-string v1, "setPiezoRCV"

    const-string v2, "setPiezoRCV"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    const-string v1, "piezoHandset=true"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    const-string v1, "piezoHandset=false"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStreamMusicVolumeMax()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 228
    return-void
.end method

.method public startLoopback(II)V
    .locals 4
    .param p1, "path"    # I
    .param p2, "type"    # I

    .prologue
    .line 183
    const-string v0, "LoopbackTestDual"

    const-string v1, "startLoopback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startLoopback path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->setStreamMusicVolumeMax()V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_PATH:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTestDual;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/LoopbackTestDual$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/LoopbackTestDual$1;-><init>(Lcom/sec/android/app/status/LoopbackTestDual;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 201
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/LoopbackTestDual;->mIsDoingLoopback:Z

    .line 202
    return-void
.end method

.method public stopLoopback()V
    .locals 4

    .prologue
    .line 205
    const-string v1, "LoopbackTestDual"

    const-string v2, "stopLoopback"

    const-string v3, "stopLoopback"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    const-string v2, "piezoHandset=false"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTestDual;->_am:Landroid/media/AudioManager;

    const-string v2, "factory_test_loopback=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 210
    const-string v1, "LoopbackTestDual"

    const-string v2, "stopLoopback"

    const-string v3, "send moduleAudioService stop intent"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 212
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTestDual;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/status/ModuleAudioService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 213
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTestDual;->stopService(Landroid/content/Intent;)Z

    .line 216
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTestDual;->mIsDoingLoopback:Z

    .line 217
    return-void
.end method
