.class Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;
.super Ljava/lang/Object;
.source "touch_key_sensitivity_default.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/touch_key_sensitivity_default;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MinMax"
.end annotation


# instance fields
.field private mData:I

.field private mIsFirstData:Z

.field private mMax:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    .line 221
    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I

    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mIsFirstData:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/touch_key_sensitivity_default$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default$1;

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;-><init>()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    .prologue
    .line 218
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    return v0
.end method

.method private setData(Ljava/lang/String;)V
    .locals 3
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 226
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mIsFirstData:Z

    if-eqz v1, :cond_1

    .line 235
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I

    .line 236
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mIsFirstData:Z

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 229
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    goto :goto_0

    .line 241
    .end local v0    # "ne":Ljava/lang/NumberFormatException;
    :cond_1
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    if-ge v1, v2, :cond_0

    .line 242
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I

    goto :goto_0
.end method
