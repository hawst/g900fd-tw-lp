.class public Lcom/sec/android/app/status/TspResult;
.super Landroid/app/Activity;
.source "TspResult.java"


# instance fields
.field private CMD_NODE:Ljava/lang/String;

.field private RESULT_NODE:Ljava/lang/String;

.field private STATUS_NODE:Ljava/lang/String;

.field private TSP_STATUS_WATING_TIME_OUT:J

.field private final TSP_STATUS__COMMON__FAIL:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__NOT_APPLICABLE:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__OK:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__RUNNING:Ljava/lang/String;

.field private final TSP_STATUS__COMMON__WAITING:Ljava/lang/String;

.field private mTxtTspTesult:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 20
    const-string v0, "WAITING"

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS__COMMON__WAITING:Ljava/lang/String;

    .line 21
    const-string v0, "RUNNING"

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS__COMMON__RUNNING:Ljava/lang/String;

    .line 22
    const-string v0, "OK"

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS__COMMON__OK:Ljava/lang/String;

    .line 23
    const-string v0, "FAIL"

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS__COMMON__FAIL:Ljava/lang/String;

    .line 24
    const-string v0, "NOT_APPLICABLE"

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS__COMMON__NOT_APPLICABLE:Ljava/lang/String;

    .line 27
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS_WATING_TIME_OUT:J

    return-void
.end method

.method private getResult()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    const-string v2, "TSP_COMMAND_CMD"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TspResult;->CMD_NODE:Ljava/lang/String;

    .line 128
    const-string v2, "TSP_COMMAND_STATUS"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TspResult;->STATUS_NODE:Ljava/lang/String;

    .line 129
    const-string v2, "TSP_COMMAND_RESULT"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TspResult;->RESULT_NODE:Ljava/lang/String;

    .line 131
    const-string v2, "get_tsp_test_result"

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspResult;->setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 132
    .local v1, "status":Ljava/lang/String;
    const-string v2, "get_tsp_test_result"

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspResult;->getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "result":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/status/TspResult;->mTxtTspTesult:Landroid/widget/TextView;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    const-string v2, "PASS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/sec/android/app/status/TspResult;->mTxtTspTesult:Landroid/widget/TextView;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 140
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/status/TspResult;->mTxtTspTesult:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    :goto_0
    return-void

    .line 142
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/TspResult;->mTxtTspTesult:Landroid/widget/TextView;

    const-string v3, "Error"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getTSPCommandResult(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    .line 101
    if-eqz p2, :cond_0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 105
    :cond_0
    const-string v1, "TspTesult"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "2. get Result => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/status/TspResult;->RESULT_NODE:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "result":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 110
    const-string v1, "TspTesult"

    const-string v2, "getTSPCommandResult"

    const-string v3, "Fail - Result Read =>1 result == null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v1, "NG"

    .line 120
    :goto_0
    return-object v1

    .line 113
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 114
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 115
    const-string v1, "TspTesult"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 116
    goto :goto_0

    .line 118
    :cond_2
    const-string v1, "TspTesult"

    const-string v2, "getTSPCommandResult"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(Write Command) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != (Result Command) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v1, "NG"

    goto :goto_0
.end method

.method private setTSPCommand(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "subCommand"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 36
    const-string v0, "setTSPCommand"

    .line 37
    .local v0, "METHOD_NAME":Ljava/lang/String;
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "command => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "subCommand => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    if-eqz p2, :cond_0

    .line 41
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 44
    :cond_0
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1. set Command => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    .line 46
    .local v1, "cmd":[B
    const-string v2, ""

    .line 50
    .local v2, "status":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/status/TspResult;->CMD_NODE:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;[B)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 52
    iget-object v3, p0, Lcom/sec/android/app/status/TspResult;->STATUS_NODE:Ljava/lang/String;

    invoke-static {v3, v10}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 54
    if-nez v2, :cond_1

    .line 55
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "status == null"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v3, "FAIL"

    .line 95
    :goto_0
    return-object v3

    .line 57
    :cond_1
    const-string v3, "FAIL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 58
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "status == fail"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v3, "FAIL"

    goto :goto_0

    .line 60
    :cond_2
    const-string v3, "NOT_APPLICABLE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 61
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "status == not applicable"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v3, "NOT_APPLICABLE"

    goto :goto_0

    .line 63
    :cond_3
    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 64
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "status == ok"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v3, "OK"

    goto :goto_0

    .line 66
    :cond_4
    const-string v3, "RUNNING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 67
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "status == running"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v3, "TspTesult"

    const-string v6, "checkTSPStatus"

    const/4 v7, 0x0

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 72
    .local v4, "timeStart":J
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/status/TspResult;->STATUS_NODE:Ljava/lang/String;

    invoke-static {v3, v10}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_6

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 75
    const-string v3, "TspTesult"

    const-string v6, "checkTSPStatus"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "finish => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move-object v3, v2

    .line 88
    goto/16 :goto_0

    .line 80
    :cond_6
    iget-wide v6, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS_WATING_TIME_OUT:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    cmp-long v3, v6, v8

    if-gez v3, :cond_5

    .line 81
    const-string v3, "TspTesult"

    const-string v6, "checkTSPStatus"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "finish => Time Out("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/android/app/status/TspResult;->TSP_STATUS_WATING_TIME_OUT:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v2, "FAIL"

    .line 84
    goto :goto_1

    .line 90
    .end local v4    # "timeStart":J
    :cond_7
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "status == Unknown : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v3, "FAIL"

    goto/16 :goto_0

    .line 94
    :cond_8
    const-string v3, "TspTesult"

    const-string v6, "setTSPCommand"

    const-string v7, "Fail - Command Write"

    invoke-static {v3, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v3, "FAIL"

    goto/16 :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 151
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 155
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 156
    const v0, 0x7f030070

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspResult;->setContentView(I)V

    .line 159
    const v0, 0x7f0a02a9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspResult;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/TspResult;->mTxtTspTesult:Landroid/widget/TextView;

    .line 160
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 169
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 164
    invoke-direct {p0}, Lcom/sec/android/app/status/TspResult;->getResult()V

    .line 165
    return-void
.end method
