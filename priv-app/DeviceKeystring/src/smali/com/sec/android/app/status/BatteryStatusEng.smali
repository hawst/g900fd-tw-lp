.class public Lcom/sec/android/app/status/BatteryStatusEng;
.super Landroid/app/Activity;
.source "BatteryStatusEng.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

.field public static mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

.field private static needTempToADC:Z


# instance fields
.field public mHandler:Landroid/os/Handler;

.field private mHelpView:Landroid/widget/TextView;

.field private mModelCommunicationMode:Ljava/lang/String;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mTextView:Landroid/widget/TextView;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private phone:Lcom/android/internal/telephony/Phone;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    sput-object v1, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/BatteryStatusEng;->needTempToADC:Z

    .line 56
    sput-object v1, Lcom/sec/android/app/status/BatteryStatusEng;->mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->phone:Lcom/android/internal/telephony/Phone;

    .line 57
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mModelCommunicationMode:Ljava/lang/String;

    .line 395
    new-instance v0, Lcom/sec/android/app/status/BatteryStatusEng$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/BatteryStatusEng$4;-><init>(Lcom/sec/android/app/status/BatteryStatusEng;)V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/BatteryStatusEng;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/BatteryStatusEng;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->doQuickStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/BatteryStatusEng;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/BatteryStatusEng;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->lcdOn_sleeptest()V

    return-void
.end method

.method private doQuickStart()V
    .locals 6

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->lcdOff_sleeptest()V

    .line 211
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    sget-object v2, Lcom/sec/android/app/status/BatteryStatusEng;->mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    invoke-virtual {v2}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->resetFuelGaugeIC()Z

    .line 214
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 215
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 216
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 217
    .local v1, "timer":Ljava/util/Timer;
    new-instance v2, Lcom/sec/android/app/status/BatteryStatusEng$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/BatteryStatusEng$3;-><init>(Lcom/sec/android/app/status/BatteryStatusEng;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 222
    return-void

    .line 212
    .end local v0    # "msg":Landroid/os/Message;
    .end local v1    # "timer":Ljava/util/Timer;
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private fixedTemp(I)Ljava/lang/String;
    .locals 4
    .param p1, "temp"    # I

    .prologue
    .line 181
    div-int/lit8 v0, p1, 0xa

    .line 182
    .local v0, "tens":I
    if-gez p1, :cond_0

    const/16 v1, -0xa

    if-ge v1, p1, :cond_0

    .line 183
    new-instance v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0xa

    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 185
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0xa

    sub-int v3, p1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getBlock(I)I
    .locals 2
    .param p1, "batt_level"    # I

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 351
    .local v0, "block":I
    if-ltz p1, :cond_1

    const/4 v1, 0x4

    if-gt p1, v1, :cond_1

    .line 352
    const/4 v0, 0x0

    .line 371
    :cond_0
    :goto_0
    return v0

    .line 353
    :cond_1
    const/4 v1, 0x5

    if-lt p1, v1, :cond_2

    const/16 v1, 0xa

    if-gt p1, v1, :cond_2

    .line 354
    const/4 v0, 0x1

    goto :goto_0

    .line 355
    :cond_2
    const/16 v1, 0xb

    if-lt p1, v1, :cond_3

    const/16 v1, 0x14

    if-gt p1, v1, :cond_3

    .line 356
    const/4 v0, 0x2

    goto :goto_0

    .line 357
    :cond_3
    const/16 v1, 0x15

    if-lt p1, v1, :cond_4

    const/16 v1, 0x22

    if-gt p1, v1, :cond_4

    .line 358
    const/4 v0, 0x3

    goto :goto_0

    .line 359
    :cond_4
    const/16 v1, 0x23

    if-lt p1, v1, :cond_5

    const/16 v1, 0x31

    if-gt p1, v1, :cond_5

    .line 360
    const/4 v0, 0x4

    goto :goto_0

    .line 361
    :cond_5
    const/16 v1, 0x32

    if-lt p1, v1, :cond_6

    const/16 v1, 0x40

    if-gt p1, v1, :cond_6

    .line 362
    const/4 v0, 0x5

    goto :goto_0

    .line 363
    :cond_6
    const/16 v1, 0x41

    if-lt p1, v1, :cond_7

    const/16 v1, 0x4f

    if-gt p1, v1, :cond_7

    .line 364
    const/4 v0, 0x6

    goto :goto_0

    .line 365
    :cond_7
    const/16 v1, 0x50

    if-lt p1, v1, :cond_8

    const/16 v1, 0x5e

    if-gt p1, v1, :cond_8

    .line 366
    const/4 v0, 0x7

    goto :goto_0

    .line 367
    :cond_8
    const/16 v1, 0x5f

    if-lt p1, v1, :cond_0

    const/16 v1, 0x64

    if-gt p1, v1, :cond_0

    .line 368
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private lcdOff_sleeptest()V
    .locals 4

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->wakeLockacquire()V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    .line 256
    return-void
.end method

.method private lcdOn_sleeptest()V
    .locals 4

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->wakeLockRelease()V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mPowerManager:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->wakeUp(J)V

    .line 250
    return-void
.end method

.method private setInitFeature()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 375
    const/4 v3, 0x6

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "MSM8960"

    aput-object v4, v2, v3

    const-string v3, "MSM8260A"

    aput-object v3, v2, v5

    const/4 v3, 0x2

    const-string v4, "MSM8930"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "MSM8226"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "MSM8926"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "MSM8626"

    aput-object v4, v2, v3

    .line 378
    .local v2, "solutionArray":[Ljava/lang/String;
    const-string v3, "ro.chipname"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 380
    .local v1, "solution":Ljava/lang/String;
    const-string v3, "NONE"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 381
    const-string v3, "ro.product.board"

    const-string v4, "NONE"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 384
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 385
    aget-object v3, v2, v0

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v5, :cond_1

    .line 386
    sput-boolean v5, Lcom/sec/android/app/status/BatteryStatusEng;->needTempToADC:Z

    .line 387
    const-string v3, "BatteryStatus"

    const-string v4, "setInitFeature"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "needTempToADC="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/sec/android/app/status/BatteryStatusEng;->needTempToADC:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :goto_1
    return-void

    .line 384
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 392
    :cond_2
    const-string v3, "BatteryStatus"

    const-string v4, "setInitFeature"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "needTempToADC="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/sec/android/app/status/BatteryStatusEng;->needTempToADC:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private wakeLockRelease()V
    .locals 3

    .prologue
    .line 225
    sget-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 226
    sget-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    sget-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 228
    const-string v0, "BatteryStatus"

    const-string v1, "wakeLockRelease"

    const-string v2, "mLcdCTRLWakeLock is released"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 234
    :cond_1
    const-string v0, "BatteryStatus"

    const-string v1, "wakeLockRelease"

    const-string v2, "lcdOn() : setScreenState set to true "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method private wakeLockacquire()V
    .locals 3

    .prologue
    .line 238
    sget-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mPowerManager:Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "LCDCTRL"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 240
    sget-object v0, Lcom/sec/android/app/status/BatteryStatusEng;->mLcdCTRLWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 241
    const-string v0, "BatteryStatus"

    const-string v1, "wakeLockacquire"

    const-string v2, "lcdOff() : mLcdCTRLWakeLock is acquired "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    const-string v0, "BatteryStatus"

    const-string v1, "wakeLockacquire"

    const-string v2, "lcdOff() : setScreenState set to false "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 189
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Warning"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Fuelgauge will be reset, causing battery capacity to be inaccurate.\nDo NOT use it unless it is written in your test procedure."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "OK"

    new-instance v2, Lcom/sec/android/app/status/BatteryStatusEng$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/BatteryStatusEng$2;-><init>(Lcom/sec/android/app/status/BatteryStatusEng;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Cancel"

    new-instance v2, Lcom/sec/android/app/status/BatteryStatusEng$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/BatteryStatusEng$1;-><init>(Lcom/sec/android/app/status/BatteryStatusEng;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 206
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 63
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 66
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 68
    :cond_0
    const-string v2, "MODEL_COMMUNICATION_MODE"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mModelCommunicationMode:Ljava/lang/String;

    .line 70
    const-string v0, ""

    .line 72
    .local v0, "help_str":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/status/BatteryStatusEng;->setInitFeature()V

    .line 73
    const v2, 0x7f030008

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatusEng;->setContentView(I)V

    .line 74
    const v2, 0x7f0a0022

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatusEng;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mTextView:Landroid/widget/TextView;

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    const v2, 0x7f0a0023

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatusEng;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHelpView:Landroid/widget/TextView;

    .line 77
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHelpView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHelpView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    const v2, 0x7f0a0024

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatusEng;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 80
    .local v1, "quickstartbutton":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const-string v2, "none"

    iget-object v3, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mModelCommunicationMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    const-string v2, "BatteryStatus"

    const-string v3, "PhoneFactory.getDefaultPhone()"

    const-string v4, "N/A"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_0
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatusEng;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mPowerManager:Landroid/os/PowerManager;

    .line 87
    sget-object v2, Lcom/sec/android/app/status/BatteryStatusEng;->mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    if-nez v2, :cond_2

    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModulePower;

    move-result-object v2

    :goto_1
    sput-object v2, Lcom/sec/android/app/status/BatteryStatusEng;->mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mPowerManager:Landroid/os/PowerManager;

    const/4 v3, 0x6

    const-string v4, "BatteryStatus"

    invoke-virtual {v2, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 89
    return-void

    .line 84
    :cond_1
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/BatteryStatusEng;->phone:Lcom/android/internal/telephony/Phone;

    goto :goto_0

    .line 87
    :cond_2
    sget-object v2, Lcom/sec/android/app/status/BatteryStatusEng;->mModulePower:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 116
    .line 126
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 97
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 99
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatusEng;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 111
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 112
    return-void
.end method

.method updateBatteryStatus()V
    .locals 26

    .prologue
    .line 259
    const-string v7, ""

    .line 276
    .local v7, "status":Ljava/lang/String;
    const-string v20, "BATTERY_CAPACITY"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v3

    .line 277
    .local v3, "batt_level":I
    const-string v20, "BATTERY_VOLT"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v15

    .line 278
    .local v15, "volt":I
    int-to-float v0, v15

    move/from16 v20, v0

    const v21, 0x3a83126f    # 0.001f

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v15, v0

    .line 279
    const-string v20, "BATTERY_VOLT_ADC"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v16

    .line 280
    .local v16, "volt_adc":I
    const-string v20, "BATTERY_VOLT_ADC_CAL"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v18

    .line 281
    .local v18, "volt_adc_cal":I
    const-string v20, "BATTERY_VOLT_AVERAGE"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v19

    .line 282
    .local v19, "volt_aver":I
    const-string v20, "BATTERY_VOLT_ADC_AVERAGE"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v17

    .line 283
    .local v17, "volt_adc_aver":I
    const-string v20, "BATTERY_TEMP"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v8

    .line 284
    .local v8, "temp":I
    const-string v20, "BATTERY_TEMP_ADC"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v9

    .line 285
    .local v9, "temp_adc":I
    const-string v20, "BATTERY_TEMP_ADC_CAL"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v11

    .line 286
    .local v11, "temp_adc_cal":I
    const-string v20, "BATTERY_TEMP_AVERAGE"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v12

    .line 287
    .local v12, "temp_aver":I
    const-string v20, "BATTERY_TEMP_ADC_AVERAGE"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v10

    .line 288
    .local v10, "temp_adc_aver":I
    const-string v20, "BATTERY_TEMP_VF_ADC"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v13

    .line 289
    .local v13, "temp_vf_adc":I
    const-string v20, "BATTERY_TEMP_VF_OCV"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v14

    .line 292
    .local v14, "temp_vf_ocv":I
    const-string v20, "SEC_EXT_THERMISTER_ADC"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v5

    .line 293
    .local v5, "external_thermistor_adc":I
    const-string v20, "SEC_EXT_THERMISTER_TEMP"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v6

    .line 294
    .local v6, "external_thermistor_temp":I
    const-string v20, "AP_GROUP"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 295
    .local v2, "ap_group":Ljava/lang/String;
    const-string v20, "CHG_CURRENT_ADC"

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readVal(Ljava/lang/String;)I

    move-result v4

    .line 296
    .local v4, "chg_current_adc":I
    const-string v20, "none"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/BatteryStatusEng;->mModelCommunicationMode:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 297
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Network: N/A\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 298
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " RSSI: N/A\n\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 308
    :goto_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Current Value: \n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 309
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Voltage:  %.1f (mV)\n"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    int-to-double v0, v15

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 310
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 313
    sget-boolean v20, Lcom/sec/android/app/status/BatteryStatusEng;->needTempToADC:Z

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 314
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Temperature: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " \n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 319
    :goto_1
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 320
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     VF ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 321
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     VF OCV:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 323
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Level: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (%)\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 324
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Level Block: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/status/BatteryStatusEng;->getBlock(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " blocks (0~8) \n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 326
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Current Chg ADC:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 327
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Average Value: \n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 328
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Voltage:  %.1f (mV)\n"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v19

    int-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 329
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 330
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     cal.: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 332
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Temperature: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/status/BatteryStatusEng;->fixedTemp(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (\'C)\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 333
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 334
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     cal.: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 335
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *External Thermistor \n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 336
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     ADC:  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 337
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "     Temperature: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    div-int/lit8 v21, v6, 0xa

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (\'C)\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 339
    if-eqz v2, :cond_0

    .line 340
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *AG: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 343
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/BatteryStatusEng;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/BatteryStatusEng;->mTextView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    return-void

    .line 300
    :cond_1
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " Network: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "gsm.network.type"

    invoke-static/range {v21 .. v21}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 301
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " RSSI: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/BatteryStatusEng;->phone:Lcom/android/internal/telephony/Phone;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, Lcom/android/internal/telephony/Phone;->getSignalStrength()Landroid/telephony/SignalStrength;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "\n\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 316
    :cond_2
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " *Temperature: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/status/BatteryStatusEng;->fixedTemp(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " (\'C)\n"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1
.end method
