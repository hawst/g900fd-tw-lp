.class interface abstract Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "KeyListener"
.end annotation


# virtual methods
.method public abstract addView(Landroid/widget/FrameLayout;)V
.end method

.method public abstract getStatus()Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
.end method

.method public abstract getType()Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
.end method

.method public abstract onKey(ILandroid/view/KeyEvent;)V
.end method

.method public abstract onReset()V
.end method

.method public abstract release()V
.end method

.method public abstract setParam(I)V
.end method

.method public abstract setPosition(II)V
.end method

.method public abstract setSize(II)V
.end method

.method public abstract setTextSize(F)V
.end method
