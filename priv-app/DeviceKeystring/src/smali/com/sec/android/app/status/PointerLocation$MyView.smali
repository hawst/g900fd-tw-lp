.class public Lcom/sec/android/app/status/PointerLocation$MyView;
.super Landroid/view/View;
.source "PointerLocation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/PointerLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyView"
.end annotation


# instance fields
.field private mCurDown:Z

.field private mCurPressure:F

.field private mCurSize:F

.field private mCurWidth:I

.field private mCurX:I

.field private mCurY:I

.field private mHeaderBottom:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTargetPaint:Landroid/graphics/Paint;

.field private final mTextBackgroundPaint:Landroid/graphics/Paint;

.field private final mTextLevelPaint:Landroid/graphics/Paint;

.field private final mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

.field private final mTextPaint:Landroid/graphics/Paint;

.field private mVelocity:Landroid/view/VelocityTracker;

.field private final mXs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final mYs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    const/16 v5, 0xc0

    const/4 v4, 0x1

    const/16 v3, 0xff

    const/4 v2, 0x0

    .line 69
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mYs:Ljava/util/ArrayList;

    .line 70
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 74
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    const/16 v1, 0x80

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 77
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextLevelPaint:Landroid/graphics/Paint;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextLevelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextLevelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v3, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 85
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5, v2, v2, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 90
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 104
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->getWidth()I

    move-result v1

    div-int/lit8 v18, v1, 0x5

    .line 105
    .local v18, "w":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    neg-int v1, v1

    add-int/lit8 v13, v1, 0x1

    .line 106
    .local v13, "base":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mHeaderBottom:I

    .line 107
    .local v14, "bottom":I
    const/4 v2, 0x0

    const/4 v3, 0x0

    add-int/lit8 v1, v18, -0x1

    int-to-float v4, v1

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "X: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v6, 0x3f800000    # 1.0f

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 109
    move/from16 v0, v18

    int-to-float v2, v0

    const/4 v3, 0x0

    mul-int/lit8 v1, v18, 0x2

    add-int/lit8 v1, v1, -0x1

    int-to-float v4, v1

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Y: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v6, v18, 0x1

    int-to-float v6, v6

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 111
    mul-int/lit8 v1, v18, 0x2

    int-to-float v2, v1

    const/4 v3, 0x0

    mul-int/lit8 v1, v18, 0x3

    add-int/lit8 v1, v1, -0x1

    int-to-float v4, v1

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 112
    mul-int/lit8 v1, v18, 0x2

    int-to-float v2, v1

    const/4 v3, 0x0

    mul-int/lit8 v1, v18, 0x2

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurPressure:F

    move/from16 v0, v18

    int-to-float v7, v0

    mul-float/2addr v6, v7

    add-float/2addr v1, v6

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v4, v1, v6

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextLevelPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 113
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Pres: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurPressure:F

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    mul-int/lit8 v6, v18, 0x2

    add-int/lit8 v6, v6, 0x1

    int-to-float v6, v6

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 114
    mul-int/lit8 v1, v18, 0x3

    int-to-float v2, v1

    const/4 v3, 0x0

    mul-int/lit8 v1, v18, 0x4

    add-int/lit8 v1, v1, -0x1

    int-to-float v4, v1

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 115
    mul-int/lit8 v1, v18, 0x3

    int-to-float v2, v1

    const/4 v3, 0x0

    mul-int/lit8 v1, v18, 0x3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurSize:F

    move/from16 v0, v18

    int-to-float v7, v0

    mul-float/2addr v6, v7

    add-float/2addr v1, v6

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v4, v1, v6

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextLevelPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Size: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurSize:F

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    mul-int/lit8 v6, v18, 0x3

    add-int/lit8 v6, v6, 0x1

    int-to-float v6, v6

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 117
    mul-int/lit8 v1, v18, 0x4

    int-to-float v2, v1

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->getWidth()I

    move-result v1

    int-to-float v4, v1

    int-to-float v5, v14

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 118
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    if-nez v1, :cond_1

    const/16 v17, 0x0

    .line 119
    .local v17, "velocity":I
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "yVel: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    mul-int/lit8 v6, v18, 0x4

    add-int/lit8 v6, v6, 0x1

    int-to-float v6, v6

    int-to-float v7, v13

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 120
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 121
    .local v12, "N":I
    const/4 v2, 0x0

    .local v2, "lastX":F
    const/4 v3, 0x0

    .line 122
    .local v3, "lastY":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    const/16 v6, 0xff

    const/4 v7, 0x0

    const/16 v8, 0xff

    const/16 v9, 0xff

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 124
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    if-ge v15, v12, :cond_2

    .line 125
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 126
    .local v4, "x":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mYs:Ljava/util/ArrayList;

    invoke-virtual {v1, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 128
    .local v5, "y":F
    if-lez v15, :cond_0

    .line 129
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 130
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 133
    :cond_0
    move v2, v4

    .line 134
    move v3, v5

    .line 124
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 118
    .end local v2    # "lastX":F
    .end local v3    # "lastY":F
    .end local v4    # "x":F
    .end local v5    # "y":F
    .end local v12    # "N":I
    .end local v15    # "i":I
    .end local v17    # "velocity":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    const/high16 v6, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v6

    float-to-int v0, v1

    move/from16 v17, v0

    goto/16 :goto_0

    .line 137
    .restart local v2    # "lastX":F
    .restart local v3    # "lastY":F
    .restart local v12    # "N":I
    .restart local v15    # "i":I
    .restart local v17    # "velocity":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_4

    .line 138
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    const/16 v6, 0xff

    const/16 v7, 0xff

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 139
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v1

    const/high16 v6, 0x41800000    # 16.0f

    mul-float v19, v1, v6

    .line 140
    .local v19, "xVel":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    const/high16 v6, 0x41800000    # 16.0f

    mul-float v20, v1, v6

    .line 141
    .local v20, "yVel":F
    add-float v9, v2, v19

    add-float v10, v3, v20

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    move v7, v2

    move v8, v3

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 146
    .end local v19    # "xVel":F
    .end local v20    # "yVel":F
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurDown:Z

    if-eqz v1, :cond_3

    .line 147
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    int-to-float v8, v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->getWidth()I

    move-result v1

    int-to-float v9, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 148
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    int-to-float v7, v1

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    int-to-float v9, v1

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->getHeight()I

    move-result v1

    int-to-float v10, v1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTargetPaint:Landroid/graphics/Paint;

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 149
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurPressure:F

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v1, v6

    float-to-int v0, v1

    move/from16 v16, v0

    .line 150
    .local v16, "pressureLevel":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    const/16 v6, 0xff

    const/16 v7, 0x80

    move/from16 v0, v16

    rsub-int v8, v0, 0xff

    move/from16 v0, v16

    invoke-virtual {v1, v6, v0, v7, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 151
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 152
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurWidth:I

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 154
    .end local v16    # "pressureLevel":I
    :cond_3
    return-void

    .line 143
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/PointerLocation$MyView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto/16 :goto_2
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->getFontMetricsInt(Landroid/graphics/Paint$FontMetricsInt;)I

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    neg-int v0, v0

    iget-object v1, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mHeaderBottom:I

    .line 97
    const-string v0, "PointerLocation"

    const-string v1, "onMeasure"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Metrics: ascent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " descent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " leading="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " top="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bottom="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mTextMetrics:Landroid/graphics/Paint$FontMetricsInt;

    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    .line 158
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 160
    .local v1, "action":I
    if-nez v1, :cond_0

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mYs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 163
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    .line 166
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mVelocity:Landroid/view/VelocityTracker;

    invoke-virtual {v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 168
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    .line 170
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mYs:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mXs:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mYs:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    if-eqz v1, :cond_2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    :cond_2
    move v3, v4

    :goto_1
    iput-boolean v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurDown:Z

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurX:I

    .line 179
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurY:I

    .line 180
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurPressure:F

    .line 181
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurSize:F

    .line 182
    iget v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurSize:F

    invoke-virtual {p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v5, v6

    mul-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/status/PointerLocation$MyView;->mCurWidth:I

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/status/PointerLocation$MyView;->invalidate()V

    .line 184
    return v4

    .line 177
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method
