.class Lcom/sec/android/app/status/WiFiStressTest$1;
.super Landroid/os/Handler;
.source "WiFiStressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/WiFiStressTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/WiFiStressTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/WiFiStressTest;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x1

    .line 99
    const-string v2, "WiFiStressTest"

    const-string v3, "mHandler()"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/os/Message;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/WiFiStressTest;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    const-string v2, "WiFiStressTest"

    const-string v3, "mHandler()"

    const-string v4, "Already finished test"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :goto_0
    return-void

    .line 106
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 109
    :pswitch_0
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    const-string v2, "WiFiStressTest"

    const-string v3, "handleMessage()"

    const-string v4, "alarm on"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/WiFiStressTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # operator++ for: Lcom/sec/android/app/status/WiFiStressTest;->mTestCount:I
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$008(Lcom/sec/android/app/status/WiFiStressTest;)I

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # invokes: Lcom/sec/android/app/status/WiFiStressTest;->canceltAlarm()V
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$100(Lcom/sec/android/app/status/WiFiStressTest;)V

    .line 115
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # invokes: Lcom/sec/android/app/status/WiFiStressTest;->setAlarm()V
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$200(Lcom/sec/android/app/status/WiFiStressTest;)V

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/WiFiStressTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 117
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_0

    .line 120
    .end local v0    # "pm":Landroid/os/PowerManager;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    const-string v3, "wifi"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/WiFiStressTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 122
    .local v1, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 124
    :cond_2
    const-string v2, "WiFiStressTest"

    const-string v3, "mHandler()"

    const-string v4, "Start Test"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$300(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Switch;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/Switch;->setChecked(Z)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    sget-object v3, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->ENBLED:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    # setter for: Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;
    invoke-static {v2, v3}, Lcom/sec/android/app/status/WiFiStressTest;->access$402(Lcom/sec/android/app/status/WiFiStressTest;Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;)Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$500(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/TextView;

    move-result-object v2

    const-string v3, "WiFi ON"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$600(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setActivated(Z)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$600(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setClickable(Z)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;
    invoke-static {v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$600(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    iget-object v3, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mStartButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/status/WiFiStressTest;->access$600(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/WiFiStressTest;->onClick(Landroid/view/View;)V

    goto/16 :goto_0

    .line 135
    :cond_3
    invoke-virtual {v1, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/status/WiFiStressTest$1;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    iget-object v2, v2, Lcom/sec/android/app/status/WiFiStressTest;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
