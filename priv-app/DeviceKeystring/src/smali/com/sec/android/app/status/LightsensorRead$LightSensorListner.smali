.class Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;
.super Ljava/lang/Object;
.source "LightsensorRead.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/LightsensorRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LightSensorListner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/LightsensorRead;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/LightsensorRead;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;->this$0:Lcom/sec/android/app/status/LightsensorRead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/LightsensorRead;Lcom/sec/android/app/status/LightsensorRead$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/LightsensorRead;
    .param p2, "x1"    # Lcom/sec/android/app/status/LightsensorRead$1;

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;-><init>(Lcom/sec/android/app/status/LightsensorRead;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 214
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 217
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 225
    :goto_0
    return-void

    .line 219
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;->this$0:Lcom/sec/android/app/status/LightsensorRead;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    # invokes: Lcom/sec/android/app/status/LightsensorRead;->setSensorValue(F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/LightsensorRead;->access$400(Lcom/sec/android/app/status/LightsensorRead;F)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/status/LightsensorRead$LightSensorListner;->this$0:Lcom/sec/android/app/status/LightsensorRead;

    # invokes: Lcom/sec/android/app/status/LightsensorRead;->printCurrentLuxInfo()V
    invoke-static {v0}, Lcom/sec/android/app/status/LightsensorRead;->access$100(Lcom/sec/android/app/status/LightsensorRead;)V

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
