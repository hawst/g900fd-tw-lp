.class public Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;
.super Landroid/view/View;
.source "SPen_LinearityUI.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/SPen_LinearityUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TouchView"
.end annotation


# instance fields
.field private isHover:Z

.field private isLandscape:Z

.field private isPenDetect:Z

.field private isTouchDown:Z

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mHoverCircleSize:F

.field private mHoverPaint:Landroid/graphics/Paint;

.field private mHoveredX:F

.field private mHoveredY:F

.field private mInfoBackgroundPaint:Landroid/graphics/Paint;

.field private mInfoBitmap:Landroid/graphics/Bitmap;

.field private mInfoCanvas:Landroid/graphics/Canvas;

.field private mInfoPaint:Landroid/graphics/Paint;

.field private mLevelPaint:Landroid/graphics/Paint;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreHoveredX:F

.field private mPreHoveredY:F

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/status/SPen_LinearityUI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/SPen_LinearityUI;Landroid/content/Context;)V
    .locals 13
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 113
    iput-object p1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    .line 114
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 85
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    .line 86
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    .line 87
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 88
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 90
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    .line 91
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    .line 92
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    .line 93
    iput v9, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    .line 115
    invoke-virtual {p0, v11}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->setKeepScreenOn(Z)V

    .line 116
    const-string v5, "window"

    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 117
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 118
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 119
    iget v5, v2, Landroid/graphics/Point;->x:I

    iput v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    .line 120
    iget v5, v2, Landroid/graphics/Point;->y:I

    iput v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    .line 121
    const-string v5, "SPenTestUI"

    const-string v6, "TouchView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Screen size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 124
    .local v4, "r":Landroid/content/res/Resources;
    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->SIZE_RECT:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$200(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v12, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 125
    .local v3, "pxsize":F
    iget v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I
    invoke-static {p1, v5}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$302(Lcom/sec/android/app/status/SPen_LinearityUI;I)I

    .line 126
    iget v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I
    invoke-static {p1, v5}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$402(Lcom/sec/android/app/status/SPen_LinearityUI;I)I

    .line 127
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v12, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverCircleSize:F

    .line 130
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 131
    :cond_0
    iput-boolean v10, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isLandscape:Z

    .line 136
    :goto_0
    const-string v5, "SPenTestUI"

    const-string v6, "TouchView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "channel number: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$300(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$400(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    iget v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 138
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    iget v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    invoke-static {v0, v5, v6, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 139
    new-instance v5, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 140
    iget-object v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->setPaint()V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->initRect()V

    .line 144
    invoke-direct {p0, v9, v9, v9}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V

    .line 145
    iput-boolean v10, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    .line 146
    iput-boolean v10, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    .line 147
    iput-boolean v10, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isHover:Z

    .line 148
    return-void

    .line 133
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iput-boolean v11, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isLandscape:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->initRect()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;FFF)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;
    .param p1, "x1"    # F
    .param p2, "x2"    # F
    .param p3, "x3"    # F

    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V

    return-void
.end method

.method private drawInfo(FFF)V
    .locals 14
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "pressure"    # F

    .prologue
    .line 344
    const-string v13, ""

    .line 345
    .local v13, "result":Ljava/lang/String;
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    div-int/lit8 v12, v0, 0x5

    .line 346
    .local v12, "itemW":I
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 347
    .local v5, "mtx":Landroid/graphics/Matrix;
    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    const/16 v4, 0x1f

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBitmap:Landroid/graphics/Bitmap;

    .line 350
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    .line 351
    iget-object v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0x1

    int-to-float v9, v0

    const/high16 v10, 0x41f00000    # 30.0f

    iget-object v11, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 354
    const-string v13, ""

    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "   Scr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 357
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isLandscape:Z

    if-eqz v0, :cond_1

    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "LAND"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 363
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v1, v12, 0x0

    int-to-float v1, v1

    const/high16 v2, 0x41d00000    # 26.0f

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 366
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    const/16 v1, 0xc0

    const/16 v2, 0x64

    const/16 v3, 0x64

    const/16 v4, 0xc8

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 370
    iget-object v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v0, v12, 0x1

    int-to-float v7, v0

    const/high16 v8, 0x3f800000    # 1.0f

    mul-int/lit8 v0, v12, 0x2

    add-int/lit8 v0, v0, -0x5

    int-to-float v9, v0

    const/high16 v10, 0x41f00000    # 30.0f

    iget-object v11, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 373
    :cond_0
    const-string v13, ""

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  PDCT : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 376
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    if-eqz v0, :cond_2

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "On "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 382
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v1, v12, 0x1

    int-to-float v1, v1

    const/high16 v2, 0x41d00000    # 26.0f

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 384
    const-string v13, ""

    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " X: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 386
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%.2f "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v1, v12, 0x2

    int-to-float v1, v1

    const/high16 v2, 0x41d00000    # 26.0f

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 389
    const-string v13, ""

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Y: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%.2f "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v1, v12, 0x3

    int-to-float v1, v1

    const/high16 v2, 0x41d00000    # 26.0f

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    const/16 v1, 0xc0

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 397
    iget-object v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v0, v12, 0x4

    int-to-float v7, v0

    const/high16 v8, 0x3f800000    # 1.0f

    mul-int/lit8 v0, v12, 0x5

    add-int/lit8 v0, v0, -0x1

    int-to-float v9, v0

    const/high16 v10, 0x41f00000    # 30.0f

    iget-object v11, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 399
    iget-object v6, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v0, v12, 0x4

    int-to-float v7, v0

    const/high16 v8, 0x3f800000    # 1.0f

    mul-int/lit8 v0, v12, 0x4

    int-to-float v0, v0

    int-to-float v1, v12

    mul-float v1, v1, p3

    add-float v9, v0, v1

    const/high16 v10, 0x41f00000    # 30.0f

    iget-object v11, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 400
    const-string v13, ""

    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Prs : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%.3f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    mul-int/lit8 v1, v12, 0x4

    int-to-float v1, v1

    const/high16 v2, 0x41d00000    # 26.0f

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v13, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 404
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 406
    return-void

    .line 360
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "PORT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 379
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Off"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_1
.end method

.method private drawLine(FFFFZ)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "bTouch"    # Z

    .prologue
    .line 309
    if-eqz p5, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 313
    :cond_0
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 315
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_1

    .line 316
    float-to-int v6, p1

    .line 317
    float-to-int v8, p3

    .line 323
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_2

    .line 324
    float-to-int v7, p2

    .line 325
    float-to-int v9, p4

    .line 331
    :goto_1
    if-eqz p5, :cond_3

    .line 332
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x3

    add-int/lit8 v2, v9, -0x3

    add-int/lit8 v3, v6, 0x3

    add-int/lit8 v4, v7, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 336
    :goto_2
    return-void

    .line 319
    :cond_1
    float-to-int v6, p3

    .line 320
    float-to-int v8, p1

    goto :goto_0

    .line 327
    :cond_2
    float-to-int v7, p4

    .line 328
    float-to-int v9, p2

    goto :goto_1

    .line 334
    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x7

    add-int/lit8 v2, v9, -0x7

    add-int/lit8 v3, v6, 0x7

    add-int/lit8 v4, v7, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_2
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 340
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x3

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x3

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x3

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 341
    return-void
.end method

.method private initRect()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 410
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$400(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v8, v0, v2

    .line 411
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$300(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v9, v0, v2

    .line 412
    .local v9, "col_width":F
    const/4 v6, 0x0

    .line 413
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 414
    .local v7, "ColY":I
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 415
    .local v12, "mRectPaint":Landroid/graphics/Paint;
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 417
    const/high16 v0, -0x1000000

    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 419
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$400(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v0

    if-ge v10, v0, :cond_1

    .line 420
    int-to-float v0, v10

    mul-float/2addr v0, v8

    float-to-int v7, v0

    .line 422
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    # getter for: Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->access$300(Lcom/sec/android/app/status/SPen_LinearityUI;)I

    move-result v0

    if-ge v11, v0, :cond_0

    .line 423
    int-to-float v0, v11

    mul-float/2addr v0, v9

    float-to-int v6, v0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v7

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mScreenHeight:I

    int-to-float v4, v4

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 422
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 419
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 428
    .end local v11    # "j":I
    :cond_1
    return-void
.end method

.method private setPaint()V
    .locals 7

    .prologue
    const v6, -0xff0100

    const v5, -0xffff01

    const/16 v4, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 151
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 167
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 170
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 174
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    const/16 v1, 0xc0

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 177
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 179
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mInfoBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 186
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isHover:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    invoke-virtual {v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringEnbale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverCircleSize:F

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 189
    :cond_0
    return-void
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 254
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 256
    .local v6, "action":I
    packed-switch v6, :pswitch_data_0

    .line 304
    :goto_0
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V

    .line 305
    return v5

    .line 258
    :pswitch_1
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    const-string v2, "ACTION_HOVER_ENTER "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    .line 260
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    .line 261
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    const-string v2, "ACTION_HOVER_ENTER test start!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    .line 263
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isHover:Z

    goto :goto_0

    .line 266
    :pswitch_2
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    const-string v2, "ACTION_HOVER_MOVE"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 269
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    .line 270
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    .line 271
    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 272
    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    invoke-virtual {v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringEnbale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawLine(FFFFZ)V

    .line 268
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 279
    :cond_1
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    .line 280
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    .line 281
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    .line 282
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    .line 283
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PreX,Y:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CurX,Y:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_LinearityUI;

    invoke-virtual {v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringEnbale()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawLine(FFFFZ)V

    .line 290
    :cond_2
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    .line 291
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isHover:Z

    goto/16 :goto_0

    .line 294
    .end local v7    # "i":I
    :pswitch_3
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    const-string v2, "ACTION_HOVER_EXIT"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    .line 296
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isHover:Z

    .line 297
    const-string v0, "SPenTestUI"

    const-string v1, "onHover"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PREX,Y:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredX:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreHoveredY:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x7

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    float-to-int v2, v2

    add-int/lit8 v2, v2, -0x7

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0x7

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 299
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredX:F

    .line 300
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mHoveredY:F

    goto/16 :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 193
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 195
    .local v6, "action":I
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 196
    packed-switch v6, :pswitch_data_0

    .line 243
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V

    .line 248
    :goto_1
    return v5

    .line 198
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 200
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    .line 201
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    goto :goto_0

    .line 205
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 206
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 207
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    .line 208
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    .line 209
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 210
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 211
    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawLine(FFFFZ)V

    .line 206
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 214
    :cond_1
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    .line 215
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    .line 216
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 217
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 218
    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawLine(FFFFZ)V

    .line 219
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    .line 220
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    goto :goto_0

    .line 226
    .end local v7    # "i":I
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 227
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    .line 228
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    .line 229
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    .line 230
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    .line 232
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 233
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawPoint(FF)V

    .line 236
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isTouchDown:Z

    .line 237
    iput-boolean v3, p0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->isPenDetect:Z

    goto/16 :goto_0

    .line 245
    :cond_3
    invoke-direct {p0, v2, v2, v2}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V

    goto/16 :goto_1

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
