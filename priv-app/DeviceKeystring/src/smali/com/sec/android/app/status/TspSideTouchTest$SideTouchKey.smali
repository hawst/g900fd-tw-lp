.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;
.super Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
.source "TspSideTouchTest.java"

# interfaces
.implements Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SideTouchKey"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 1
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    .line 899
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    .line 900
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 901
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 902
    return-void
.end method


# virtual methods
.method public onKey(ILandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 912
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyCode:I

    if-eq p1, v0, :cond_0

    .line 935
    :goto_0
    return-void

    .line 916
    :cond_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", keyCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 927
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->request(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V

    goto :goto_0

    .line 930
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->cancel(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V

    goto :goto_0

    .line 919
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStatusChanged(Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;)V
    .locals 4
    .param p1, "status"    # Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .prologue
    .line 1002
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_CANCEL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-ne p1, v0, :cond_0

    .line 1003
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1012
    :cond_0
    :pswitch_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStatusChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1015
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1016
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->updateView()V

    .line 1017
    :pswitch_1
    return-void

    .line 1003
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUpdate([Ljava/lang/String;)V
    .locals 7
    .param p1, "value"    # [Ljava/lang/String;

    .prologue
    .line 979
    if-eqz p1, :cond_0

    .line 981
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_param:I

    .line 982
    .local v1, "index":I
    array-length v3, p1

    if-ge v1, v3, :cond_0

    .line 983
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "onUpdate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", value: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    :try_start_0
    aget-object v3, p1, v1

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 987
    .local v2, "sensitivity":I
    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->setSensitivity(I)V

    .line 988
    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-static {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->playKeySound(Landroid/content/Context;)V

    .line 989
    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    sget-object v4, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-ne v3, v4, :cond_0

    .line 990
    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V
    invoke-static {v3}, Lcom/sec/android/app/status/TspSideTouchTest;->access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 997
    .end local v1    # "index":I
    .end local v2    # "sensitivity":I
    :cond_0
    :goto_0
    return-void

    .line 992
    .restart local v1    # "index":I
    :catch_0
    move-exception v0

    .line 993
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected setSensitivity(I)V
    .locals 5
    .param p1, "sensitivity"    # I

    .prologue
    .line 940
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_sensitivity:I

    .line 941
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_specMin:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_sensitivity:I

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_sensitivity:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_specMax:I

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 942
    .local v0, "bSpecIn":Z
    :goto_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setKeyStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensitivity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bSpecIn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    if-eqz v0, :cond_2

    .line 946
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 974
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->updateView()V

    .line 975
    return-void

    .line 941
    .end local v0    # "bSpecIn":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 948
    .restart local v0    # "bSpecIn":Z
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 949
    iget-boolean v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_bNvUpdate:Z

    if-eqz v1, :cond_1

    .line 952
    :cond_1
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->cancel(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V

    goto :goto_1

    .line 962
    :cond_2
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    :pswitch_2
    goto :goto_1

    .line 964
    :pswitch_3
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    goto :goto_1

    .line 946
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 962
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    .locals 1
    .param p1, "param"    # I

    .prologue
    .line 906
    invoke-static {p1}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->getThreshold(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->_threshold:Ljava/lang/String;

    .line 907
    return-object p0
.end method
