.class public Lcom/sec/android/app/status/BatteryStatus_kor;
.super Landroid/app/Activity;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/BatteryStatus_kor$1;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;,
        Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;,
        Lcom/sec/android/app/status/BatteryStatus_kor$Module;,
        Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;,
        Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 658
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1223
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 1224
    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor;->setContentView(I)V

    .line 1226
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->getModelInfo()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v1

    .line 1227
    .local v1, "mi":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getIsCreated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1228
    const-string v2, "BatteryStatus"

    const-string v3, "Initialize(%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    invoke-static {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->create(Landroid/app/Activity;)Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    .line 1230
    .local v0, "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setOrientation()V

    .line 1231
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->createButtons()Z

    .line 1236
    .end local v0    # "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    :goto_0
    return-void

    .line 1234
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor;->recreate()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 1265
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    .line 1266
    .local v0, "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->resetOrientation()V

    .line 1267
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1268
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1241
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    .line 1242
    .local v0, "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setBrightnessMode()V

    .line 1243
    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setLcdDimming(Z)V

    .line 1244
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setScreenOn()V

    .line 1245
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->createHandler()V

    .line 1246
    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->sendMessage(ILjava/lang/Object;)V

    .line 1247
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->sendMessage(ILjava/lang/Object;)V

    .line 1248
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 1249
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1254
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    .line 1255
    .local v0, "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->removeHandler()V

    .line 1256
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setLcdDimming(Z)V

    .line 1257
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->resetBrightnessMode()V

    .line 1258
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->resetScreenOn()V

    .line 1259
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 1260
    return-void
.end method
