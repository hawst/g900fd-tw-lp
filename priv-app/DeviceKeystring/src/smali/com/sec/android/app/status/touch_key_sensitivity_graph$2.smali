.class Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;
.super Ljava/util/TimerTask;
.source "touch_key_sensitivity_graph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/touch_key_sensitivity_graph;->drawGraph()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iput-object p2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 310
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsBack:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_back"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 312
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_BACK:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 314
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsHome:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_home"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 316
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_HOME:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 318
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsRecent:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_recent"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 320
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_RECENT:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 322
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsMenu:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 323
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_menu"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$402(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 324
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_MENU:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 327
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy1key:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY1:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$602(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 329
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_1KEY:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 331
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy3key:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY3:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 333
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_3KEY:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 335
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy4key:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY4:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1602(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 337
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_4KEY:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$3900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 339
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy5key:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 340
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY5:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$802(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 341
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_5KEY:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 343
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isDummy6key:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 344
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->PATH_NOISE_TSK_DUMMY6:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 345
    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    invoke-static {}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2200()[Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->INDEX_6KEY:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V

    .line 348
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isExistsThres:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 351
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_threshold"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4702(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;)Ljava/lang/String;

    .line 352
    const-string v3, "touch_key_sensitivity_graph"

    const-string v4, "isExistsThres"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "thresholdValueStr : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 355
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValueStr:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$4700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 357
    .local v0, "TempSpiltStr":[Ljava/lang/String;
    array-length v3, v0

    const/4 v4, 0x5

    if-ne v3, v4, :cond_b

    .line 358
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$902(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 359
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1002(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 360
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 361
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x4

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 387
    .end local v0    # "TempSpiltStr":[Ljava/lang/String;
    :cond_a
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 388
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 392
    .end local v2    # "msg":Landroid/os/Message;
    :goto_1
    return-void

    .line 363
    .restart local v0    # "TempSpiltStr":[Ljava/lang/String;
    :cond_b
    array-length v3, v0

    const/4 v4, 0x6

    if-ne v3, v4, :cond_c

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$902(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 365
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1002(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 367
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 368
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x4

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 369
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x5

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1802(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 389
    .end local v0    # "TempSpiltStr":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 390
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v3, "NumberFormatException"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NumberFormatException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 370
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .restart local v0    # "TempSpiltStr":[Ljava/lang/String;
    :cond_c
    :try_start_1
    array-length v3, v0

    if-ne v3, v7, :cond_d

    .line 371
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1002(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    goto/16 :goto_0

    .line 374
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$902(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 375
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1002(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 376
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1102(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 377
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1202(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 378
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1302(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    .line 379
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1802(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I

    goto/16 :goto_0

    .line 383
    .end local v0    # "TempSpiltStr":[Ljava/lang/String;
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    const-string v5, "/sys/class/sec/sec_touchkey/touchkey_threshold"

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->fileRead(Ljava/lang/String;I)Ljava/lang/String;
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$2100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    # setter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I
    invoke-static {v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$502(Lcom/sec/android/app/status/touch_key_sensitivity_graph;I)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
