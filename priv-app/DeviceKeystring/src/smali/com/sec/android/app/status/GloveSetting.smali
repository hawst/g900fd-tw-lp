.class public Lcom/sec/android/app/status/GloveSetting;
.super Landroid/app/Activity;
.source "GloveSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private handler:Landroid/os/Handler;

.field private run:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    new-instance v0, Lcom/sec/android/app/status/GloveSetting$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GloveSetting$1;-><init>(Lcom/sec/android/app/status/GloveSetting;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GloveSetting;->run:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v4, 0x1f4

    .line 51
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_0
    return-void

    .line 53
    :pswitch_0
    const-string v0, "GloveSetting"

    const-string v1, "onClick"

    const-string v2, "glove_on"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "TSP_COMMAND_CMD"

    const-string v1, "glove_mode,1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/status/GloveSetting;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/GloveSetting;->run:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 58
    :pswitch_1
    const-string v0, "GloveSetting"

    const-string v1, "onClick"

    const-string v2, "glove_off"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "TSP_COMMAND_CMD"

    const-string v1, "glove_mode,0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/status/GloveSetting;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/GloveSetting;->run:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x7f0a0087
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    const v2, 0x7f030025

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/GloveSetting;->setContentView(I)V

    .line 22
    const v2, 0x7f0a0087

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/GloveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 23
    .local v1, "button_glove_on":Landroid/widget/Button;
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    const v2, 0x7f0a0088

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/GloveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 25
    .local v0, "button_glove_off":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/status/GloveSetting;->handler:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 35
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 31
    return-void
.end method
