.class public Lcom/sec/android/app/shutdown/ShutdownPreference;
.super Landroid/preference/PreferenceActivity;
.source "ShutdownPreference.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static NO_POPUP:I

.field private static ON_POPUP:I


# instance fields
.field private CHANGE_HARD_RESET:Ljava/lang/String;

.field private final CLASS_NAME:Ljava/lang/String;

.field mChangeHardReset:Landroid/preference/CheckBoxPreference;

.field mEnableShutdown:Landroid/preference/CheckBoxPreference;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/shutdown/ShutdownPreference;->ON_POPUP:I

    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/shutdown/ShutdownPreference;->NO_POPUP:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mEnableShutdown:Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mChangeHardReset:Landroid/preference/CheckBoxPreference;

    .line 26
    const-string v0, "ON"

    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->CHANGE_HARD_RESET:Ljava/lang/String;

    .line 27
    const-string v0, "ShutdownPreference"

    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method private enable_uart(Z)V
    .locals 9
    .param p1, "command"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 49
    if-eqz p1, :cond_0

    move v2, v5

    .line 50
    .local v2, "uart_enable":I
    :goto_0
    const-string v7, "PDA"

    const-string v8, "USB_PATH"

    invoke-static {v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v4, v5

    .line 51
    .local v4, "usb_sel":I
    :goto_1
    const-string v7, "AP"

    const-string v8, "UART_PATH"

    invoke-static {v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v3, v5

    .line 54
    .local v3, "uart_sel":I
    :goto_2
    shl-int/lit8 v5, v2, 0x3

    shl-int/lit8 v6, v3, 0x1

    or-int/2addr v5, v6

    or-int v1, v5, v4

    .line 55
    .local v1, "switch_sel":I
    const-string v5, "ShutdownPreference"

    const-string v6, "onResume"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "switch_sel : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "power"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 58
    .local v0, "pm":Landroid/os/PowerManager;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "swsel"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 59
    return-void

    .end local v0    # "pm":Landroid/os/PowerManager;
    .end local v1    # "switch_sel":I
    .end local v2    # "uart_enable":I
    .end local v3    # "uart_sel":I
    .end local v4    # "usb_sel":I
    :cond_0
    move v2, v6

    .line 49
    goto :goto_0

    .restart local v2    # "uart_enable":I
    :cond_1
    move v4, v6

    .line 50
    goto :goto_1

    .restart local v4    # "usb_sel":I
    :cond_2
    move v3, v6

    .line 51
    goto :goto_2
.end method


# virtual methods
.method public getPopupWhenPowerOff()I
    .locals 4

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 116
    .local v1, "value":I
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "SHOULD_SHUT_DOWN"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 122
    :goto_0
    return v1

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 119
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStatusOfHardReset()Ljava/lang/String;
    .locals 4

    .prologue
    .line 126
    const-string v1, ""

    .line 128
    .local v1, "value":Ljava/lang/String;
    const-string v2, "pref_hardReset"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 129
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "mode_hardReset"

    const-string v3, "OFF"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->addPreferencesFromResource(I)V

    .line 33
    const-string v0, "shutdown"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mEnableShutdown:Landroid/preference/CheckBoxPreference;

    .line 34
    const-string v0, "hardReset"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mChangeHardReset:Landroid/preference/CheckBoxPreference;

    .line 46
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1, "p"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x1

    .line 70
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "key":Ljava/lang/String;
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 73
    .local v1, "enabled":Z
    const-string v4, "shutdown"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 74
    if-eqz v1, :cond_1

    .line 75
    sget v4, Lcom/sec/android/app/shutdown/ShutdownPreference;->NO_POPUP:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shutdown/ShutdownPreference;->setPopupWhenPowerOff(I)V

    .line 76
    const-string v4, "SHOULD_SHUT_DOWN"

    const-string v5, "1"

    invoke-static {v4, v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 77
    const-string v4, "SUPPORT_IF_CORROSION"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-direct {p0, v6}, Lcom/sec/android/app/shutdown/ShutdownPreference;->enable_uart(Z)V

    .line 105
    :cond_0
    :goto_0
    return v6

    .line 81
    :cond_1
    sget v4, Lcom/sec/android/app/shutdown/ShutdownPreference;->ON_POPUP:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/shutdown/ShutdownPreference;->setPopupWhenPowerOff(I)V

    .line 82
    const-string v4, "SHOULD_SHUT_DOWN"

    const-string v5, "0"

    invoke-static {v4, v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 83
    const-string v4, "SUPPORT_IF_CORROSION"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 84
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/sec/android/app/shutdown/ShutdownPreference;->enable_uart(Z)V

    goto :goto_0

    .line 87
    :cond_2
    const-string v4, "hardReset"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 88
    if-eqz v1, :cond_3

    .line 89
    const-string v4, "pref_hardReset"

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 90
    .local v3, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "mode_hardReset"

    const-string v5, "ON"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 94
    const-string v4, "RESET_ENABLED"

    const-string v5, "1"

    invoke-static {v4, v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 96
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "preferences":Landroid/content/SharedPreferences;
    :cond_3
    const-string v4, "pref_hardReset"

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 97
    .restart local v3    # "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 98
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "mode_hardReset"

    const-string v5, "OFF"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 99
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 101
    const-string v4, "RESET_ENABLED"

    const-string v5, "0"

    invoke-static {v4, v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 63
    const-string v0, "ShutdownPreference"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getPopupWhenPowerOff()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getStatusOfHardReset()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/shutdown/ShutdownPreference;->updateCheckBoxUI(ILjava/lang/String;)V

    .line 66
    return-void
.end method

.method public setPopupWhenPowerOff(I)V
    .locals 2
    .param p1, "enableFlag"    # I

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/shutdown/ShutdownPreference;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "SHOULD_SHUT_DOWN"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 110
    return-void
.end method

.method public updateCheckBoxUI(ILjava/lang/String;)V
    .locals 4
    .param p1, "value_shutdown"    # I
    .param p2, "value_hardReset"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mEnableShutdown:Landroid/preference/CheckBoxPreference;

    sget v0, Lcom/sec/android/app/shutdown/ShutdownPreference;->NO_POPUP:I

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mEnableShutdown:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mChangeHardReset:Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->CHANGE_HARD_RESET:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/shutdown/ShutdownPreference;->mChangeHardReset:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 140
    return-void

    :cond_0
    move v0, v2

    .line 135
    goto :goto_0

    :cond_1
    move v1, v2

    .line 138
    goto :goto_1
.end method
