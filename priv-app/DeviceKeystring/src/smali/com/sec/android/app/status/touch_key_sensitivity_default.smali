.class public Lcom/sec/android/app/status/touch_key_sensitivity_default;
.super Landroid/app/Activity;
.source "touch_key_sensitivity_default.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;
    }
.end annotation


# static fields
.field private static current_Version:Ljava/lang/String;

.field private static recommand_Version:Ljava/lang/String;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private DUMMY:I

.field private GET_DATA:I

.field private INDEX_RAWDATA_BACK:I

.field private INDEX_RAWDATA_BACK_OUTER:I

.field private INDEX_RAWDATA_HIDDEN1:I

.field private INDEX_RAWDATA_HIDDEN2:I

.field private INDEX_RAWDATA_HOME:I

.field private INDEX_RAWDATA_HOME_OUTER:I

.field private INDEX_RAWDATA_MENU:I

.field private INDEX_RAWDATA_MENU_OUTER:I

.field private INDEX_RAWDATA_RECENT:I

.field private INDEX_RAWDATA_RECENT_OUTER:I

.field private INDEX_RAWDATA_REF1:I

.field private INDEX_RAWDATA_REF2:I

.field private INDEX_SENSITIVITY_BACK:I

.field private INDEX_SENSITIVITY_BACK_OUTER:I

.field private INDEX_SENSITIVITY_HIDDEN1:I

.field private INDEX_SENSITIVITY_HIDDEN2:I

.field private INDEX_SENSITIVITY_HOME:I

.field private INDEX_SENSITIVITY_HOME_OUTER:I

.field private INDEX_SENSITIVITY_MENU:I

.field private INDEX_SENSITIVITY_MENU_OUTER:I

.field private INDEX_SENSITIVITY_RECENT:I

.field private INDEX_SENSITIVITY_RECENT_OUTER:I

.field private INDEX_SENSITIVITY_REF1:I

.field private INDEX_SENSITIVITY_REF2:I

.field private KEY_BACK:I

.field private final KEY_HIDDEN1:I

.field private final KEY_HIDDEN2:I

.field private KEY_HOME:I

.field private KEY_MENU:I

.field private KEY_RECENT:I

.field private final KEY_REF1:I

.field private final KEY_REF2:I

.field private KEY_SEARCH:I

.field private final PATH_AUTOCAL:Ljava/lang/String;

.field private final PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_IDAC:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_IDAC_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_IDAC_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_RAWDATA_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_RAWDATA_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_RAWDATA_REFERENCE:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_REFERENCE:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_THRESHOLD_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_BACK_THRESHOLD_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HIDDEN1:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HIDDEN1_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HIDDEN2:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HIDDEN2_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HOME:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HOME_IDAC:Ljava/lang/String;

.field private final PATH_NOISE_TSK_HOME_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_MENU:Ljava/lang/String;

.field private final PATH_NOISE_TSK_MENU_IDAC:Ljava/lang/String;

.field private final PATH_NOISE_TSK_MENU_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_IDAC:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_IDAC_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_IDAC_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_RAWDATA:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_RAWDATA_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_RAWDATA_OUTER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_RAWDATA_REFERENCE:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_REFERENCE:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_THRESHOLD_INNER:Ljava/lang/String;

.field private final PATH_NOISE_TSK_RECENT_THRESHOLD_OUTER:Ljava/lang/String;

.field private final PATH_RECOMMEND_VERSION:Ljava/lang/String;

.field private final PATH_THD_SPEC:Ljava/lang/String;

.field private final PATH_TSK_BRIGHTESS:Ljava/lang/String;

.field private final PATH_TSK_UPDATE:Ljava/lang/String;

.field private final PATH_TSK_VERSION:Ljava/lang/String;

.field private SHOW_DATA:I

.field private bKey_Color:I

.field exit_button:Landroid/widget/Button;

.field final handler:Landroid/os/Handler;

.field private idacBackKeyValue:Ljava/lang/String;

.field private idacBackKeyValue_outer:Ljava/lang/String;

.field private idacHomeKeyValue:Ljava/lang/String;

.field private idacHomeKeyValue_outer:Ljava/lang/String;

.field private idacMenuKeyValue:Ljava/lang/String;

.field private idacMenuKeyValue_outer:Ljava/lang/String;

.field private idacRecentKeyValue:Ljava/lang/String;

.field private idacRecentKeyValue_outer:Ljava/lang/String;

.field private idac_label:Landroid/widget/TextView;

.field private isAutocal:Z

.field private isExistsBack:Z

.field private isExistsHidden1:Z

.field private isExistsHidden2:Z

.field private isExistsHome:Z

.field private isExistsMenu:Z

.field private isExistsRecent:Z

.field private isExistsRef1:Z

.field private isExistsRef2:Z

.field private isInOut:Z

.field private linear_backraw:Landroid/widget/LinearLayout;

.field private linear_backthd:Landroid/widget/LinearLayout;

.field private linear_homeraw:Landroid/widget/LinearLayout;

.field private linear_homethd:Landroid/widget/LinearLayout;

.field private linear_menuraw:Landroid/widget/LinearLayout;

.field private linear_menuthd:Landroid/widget/LinearLayout;

.field private linear_recentraw:Landroid/widget/LinearLayout;

.field private linear_recentthd:Landroid/widget/LinearLayout;

.field private linear_ts_backraw:Landroid/widget/LinearLayout;

.field private linear_ts_homeraw:Landroid/widget/LinearLayout;

.field private linear_ts_menuraw:Landroid/widget/LinearLayout;

.field private linear_ts_recentraw:Landroid/widget/LinearLayout;

.field private mButtonLedTime:I

.field private mContext:Landroid/content/Context;

.field private final mEnabledDummyKey:Z

.field private mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

.field private mIsPausedByLcdOff:Z

.field private mKey_Color:I

.field private mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

.field private mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

.field protected mTimerHandler:Landroid/os/Handler;

.field private mToggleHighsensitivityMode:Z

.field private mWl:Landroid/os/PowerManager$WakeLock;

.field private rawBackKeyValue:Ljava/lang/String;

.field private rawBackKeyValue_outer:Ljava/lang/String;

.field private rawHiddenKey1Value:Ljava/lang/String;

.field private rawHiddenKey2Value:Ljava/lang/String;

.field private rawHomeKeyValue:Ljava/lang/String;

.field private rawHomeKeyValue_outer:Ljava/lang/String;

.field private rawMenuKeyValue:Ljava/lang/String;

.field private rawMenuKeyValue_outer:Ljava/lang/String;

.field private rawRecentKeyValue:Ljava/lang/String;

.field private rawRecentKeyValue_outer:Ljava/lang/String;

.field private rawRef1Value:Ljava/lang/String;

.field private rawRef2Value:Ljava/lang/String;

.field private sensitivityBackKeyValue:Ljava/lang/String;

.field private sensitivityBackKeyValue_outer:Ljava/lang/String;

.field private sensitivityHiddenKey1Value:Ljava/lang/String;

.field private sensitivityHiddenKey2Value:Ljava/lang/String;

.field private sensitivityHomeKeyValue:Ljava/lang/String;

.field private sensitivityHomeKeyValue_outer:Ljava/lang/String;

.field private sensitivityMenuKeyValue:Ljava/lang/String;

.field private sensitivityMenuKeyValue_outer:Ljava/lang/String;

.field private sensitivityRecentKeyValue:Ljava/lang/String;

.field private sensitivityRecentKeyValue_outer:Ljava/lang/String;

.field private sensitivityRef1Value:Ljava/lang/String;

.field private sensitivityRef2Value:Ljava/lang/String;

.field private tablerow_idac:Landroid/widget/TableRow;

.field private tablerow_raw:Landroid/widget/TableRow;

.field private tablerow_raw_max:Landroid/widget/TableRow;

.field private tablerow_thd:Landroid/widget/TableRow;

.field private tablerow_title:Landroid/widget/TableRow;

.field private tablerow_ts_inout:Landroid/widget/TableRow;

.field private tablerow_ts_max:Landroid/widget/TableRow;

.field private tablerow_ts_title:Landroid/widget/TableRow;

.field private tablerow_ts_value:Landroid/widget/TableRow;

.field private tablethd_inout:Landroid/widget/TableLayout;

.field private final test_off:[B

.field private final test_on:[B

.field private txtHiddenKey1:Landroid/widget/TextView;

.field private txtHiddenKey1_Title:Landroid/widget/TextView;

.field private txtHiddenKey1_label:Landroid/widget/TextView;

.field private txtHiddenKey1_max:Landroid/widget/TextView;

.field private txtHiddenKey1_raw:Landroid/widget/TextView;

.field private txtHiddenKey1_raw_max:Landroid/widget/TextView;

.field private txtHiddenKey2:Landroid/widget/TextView;

.field private txtHiddenKey2_Title:Landroid/widget/TextView;

.field private txtHiddenKey2_label:Landroid/widget/TextView;

.field private txtHiddenKey2_max:Landroid/widget/TextView;

.field private txtHiddenKey2_raw:Landroid/widget/TextView;

.field private txtHiddenKey2_raw_max:Landroid/widget/TextView;

.field private txtMessage:Landroid/widget/TextView;

.field private txtMinMax:Landroid/widget/TextView;

.field private txtRef1:Landroid/widget/TextView;

.field private txtRef1_Title:Landroid/widget/TextView;

.field private txtRef1_max:Landroid/widget/TextView;

.field private txtRef2:Landroid/widget/TextView;

.field private txtRef2_Title:Landroid/widget/TextView;

.field private txtRef2_max:Landroid/widget/TextView;

.field private txtSensitivityTitle:Landroid/widget/TextView;

.field private txtTS_max_label:Landroid/widget/TextView;

.field private txtTitle:Landroid/widget/TextView;

.field private txt_raw_label:Landroid/widget/TextView;

.field private txt_raw_max_label:Landroid/widget/TextView;

.field private txt_ref1_label:Landroid/widget/TextView;

.field private txt_ref1_rawdata:Landroid/widget/TextView;

.field private txt_ref1_rawdata_max:Landroid/widget/TextView;

.field private txt_ref2_label:Landroid/widget/TextView;

.field private txt_ref2_rawdata:Landroid/widget/TextView;

.field private txt_ref2_rawdata_max:Landroid/widget/TextView;

.field private txtbackkey:Landroid/widget/TextView;

.field private txtbackkey_Title:Landroid/widget/TextView;

.field private txtbackkey_idac:Landroid/widget/TextView;

.field private txtbackkey_in:Landroid/widget/TextView;

.field private txtbackkey_label:Landroid/widget/TextView;

.field private txtbackkey_max:Landroid/widget/TextView;

.field private txtbackkey_out:Landroid/widget/TextView;

.field private txtbackkey_raw:Landroid/widget/TextView;

.field private txtbackkey_raw_max:Landroid/widget/TextView;

.field private txtbackkey_ts_in:Landroid/widget/TextView;

.field private txtbackkey_ts_out:Landroid/widget/TextView;

.field private txtbackthd_in:Landroid/widget/TextView;

.field private txtbackthd_invalue:Landroid/widget/TextView;

.field private txtbackthd_label:Landroid/widget/TextView;

.field private txtbackthd_out:Landroid/widget/TextView;

.field private txtbackthd_outvalue:Landroid/widget/TextView;

.field private txtcurrentversion:Landroid/widget/TextView;

.field private txthomekey:Landroid/widget/TextView;

.field private txthomekey_Title:Landroid/widget/TextView;

.field private txthomekey_idac:Landroid/widget/TextView;

.field private txthomekey_in:Landroid/widget/TextView;

.field private txthomekey_label:Landroid/widget/TextView;

.field private txthomekey_max:Landroid/widget/TextView;

.field private txthomekey_out:Landroid/widget/TextView;

.field private txthomekey_raw:Landroid/widget/TextView;

.field private txthomekey_raw_max:Landroid/widget/TextView;

.field private txthomekey_ts_in:Landroid/widget/TextView;

.field private txthomekey_ts_out:Landroid/widget/TextView;

.field private txthomekeybottom:Landroid/widget/TextView;

.field private txthomekeytitle:Landroid/widget/TextView;

.field private txthomethd_in:Landroid/widget/TextView;

.field private txthomethd_invalue:Landroid/widget/TextView;

.field private txthomethd_label:Landroid/widget/TextView;

.field private txthomethd_out:Landroid/widget/TextView;

.field private txthomethd_outvalue:Landroid/widget/TextView;

.field private txtmenukey:Landroid/widget/TextView;

.field private txtmenukey_Title:Landroid/widget/TextView;

.field private txtmenukey_idac:Landroid/widget/TextView;

.field private txtmenukey_in:Landroid/widget/TextView;

.field private txtmenukey_label:Landroid/widget/TextView;

.field private txtmenukey_max:Landroid/widget/TextView;

.field private txtmenukey_out:Landroid/widget/TextView;

.field private txtmenukey_raw:Landroid/widget/TextView;

.field private txtmenukey_raw_max:Landroid/widget/TextView;

.field private txtmenukey_ts_in:Landroid/widget/TextView;

.field private txtmenukey_ts_out:Landroid/widget/TextView;

.field private txtmenuthd_in:Landroid/widget/TextView;

.field private txtmenuthd_invalue:Landroid/widget/TextView;

.field private txtmenuthd_label:Landroid/widget/TextView;

.field private txtmenuthd_out:Landroid/widget/TextView;

.field private txtmenuthd_outvalue:Landroid/widget/TextView;

.field private txtrecentkey:Landroid/widget/TextView;

.field private txtrecentkey_Title:Landroid/widget/TextView;

.field private txtrecentkey_idac:Landroid/widget/TextView;

.field private txtrecentkey_in:Landroid/widget/TextView;

.field private txtrecentkey_label:Landroid/widget/TextView;

.field private txtrecentkey_max:Landroid/widget/TextView;

.field private txtrecentkey_out:Landroid/widget/TextView;

.field private txtrecentkey_raw:Landroid/widget/TextView;

.field private txtrecentkey_raw_max:Landroid/widget/TextView;

.field private txtrecentkey_ts_in:Landroid/widget/TextView;

.field private txtrecentkey_ts_out:Landroid/widget/TextView;

.field private txtrecentthd_in:Landroid/widget/TextView;

.field private txtrecentthd_invalue:Landroid/widget/TextView;

.field private txtrecentthd_label:Landroid/widget/TextView;

.field private txtrecentthd_out:Landroid/widget/TextView;

.field private txtrecentthd_outvalue:Landroid/widget/TextView;

.field private txtrecommendversion:Landroid/widget/TextView;

.field private txtthd_spec:Landroid/widget/TextView;

.field update_button:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/16 v3, -0x100

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    const-string v0, "touch_key_sensitivity_default"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->CLASS_NAME:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    .line 126
    const-string v0, "PATH_NOISE_TOUCH_SENSITIVITY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

    .line 127
    const-string v0, "PATH_THD_SPEC"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_THD_SPEC:Ljava/lang/String;

    .line 128
    const-string v0, "PATH_TSK_VERSION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_VERSION:Ljava/lang/String;

    .line 129
    const-string v0, "PATH_RECOMMEND_VERSION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_RECOMMEND_VERSION:Ljava/lang/String;

    .line 130
    const-string v0, "PATH_TSK_UPDATE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_UPDATE:Ljava/lang/String;

    .line 131
    const-string v0, "PATH_NOISE_TSK_MENU_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU_RAWDATA:Ljava/lang/String;

    .line 132
    const-string v0, "PATH_NOISE_TSK_BACK_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA:Ljava/lang/String;

    .line 133
    const-string v0, "PATH_NOISE_TSK_HOME_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME_RAWDATA:Ljava/lang/String;

    .line 134
    const-string v0, "PATH_NOISE_TSK_RECENT_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA:Ljava/lang/String;

    .line 135
    const-string v0, "PATH_NOISE_TSK_MENU_IDAC"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU_IDAC:Ljava/lang/String;

    .line 136
    const-string v0, "PATH_NOISE_TSK_BACK_IDAC"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC:Ljava/lang/String;

    .line 137
    const-string v0, "PATH_NOISE_TSK_HOME_IDAC"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME_IDAC:Ljava/lang/String;

    .line 138
    const-string v0, "PATH_NOISE_TSK_RECENT_IDAC"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC:Ljava/lang/String;

    .line 139
    const-string v0, "PATH_NOISE_TSK_MENU"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU:Ljava/lang/String;

    .line 140
    const-string v0, "PATH_NOISE_TSK_BACK"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK:Ljava/lang/String;

    .line 141
    const-string v0, "PATH_NOISE_TSK_HOME"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME:Ljava/lang/String;

    .line 142
    const-string v0, "PATH_NOISE_TSK_RECENT"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT:Ljava/lang/String;

    .line 143
    const-string v0, "PATH_AUTOCAL"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_AUTOCAL:Ljava/lang/String;

    .line 144
    const-string v0, "PATH_TSK_BRIGHTESS"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_BRIGHTESS:Ljava/lang/String;

    .line 147
    const-string v0, "PATH_NOISE_TSK_HIDDEN1_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN1_RAWDATA:Ljava/lang/String;

    .line 148
    const-string v0, "PATH_NOISE_TSK_HIDDEN2_RAWDATA"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN2_RAWDATA:Ljava/lang/String;

    .line 149
    const-string v0, "PATH_NOISE_TSK_HIDDEN1"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN1:Ljava/lang/String;

    .line 150
    const-string v0, "PATH_NOISE_TSK_HIDDEN2"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN2:Ljava/lang/String;

    .line 152
    const-string v0, "PATH_NOISE_TSK_BACK_RAWDATA_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_INNER:Ljava/lang/String;

    .line 153
    const-string v0, "PATH_NOISE_TSK_BACK_RAWDATA_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_OUTER:Ljava/lang/String;

    .line 154
    const-string v0, "PATH_NOISE_TSK_RECENT_RAWDATA_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_INNER:Ljava/lang/String;

    .line 155
    const-string v0, "PATH_NOISE_TSK_RECENT_RAWDATA_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_OUTER:Ljava/lang/String;

    .line 156
    const-string v0, "PATH_NOISE_TSK_BACK_IDAC_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC_INNER:Ljava/lang/String;

    .line 157
    const-string v0, "PATH_NOISE_TSK_BACK_IDAC_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC_OUTER:Ljava/lang/String;

    .line 158
    const-string v0, "PATH_NOISE_TSK_RECENT_IDAC_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC_INNER:Ljava/lang/String;

    .line 159
    const-string v0, "PATH_NOISE_TSK_RECENT_IDAC_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC_OUTER:Ljava/lang/String;

    .line 160
    const-string v0, "PATH_NOISE_TSK_BACK_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_INNER:Ljava/lang/String;

    .line 161
    const-string v0, "PATH_NOISE_TSK_BACK_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_OUTER:Ljava/lang/String;

    .line 162
    const-string v0, "PATH_NOISE_TSK_RECENT_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_INNER:Ljava/lang/String;

    .line 163
    const-string v0, "PATH_NOISE_TSK_RECENT_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_OUTER:Ljava/lang/String;

    .line 164
    const-string v0, "PATH_NOISE_TSK_RECENT_THRESHOLD_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_THRESHOLD_INNER:Ljava/lang/String;

    .line 165
    const-string v0, "PATH_NOISE_TSK_RECENT_THRESHOLD_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_THRESHOLD_OUTER:Ljava/lang/String;

    .line 166
    const-string v0, "PATH_NOISE_TSK_BACK_THRESHOLD_INNER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_THRESHOLD_INNER:Ljava/lang/String;

    .line 167
    const-string v0, "PATH_NOISE_TSK_BACK_THRESHOLD_OUTER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_THRESHOLD_OUTER:Ljava/lang/String;

    .line 169
    const-string v0, "PATH_NOISE_TSK_RECENT_RAWDATA_REFERENCE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_REFERENCE:Ljava/lang/String;

    .line 170
    const-string v0, "PATH_NOISE_TSK_BACK_RAWDATA_REFERENCE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_REFERENCE:Ljava/lang/String;

    .line 171
    const-string v0, "PATH_NOISE_TSK_RECENT_REFERENCE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_REFERENCE:Ljava/lang/String;

    .line 172
    const-string v0, "PATH_NOISE_TSK_BACK_REFERENCE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_REFERENCE:Ljava/lang/String;

    .line 183
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    .line 184
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 186
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    .line 187
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    .line 188
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    .line 189
    iput v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    .line 190
    iput v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_SEARCH:I

    .line 191
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_REF1:I

    .line 192
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_REF2:I

    .line 195
    const/16 v0, 0xf6

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HIDDEN1:I

    .line 196
    const/16 v0, 0xf8

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HIDDEN2:I

    .line 203
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    .line 204
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    .line 205
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    .line 206
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    .line 207
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden1:Z

    .line 208
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden2:Z

    .line 209
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    .line 210
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    .line 211
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    .line 212
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    .line 213
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mIsPausedByLcdOff:Z

    .line 214
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mToggleHighsensitivityMode:Z

    .line 215
    new-array v0, v2, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->test_on:[B

    .line 216
    new-array v0, v2, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->test_off:[B

    .line 248
    const/16 v0, 0x18

    new-array v0, v0, [Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    .line 251
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    .line 252
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_MENU:I

    .line 253
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HOME:I

    .line 254
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    .line 255
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    .line 256
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_MENU:I

    .line 257
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HOME:I

    .line 258
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    .line 259
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    .line 260
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_MENU_OUTER:I

    .line 261
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HOME_OUTER:I

    .line 262
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK_OUTER:I

    .line 263
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT_OUTER:I

    .line 264
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_MENU_OUTER:I

    .line 265
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HOME_OUTER:I

    .line 266
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK_OUTER:I

    .line 267
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT_OUTER:I

    .line 269
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN1:I

    .line 270
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN2:I

    .line 271
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN1:I

    .line 272
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN2:I

    .line 274
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF1:I

    .line 275
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF2:I

    .line 276
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF1:I

    .line 277
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->DUMMY:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF2:I

    .line 279
    const-string v0, "IS_HIDDEN_TOUCHKEY"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    .line 1596
    new-instance v0, Lcom/sec/android/app/status/touch_key_sensitivity_default$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default$2;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mTimerHandler:Landroid/os/Handler;

    .line 1638
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawMenuKeyValue:Ljava/lang/String;

    .line 1639
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHomeKeyValue:Ljava/lang/String;

    .line 1640
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    .line 1641
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    .line 1642
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey1Value:Ljava/lang/String;

    .line 1643
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey2Value:Ljava/lang/String;

    .line 1644
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef1Value:Ljava/lang/String;

    .line 1645
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef2Value:Ljava/lang/String;

    .line 1646
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacMenuKeyValue:Ljava/lang/String;

    .line 1647
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacHomeKeyValue:Ljava/lang/String;

    .line 1648
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue:Ljava/lang/String;

    .line 1649
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue:Ljava/lang/String;

    .line 1650
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityMenuKeyValue:Ljava/lang/String;

    .line 1651
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHomeKeyValue:Ljava/lang/String;

    .line 1652
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    .line 1653
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    .line 1654
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey1Value:Ljava/lang/String;

    .line 1655
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey2Value:Ljava/lang/String;

    .line 1656
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef1Value:Ljava/lang/String;

    .line 1657
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef2Value:Ljava/lang/String;

    .line 1658
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawMenuKeyValue_outer:Ljava/lang/String;

    .line 1659
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHomeKeyValue_outer:Ljava/lang/String;

    .line 1660
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue_outer:Ljava/lang/String;

    .line 1661
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue_outer:Ljava/lang/String;

    .line 1662
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacMenuKeyValue_outer:Ljava/lang/String;

    .line 1663
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacHomeKeyValue_outer:Ljava/lang/String;

    .line 1664
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue_outer:Ljava/lang/String;

    .line 1665
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue_outer:Ljava/lang/String;

    .line 1666
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityMenuKeyValue_outer:Ljava/lang/String;

    .line 1667
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHomeKeyValue_outer:Ljava/lang/String;

    .line 1668
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue_outer:Ljava/lang/String;

    .line 1669
    const-string v0, "-"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue_outer:Ljava/lang/String;

    .line 1671
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->SHOW_DATA:I

    .line 1672
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->GET_DATA:I

    .line 1674
    new-instance v0, Lcom/sec/android/app/status/touch_key_sensitivity_default$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default$3;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    return-void

    .line 215
    nop

    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 216
    nop

    :array_1
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/touch_key_sensitivity_default;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->turnOnTSKSensitivity(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/touch_key_sensitivity_default;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mIsPausedByLcdOff:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/touch_key_sensitivity_default;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_UPDATE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/touch_key_sensitivity_default;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/touch_key_sensitivity_default;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->SHOW_DATA:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->showData()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/touch_key_sensitivity_default;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->GET_DATA:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_default;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->readData()V

    return-void
.end method

.method private check_autocal()V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x4

    const/4 v2, 0x0

    .line 1348
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_AUTOCAL:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1349
    .local v0, "result":Ljava/lang/String;
    const-string v1, "touch_key_sensitivity_default"

    const-string v4, "check_autocal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1351
    if-eqz v0, :cond_1

    .line 1352
    const-string v1, "enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    .line 1355
    :cond_1
    const-string v1, "SUPPORT_TSK_PATH_AUTOCAL_SKIP"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1356
    iput-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    .line 1358
    :cond_2
    const-string v1, "touch_key_sensitivity_default"

    const-string v3, "check_autocal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isAutocal  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1360
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-nez v1, :cond_4

    .line 1361
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idac_label:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1362
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1363
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1364
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1365
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1373
    :goto_1
    return-void

    :cond_3
    move v1, v2

    .line 1352
    goto :goto_0

    .line 1367
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idac_label:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1368
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1369
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1370
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private fileExists(I)Z
    .locals 6
    .param p1, "key"    # I

    .prologue
    .line 1458
    const/4 v1, 0x0

    .line 1460
    .local v1, "isExists":Z
    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    if-ne p1, v2, :cond_1

    .line 1461
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1462
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 1486
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    const-string v2, "touch_key_sensitivity_default"

    const-string v3, "fileExists"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "key("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1488
    return v1

    .line 1463
    :cond_1
    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    if-ne p1, v2, :cond_2

    .line 1464
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1465
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0

    .line 1466
    .end local v0    # "file":Ljava/io/File;
    :cond_2
    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    if-ne p1, v2, :cond_3

    .line 1467
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1468
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0

    .line 1469
    .end local v0    # "file":Ljava/io/File;
    :cond_3
    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    if-ne p1, v2, :cond_4

    .line 1470
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1471
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0

    .line 1472
    .end local v0    # "file":Ljava/io/File;
    :cond_4
    const/16 v2, 0xf6

    if-ne p1, v2, :cond_5

    .line 1473
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN1:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1474
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0

    .line 1475
    .end local v0    # "file":Ljava/io/File;
    :cond_5
    const/16 v2, 0xf8

    if-ne p1, v2, :cond_6

    .line 1476
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN2:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1477
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0

    .line 1478
    .end local v0    # "file":Ljava/io/File;
    :cond_6
    const/4 v2, 0x4

    if-ne p1, v2, :cond_7

    .line 1479
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_REFERENCE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1480
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto/16 :goto_0

    .line 1481
    .end local v0    # "file":Ljava/io/File;
    :cond_7
    const/4 v2, 0x5

    if-ne p1, v2, :cond_0

    .line 1482
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_REFERENCE:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1483
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto/16 :goto_0
.end method

.method private fileRead(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "size"    # I

    .prologue
    .line 1403
    const/4 v0, 0x0

    .line 1404
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1407
    .local v2, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4, p2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1408
    .end local v2    # "in":Ljava/io/BufferedReader;
    .local v3, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 1414
    if-nez v0, :cond_0

    .line 1415
    const-string v0, ""

    .line 1418
    :cond_0
    if-eqz v3, :cond_6

    .line 1420
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 1427
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return-object v0

    .line 1421
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 1422
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 1423
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1409
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 1410
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1414
    if-nez v0, :cond_2

    .line 1415
    const-string v0, ""

    .line 1418
    :cond_2
    if-eqz v2, :cond_1

    .line 1420
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1421
    :catch_2
    move-exception v1

    .line 1422
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1411
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 1412
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1414
    if-nez v0, :cond_3

    .line 1415
    const-string v0, ""

    .line 1418
    :cond_3
    if-eqz v2, :cond_1

    .line 1420
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1421
    :catch_4
    move-exception v1

    .line 1422
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1414
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-nez v0, :cond_4

    .line 1415
    const-string v0, ""

    .line 1418
    :cond_4
    if-eqz v2, :cond_5

    .line 1420
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1423
    :cond_5
    :goto_4
    throw v4

    .line 1421
    :catch_5
    move-exception v1

    .line 1422
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1414
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_3

    .line 1411
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1409
    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :cond_6
    move-object v2, v3

    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v2    # "in":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private fileWrite(Ljava/lang/String;[B)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "write"    # [B

    .prologue
    .line 1431
    const/4 v3, 0x1

    .line 1432
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 1435
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1436
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1444
    if-eqz v2, :cond_2

    .line 1446
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1453
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return v3

    .line 1447
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 1448
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 1449
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 1437
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1438
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1439
    const/4 v3, 0x0

    .line 1444
    if-eqz v1, :cond_0

    .line 1446
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1447
    :catch_2
    move-exception v0

    .line 1448
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1440
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1441
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1442
    const/4 v3, 0x0

    .line 1444
    if-eqz v1, :cond_0

    .line 1446
    :try_start_6
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1447
    :catch_4
    move-exception v0

    .line 1448
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1444
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_1

    .line 1446
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1449
    :cond_1
    :goto_4
    throw v4

    .line 1447
    :catch_5
    move-exception v0

    .line 1448
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1444
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1440
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 1437
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :cond_2
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method private getData()V
    .locals 4

    .prologue
    .line 2048
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->GET_DATA:I

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2049
    return-void
.end method

.method private init()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/high16 v11, -0x10000

    const/4 v10, -0x1

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 524
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 525
    .local v2, "pm":Landroid/os/PowerManager;
    const v4, 0x3000001a

    const-string v5, "touch_key_sensitivity_default"

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mWl:Landroid/os/PowerManager$WakeLock;

    .line 529
    :try_start_0
    const-string v4, "window"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v3

    .line 531
    .local v3, "windowManager":Landroid/view/IWindowManager;
    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z

    .line 532
    const/16 v4, 0xbb

    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->getComponentName()Landroid/content/ComponentName;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v3, v4, v5, v6}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 538
    .end local v3    # "windowManager":Landroid/view/IWindowManager;
    :goto_0
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModulePower;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->getTouchLedTime()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mButtonLedTime:I

    .line 539
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModulePower;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->setTouchLedTime(I)V

    .line 541
    const-string v4, "ro.product.model"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 542
    .local v1, "mProduct":Ljava/lang/String;
    const-string v4, "touch_key_sensitivity_default"

    const-string v5, "onClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[ ***** ] ro.product.model ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const v4, 0x7f0a015d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTitle:Landroid/widget/TextView;

    .line 544
    const v4, 0x7f0a0241

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    .line 545
    const v4, 0x7f0a0242

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtcurrentversion:Landroid/widget/TextView;

    .line 546
    const v4, 0x7f0a0243

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecommendversion:Landroid/widget/TextView;

    .line 548
    const v4, 0x7f0a0246

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    .line 549
    const v4, 0x7f0a0248

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    .line 550
    const v4, 0x7f0a024c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    .line 551
    const v4, 0x7f0a0250

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_label:Landroid/widget/TextView;

    .line 552
    const v4, 0x7f0a0254

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_label:Landroid/widget/TextView;

    .line 554
    const v4, 0x7f0a0257

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    .line 555
    const v4, 0x7f0a0258

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    .line 557
    const v4, 0x7f0a0259

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    .line 558
    const v4, 0x7f0a025a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    .line 560
    const v4, 0x7f0a025b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    .line 561
    const v4, 0x7f0a025c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_label:Landroid/widget/TextView;

    .line 562
    const v4, 0x7f0a025d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    .line 563
    const v4, 0x7f0a025e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    .line 564
    const v4, 0x7f0a025f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    .line 565
    const v4, 0x7f0a0260

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    .line 567
    const v4, 0x7f0a0261

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    .line 568
    const v4, 0x7f0a0262

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    .line 570
    const v4, 0x7f0a0263

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    .line 571
    const v4, 0x7f0a0264

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    .line 573
    const v4, 0x7f0a0265

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    .line 574
    const v4, 0x7f0a0266

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_max_label:Landroid/widget/TextView;

    .line 575
    const v4, 0x7f0a0267

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    .line 576
    const v4, 0x7f0a0268

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    .line 577
    const v4, 0x7f0a0269

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    .line 578
    const v4, 0x7f0a026a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    .line 580
    const v4, 0x7f0a026b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    .line 581
    const v4, 0x7f0a026c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    .line 583
    const v4, 0x7f0a026d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    .line 584
    const v4, 0x7f0a026e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    .line 586
    const v4, 0x7f0a0059

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMessage:Landroid/widget/TextView;

    .line 589
    const v4, 0x7f0a026f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    .line 590
    const v4, 0x7f0a0270

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idac_label:Landroid/widget/TextView;

    .line 591
    const v4, 0x7f0a0271

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    .line 592
    const v4, 0x7f0a0272

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    .line 593
    const v4, 0x7f0a0273

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    .line 594
    const v4, 0x7f0a0274

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    .line 596
    const v4, 0x7f0a0276

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtSensitivityTitle:Landroid/widget/TextView;

    .line 597
    const v4, 0x7f0a0277

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMinMax:Landroid/widget/TextView;

    .line 599
    const v4, 0x7f0a0278

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    .line 600
    const v4, 0x7f0a027a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_Title:Landroid/widget/TextView;

    .line 601
    const v4, 0x7f0a027e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_Title:Landroid/widget/TextView;

    .line 602
    const v4, 0x7f0a0282

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_Title:Landroid/widget/TextView;

    .line 603
    const v4, 0x7f0a0286

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_Title:Landroid/widget/TextView;

    .line 605
    const v4, 0x7f0a0289

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    .line 606
    const v4, 0x7f0a028a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    .line 608
    const v4, 0x7f0a028b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    .line 609
    const v4, 0x7f0a028c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    .line 611
    const v4, 0x7f0a028d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    .line 612
    const v4, 0x7f0a028e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    .line 613
    const v4, 0x7f0a028f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    .line 614
    const v4, 0x7f0a0290

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    .line 615
    const v4, 0x7f0a0291

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    .line 617
    const v4, 0x7f0a0292

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    .line 618
    const v4, 0x7f0a0293

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    .line 619
    const v4, 0x7f0a0294

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    .line 620
    const v4, 0x7f0a0295

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    .line 622
    const v4, 0x7f0a0296

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    .line 623
    const v4, 0x7f0a0297

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTS_max_label:Landroid/widget/TextView;

    .line 624
    const v4, 0x7f0a0298

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    .line 625
    const v4, 0x7f0a0299

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    .line 626
    const v4, 0x7f0a029a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    .line 627
    const v4, 0x7f0a029b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    .line 629
    const v4, 0x7f0a029c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    .line 630
    const v4, 0x7f0a029d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    .line 631
    const v4, 0x7f0a029e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    .line 632
    const v4, 0x7f0a029f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    .line 634
    const v4, 0x7f0a0247

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuraw:Landroid/widget/LinearLayout;

    .line 635
    const v4, 0x7f0a024b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homeraw:Landroid/widget/LinearLayout;

    .line 636
    const v4, 0x7f0a024f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backraw:Landroid/widget/LinearLayout;

    .line 637
    const v4, 0x7f0a0253

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentraw:Landroid/widget/LinearLayout;

    .line 639
    const v4, 0x7f0a0279

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_menuraw:Landroid/widget/LinearLayout;

    .line 640
    const v4, 0x7f0a027d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_homeraw:Landroid/widget/LinearLayout;

    .line 641
    const v4, 0x7f0a0281

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_backraw:Landroid/widget/LinearLayout;

    .line 642
    const v4, 0x7f0a0285

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_recentraw:Landroid/widget/LinearLayout;

    .line 644
    const-string v4, "IS_INNER_OUTER_TOUCHKEY"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    .line 646
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_0

    .line 647
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 649
    const v4, 0x7f0a0227

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablethd_inout:Landroid/widget/TableLayout;

    .line 650
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablethd_inout:Landroid/widget/TableLayout;

    invoke-virtual {v4, v9}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 651
    const v4, 0x7f0a0228

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    .line 653
    const v4, 0x7f0a022a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_label:Landroid/widget/TextView;

    .line 654
    const v4, 0x7f0a0230

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_label:Landroid/widget/TextView;

    .line 655
    const v4, 0x7f0a0236

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_label:Landroid/widget/TextView;

    .line 656
    const v4, 0x7f0a023c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_label:Landroid/widget/TextView;

    .line 657
    const v4, 0x7f0a022b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_in:Landroid/widget/TextView;

    .line 658
    const v4, 0x7f0a0231

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_in:Landroid/widget/TextView;

    .line 659
    const v4, 0x7f0a0237

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_in:Landroid/widget/TextView;

    .line 660
    const v4, 0x7f0a023d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_in:Landroid/widget/TextView;

    .line 661
    const v4, 0x7f0a022c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_out:Landroid/widget/TextView;

    .line 662
    const v4, 0x7f0a0232

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_out:Landroid/widget/TextView;

    .line 663
    const v4, 0x7f0a0238

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_out:Landroid/widget/TextView;

    .line 664
    const v4, 0x7f0a023e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_out:Landroid/widget/TextView;

    .line 666
    const v4, 0x7f0a022d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_invalue:Landroid/widget/TextView;

    .line 667
    const v4, 0x7f0a0233

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_invalue:Landroid/widget/TextView;

    .line 668
    const v4, 0x7f0a0239

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_invalue:Landroid/widget/TextView;

    .line 669
    const v4, 0x7f0a023f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_invalue:Landroid/widget/TextView;

    .line 670
    const v4, 0x7f0a022e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_outvalue:Landroid/widget/TextView;

    .line 671
    const v4, 0x7f0a0234

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_outvalue:Landroid/widget/TextView;

    .line 672
    const v4, 0x7f0a023a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_outvalue:Landroid/widget/TextView;

    .line 673
    const v4, 0x7f0a0240

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_outvalue:Landroid/widget/TextView;

    .line 674
    const v4, 0x7f0a0229

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuthd:Landroid/widget/LinearLayout;

    .line 675
    const v4, 0x7f0a022f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homethd:Landroid/widget/LinearLayout;

    .line 676
    const v4, 0x7f0a0235

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backthd:Landroid/widget/LinearLayout;

    .line 677
    const v4, 0x7f0a023b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentthd:Landroid/widget/LinearLayout;

    .line 680
    const v4, 0x7f0a0249

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_in:Landroid/widget/TextView;

    .line 681
    const v4, 0x7f0a024d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_in:Landroid/widget/TextView;

    .line 682
    const v4, 0x7f0a0251

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_in:Landroid/widget/TextView;

    .line 683
    const v4, 0x7f0a0255

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_in:Landroid/widget/TextView;

    .line 684
    const v4, 0x7f0a024a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_out:Landroid/widget/TextView;

    .line 685
    const v4, 0x7f0a024e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_out:Landroid/widget/TextView;

    .line 686
    const v4, 0x7f0a0252

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_out:Landroid/widget/TextView;

    .line 687
    const v4, 0x7f0a0256

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_out:Landroid/widget/TextView;

    .line 688
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 689
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 690
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 691
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 692
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 693
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 694
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 695
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 698
    const v4, 0x7f0a028d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_inout:Landroid/widget/TableRow;

    .line 699
    const v4, 0x7f0a027b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_in:Landroid/widget/TextView;

    .line 700
    const v4, 0x7f0a027f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_in:Landroid/widget/TextView;

    .line 701
    const v4, 0x7f0a0283

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_in:Landroid/widget/TextView;

    .line 702
    const v4, 0x7f0a0287

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_in:Landroid/widget/TextView;

    .line 703
    const v4, 0x7f0a027c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_out:Landroid/widget/TextView;

    .line 704
    const v4, 0x7f0a0280

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_out:Landroid/widget/TextView;

    .line 705
    const v4, 0x7f0a0284

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_out:Landroid/widget/TextView;

    .line 706
    const v4, 0x7f0a0288

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_out:Landroid/widget/TextView;

    .line 707
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 708
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 709
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 710
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 711
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 712
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 713
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 714
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 717
    :cond_0
    const-string v4, "KEY_TEST_APP_RECENT"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 718
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 719
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 720
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 721
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 722
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 723
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 725
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 726
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 728
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 729
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_recentraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 730
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 731
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 732
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 733
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 735
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_1

    .line 736
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 737
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentthd:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_APP_RECENT"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 741
    :cond_1
    const-string v4, "KEY_TEST_BACK"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 742
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 743
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 744
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 745
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 746
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 747
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 749
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 750
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 752
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_backraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 753
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_backraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 754
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 755
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 756
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 757
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 759
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_2

    .line 760
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 761
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backthd:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_BACK"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 765
    :cond_2
    const-string v4, "KEY_TEST_MENU"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 766
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 767
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 768
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 769
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 770
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 771
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 773
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 774
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 776
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_menuraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 777
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_menuraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 778
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 779
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 780
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 781
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 783
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_3

    .line 784
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 785
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuthd:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_MENU"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 789
    :cond_3
    const-string v4, "KEY_TEST_HOME"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 790
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 791
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 792
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 793
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 794
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 795
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 797
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 798
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_idac:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 800
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_homeraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 801
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_homeraw:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 802
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 803
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 804
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 805
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 807
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_4

    .line 808
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homethd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 809
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_thd:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homethd:Landroid/widget/LinearLayout;

    const-string v6, "KEY_TEST_HOME"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 813
    :cond_4
    const-string v4, "GT-I9300"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 814
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMinMax:Landroid/widget/TextView;

    const v5, 0x7f070176

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 819
    :goto_1
    const-string v4, "SCH-I415"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "KEY_TEST_APP_RECENT"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 820
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 821
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 822
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 823
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 824
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 825
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 826
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_5

    .line 827
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 828
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 829
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 833
    :cond_5
    const-string v4, "KEY_TEST_HIDDEN1"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 834
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 835
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 837
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 838
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 840
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 841
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 843
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 844
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 846
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 847
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 849
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 850
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 853
    :cond_6
    const-string v4, "KEY_TEST_HIDDEN2"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 854
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 855
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 857
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 858
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 860
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 861
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 863
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 864
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 866
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 867
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 869
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 870
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_HIDDEN2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 875
    :cond_7
    const-string v4, "KEY_TEST_REFERENCE1"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 876
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 877
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 879
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 880
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 882
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 883
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 885
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 886
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 888
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 889
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 891
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 892
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE1"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 895
    :cond_8
    const-string v4, "KEY_TEST_REFERENCE2"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getEnabled(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 896
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 897
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 899
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 900
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 902
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 903
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_raw_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 905
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 906
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_title:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 908
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 909
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_value:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 911
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TableRow;->removeView(Landroid/view/View;)V

    .line 912
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->tablerow_ts_max:Landroid/widget/TableRow;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    const-string v6, "KEY_TEST_REFERENCE2"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestOrder(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TableRow;->addView(Landroid/view/View;I)V

    .line 915
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->check_autocal()V

    .line 920
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    .line 921
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    .line 922
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    .line 923
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    .line 924
    const/16 v4, 0xf6

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden1:Z

    .line 925
    const/16 v4, 0xf8

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden2:Z

    .line 926
    const/4 v4, 0x4

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    .line 927
    const/4 v4, 0x5

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileExists(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    .line 929
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    if-nez v4, :cond_a

    .line 930
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 931
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 932
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 933
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 934
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 935
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 936
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 937
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 938
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_menuraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 939
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_a

    .line 940
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_menuthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 941
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 942
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 943
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 944
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 948
    :cond_a
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    if-nez v4, :cond_b

    .line 949
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 950
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 951
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 952
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 953
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 954
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 955
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 956
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homeraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_homeraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 958
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_b

    .line 959
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_homethd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 960
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 961
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 962
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 963
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 967
    :cond_b
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    if-nez v4, :cond_c

    .line 968
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 969
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 970
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 971
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 972
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 973
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 974
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 975
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 976
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_backraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 977
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_c

    .line 978
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_backthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 979
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 980
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 981
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 982
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 986
    :cond_c
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    if-nez v4, :cond_d

    .line 987
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 988
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 989
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 990
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 991
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 992
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 993
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 994
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 995
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_ts_recentraw:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 996
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v4, :cond_d

    .line 997
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->linear_recentthd:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 998
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 999
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1000
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1005
    :cond_d
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden1:Z

    if-nez v4, :cond_e

    .line 1006
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1007
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1008
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1009
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1010
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1011
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1014
    :cond_e
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHidden2:Z

    if-nez v4, :cond_f

    .line 1015
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1016
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1017
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1018
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1019
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1020
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1023
    :cond_f
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    if-nez v4, :cond_10

    .line 1024
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1025
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1026
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1027
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1028
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1029
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1032
    :cond_10
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    if-nez v4, :cond_11

    .line 1033
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1034
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1035
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1036
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1037
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1038
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1041
    :cond_11
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->showTHD()V

    .line 1043
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_RECOMMEND_VERSION:Ljava/lang/String;

    const/4 v5, 0x4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/status/touch_key_sensitivity_default;->recommand_Version:Ljava/lang/String;

    .line 1044
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecommendversion:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Phone\'s Touch ket FW version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/status/touch_key_sensitivity_default;->recommand_Version:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1045
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_VERSION:Ljava/lang/String;

    const/4 v5, 0x4

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/status/touch_key_sensitivity_default;->current_Version:Ljava/lang/String;

    .line 1046
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtcurrentversion:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Part\'s Touch key FW version : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/status/touch_key_sensitivity_default;->current_Version:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1047
    const v4, 0x7f0a0244

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->update_button:Landroid/widget/Button;

    .line 1048
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->update_button:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1049
    const v4, 0x7f0a0245

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->exit_button:Landroid/widget/Button;

    .line 1050
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->exit_button:Landroid/widget/Button;

    invoke-virtual {v4, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1051
    const v4, 0x7f0a0275

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ToggleButton;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    .line 1052
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    invoke-virtual {v4, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1053
    const-string v4, "PATH_HIGH_SENSITIVITY_MODE"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1054
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    invoke-virtual {v4, v9}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 1056
    :cond_12
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1057
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1058
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1059
    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1060
    const/16 v4, 0xf6

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1061
    const/16 v4, 0xf8

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1062
    const/4 v4, 0x4

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1063
    const/4 v4, 0x5

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 1065
    const-string v4, "SCH-I939"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, "SC-06D"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_13

    const-string v4, "SGH-T699"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1072
    :cond_13
    const v4, 0x7f0a027e

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekeybottom:Landroid/widget/TextView;

    .line 1073
    const v4, 0x7f0a024c

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekeytitle:Landroid/widget/TextView;

    .line 1074
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekeybottom:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1075
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekeytitle:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1076
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1077
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1078
    iput-boolean v9, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    .line 1081
    :cond_14
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    if-ne v4, v12, :cond_16

    .line 1082
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1087
    :goto_2
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    if-ne v4, v12, :cond_17

    .line 1088
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1093
    :goto_3
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    if-ne v4, v12, :cond_18

    .line 1094
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1099
    :goto_4
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    if-ne v4, v12, :cond_19

    .line 1100
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1105
    :goto_5
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-eqz v4, :cond_1a

    .line 1106
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1107
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1113
    :goto_6
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    if-ne v4, v12, :cond_1b

    .line 1114
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1119
    :goto_7
    iget-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    if-ne v4, v12, :cond_1c

    .line 1120
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1124
    :goto_8
    return-void

    .line 533
    .end local v1    # "mProduct":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "touch_key_sensitivity_default"

    const-string v5, "onClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Home Key Block Exception"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 816
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "mProduct":Ljava/lang/String;
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMinMax:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 1084
    :cond_16
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 1090
    :cond_17
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 1096
    :cond_18
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_4

    .line 1102
    :cond_19
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_5

    .line 1109
    :cond_1a
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1110
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_6

    .line 1116
    :cond_1b
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_7

    .line 1122
    :cond_1c
    iget-object v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_8
.end method

.method private initMinMaxIfon()V
    .locals 4

    .prologue
    .line 1289
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1290
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    new-instance v2, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_default$1;)V

    aput-object v2, v1, v0

    .line 1289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1292
    :cond_0
    return-void
.end method

.method public static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1376
    const/4 v3, 0x0

    .line 1377
    .local v3, "value":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1380
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1382
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    if-eqz v2, :cond_0

    .line 1383
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 1384
    if-eqz v3, :cond_0

    .line 1385
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1390
    :cond_0
    if-eqz v2, :cond_3

    .line 1392
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1399
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return-object v3

    .line 1393
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 1394
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 1395
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1387
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1388
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1390
    if-eqz v1, :cond_1

    .line 1392
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1393
    :catch_2
    move-exception v0

    .line 1394
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1390
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_2

    .line 1392
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1395
    :cond_2
    :goto_3
    throw v4

    .line 1393
    :catch_3
    move-exception v0

    .line 1394
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1390
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1387
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private readData()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1845
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    if-eqz v0, :cond_2

    .line 1847
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawMenuKeyValue:Ljava/lang/String;

    .line 1849
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawMenuKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1850
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_MENU:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawMenuKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1854
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_1

    .line 1855
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU_IDAC:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacMenuKeyValue:Ljava/lang/String;

    .line 1859
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_MENU:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityMenuKeyValue:Ljava/lang/String;

    .line 1861
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityMenuKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1862
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_MENU:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityMenuKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1866
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    if-eqz v0, :cond_5

    .line 1868
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHomeKeyValue:Ljava/lang/String;

    .line 1870
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHomeKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1871
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HOME:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHomeKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1875
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_4

    .line 1876
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME_IDAC:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacHomeKeyValue:Ljava/lang/String;

    .line 1880
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HOME:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHomeKeyValue:Ljava/lang/String;

    .line 1882
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHomeKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1883
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HOME:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHomeKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1887
    :cond_5
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    if-eqz v0, :cond_a

    .line 1888
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v0, :cond_18

    .line 1890
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    .line 1891
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue_outer:Ljava/lang/String;

    .line 1893
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1894
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1896
    :cond_6
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1897
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK_OUTER:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue_outer:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1901
    :cond_7
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_8

    .line 1902
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue:Ljava/lang/String;

    .line 1903
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue_outer:Ljava/lang/String;

    .line 1907
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    .line 1908
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue_outer:Ljava/lang/String;

    .line 1910
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1911
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1913
    :cond_9
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1914
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK_OUTER:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue_outer:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1938
    :cond_a
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    if-eqz v0, :cond_f

    .line 1939
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v0, :cond_1b

    .line 1941
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    .line 1942
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue_outer:Ljava/lang/String;

    .line 1944
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1945
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1947
    :cond_b
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1948
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT_OUTER:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue_outer:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1952
    :cond_c
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_d

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue:Ljava/lang/String;

    .line 1954
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue_outer:Ljava/lang/String;

    .line 1957
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_INNER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_OUTER:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue_outer:Ljava/lang/String;

    .line 1960
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1961
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1963
    :cond_e
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1964
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT_OUTER:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue_outer:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1988
    :cond_f
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-eqz v0, :cond_13

    .line 1990
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN1_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey1Value:Ljava/lang/String;

    .line 1992
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey1Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1993
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN1:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey1Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1996
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN1:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey1Value:Ljava/lang/String;

    .line 1998
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey1Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1999
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN1:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey1Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2003
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN2_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey2Value:Ljava/lang/String;

    .line 2005
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey2Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 2006
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN2:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawHiddenKey2Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2009
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_HIDDEN2:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey2Value:Ljava/lang/String;

    .line 2011
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey2Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 2012
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN2:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityHiddenKey2Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2016
    :cond_13
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    if-eqz v0, :cond_15

    .line 2018
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA_REFERENCE:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef1Value:Ljava/lang/String;

    .line 2020
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef1Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 2021
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF1:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef1Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2025
    :cond_14
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_REFERENCE:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef1Value:Ljava/lang/String;

    .line 2026
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef1Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 2027
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF1:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef1Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2031
    :cond_15
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    if-eqz v0, :cond_17

    .line 2033
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA_REFERENCE:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef2Value:Ljava/lang/String;

    .line 2035
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef2Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 2036
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF2:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRef2Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2040
    :cond_16
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_REFERENCE:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef2Value:Ljava/lang/String;

    .line 2041
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef2Value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 2042
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF2:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRef2Value:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 2045
    :cond_17
    return-void

    .line 1918
    :cond_18
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    .line 1920
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1921
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawBackKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1925
    :cond_19
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_1a

    .line 1926
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_IDAC:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue:Ljava/lang/String;

    .line 1930
    :cond_1a
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    .line 1932
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityBackKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1968
    :cond_1b
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_RAWDATA:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    .line 1970
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1971
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->rawRecentKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    .line 1975
    :cond_1c
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_1d

    .line 1976
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_IDAC:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue:Ljava/lang/String;

    .line 1979
    :cond_1d
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    .line 1981
    const-string v0, ""

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1982
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->sensitivityRecentKeyValue:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->setData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private setUIRate()V
    .locals 4

    .prologue
    .line 1127
    const-string v2, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 1128
    .local v1, "model":Ljava/lang/String;
    const-string v2, "ro.product.device"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1130
    .local v0, "device":Ljava/lang/String;
    const-string v2, "prevail2spr"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "sph-m830"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "prevail2spr"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "sph-m830"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1132
    :cond_0
    const v2, 0x3f333333    # 0.7f

    invoke-direct {p0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->setUIRate(F)V

    .line 1136
    :goto_0
    return-void

    .line 1134
    :cond_1
    const-string v2, "TOUCH_KEY_DEFAULT_SCALING"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->setUIRate(F)V

    goto :goto_0
.end method

.method private setUIRate(F)V
    .locals 3
    .param p1, "rate"    # F

    .prologue
    const/4 v2, 0x0

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1142
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtcurrentversion:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtcurrentversion:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1145
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecommendversion:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecommendversion:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1148
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->update_button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->update_button:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1149
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->exit_button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->exit_button:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1150
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/ToggleButton;->setTextSize(IF)V

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1156
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1158
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1162
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1164
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1166
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1167
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1168
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1174
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_max_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_max_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1182
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1184
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1186
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1189
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMessage:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1191
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idac_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idac_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1192
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1194
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1201
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtSensitivityTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtSensitivityTitle:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1203
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMinMax:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtMinMax:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1208
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1210
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1212
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1214
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1216
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1219
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1220
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1221
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1222
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTS_max_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtTS_max_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1224
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1225
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1226
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1227
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1229
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1231
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1233
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1234
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1235
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1236
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1239
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1240
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1241
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1242
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_Title:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1246
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v0, :cond_0

    .line 1247
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1250
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_label:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_label:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1251
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1253
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1255
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1256
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1258
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1259
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_invalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_invalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1260
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_invalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_invalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1261
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_invalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_invalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1262
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_invalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_invalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1263
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_outvalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenuthd_outvalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1264
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_outvalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomethd_outvalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1265
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_outvalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_outvalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_outvalue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_outvalue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1268
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1269
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1271
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1272
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1273
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1274
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1275
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1277
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1278
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1279
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_in:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_in:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1284
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_out:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_ts_out:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    mul-float/2addr v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1286
    :cond_0
    return-void
.end method

.method private showData()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1717
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    if-ne v0, v4, :cond_1

    .line 1719
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_MENU:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1720
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_MENU:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1723
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_0

    .line 1724
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacMenuKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1728
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_MENU:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1729
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_MENU:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1732
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    if-ne v0, v4, :cond_3

    .line 1734
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HOME:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1735
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HOME:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1738
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_2

    .line 1739
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacHomeKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1743
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HOME:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1744
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HOME:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1747
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    if-ne v0, v4, :cond_5

    .line 1748
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v0, :cond_b

    .line 1750
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1754
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_4

    .line 1755
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1759
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1777
    :cond_5
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    if-ne v0, v4, :cond_7

    .line 1778
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v0, :cond_d

    .line 1780
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1781
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1784
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_6

    .line 1785
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue_outer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1788
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1789
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "          "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT_OUTER:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1805
    :cond_7
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-eqz v0, :cond_8

    .line 1807
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1812
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1815
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1816
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_HIDDEN2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1819
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1820
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_HIDDEN2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1823
    :cond_8
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    if-ne v0, v4, :cond_9

    .line 1825
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1826
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref1_rawdata_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1829
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1830
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF1:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833
    :cond_9
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    if-ne v0, v4, :cond_a

    .line 1835
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1836
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_ref2_rawdata_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_REF2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1839
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1840
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_REF2:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1842
    :cond_a
    return-void

    .line 1763
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1764
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1767
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_c

    .line 1768
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacBackKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1772
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_BACK:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1792
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1793
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_raw_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_RAWDATA_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1796
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isAutocal:Z

    if-eqz v0, :cond_e

    .line 1797
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_idac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->idacRecentKeyValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1800
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mData:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1801
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey_max:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mMinMax:[Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->INDEX_SENSITIVITY_RECENT:I

    aget-object v2, v2, v3

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->mMax:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_default$MinMax;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private showTHD()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v3, 0x8

    .line 1689
    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1691
    .local v0, "mProduct":Ljava/lang/String;
    const-string v1, "SM-W2014"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1692
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    const-string v2, "THD Spec: 9 "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1693
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_label:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1694
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_label:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1695
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_label:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1696
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_label:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1697
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1698
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1699
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1700
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txt_raw_max_label:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1701
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1702
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1703
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey_raw_max:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1714
    :goto_0
    return-void

    .line 1705
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isInOut:Z

    if-eqz v1, :cond_1

    .line 1706
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_invalue:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_THRESHOLD_INNER:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1707
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackthd_outvalue:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_BACK_THRESHOLD_OUTER:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1708
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_invalue:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_THRESHOLD_INNER:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1709
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentthd_outvalue:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TSK_RECENT_THRESHOLD_OUTER:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1711
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtthd_spec:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "THD Spec : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_THD_SPEC:Ljava/lang/String;

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileRead(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private turnOnTSKSensitivity(Z)V
    .locals 10
    .param p1, "isOn"    # Z

    .prologue
    const/4 v7, 0x2

    .line 1295
    new-instance v3, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1296
    .local v3, "file":Ljava/io/File;
    const/4 v0, 0x5

    .line 1298
    .local v0, "count":I
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1299
    if-eqz p1, :cond_2

    .line 1300
    new-array v5, v7, [B

    fill-array-data v5, :array_0

    .line 1306
    .local v5, "test_on":[B
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

    invoke-direct {p0, v6, v5}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileWrite(Ljava/lang/String;[B)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1308
    :try_start_0
    const-string v6, "touch_key_sensitivity_default"

    const-string v7, "turnOnTSKSensitivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "retry(100ms)-remain count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "count":I
    .local v1, "count":I
    :try_start_1
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    move v0, v1

    .line 1315
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :goto_0
    if-gez v0, :cond_0

    .line 1342
    .end local v5    # "test_on":[B
    :cond_1
    :goto_1
    return-void

    .line 1311
    .restart local v5    # "test_on":[B
    :catch_0
    move-exception v2

    .line 1312
    .local v2, "e":Ljava/lang/InterruptedException;
    :goto_2
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1320
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v5    # "test_on":[B
    :cond_2
    new-array v4, v7, [B

    fill-array-data v4, :array_1

    .line 1323
    .local v4, "test_off":[B
    const-string v6, "touch_key_sensitivity_default"

    const-string v7, "turnOnTSKSensitivity"

    const-string v8, "Turn off TSK Sensitivity"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_NOISE_TOUCH_SENSITIVITY:Ljava/lang/String;

    invoke-direct {p0, v6, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileWrite(Ljava/lang/String;[B)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1327
    :try_start_2
    const-string v6, "touch_key_sensitivity_default"

    const-string v7, "turnOnTSKSensitivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "retry(100ms)-remain count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v8

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "count":I
    .restart local v1    # "count":I
    :try_start_3
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    move v0, v1

    .line 1334
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :goto_3
    if-gez v0, :cond_3

    goto :goto_1

    .line 1330
    :catch_1
    move-exception v2

    .line 1331
    .restart local v2    # "e":Ljava/lang/InterruptedException;
    :goto_4
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 1340
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v4    # "test_off":[B
    :cond_4
    const-string v6, "touch_key_sensitivity_default"

    const-string v7, "turnOnTSKSensitivity"

    const-string v8, "File does not exist"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1330
    .end local v0    # "count":I
    .restart local v1    # "count":I
    .restart local v4    # "test_off":[B
    :catch_2
    move-exception v2

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_4

    .line 1311
    .end local v0    # "count":I
    .end local v4    # "test_off":[B
    .restart local v1    # "count":I
    .restart local v5    # "test_on":[B
    :catch_3
    move-exception v2

    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_2

    .line 1300
    :array_0
    .array-data 1
        0x31t
        0x0t
    .end array-data

    .line 1320
    nop

    :array_1
    .array-data 1
        0x30t
        0x0t
    .end array-data
.end method

.method private updateKey(I)V
    .locals 6
    .param p1, "key"    # I

    .prologue
    const/4 v5, 0x1

    const/high16 v4, -0x10000

    const/16 v3, -0x100

    .line 1492
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    if-ne p1, v1, :cond_3

    .line 1493
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsMenu:Z

    if-ne v1, v5, :cond_2

    .line 1494
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    if-ne v1, v3, :cond_1

    .line 1495
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    .line 1500
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1594
    :cond_0
    :goto_1
    return-void

    .line 1497
    :cond_1
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    goto :goto_0

    .line 1502
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1504
    :cond_3
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    if-ne p1, v1, :cond_6

    .line 1505
    const-string v1, "ro.product.model"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1507
    .local v0, "mProduct":Ljava/lang/String;
    const-string v1, "SCH-I939"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-06D"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SGH-T699"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1509
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsHome:Z

    if-ne v1, v5, :cond_5

    .line 1510
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    if-ne v1, v3, :cond_4

    .line 1511
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 1516
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 1513
    :cond_4
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    goto :goto_2

    .line 1518
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1521
    .end local v0    # "mProduct":Ljava/lang/String;
    :cond_6
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    if-ne p1, v1, :cond_9

    .line 1522
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsBack:Z

    if-ne v1, v5, :cond_8

    .line 1523
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    if-ne v1, v3, :cond_7

    .line 1524
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 1529
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 1526
    :cond_7
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    goto :goto_3

    .line 1531
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1533
    :cond_9
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    if-ne p1, v1, :cond_c

    .line 1534
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRecent:Z

    if-ne v1, v5, :cond_b

    .line 1535
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    if-ne v1, v3, :cond_a

    .line 1536
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 1541
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1538
    :cond_a
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    goto :goto_4

    .line 1543
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1545
    :cond_c
    const/16 v1, 0xf6

    if-ne p1, v1, :cond_f

    .line 1546
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-ne v1, v5, :cond_e

    .line 1547
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    if-ne v1, v3, :cond_d

    .line 1548
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 1553
    :goto_5
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1550
    :cond_d
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    goto :goto_5

    .line 1555
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1557
    :cond_f
    const/16 v1, 0xf8

    if-ne p1, v1, :cond_12

    .line 1558
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-ne v1, v5, :cond_11

    .line 1559
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    if-ne v1, v3, :cond_10

    .line 1560
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    .line 1565
    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1562
    :cond_10
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->bKey_Color:I

    goto :goto_6

    .line 1567
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1569
    :cond_12
    const/4 v1, 0x4

    if-ne p1, v1, :cond_15

    .line 1570
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef1:Z

    if-ne v1, v5, :cond_14

    .line 1571
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    if-ne v1, v3, :cond_13

    .line 1572
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    .line 1577
    :goto_7
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1574
    :cond_13
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    goto :goto_7

    .line 1579
    :cond_14
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1581
    :cond_15
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    .line 1582
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->isExistsRef2:Z

    if-ne v1, v5, :cond_17

    .line 1583
    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    if-ne v1, v3, :cond_16

    .line 1584
    iput v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    .line 1589
    :goto_8
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 1586
    :cond_16
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mKey_Color:I

    goto :goto_8

    .line 1591
    :cond_17
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    const-string v2, "Not Support"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    .line 435
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 494
    :goto_0
    return-void

    .line 437
    :sswitch_0
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "onClick"

    const-string v5, "Button F/W update"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    sget-object v3, Lcom/sec/android/app/status/touch_key_sensitivity_default;->current_Version:Ljava/lang/String;

    sget-object v4, Lcom/sec/android/app/status/touch_key_sensitivity_default;->recommand_Version:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 440
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;

    const-string v4, "Current Version equals Recommended Version"

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 442
    :cond_0
    sget-object v3, Lcom/sec/android/app/status/touch_key_sensitivity_default;->current_Version:Ljava/lang/String;

    const-string v4, "0x"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    sget-object v4, Lcom/sec/android/app/status/touch_key_sensitivity_default;->recommand_Version:Ljava/lang/String;

    const-string v5, "0x"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 443
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;

    const-string v4, "Starting TouchKey Firmware Update..\nPlease Wait about 15 seconds"

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 447
    const/4 v1, 0x0

    .line 450
    .local v1, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_UPDATE:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    .end local v1    # "out":Ljava/io/FileWriter;
    .local v2, "out":Ljava/io/FileWriter;
    if-eqz v2, :cond_1

    .line 452
    const/16 v3, 0x53

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 457
    :cond_1
    if-eqz v2, :cond_6

    .line 459
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 466
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v3, v7, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 460
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 462
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_1

    .line 453
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 455
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "onClick"

    const-string v5, "File open error"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 457
    if-eqz v1, :cond_2

    .line 459
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 460
    :catch_2
    move-exception v0

    .line 461
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 457
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_3
    if-eqz v1, :cond_3

    .line 459
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 462
    :cond_3
    :goto_4
    throw v3

    .line 460
    :catch_3
    move-exception v0

    .line 461
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 469
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileWriter;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;

    const-string v4, "IC\'s F/W is higher than Bin\'s F/W"

    invoke-static {v3, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 474
    :sswitch_1
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "onClick"

    const-string v5, "Button High Sensitivity Mode"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mHighSensitivity_Toggle:Landroid/widget/ToggleButton;

    invoke-virtual {v3}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 476
    iput-boolean v7, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mToggleHighsensitivityMode:Z

    .line 477
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "1"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->setHighSensitivityModeOnOff(Ljava/lang/String;)V

    .line 483
    :goto_5
    const-wide/16 v4, 0x1f4

    :try_start_6
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4

    .line 487
    :goto_6
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->showTHD()V

    goto/16 :goto_0

    .line 479
    :cond_5
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mToggleHighsensitivityMode:Z

    .line 480
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "0"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->setHighSensitivityModeOnOff(Ljava/lang/String;)V

    goto :goto_5

    .line 484
    :catch_4
    move-exception v0

    .line 485
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_6

    .line 490
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :sswitch_2
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "onClick"

    const-string v5, "Button Exit"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->finish()V

    goto/16 :goto_0

    .line 457
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_3

    .line 453
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_5
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :cond_6
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto/16 :goto_1

    .line 435
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0244 -> :sswitch_0
        0x7f0a0245 -> :sswitch_2
        0x7f0a0275 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 286
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 288
    const v0, 0x7f030069

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->setContentView(I)V

    .line 289
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iput-object p0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;

    .line 291
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    .line 294
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-eqz v0, :cond_0

    .line 295
    const-string v0, "TSK_FACTORY_MODE"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 296
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onCreate"

    const-string v2, "TSK_FACTORY_MODE : 1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->init()V

    .line 300
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->setUIRate()V

    .line 301
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->initMinMaxIfon()V

    .line 302
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 352
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mEnabledDummyKey:Z

    if-eqz v0, :cond_0

    .line 353
    const-string v0, "TSK_FACTORY_MODE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 354
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onCreate"

    const-string v2, "TSK_FACTORY_MODE : 0"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    :cond_0
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onDestroy"

    const-string v2, "onDestory"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModulePower;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mButtonLedTime:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->setTouchLedTime(I)V

    .line 360
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 361
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 400
    sparse-switch p1, :sswitch_data_0

    .line 429
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 402
    :sswitch_0
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Menu Key"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_MENU:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 406
    :sswitch_1
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Home Key"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_HOME:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 410
    :sswitch_2
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Back Key"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_BACK:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 412
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 415
    :sswitch_3
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Recent Key"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->KEY_RECENT:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    .line 417
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 420
    :sswitch_4
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Hidden Key 1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const/16 v0, 0xf6

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 424
    :sswitch_5
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyDown"

    const-string v2, "Hidden Key 2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const/16 v0, 0xf8

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->updateKey(I)V

    goto :goto_0

    .line 400
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_2
        0x52 -> :sswitch_0
        0xbb -> :sswitch_3
        0xf6 -> :sswitch_4
        0xf8 -> :sswitch_5
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, -0x1

    .line 366
    sparse-switch p1, :sswitch_data_0

    .line 394
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 368
    :sswitch_0
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onKeyUp"

    const-string v2, "Enter Key => Home Key"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 373
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtmenukey:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 376
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txthomekey:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 379
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtbackkey:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 383
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtrecentkey:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtRef1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 387
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey1:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 390
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->txtHiddenKey2:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 366
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x18 -> :sswitch_0
        0x52 -> :sswitch_1
        0xbb -> :sswitch_4
        0xf6 -> :sswitch_5
        0xf8 -> :sswitch_6
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 330
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->GET_DATA:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->GET_DATA:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->SHOW_DATA:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->SHOW_DATA:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 340
    :cond_1
    const-string v0, "PATH_HIGH_SENSITIVITY_MODE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mToggleHighsensitivityMode:Z

    if-ne v0, v4, :cond_2

    .line 341
    iput-boolean v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mToggleHighsensitivityMode:Z

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->setHighSensitivityModeOnOff(Ljava/lang/String;)V

    .line 344
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->turnOnTSKSensitivity(Z)V

    .line 345
    iput-boolean v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mIsPausedByLcdOff:Z

    .line 347
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 348
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 307
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 308
    const-string v0, "touch_key_sensitivity_default"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 310
    const-string v0, "IS_NEED_TURNON_LEDKEY"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_BRIGHTESS:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->test_on:[B

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->fileWrite(Ljava/lang/String;[B)Z

    .line 313
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->turnOnTSKSensitivity(Z)V

    .line 314
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->getData()V

    .line 317
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->mIsPausedByLcdOff:Z

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/status/touch_key_sensitivity_default$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/touch_key_sensitivity_default$1;-><init>(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 325
    :cond_1
    return-void
.end method
