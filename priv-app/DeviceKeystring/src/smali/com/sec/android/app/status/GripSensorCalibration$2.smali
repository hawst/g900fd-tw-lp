.class Lcom/sec/android/app/status/GripSensorCalibration$2;
.super Landroid/os/Handler;
.source "GripSensorCalibration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibration;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorCalibration;)V
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private readCalibration()V
    .locals 4

    .prologue
    .line 512
    const/4 v0, 0x0

    .line 513
    .local v0, "RAWCount":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const-string v3, "/sys/class/sensors/grip_sensor/raw_data"

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1400(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1302(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1300(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1300(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 520
    :goto_0
    if-eqz v0, :cond_0

    array-length v1, v0

    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    .line 521
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/4 v2, 0x4

    aget-object v2, v0, v2

    # setter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibration;->access$802(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/16 v2, 0x9

    aget-object v2, v0, v2

    # setter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1002(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;

    .line 525
    :cond_0
    return-void

    .line 517
    :cond_1
    const-string v1, "GripSensorCalibration"

    const-string v2, "UpdateCSPercent"

    const-string v3, "mCspercent_data null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 419
    const/4 v5, 0x0

    .line 420
    .local v5, "result":Z
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 508
    :goto_0
    return-void

    .line 422
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/16 v7, 0x31

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->calibration(B)Z
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibration;->access$300(Lcom/sec/android/app/status/GripSensorCalibration;B)Z

    move-result v5

    .line 423
    if-nez v5, :cond_0

    .line 424
    const-string v6, "GripSensorCalibration"

    const-string v7, "onClick"

    const-string v8, "gripsensor_cal_fail"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    :cond_0
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->UpdateGripdata()V
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$400(Lcom/sec/android/app/status/GripSensorCalibration;)V

    .line 434
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibration$2;->readCalibration()V

    .line 436
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3e8

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 438
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3eb

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 429
    :catch_0
    move-exception v2

    .line 430
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 441
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/16 v7, 0x30

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->calibration(B)Z
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibration;->access$300(Lcom/sec/android/app/status/GripSensorCalibration;B)Z

    move-result v5

    .line 442
    if-nez v5, :cond_1

    .line 443
    const-string v6, "GripSensorCalibration"

    const-string v7, "onClick"

    const-string v8, "gripsensor_calerase_fail"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 447
    :cond_1
    const-wide/16 v6, 0x1f4

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 451
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->UpdateGripdata()V
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$400(Lcom/sec/android/app/status/GripSensorCalibration;)V

    .line 453
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3e8

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 455
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3eb

    const-wide/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 448
    :catch_1
    move-exception v2

    .line 449
    .restart local v2    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 458
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const-string v7, "/sys/class/sensors/grip_sensor/reset"

    const/16 v8, 0x31

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$600(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;B)Z

    goto/16 :goto_0

    .line 461
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->toggleButtonClickable(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibration;->access$700(Lcom/sec/android/app/status/GripSensorCalibration;Z)V

    .line 462
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_PROGRESS"

    const-string v8, "Doing Cal.."

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 466
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const/4 v7, 0x1

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->toggleButtonClickable(Z)V
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibration;->access$700(Lcom/sec/android/app/status/GripSensorCalibration;Z)V

    goto/16 :goto_0

    .line 470
    :pswitch_5
    const/4 v1, 0x0

    .line 471
    .local v1, "csPass":Z
    const/4 v0, 0x0

    .line 473
    .local v0, "crPass":Z
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 474
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    const/16 v7, 0x28

    if-gt v6, v7, :cond_2

    .line 475
    const/4 v1, 0x1

    .line 480
    :cond_2
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$900(Lcom/sec/android/app/status/GripSensorCalibration;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 481
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 482
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_UI"

    const-string v8, "isCalSuccess()"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1100(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " Y ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1000(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 478
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    const-string v7, "0"

    # setter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorCalibration;->access$802(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 486
    :cond_4
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_UI"

    const-string v8, "CS, CR average value is too high"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1100(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " N ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1000(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 490
    :cond_5
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_UI"

    const-string v8, "isCalSuccess() fail"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1100(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " N ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1000(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 495
    .end local v0    # "crPass":Z
    .end local v1    # "csPass":Z
    :pswitch_6
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$900(Lcom/sec/android/app/status/GripSensorCalibration;)Z

    move-result v3

    .line 496
    .local v3, "iscalsuccess":Z
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibration;->isSpecIn()Z
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$1200(Lcom/sec/android/app/status/GripSensorCalibration;)Z

    move-result v4

    .line 498
    .local v4, "isspecin":Z
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_RESULT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isSpecIn(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v6, "GripSensorCalibration"

    const-string v7, "MSG_UPDATE_RESULT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCalSuccess(): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibration;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibration;->access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x3eb

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 420
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
