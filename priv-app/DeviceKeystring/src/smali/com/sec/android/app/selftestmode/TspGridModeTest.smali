.class public Lcom/sec/android/app/selftestmode/TspGridModeTest;
.super Landroid/app/Activity;
.source "TspGridModeTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;
    }
.end annotation


# instance fields
.field private final HEIGHT_BASIS:I

.field private final WIDTH_BASIS:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    const/16 v0, 0xd

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest;->HEIGHT_BASIS:I

    .line 36
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest;->WIDTH_BASIS:I

    .line 55
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    new-instance v0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;-><init>(Lcom/sec/android/app/selftestmode/TspGridModeTest;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/TspGridModeTest;->setContentView(Landroid/view/View;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/TspGridModeTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 43
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 53
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 48
    return-void
.end method
