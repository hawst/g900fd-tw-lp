.class public Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
.super Landroid/app/Activity;
.source "GripSensorCalibrationDoubleSXxxxx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;
    }
.end annotation


# instance fields
.field private final CALDATA_SYSFS_PATH:Ljava/lang/String;

.field private final CALDATA_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final CALIBRATION:B

.field private final CALIBRATION_ERASE:B

.field private final CAL_SYSFS_PATH:Ljava/lang/String;

.field private final CAL_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final GRIP_OFF:B

.field private final GRIP_ON:B

.field private final MODE_SYSFS_PATH:Ljava/lang/String;

.field private final MODE_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final ONOFF_SYSFS_PATH:Ljava/lang/String;

.field private final ONOFF_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH_2ND:Ljava/lang/String;

.field private final RESET_SYSFS_PATH:Ljava/lang/String;

.field private isTimerFinish:Z

.field private isgrip_On:Z

.field private ispass:Z

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mButton_Exit:Landroid/widget/Button;

.field private mButton_Next:Landroid/widget/Button;

.field private mButton_reset:Landroid/widget/Button;

.field private mCaldata_data:Ljava/lang/String;

.field private mCaldata_data2:Ljava/lang/String;

.field private mCspercent_data:Ljava/lang/String;

.field private mCspercent_data2:Ljava/lang/String;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mIsCalSuccess:Z

.field private mMode_data:Ljava/lang/String;

.field private mMode_data2:Ljava/lang/String;

.field private mProxpercent_data:Ljava/lang/String;

.field private mProxpercent_data2:Ljava/lang/String;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mText_Caldata:Landroid/widget/TextView;

.field private mText_Caldata2:Landroid/widget/TextView;

.field private mText_Diff:Landroid/widget/TextView;

.field private mText_Diff2:Landroid/widget/TextView;

.field private mText_Mode:Landroid/widget/TextView;

.field private mText_Mode2:Landroid/widget/TextView;

.field private mText_Offset:Landroid/widget/TextView;

.field private mText_Offset2:Landroid/widget/TextView;

.field private mText_RAW_Count:Landroid/widget/TextView;

.field private mText_RAW_Count2:Landroid/widget/TextView;

.field private mText_Thd:Landroid/widget/TextView;

.field private mText_Thd2:Landroid/widget/TextView;

.field mThreadGetData:Ljava/lang/Thread;

.field private mTimerHandler:Landroid/os/Handler;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x31

    const/16 v0, 0x30

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mThreadGetData:Ljava/lang/Thread;

    .line 48
    iput-byte v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CALIBRATION:B

    .line 49
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CALIBRATION_ERASE:B

    .line 50
    iput-byte v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->GRIP_ON:B

    .line 51
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->GRIP_OFF:B

    .line 52
    const-string v0, "/sys/class/sensors/grip_sensor/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CSPERSENT_SYSFS_PATH:Ljava/lang/String;

    .line 53
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CSPERSENT_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 54
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

    .line 55
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->PROXIMITYPERCENT_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 56
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CALDATA_SYSFS_PATH:Ljava/lang/String;

    .line 57
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CALDATA_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 58
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CAL_SYSFS_PATH:Ljava/lang/String;

    .line 59
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->CAL_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 60
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->ONOFF_SYSFS_PATH:Ljava/lang/String;

    .line 61
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/onoff"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->ONOFF_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 62
    const-string v0, "/sys/class/sensors/grip_sensor/reset"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->RESET_SYSFS_PATH:Ljava/lang/String;

    .line 63
    const-string v0, "/sys/class/sensors/grip_sensor/mode"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->MODE_SYSFS_PATH:Ljava/lang/String;

    .line 64
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/mode"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->MODE_SYSFS_PATH_2ND:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data2:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data2:Ljava/lang/String;

    .line 73
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->ispass:Z

    .line 74
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isgrip_On:Z

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isTimerFinish:Z

    .line 85
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mIsCalSuccess:Z

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 583
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$3;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    .line 651
    return-void
.end method

.method private UpdateCSPercent()V
    .locals 12

    .prologue
    .line 312
    const/4 v0, 0x0

    .line 313
    .local v0, "RAWCount":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 314
    .local v1, "RAWCount2":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 315
    .local v7, "rawData":Ljava/lang/String;
    const/4 v8, 0x0

    .line 316
    .local v8, "rawData2":Ljava/lang/String;
    const/4 v2, 0x0

    .line 317
    .local v2, "diffData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 318
    .local v3, "diffData2":Ljava/lang/String;
    const/4 v5, 0x0

    .line 319
    .local v5, "offsetData":Ljava/lang/String;
    const/4 v6, 0x0

    .line 320
    .local v6, "offsetData2":Ljava/lang/String;
    const-string v9, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 321
    const-string v9, "/sys/class/sensors/grip_sensor_2nd/threshold"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data2:Ljava/lang/String;

    .line 322
    const-string v9, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 323
    const-string v9, "/sys/class/sensors/grip_sensor_2nd/calibration"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;

    .line 324
    const-string v9, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    .line 325
    const-string v9, "/sys/class/sensors/grip_sensor_2nd/raw_data"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    .line 326
    const-string v9, "/sys/class/sensors/grip_sensor/mode"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data:Ljava/lang/String;

    .line 327
    const-string v9, "/sys/class/sensors/grip_sensor_2nd/mode"

    invoke-direct {p0, v9}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data2:Ljava/lang/String;

    .line 328
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 329
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data:Ljava/lang/String;

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 330
    .local v4, "mProxpercent_datas":[Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Thd:Landroid/widget/TextView;

    const/4 v10, 0x0

    aget-object v10, v4, v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    .end local v4    # "mProxpercent_datas":[Ljava/lang/String;
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data2:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 333
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Thd2:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data2:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v9, :cond_6

    .line 336
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 340
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    if-eqz v9, :cond_7

    .line 341
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 346
    :goto_1
    if-eqz v0, :cond_2

    .line 347
    const/4 v9, 0x0

    aget-object v7, v0, v9

    .line 348
    const/4 v9, 0x1

    aget-object v2, v0, v9

    .line 349
    const/4 v9, 0x2

    aget-object v5, v0, v9

    .line 351
    :cond_2
    if-eqz v1, :cond_3

    .line 352
    const/4 v9, 0x0

    aget-object v8, v1, v9

    .line 353
    const/4 v9, 0x1

    aget-object v3, v1, v9

    .line 354
    const/4 v9, 0x2

    aget-object v6, v1, v9

    .line 357
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data:Ljava/lang/String;

    const-string v10, "exception"

    if-eq v9, v10, :cond_a

    .line 358
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v9

    const/high16 v10, -0x10000

    invoke-static {v10}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v10

    if-ne v9, v10, :cond_8

    .line 359
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 360
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 361
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 368
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data:Ljava/lang/String;

    const-string v10, "0"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 372
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v10, "INIT"

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :cond_4
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCspercent_data2:Ljava/lang/String;

    const-string v10, "exception"

    if-eq v9, v10, :cond_d

    .line 383
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v9

    const/high16 v10, -0x10000

    invoke-static {v10}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v10

    if-ne v9, v10, :cond_b

    .line 384
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 385
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff2:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 386
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    const/16 v10, -0x100

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 393
    :goto_4
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff2:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    const-string v9, "0"

    iget-object v10, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data2:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 397
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode2:Landroid/widget/TextView;

    const-string v10, "INIT"

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    :cond_5
    :goto_5
    return-void

    .line 338
    :cond_6
    const-string v9, "GripSensorCalibrationDoubleSXxxxx"

    const-string v10, "UpdateCSPercent"

    const-string v11, "mCspercent_data null"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 343
    :cond_7
    const-string v9, "GripSensorCalibrationDoubleSXxxxx"

    const-string v10, "UpdateCSPercent2"

    const-string v11, "mCspercent_data2 null"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 364
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 365
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 366
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 373
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data:Ljava/lang/String;

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 374
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v10, "NORMAL"

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 378
    :cond_a
    const-string v9, "GripSensorCalibrationDoubleSXxxxx"

    const-string v10, "UpdateCSPercent"

    const-string v11, "mCspercent_data null"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 389
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 390
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff2:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    .line 391
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    const/high16 v10, -0x10000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_4

    .line 398
    :cond_c
    const-string v9, "1"

    iget-object v10, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mMode_data2:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 399
    iget-object v9, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode2:Landroid/widget/TextView;

    const-string v10, "NORMAL"

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 403
    :cond_d
    const-string v9, "GripSensorCalibrationDoubleSXxxxx"

    const-string v10, "UpdateCSPercent"

    const-string v11, "mCspercent_data null"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method private UpdateGripdata()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 307
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mProxpercent_data2:Ljava/lang/String;

    .line 308
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 309
    const-string v0, "/sys/class/sensors/grip_sensor_2nd/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;

    .line 310
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mCaldata_data2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isCalSuccess2()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isSpecIn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/hardware/Sensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
    .param p1, "x1"    # B

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->calibration(B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->toggleButtonClickable(Z)V

    return-void
.end method

.method private adjustTextSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 197
    return-void
.end method

.method private calibration(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 409
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "calibration"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calibration data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "/sys/class/sensors/grip_sensor_2nd/calibration"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCalSuccess()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 463
    const-string v4, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 464
    .local v1, "caldata_data":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 465
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "isCalSuccess()"

    const-string v6, "Sysfs Value Null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_0
    :goto_0
    return v3

    .line 468
    :cond_1
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 471
    .local v2, "caldatas":[Ljava/lang/String;
    const-string v0, "0"

    .line 473
    .local v0, "NOT_CALIBRATED":Ljava/lang/String;
    const-string v4, "0"

    aget-object v5, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 474
    const-string v3, "GripSensorCalibrationDoubleSXxxxx"

    const-string v4, "isCalSuccess()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOT CALIBRATED: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isCalSuccess2()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 481
    const-string v4, "/sys/class/sensors/grip_sensor_2nd/calibration"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "caldata_data2":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 483
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "isCalSuccess()"

    const-string v6, "Sysfs Value Null2"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_0
    :goto_0
    return v3

    .line 486
    :cond_1
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 489
    .local v2, "caldatas2":[Ljava/lang/String;
    const-string v0, "0"

    .line 491
    .local v0, "NOT_CALIBRATED":Ljava/lang/String;
    const-string v4, "0"

    aget-object v5, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 492
    const-string v3, "GripSensorCalibrationDoubleSXxxxx"

    const-string v4, "isCalSuccess()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOT CALIBRATED: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isSpecIn()Z
    .locals 22

    .prologue
    .line 538
    const-string v18, "/sys/class/sensors/grip_sensor/threshold"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 539
    .local v14, "proxpercent_data":Ljava/lang/String;
    const-string v18, "/sys/class/sensors/grip_sensor_2nd/threshold"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 540
    .local v15, "proxpercent_data2":Ljava/lang/String;
    const-string v18, "/sys/class/sensors/grip_sensor/calibration"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 541
    .local v3, "caldata_data":Ljava/lang/String;
    const-string v18, "/sys/class/sensors/grip_sensor_2nd/calibration"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 542
    .local v4, "caldata_data2":Ljava/lang/String;
    const-string v18, "/sys/class/sensors/grip_sensor/raw_data"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 543
    .local v9, "cspercent_data":Ljava/lang/String;
    const-string v18, "/sys/class/sensors/grip_sensor_2nd/raw_data"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 544
    .local v10, "cspercent_data2":Ljava/lang/String;
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 545
    .local v7, "csPercentList":[Ljava/lang/String;
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 546
    .local v8, "csPercentList2":[Ljava/lang/String;
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 547
    .local v5, "caldatas":[Ljava/lang/String;
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 548
    .local v6, "caldatas2":[Ljava/lang/String;
    if-eqz v7, :cond_0

    if-nez v5, :cond_1

    .line 549
    :cond_0
    const/16 v18, 0x0

    .line 580
    :goto_0
    return v18

    .line 551
    :cond_1
    if-eqz v8, :cond_2

    if-nez v6, :cond_3

    .line 552
    :cond_2
    const/16 v18, 0x0

    goto :goto_0

    .line 554
    :cond_3
    array-length v0, v7

    move/from16 v18, v0

    const/16 v19, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    .line 555
    const-string v18, "GripSensorCalibrationDoubleSXxxxx"

    const-string v19, "isSpecIn"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "csList.length : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    array-length v0, v7

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const/16 v18, 0x0

    goto :goto_0

    .line 558
    :cond_4
    array-length v0, v8

    move/from16 v18, v0

    const/16 v19, 0x5

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_5

    .line 559
    const-string v18, "GripSensorCalibrationDoubleSXxxxx"

    const-string v19, "isSpecIn"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "csList.length : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    array-length v0, v8

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const/16 v18, 0x0

    goto :goto_0

    .line 563
    :cond_5
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 564
    .local v16, "thdSpec":I
    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 565
    .local v17, "thdSpec2":I
    const/4 v2, 0x4

    .line 566
    .local v2, "CS_PERCENT_CNT":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v11, v0, :cond_7

    .line 567
    aget-object v18, v7, v11

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 568
    .local v12, "percent":I
    move/from16 v0, v16

    if-ge v12, v0, :cond_6

    .line 569
    const-string v18, "GripSensorCalibrationDoubleSXxxxx"

    const-string v19, "isSpecIn"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "spec out : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 566
    :cond_6
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 573
    .end local v12    # "percent":I
    :cond_7
    const/4 v11, 0x0

    :goto_2
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v11, v0, :cond_9

    .line 574
    aget-object v18, v8, v11

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 575
    .local v13, "percent2":I
    move/from16 v0, v17

    if-ge v13, v0, :cond_8

    .line 576
    const-string v18, "GripSensorCalibrationDoubleSXxxxx"

    const-string v19, "isSpecIn"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "spec out : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 573
    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 580
    .end local v13    # "percent2":I
    :cond_9
    const/16 v18, 0x1

    goto/16 :goto_0
.end method

.method private readSysfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    .line 418
    const/4 v2, 0x0

    .line 419
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v1, ""

    .line 421
    .local v1, "readData":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 423
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sysfs Data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 429
    if-eqz v3, :cond_0

    .line 430
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 436
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return-object v1

    .line 432
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 433
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 435
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 424
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 425
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 426
    const/4 v1, 0x0

    .line 429
    if-eqz v2, :cond_1

    .line 430
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 432
    :catch_2
    move-exception v0

    .line 433
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 428
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 429
    :goto_2
    if-eqz v2, :cond_2

    .line 430
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 434
    :cond_2
    :goto_3
    throw v4

    .line 432
    :catch_3
    move-exception v0

    .line 433
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationDoubleSXxxxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 428
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 424
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method private sensorOn()V
    .locals 5

    .prologue
    .line 238
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    if-nez v1, :cond_0

    .line 239
    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 242
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 244
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 247
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 249
    .local v0, "result":Z
    const-string v1, "GripSensorCalibrationDoubleSXxxxx"

    const-string v2, "sensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method private toggleButtonClickable(Z)V
    .locals 4
    .param p1, "clickable"    # Z

    .prologue
    .line 252
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "toggleButtonClickable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Clickable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 262
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 440
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const/4 v3, 0x0

    .line 442
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 444
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 446
    const/4 v3, 0x1

    .line 451
    if-eqz v2, :cond_0

    .line 452
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 458
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    return v3

    .line 454
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 457
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 447
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 448
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 451
    if-eqz v1, :cond_1

    .line 452
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 454
    :catch_2
    move-exception v0

    .line 455
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 450
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 451
    :goto_2
    if-eqz v1, :cond_2

    .line 452
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 456
    :cond_2
    :goto_3
    throw v4

    .line 454
    :catch_3
    move-exception v0

    .line 455
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationDoubleSXxxxx"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 450
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 447
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x64

    const/16 v2, 0x3ea

    const/16 v6, 0x3e9

    const/16 v4, 0x3e8

    .line 265
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 299
    :goto_0
    return-void

    .line 267
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 271
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 274
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ed

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 278
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 281
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 282
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "GripSensorCalibrationDoubleSXxxxx"

    const-string v2, "onClick"

    const-string v3, "go to GripSensorInitialWorkingTestDouble.java"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 286
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ee

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ef

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 291
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 294
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->finish()V

    goto :goto_0

    .line 265
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a009f -> :sswitch_0
        0x7f0a00a0 -> :sswitch_1
        0x7f0a00a1 -> :sswitch_2
        0x7f0a00a4 -> :sswitch_4
        0x7f0a00aa -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const-string v3, "GripSensorCalibrationDoubleSXxxxx"

    const-string v4, "onCreate"

    const-string v5, "onCreate"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const v3, 0x7f03002a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->setContentView(I)V

    .line 95
    const v3, 0x7f0a00a6

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Thd:Landroid/widget/TextView;

    .line 96
    const v3, 0x7f0a00ae

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Thd2:Landroid/widget/TextView;

    .line 97
    const v3, 0x7f0a008f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    .line 98
    const v3, 0x7f0a00b0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count2:Landroid/widget/TextView;

    .line 99
    const v3, 0x7f0a0090

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff:Landroid/widget/TextView;

    .line 100
    const v3, 0x7f0a00b1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Diff2:Landroid/widget/TextView;

    .line 101
    const v3, 0x7f0a0091

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset:Landroid/widget/TextView;

    .line 102
    const v3, 0x7f0a00b2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Offset2:Landroid/widget/TextView;

    .line 103
    const v3, 0x7f0a00a7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    .line 104
    const v3, 0x7f0a00b3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Caldata2:Landroid/widget/TextView;

    .line 105
    const v3, 0x7f0a00a5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode:Landroid/widget/TextView;

    .line 106
    const v3, 0x7f0a00af

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_Mode2:Landroid/widget/TextView;

    .line 108
    const v3, 0x7f0a009f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v3, 0x7f0a00a0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const v3, 0x7f0a00a1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Next:Landroid/widget/Button;

    .line 113
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    const-string v4, " : wait"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    const v3, 0x7f0a00aa

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_reset:Landroid/widget/Button;

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_reset:Landroid/widget/Button;

    const-string v4, "RESET"

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v3, 0x7f0a00a4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Exit:Landroid/widget/Button;

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const/high16 v3, 0x41a00000    # 20.0f

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->adjustTextSize(F)V

    .line 121
    const/4 v1, 0x0

    .line 124
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    const-string v3, "/sys/class/sensors/grip_sensor/onoff"

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 130
    if-eqz v2, :cond_2

    .line 132
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 138
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 133
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "GripSensorCalibrationDoubleSXxxxx"

    const-string v4, "onCreate"

    const-string v5, "error when close FileOutputStream"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 135
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 128
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 130
    if-eqz v1, :cond_0

    .line 132
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 133
    :catch_2
    move-exception v0

    .line 134
    const-string v3, "GripSensorCalibrationDoubleSXxxxx"

    const-string v4, "onCreate"

    const-string v5, "error when close FileOutputStream"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 130
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_1

    .line 132
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 135
    :cond_1
    :goto_3
    throw v3

    .line 133
    :catch_3
    move-exception v0

    .line 134
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationDoubleSXxxxx"

    const-string v5, "onCreate"

    const-string v6, "error when close FileOutputStream"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 130
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 126
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :cond_2
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 234
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 236
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 220
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 224
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;

    .line 225
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 226
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 227
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isgrip_On:Z

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isTimerFinish:Z

    .line 229
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mIsCalSuccess:Z

    .line 230
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 231
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 200
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 201
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->sensorOn()V

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->UpdateGripdata()V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 206
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$2;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mTimerHandler:Landroid/os/Handler;

    .line 214
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->isTimerFinish:Z

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 216
    return-void
.end method
