.class Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;
.super Ljava/lang/Object;
.source "LoopbackTest2.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->startRecording()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)V
    .locals 0

    .prologue
    .line 576
    iput-object p1, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 578
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 579
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioRecord;->startRecording()V

    .line 583
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    if-nez v4, :cond_4

    .line 612
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 613
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioRecord;->getState()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 614
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioRecord;->stop()V

    .line 616
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioRecord;->release()V

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    const/4 v5, 0x0

    # setter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v4, v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$502(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;

    .line 620
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # setter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mIsRecording:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$602(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Z)Z

    .line 622
    :cond_3
    return-void

    .line 586
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mIsRecording:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$602(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Z)Z

    .line 589
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$700(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioManager;

    move-result-object v4

    const-string v5, "factory_test_mic_check=on"

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 592
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    const-string v4, "LoopbackTest2.AudioRecording"

    const-string v5, "micCheckOnOff"

    const-string v6, "factory_test_mic_check=off"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$700(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioManager;

    move-result-object v4

    const-string v5, "factory_test_mic_check=off"

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 602
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-virtual {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->getRMS()Ljava/lang/String;

    move-result-object v3

    .line 603
    .local v3, "rmsValue":Ljava/lang/String;
    const-string v4, "LoopbackTest2.AudioRecording"

    const-string v5, "MicTestRecoder.doInBackground"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rmsValue: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    const-string v5, "L_value="

    # invokes: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->getParameter(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v4, v3, v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$800(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 605
    .local v1, "rmsL":I
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    const-string v5, "R_value="

    # invokes: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->getParameter(Ljava/lang/String;Ljava/lang/String;)I
    invoke-static {v4, v3, v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$800(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 608
    .local v2, "rmsR":I
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$900(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$900(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v8, v2, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 609
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    iget-object v5, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mRecorder:Landroid/media/AudioRecord;
    invoke-static {v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$500(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)Landroid/media/AudioRecord;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    iget-object v6, v6, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->buffer:[B

    iget-object v7, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    # getter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mBufferSize:I
    invoke-static {v7}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$1100(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;)I

    move-result v7

    invoke-virtual {v5, v6, v8, v7}, Landroid/media/AudioRecord;->read([BII)I

    move-result v5

    # setter for: Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mReadBytes:I
    invoke-static {v4, v5}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->access$1002(Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;I)I

    .line 610
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording$1;->this$0:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    iget-object v4, v4, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->mThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-eqz v4, :cond_0

    goto/16 :goto_0

    .line 593
    .end local v1    # "rmsL":I
    .end local v2    # "rmsR":I
    .end local v3    # "rmsValue":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 594
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method
