.class public Lcom/sec/android/app/status/TouchNoise;
.super Landroid/app/Activity;
.source "TouchNoise.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TouchNoise$MyView;
    }
.end annotation


# static fields
.field private static FILE_TIME:Ljava/lang/String;

.field protected static test_num:I


# instance fields
.field private HEIGHT_BASIS:I

.field private final MAX_BUFFER_SIZE:I

.field private RawDiff_Buf:Ljava/lang/StringBuffer;

.field private WIDTH_BASIS:I

.field col_height:F

.field col_width:F

.field private currentX:Ljava/lang/String;

.field private currentY:Ljava/lang/String;

.field private deltaValue_10:Ljava/lang/String;

.field private deltaValue_11:Ljava/lang/String;

.field private deltaValue_5:Ljava/lang/String;

.field private deltaValue_6:Ljava/lang/String;

.field private deltaValue_7:Ljava/lang/String;

.field private deltaValue_8:Ljava/lang/String;

.field private deltaValue_9:Ljava/lang/String;

.field private externalSdPath:Ljava/lang/String;

.field private font_size:I

.field private intensityValue_0:Ljava/lang/String;

.field private intensityValue_1:Ljava/lang/String;

.field private intensityValue_10:Ljava/lang/String;

.field private intensityValue_11:Ljava/lang/String;

.field private intensityValue_12:Ljava/lang/String;

.field private intensityValue_13:Ljava/lang/String;

.field private intensityValue_14:Ljava/lang/String;

.field private intensityValue_2:Ljava/lang/String;

.field private intensityValue_3:Ljava/lang/String;

.field private intensityValue_4:Ljava/lang/String;

.field private intensityValue_5:Ljava/lang/String;

.field private intensityValue_6:Ljava/lang/String;

.field private intensityValue_7:Ljava/lang/String;

.field private intensityValue_8:Ljava/lang/String;

.field private intensityValue_9:Ljava/lang/String;

.field private isDataRawFileExist:Z

.field private isMelfas:Z

.field private isSTM:Z

.field private isSaving:Z

.field private lcd_height:I

.field private lcd_width:I

.field private mCanvas:Landroid/graphics/Canvas;

.field protected mSavingHandler:Landroid/os/Handler;

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mStorageVolumes:[Landroid/os/storage/StorageVolume;

.field private mTspManufacture:Ljava/lang/String;

.field private mbackgroundPaint:Landroid/graphics/Paint;

.field private mlinePaint:Landroid/graphics/Paint;

.field private mrectPaint:Landroid/graphics/Paint;

.field private mrectPaint2:Landroid/graphics/Paint;

.field private productModel:Ljava/lang/String;

.field private rawArryInt:[[Ljava/lang/String;

.field private rawArryRef:[[Ljava/lang/String;

.field private thresholdValue:Ljava/lang/String;

.field private threshold_all_refer:Ljava/lang/String;

.field private threshold_delta_0:Ljava/lang/String;

.field private threshold_delta_1:Ljava/lang/String;

.field private threshold_delta_2:Ljava/lang/String;

.field private threshold_delta_3:Ljava/lang/String;

.field private threshold_delta_4:Ljava/lang/String;

.field private threshold_intensity_0:Ljava/lang/String;

.field private threshold_intensity_1:Ljava/lang/String;

.field private threshold_intensity_10:Ljava/lang/String;

.field private threshold_intensity_11:Ljava/lang/String;

.field private threshold_intensity_12:Ljava/lang/String;

.field private threshold_intensity_13:Ljava/lang/String;

.field private threshold_intensity_14:Ljava/lang/String;

.field private threshold_intensity_2:Ljava/lang/String;

.field private threshold_intensity_3:Ljava/lang/String;

.field private threshold_intensity_4:Ljava/lang/String;

.field private threshold_intensity_5:Ljava/lang/String;

.field private threshold_intensity_6:Ljava/lang/String;

.field private threshold_intensity_7:Ljava/lang/String;

.field private threshold_intensity_8:Ljava/lang/String;

.field private threshold_intensity_9:Ljava/lang/String;

.field private threshold_refer:Ljava/lang/String;

.field private threshold_refer_0:Ljava/lang/String;

.field private threshold_refer_1:Ljava/lang/String;

.field private threshold_refer_10:Ljava/lang/String;

.field private threshold_refer_11:Ljava/lang/String;

.field private threshold_refer_12:Ljava/lang/String;

.field private threshold_refer_13:Ljava/lang/String;

.field private threshold_refer_14:Ljava/lang/String;

.field private threshold_refer_2:Ljava/lang/String;

.field private threshold_refer_3:Ljava/lang/String;

.field private threshold_refer_4:Ljava/lang/String;

.field private threshold_refer_5:Ljava/lang/String;

.field private threshold_refer_6:Ljava/lang/String;

.field private threshold_refer_7:Ljava/lang/String;

.field private threshold_refer_8:Ljava/lang/String;

.field private threshold_refer_9:Ljava/lang/String;

.field private touchValue_0:Ljava/lang/String;

.field private touchValue_1:Ljava/lang/String;

.field private touchValue_10:Ljava/lang/String;

.field private touchValue_11:Ljava/lang/String;

.field private touchValue_12:Ljava/lang/String;

.field private touchValue_13:Ljava/lang/String;

.field private touchValue_14:Ljava/lang/String;

.field private touchValue_2:Ljava/lang/String;

.field private touchValue_3:Ljava/lang/String;

.field private touchValue_4:Ljava/lang/String;

.field private touchValue_5:Ljava/lang/String;

.field private touchValue_6:Ljava/lang/String;

.field private touchValue_7:Ljava/lang/String;

.field private touchValue_8:Ljava/lang/String;

.field private touchValue_9:Ljava/lang/String;

.field private touchX:I

.field private touchY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/status/TouchNoise;->test_num:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 87
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;

    .line 88
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_1:Ljava/lang/String;

    .line 89
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_2:Ljava/lang/String;

    .line 90
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_3:Ljava/lang/String;

    .line 91
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_4:Ljava/lang/String;

    .line 92
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;

    .line 93
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;

    .line 94
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;

    .line 95
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;

    .line 96
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;

    .line 97
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_10:Ljava/lang/String;

    .line 98
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_11:Ljava/lang/String;

    .line 99
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_12:Ljava/lang/String;

    .line 100
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_13:Ljava/lang/String;

    .line 101
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_14:Ljava/lang/String;

    .line 103
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->thresholdValue:Ljava/lang/String;

    .line 105
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_5:Ljava/lang/String;

    .line 106
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_6:Ljava/lang/String;

    .line 107
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_7:Ljava/lang/String;

    .line 108
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_8:Ljava/lang/String;

    .line 109
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_9:Ljava/lang/String;

    .line 110
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_10:Ljava/lang/String;

    .line 111
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_11:Ljava/lang/String;

    .line 113
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_0:Ljava/lang/String;

    .line 114
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_1:Ljava/lang/String;

    .line 115
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_2:Ljava/lang/String;

    .line 116
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_3:Ljava/lang/String;

    .line 117
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_4:Ljava/lang/String;

    .line 118
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;

    .line 119
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;

    .line 120
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;

    .line 121
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;

    .line 122
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;

    .line 123
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_10:Ljava/lang/String;

    .line 124
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_11:Ljava/lang/String;

    .line 125
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_12:Ljava/lang/String;

    .line 126
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_13:Ljava/lang/String;

    .line 127
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_14:Ljava/lang/String;

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_all_refer:Ljava/lang/String;

    .line 137
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_9:Ljava/lang/String;

    .line 141
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_14:Ljava/lang/String;

    .line 144
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;

    .line 149
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_9:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_14:Ljava/lang/String;

    .line 157
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->MAX_BUFFER_SIZE:I

    .line 159
    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->touchX:I

    .line 160
    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->touchY:I

    .line 162
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->currentX:Ljava/lang/String;

    .line 163
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->currentY:Ljava/lang/String;

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    .line 165
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z

    .line 166
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoise;->isSTM:Z

    .line 167
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    .line 168
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z

    .line 176
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->RawDiff_Buf:Ljava/lang/StringBuffer;

    .line 179
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    .line 305
    new-instance v0, Lcom/sec/android/app/status/TouchNoise$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TouchNoise$1;-><init>(Lcom/sec/android/app/status/TouchNoise;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mSavingHandler:Landroid/os/Handler;

    .line 839
    return-void
.end method

.method private WriteToStorage()V
    .locals 11

    .prologue
    .line 372
    const/4 v5, 0x0

    .line 373
    .local v5, "strBuffer":Ljava/lang/String;
    const/4 v2, 0x0

    .line 376
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v6, "/mnt/extSdCard/"

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->externalSdPath:Ljava/lang/String;

    .line 377
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/status/TouchNoise;->externalSdPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/status/TouchNoise;->FILE_TIME:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Tsp_RawAndDiff_Data.txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v0, "Openfile":Ljava/io/File;
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file create path = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->externalSdPath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Tsp_RawAndDiff_Data.txt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v6, 0x1

    invoke-direct {v3, v0, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->RawDiff_Buf:Ljava/lang/StringBuffer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 385
    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 394
    if-eqz v3, :cond_0

    .line 395
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 402
    .end local v0    # "Openfile":Ljava/io/File;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    return-void

    .line 397
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "Openfile":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 398
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File close exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 401
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 386
    .end local v0    # "Openfile":Ljava/io/File;
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 387
    .local v4, "ie":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file create IOException : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 394
    if-eqz v2, :cond_1

    .line 395
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 397
    :catch_2
    move-exception v1

    .line 398
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File close exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "ie":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 390
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file create Exception : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 394
    if-eqz v2, :cond_1

    .line 395
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto/16 :goto_0

    .line 397
    :catch_4
    move-exception v1

    .line 398
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "TouchNoise"

    const-string v7, "WriteToStorage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File close exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 393
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 394
    :goto_3
    if-eqz v2, :cond_2

    .line 395
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 400
    :cond_2
    :goto_4
    throw v6

    .line 397
    :catch_5
    move-exception v1

    .line 398
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "TouchNoise"

    const-string v8, "WriteToStorage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "File close exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 393
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v0    # "Openfile":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 389
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 386
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_1
.end method

.method static synthetic access$002(Lcom/sec/android/app/status/TouchNoise;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/TouchNoise;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Canvas;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/status/TouchNoise;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Landroid/graphics/Canvas;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->mCanvas:Landroid/graphics/Canvas;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->font_size:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/TouchNoise;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/TouchNoise;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_melfas2()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_0:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchX:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/status/TouchNoise;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchY:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->thresholdValue:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->thresholdValue:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_5:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_6:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_7:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/TouchNoise;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_8:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_9:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_5:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_6:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_7:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/TouchNoise;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_melfas()V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_8:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_9:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_5:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_6:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_7:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_7:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/TouchNoise;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/status/TouchNoise;->isSTM:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_8:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_8:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_9:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5302(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->deltaValue_9:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_1:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_2:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_3:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_4:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$5900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/TouchNoise;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_stm()V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_7:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_8:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_9:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_10:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_10:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_10:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_11:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_11:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_11:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_12:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_12:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$6900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_12:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/TouchNoise;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus()V

    return-void
.end method

.method static synthetic access$7000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_13:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_13:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_13:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_14:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7202(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->touchValue_14:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_14:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_0:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_0:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_1:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7502(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_1:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_2:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_3:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7702(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_3:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_4:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_4:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_5:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_6:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_7:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_8:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_9:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_10:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8402(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_10:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8500(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_10:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8600(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_11:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8602(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_11:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8700(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_11:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8800(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_12:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8802(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_12:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$8900(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_12:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9000(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_13:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9002(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_13:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$9100(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_13:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_14:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9202(Lcom/sec/android/app/status/TouchNoise;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise;->intensityValue_14:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$9300(Lcom/sec/android/app/status/TouchNoise;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoise;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_14:Ljava/lang/String;

    return-object v0
.end method

.method private checkExternalSdcard()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 345
    const-string v0, "storage"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-virtual {v0}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageManager:Landroid/os/storage/StorageManager;

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "mounted"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mStorageVolumes:[Landroid/os/storage/StorageVolume;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->externalSdPath:Ljava/lang/String;

    move v0, v1

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTimeToString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 359
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 362
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x2

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 363
    .local v4, "month":Ljava/lang/String;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x5

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 364
    .local v1, "day":Ljava/lang/String;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xb

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 365
    .local v2, "hour":Ljava/lang/String;
    new-instance v6, Ljava/text/DecimalFormat;

    const-string v7, "00"

    invoke-direct {v6, v7}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v7, 0xc

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 366
    .local v3, "min":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 367
    .local v5, "strTime":Ljava/lang/String;
    const-string v6, "TouchNoise"

    const-string v7, "getTimeToString"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getTimeToString : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    return-object v5
.end method

.method private getValueFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x64

    const/4 v8, 0x0

    .line 790
    const/4 v3, 0x0

    .line 791
    .local v3, "in":Ljava/io/InputStream;
    new-array v0, v9, [B

    .line 792
    .local v0, "buffer":[B
    const/4 v6, 0x0

    .line 793
    .local v6, "value":Ljava/lang/String;
    const/4 v5, 0x0

    .line 795
    .local v5, "length":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v9, :cond_0

    .line 796
    aput-byte v8, v0, v2

    .line 795
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 800
    :cond_0
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 801
    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 803
    if-eqz v5, :cond_1

    .line 804
    new-instance v7, Ljava/lang/String;

    const/4 v8, 0x0

    add-int/lit8 v9, v5, -0x1

    invoke-direct {v7, v0, v8, v9}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "value":Ljava/lang/String;
    .local v7, "value":Ljava/lang/String;
    move-object v6, v7

    .line 810
    .end local v7    # "value":Ljava/lang/String;
    .restart local v6    # "value":Ljava/lang/String;
    :cond_1
    if-eqz v4, :cond_4

    .line 812
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 820
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_2
    :goto_1
    return-object v6

    .line 813
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 814
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "TouchNoise"

    const-string v9, "getValueFromFile"

    const-string v10, "IOException"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 816
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1

    .line 806
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 807
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 808
    const-string v8, "TouchNoise"

    const-string v9, "getValueFromFile"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "IOException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 810
    if-eqz v3, :cond_2

    .line 812
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 813
    :catch_2
    move-exception v1

    .line 814
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "TouchNoise"

    const-string v9, "getValueFromFile"

    const-string v10, "IOException"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 810
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_3
    if-eqz v3, :cond_3

    .line 812
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 816
    :cond_3
    :goto_4
    throw v8

    .line 813
    :catch_3
    move-exception v1

    .line 814
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v9, "TouchNoise"

    const-string v10, "getValueFromFile"

    const-string v11, "IOException"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 810
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 806
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_4
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "in":Ljava/io/InputStream;
    :cond_4
    move-object v3, v4

    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private initPaint()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 824
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mlinePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/status/TouchNoise;->font_size:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 828
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 831
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mbackgroundPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 834
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 836
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->mrectPaint2:Landroid/graphics/Paint;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 837
    return-void
.end method

.method private readCmdResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 548
    const-string v3, "TouchNoise"

    const-string v4, "readCmdResult"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "***readCmdResult = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v2, ""

    .line 550
    .local v2, "status":Ljava/lang/String;
    const-string v1, ""

    .line 551
    .local v1, "result":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/status/TouchNoise;->writeCmd(Ljava/lang/String;)V

    .line 552
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->readStatus()Ljava/lang/String;

    move-result-object v2

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->readResult()Ljava/lang/String;

    move-result-object v1

    .line 554
    const/4 v0, 0x0

    .line 556
    .local v0, "arr":[Ljava/lang/String;
    if-eqz v2, :cond_3

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 558
    if-eqz v1, :cond_0

    .line 559
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 561
    :cond_0
    if-eqz v0, :cond_1

    .line 562
    array-length v3, v0

    if-le v3, v7, :cond_2

    .line 563
    aget-object v1, v0, v7

    .line 576
    :cond_1
    :goto_0
    return-object v1

    .line 565
    :cond_2
    const/4 v3, 0x0

    aget-object v1, v0, v3

    goto :goto_0

    .line 568
    :cond_3
    const-string v3, "RUNNING"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 569
    const-string v1, "running"

    goto :goto_0

    .line 570
    :cond_4
    const-string v3, "FAIL"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 571
    const-string v1, "fail"

    goto :goto_0

    .line 573
    :cond_5
    const-string v1, "NA"

    goto :goto_0
.end method

.method private readRawData_melfas()V
    .locals 12

    .prologue
    .line 580
    const-string v8, ""

    .line 581
    .local v8, "raw":Ljava/lang/String;
    const/4 v9, 0x0

    .line 582
    .local v9, "raw_a":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 585
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    const-string v11, "/sys/class/sec/tsp/raw_data"

    invoke-direct {v10, v11}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    :try_start_1
    iget v10, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    if-ge v6, v10, :cond_4

    .line 588
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_1
    iget v10, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    if-ge v7, v10, :cond_3

    .line 589
    if-eqz v1, :cond_0

    .line 590
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .line 593
    :cond_0
    if-eqz v8, :cond_1

    .line 594
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 595
    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 598
    :cond_1
    if-eqz v9, :cond_2

    .line 599
    iget-object v10, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v10, v10, v6

    const/4 v11, 0x1

    aget-object v11, v9, v11

    aput-object v11, v10, v7

    .line 600
    iget-object v10, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v10, v10, v6

    const/4 v11, 0x0

    aget-object v11, v9, v11

    aput-object v11, v10, v7
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 601
    const/4 v9, 0x0

    .line 588
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 587
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 617
    .end local v7    # "j":I
    :cond_4
    if-eqz v1, :cond_5

    .line 618
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_5
    move-object v0, v1

    .line 624
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .end local v6    # "i":I
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_6
    :goto_2
    return-void

    .line 620
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "i":I
    :catch_0
    move-exception v2

    .line 621
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    .line 623
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 605
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v6    # "i":I
    :catch_1
    move-exception v5

    .line 606
    .local v5, "ex":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 607
    const-string v8, "0"
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 617
    if-eqz v0, :cond_6

    .line 618
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    .line 620
    :catch_2
    move-exception v2

    .line 621
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 608
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v5    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 609
    .local v2, "e":Ljava/io/IOException;
    :goto_4
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 610
    const-string v8, "0"
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 617
    if-eqz v0, :cond_6

    .line 618
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 620
    :catch_4
    move-exception v2

    .line 621
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 611
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v3

    .line 612
    .local v3, "e1":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_5
    :try_start_7
    invoke-virtual {v3}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 617
    if-eqz v0, :cond_6

    .line 618
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_2

    .line 620
    :catch_6
    move-exception v2

    .line 621
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 613
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "e1":Ljava/lang/ArrayIndexOutOfBoundsException;
    :catch_7
    move-exception v4

    .line 614
    .local v4, "e2":Ljava/lang/NullPointerException;
    :goto_6
    :try_start_9
    invoke-virtual {v4}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 617
    if-eqz v0, :cond_6

    .line 618
    :try_start_a
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    goto :goto_2

    .line 620
    :catch_8
    move-exception v2

    .line 621
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 616
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "e2":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v10

    .line 617
    :goto_7
    if-eqz v0, :cond_7

    .line 618
    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 622
    :cond_7
    :goto_8
    throw v10

    .line 620
    :catch_9
    move-exception v2

    .line 621
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 616
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v6    # "i":I
    :catchall_1
    move-exception v10

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_7

    .line 613
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_a
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_6

    .line 611
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_b
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_5

    .line 608
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_c
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 605
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_d
    move-exception v5

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method private setModelFeature()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 182
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    move-result-object v1

    const-string v2, "get_chip_vendor"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    .line 183
    const-string v1, "TouchNoise"

    const-string v2, "TSP vendor"

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v1, "MELFAS"

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 186
    iput-boolean v4, p0, Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setHeightWidth()V

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setScreenHeightWidth()V

    .line 198
    :cond_0
    :goto_0
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/sec/tsp/raw_data"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "dataRaw":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-ne v1, v4, :cond_1

    .line 201
    iput-boolean v4, p0, Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/TouchNoise;->setNavigationBarCanHide(Landroid/content/ComponentName;)V

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/TouchNoise;->setStatusBarCanHide(Landroid/content/ComponentName;)V

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setHeightWidth()V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setScreenHeightWidth()V

    .line 206
    iget v1, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Ljava/lang/String;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    .line 207
    iget v1, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    filled-new-array {v1, v2}, [I

    move-result-object v1

    const-class v2, Ljava/lang/String;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    .line 208
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->getTimeToString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/status/TouchNoise;->FILE_TIME:Ljava/lang/String;

    .line 210
    :cond_1
    return-void

    .line 189
    .end local v0    # "dataRaw":Ljava/io/File;
    :cond_2
    const-string v1, "STM"

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 190
    iput-boolean v4, p0, Lcom/sec/android/app/status/TouchNoise;->isSTM:Z

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setHeightWidth()V

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->setScreenHeightWidth()V

    goto :goto_0

    .line 193
    :cond_3
    const-string v1, "SYNAPTICS"

    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->mTspManufacture:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/status/TouchNoiseSynaptics;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/TouchNoise;->startActivity(Landroid/content/Intent;)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->finish()V

    goto :goto_0
.end method

.method private setNavigationBarCanHide(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 406
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    .line 419
    .local v0, "windowmanager":Landroid/view/IWindowManager;
    return-void
.end method

.method private setStatusBarCanHide(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 423
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    .line 436
    .local v0, "windowmanager":Landroid/view/IWindowManager;
    return-void
.end method

.method private updateCurrentTouchNoiseStatus()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 451
    const-string v0, "/sys/class/sec/tsp_noise_test/set_threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    .line 458
    const-string v0, "/sys/class/sec/tsp_noise_test/set_refer0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    .line 459
    const-string v0, "/sys/class/sec/tsp_noise_test/set_refer1"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    .line 460
    const-string v0, "/sys/class/sec/tsp_noise_test/set_refer2"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    .line 461
    const-string v0, "/sys/class/sec/tsp_noise_test/set_refer3"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    .line 462
    const-string v0, "/sys/class/sec/tsp_noise_test/set_refer4"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    .line 467
    const-string v0, "/sys/class/sec/tsp_noise_test/set_delta0"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_0:Ljava/lang/String;

    .line 468
    const-string v0, "/sys/class/sec/tsp_noise_test/set_delta1"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_1:Ljava/lang/String;

    .line 469
    const-string v0, "/sys/class/sec/tsp_noise_test/set_delta2"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_2:Ljava/lang/String;

    .line 470
    const-string v0, "/sys/class/sec/tsp_noise_test/set_delta3"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_3:Ljava/lang/String;

    .line 471
    const-string v0, "/sys/class/sec/tsp_noise_test/set_delta4"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->getValueFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;

    .line 476
    return-void
.end method

.method private updateCurrentTouchNoiseStatus_melfas()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 716
    const/4 v6, 0x5

    new-array v0, v6, [Ljava/lang/String;

    const-string v6, "1,1"

    aput-object v6, v0, v10

    const-string v6, "1,9"

    aput-object v6, v0, v11

    const-string v6, "9,5"

    aput-object v6, v0, v12

    const-string v6, "17,1"

    aput-object v6, v0, v13

    const-string v6, "17,9"

    aput-object v6, v0, v14

    .line 719
    .local v0, "coordinates":[Ljava/lang/String;
    const/4 v6, 0x5

    new-array v1, v6, [Ljava/lang/String;

    const-string v6, "1,1"

    aput-object v6, v1, v10

    const-string v6, "1,12"

    aput-object v6, v1, v11

    const-string v6, "13,7"

    aput-object v6, v1, v12

    const-string v6, "24,1"

    aput-object v6, v1, v13

    const-string v6, "24,12"

    aput-object v6, v1, v14

    .line 722
    .local v1, "coordinatesForM3":[Ljava/lang/String;
    const/4 v6, 0x5

    new-array v3, v6, [Ljava/lang/String;

    const-string v6, "1,1"

    aput-object v6, v3, v10

    const-string v6, "1,15"

    aput-object v6, v3, v11

    const-string v6, "14,8"

    aput-object v6, v3, v12

    const-string v6, "28,1"

    aput-object v6, v3, v13

    const-string v6, "28,15"

    aput-object v6, v3, v14

    .line 725
    .local v3, "coordinatesForT0":[Ljava/lang/String;
    const/4 v6, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v6, "1,1"

    aput-object v6, v2, v10

    const-string v6, "1,25"

    aput-object v6, v2, v11

    const-string v6, "7,13"

    aput-object v6, v2, v12

    const-string v6, "13,1"

    aput-object v6, v2, v13

    const-string v6, "13,25"

    aput-object v6, v2, v14

    .line 729
    .local v2, "coordinatesForSuperior":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    const-string v7, "i9260"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 730
    move-object v0, v2

    .line 737
    :cond_0
    :goto_0
    const-string v6, "get_threshold"

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    .line 738
    const-string v6, "TouchNoise"

    const-string v7, "updateCurrentTouchNoiseStatus_melfas"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "runReferenceRead_result = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    const-string v6, "run_reference_read"

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 740
    .local v5, "runReferenceRead_result":Ljava/lang/String;
    const-string v6, "TouchNoise"

    const-string v7, "updateCurrentTouchNoiseStatus_melfas"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "runReferenceRead_result = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_reference,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    .line 742
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_reference,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    .line 743
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_reference,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    .line 744
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_reference,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v13

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    .line 745
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_reference,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v14

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    .line 746
    const-string v6, "TouchNoise"

    const-string v7, "requestGripSensorOn"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "refer_0="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "refer_1="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "refer_2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "refer_3="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "refer_4="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v6, "run_intensity_read"

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 750
    .local v4, "runIntensityRead_result":Ljava/lang/String;
    const-string v6, "TouchNoise"

    const-string v7, "updateCurrentTouchNoiseStatus_melfas"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "runIntensityRead_result = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_intensity,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v10

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;

    .line 752
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_intensity,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v11

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;

    .line 753
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_intensity,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v12

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;

    .line 754
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_intensity,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v13

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;

    .line 755
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get_intensity,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v0, v14

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;

    .line 756
    const-string v6, "TouchNoise"

    const-string v7, "updateCurrentTouchNoiseStatus_melfas"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "intencity_0="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "intencity_1="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "intencity_2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "intencity_3="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "intencity_4="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    return-void

    .line 731
    .end local v4    # "runIntensityRead_result":Ljava/lang/String;
    .end local v5    # "runReferenceRead_result":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    const-string v7, "n710"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    const-string v7, "i317"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    const-string v7, "t889"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 732
    :cond_2
    move-object v0, v3

    goto/16 :goto_0

    .line 733
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->productModel:Ljava/lang/String;

    const-string v7, "i9305"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 734
    move-object v0, v1

    goto/16 :goto_0
.end method

.method private updateCurrentTouchNoiseStatus_melfas2()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 628
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->readRawData_melfas()V

    .line 629
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    .line 630
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    .line 631
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    .line 632
    const-string v2, "TouchNoise"

    const-string v3, "updateCurrentTouchNoiseStatus_melfas2"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "refer_0="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "refer_1="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "refer_2="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    .line 636
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    .line 638
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_5:Ljava/lang/String;

    .line 640
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_6:Ljava/lang/String;

    .line 643
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_7:Ljava/lang/String;

    .line 646
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_8:Ljava/lang/String;

    .line 649
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_9:Ljava/lang/String;

    .line 651
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_10:Ljava/lang/String;

    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_11:Ljava/lang/String;

    .line 655
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_12:Ljava/lang/String;

    .line 658
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_13:Ljava/lang/String;

    .line 661
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_14:Ljava/lang/String;

    .line 664
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_0:Ljava/lang/String;

    .line 666
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_1:Ljava/lang/String;

    .line 668
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_2:Ljava/lang/String;

    .line 670
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_3:Ljava/lang/String;

    .line 672
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_4:Ljava/lang/String;

    .line 674
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    aget-object v3, v3, v8

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_5:Ljava/lang/String;

    .line 676
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_6:Ljava/lang/String;

    .line 679
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_7:Ljava/lang/String;

    .line 682
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_8:Ljava/lang/String;

    .line 685
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_9:Ljava/lang/String;

    .line 687
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_10:Ljava/lang/String;

    .line 689
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v6

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v7

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v3, v3, v8

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_11:Ljava/lang/String;

    .line 691
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_12:Ljava/lang/String;

    .line 694
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_13:Ljava/lang/String;

    .line 697
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x2

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_intensity_14:Ljava/lang/String;

    .line 701
    iget-boolean v2, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    if-eqz v2, :cond_2

    .line 702
    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->RawDiff_Buf:Ljava/lang/StringBuffer;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 704
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    if-ge v0, v2, :cond_1

    .line 705
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    if-ge v1, v2, :cond_0

    .line 706
    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->RawDiff_Buf:Ljava/lang/StringBuffer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryRef:[[Ljava/lang/String;

    aget-object v4, v4, v0

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoise;->rawArryInt:[[Ljava/lang/String;

    aget-object v4, v4, v0

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 705
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 704
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 710
    .end local v1    # "j":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/TouchNoise;->RawDiff_Buf:Ljava/lang/StringBuffer;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 711
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->WriteToStorage()V

    .line 713
    .end local v0    # "i":I
    :cond_2
    return-void
.end method

.method private updateCurrentTouchNoiseStatus_stm()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 762
    const/4 v3, 0x5

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "11,18"

    aput-object v3, v0, v7

    const-string v3, "1,18"

    aput-object v3, v0, v8

    const-string v3, "6,10"

    aput-object v3, v0, v9

    const-string v3, "11,1"

    aput-object v3, v0, v10

    const-string v3, "1,1"

    aput-object v3, v0, v11

    .line 765
    .local v0, "coordinates":[Ljava/lang/String;
    const-string v3, "get_threshold"

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    .line 766
    const-string v3, "TouchNoise"

    const-string v4, "updateCurrentTouchNoiseStatus_stm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runReferenceRead_result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const-string v3, "run_reference_read"

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 768
    .local v2, "runReferenceRead_result":Ljava/lang/String;
    const-string v3, "TouchNoise"

    const-string v4, "updateCurrentTouchNoiseStatus_stm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runReferenceRead_result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_reference,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    .line 770
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_reference,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    .line 771
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_reference,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    .line 772
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_reference,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v10

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    .line 773
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_reference,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v11

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    .line 774
    const-string v3, "TouchNoise"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "refer_0="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_0:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "refer_1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_1:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "refer_2="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_2:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "refer_3="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_3:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "refer_4="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_refer_4:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    const-string v3, "run_delta_read"

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 778
    .local v1, "runIntensityRead_result":Ljava/lang/String;
    const-string v3, "TouchNoise"

    const-string v4, "updateCurrentTouchNoiseStatus_stm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runIntensityRead_result = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_delta,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_0:Ljava/lang/String;

    .line 780
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_delta,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_1:Ljava/lang/String;

    .line 781
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_delta,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v9

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_2:Ljava/lang/String;

    .line 782
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_delta,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v10

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_3:Ljava/lang/String;

    .line 783
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_delta,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v11

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;

    .line 784
    const-string v3, "TouchNoise"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "delta_0="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_0:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "delta_1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_1:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "delta_2="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_2:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "delta_3="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_3:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "delta_4="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoise;->threshold_delta_4:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 213
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 215
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 219
    :cond_0
    const/16 v0, 0x13

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    .line 220
    const/16 v0, 0xb

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    .line 221
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->font_size:I

    .line 222
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->setModelFeature()V

    .line 223
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    .line 224
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    .line 225
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    .line 226
    iget v0, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    .line 227
    const-string v0, "TouchNoise"

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isMelfas= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v0, "TouchNoise"

    const-string v1, "onCreate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isSTM= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/TouchNoise;->isSTM:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->initPaint()V

    .line 230
    new-instance v0, Lcom/sec/android/app/status/TouchNoise$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/TouchNoise$MyView;-><init>(Lcom/sec/android/app/status/TouchNoise;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TouchNoise;->setContentView(Landroid/view/View;)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 232
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 237
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v2, 0x19

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 256
    iget-boolean v1, p0, Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z

    if-eqz v1, :cond_5

    .line 259
    if-ne p1, v2, :cond_2

    .line 260
    const-string v1, "TouchNoise"

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_VOLUME_DOWN"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-boolean v1, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    if-eqz v1, :cond_1

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "data is saving.."

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 265
    .local v0, "ReadTotoast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 266
    iput-boolean v4, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    .line 297
    .end local v0    # "ReadTotoast":Landroid/widget/Toast;
    :cond_0
    :goto_0
    return v6

    .line 268
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->finish()V

    goto :goto_0

    .line 270
    :cond_2
    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    .line 271
    const-string v1, "TouchNoise"

    const-string v2, "onKeyDown"

    const-string v3, "KEYCODE_VOLUME_UP"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoise;->checkExternalSdcard()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 274
    iget-boolean v1, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    if-nez v1, :cond_3

    .line 275
    iput-boolean v6, p0, Lcom/sec/android/app/status/TouchNoise;->isSaving:Z

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Start TSP data Save"

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 278
    .restart local v0    # "ReadTotoast":Landroid/widget/Toast;
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise;->mSavingHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const-wide/32 v4, 0x1d4c0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 284
    :goto_1
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 280
    .end local v0    # "ReadTotoast":Landroid/widget/Toast;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "data is saving.."

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .restart local v0    # "ReadTotoast":Landroid/widget/Toast;
    goto :goto_1

    .line 286
    .end local v0    # "ReadTotoast":Landroid/widget/Toast;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "No SD card detected.."

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 288
    .restart local v0    # "ReadTotoast":Landroid/widget/Toast;
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 292
    .end local v0    # "ReadTotoast":Landroid/widget/Toast;
    :cond_5
    if-ne p1, v2, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoise;->finish()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 241
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 242
    iput v1, p0, Lcom/sec/android/app/status/TouchNoise;->touchX:I

    .line 243
    iput v1, p0, Lcom/sec/android/app/status/TouchNoise;->touchY:I

    .line 250
    :goto_0
    return v2

    .line 245
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchX:I

    .line 246
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoise;->touchY:I

    goto :goto_0
.end method

.method public readResult()Ljava/lang/String;
    .locals 8

    .prologue
    .line 524
    const-string v4, "TouchNoise"

    const-string v5, "readResult"

    const-string v6, "***readResult ()"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/4 v3, 0x0

    .line 526
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 529
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_result"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 530
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 531
    const-string v4, "TouchNoise"

    const-string v5, "readResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 537
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 544
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    return-object v3

    .line 538
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 540
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 542
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 532
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 534
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 537
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 538
    :catch_2
    move-exception v2

    .line 540
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 536
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 537
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 541
    :goto_3
    throw v4

    .line 538
    :catch_3
    move-exception v2

    .line 540
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 536
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 532
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public readStatus()Ljava/lang/String;
    .locals 8

    .prologue
    .line 502
    const-string v4, "TouchNoise"

    const-string v5, "readStatus"

    const-string v6, "***readStatus ()"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const/4 v3, 0x0

    .line 504
    .local v3, "status":Ljava/lang/String;
    const/4 v0, 0x0

    .line 507
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_status"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 509
    const-string v4, "TouchNoise"

    const-string v5, "readStatus"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "status = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 514
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 520
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    return-object v3

    .line 515
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 516
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 518
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 510
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 511
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 514
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 515
    :catch_2
    move-exception v2

    .line 516
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 513
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 514
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 517
    :goto_3
    throw v4

    .line 515
    :catch_3
    move-exception v2

    .line 516
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 513
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 510
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public setHeightWidth()V
    .locals 8

    .prologue
    .line 320
    const-string v4, "TouchNoise"

    const-string v5, "setHeightWidth"

    const-string v6, "setHeightWidth"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const-string v4, "get_x_num"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 323
    .local v2, "tmpX":Ljava/lang/String;
    const-string v4, "get_y_num"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/TouchNoise;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 324
    .local v3, "tmpY":Ljava/lang/String;
    const-string v4, "ro.product.model"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 327
    .local v1, "productModel":Ljava/lang/String;
    :try_start_0
    const-string v4, "n710"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "i317"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "t889"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "i9305"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 329
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    .line 330
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_0
    const-string v4, "TouchNoise"

    const-string v5, "setHeightWidth"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setHeightWidth height = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " width = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void

    .line 332
    :cond_1
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    .line 333
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 337
    const/16 v4, 0x13

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    .line 338
    const/16 v4, 0xb

    iput v4, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    goto :goto_0
.end method

.method public setScreenHeightWidth()V
    .locals 6

    .prologue
    .line 439
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/TouchNoise;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 441
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 442
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 443
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    .line 444
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    .line 445
    const-string v2, "TouchNoise"

    const-string v3, "setScreenHeightWidth"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Screen size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_height:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoise;->HEIGHT_BASIS:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->col_height:F

    .line 447
    iget v2, p0, Lcom/sec/android/app/status/TouchNoise;->lcd_width:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoise;->WIDTH_BASIS:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/status/TouchNoise;->col_width:F

    .line 448
    return-void
.end method

.method public writeCmd(Ljava/lang/String;)V
    .locals 7
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 483
    const-string v3, "TouchNoise"

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writeCmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    const/4 v0, 0x0

    .line 487
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/sys/class/sec/tsp/cmd"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 489
    const-string v3, "TouchNoise"

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "write("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 494
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 499
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 495
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v2

    .line 496
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 498
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 490
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 491
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 494
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 495
    :catch_2
    move-exception v2

    .line 496
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 493
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 494
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 497
    :goto_3
    throw v3

    .line 495
    :catch_3
    move-exception v2

    .line 496
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 493
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 490
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_1
.end method
