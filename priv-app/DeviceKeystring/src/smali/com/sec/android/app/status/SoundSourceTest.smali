.class public Lcom/sec/android/app/status/SoundSourceTest;
.super Landroid/app/Activity;
.source "SoundSourceTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final AUDIO_PATH:[Ljava/lang/String;

.field private final CLASS_NAME:Ljava/lang/String;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBasic_bell_L:Landroid/widget/ToggleButton;

.field private mBasic_bell_R:Landroid/widget/ToggleButton;

.field private mBasic_bell_ST:Landroid/widget/ToggleButton;

.field private mBasic_tone_L:Landroid/widget/ToggleButton;

.field private mBasic_tone_R:Landroid/widget/ToggleButton;

.field private mBasic_tone_ST:Landroid/widget/ToggleButton;

.field private mBugs_story_L:Landroid/widget/ToggleButton;

.field private mBugs_story_R:Landroid/widget/ToggleButton;

.field private mBugs_story_ST:Landroid/widget/ToggleButton;

.field private mChime_L:Landroid/widget/ToggleButton;

.field private mChime_R:Landroid/widget/ToggleButton;

.field private mChime_ST:Landroid/widget/ToggleButton;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOver_the_horizon_2013_L:Landroid/widget/ToggleButton;

.field private mOver_the_horizon_2013_R:Landroid/widget/ToggleButton;

.field private mOver_the_horizon_2013_ST:Landroid/widget/ToggleButton;

.field private mRolling_tone_L:Landroid/widget/ToggleButton;

.field private mRolling_tone_R:Landroid/widget/ToggleButton;

.field private mRolling_tone_ST:Landroid/widget/ToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const-string v0, "sound source test"

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->CLASS_NAME:Ljava/lang/String;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "spk"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rcv"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ear"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "hdmi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "off"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->AUDIO_PATH:[Ljava/lang/String;

    return-void
.end method

.method private release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 263
    const-string v0, "sound source test"

    const-string v1, "release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 266
    iput-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 268
    :cond_0
    return-void
.end method

.method private startMedia(I)V
    .locals 3
    .param p1, "resID"    # I

    .prologue
    .line 246
    const-string v0, "sound source test"

    const-string v1, "startMedia"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-direct {p0}, Lcom/sec/android/app/status/SoundSourceTest;->release()V

    .line 248
    invoke-static {p0, p1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 252
    return-void
.end method

.method private stopMedia()V
    .locals 3

    .prologue
    .line 255
    const-string v0, "sound source test"

    const-string v1, "stopMedia"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 258
    invoke-direct {p0}, Lcom/sec/android/app/status/SoundSourceTest;->release()V

    .line 260
    :cond_0
    return-void
.end method

.method private updateMedia(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 171
    const-string v2, "sound source test"

    const-string v3, "updateMedia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p1

    .line 172
    check-cast v1, Landroid/widget/ToggleButton;

    .line 173
    .local v1, "toggle":Landroid/widget/ToggleButton;
    invoke-direct {p0}, Lcom/sec/android/app/status/SoundSourceTest;->stopMedia()V

    .line 175
    const-string v2, "sound source test"

    const-string v3, "updateMedia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "toggle.isChecked() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    const/4 v0, 0x0

    .line 179
    .local v0, "resID":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 241
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->startMedia(I)V

    .line 243
    .end local v0    # "resID":I
    :cond_0
    return-void

    .line 181
    .restart local v0    # "resID":I
    :pswitch_0
    const/high16 v0, 0x7f040000

    .line 182
    goto :goto_0

    .line 184
    :pswitch_1
    const v0, 0x7f040001

    .line 185
    goto :goto_0

    .line 187
    :pswitch_2
    const v0, 0x7f040002

    .line 188
    goto :goto_0

    .line 191
    :pswitch_3
    const v0, 0x7f040003

    .line 192
    goto :goto_0

    .line 194
    :pswitch_4
    const v0, 0x7f040004

    .line 195
    goto :goto_0

    .line 197
    :pswitch_5
    const v0, 0x7f040005

    .line 198
    goto :goto_0

    .line 201
    :pswitch_6
    const v0, 0x7f040006

    .line 202
    goto :goto_0

    .line 204
    :pswitch_7
    const v0, 0x7f040007

    .line 205
    goto :goto_0

    .line 207
    :pswitch_8
    const v0, 0x7f040008

    .line 208
    goto :goto_0

    .line 211
    :pswitch_9
    const v0, 0x7f04000a

    .line 212
    goto :goto_0

    .line 214
    :pswitch_a
    const v0, 0x7f04000b

    .line 215
    goto :goto_0

    .line 217
    :pswitch_b
    const v0, 0x7f04000c

    .line 218
    goto :goto_0

    .line 221
    :pswitch_c
    const v0, 0x7f040010

    .line 222
    goto :goto_0

    .line 224
    :pswitch_d
    const v0, 0x7f040011

    .line 225
    goto :goto_0

    .line 227
    :pswitch_e
    const v0, 0x7f040012

    .line 228
    goto :goto_0

    .line 231
    :pswitch_f
    const v0, 0x7f040015

    .line 232
    goto :goto_0

    .line 234
    :pswitch_10
    const v0, 0x7f040016

    .line 235
    goto :goto_0

    .line 237
    :pswitch_11
    const v0, 0x7f040017

    goto :goto_0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a01d2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method private updateToggle(I)V
    .locals 4
    .param p1, "resID"    # I

    .prologue
    const/4 v1, 0x0

    .line 142
    const-string v0, "sound source test"

    const-string v2, "updateToggle"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d3

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_R:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d4

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d5

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d6

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_R:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d7

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_5
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d8

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01d9

    if-ne p1, v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_7
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_R:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01da

    if-ne p1, v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_8
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01db

    if-ne p1, v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_9
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01dc

    if-ne p1, v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_a
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_R:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01dd

    if-ne p1, v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_b
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01de

    if-ne p1, v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_c
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01df

    if-ne p1, v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_d
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_R:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01e0

    if-ne p1, v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_e
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_ST:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01e1

    if-ne p1, v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_f
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_L:Landroid/widget/ToggleButton;

    const v0, 0x7f0a01e2

    if-ne p1, v0, :cond_11

    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v0

    :goto_10
    invoke-virtual {v2, v0}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_R:Landroid/widget/ToggleButton;

    const v2, 0x7f0a01e3

    if-ne p1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_R:Landroid/widget/ToggleButton;

    invoke-virtual {v1}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 167
    const-string v0, "sound source test"

    const-string v1, "updateToggle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void

    :cond_1
    move v0, v1

    .line 143
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 144
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 145
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 147
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 148
    goto/16 :goto_4

    :cond_6
    move v0, v1

    .line 149
    goto/16 :goto_5

    :cond_7
    move v0, v1

    .line 151
    goto/16 :goto_6

    :cond_8
    move v0, v1

    .line 152
    goto/16 :goto_7

    :cond_9
    move v0, v1

    .line 153
    goto/16 :goto_8

    :cond_a
    move v0, v1

    .line 155
    goto/16 :goto_9

    :cond_b
    move v0, v1

    .line 156
    goto/16 :goto_a

    :cond_c
    move v0, v1

    .line 157
    goto/16 :goto_b

    :cond_d
    move v0, v1

    .line 159
    goto/16 :goto_c

    :cond_e
    move v0, v1

    .line 160
    goto/16 :goto_d

    :cond_f
    move v0, v1

    .line 161
    goto :goto_e

    :cond_10
    move v0, v1

    .line 163
    goto :goto_f

    :cond_11
    move v0, v1

    .line 164
    goto :goto_10
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    const-string v0, "sound source test"

    const-string v1, "onClick"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->updateToggle(I)V

    .line 138
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/SoundSourceTest;->updateMedia(Landroid/view/View;)V

    .line 139
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    const v0, 0x7f030060

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->setContentView(I)V

    .line 62
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mAudioManager:Landroid/media/AudioManager;

    .line 63
    const v0, 0x7f0a01d2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_ST:Landroid/widget/ToggleButton;

    .line 64
    const v0, 0x7f0a01d3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_L:Landroid/widget/ToggleButton;

    .line 65
    const v0, 0x7f0a01d4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_R:Landroid/widget/ToggleButton;

    .line 67
    const v0, 0x7f0a01d5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_ST:Landroid/widget/ToggleButton;

    .line 68
    const v0, 0x7f0a01d6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_L:Landroid/widget/ToggleButton;

    .line 69
    const v0, 0x7f0a01d7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_R:Landroid/widget/ToggleButton;

    .line 71
    const v0, 0x7f0a01d8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_ST:Landroid/widget/ToggleButton;

    .line 72
    const v0, 0x7f0a01d9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_L:Landroid/widget/ToggleButton;

    .line 73
    const v0, 0x7f0a01da

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_R:Landroid/widget/ToggleButton;

    .line 75
    const v0, 0x7f0a01db

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_ST:Landroid/widget/ToggleButton;

    .line 76
    const v0, 0x7f0a01dc

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_L:Landroid/widget/ToggleButton;

    .line 77
    const v0, 0x7f0a01dd

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_R:Landroid/widget/ToggleButton;

    .line 79
    const v0, 0x7f0a01de

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_ST:Landroid/widget/ToggleButton;

    .line 80
    const v0, 0x7f0a01df

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_L:Landroid/widget/ToggleButton;

    .line 81
    const v0, 0x7f0a01e0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_R:Landroid/widget/ToggleButton;

    .line 83
    const v0, 0x7f0a01e1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_ST:Landroid/widget/ToggleButton;

    .line 84
    const v0, 0x7f0a01e2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_L:Landroid/widget/ToggleButton;

    .line 85
    const v0, 0x7f0a01e3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SoundSourceTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_R:Landroid/widget/ToggleButton;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_bell_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBasic_tone_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mBugs_story_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mChime_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mOver_the_horizon_2013_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_ST:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_L:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mRolling_tone_R:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    .line 122
    const-string v1, "sound source test"

    const-string v2, "onPause"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 124
    invoke-direct {p0}, Lcom/sec/android/app/status/SoundSourceTest;->stopMedia()V

    .line 127
    :try_start_0
    const-string v1, "sound source test"

    const-string v2, "onPause"

    const-string v3, "wait 200ms... release media player"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-wide/16 v2, 0xc8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 114
    const-string v0, "sound source test"

    const-string v1, "onResume"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/SoundSourceTest;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/SoundSourceTest;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 118
    return-void
.end method
