.class Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->startThreadGettingRawData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 168
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$900(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_2

    .line 170
    const-string v6, "GRIP_SENSOR_RAWDATA"

    invoke-static {v6, v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 171
    .local v4, "rawData":Ljava/lang/String;
    const-string v6, ","

    invoke-virtual {v4, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 173
    .local v5, "spiltRawData":[Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    const-string v7, "GRIP_SENSOR_NAME"

    invoke-static {v7}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1002(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;)Ljava/lang/String;

    .line 176
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "SX9500"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "SX9306"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "AD7146"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 177
    :cond_0
    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 178
    .local v2, "nRawData":I
    const-string v6, "GRIP_SENSOR_MODE"

    invoke-static {v6}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "gripMode":Ljava/lang/String;
    const-string v6, "0"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 180
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    const-string v7, "Initial"

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1102(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;)Ljava/lang/String;

    .line 184
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 195
    .end local v0    # "gripMode":Ljava/lang/String;
    .end local v2    # "nRawData":I
    :goto_2
    const-wide/16 v6, 0x64

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 198
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->sendLogs(Ljava/lang/String;)V

    .line 203
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v4    # "rawData":Ljava/lang/String;
    .end local v5    # "spiltRawData":[Ljava/lang/String;
    :cond_2
    :goto_3
    return-void

    .line 181
    .restart local v0    # "gripMode":Ljava/lang/String;
    .restart local v2    # "nRawData":I
    .restart local v4    # "rawData":Ljava/lang/String;
    .restart local v5    # "spiltRawData":[Ljava/lang/String;
    :cond_3
    :try_start_2
    const-string v6, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 182
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    const-string v7, "Normal"

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$1102(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 188
    .end local v0    # "gripMode":Ljava/lang/String;
    .end local v2    # "nRawData":I
    :catch_1
    move-exception v3

    .line 189
    .local v3, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 190
    invoke-virtual {v3}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->sendLogs(Ljava/lang/String;)V

    goto :goto_3

    .line 186
    .end local v3    # "ne":Ljava/lang/NumberFormatException;
    :cond_4
    :try_start_3
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method

.method public sendLogs(Ljava/lang/String;)V
    .locals 3
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 160
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 161
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 162
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "log"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 164
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 165
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 166
    return-void
.end method
