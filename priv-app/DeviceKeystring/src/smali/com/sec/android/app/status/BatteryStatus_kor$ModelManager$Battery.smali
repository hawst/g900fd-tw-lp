.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;
.super Lcom/sec/android/app/status/BatteryStatus_kor$Module;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Battery"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 464
    const-string v0, "Battery"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;-><init>(Ljava/lang/String;)V

    .line 465
    return-void
.end method


# virtual methods
.method protected setModule()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 470
    .local v1, "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v0

    .line 472
    .local v0, "model_info":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    new-array v2, v7, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Revision"

    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getRevision()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 473
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->EXYNOS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    if-ne v2, v3, :cond_0

    .line 474
    new-array v2, v7, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "AG"

    const-string v5, "/sys/module/asv_exynos/parameters/asv_group"

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 477
    :cond_0
    new-array v2, v10, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Voltage Now"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/voltage_now"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Avg"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/voltage_avg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "OCV"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/batt_vfocv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 480
    new-array v2, v10, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Capacity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/capacity"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "RSoC"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/batt_read_raw_soc"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "ASoc"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/batt_read_adj_soc"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 483
    new-array v2, v10, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Current Avg"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/current_avg"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Now"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/current_now"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Max"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/current_max"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 486
    new-array v2, v10, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Status"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/status"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v9

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Health"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/health"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v7

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Present"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/present"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v8}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v2, v8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 489
    return-void
.end method
