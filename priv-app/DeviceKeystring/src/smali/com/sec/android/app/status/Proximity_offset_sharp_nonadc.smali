.class public Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;
.super Landroid/app/Activity;
.source "Proximity_offset_sharp_nonadc.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;
    }
.end annotation


# instance fields
.field private mBackview:Landroid/view/View;

.field private mHandler:Landroid/os/Handler;

.field private mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

.field private mMode1:Landroid/widget/Button;

.field private mMode2:Landroid/widget/Button;

.field private mMode_check:Landroid/widget/Button;

.field private mReset:Landroid/widget/Button;

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThd_mode:Landroid/widget/TextView;

.field private mTimer:Ljava/util/Timer;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 59
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$1;-><init>(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    .line 237
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mThd_mode:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->getThdMode()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mBackview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->startVibrate()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->stopVibrate()V

    return-void
.end method

.method private getThdMode()V
    .locals 4

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->readThdMode()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "Thd_val":Ljava/lang/String;
    const/4 v1, 0x0

    .line 163
    .local v1, "thdValue":I
    const-string v2, "NONE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 164
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 167
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 180
    :goto_0
    return-void

    .line 169
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mThd_mode:Landroid/widget/TextView;

    const-string v3, ": Default(THD*100%)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 172
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mThd_mode:Landroid/widget/TextView;

    const-string v3, ": Mode 1(THD*140%)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 175
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mThd_mode:Landroid/widget/TextView;

    const-string v3, ": Mode 2(THD*180%)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startVibrate()V
    .locals 3

    .prologue
    .line 131
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 132
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 133
    return-void

    .line 131
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibrate()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 137
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 183
    const/4 v1, 0x0

    .line 184
    .local v1, "fw":Ljava/io/FileWriter;
    const/4 v3, 0x0

    .line 187
    .local v3, "result":Z
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 189
    const/4 v3, 0x1

    .line 194
    if-eqz v2, :cond_2

    .line 196
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 203
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return v3

    .line 197
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 199
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 190
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 191
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "writeFile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException filepath : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " value : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "writeFile"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reason: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 194
    if-eqz v1, :cond_0

    .line 196
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 197
    :catch_2
    move-exception v0

    .line 198
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "writeFile"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 194
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_1

    .line 196
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 199
    :cond_1
    :goto_3
    throw v4

    .line 197
    :catch_3
    move-exception v0

    .line 198
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "Proximityoffset_sharp"

    const-string v6, "writeFile"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 194
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 190
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 141
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a016c

    if-ne v0, v1, :cond_1

    .line 142
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onClick"

    const-string v2, "Change Mode1!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 157
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a016d

    if-ne v0, v1, :cond_2

    .line 146
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onClick"

    const-string v2, "Change Mode2!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 149
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a016f

    if-ne v0, v1, :cond_3

    .line 150
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onClick"

    const-string v2, "THD Mode check!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 152
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a016e

    if-ne v0, v1, :cond_0

    .line 153
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onClick"

    const-string v2, "Thd Reset!!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "/sys/class/sensors/proximity_sensor/prox_cal"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->writeFile(Ljava/lang/String;Ljava/lang/String;)Z

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 81
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const v0, 0x7f030052

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->setContentView(I)V

    .line 84
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mVibrator:Landroid/os/Vibrator;

    .line 85
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensorManager:Landroid/hardware/SensorManager;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensor:Landroid/hardware/Sensor;

    .line 87
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mBackview:Landroid/view/View;

    .line 88
    const v0, 0x7f0a0166

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0a016a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mThd_mode:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0a016e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mReset:Landroid/widget/Button;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mReset:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v0, 0x7f0a016c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode1:Landroid/widget/Button;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode1:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v0, 0x7f0a016d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode2:Landroid/widget/Button;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode2:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f0a016f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode_check:Landroid/widget/Button;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mMode_check:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mTimer:Ljava/util/Timer;

    .line 100
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 103
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 125
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 128
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 116
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->stopVibrate()V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->access$400(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;)V

    .line 121
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 107
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 108
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->access$300(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;)V

    .line 112
    return-void
.end method

.method public readThdMode()Ljava/lang/String;
    .locals 9

    .prologue
    .line 207
    const/4 v0, 0x0

    .line 209
    .local v0, "data":Ljava/lang/String;
    const/4 v2, 0x0

    .line 212
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "sys/class/sensors/proximity_sensor/prox_cal"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 219
    :cond_0
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "readThdMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 224
    if-eqz v3, :cond_4

    .line 226
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 234
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    const-string v0, "NONE"

    .end local v0    # "data":Ljava/lang/String;
    :cond_2
    return-object v0

    .line 227
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "data":Ljava/lang/String;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "readThdMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 230
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 220
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 221
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "readThdMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " ProximitySensor IOException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 224
    if-eqz v2, :cond_1

    .line 226
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 227
    :catch_2
    move-exception v1

    .line 228
    const-string v4, "Proximityoffset_sharp"

    const-string v5, "readThdMode"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v2, :cond_3

    .line 226
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 230
    :cond_3
    :goto_3
    throw v4

    .line 227
    :catch_3
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v5, "Proximityoffset_sharp"

    const-string v6, "readThdMode"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 224
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 220
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_4
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method
