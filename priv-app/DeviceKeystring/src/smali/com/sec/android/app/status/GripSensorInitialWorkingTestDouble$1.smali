.class Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTestDouble.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x31

    const/4 v3, 0x0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$002(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$102(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor2()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    const-string v1, "/sys/class/sensors/grip_sensor/reset"

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;B)Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    const-string v1, "/sys/class/sensors/grip_sensor_2nd/reset"

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Ljava/lang/String;B)Z

    .line 88
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onCreate"

    const-string v2, "writeSysfs"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResetButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$500(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$600(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->stopThreadGettingRawData2()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$700(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->sensorOff()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1$1;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 115
    return-void
.end method
