.class public Lcom/sec/android/app/lcdtest/support/Support$Feature;
.super Ljava/lang/Object;
.source "Support.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/lcdtest/support/Support;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Feature"
.end annotation


# direct methods
.method public static getBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 171
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/lcdtest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;)Z
    invoke-static {p0, v0}, Lcom/sec/android/app/lcdtest/support/Support$Values;->access$000(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 175
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/lcdtest/support/Support$Values;->getBoolean(Ljava/lang/String;Ljava/lang/String;Z)Z
    invoke-static {p0, v0, p1}, Lcom/sec/android/app/lcdtest/support/Support$Values;->access$300(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 183
    const-string v0, "value"

    # invokes: Lcom/sec/android/app/lcdtest/support/Support$Values;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/sec/android/app/lcdtest/support/Support$Values;->access$100(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
