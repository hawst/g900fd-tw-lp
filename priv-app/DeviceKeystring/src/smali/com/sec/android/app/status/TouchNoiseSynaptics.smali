.class public Lcom/sec/android/app/status/TouchNoiseSynaptics;
.super Landroid/app/Activity;
.source "TouchNoiseSynaptics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;
    }
.end annotation


# static fields
.field private static FILE_TIME:Ljava/lang/String;


# instance fields
.field private RawDiff_Buf:Ljava/lang/StringBuffer;

.field private isSaving:Z

.field private mCanvas:Landroid/graphics/Canvas;

.field private mChannelHeight:I

.field private mChannelWidth:I

.field public mColumHeigh:F

.field public mColumWidth:F

.field public mCoordinates:[Ljava/lang/String;

.field private mCurrentX:I

.field private mCurrentY:I

.field private mDelta:[Ljava/lang/String;

.field private mDeltaRaw:[Ljava/lang/String;

.field private mFontSize:I

.field public mIsRunningThread:Z

.field private mLcdHeight:I

.field private mLcdWidth:I

.field private mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

.field public mMyView:Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

.field private mRawcap:[Ljava/lang/String;

.field private mRawcapRaw:[Ljava/lang/String;

.field private mThread:Ljava/lang/Thread;

.field private mThresholdGlove:Ljava/lang/String;

.field private mThresholdRaw:Ljava/lang/String;

.field private mTouchPoint:Ljava/lang/String;

.field private mbackgroundPaint:Landroid/graphics/Paint;

.field private mlinePaint:Landroid/graphics/Paint;

.field private mrectPaint:Landroid/graphics/Paint;

.field private mrectPaint2:Landroid/graphics/Paint;

.field private thresholdValue:Ljava/lang/String;

.field private thresholdValue_glove:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I

    .line 81
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue:Ljava/lang/String;

    .line 82
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue_glove:Ljava/lang/String;

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdRaw:Ljava/lang/String;

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdGlove:Ljava/lang/String;

    .line 85
    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentX:I

    .line 86
    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentY:I

    .line 87
    const-string v0, "0,0"

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mTouchPoint:Ljava/lang/String;

    .line 89
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0,0"

    aput-object v1, v0, v2

    const-string v1, "0,0"

    aput-object v1, v0, v3

    const-string v1, "0,0"

    aput-object v1, v0, v4

    const-string v1, "0,0"

    aput-object v1, v0, v5

    const-string v1, "0,0"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;

    .line 92
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0,0"

    aput-object v1, v0, v2

    const-string v1, "0,0"

    aput-object v1, v0, v3

    const-string v1, "0,0"

    aput-object v1, v0, v4

    const-string v1, "0,0"

    aput-object v1, v0, v5

    const-string v1, "0,0"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;

    .line 95
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0,0"

    aput-object v1, v0, v2

    const-string v1, "0,0"

    aput-object v1, v0, v3

    const-string v1, "0,0"

    aput-object v1, v0, v4

    const-string v1, "0,0"

    aput-object v1, v0, v5

    const-string v1, "0,0"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;

    .line 98
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "0,0"

    aput-object v1, v0, v2

    const-string v1, "0,0"

    aput-object v1, v0, v3

    const-string v1, "0,0"

    aput-object v1, v0, v4

    const-string v1, "0,0"

    aput-object v1, v0, v5

    const-string v1, "0,0"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mIsRunningThread:Z

    .line 107
    iput-boolean v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->isSaving:Z

    .line 111
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->RawDiff_Buf:Ljava/lang/StringBuffer;

    .line 394
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->updateCurrentTouchNoiseStatus()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mTouchPoint:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mTouchPoint:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentX:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentY:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdRaw:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue_glove:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/status/TouchNoiseSynaptics;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->thresholdValue_glove:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdGlove:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcap:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDelta:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Canvas;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/status/TouchNoiseSynaptics;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;
    .param p1, "x1"    # Landroid/graphics/Canvas;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCanvas:Landroid/graphics/Canvas;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mbackgroundPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/TouchNoiseSynaptics;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TouchNoiseSynaptics;

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    return v0
.end method

.method private getTimeToString()Ljava/lang/String;
    .locals 11

    .prologue
    .line 262
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 265
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 266
    .local v4, "month":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x5

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "day":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xb

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 268
    .local v2, "hour":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 269
    .local v3, "min":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xd

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 270
    .local v5, "sec":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 271
    .local v6, "strTime":Ljava/lang/String;
    const-string v7, "TouchNoiseSynaptics"

    const-string v8, "getTimeToString"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getTimeToString : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    return-object v6
.end method

.method private init()V
    .locals 4

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getXYChannel()V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getScreenHeightWidth()V

    .line 124
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "1,1"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    add-int/lit8 v3, v3, -0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    div-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    div-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "1,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    add-int/lit8 v3, v3, -0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    add-int/lit8 v3, v3, -0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    add-int/lit8 v3, v3, -0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    .line 130
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getTimeToString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->FILE_TIME:Ljava/lang/String;

    .line 131
    return-void
.end method

.method private initPaint()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 379
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mlinePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 383
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 386
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mbackgroundPaint:Landroid/graphics/Paint;

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mbackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mbackgroundPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 389
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mrectPaint2:Landroid/graphics/Paint;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 392
    return-void
.end method

.method private startThread()Z
    .locals 2

    .prologue
    .line 318
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;-><init>(Lcom/sec/android/app/status/TouchNoiseSynaptics;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 331
    const/4 v0, 0x1

    return v0
.end method

.method private stopThread()Z
    .locals 4

    .prologue
    .line 335
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 336
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 339
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mIsRunningThread:Z

    if-eqz v1, :cond_1

    .line 341
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 342
    :catch_0
    move-exception v0

    .line 343
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 347
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x1

    return v1
.end method

.method private updateCurrentTouchNoiseStatus()V
    .locals 7

    .prologue
    .line 351
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "get_threshold"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdRaw:Ljava/lang/String;

    .line 352
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_threshold = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdRaw:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "get_glove_sensitivity"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdGlove:Ljava/lang/String;

    .line 355
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_glove_sensitivity = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThresholdGlove:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "run_rawcap_read"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 358
    .local v2, "runRawcapReadresult":Ljava/lang/String;
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "run_rawcap_read = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 362
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_rawcap,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 363
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_rawcap["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mRawcapRaw:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "run_delta_read"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 368
    .local v1, "runDeltaRead_result":Ljava/lang/String;
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "run_delta_read = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 372
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_delta,"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 373
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "updateCurrentTouchNoiseStatus"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_delta["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCoordinates:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mDeltaRaw:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 376
    :cond_1
    return-void
.end method


# virtual methods
.method public getScreenHeightWidth()V
    .locals 6

    .prologue
    .line 305
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 307
    .local v0, "mDisplay":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 308
    .local v1, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 309
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    .line 310
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    .line 311
    const-string v2, "TouchNoiseSynaptics"

    const-string v3, "getScreenHeightWidth"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Screen size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    .line 314
    iget v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    .line 315
    return-void
.end method

.method public getXYChannel()V
    .locals 7

    .prologue
    .line 229
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "get_x_num"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 230
    .local v1, "tmpX":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v4, "get_y_num"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "tmpY":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    .line 234
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    const-string v3, "TouchNoiseSynaptics"

    const-string v4, "getXYChannel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getXYChannel height = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " width = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 237
    const/16 v3, 0x13

    iput v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    .line 238
    const/16 v3, 0xb

    iput v3, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 135
    const-string v0, "TouchNoiseSynaptics"

    const-string v1, "onCreate"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->init()V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    .line 140
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumHeigh:F

    .line 141
    iget v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mLcdWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mChannelWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mColumWidth:F

    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->initPaint()V

    .line 143
    new-instance v0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;-><init>(Lcom/sec/android/app/status/TouchNoiseSynaptics;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mMyView:Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mMyView:Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->setContentView(Landroid/view/View;)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 146
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 162
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 163
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 181
    const/16 v0, 0x19

    if-ne p1, v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->finish()V

    .line 225
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->stopThread()Z

    .line 158
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 150
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->startThread()Z

    .line 152
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 167
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 168
    iput v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentX:I

    .line 169
    iput v1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentY:I

    .line 175
    :goto_0
    return v2

    .line 171
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentX:I

    .line 172
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mCurrentY:I

    goto :goto_0
.end method
