.class Lcom/sec/android/app/status/AP_chip_test$2;
.super Ljava/lang/Object;
.source "AP_chip_test.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/AP_chip_test;->updateGUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/AP_chip_test;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/AP_chip_test;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/high16 v6, -0x10000

    .line 69
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    const-string v3, "/sys/power/asv_group_info"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/AP_chip_test;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "data":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "updateGUI, "

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "asv_group_info : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v2, ""

    if-eq v0, v2, :cond_1

    const-string v2, "NO_FILE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 74
    .local v1, "data_split":[Ljava/lang/String;
    aget-object v2, v1, v7

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "C"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data3Result_TextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    aget-object v3, v1, v7

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data1_TextView:Landroid/widget/TextView;

    const-string v3, "ASV : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data1Result_TextView:Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data1_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data1Result_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data2_TextView:Landroid/widget/TextView;

    const-string v3, "TMCB : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data2Result_TextView:Landroid/widget/TextView;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data2_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data2Result_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data3_TextView:Landroid/widget/TextView;

    const-string v3, "IDS : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data3Result_TextView:Landroid/widget/TextView;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data3_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 102
    .end local v1    # "data_split":[Ljava/lang/String;
    :goto_1
    return-void

    .line 78
    .restart local v1    # "data_split":[Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 94
    .end local v1    # "data_split":[Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    const-string v3, " error"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data1_TextView:Landroid/widget/TextView;

    const-string v3, "Data 1 : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data2_TextView:Landroid/widget/TextView;

    const-string v3, "Data 2 : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/status/AP_chip_test$2;->this$0:Lcom/sec/android/app/status/AP_chip_test;

    iget-object v2, v2, Lcom/sec/android/app/status/AP_chip_test;->data3_TextView:Landroid/widget/TextView;

    const-string v3, "Data 3 : "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
