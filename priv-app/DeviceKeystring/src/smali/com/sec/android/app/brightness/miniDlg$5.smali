.class Lcom/sec/android/app/brightness/miniDlg$5;
.super Ljava/lang/Object;
.source "miniDlg.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/brightness/miniDlg;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/brightness/miniDlg;


# direct methods
.method constructor <init>(Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg$5;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 158
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$5;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v1}, Lcom/sec/android/app/brightness/miniDlg;->getBrightnessLevel()I

    move-result v0

    .line 160
    .local v0, "brightnessLevel":I
    # getter for: Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I
    invoke-static {}, Lcom/sec/android/app/brightness/miniDlg;->access$100()[I

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 161
    add-int/lit8 v0, v0, 0x1

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$5;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # getter for: Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/brightness/miniDlg;->access$200(Lcom/sec/android/app/brightness/miniDlg;)Landroid/widget/SeekBar;

    move-result-object v1

    # getter for: Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I
    invoke-static {}, Lcom/sec/android/app/brightness/miniDlg;->access$100()[I

    move-result-object v2

    aget v2, v2, v0

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 165
    return-void
.end method
