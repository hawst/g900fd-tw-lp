.class public Lcom/sec/android/app/status/GestureSensor;
.super Landroid/app/Activity;
.source "GestureSensor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private avgData:Ljava/lang/String;

.field private avgDataValueTextView:Landroid/widget/TextView;

.field private bgView:Landroid/view/View;

.field private checkBlue:Z

.field private checkGreen:Z

.field private checkRed:Z

.field private checkYellow:Z

.field private currentData:Ljava/lang/String;

.field private dataValueTextView:Landroid/widget/TextView;

.field private directionTextView:Landroid/widget/TextView;

.field private gestureValueTextView:Landroid/widget/TextView;

.field private mFormat:Ljava/text/DecimalFormat;

.field private mGetDataCnt:I

.field private final mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

.field private mStartBtn:Landroid/widget/Button;

.field private mTotalData:[I

.field private sensorReadTimer:Ljava/util/Timer;

.field private updateTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const-string v0, "Gesture Sensor"

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->CLASS_NAME:Ljava/lang/String;

    .line 68
    iput-boolean v1, p0, Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z

    iput-boolean v1, p0, Lcom/sec/android/app/status/GestureSensor;->checkRed:Z

    iput-boolean v1, p0, Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z

    iput-boolean v1, p0, Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z

    .line 70
    const/16 v0, 0x65

    iput v0, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    .line 74
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, ".##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    .line 256
    new-instance v0, Lcom/sec/android/app/status/GestureSensor$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GestureSensor$4;-><init>(Lcom/sec/android/app/status/GestureSensor;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    return-void
.end method

.method private DirectionSetText(F)V
    .locals 4
    .param p1, "DirectionResult"    # F

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 146
    .local v0, "direction_str":Ljava/lang/String;
    const/high16 v1, 0x42880000    # 68.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_0

    .line 147
    const-string v0, "LEFT"

    .line 158
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GestureSensor;->directionTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Direction : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    return-void

    .line 148
    :cond_0
    const/high16 v1, 0x42aa0000    # 85.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    .line 149
    const-string v0, "RIGHT"

    goto :goto_0

    .line 150
    :cond_1
    const/high16 v1, 0x42a40000    # 82.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    .line 151
    const-string v0, "DOWN"

    goto :goto_0

    .line 152
    :cond_2
    const/high16 v1, 0x42980000    # 76.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_3

    .line 153
    const-string v0, "UP"

    goto :goto_0

    .line 155
    :cond_3
    const-string v0, "No Detecting"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GestureSensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/status/GestureSensor;->updateGUI()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GestureSensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/status/GestureSensor;->getSensorData()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GestureSensor;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;
    .param p1, "x1"    # F

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GestureSensor;->DirectionSetText(F)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GestureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->currentData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GestureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->dataValueTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GestureSensor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->avgData:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GestureSensor;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->avgDataValueTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/status/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/status/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GestureSensor;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/status/GestureSensor;->checkRed:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/status/GestureSensor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GestureSensor;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/sec/android/app/status/GestureSensor;->checkRed:Z

    return p1
.end method

.method private getSensorData()V
    .locals 12

    .prologue
    .line 199
    const/4 v5, 0x4

    new-array v0, v5, [I

    .line 200
    .local v0, "dir_data":[I
    const/4 v1, 0x0

    .line 201
    .local v1, "rawData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 203
    .local v3, "selfTestData":Ljava/lang/String;
    const-string v5, "/sys/class/sensors/gesture_sensor/raw_data"

    invoke-direct {p0, v5}, Lcom/sec/android/app/status/GestureSensor;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_1

    .line 206
    const-string v5, " No sysfs files"

    iput-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->currentData:Ljava/lang/String;

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    const-string v5, ","

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "rawDatas":[Ljava/lang/String;
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 210
    const/4 v5, 0x2

    const/4 v6, 0x2

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 211
    const/4 v5, 0x3

    const/4 v6, 0x3

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 212
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 213
    const/4 v5, 0x1

    const/4 v6, 0x1

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 214
    const/4 v5, 0x2

    const/4 v6, 0x2

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 215
    const/4 v5, 0x3

    const/4 v6, 0x3

    aget-object v6, v2, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v5

    .line 216
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "A : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x3

    aget v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->currentData:Ljava/lang/String;

    .line 219
    iget v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    const/16 v6, 0x64

    if-ge v5, v6, :cond_2

    .line 220
    const-string v5, "GESTURE_CROSSTALK_RAW"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    const/4 v8, 0x0

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    const/4 v8, 0x1

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 223
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    const/4 v8, 0x2

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 224
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    const/4 v8, 0x3

    aget v8, v0, v8

    add-int/2addr v7, v8

    aput v7, v5, v6

    .line 225
    iget v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    .line 238
    :cond_2
    :goto_1
    iget v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    const/16 v6, 0x64

    if-ne v5, v6, :cond_0

    .line 239
    const-string v5, "GESTURE_CROSSTALK_RAW"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 240
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RESULT - A:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    int-to-double v8, v7

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->avgData:Ljava/lang/String;

    .line 251
    :goto_2
    iget v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    goto/16 :goto_0

    .line 227
    :cond_3
    const-string v5, "Gesture Sensor"

    const-string v6, "START CROSSTALK"

    const-string v7, "GESTURE_CROSSTALK with selftest"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v5, "/sys/class/sensors/gesture_sensor/selftest"

    invoke-direct {p0, v5}, Lcom/sec/android/app/status/GestureSensor;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 229
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 230
    .local v4, "selfTestDatas":[Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 231
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x1

    const/4 v7, 0x1

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 232
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x2

    const/4 v7, 0x2

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v6, 0x3

    const/4 v7, 0x3

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 234
    const/16 v5, 0x64

    iput v5, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    goto/16 :goto_1

    .line 245
    .end local v4    # "selfTestDatas":[Ljava/lang/String;
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RESULT - A:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", B:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", C:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x2

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", D:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GestureSensor;->mFormat:Ljava/text/DecimalFormat;

    iget-object v7, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v8, 0x3

    aget v7, v7, v8

    int-to-long v8, v7

    invoke-virtual {v6, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/GestureSensor;->avgData:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 162
    const-string v4, ""

    .line 163
    .local v4, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 166
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x1fa0

    invoke-direct {v1, v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 169
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 170
    if-eqz v4, :cond_0

    .line 171
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 181
    :cond_0
    if-eqz v1, :cond_4

    .line 183
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 191
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v4, :cond_2

    .line 192
    const-string v4, ""

    .line 194
    .end local v4    # "result":Ljava/lang/String;
    :cond_2
    return-object v4

    .line 184
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v4    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 185
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 187
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 174
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 175
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "FileNotFoundException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 181
    if-eqz v0, :cond_1

    .line 183
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 184
    :catch_2
    move-exception v2

    .line 185
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 177
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 178
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 181
    if-eqz v0, :cond_1

    .line 183
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 184
    :catch_4
    move-exception v2

    .line 185
    const-string v5, "Gesture Sensor"

    const-string v6, "readOneLine"

    const-string v7, "IOException"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 181
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v0, :cond_3

    .line 183
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 187
    :cond_3
    :goto_4
    throw v5

    .line 184
    :catch_5
    move-exception v2

    .line 185
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v6, "Gesture Sensor"

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 181
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 177
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 174
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private updateGUI()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/sec/android/app/status/GestureSensor$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GestureSensor$3;-><init>(Lcom/sec/android/app/status/GestureSensor;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 141
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 122
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0074

    if-ne v0, v1, :cond_0

    .line 123
    const-string v0, "Gathering raw datas..."

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->avgData:Ljava/lang/String;

    .line 124
    iput v2, p0, Lcom/sec/android/app/status/GestureSensor;->mGetDataCnt:I

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    aput v2, v0, v2

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    const/4 v1, 0x3

    aput v2, v0, v1

    .line 130
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->setContentView(I)V

    .line 80
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mTotalData:[I

    .line 81
    const v0, 0x7f0a0072

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->gestureValueTextView:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0a0071

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->dataValueTextView:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0a0073

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->directionTextView:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0a000f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;

    .line 85
    const v0, 0x7f0a0075

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->avgDataValueTextView:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0a0074

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mStartBtn:Landroid/widget/Button;

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->mStartBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const-string v0, "sensorhub"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sensorhub/SensorHubManager;

    sput-object v0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    .line 89
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->updateTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 116
    sget-object v0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->unregisterListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/status/GestureSensor;->finish()V

    .line 118
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const-wide/16 v2, 0xc8

    .line 93
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 94
    sget-object v0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sensorhub/SensorHubManager;->getDefaultSensorHub(I)Lcom/samsung/android/sensorhub/SensorHub;

    move-result-object v6

    .line 95
    .local v6, "gesture":Lcom/samsung/android/sensorhub/SensorHub;
    sget-object v0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubManager:Lcom/samsung/android/sensorhub/SensorHubManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GestureSensor;->mSensorHubEventListener:Lcom/samsung/android/sensorhub/SensorHubEventListener;

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v6, v4}, Lcom/samsung/android/sensorhub/SensorHubManager;->registerListener(Lcom/samsung/android/sensorhub/SensorHubEventListener;Lcom/samsung/android/sensorhub/SensorHub;I)Z

    .line 97
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Gesture_Update"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->updateTimer:Ljava/util/Timer;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->updateTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/status/GestureSensor$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GestureSensor$1;-><init>(Lcom/sec/android/app/status/GestureSensor;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 103
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Gesture_Read"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor;->sensorReadTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/status/GestureSensor$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GestureSensor$2;-><init>(Lcom/sec/android/app/status/GestureSensor;)V

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0xa

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GestureSensor;->DirectionSetText(F)V

    .line 110
    return-void
.end method
