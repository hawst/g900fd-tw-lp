.class Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->releaseGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    const-string v1, "/sys/class/sensors/grip_sensor/reset"

    const/16 v2, 0x31

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$100(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;B)Z

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResetButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopThreadGettingRawData()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$300(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOff()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$400(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 91
    return-void
.end method
