.class Lcom/sec/android/app/phoneutil/UsbLogging$1;
.super Ljava/lang/Object;
.source "UsbLogging.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/phoneutil/UsbLogging;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/phoneutil/UsbLogging;


# direct methods
.method constructor <init>(Lcom/sec/android/app/phoneutil/UsbLogging;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/phoneutil/UsbLogging$1;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    const/4 v5, 0x0

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$1;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    # getter for: Lcom/sec/android/app/phoneutil/UsbLogging;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$000(Lcom/sec/android/app/phoneutil/UsbLogging;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "usblogging.preferences_name"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 72
    .local v2, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 74
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    packed-switch p2, :pswitch_data_0

    .line 102
    :goto_0
    return-void

    .line 78
    :pswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$1;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    const-string v4, "YES"

    # invokes: Lcom/sec/android/app/phoneutil/UsbLogging;->loggingenable(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$100(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V

    .line 79
    const-string v3, "Enable"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 80
    const-string v3, "Disable"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 81
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/phoneutil/UsbLogging$1;->this$0:Lcom/sec/android/app/phoneutil/UsbLogging;

    const-string v4, "NO"

    # invokes: Lcom/sec/android/app/phoneutil/UsbLogging;->loggingenable(Ljava/lang/String;)V
    invoke-static {v3, v4}, Lcom/sec/android/app/phoneutil/UsbLogging;->access$100(Lcom/sec/android/app/phoneutil/UsbLogging;Ljava/lang/String;)V

    .line 91
    const-string v3, "Enable"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 92
    const-string v3, "Disable"

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a02ac
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
