.class public Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;
.super Landroid/app/Activity;
.source "AccelerometerSensorTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field iv:Landroid/widget/ImageView;

.field private mDeviceType:Ljava/lang/String;

.field private mSensor:Landroid/hardware/Sensor;

.field mSensorListner:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;

.field private mSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 38
    const-string v0, "AccelerometerSensorTest"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->CLASS_NAME:Ljava/lang/String;

    .line 39
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mDeviceType:Ljava/lang/String;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mDeviceType:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->setContentView(I)V

    .line 45
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensor:Landroid/hardware/Sensor;

    .line 47
    const v0, 0x7f0a000e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->iv:Landroid/widget/ImageView;

    const v1, 0x7f020003

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 49
    new-instance v0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;

    invoke-direct {v0, p0}, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;-><init>(Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;)V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorListner:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;

    .line 50
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 61
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 54
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorListner:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;

    iget-object v2, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 56
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;->mSensorListner:Lcom/sec/android/app/selftestmode/AccelerometerSensorTest$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 66
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 67
    return-void
.end method
