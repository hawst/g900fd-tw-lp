.class public Lcom/sec/android/app/status/AP_chip_test;
.super Landroid/app/Activity;
.source "AP_chip_test.java"


# instance fields
.field public CLASS_NAME:Ljava/lang/String;

.field public data1Result_TextView:Landroid/widget/TextView;

.field public data1_TextView:Landroid/widget/TextView;

.field public data2Result_TextView:Landroid/widget/TextView;

.field public data2_TextView:Landroid/widget/TextView;

.field public data3Result_TextView:Landroid/widget/TextView;

.field public data3_TextView:Landroid/widget/TextView;

.field public group_TextView:Landroid/widget/TextView;

.field public updateTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const-string v0, "AP_chip_test"

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->setContentView(I)V

    .line 40
    const v0, 0x7f0a0012

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->group_TextView:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f0a0013

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data1_TextView:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f0a0015

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data2_TextView:Landroid/widget/TextView;

    .line 43
    const v0, 0x7f0a0017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data3_TextView:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0a0014

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data1Result_TextView:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0a0016

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data2Result_TextView:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f0a0018

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->data3Result_TextView:Landroid/widget/TextView;

    .line 47
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->updateTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/status/AP_chip_test;->finish()V

    .line 64
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const-wide/16 v2, 0xc8

    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 52
    new-instance v0, Ljava/util/Timer;

    const-string v1, "AP_Update"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->updateTimer:Ljava/util/Timer;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/status/AP_chip_test;->updateTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sec/android/app/status/AP_chip_test$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/AP_chip_test$1;-><init>(Lcom/sec/android/app/status/AP_chip_test;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 58
    return-void
.end method

.method public readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v4, ""

    .line 108
    .local v4, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 109
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 112
    .local v5, "value":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1fa0

    invoke-direct {v1, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 114
    if-eqz v5, :cond_2

    .line 115
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 127
    :goto_0
    if-eqz v1, :cond_4

    .line 129
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 137
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    if-nez v4, :cond_1

    .line 138
    const-string v4, ""

    .line 140
    .end local v4    # "result":Ljava/lang/String;
    :cond_1
    return-object v4

    .line 117
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v4    # "result":Ljava/lang/String;
    :cond_2
    :try_start_3
    const-string v4, ""
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 130
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 133
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .line 119
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 120
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    iget-object v6, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "FileNotFoundException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 122
    const-string v4, "NO_FILE"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 127
    if-eqz v0, :cond_0

    .line 129
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 130
    :catch_2
    move-exception v2

    .line 131
    .restart local v2    # "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 123
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 124
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    iget-object v6, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 127
    if-eqz v0, :cond_0

    .line 129
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 130
    :catch_4
    move-exception v2

    .line 131
    iget-object v6, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "readOneLine"

    const-string v8, "IOException"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 127
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v0, :cond_3

    .line 129
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 133
    :cond_3
    :goto_5
    throw v6

    .line 130
    :catch_5
    move-exception v2

    .line 131
    .restart local v2    # "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/sec/android/app/status/AP_chip_test;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "readOneLine"

    const-string v9, "IOException"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 127
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 123
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 119
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_7
    move-exception v3

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public updateGUI()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/sec/android/app/status/AP_chip_test$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/AP_chip_test$2;-><init>(Lcom/sec/android/app/status/AP_chip_test;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AP_chip_test;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 104
    return-void
.end method
