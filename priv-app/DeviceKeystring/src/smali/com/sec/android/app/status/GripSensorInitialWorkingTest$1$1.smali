.class Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTest.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 76
    const-wide/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOn()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$500(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 82
    const-wide/16 v2, 0xc8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 86
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->UpdateThreshold()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$600(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->startThreadGettingRawData()V
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$700(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1$1;->this$1:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    iget-object v1, v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResetButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 89
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0

    .line 83
    .end local v0    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 84
    .restart local v0    # "ie":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method
