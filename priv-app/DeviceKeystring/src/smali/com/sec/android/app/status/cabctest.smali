.class public Lcom/sec/android/app/status/cabctest;
.super Landroid/app/Activity;
.source "cabctest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 121
    if-eqz v2, :cond_0

    .line 122
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 128
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_1
    :goto_0
    return-void

    .line 124
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "cabctest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 127
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 118
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "cabctest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    if-eqz v1, :cond_1

    .line 122
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 124
    :catch_2
    move-exception v0

    .line 125
    const-string v3, "cabctest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 121
    :goto_2
    if-eqz v1, :cond_2

    .line 122
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 126
    :cond_2
    :goto_3
    throw v3

    .line 124
    :catch_3
    move-exception v0

    .line 125
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "cabctest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "IOException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 120
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 117
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 110
    :goto_0
    :pswitch_0
    return-void

    .line 77
    :pswitch_1
    const-string v0, "cabctest"

    const-string v1, "cabc on"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const-string v0, "/sys/class/mdnie/mdnie/cabc"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :pswitch_2
    const-string v0, "cabctest"

    const-string v1, "cabc off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v0, "/sys/class/mdnie/mdnie/cabc"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :pswitch_3
    const-string v0, "cabctest"

    const-string v1, "outside mode on"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v0, "/sys/class/mdnie/mdnie/cabc"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v0, "/sys/class/tcon/tcon/auto_br"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "/sys/class/tcon/tcon/lux"

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "/sys/class/tcon/tcon/mode"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :pswitch_4
    const-string v0, "cabctest"

    const-string v1, "outside mode off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v0, "/sys/class/mdnie/mdnie/cabc"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "/sys/class/tcon/tcon/auto_br"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v0, "/sys/class/tcon/tcon/lux"

    const-string v1, "1"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "/sys/class/tcon/tcon/mode"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :pswitch_5
    const-string v0, "cabctest"

    const-string v1, "hbm mode on"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v0, "/sys/class/backlight/panel/brightness"

    const-string v1, "255"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const-string v0, "/sys/class/backlight/panel/auto_brightness"

    const-string v1, "6"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    :pswitch_6
    const-string v0, "cabctest"

    const-string v1, "hbm mode off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v0, "/sys/class/backlight/panel/auto_brightness"

    const-string v1, "0"

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/cabctest;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x7f0a0040
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x8

    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v8, 0x7f030011

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->setContentView(I)V

    .line 32
    const v8, 0x7f0a0040

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 33
    .local v3, "buttonon":Landroid/view/View;
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    const v8, 0x7f0a0041

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 35
    .local v2, "buttonoff":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v8, 0x7f0a0042

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 37
    .local v7, "textoutside":Landroid/view/View;
    const v8, 0x7f0a0043

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 38
    .local v5, "buttonoutsideon":Landroid/view/View;
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v8, 0x7f0a0044

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 40
    .local v4, "buttonoutsideoff":Landroid/view/View;
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v8

    if-nez v8, :cond_0

    .line 42
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 44
    :cond_0
    const-string v8, "SUPPORT_OUTSIDE_MODE_TEST"

    invoke-static {v8}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 45
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 46
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 47
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 49
    :cond_1
    const v8, 0x7f0a0045

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 50
    .local v6, "texthbm":Landroid/view/View;
    const v8, 0x7f0a0046

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "buttonhbmon":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v8, 0x7f0a0047

    invoke-virtual {p0, v8}, Lcom/sec/android/app/status/cabctest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 53
    .local v0, "buttonhbmoff":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v8

    if-nez v8, :cond_2

    .line 55
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v8

    invoke-virtual {v8, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 57
    :cond_2
    const-string v8, "SUPPORT_HBM_MODE_TEST"

    invoke-static {v8}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 58
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 59
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 60
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 62
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 71
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 66
    return-void
.end method
