.class public Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;
.super Landroid/view/View;
.source "TspHoverAccuracyPoint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspHoverAccuracyPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private final HEIGHT_BASIS:I

.field private final WIDTH_BASIS:I

.field private _mmToPixel:F

.field private isTouchDown:Z

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mClickPaint:Landroid/graphics/Paint;

.field private mDotBitmap:Landroid/graphics/Bitmap;

.field private mDotCanvas:Landroid/graphics/Canvas;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field private mUntouchablePaint:Landroid/graphics/Paint;

.field maxY:F

.field minY:F

.field final synthetic this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspHoverAccuracyPoint;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 113
    iput-object p1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .line 114
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 92
    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedX:F

    .line 93
    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedY:F

    .line 94
    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    .line 95
    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    .line 107
    const/4 v0, 0x5

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->_mmToPixel:F

    .line 115
    invoke-virtual {p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    .line 116
    invoke-virtual {p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    .line 117
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 119
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 121
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mDotBitmap:Landroid/graphics/Bitmap;

    .line 122
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mDotBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mDotCanvas:Landroid/graphics/Canvas;

    .line 123
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 124
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 126
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_width_mm:F
    invoke-static {p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$000(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->_mmToPixel:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    .line 127
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_height_mm:F
    invoke-static {p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$100(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->_mmToPixel:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    div-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    .line 128
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->minY:F

    .line 129
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->maxY:F

    .line 133
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    # setter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->click:[[Z
    invoke-static {p1, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$202(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z

    .line 134
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    # setter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->draw:[[Z
    invoke-static {p1, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$302(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z

    .line 135
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    # setter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {p1, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$402(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->setPaint()V

    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->initCircle()V

    .line 142
    return-void
.end method

.method private clearCanvas(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v4, 0x0

    .line 393
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 394
    .local v0, "clearPaint":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    .line 395
    .local v2, "xmode":Landroid/graphics/Xfermode;
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 397
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 398
    .local v1, "iCnt":I
    invoke-virtual {p1, p2, v4, v4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 399
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 400
    return-void
.end method

.method private drawCircle(FFLandroid/graphics/Paint;)V
    .locals 22
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 403
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v4, v17, v18

    .line 404
    .local v4, "col_height":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v5, v17, v18

    .line 405
    .local v5, "col_width":F
    div-float v17, p1, v5

    move/from16 v0, v17

    float-to-int v6, v0

    .line 406
    .local v6, "countX":I
    div-float v17, p2, v4

    move/from16 v0, v17

    float-to-int v7, v0

    .line 407
    .local v7, "countY":I
    int-to-float v0, v6

    move/from16 v17, v0

    mul-float v2, v5, v17

    .line 408
    .local v2, "ColX":F
    int-to-float v0, v7

    move/from16 v17, v0

    mul-float v3, v4, v17

    .line 409
    .local v3, "ColY":F
    const/high16 v17, 0x40200000    # 2.5f

    mul-float v15, v5, v17

    .line 410
    .local v15, "startX":F
    const/high16 v17, 0x40200000    # 2.5f

    mul-float v16, v4, v17

    .line 411
    .local v16, "startY":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x5

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    mul-float v8, v17, v5

    .line 412
    .local v8, "deltaX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x5

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40800000    # 4.0f

    div-float v17, v17, v18

    mul-float v9, v17, v4

    .line 413
    .local v9, "deltaY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Outer_magnified_block:F
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$500(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v17

    mul-float v14, v17, v5

    .line 414
    .local v14, "radiusOuter":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Inner_magnified_block:F
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$600(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v17

    mul-float v13, v17, v5

    .line 415
    .local v13, "radiusInner":F
    const/4 v10, 0x0

    .line 417
    .local v10, "distTemp":F
    if-lez v6, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-ge v6, v0, :cond_2

    .line 418
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ge v11, v0, :cond_2

    .line 419
    int-to-float v0, v11

    move/from16 v17, v0

    mul-float v17, v17, v9

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v3, v0

    .line 421
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    const/16 v17, 0x3

    move/from16 v0, v17

    if-ge v12, v0, :cond_1

    .line 422
    int-to-float v0, v12

    move/from16 v17, v0

    mul-float v17, v17, v8

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    float-to-int v0, v15

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v2, v0

    .line 423
    sub-float v17, v2, p1

    sub-float v18, v2, p1

    mul-float v17, v17, v18

    sub-float v18, v3, p2

    sub-float v19, v3, p2

    mul-float v18, v18, v19

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v10, v0

    .line 425
    cmpg-float v17, v10, v14

    if-gez v17, :cond_0

    .line 426
    invoke-direct/range {p0 .. p2}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawPoint2(FF)V

    .line 427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v3, v14, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v3, v13, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 429
    new-instance v17, Landroid/graphics/Rect;

    const/high16 v18, 0x3fa00000    # 1.25f

    mul-float v18, v18, v5

    sub-float v18, v2, v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x3fa00000    # 1.25f

    mul-float v19, v19, v5

    sub-float v19, v3, v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x3fa00000    # 1.25f

    mul-float v20, v20, v5

    add-float v20, v20, v2

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x3fa00000    # 1.25f

    mul-float v21, v21, v5

    add-float v21, v21, v3

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    invoke-direct/range {v17 .. v21}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 421
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    .line 418
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 440
    .end local v11    # "i":I
    .end local v12    # "j":I
    :cond_2
    if-ltz v7, :cond_3

    if-ltz v6, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-gt v7, v0, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-le v6, v0, :cond_3

    .line 445
    :cond_3
    return-void
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 343
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 344
    return-void
.end method

.method private drawPoint2(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mDotCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 348
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 349
    return-void
.end method

.method private draw_down(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    .line 266
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    .line 267
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawCircle(FFLandroid/graphics/Paint;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->clearCanvas(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 269
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawPoint(FF)V

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    .line 271
    return-void
.end method

.method private draw_move(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 274
    iget-boolean v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    if-eqz v1, :cond_1

    .line 275
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 276
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedX:F

    .line 277
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedY:F

    .line 278
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    .line 279
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    .line 281
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawCircle(FFLandroid/graphics/Paint;)V

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_0
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedX:F

    .line 286
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedY:F

    .line 287
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    .line 288
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    .line 289
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iget-object v3, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawCircle(FFLandroid/graphics/Paint;)V

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->clearCanvas(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 292
    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawPoint(FF)V

    .line 293
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    .line 295
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private draw_up(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 299
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedX:F

    .line 300
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedY:F

    .line 301
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    .line 302
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 304
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    iget-object v2, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawCircle(FFLandroid/graphics/Paint;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 307
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->clearCanvas(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    .line 309
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->drawPoint(FF)V

    .line 312
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->isTouchDown:Z

    .line 314
    :cond_1
    return-void
.end method

.method private initCircle()V
    .locals 19

    .prologue
    .line 352
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    int-to-float v2, v2

    div-float v9, v1, v2

    .line 353
    .local v9, "col_height":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    int-to-float v2, v2

    div-float v10, v1, v2

    .line 354
    .local v10, "col_width":F
    const/high16 v1, 0x40200000    # 2.5f

    mul-float v17, v10, v1

    .line 355
    .local v17, "startX":F
    const/high16 v1, 0x40200000    # 2.5f

    mul-float v18, v9, v1

    .line 356
    .local v18, "startY":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    add-int/lit8 v1, v1, -0x5

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    mul-float v11, v1, v10

    .line 357
    .local v11, "deltaX":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    add-int/lit8 v1, v1, -0x5

    int-to-float v1, v1

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    mul-float v12, v1, v9

    .line 358
    .local v12, "deltaY":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Outer_magnified_block:F
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$500(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v1

    mul-float v16, v1, v10

    .line 359
    .local v16, "radiusOuter":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Inner_magnified_block:F
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$600(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F

    move-result v1

    mul-float v15, v1, v10

    .line 360
    .local v15, "radiusInner":F
    const/4 v7, 0x0

    .line 361
    .local v7, "ColX":I
    const/4 v8, 0x0

    .line 362
    .local v8, "ColY":I
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 363
    .local v6, "mRectPaint":Landroid/graphics/Paint;
    const/high16 v1, -0x1000000

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 365
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    if-ge v13, v1, :cond_1

    .line 366
    int-to-float v1, v13

    mul-float/2addr v1, v9

    float-to-int v8, v1

    .line 368
    const/4 v14, 0x0

    .local v14, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    if-ge v14, v1, :cond_0

    .line 369
    int-to-float v1, v14

    mul-float/2addr v1, v10

    float-to-int v7, v1

    .line 370
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v7

    int-to-float v3, v8

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    int-to-float v4, v4

    int-to-float v5, v8

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v7

    int-to-float v3, v8

    int-to-float v4, v7

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 372
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->draw:[[Z
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$300(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v1

    aget-object v1, v1, v13

    const/4 v2, 0x0

    aput-boolean v2, v1, v14

    .line 373
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->click:[[Z
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$200(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v1

    aget-object v1, v1, v13

    const/4 v2, 0x0

    aput-boolean v2, v1, v14

    .line 368
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 365
    :cond_0
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 377
    .end local v14    # "j":I
    :cond_1
    const/4 v13, 0x0

    :goto_2
    const/4 v1, 0x5

    if-ge v13, v1, :cond_3

    .line 378
    int-to-float v1, v13

    mul-float/2addr v1, v12

    float-to-int v1, v1

    move/from16 v0, v18

    float-to-int v2, v0

    add-int v8, v1, v2

    .line 380
    const/4 v14, 0x0

    .restart local v14    # "j":I
    :goto_3
    const/4 v1, 0x3

    if-ge v14, v1, :cond_2

    .line 381
    int-to-float v1, v14

    mul-float/2addr v1, v11

    float-to-int v1, v1

    move/from16 v0, v17

    float-to-int v2, v0

    add-int v7, v1, v2

    .line 382
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v7

    int-to-float v3, v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    move/from16 v0, v16

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v2, v7

    int-to-float v3, v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v3, v15, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->draw:[[Z
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$300(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v1

    aget-object v1, v1, v13

    const/4 v2, 0x0

    aput-boolean v2, v1, v14

    .line 385
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->click:[[Z
    invoke-static {v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$200(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v1

    aget-object v1, v1, v13

    const/4 v2, 0x0

    aput-boolean v2, v1, v14

    .line 380
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 377
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 388
    .end local v14    # "j":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mUntouchablePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->setUntouchableRect(Landroid/graphics/Paint;)V

    .line 390
    return-void
.end method

.method private setPaint()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/high16 v5, -0x10000

    const/high16 v4, -0x1000000

    const/4 v3, 0x0

    .line 145
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 154
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 155
    .local v0, "e":Landroid/graphics/PathEffect;
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mUntouchablePaint:Landroid/graphics/Paint;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mUntouchablePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mUntouchablePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 167
    return-void

    .line 154
    nop

    :array_0
    .array-data 4
        0x40400000    # 3.0f
        0x40400000    # 3.0f
    .end array-data
.end method

.method private setUntouchableRect(Landroid/graphics/Paint;)V
    .locals 15
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 170
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    int-to-float v1, v1

    div-float v8, v0, v1

    .line 171
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    int-to-float v1, v1

    div-float v9, v0, v1

    .line 172
    .local v9, "col_width":F
    const/4 v10, 0x0

    .line 173
    .local v10, "countX":I
    iget v11, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    .line 174
    .local v11, "countY":I
    iget v13, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    .line 175
    .local v13, "updown_redX":I
    const/4 v14, 0x0

    .line 176
    .local v14, "updown_redY":I
    const/4 v6, 0x0

    .line 177
    .local v6, "ColX":F
    const/4 v7, 0x0

    .line 179
    .local v7, "ColY":F
    const/4 v10, 0x0

    .line 180
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    if-ge v12, v11, :cond_1

    .line 181
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 182
    int-to-float v0, v12

    mul-float v7, v8, v0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v10

    aget-boolean v0, v0, v12

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v10

    const/4 v1, 0x1

    aput-boolean v1, v0, v12

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 188
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 180
    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 193
    :cond_1
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->WIDTH_BASIS:I

    add-int/lit8 v10, v0, -0x1

    .line 194
    const/4 v12, 0x0

    :goto_1
    if-ge v12, v11, :cond_3

    .line 195
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 196
    int-to-float v0, v12

    mul-float v7, v8, v0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v10

    aget-boolean v0, v0, v12

    if-nez v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v10

    const/4 v1, 0x1

    aput-boolean v1, v0, v12

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 202
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 194
    :cond_2
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 207
    :cond_3
    const/4 v14, 0x0

    .line 208
    const/4 v12, 0x0

    :goto_2
    if-ge v12, v13, :cond_5

    .line 209
    int-to-float v0, v14

    mul-float v7, v8, v0

    .line 210
    int-to-float v0, v12

    mul-float v6, v9, v0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v12

    aget-boolean v0, v0, v14

    if-nez v0, :cond_4

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v12

    const/4 v1, 0x1

    aput-boolean v1, v0, v14

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 217
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 208
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 223
    :cond_5
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->HEIGHT_BASIS:I

    add-int/lit8 v14, v0, -0x1

    .line 224
    const/4 v12, 0x0

    :goto_3
    if-ge v12, v13, :cond_7

    .line 225
    int-to-float v0, v14

    mul-float v7, v8, v0

    .line 226
    int-to-float v0, v12

    mul-float v6, v9, v0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v12

    aget-boolean v0, v0, v14

    if-nez v0, :cond_6

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->this$0:Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    # getter for: Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z
    invoke-static {v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z

    move-result-object v0

    aget-object v0, v0, v12

    const/4 v1, 0x1

    aput-boolean v1, v0, v14

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object/from16 v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 233
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 224
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    .line 238
    :cond_7
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->mDotBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 245
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 249
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 250
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 261
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 252
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->draw_down(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 255
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->draw_move(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 258
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;->draw_up(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
