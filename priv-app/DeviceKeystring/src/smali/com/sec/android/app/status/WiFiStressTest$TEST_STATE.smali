.class public final enum Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;
.super Ljava/lang/Enum;
.source "WiFiStressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/WiFiStressTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TEST_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

.field public static final enum START:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

.field public static final enum STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;


# instance fields
.field private mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 84
    new-instance v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    const-string v1, "START"

    const-string v2, "STOP"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->START:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    new-instance v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    const-string v1, "STOP"

    const-string v2, "START"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    .line 83
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    sget-object v1, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->START:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->STOP:Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->$VALUES:[Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->mText:Ljava/lang/String;

    .line 87
    iput-object p3, p0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->mText:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    const-class v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->$VALUES:[Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest$TEST_STATE;->mText:Ljava/lang/String;

    return-object v0
.end method
