.class public Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
.super Landroid/app/Activity;
.source "GripSensorInitialWorkingTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;
    }
.end annotation


# instance fields
.field private gripSensorName:Ljava/lang/String;

.field private mBackgroundLinearLayout:Landroid/widget/LinearLayout;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field public mInitialThreshold:I

.field public mIsWorking:Z

.field private mMode:Ljava/lang/String;

.field private mResetButton:Landroid/widget/Button;

.field private mResultTextView:Landroid/widget/TextView;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mThreadGetData:Ljava/lang/Thread;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    .line 46
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    .line 47
    iput v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mInitialThreshold:I

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mGripSensor:Landroid/hardware/Sensor;

    .line 223
    new-instance v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$3;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    .line 337
    return-void
.end method

.method private Init()V
    .locals 4

    .prologue
    .line 254
    const-string v1, "GRIP_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    const-string v2, "SX9500"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    const-string v2, "SX9306"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    const-string v2, "AD7146"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 256
    :cond_0
    const-string v1, "GRIP_SENSOR_MODE"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "gripMode":Ljava/lang/String;
    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 258
    const-string v1, "Initial"

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    .line 262
    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    if-eqz v1, :cond_3

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Working"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    .end local v0    # "gripMode":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Release"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    return-void

    .line 259
    .restart local v0    # "gripMode":Ljava/lang/String;
    :cond_2
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 260
    const-string v1, "Normal"

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    goto :goto_0

    .line 265
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Release"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 267
    .end local v0    # "gripMode":Ljava/lang/String;
    :cond_4
    const-string v1, "Initial"

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    goto :goto_1
.end method

.method private UpdateThreshold()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 119
    const-string v3, "GRIP_SENSOR_THRESHOLD"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "threshold":Ljava/lang/String;
    const-string v3, "GripSensorInitialWorkingTest"

    const-string v4, "UpdateThreshold"

    invoke-static {v3, v4, v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    if-eqz v1, :cond_0

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "thresholds":[Ljava/lang/String;
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mInitialThreshold:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_1
    return-void

    .line 121
    .end local v2    # "thresholds":[Ljava/lang/String;
    :cond_0
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "0"

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "0"

    aput-object v4, v2, v3

    goto :goto_0

    .line 124
    .restart local v2    # "thresholds":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->releaseGripSensor()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->gripSensorName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->workingGripSensor()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResetButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopThreadGettingRawData()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOff()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOn()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->UpdateThreshold()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->startThreadGettingRawData()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    return-object v0
.end method

.method private releaseGripSensor()V
    .locals 4

    .prologue
    .line 282
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "releaseGripSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopVibration()V

    .line 287
    return-void
.end method

.method private sensorOff()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 309
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    .line 310
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 311
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mGripSensor:Landroid/hardware/Sensor;

    .line 312
    return-void
.end method

.method private sensorOn()V
    .locals 5

    .prologue
    .line 290
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    if-nez v1, :cond_0

    .line 291
    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 294
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mGripSensor:Landroid/hardware/Sensor;

    .line 300
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mSensorListener:Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 302
    .local v0, "result":Z
    const-string v1, "GripSensorInitialWorkingTest"

    const-string v2, "sensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method private startThreadGettingRawData()V
    .locals 4

    .prologue
    .line 156
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "startThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopThreadGettingRawData()V

    .line 158
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$2;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 206
    return-void
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 130
    const-string v1, "GripSensorInitialWorkingTest"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 136
    .local v0, "pattern":[J
    if-nez p1, :cond_2

    .line 137
    aput-wide v6, v0, v4

    .line 138
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 144
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 146
    .end local v0    # "pattern":[J
    :cond_1
    return-void

    .line 139
    .restart local v0    # "pattern":[J
    :cond_2
    if-ne p1, v5, :cond_0

    .line 140
    aput-wide v6, v0, v4

    .line 141
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 132
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopThreadGettingRawData()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 209
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 216
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 219
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    .line 220
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 149
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 153
    :cond_0
    return-void
.end method

.method private workingGripSensor()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 274
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "workingGripSensor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mIsWorking="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mMode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->startVibration(I)V

    .line 279
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 315
    const-string v4, "GripSensorInitialWorkingTest"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const/4 v3, 0x0

    .line 317
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 319
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 321
    const/4 v3, 0x1

    .line 326
    if-eqz v2, :cond_0

    .line 327
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 333
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorInitialWorkingTest"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    return v3

    .line 329
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorInitialWorkingTest"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 332
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 322
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 323
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorInitialWorkingTest"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 326
    if-eqz v1, :cond_1

    .line 327
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 329
    :catch_2
    move-exception v0

    .line 330
    const-string v4, "GripSensorInitialWorkingTest"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 325
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 326
    :goto_2
    if-eqz v1, :cond_2

    .line 327
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 331
    :cond_2
    :goto_3
    throw v4

    .line 329
    :catch_3
    move-exception v0

    .line 330
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorInitialWorkingTest"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 325
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 322
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->setContentView(I)V

    .line 58
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mVibrator:Landroid/os/Vibrator;

    .line 60
    const v0, 0x7f0a000b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mBackgroundLinearLayout:Landroid/widget/LinearLayout;

    .line 61
    const v0, 0x7f0a00ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResultTextView:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0a00bb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResetButton:Landroid/widget/Button;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mResetButton:Landroid/widget/Button;

    new-instance v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 114
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 116
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 105
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOff()V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopThreadGettingRawData()V

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->stopVibration()V

    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 110
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->sensorOn()V

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->UpdateThreshold()V

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->startThreadGettingRawData()V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->Init()V

    .line 101
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    return-void
.end method
