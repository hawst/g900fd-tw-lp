.class Lcom/sec/android/app/status/TouchNoiseSynaptics$1;
.super Ljava/lang/Object;
.source "TouchNoiseSynaptics.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/TouchNoiseSynaptics;->startThread()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/TouchNoiseSynaptics;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # getter for: Lcom/sec/android/app/status/TouchNoiseSynaptics;->mThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$000(Lcom/sec/android/app/status/TouchNoiseSynaptics;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mIsRunningThread:Z

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    # invokes: Lcom/sec/android/app/status/TouchNoiseSynaptics;->updateCurrentTouchNoiseStatus()V
    invoke-static {v0}, Lcom/sec/android/app/status/TouchNoiseSynaptics;->access$100(Lcom/sec/android/app/status/TouchNoiseSynaptics;)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iget-object v0, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mMyView:Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;

    iget-object v0, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics$MyView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/TouchNoiseSynaptics$1;->this$0:Lcom/sec/android/app/status/TouchNoiseSynaptics;

    iput-boolean v2, v0, Lcom/sec/android/app/status/TouchNoiseSynaptics;->mIsRunningThread:Z

    .line 328
    return-void
.end method
