.class Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;
.super Ljava/util/TimerTask;
.source "Proximity_offset_sharp_nonadc.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;
    .param p2, "x1"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$1;

    .prologue
    .line 237
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->resume()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->pause()V

    return-void
.end method

.method private pause()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method

.method private resume()V
    .locals 0

    .prologue
    .line 263
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 240
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 243
    const-string v0, "Proximityoffset_sharp"

    const-string v1, "onSensorChanged"

    const-string v2, "SensorChanged!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$500(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Working"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$500(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$600(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->startVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$700(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V

    .line 256
    :goto_0
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$500(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$500(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$600(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->stopVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;->access$800(Lcom/sec/android/app/status/Proximity_offset_sharp_nonadc;)V

    goto :goto_0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 260
    return-void
.end method
