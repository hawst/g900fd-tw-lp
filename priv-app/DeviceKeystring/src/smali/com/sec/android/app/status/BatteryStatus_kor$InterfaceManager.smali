.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.super Ljava/lang/Object;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InterfaceManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;,
        Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;


# instance fields
.field private mBrightnesMode:I

.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mOrientation:I

.field private mParent:Landroid/app/Activity;

.field private mTableLayout:Landroid/widget/TableLayout;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 674
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "parent"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 696
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const v1, 0x7f0a0026

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTextView:Landroid/widget/TextView;

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 702
    return-void
.end method

.method private addButton(Landroid/widget/Button;Landroid/widget/LinearLayout;)V
    .locals 4
    .param p1, "btn"    # Landroid/widget/Button;
    .param p2, "ll"    # Landroid/widget/LinearLayout;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x2

    .line 739
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 741
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 742
    invoke-virtual {p2, p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 743
    return-void
.end method

.method private varargs addButtons([Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;)V
    .locals 9
    .param p1, "btns"    # [Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    .prologue
    const/4 v8, -0x1

    .line 745
    new-instance v5, Landroid/widget/TableRow;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v5, v6}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 746
    .local v5, "tr":Landroid/widget/TableRow;
    new-instance v4, Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v4, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 747
    .local v4, "ll":Landroid/widget/LinearLayout;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 748
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 749
    .local v1, "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->getAwaysOn()Z

    move-result v7

    invoke-direct {p0, v1, v6, v7, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->initButton(Landroid/widget/Button;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    .line 750
    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->addButton(Landroid/widget/Button;Landroid/widget/LinearLayout;)V

    .line 751
    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 748
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 753
    .end local v1    # "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    :cond_0
    invoke-virtual {v5, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 754
    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTableLayout:Landroid/widget/TableLayout;

    new-instance v7, Landroid/widget/TableLayout$LayoutParams;

    invoke-direct {v7, v8, v8}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v5, v7}, Landroid/widget/TableLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 755
    return-void
.end method

.method public static create(Landroid/app/Activity;)Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    .locals 1
    .param p0, "parent"    # Landroid/app/Activity;

    .prologue
    .line 682
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    .line 683
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 676
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    if-nez v0, :cond_0

    .line 677
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Instance is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 679
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    return-object v0
.end method

.method private initButton(Landroid/widget/Button;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "btn"    # Landroid/widget/Button;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "enable"    # Z
    .param p4, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 733
    const/high16 v0, 0x41100000    # 9.0f

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextSize(F)V

    .line 734
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 735
    invoke-virtual {p1, p4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 736
    invoke-virtual {p1, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 737
    return-void
.end method


# virtual methods
.method public createButtons()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 707
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mButtonList:Ljava/util/ArrayList;

    .line 708
    new-instance v4, Landroid/widget/TableLayout;

    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v4, v5}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTableLayout:Landroid/widget/TableLayout;

    .line 709
    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v4, v2}, Landroid/widget/TableLayout;->setStretchAllColumns(Z)V

    .line 710
    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mLinearLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTableLayout:Landroid/widget/TableLayout;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 713
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v0

    .line 714
    .local v0, "model_info":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getMode()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->NONE:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    if-ne v4, v5, :cond_1

    :cond_0
    move v2, v3

    .line 729
    :goto_0
    return v2

    .line 719
    :cond_1
    new-array v4, v2, [Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    new-instance v5, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v3

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->addButtons([Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;)V

    .line 721
    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-static {v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    move-result-object v1

    .line 722
    .local v1, "suBtn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    if-eqz v1, :cond_2

    .line 723
    new-array v4, v9, [Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    new-instance v5, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v3

    aput-object v1, v4, v2

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->addButtons([Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;)V

    .line 728
    :goto_1
    const/4 v4, 0x3

    new-array v4, v4, [Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    new-instance v5, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$FGResetSOC;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v3

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;

    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v3, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;-><init>(Landroid/content/Context;)V

    aput-object v3, v4, v2

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;

    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v3, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DataLog;-><init>(Landroid/content/Context;)V

    aput-object v3, v4, v9

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->addButtons([Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;)V

    goto :goto_0

    .line 725
    :cond_2
    new-array v4, v2, [Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    new-instance v5, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;

    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-direct {v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v3

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->addButtons([Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;)V

    goto :goto_1
.end method

.method public createHandler()V
    .locals 1

    .prologue
    .line 866
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;-><init>(Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;)V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    .line 867
    return-void
.end method

.method public getButtonList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 870
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 894
    :cond_0
    :goto_0
    return-void

    .line 872
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->updateStatus()V

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 874
    .local v1, "update_msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 877
    .end local v1    # "update_msg":Landroid/os/Message;
    :pswitch_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 878
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->updateButtons()V

    goto :goto_0

    .line 879
    :cond_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v2, v2, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    if-eqz v2, :cond_0

    .line 880
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    .line 881
    .local v0, "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->update()V

    goto :goto_0

    .line 885
    .end local v0    # "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->resetScreenOn()V

    .line 886
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->goToSleep(J)V

    goto :goto_0

    .line 889
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setScreenOn()V

    .line 890
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->wakeUp(J)V

    goto :goto_0

    .line 870
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public removeHandler()V
    .locals 2

    .prologue
    .line 901
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->removeMessages(I)V

    .line 902
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->removeMessages(I)V

    .line 903
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->removeMessages(I)V

    .line 904
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->removeMessages(I)V

    .line 905
    return-void
.end method

.method public resetBrightnessMode()V
    .locals 4

    .prologue
    .line 826
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    iget v3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mBrightnesMode:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 832
    :goto_0
    return-void

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resetOrientation()V
    .locals 2

    .prologue
    .line 798
    iget v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mOrientation:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 799
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    iget v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mOrientation:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 801
    :cond_0
    return-void
.end method

.method public resetScreenOn()V
    .locals 2

    .prologue
    .line 806
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 807
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 896
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 897
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mHandler:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->sendMessage(Landroid/os/Message;)Z

    .line 899
    return-void
.end method

.method public setBrightness(I)V
    .locals 3
    .param p1, "brightness"    # I

    .prologue
    .line 834
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 835
    .local v0, "power":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    .line 836
    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 840
    :goto_0
    return-void

    .line 838
    :cond_0
    const-string v1, "BatteryStatus"

    const-string v2, "do not access PowerManager!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setBrightnessMode()V
    .locals 4

    .prologue
    .line 811
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mBrightnesMode:I

    .line 813
    iget v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mBrightnesMode:I

    if-eqz v1, :cond_0

    .line 814
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 823
    :cond_0
    :goto_0
    return-void

    .line 818
    :catch_0
    move-exception v0

    .line 819
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v1, "BatteryStatus"

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 820
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_1
    move-exception v0

    .line 821
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLcdDimming(Z)V
    .locals 4
    .param p1, "dim"    # Z

    .prologue
    .line 853
    const/16 v0, 0x14

    .line 854
    .local v0, "brightness":I
    if-nez p1, :cond_0

    .line 856
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_brightness"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 863
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setBrightness(I)V

    .line 864
    return-void

    .line 858
    :catch_0
    move-exception v1

    .line 859
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "BatteryStatus"

    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    const/16 v0, 0x14

    goto :goto_0
.end method

.method public setOrientation()V
    .locals 7

    .prologue
    .line 781
    const/4 v3, 0x1

    .line 783
    .local v3, "org":I
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 784
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 785
    .local v0, "dp":Landroid/view/Display;
    new-instance v1, Landroid/view/DisplayInfo;

    invoke-direct {v1}, Landroid/view/DisplayInfo;-><init>()V

    .line 786
    .local v1, "dp_info":Landroid/view/DisplayInfo;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getDisplayInfo(Landroid/view/DisplayInfo;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 787
    invoke-virtual {v1}, Landroid/view/DisplayInfo;->getNaturalWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/view/DisplayInfo;->getNaturalHeight()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-le v5, v6, :cond_0

    .line 788
    const/4 v3, 0x0

    .line 794
    .end local v0    # "dp":Landroid/view/Display;
    .end local v1    # "dp_info":Landroid/view/DisplayInfo;
    .end local v4    # "wm":Landroid/view/WindowManager;
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mOrientation:I

    .line 795
    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v5, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 796
    return-void

    .line 791
    :catch_0
    move-exception v2

    .line 792
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "BatteryStatus"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setScreenOn()V
    .locals 2

    .prologue
    .line 803
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mParent:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 804
    return-void
.end method

.method public updateButtons()V
    .locals 3

    .prologue
    .line 757
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mButtonList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 758
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    .line 759
    .local v0, "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->update()V

    goto :goto_0

    .line 762
    .end local v0    # "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method public updateStatus()V
    .locals 7

    .prologue
    .line 769
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v2

    .line 770
    .local v2, "model_info":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getMode()I

    move-result v1

    .line 771
    .local v1, "mode":I
    const-string v4, ""

    .line 772
    .local v4, "text":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getModuleList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/status/BatteryStatus_kor$Module;

    .line 773
    .local v3, "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    invoke-virtual {v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->getIsView()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 774
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->updateLineStatus(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 777
    .end local v3    # "module":Lcom/sec/android/app/status/BatteryStatus_kor$Module;
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 778
    return-void
.end method
