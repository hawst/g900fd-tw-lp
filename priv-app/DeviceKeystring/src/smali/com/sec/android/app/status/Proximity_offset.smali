.class public Lcom/sec/android/app/status/Proximity_offset;
.super Landroid/app/Activity;
.source "Proximity_offset.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/Proximity_offset$handlersensor;
    }
.end annotation


# instance fields
.field private mAdcView:Landroid/widget/TextView;

.field private mBackview:Landroid/view/View;

.field private mCM36686:Z

.field private mHandler:Landroid/os/Handler;

.field private mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

.field private mHighThresholdView:Landroid/widget/TextView;

.field private mIsSupportSensorhub:Z

.field private mIsThresholdChecked:Z

.field private mLowThresholdView:Landroid/widget/TextView;

.field private mOffsetBtn:Landroid/widget/Button;

.field private mOffsetResult:Landroid/widget/TextView;

.field private mOffsetView:Landroid/widget/TextView;

.field private mProxOnOff:Landroid/widget/Button;

.field private mProxOnOffStatus:Z

.field private mProximityTitle:Landroid/widget/TextView;

.field private mResetBtn:Landroid/widget/Button;

.field private mSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTMD3782:Z

.field private mTMG399X:Z

.field private mThresholdChangeBtn:Landroid/widget/Button;

.field private mThreshold_HIGH:Landroid/widget/EditText;

.field private mThreshold_LOW:Landroid/widget/EditText;

.field private mTimer:Ljava/util/Timer;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkView:Landroid/widget/TextView;

.field private mbTMD27723:Z

.field private mnOffsetPassState:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 59
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMG399X:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mCM36686:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMD3782:Z

    .line 63
    iput v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsThresholdChecked:Z

    .line 69
    iput-boolean v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsSupportSensorhub:Z

    .line 71
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/Proximity_offset$1;-><init>(Lcom/sec/android/app/status/Proximity_offset;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    .line 363
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/Proximity_offset;)Lcom/sec/android/app/status/Proximity_offset$handlersensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mAdcView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/Proximity_offset;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/Proximity_offset;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/Proximity_offset;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mBackview:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/Proximity_offset;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset;->startVibrate()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/Proximity_offset;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset;->stopVibrate()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/Proximity_offset;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private startVibrate()V
    .locals 3

    .prologue
    .line 188
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 191
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 192
    return-void

    .line 188
    nop

    :array_0
    .array-data 8
        0x0
        0x1388
    .end array-data
.end method

.method private stopVibrate()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 196
    return-void
.end method


# virtual methods
.method public IsOffsetPass()I
    .locals 5

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "data":Ljava/lang/String;
    const-string v1, "PROXI_SENSOR_OFFSET_PASS"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    if-eqz v0, :cond_1

    .line 327
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 328
    const-string v1, "Proximityoffset"

    const-string v2, "IsOffsetPass"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IsOffsetPass: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 331
    const/4 v1, 0x1

    .line 337
    :goto_0
    return v1

    .line 332
    :cond_0
    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 333
    const/4 v1, 0x2

    goto :goto_0

    .line 337
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public ReadSensorModelName()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 342
    const/4 v0, 0x0

    .line 343
    .local v0, "data":Ljava/lang/String;
    const-string v1, "PROXI_SENSOR_NAME"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_1

    .line 346
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 347
    const-string v1, "Proximityoffset"

    const-string v2, "ReadSensorModelName"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SensorModelName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    const-string v1, "TMD27723"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TMD2672X"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 350
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    .line 360
    :cond_1
    :goto_0
    return-void

    .line 351
    :cond_2
    const-string v1, "TMG399X"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 352
    iput-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMG399X:Z

    goto :goto_0

    .line 353
    :cond_3
    const-string v1, "CM36686"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 354
    iput-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset;->mCM36686:Z

    goto :goto_0

    .line 355
    :cond_4
    const-string v1, "TMD3782"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    iput-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMD3782:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x2

    const v5, -0xffff01

    const/4 v6, 0x1

    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0167

    if-ne v2, v3, :cond_4

    .line 202
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    const-string v4, "offset"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-string v2, "PROXI_SENSOR_OFFSET"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 204
    const-string v2, "PROXI_SENSOR_OFFSET"

    const-string v3, "1"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 207
    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMG399X:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mCM36686:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMD3782:Z

    if-eqz v2, :cond_3

    .line 208
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->IsOffsetPass()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    .line 210
    iget v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    if-lez v2, :cond_2

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    const-string v3, " Pass"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    :cond_1
    :goto_0
    return-void

    .line 214
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    const-string v3, " Fail"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 218
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    const-string v3, " Pass"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 221
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0168

    if-ne v2, v3, :cond_7

    .line 222
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    const-string v4, "reset"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v2, "PROXI_SENSOR_OFFSET"

    const-string v3, "0"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 224
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMG399X:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mCM36686:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mTMD3782:Z

    if-eqz v2, :cond_6

    .line 227
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->IsOffsetPass()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    .line 230
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 231
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0165

    if-ne v2, v3, :cond_9

    .line 232
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    const-string v4, "Change threshold "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/4 v1, 0x0

    .line 234
    .local v1, "newthresh_low":Ljava/lang/String;
    const/4 v0, 0x0

    .line 235
    .local v0, "newthresh_high":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_LOW:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_HIGH:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New low thresh : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "New high thresh : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    if-eqz v1, :cond_8

    if-eqz v0, :cond_8

    .line 241
    const-string v2, "PROXI_SENSOR_SET_LOW_THRESHOLD"

    invoke-static {v2, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 242
    const-string v2, "PROXI_SENSOR_SET_HIGH_THRESHOLD"

    invoke-static {v2, v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 245
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 246
    .end local v0    # "newthresh_high":Ljava/lang/String;
    .end local v1    # "newthresh_low":Ljava/lang/String;
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0169

    if-ne v2, v3, :cond_1

    .line 247
    const-string v2, "Proximityoffset"

    const-string v3, "onClick"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Prox OnOff : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    if-ne v2, v6, :cond_a

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset$handlersensor;->pause()V
    invoke-static {v2}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->access$400(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 251
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProximityTitle:Landroid/widget/TextView;

    const-string v3, "Proximity Sensor (OFF)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    const-string v3, "Proximity ON"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 253
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    goto/16 :goto_0

    .line 255
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensor:Landroid/hardware/Sensor;

    invoke-virtual {v2, v3, v4, v7}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 256
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProximityTitle:Landroid/widget/TextView;

    const-string v3, "Proximity Sensor (ON)"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    const-string v3, "Proximity OFF"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iput-boolean v6, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset$handlersensor;->resume()V
    invoke-static {v2}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->access$300(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x2

    const/4 v5, 0x1

    .line 89
    const-string v0, "Proximityoffset"

    const-string v1, "onCreate"

    const-string v4, "onCreate"

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->ReadSensorModelName()V

    .line 93
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    if-ne v0, v5, :cond_2

    .line 94
    const-string v0, "Proximityoffset"

    const-string v1, "onCreate"

    const-string v4, "This is TMD27723 or TMD2672X sensors"

    invoke-static {v0, v1, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const v0, 0x7f030055

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->setContentView(I)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->IsOffsetPass()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I

    .line 102
    :goto_0
    const-string v0, "THRETHOLD_CHECK"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsThresholdChecked:Z

    .line 106
    :cond_0
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mVibrator:Landroid/os/Vibrator;

    .line 107
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensor:Landroid/hardware/Sensor;

    .line 109
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mBackview:Landroid/view/View;

    .line 110
    const v0, 0x7f0a0166

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0a015e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mAdcView:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0a015f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetView:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0a0160

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mLowThresholdView:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0a0161

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHighThresholdView:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetResult:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f0a0167

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetBtn:Landroid/widget/Button;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v0, 0x7f0a0168

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mResetBtn:Landroid/widget/Button;

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mResetBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f0a0169

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    const v0, 0x7f0a015d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProximityTitle:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f0a0163

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_LOW:Landroid/widget/EditText;

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_LOW:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 125
    const v0, 0x7f0a0164

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_HIGH:Landroid/widget/EditText;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThreshold_HIGH:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 127
    const v0, 0x7f0a0165

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThresholdChangeBtn:Landroid/widget/Button;

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mThresholdChangeBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mTimer:Ljava/util/Timer;

    .line 130
    new-instance v0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset;Lcom/sec/android/app/status/Proximity_offset$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    .line 132
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z

    if-ne v0, v5, :cond_3

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    const-wide/16 v4, 0x5dc

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 138
    :goto_1
    new-instance v6, Ljava/io/File;

    const-string v0, "SENSORHUB_MCU"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->getFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 139
    .local v6, "nodeSensorhub":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsSupportSensorhub:Z

    .line 141
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsSupportSensorhub:Z

    if-eqz v0, :cond_1

    .line 142
    const-string v0, "PROXI_SENSOR_ADC_AVG"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 144
    :cond_1
    return-void

    .line 98
    .end local v6    # "nodeSensorhub":Ljava/io/File;
    :cond_2
    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/Proximity_offset;->setContentView(I)V

    goto/16 :goto_0

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 178
    const-string v0, "Proximityoffset"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 182
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mIsSupportSensorhub:Z

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "PROXI_SENSOR_ADC_AVG"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 185
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 167
    const-string v0, "Proximityoffset"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 169
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 171
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset;->stopVibrate()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset$handlersensor;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->access$400(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V

    .line 173
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 148
    const-string v0, "Proximityoffset"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    iget-object v2, p0, Lcom/sec/android/app/status/Proximity_offset;->mSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 151
    iput-boolean v4, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandlersensor:Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    # invokes: Lcom/sec/android/app/status/Proximity_offset$handlersensor;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->access$300(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 156
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOffStatus:Z

    if-ne v0, v4, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    const-string v1, "Proximity OFF"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProximityTitle:Landroid/widget/TextView;

    const-string v1, "Proximity Sensor (ON)"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProxOnOff:Landroid/widget/Button;

    const-string v1, "Proximity ON"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset;->mProximityTitle:Landroid/widget/TextView;

    const-string v1, "Proximity Sensor (OFF)"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public readOffset()Ljava/lang/String;
    .locals 5

    .prologue
    .line 309
    const/4 v0, 0x0

    .line 310
    .local v0, "data":Ljava/lang/String;
    const-string v1, "PROXI_SENSOR_OFFSET"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    .line 313
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 314
    const-string v1, "Proximityoffset"

    const-string v2, "readOffset"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Offset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 318
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "NONE"

    goto :goto_0
.end method

.method public showOffset()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->readOffset()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, "OffsetValue":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, v4, :cond_0

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mOffsetView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mHighThresholdView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/status/Proximity_offset;->mLowThresholdView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    const-string v1, "Proximityoffset"

    const-string v2, "readOffset"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readOffset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/status/Proximity_offset;->readOffset()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
