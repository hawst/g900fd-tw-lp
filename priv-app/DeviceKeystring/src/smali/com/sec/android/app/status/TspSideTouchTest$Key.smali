.class Lcom/sec/android/app/status/TspSideTouchTest$Key;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"

# interfaces
.implements Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Key"
.end annotation


# instance fields
.field protected _bNvUpdate:Z

.field protected _height:I

.field protected _keyCode:I

.field protected _keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

.field protected _name:Ljava/lang/String;

.field protected _nameView:Landroid/widget/TextView;

.field protected _pX:I

.field protected _pY:I

.field protected _param:I

.field protected _textsize:F

.field protected _type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field protected _width:I

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 509
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    const-string v0, "KEY"

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_name:Ljava/lang/String;

    .line 492
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->HARD:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 500
    iput-boolean v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_bNvUpdate:Z

    .line 502
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 505
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_textsize:F

    .line 507
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    .line 510
    iput-object p2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_name:Ljava/lang/String;

    .line 511
    iput p3, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyCode:I

    .line 512
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pY:I

    .line 513
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pX:I

    .line 514
    return-void
.end method


# virtual methods
.method public addView(Landroid/widget/FrameLayout;)V
    .locals 4
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x2

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$Key;->updateView()V

    .line 550
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 552
    .local v0, "llp":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pX:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pY:I

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 553
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 554
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 555
    return-void
.end method

.method public getStatus()Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    return-object v0
.end method

.method public getType()Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    return-object v0
.end method

.method public onKey(ILandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 559
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyCode:I

    if-eq p1, v0, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 567
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 584
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$Key;->updateView()V

    goto :goto_0

    .line 570
    :pswitch_0
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->playKeySound(Landroid/content/Context;)V

    .line 572
    iget-boolean v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_bNvUpdate:Z

    if-eqz v0, :cond_2

    .line 575
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest;->access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V

    goto :goto_1

    .line 578
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->playKeySound(Landroid/content/Context;)V

    goto :goto_1

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onReset()V
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 591
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$Key;->updateView()V

    .line 592
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 619
    return-void
.end method

.method public setParam(I)V
    .locals 0
    .param p1, "param"    # I

    .prologue
    .line 524
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_param:I

    .line 525
    return-void
.end method

.method public setPosition(II)V
    .locals 0
    .param p1, "pX"    # I
    .param p2, "pY"    # I

    .prologue
    .line 529
    iput p2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pY:I

    .line 530
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_pX:I

    .line 531
    return-void
.end method

.method public setSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 535
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_width:I

    .line 536
    iput p2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_height:I

    .line 537
    return-void
.end method

.method public setTextSize(F)V
    .locals 2
    .param p1, "textSize"    # F

    .prologue
    const/high16 v1, 0x41000000    # 8.0f

    .line 601
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_textsize:F

    .line 602
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_textsize:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 603
    iput v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_textsize:F

    .line 605
    :cond_0
    return-void
.end method

.method protected updateView()V
    .locals 3

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_textsize:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$Key;->_nameView:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 545
    return-void
.end method
