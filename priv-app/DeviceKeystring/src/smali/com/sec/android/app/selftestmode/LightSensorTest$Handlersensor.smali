.class Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;
.super Ljava/lang/Object;
.source "LightSensorTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/selftestmode/LightSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Handlersensor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/selftestmode/LightSensorTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/selftestmode/LightSensorTest;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/LightSensorTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/selftestmode/LightSensorTest;Lcom/sec/android/app/selftestmode/LightSensorTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/selftestmode/LightSensorTest;
    .param p2, "x1"    # Lcom/sec/android/app/selftestmode/LightSensorTest$1;

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;-><init>(Lcom/sec/android/app/selftestmode/LightSensorTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 155
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v4, 0x0

    .line 158
    const-string v0, "LightSensorTest"

    const-string v1, "onAccuracyChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor value changed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 165
    :goto_0
    return-void

    .line 162
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/LightSensorTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/LightSensorTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    # invokes: Lcom/sec/android/app/selftestmode/LightSensorTest;->changeLightSensor(Landroid/hardware/SensorEvent;F)V
    invoke-static {v0, p1, v1}, Lcom/sec/android/app/selftestmode/LightSensorTest;->access$100(Lcom/sec/android/app/selftestmode/LightSensorTest;Landroid/hardware/SensorEvent;F)V

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
