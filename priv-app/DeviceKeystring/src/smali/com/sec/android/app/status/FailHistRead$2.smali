.class Lcom/sec/android/app/status/FailHistRead$2;
.super Ljava/lang/Object;
.source "FailHistRead.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/FailHistRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/FailHistRead;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/FailHistRead;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    # getter for: Lcom/sec/android/app/status/FailHistRead;->mBackButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/status/FailHistRead;->access$100(Lcom/sec/android/app/status/FailHistRead;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    invoke-virtual {v1}, Lcom/sec/android/app/status/FailHistRead;->finish()V

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    # getter for: Lcom/sec/android/app/status/FailHistRead;->mEraseButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/status/FailHistRead;->access$200(Lcom/sec/android/app/status/FailHistRead;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    const v2, 0x7f070152

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/FailHistRead;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    # getter for: Lcom/sec/android/app/status/FailHistRead;->mVersion:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/FailHistRead;->access$300(Lcom/sec/android/app/status/FailHistRead;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    move-result-object v0

    .line 88
    .local v0, "alertDialog":Landroid/app/DialogFragment;
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$2;->this$0:Lcom/sec/android/app/status/FailHistRead;

    invoke-virtual {v1}, Lcom/sec/android/app/status/FailHistRead;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 90
    .end local v0    # "alertDialog":Landroid/app/DialogFragment;
    :cond_1
    return-void
.end method
