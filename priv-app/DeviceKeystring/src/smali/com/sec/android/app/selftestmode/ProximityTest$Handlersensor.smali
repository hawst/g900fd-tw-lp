.class Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;
.super Ljava/lang/Object;
.source "ProximityTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/selftestmode/ProximityTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Handlersensor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/selftestmode/ProximityTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/selftestmode/ProximityTest;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/selftestmode/ProximityTest;Lcom/sec/android/app/selftestmode/ProximityTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/selftestmode/ProximityTest;
    .param p2, "x1"    # Lcom/sec/android/app/selftestmode/ProximityTest$1;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;-><init>(Lcom/sec/android/app/selftestmode/ProximityTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 118
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 121
    const-string v0, "Proximity Test"

    const-string v1, "onSensorChanged"

    const-string v2, "Sensing change"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 124
    const-string v0, "Proximity Test"

    const-string v1, "onSensorChanged"

    const-string v2, "Sensing -> 1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    # invokes: Lcom/sec/android/app/selftestmode/ProximityTest;->startVibration()V
    invoke-static {v0}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$100(Lcom/sec/android/app/selftestmode/ProximityTest;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/selftestmode/ProximityTest;->working:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$202(Lcom/sec/android/app/selftestmode/ProximityTest;Z)Z

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    # invokes: Lcom/sec/android/app/selftestmode/ProximityTest;->isWorking()V
    invoke-static {v0}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$300(Lcom/sec/android/app/selftestmode/ProximityTest;)V

    .line 134
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    # invokes: Lcom/sec/android/app/selftestmode/ProximityTest;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$400(Lcom/sec/android/app/selftestmode/ProximityTest;)V

    .line 130
    const-string v0, "Proximity Test"

    const-string v1, "onSensorChanged"

    const-string v2, "Sensing -> 0"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    # setter for: Lcom/sec/android/app/selftestmode/ProximityTest;->working:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$202(Lcom/sec/android/app/selftestmode/ProximityTest;Z)Z

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/ProximityTest$Handlersensor;->this$0:Lcom/sec/android/app/selftestmode/ProximityTest;

    # invokes: Lcom/sec/android/app/selftestmode/ProximityTest;->isWorking()V
    invoke-static {v0}, Lcom/sec/android/app/selftestmode/ProximityTest;->access$300(Lcom/sec/android/app/selftestmode/ProximityTest;)V

    goto :goto_0
.end method
