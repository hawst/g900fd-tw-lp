.class Lcom/sec/android/app/status/GripSensorTest$2;
.super Landroid/content/BroadcastReceiver;
.source "GripSensorTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorTest;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const v10, 0x7f070099

    const v9, 0x7f06001d

    const v8, 0x7f070096

    const v7, 0x7f060002

    .line 192
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 193
    .local v0, "action":Ljava/lang/String;
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Action :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v3, "com.sec.android.app.factorytest"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 196
    const-string v3, "COMMAND"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "cmdData":Ljava/lang/String;
    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "sensorData":Ljava/lang/String;
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cmdData=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sensorData=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    # getter for: Lcom/sec/android/app/status/GripSensorTest;->IS_GRIP_COUNT:I
    invoke-static {}, Lcom/sec/android/app/status/GripSensorTest;->access$000()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    .line 202
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "IS_GRIP_COUNT == 1"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v3, "0100"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 205
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$100(Lcom/sec/android/app/status/GripSensorTest;)V

    .line 206
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 208
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "________ 0100 ________"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .end local v1    # "cmdData":Ljava/lang/String;
    .end local v2    # "sensorData":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 209
    .restart local v1    # "cmdData":Ljava/lang/String;
    .restart local v2    # "sensorData":Ljava/lang/String;
    :cond_1
    const-string v3, "0000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 211
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/status/GripSensorTest;->access$400(Lcom/sec/android/app/status/GripSensorTest;I)V

    .line 213
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "________ 0000 ________"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_2
    const-string v3, "010001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "010000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "000001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 217
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 218
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 219
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 220
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 221
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/GripSensorTest;->access$400(Lcom/sec/android/app/status/GripSensorTest;I)V

    goto/16 :goto_0

    .line 223
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$100(Lcom/sec/android/app/status/GripSensorTest;)V

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 225
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 228
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 231
    :cond_5
    const-string v3, "010001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 233
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 235
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 237
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    const/4 v4, 0x1

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/status/GripSensorTest;->access$400(Lcom/sec/android/app/status/GripSensorTest;I)V

    .line 238
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_SIDE_BACK_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 239
    :cond_6
    const-string v3, "010000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 242
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 243
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 244
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/status/GripSensorTest;->access$400(Lcom/sec/android/app/status/GripSensorTest;I)V

    .line 247
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_SIDE_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 248
    :cond_7
    const-string v3, "000001000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 252
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 253
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 254
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->ActiveVibrate(I)V
    invoke-static {v3, v11}, Lcom/sec/android/app/status/GripSensorTest;->access$400(Lcom/sec/android/app/status/GripSensorTest;I)V

    .line 256
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_BACK_ON"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 257
    :cond_8
    const-string v3, "000000000000"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$100(Lcom/sec/android/app/status/GripSensorTest;)V

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 264
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 266
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->stopVibration()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$100(Lcom/sec/android/app/status/GripSensorTest;)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$200(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorTest;->access$500(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/LinearLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v5}, Lcom/sec/android/app/status/GripSensorTest;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/status/GripSensorTest;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info1:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$300(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # getter for: Lcom/sec/android/app/status/GripSensorTest;->info2:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$600(Lcom/sec/android/app/status/GripSensorTest;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 271
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "MASK_SENSING_GRIP_OFF else"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 274
    .end local v1    # "cmdData":Ljava/lang/String;
    .end local v2    # "sensorData":Ljava/lang/String;
    :cond_a
    const-string v3, "com.android.samsungtest.GripTestStop"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    # invokes: Lcom/sec/android/app/status/GripSensorTest;->sendToRilGripSensorStop()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest;->access$700(Lcom/sec/android/app/status/GripSensorTest;)V

    .line 277
    const-string v3, "GripSensor Test"

    const-string v4, "onReceive"

    const-string v5, "GRIPGRIP *******   GripSensorTest - onReceive - get Stop GripTest ******* GRIPGRIP"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest$2;->this$0:Lcom/sec/android/app/status/GripSensorTest;

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorTest;->finish()V

    goto/16 :goto_0
.end method
