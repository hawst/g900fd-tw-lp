.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SysFsUpdate"
.end annotation


# static fields
.field private static DEFAULT_DELAY:I

.field private static mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

.field private static mUpdateEn:Z

.field private static mUpdateFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1142
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    .line 1151
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->DEFAULT_DELAY:I

    .line 1152
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    .line 1153
    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/power_supply/battery/update"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateFile:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1156
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;)V

    .line 1157
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1144
    if-eqz p0, :cond_0

    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1145
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    .line 1147
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    return-object v0
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1187
    const-string v0, "%s: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Update"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-boolean v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1162
    sget-boolean v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    sput-boolean v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    .line 1163
    sget-boolean v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    if-eqz v1, :cond_0

    .line 1164
    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mInstance:Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1165
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1167
    .end local v0    # "thread":Ljava/lang/Thread;
    :cond_0
    check-cast p1, Landroid/widget/Button;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1168
    return-void

    .line 1162
    .restart local p1    # "v":Landroid/view/View;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1173
    :goto_0
    sget-boolean v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    if-eqz v1, :cond_0

    .line 1175
    :try_start_0
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateFile:Ljava/io/File;

    const-string v2, "1\n"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->writeFile(Ljava/io/File;Ljava/lang/String;Z)Z

    .line 1176
    sget v1, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->DEFAULT_DELAY:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1177
    :catch_0
    move-exception v0

    .line 1178
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1179
    sput-boolean v4, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$SysFsUpdate;->mUpdateEn:Z

    goto :goto_0

    .line 1182
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method
