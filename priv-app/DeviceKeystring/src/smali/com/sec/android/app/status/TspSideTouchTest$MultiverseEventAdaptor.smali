.class Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiverseEventAdaptor"
.end annotation


# static fields
.field private static CLASS_NAME:Ljava/lang/String;

.field private static _instance:Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

.field private static _listenerMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final STATE_KEY_DOWN:I

.field public final STATE_KEY_NONE:I

.field public final STATE_KEY_UP:I

.field private _mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1802
    const-string v0, "MultiverseEventAdaptor"

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    .line 1805
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_listenerMap:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1887
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->STATE_KEY_NONE:I

    .line 1888
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->STATE_KEY_DOWN:I

    .line 1889
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->STATE_KEY_UP:I

    .line 1810
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;
    .locals 4

    .prologue
    .line 1813
    const-class v1, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    if-nez v0, :cond_0

    .line 1814
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getInstance"

    const-string v3, "create new instance"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1815
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    invoke-direct {v0}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    .line 1816
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_listenerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 1818
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1813
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onFinish()V
    .locals 3

    .prologue
    .line 1838
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onFinish"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1840
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_listenerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 1841
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    if-eqz v0, :cond_0

    .line 1842
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;->ServiceUnregister(I)V

    .line 1843
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;->Close()V

    .line 1844
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    .line 1846
    :cond_0
    return-void
.end method

.method public onStart(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 1827
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onStart"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1829
    new-instance v1, Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    invoke-direct {v1, v5}, Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    .line 1830
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;->Open(Landroid/content/Context;)Z

    move-result v0

    .line 1831
    .local v0, "result":Z
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onStart"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Multiverse Client Open Result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1832
    if-nez v0, :cond_0

    .line 1833
    iput-object v5, p0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_mvClient:Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;

    .line 1835
    :cond_0
    return-void
.end method

.method public registerListener(ILcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;)V
    .locals 4
    .param p1, "keyId"    # I
    .param p2, "listener"    # Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;

    .prologue
    .line 1822
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "registerListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1823
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->_listenerMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1824
    return-void
.end method
