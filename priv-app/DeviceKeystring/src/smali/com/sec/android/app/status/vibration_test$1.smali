.class Lcom/sec/android/app/status/vibration_test$1;
.super Ljava/lang/Object;
.source "vibration_test.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/vibration_test;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/vibration_test;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/vibration_test;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/status/vibration_test$1;->this$0:Lcom/sec/android/app/status/vibration_test;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$1;->this$0:Lcom/sec/android/app/status/vibration_test;

    # setter for: Lcom/sec/android/app/status/vibration_test;->mMagnitude:I
    invoke-static {v0, p2}, Lcom/sec/android/app/status/vibration_test;->access$002(Lcom/sec/android/app/status/vibration_test;I)I

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$1;->this$0:Lcom/sec/android/app/status/vibration_test;

    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test$1;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mMagnitude:I
    invoke-static {v1}, Lcom/sec/android/app/status/vibration_test;->access$000(Lcom/sec/android/app/status/vibration_test;)I

    move-result v1

    # invokes: Lcom/sec/android/app/status/vibration_test;->setMagnitudeValue(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/vibration_test;->access$100(Lcom/sec/android/app/status/vibration_test;I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$1;->this$0:Lcom/sec/android/app/status/vibration_test;

    # invokes: Lcom/sec/android/app/status/vibration_test;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/status/vibration_test;->access$200(Lcom/sec/android/app/status/vibration_test;)V

    .line 102
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 97
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 94
    return-void
.end method
