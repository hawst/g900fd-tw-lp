.class public final enum Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;
.super Ljava/lang/Enum;
.source "GripSensorCalibrationEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GripSensorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

.field public static final enum OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

.field public static final enum ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

.field public static final enum UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;


# instance fields
.field private mButtonName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    const-string v1, "ON"

    const-string v2, "On"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    const-string v1, "OFF"

    const-string v2, "Off"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    const-string v1, "UNKNOWN"

    const-string v2, "Unknown"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 73
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->$VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "buttonName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-object p3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->mButtonName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 73
    const-class v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->$VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->mButtonName:Ljava/lang/String;

    return-object v0
.end method
