.class public Lcom/sec/android/app/selftestmode/TspDotModeTest;
.super Landroid/app/Activity;
.source "TspDotModeTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/TspDotModeTest$MyView;
    }
.end annotation


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private mCircleSize:I

.field private mCrossSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const-string v0, "TspDotModeTest"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->CLASS_NAME:Ljava/lang/String;

    .line 37
    const/16 v0, 0x46

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCircleSize:I

    .line 38
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCrossSize:I

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/selftestmode/TspDotModeTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/TspDotModeTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCircleSize:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/selftestmode/TspDotModeTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/TspDotModeTest;

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCrossSize:I

    return v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    new-instance v0, Lcom/sec/android/app/selftestmode/TspDotModeTest$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/selftestmode/TspDotModeTest$MyView;-><init>(Lcom/sec/android/app/selftestmode/TspDotModeTest;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/TspDotModeTest;->setContentView(Landroid/view/View;)V

    .line 44
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCircleSize:I

    .line 45
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspDotModeTest;->mCrossSize:I

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/TspDotModeTest;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 47
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 57
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 52
    return-void
.end method
