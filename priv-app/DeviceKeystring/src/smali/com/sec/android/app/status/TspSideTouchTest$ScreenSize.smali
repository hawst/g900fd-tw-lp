.class Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScreenSize"
.end annotation


# static fields
.field private static _screenHeight:I

.field private static _screenWidth:I


# direct methods
.method public static getWidth()I
    .locals 1

    .prologue
    .line 1934
    sget v0, Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;->_screenWidth:I

    return v0
.end method

.method public static setScreenSize(Landroid/app/Activity;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1927
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1928
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1929
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    sput v1, Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;->_screenWidth:I

    .line 1930
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sput v1, Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;->_screenHeight:I

    .line 1931
    return-void
.end method
