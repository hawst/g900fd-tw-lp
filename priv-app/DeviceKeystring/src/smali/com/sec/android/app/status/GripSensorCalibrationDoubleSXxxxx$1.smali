.class Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;
.super Ljava/lang/Object;
.source "GripSensorCalibrationDoubleSXxxxx.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 144
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    iget-object v8, v8, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v8}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v8

    if-nez v8, :cond_0

    .line 146
    const-string v8, "GRIP_SENSOR_RAWDATA"

    invoke-static {v8, v10}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 147
    .local v6, "rawData":Ljava/lang/String;
    const-string v8, "GRIP_SENSOR_RAWDATA_2ND"

    invoke-static {v8, v10}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    .line 148
    .local v7, "rawData2":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 149
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 150
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 151
    iput v10, v2, Landroid/os/Message;->what:I

    .line 154
    :try_start_0
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 155
    .local v3, "nRawData":I
    const-string v8, "RAW_DATA_KEY"

    invoke-virtual {v0, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 156
    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 162
    :try_start_1
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 163
    .local v4, "nRawData2":I
    const-string v8, "RAW_DATA_KEY"

    invoke-virtual {v0, v8, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 164
    iget-object v8, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    .line 171
    const-wide/16 v8, 0x64

    :try_start_2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v1

    .line 173
    .local v1, "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 177
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "nRawData":I
    .end local v4    # "nRawData2":I
    .end local v6    # "rawData":Ljava/lang/String;
    .end local v7    # "rawData2":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 157
    .restart local v0    # "data":Landroid/os/Bundle;
    .restart local v2    # "msg":Landroid/os/Message;
    .restart local v6    # "rawData":Ljava/lang/String;
    .restart local v7    # "rawData2":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 158
    .local v5, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 165
    .end local v5    # "ne":Ljava/lang/NumberFormatException;
    .restart local v3    # "nRawData":I
    :catch_2
    move-exception v5

    .line 166
    .restart local v5    # "ne":Ljava/lang/NumberFormatException;
    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method
