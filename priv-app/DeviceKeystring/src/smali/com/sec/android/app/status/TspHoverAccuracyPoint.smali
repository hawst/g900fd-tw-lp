.class public Lcom/sec/android/app/status/TspHoverAccuracyPoint;
.super Landroid/app/Activity;
.source "TspHoverAccuracyPoint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;
    }
.end annotation


# static fields
.field private static CLASS_NAME:Ljava/lang/String;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private block_size_height_mm:F

.field private block_size_width_mm:F

.field private click:[[Z

.field private draw:[[Z

.field private isDrawArea:[[Z

.field private mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

.field private radius_Inner_magnified_block:F

.field private radius_Outer_magnified_block:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "TspTestMode"

    sput-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_width_mm:F

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_height_mm:F

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->click:[[Z

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;
    .param p1, "x1"    # [[Z

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->click:[[Z

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->draw:[[Z

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;
    .param p1, "x1"    # [[Z

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->draw:[[Z

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)[[Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/status/TspHoverAccuracyPoint;[[Z)[[Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;
    .param p1, "x1"    # [[Z

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->isDrawArea:[[Z

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Outer_magnified_block:F

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/TspHoverAccuracyPoint;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Inner_magnified_block:F

    return v0
.end method

.method private setTSP()V
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    const/high16 v1, 0x41200000    # 10.0f

    .line 46
    const-string v0, "TSP_HOVER_BLOCK_SIZE_WIDTH_UM"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_width_mm:F

    .line 47
    const-string v0, "TSP_HOVER_BLOCK_SIZE_HEIGHT_UM"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->block_size_height_mm:F

    .line 48
    const-string v0, "TSP_HOVER_CIRCLE_OUTER_MAGNIFIED"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Outer_magnified_block:F

    .line 49
    const-string v0, "TSP_HOVER_CIRCLE_INNER_MAGNIFIED"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->radius_Inner_magnified_block:F

    .line 50
    return-void
.end method

.method public static wakeLockAcquire(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 485
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 486
    .local v0, "mPowerManager":Landroid/os/PowerManager;
    sget-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 487
    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 490
    :cond_0
    sget-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 491
    sget-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "wakeLockAcquire"

    const-string v3, "WakeLock acquire"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    sget-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 496
    :goto_0
    return-void

    .line 494
    :cond_1
    sget-object v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "wakeLockAcquire"

    const-string v3, "Already WakeLock acquire"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static wakeLockRelease()V
    .locals 3

    .prologue
    .line 500
    sget-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 501
    sget-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 502
    sget-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "wakeLockRelease"

    const-string v2, "WakeLock release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    sget-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 505
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 507
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->setTSP()V

    .line 58
    new-instance v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;-><init>(Lcom/sec/android/app/status/TspHoverAccuracyPoint;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->setContentView(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/16 v0, 0x18

    if-ne p1, v0, :cond_0

    .line 78
    new-instance v0, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint$MyView;-><init>(Lcom/sec/android/app/status/TspHoverAccuracyPoint;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->setContentView(Landroid/view/View;)V

    .line 79
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->wakeLockRelease()V

    .line 73
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->mModuleDevice:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->hoveringOnOFF(Ljava/lang/String;)V

    .line 65
    invoke-static {p0}, Lcom/sec/android/app/status/TspHoverAccuracyPoint;->wakeLockAcquire(Landroid/content/Context;)V

    .line 66
    return-void
.end method
