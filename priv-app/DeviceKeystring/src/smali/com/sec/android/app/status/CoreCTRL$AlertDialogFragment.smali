.class public Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "CoreCTRL.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/CoreCTRL;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AlertDialogFragment"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/CoreCTRL;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/CoreCTRL;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 195
    .local v2, "title":I
    invoke-virtual {p0}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "parentID"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 196
    .local v1, "parentID":I
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 197
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 198
    # getter for: Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/CoreCTRL;->access$000()[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;-><init>(Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;I)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 240
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public setParameter(II)V
    .locals 2
    .param p1, "title"    # I
    .param p2, "parentID"    # I

    .prologue
    .line 186
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 187
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 188
    const-string v1, "parentID"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 189
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 190
    return-void
.end method
