.class Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;
.super Ljava/util/TimerTask;
.source "Proximity_offset_JB.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/Proximity_offset_JB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field private data:Ljava/lang/String;

.field private mIsRunningTask:Z

.field private mValue:I

.field final synthetic this$0:Lcom/sec/android/app/status/Proximity_offset_JB;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/Proximity_offset_JB;)V
    .locals 1

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 374
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mIsRunningTask:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/Proximity_offset_JB;Lcom/sec/android/app/status/Proximity_offset_JB$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB;
    .param p2, "x1"    # Lcom/sec/android/app/status/Proximity_offset_JB$1;

    .prologue
    .line 371
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset_JB;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->resume()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;

    .prologue
    .line 371
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->pause()V

    return-void
.end method

.method private pause()V
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mIsRunningTask:Z

    .line 452
    return-void
.end method

.method private readToProximitySensor()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 406
    const/4 v1, 0x0

    .line 408
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "sys/class/sensors/proximity_sensor/state"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 411
    .local v3, "temp":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 412
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->data:Ljava/lang/String;

    .line 415
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->data:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->data:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mValue:I

    .line 419
    :cond_1
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ADC: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mbTMD27723:Z
    invoke-static {v4}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$1100(Lcom/sec/android/app/status/Proximity_offset_JB;)Z

    move-result v4

    if-ne v4, v7, :cond_2

    .line 423
    iget v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mValue:I

    const/16 v5, 0xc8

    if-ge v4, v5, :cond_2

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mnOffsetPassState:I
    invoke-static {v4}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$1200(Lcom/sec/android/app/status/Proximity_offset_JB;)I

    move-result v4

    if-ne v4, v7, :cond_2

    .line 425
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mValue:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 432
    :cond_2
    if-eqz v2, :cond_5

    .line 434
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 440
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .end local v3    # "temp":Ljava/lang/String;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-void

    .line 435
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 437
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 429
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 430
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ProximitySensor IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 432
    if-eqz v1, :cond_3

    .line 434
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 435
    :catch_2
    move-exception v0

    .line 436
    const-string v4, "Proximity_offset_JB"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 432
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v1, :cond_4

    .line 434
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 437
    :cond_4
    :goto_3
    throw v4

    .line 435
    :catch_3
    move-exception v0

    .line 436
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "Proximity_offset_JB"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 432
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 429
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "temp":Ljava/lang/String;
    :cond_5
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mIsRunningTask:Z

    .line 448
    return-void
.end method


# virtual methods
.method public getADC()I
    .locals 1

    .prologue
    .line 443
    iget v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mValue:I

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 377
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v2, 0x0

    .line 380
    const-string v0, "Proximity_offset_JB"

    const-string v1, "SensorChanged!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$600(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Working"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$600(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$700(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_JB;->startVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$800(Lcom/sec/android/app/status/Proximity_offset_JB;)V

    .line 394
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$600(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$600(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$700(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # invokes: Lcom/sec/android/app/status/Proximity_offset_JB;->stopVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$900(Lcom/sec/android/app/status/Proximity_offset_JB;)V

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->readToProximitySensor()V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset_JB$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset_JB;

    # getter for: Lcom/sec/android/app/status/Proximity_offset_JB;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset_JB;->access$1000(Lcom/sec/android/app/status/Proximity_offset_JB;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 403
    :cond_0
    return-void
.end method
