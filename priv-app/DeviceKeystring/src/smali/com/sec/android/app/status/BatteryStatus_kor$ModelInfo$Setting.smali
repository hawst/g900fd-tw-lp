.class final enum Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;
.super Ljava/lang/Enum;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Setting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

.field public static final enum BRIGHTNESS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

.field public static final enum FG_RESET_SOC:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 202
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    const-string v1, "FG_RESET_SOC"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->FG_RESET_SOC:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    const-string v1, "BRIGHTNESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->BRIGHTNESS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    .line 201
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->FG_RESET_SOC:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->BRIGHTNESS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->$VALUES:[Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 201
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 201
    const-class v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->$VALUES:[Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    return-object v0
.end method
