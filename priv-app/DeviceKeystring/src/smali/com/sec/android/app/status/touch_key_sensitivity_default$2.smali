.class Lcom/sec/android/app/status/touch_key_sensitivity_default$2;
.super Landroid/os/Handler;
.source "touch_key_sensitivity_default.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/touch_key_sensitivity_default;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/touch_key_sensitivity_default;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/touch_key_sensitivity_default;)V
    .locals 0

    .prologue
    .line 1596
    iput-object p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_default;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1598
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 1635
    :cond_0
    :goto_0
    return-void

    .line 1600
    :pswitch_0
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "handleMessage"

    const-string v5, "mTimerHandler"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1601
    const/4 v1, 0x0

    .line 1603
    .local v1, "out":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_default;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default;->PATH_TSK_UPDATE:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_default;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1604
    .end local v1    # "out":Ljava/io/FileWriter;
    .local v2, "out":Ljava/io/FileWriter;
    if-eqz v2, :cond_1

    .line 1605
    const/16 v3, 0x53

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(I)V

    .line 1606
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_default$2;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_default;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_default;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_default;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_default;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "FirmWare Update Completed!!!"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1611
    if-eqz v2, :cond_3

    .line 1613
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 1616
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 1614
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 1615
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 1616
    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 1608
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1609
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "touch_key_sensitivity_default"

    const-string v4, "handleMessage"

    const-string v5, "File open error"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1611
    if-eqz v1, :cond_0

    .line 1613
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1614
    :catch_2
    move-exception v0

    .line 1615
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1611
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_2

    .line 1613
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1616
    :cond_2
    :goto_3
    throw v3

    .line 1614
    :catch_3
    move-exception v0

    .line 1615
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 1611
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_2

    .line 1608
    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "out":Ljava/io/FileWriter;
    .restart local v2    # "out":Ljava/io/FileWriter;
    :cond_3
    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileWriter;
    .restart local v1    # "out":Ljava/io/FileWriter;
    goto :goto_0

    .line 1598
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
