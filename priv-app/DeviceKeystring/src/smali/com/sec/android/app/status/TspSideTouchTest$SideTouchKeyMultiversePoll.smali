.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;
.super Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SideTouchKeyMultiversePoll"
.end annotation


# instance fields
.field protected final _keyEventColor:[I

.field protected _keyEventColorIdx:I

.field protected _maxValue:I

.field protected _touchMaxValueView:Landroid/widget/TextView;

.field clock:Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

.field private mHandler:Landroid/os/Handler;

.field private mPrevBackKeyEventTime:J

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 3
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 1262
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    .line 1263
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 1240
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->mPrevBackKeyEventTime:J

    .line 1241
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-direct {v0}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->clock:Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    .line 1242
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll$1;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->mHandler:Landroid/os/Handler;

    .line 1257
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    .line 1258
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    .line 1259
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_keyEventColor:[I

    .line 1260
    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_keyEventColorIdx:I

    .line 1264
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1265
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_POLL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 1266
    return-void

    .line 1259
    :array_0
    .array-data 4
        0x7f060005
        0x7f060004
    .end array-data
.end method


# virtual methods
.method public addView(Landroid/widget/FrameLayout;)V
    .locals 4
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 1308
    invoke-super {p0, p1}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->addView(Landroid/widget/FrameLayout;)V

    .line 1310
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1312
    .local v0, "lParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1313
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_lLayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1314
    return-void
.end method

.method public onReset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1301
    invoke-super {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->onReset()V

    .line 1302
    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    .line 1303
    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    .line 1304
    return-void
.end method

.method protected setSensitivity(I)V
    .locals 6
    .param p1, "sensitivity"    # I

    .prologue
    const/4 v1, 0x1

    .line 1339
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    .line 1340
    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    if-le v2, v3, :cond_0

    .line 1341
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setSensitivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_maxValue: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    iput v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    .line 1345
    :cond_0
    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_specMin:I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    if-gt v2, v3, :cond_2

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_specMax:I

    if-gt v2, v3, :cond_2

    move v0, v1

    .line 1346
    .local v0, "bSpecIn":Z
    :goto_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setSensitivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sensitivity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bSpecIn: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1350
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->clock:Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->getTime()I

    move-result v2

    const/16 v3, 0x190

    if-ge v2, v3, :cond_3

    .line 1351
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1352
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1354
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x190

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1355
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setSensitivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Drop updateview event, clock.getTime(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->clock:Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-virtual {v4}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->getTime()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1362
    :goto_1
    return-void

    .line 1345
    .end local v0    # "bSpecIn":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1359
    .restart local v0    # "bSpecIn":Z
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->clock:Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->restart()V

    .line 1361
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->updateView()V

    goto :goto_1
.end method

.method protected updateView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x11

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 1318
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1319
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchNameView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1320
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1321
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1322
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1324
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_sensitivity:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 1325
    .local v0, "sensitivity":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1326
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchSensitivityView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1327
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1328
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1330
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_maxValue:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1331
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1333
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->_touchMaxValueView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 1334
    return-void
.end method
