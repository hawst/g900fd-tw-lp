.class public Lcom/sec/android/app/lcdtest/modules/ModuleObject;
.super Ljava/lang/Object;
.source "ModuleObject.java"


# static fields
.field protected static mContext:Landroid/content/Context;

.field private static mRegistedReceiverSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->mContext:Landroid/content/Context;

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->mRegistedReceiverSet:Ljava/util/HashSet;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "ModuleObject"

    iput-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->CLASS_NAME:Ljava/lang/String;

    .line 35
    sput-object p1, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->mContext:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->CLASS_NAME:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method protected getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getSystemService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "service="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleObject;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
