.class public final enum Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;
.super Ljava/lang/Enum;
.source "TspDataMeasure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspDataMeasure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TSP_DATATYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

.field public static final enum TSP:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

.field public static final enum TSP_HOVER:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

.field public static final enum TSP_SIDE_TOUCH:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;


# instance fields
.field private mId:I

.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 52
    new-instance v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    const-string v1, "TSP"

    const-string v2, "TSP"

    const/16 v3, 0x3e9

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    .line 53
    new-instance v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    const-string v1, "TSP_HOVER"

    const-string v2, "TSP Hover"

    const/16 v3, 0x3ea

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_HOVER:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    .line 54
    new-instance v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    const-string v1, "TSP_SIDE_TOUCH"

    const-string v2, "SideTouch"

    const/16 v3, 0x3eb

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_SIDE_TOUCH:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    sget-object v1, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_HOVER:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_SIDE_TOUCH:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->$VALUES:[Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput-object p3, p0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->mName:Ljava/lang/String;

    .line 61
    iput p4, p0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->mId:I

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->$VALUES:[Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->mId:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->mName:Ljava/lang/String;

    return-object v0
.end method
