.class public Lcom/sec/android/app/lcdtest/support/XMLDataStorage;
.super Ljava/lang/Object;
.source "XMLDataStorage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/lcdtest/support/XMLDataStorage$ElementIdMismatchException;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/app/lcdtest/support/XMLDataStorage;


# instance fields
.field private mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

.field private mDocument:Lorg/w3c/dom/Document;

.field private mWasCompletedParsing:Z

.field private mXPath:Ljavax/xml/xpath/XPath;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mInstance:Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mWasCompletedParsing:Z

    .line 50
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    .line 51
    .local v1, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setIgnoringComments(Z)V

    .line 52
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->setIgnoringElementContentWhitespace(Z)V

    .line 53
    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    .line 54
    invoke-static {}, Ljavax/xml/xpath/XPathFactory;->newInstance()Ljavax/xml/xpath/XPathFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/xpath/XPathFactory;->newXPath()Ljavax/xml/xpath/XPath;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mXPath:Ljavax/xml/xpath/XPath;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .end local v1    # "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :goto_0
    return-void

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private convertBytesToIS([B)Ljava/io/InputStream;
    .locals 1
    .param p1, "bytes"    # [B

    .prologue
    .line 158
    const/4 v0, 0x0

    .line 159
    .local v0, "byteis":Ljava/io/ByteArrayInputStream;
    new-instance v0, Ljava/io/ByteArrayInputStream;

    .end local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 160
    .restart local v0    # "byteis":Ljava/io/ByteArrayInputStream;
    return-object v0
.end method

.method private convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "xmlFile"    # Ljava/lang/String;

    .prologue
    .line 125
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v4, "stringbuilder":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 127
    const/4 v2, 0x0

    .line 130
    .local v2, "inputstream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const/4 v6, 0x3

    invoke-virtual {v5, p2, v6}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v2

    .line 134
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 136
    .local v0, "buffreader":Ljava/io/BufferedReader;
    const-string v3, ""

    .line 137
    .local v3, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 138
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 141
    .end local v0    # "buffreader":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 146
    if-eqz v2, :cond_0

    .line 147
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 154
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 146
    .restart local v0    # "buffreader":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_1
    if-eqz v2, :cond_0

    .line 147
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 149
    :catch_1
    move-exception v1

    .line 150
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 149
    .end local v0    # "buffreader":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 150
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 145
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 146
    if-eqz v2, :cond_2

    .line 147
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 151
    :cond_2
    :goto_2
    throw v5

    .line 149
    :catch_3
    move-exception v1

    .line 150
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method private getBaseDocument(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 6
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 230
    const-string v3, "/Settings/BaseDocument"

    sget-object v4, Ljavax/xml/xpath/XPathConstants;->NODE:Ljavax/xml/namespace/QName;

    invoke-direct {p0, p1, v3, v4}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->xpath(Lorg/w3c/dom/Document;Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 231
    .local v1, "element":Lorg/w3c/dom/Element;
    const-string v0, ""

    .line 232
    .local v0, "datDocument":Ljava/lang/String;
    const-string v2, ""

    .line 233
    .local v2, "result":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "document"

    invoke-interface {v1, v3}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_0
    if-eqz v0, :cond_1

    .line 237
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 242
    :goto_1
    return-object v2

    .line 233
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :cond_1
    move-object v2, v0

    goto :goto_1
.end method

.method public static declared-synchronized instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;
    .locals 2

    .prologue
    .line 41
    const-class v1, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mInstance:Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    invoke-direct {v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;-><init>()V

    sput-object v0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mInstance:Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mInstance:Lcom/sec/android/app/lcdtest/support/XMLDataStorage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private parseAsset(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "xml"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 95
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 98
    .local v2, "dataBytes":[B
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Convert enc file to xml file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v6, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    invoke-direct {p0, v2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->convertBytesToIS([B)Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 100
    .local v3, "document":Lorg/w3c/dom/Document;
    const/4 v1, 0x0

    .line 102
    .local v1, "baseXml":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->getBaseDocument(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    const/4 v2, 0x0

    .line 104
    const-string v6, "XMLDataStorage"

    const-string v7, "parseAsset"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Decode base xml file: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->convertIStoString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 106
    iget-object v6, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDOMParser:Ljavax/xml/parsers/DocumentBuilder;

    invoke-direct {p0, v2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->convertBytesToIS([B)Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 107
    .local v0, "baseDocument":Lorg/w3c/dom/Document;
    move-object v5, v3

    .line 108
    .local v5, "subDocument":Lorg/w3c/dom/Document;
    invoke-direct {p0, v0, v5}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Document;)V

    .line 109
    iput-object v0, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    .line 114
    .end local v0    # "baseDocument":Lorg/w3c/dom/Document;
    .end local v5    # "subDocument":Lorg/w3c/dom/Document;
    :goto_0
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mWasCompletedParsing:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v1    # "baseXml":Ljava/lang/String;
    .end local v2    # "dataBytes":[B
    .end local v3    # "document":Lorg/w3c/dom/Document;
    :goto_1
    iget-boolean v6, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mWasCompletedParsing:Z

    return v6

    .line 111
    .restart local v1    # "baseXml":Ljava/lang/String;
    .restart local v2    # "dataBytes":[B
    .restart local v3    # "document":Lorg/w3c/dom/Document;
    :cond_0
    :try_start_1
    iput-object v3, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 115
    .end local v1    # "baseXml":Ljava/lang/String;
    .end local v2    # "dataBytes":[B
    .end local v3    # "document":Lorg/w3c/dom/Document;
    :catch_0
    move-exception v4

    .line 116
    .local v4, "e":Ljava/lang/Exception;
    iput-boolean v10, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mWasCompletedParsing:Z

    .line 117
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Document;)V
    .locals 1
    .param p1, "baseDocument"    # Lorg/w3c/dom/Document;
    .param p2, "subDocument"    # Lorg/w3c/dom/Document;

    .prologue
    .line 272
    invoke-interface {p2}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 273
    return-void
.end method

.method private redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V
    .locals 13
    .param p1, "baseDocument"    # Lorg/w3c/dom/Document;
    .param p2, "redefNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 276
    invoke-interface {p2}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 277
    const-string v9, "id"

    invoke-interface {p2, v9}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 279
    .local v7, "id":Ljava/lang/String;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 280
    const-string v9, "XMLDataStorage"

    const-string v10, "redefinitionById"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "id="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0, p2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->getAttributeNameSet(Lorg/w3c/dom/Element;)[Ljava/lang/String;

    move-result-object v2

    .line 283
    .local v2, "attributes":[Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 284
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_2

    aget-object v1, v0, v6

    .line 285
    .local v1, "attr":Ljava/lang/String;
    const-string v9, "id"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 286
    invoke-interface {p1, v7}, Lorg/w3c/dom/Document;->getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 288
    .local v4, "element":Lorg/w3c/dom/Element;
    if-eqz v4, :cond_1

    .line 289
    invoke-interface {p2, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4, v1, v9}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    .end local v4    # "element":Lorg/w3c/dom/Element;
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 291
    .restart local v4    # "element":Lorg/w3c/dom/Element;
    :cond_1
    new-instance v9, Lcom/sec/android/app/lcdtest/support/XMLDataStorage$ElementIdMismatchException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Element \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\" does not exist in base document."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage$ElementIdMismatchException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 300
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "attr":Ljava/lang/String;
    .end local v2    # "attributes":[Ljava/lang/String;
    .end local v4    # "element":Lorg/w3c/dom/Element;
    .end local v6    # "i$":I
    .end local v7    # "id":Ljava/lang/String;
    .end local v8    # "len$":I
    :cond_2
    invoke-interface {p2}, Lorg/w3c/dom/Element;->hasChildNodes()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 301
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 303
    .local v3, "childNodes":Lorg/w3c/dom/NodeList;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v5, v9, :cond_4

    .line 304
    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    .line 305
    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    check-cast v9, Lorg/w3c/dom/Element;

    invoke-direct {p0, p1, v9}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->redefinitionById(Lorg/w3c/dom/Document;Lorg/w3c/dom/Element;)V

    .line 303
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 309
    .end local v3    # "childNodes":Lorg/w3c/dom/NodeList;
    .end local v5    # "i":I
    :cond_4
    return-void
.end method

.method private xpath(Lorg/w3c/dom/Document;Ljava/lang/String;Ljavax/xml/namespace/QName;)Ljava/lang/Object;
    .locals 3
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .param p2, "expression"    # Ljava/lang/String;
    .param p3, "returnType"    # Ljavax/xml/namespace/QName;

    .prologue
    .line 250
    const/4 v1, 0x0

    .line 253
    .local v1, "result":Ljava/lang/Object;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mXPath:Ljavax/xml/xpath/XPath;

    invoke-interface {v2, p2}, Ljavax/xml/xpath/XPath;->compile(Ljava/lang/String;)Ljavax/xml/xpath/XPathExpression;

    move-result-object v2

    invoke-interface {v2, p1, p3}, Ljavax/xml/xpath/XPathExpression;->evaluate(Ljava/lang/Object;Ljavax/xml/namespace/QName;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 258
    .end local v1    # "result":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 254
    .restart local v1    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getAttributeNameSet(Lorg/w3c/dom/Element;)[Ljava/lang/String;
    .locals 4
    .param p1, "element"    # Lorg/w3c/dom/Element;

    .prologue
    .line 211
    const/4 v0, 0x0

    .line 213
    .local v0, "attributeSet":[Ljava/lang/String;
    invoke-interface {p1}, Lorg/w3c/dom/Element;->hasAttributes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 214
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 215
    .local v2, "map":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    new-array v0, v3, [Ljava/lang/String;

    .line 217
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 218
    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 222
    .end local v1    # "i":I
    .end local v2    # "map":Lorg/w3c/dom/NamedNodeMap;
    :cond_0
    return-object v0
.end method

.method public getAttributeValueById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "resultAttr"    # Ljava/lang/String;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->getElementById(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {v0, p2}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mDocument:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method public declared-synchronized parseXML(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v3

    if-nez v3, :cond_1

    .line 62
    const-string v0, "lcdtest"

    .line 63
    .local v0, "DEFAULT_XML":Ljava/lang/String;
    const-string v3, "ro.product.model"

    const-string v4, "lcdtest"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "modelXML":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 65
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ro.product.device"

    const-string v5, "lcdtest"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "deviceXML":Ljava/lang/String;
    const-string v3, "XMLDataStorage"

    const-string v4, "parseXML"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "modelXML="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v3, "XMLDataStorage"

    const-string v4, "parseXML"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deviceXML="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    invoke-direct {v3, p1, v2}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 72
    const-string v3, "XMLDataStorage"

    const-string v4, "parseXML"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ro.product.model XML not found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    invoke-direct {v3, p1, v1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 75
    const-string v3, "XMLDataStorage"

    const-string v4, "parseXML"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ro.product.device XML not found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    const-string v4, "lcdtest.dat"

    invoke-direct {v3, p1, v4}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseAsset(Landroid/content/Context;Ljava/lang/String;)Z

    .line 78
    const-string v3, "XMLDataStorage"

    const-string v4, "parseXML"

    const-string v5, "Default XML loaded => lcdtest.dat"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_0
    const/4 v3, 0x1

    .line 88
    .end local v0    # "DEFAULT_XML":Ljava/lang/String;
    .end local v1    # "deviceXML":Ljava/lang/String;
    .end local v2    # "modelXML":Ljava/lang/String;
    :goto_0
    monitor-exit p0

    return v3

    .line 85
    :cond_1
    :try_start_1
    const-string v3, "XMLDataStorage"

    const-string v4, "parsingXML"

    const-string v5, "FtClient => XML data parsing was completed."

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    const/4 v3, 0x0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public wasCompletedParsing()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->mWasCompletedParsing:Z

    return v0
.end method
