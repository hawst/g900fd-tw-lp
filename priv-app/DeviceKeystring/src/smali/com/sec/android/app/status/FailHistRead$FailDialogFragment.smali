.class public Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;
.super Landroid/app/DialogFragment;
.source "FailHistRead.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/FailHistRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FailDialogFragment"
.end annotation


# instance fields
.field mVersion:Ljava/lang/String;

.field message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;
    .locals 3
    .param p0, "message"    # Ljava/lang/String;
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 150
    new-instance v1, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    invoke-direct {v1}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;-><init>()V

    .line 151
    .local v1, "dialog":Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 152
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "message"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v2, "verson"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v1, v0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 155
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->message:Ljava/lang/String;

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "verson"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->mVersion:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 169
    const v6, 0x7f030016

    const/4 v7, 0x0

    invoke-virtual {p1, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 170
    .local v4, "v":Landroid/view/View;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 171
    .local v2, "displayRectangle":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 172
    .local v5, "window":Landroid/view/Window;
    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 173
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    const v7, 0x3f333333    # 0.7f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumWidth(I)V

    .line 174
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setMinimumHeight(I)V

    .line 175
    const v6, 0x7f0a0059

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 176
    .local v3, "tvMessage":Landroid/widget/TextView;
    const v6, 0x7f0a005a

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 177
    .local v1, "btYes":Landroid/widget/Button;
    const v6, 0x7f070153

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setText(I)V

    .line 178
    const v6, 0x7f0a005b

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 179
    .local v0, "btNo":Landroid/widget/Button;
    const v6, 0x7f070154

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(I)V

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/view/Window;->requestFeature(I)Z

    .line 181
    iget-object v6, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->message:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    new-instance v6, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;-><init>(Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    new-instance v6, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$2;-><init>(Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    return-object v4
.end method
