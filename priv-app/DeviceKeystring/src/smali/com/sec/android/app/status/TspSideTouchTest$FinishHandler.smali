.class Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;
.super Landroid/os/Handler;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FinishHandler"
.end annotation


# instance fields
.field private mFinish:Z

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;)V
    .locals 1

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;->mFinish:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Lcom/sec/android/app/status/TspSideTouchTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest;
    .param p2, "x1"    # Lcom/sec/android/app/status/TspSideTouchTest$1;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 142
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 125
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;
    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest;->access$200(Lcom/sec/android/app/status/TspSideTouchTest;)Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->getResult()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-boolean v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;->mFinish:Z

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;->mFinish:Z

    .line 130
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mFinishHandler.handleMessage"

    const-string v2, "MESSAGE_FINISH_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler$1;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method
