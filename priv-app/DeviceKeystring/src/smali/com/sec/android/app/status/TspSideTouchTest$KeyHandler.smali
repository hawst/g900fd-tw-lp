.class Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyHandler"
.end annotation


# instance fields
.field _list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    .line 377
    return-void
.end method


# virtual methods
.method public add(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 384
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;>;"
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 386
    return-void
.end method

.method public addKeys(Landroid/widget/FrameLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 417
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 418
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-interface {v1, p1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->addView(Landroid/widget/FrameLayout;)V

    goto :goto_0

    .line 420
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    :cond_0
    return-void
.end method

.method public addViewTouchSpec(Landroid/widget/LinearLayout;)V
    .locals 5
    .param p1, "layout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 441
    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 442
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    sget-object v3, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_TYPE:[I

    invoke-interface {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->getType()Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v2, v1

    .line 448
    check-cast v2, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    .line 449
    .local v2, "touchKey":Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    invoke-virtual {v2, p1}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->addTouchSpec(Landroid/widget/LinearLayout;)V

    goto :goto_0

    .line 455
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    .end local v2    # "touchKey":Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    :cond_0
    return-void

    .line 442
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public broadcastKeyEvent(ILandroid/view/KeyEvent;)V
    .locals 3
    .param p1, "KeyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 401
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 402
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-interface {v1, p1, p2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->onKey(ILandroid/view/KeyEvent;)V

    goto :goto_0

    .line 404
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    :cond_0
    return-void
.end method

.method public getResult()Z
    .locals 4

    .prologue
    .line 407
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 408
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-interface {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->getStatus()Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-eq v2, v3, :cond_0

    .line 409
    const/4 v2, 0x0

    .line 413
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 458
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 459
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-interface {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->release()V

    goto :goto_0

    .line 461
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 389
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->_list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;

    .line 390
    .local v1, "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-interface {v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->onReset()V

    goto :goto_0

    .line 392
    .end local v1    # "l":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    :cond_0
    return-void
.end method
