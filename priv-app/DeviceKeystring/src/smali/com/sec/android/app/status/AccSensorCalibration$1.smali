.class Lcom/sec/android/app/status/AccSensorCalibration$1;
.super Landroid/os/Handler;
.source "AccSensorCalibration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/AccSensorCalibration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/AccSensorCalibration;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/AccSensorCalibration;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 57
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 74
    :goto_0
    return-void

    .line 59
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mText_X:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/AccSensorCalibration;->access$100(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "X: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->getX()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mText_Y:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/AccSensorCalibration;->access$200(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Y: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->getY()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mText_Z:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/AccSensorCalibration;->access$300(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Z: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->getZ()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration;->updateCalState()V
    invoke-static {v1}, Lcom/sec/android/app/status/AccSensorCalibration;->access$400(Lcom/sec/android/app/status/AccSensorCalibration;)V

    goto :goto_0

    .line 67
    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "enabled":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration$1;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/app/status/AccSensorCalibration;->access$500(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
