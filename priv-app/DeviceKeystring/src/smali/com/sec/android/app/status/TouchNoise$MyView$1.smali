.class Lcom/sec/android/app/status/TouchNoise$MyView$1;
.super Landroid/os/Handler;
.source "TouchNoise.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TouchNoise$MyView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/status/TouchNoise$MyView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/TouchNoise$MyView;)V
    .locals 0

    .prologue
    .line 849
    iput-object p1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 851
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 877
    :goto_0
    return-void

    .line 854
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$100(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 855
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # invokes: Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_melfas2()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$200(Lcom/sec/android/app/status/TouchNoise;)V

    .line 864
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isDataRawFileExist:Z
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$100(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 865
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    # invokes: Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare_melfas2()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise$MyView;->access$800(Lcom/sec/android/app/status/TouchNoise$MyView;)V

    .line 870
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TouchNoise$MyView;->invalidate()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 871
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "TouchNoise"

    const-string v2, "handleMessage"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 856
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isMelfas:Z
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$300(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 857
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # invokes: Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_melfas()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$400(Lcom/sec/android/app/status/TouchNoise;)V

    goto :goto_1

    .line 858
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # getter for: Lcom/sec/android/app/status/TouchNoise;->isSTM:Z
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$500(Lcom/sec/android/app/status/TouchNoise;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 859
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # invokes: Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus_stm()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$600(Lcom/sec/android/app/status/TouchNoise;)V

    goto :goto_1

    .line 861
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    iget-object v1, v1, Lcom/sec/android/app/status/TouchNoise$MyView;->this$0:Lcom/sec/android/app/status/TouchNoise;

    # invokes: Lcom/sec/android/app/status/TouchNoise;->updateCurrentTouchNoiseStatus()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise;->access$700(Lcom/sec/android/app/status/TouchNoise;)V

    goto :goto_1

    .line 867
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/status/TouchNoise$MyView$1;->this$1:Lcom/sec/android/app/status/TouchNoise$MyView;

    # invokes: Lcom/sec/android/app/status/TouchNoise$MyView;->drawSquare()V
    invoke-static {v1}, Lcom/sec/android/app/status/TouchNoise$MyView;->access$900(Lcom/sec/android/app/status/TouchNoise$MyView;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 851
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
