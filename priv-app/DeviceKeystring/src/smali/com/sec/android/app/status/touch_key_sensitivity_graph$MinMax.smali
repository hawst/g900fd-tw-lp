.class Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
.super Ljava/lang/Object;
.source "touch_key_sensitivity_graph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/touch_key_sensitivity_graph;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MinMax"
.end annotation


# instance fields
.field private mData:I

.field private mIsFirstData:Z

.field private mMax:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mData:I

    .line 131
    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mIsFirstData:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;-><init>()V

    return-void
.end method

.method static synthetic access$2400(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;
    .param p1, "x1"    # I

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->setData(I)V

    return-void
.end method

.method static synthetic access$4900(Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I

    return v0
.end method

.method private setData(I)V
    .locals 2
    .param p1, "data"    # I

    .prologue
    .line 135
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mData:I

    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mIsFirstData:Z

    if-eqz v0, :cond_1

    .line 138
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mData:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mIsFirstData:Z

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mData:I

    if-ge v0, v1, :cond_0

    .line 145
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mData:I

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$MinMax;->mMax:I

    goto :goto_0
.end method
