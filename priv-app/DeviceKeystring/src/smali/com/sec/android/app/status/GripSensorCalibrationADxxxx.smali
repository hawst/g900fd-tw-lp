.class public Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;
.super Landroid/app/Activity;
.source "GripSensorCalibrationADxxxx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;
    }
.end annotation


# instance fields
.field private isTimerFinish:Z

.field private mButtonNext:Landroid/widget/Button;

.field private mChipName:Ljava/lang/String;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field private mMaxRawData:I

.field private mMinRawData:I

.field private mMode_data:Ljava/lang/String;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTextGripSensorInitThd:Landroid/widget/TextView;

.field private mTextGripSensorNormalThd:Landroid/widget/TextView;

.field private mTextGripSensorRawData:[Landroid/widget/TextView;

.field private mTextGripSensorResult:Landroid/widget/TextView;

.field private mText_Mode:Landroid/widget/TextView;

.field mThreadGetData:Ljava/lang/Thread;

.field private mTimerHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMode_data:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->isTimerFinish:Z

    .line 372
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$6;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    .line 411
    return-void
.end method

.method private UpdateCalResult(Ljava/lang/String;)V
    .locals 4
    .param p1, "op"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 242
    const-string v0, "1"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 243
    const-string v0, "2"

    const-string v1, "GRIP_SENSOR_CALIBRATION"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "UpdateCalResult"

    const-string v2, "SUCCESS"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 248
    const-string v0, "CAL SUCCESS"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 266
    :goto_0
    return-void

    .line 250
    :cond_0
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "UpdateCalResult"

    const-string v2, "FAIL"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v0, "CAL FAIL"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 254
    :cond_1
    const-string v0, "0"

    const-string v1, "GRIP_SENSOR_CALIBRATION"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "UpdateCalResult"

    const-string v2, "SUCCESS"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const-string v1, "N"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 260
    const-string v0, "CAL ERASE SUCCESS"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 262
    :cond_2
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "UpdateCalResult"

    const-string v2, "FAIL"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v0, "CAL ERASE FAIL"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private UpdateThreshold()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    .line 269
    const-string v2, "GRIP_SENSOR_THRESHOLD"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "threshold":Ljava/lang/String;
    const-string v2, "GripSensorCalibrationBySensorManager"

    const-string v3, "UpdateThreshold"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    if-eqz v0, :cond_2

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 273
    .local v1, "thresholds":[Ljava/lang/String;
    :goto_0
    array-length v2, v1

    if-lez v2, :cond_0

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorInitThd:Landroid/widget/TextView;

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    :cond_0
    array-length v2, v1

    if-le v2, v5, :cond_1

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorNormalThd:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v1, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    :cond_1
    return-void

    .line 271
    .end local v1    # "thresholds":[Ljava/lang/String;
    :cond_2
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    const-string v2, "none"

    aput-object v2, v1, v4

    const-string v2, "none"

    aput-object v2, v1, v6

    const-string v2, "none"

    aput-object v2, v1, v5

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->updateSensorMode()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMinRawData:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMinRawData:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMaxRawData:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMaxRawData:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->UpdateCalResult(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->UpdateThreshold()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->sensorOn()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->startThreadGettingRawData()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)[Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    return-object v0
.end method

.method private checkCalData()V
    .locals 3

    .prologue
    .line 181
    const-string v1, "GRIP_SENSOR_CALIBRATION"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 183
    .local v0, "calData":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const-string v2, "Y"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const-string v2, "N"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private initRawData()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 174
    iput v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMinRawData:I

    .line 175
    iput v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMaxRawData:I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-string v1, "Not Set"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-string v1, "Not Set"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    return-void
.end method

.method private sensorOff()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 168
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    .line 169
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 170
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 171
    return-void
.end method

.method private sensorOn()V
    .locals 5

    .prologue
    .line 149
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    if-nez v1, :cond_0

    .line 150
    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    .line 152
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 153
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 156
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 161
    .local v0, "result":Z
    const-string v1, "GripSensorCalibrationBySensorManager"

    const-string v2, "sensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method private startThreadGettingRawData()V
    .locals 4

    .prologue
    .line 195
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "startThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->stopThreadGettingRawData()V

    .line 197
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$2;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    .line 225
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 226
    return-void
.end method

.method private stopThreadGettingRawData()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 234
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 237
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    .line 238
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    return-void
.end method

.method private updateSensorMode()V
    .locals 2

    .prologue
    .line 123
    const-string v0, "GRIP_SENSOR_MODE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMode_data:Ljava/lang/String;

    .line 124
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMode_data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v1, "INIT"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :goto_0
    return-void

    .line 126
    :cond_0
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mMode_data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v1, "NORMAL"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v1, "Unknown"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 350
    const-string v4, "GripSensorCalibrationBySensorManager"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const/4 v3, 0x0

    .line 352
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 354
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 356
    const/4 v3, 0x1

    .line 361
    if-eqz v2, :cond_0

    .line 362
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 368
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibrationBySensorManager"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    return v3

    .line 364
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationBySensorManager"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 367
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 357
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 358
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationBySensorManager"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 361
    if-eqz v1, :cond_1

    .line 362
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 364
    :catch_2
    move-exception v0

    .line 365
    const-string v4, "GripSensorCalibrationBySensorManager"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 360
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 361
    :goto_2
    if-eqz v1, :cond_2

    .line 362
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 366
    :cond_2
    :goto_3
    throw v4

    .line 364
    :catch_3
    move-exception v0

    .line 365
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationBySensorManager"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 360
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 357
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 283
    const-string v2, "GripSensorCalibrationBySensorManager"

    const-string v3, "onClick"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v1, p1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 347
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 286
    :pswitch_1
    const-string v1, "GRIP_SENSOR_CALIBRATION"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    const-string v1, "Do CAL, Please wait 500ms"

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$3;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 299
    :pswitch_2
    const-string v1, "GRIP_SENSOR_CALIBRATION"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    const-string v1, "Do CAL Erase, Please wait 500ms"

    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$4;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 312
    :pswitch_3
    const-string v1, "/sys/class/sensors/grip_sensor/reset"

    const/16 v2, 0x31

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->writeSysfs(Ljava/lang/String;B)Z

    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->sensorOff()V

    .line 314
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$5;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->initRawData()V

    goto :goto_0

    .line 335
    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 336
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 337
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 341
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_5
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->finish()V

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x7f0a009f
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->setContentView(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "grip_sensor_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mChipName:Ljava/lang/String;

    .line 79
    const v0, 0x7f0a00a2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mChipName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mText_Mode:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorInitThd:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0a008a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorNormalThd:Landroid/widget/TextView;

    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const/4 v2, 0x0

    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const/4 v2, 0x1

    const v0, 0x7f0a008c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const/4 v2, 0x2

    const v0, 0x7f0a008d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v2

    .line 90
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTextGripSensorResult:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v0, 0x7f0a00a3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v0, 0x7f0a00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mButtonNext:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    return-void

    .line 76
    :cond_0
    const-string v0, "Unknown Sensor"

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 144
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 146
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 135
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->stopThreadGettingRawData()V

    .line 137
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->sensorOff()V

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->isTimerFinish:Z

    .line 139
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 140
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 103
    const-string v0, "GripSensorCalibrationBySensorManager"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->sensorOn()V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->initRawData()V

    .line 107
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->UpdateThreshold()V

    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->startThreadGettingRawData()V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->checkCalData()V

    .line 110
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx$1;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTimerHandler:Landroid/os/Handler;

    .line 118
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->isTimerFinish:Z

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationADxxxx;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 120
    return-void
.end method
