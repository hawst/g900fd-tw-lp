.class abstract Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.super Landroid/widget/Button;
.source "BatteryStatus_kor.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "ButtonEx"
.end annotation


# instance fields
.field protected mAlwaysOn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 910
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 911
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->mAlwaysOn:Z

    .line 912
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "awayson"    # Z

    .prologue
    .line 914
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 915
    iput-boolean p2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->mAlwaysOn:Z

    .line 916
    return-void
.end method


# virtual methods
.method public getAwaysOn()Z
    .locals 1

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->mAlwaysOn:Z

    return v0
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public update()V
    .locals 0

    .prologue
    .line 920
    return-void
.end method
