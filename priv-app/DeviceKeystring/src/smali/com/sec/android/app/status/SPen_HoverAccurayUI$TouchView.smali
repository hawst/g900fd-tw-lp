.class public Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;
.super Landroid/view/View;
.source "SPen_HoverAccurayUI.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/SPen_HoverAccurayUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TouchView"
.end annotation


# instance fields
.field private isHover:Z

.field private isLandscape:Z

.field private isPenDetect:Z

.field private isTouchDown:Z

.field private mCrossPaint:Landroid/graphics/Paint;

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mFisrtTouchX:F

.field private mFisrtTouchY:F

.field private mHoverCircleSize:F

.field private mHoverPaint:Landroid/graphics/Paint;

.field private mHoveredX:F

.field private mHoveredY:F

.field private mInfoBackgroundPaint:Landroid/graphics/Paint;

.field private mInfoBitmap:Landroid/graphics/Bitmap;

.field private mInfoCanvas:Landroid/graphics/Canvas;

.field private mInfoPaint:Landroid/graphics/Paint;

.field private mLevelPaint:Landroid/graphics/Paint;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreHoveredX:F

.field private mPreHoveredY:F

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mRectPaint:Landroid/graphics/Paint;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field private pxsize:F

.field final synthetic this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/SPen_HoverAccurayUI;Landroid/content/Context;)V
    .locals 11
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 119
    iput-object p1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    .line 120
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 85
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedX:F

    .line 86
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedY:F

    .line 87
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 88
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 89
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    .line 90
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    .line 92
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredX:F

    .line 93
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredY:F

    .line 94
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    .line 95
    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    .line 121
    invoke-virtual {p0, v9}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->setKeepScreenOn(Z)V

    .line 122
    const-string v4, "window"

    invoke-virtual {p2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 123
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 124
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 125
    iget v4, v2, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    .line 126
    iget v4, v2, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    .line 127
    const-string v4, "SPenTestUI"

    const-string v5, "TouchView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Screen size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 130
    .local v3, "r":Landroid/content/res/Resources;
    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->SIZE_RECT:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$100(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v10, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->pxsize:F

    .line 131
    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->pxsize:F

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->WIDTH_BASIS:I
    invoke-static {p1, v4}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$202(Lcom/sec/android/app/status/SPen_HoverAccurayUI;I)I

    .line 132
    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->pxsize:F

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->HEIGHT_BASIS:I
    invoke-static {p1, v4}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$302(Lcom/sec/android/app/status/SPen_HoverAccurayUI;I)I

    .line 133
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v10, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverCircleSize:F

    .line 136
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 137
    :cond_0
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isLandscape:Z

    .line 142
    :goto_0
    const-string v4, "SPenTestUI"

    const-string v5, "TouchView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "channel number: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->WIDTH_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$200(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->HEIGHT_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$300(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 144
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    invoke-static {v0, v4, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 145
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 148
    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    invoke-static {v0, v4, v5, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoBitmap:Landroid/graphics/Bitmap;

    .line 149
    new-instance v4, Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoCanvas:Landroid/graphics/Canvas;

    .line 151
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->setPaint()V

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->initRect()V

    .line 154
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    .line 155
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    .line 156
    iput-boolean v8, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isHover:Z

    .line 157
    return-void

    .line 139
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isLandscape:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->initRect()V

    return-void
.end method

.method private initRect()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 453
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$300(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v8, v0, v2

    .line 454
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$200(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v9, v0, v2

    .line 455
    .local v9, "col_width":F
    const/4 v6, 0x0

    .line 456
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 457
    .local v7, "ColY":I
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 458
    .local v12, "mRectPaint":Landroid/graphics/Paint;
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 460
    const/high16 v0, -0x1000000

    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 462
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$300(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v0

    if-ge v10, v0, :cond_1

    .line 463
    int-to-float v0, v10

    mul-float/2addr v0, v8

    float-to-int v7, v0

    .line 465
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$200(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v0

    if-ge v11, v0, :cond_0

    .line 466
    int-to-float v0, v11

    mul-float/2addr v0, v9

    float-to-int v6, v0

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    iget v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v7

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    int-to-float v4, v4

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 465
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 462
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 471
    .end local v11    # "j":I
    :cond_1
    return-void
.end method

.method private setPaint()V
    .locals 8

    .prologue
    const/high16 v7, 0x40400000    # 3.0f

    const/4 v6, 0x0

    const v5, -0xffff01

    const/16 v4, 0xff

    const/4 v3, 0x1

    .line 160
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 169
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setDither(Z)V

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 177
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 186
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 188
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mRectPaint:Landroid/graphics/Paint;

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mRectPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 191
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 195
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 196
    .local v0, "mtx":Landroid/graphics/Matrix;
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 198
    return-void
.end method


# virtual methods
.method public displayInfoBox(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    if-eqz v0, :cond_1

    .line 274
    const/16 v7, 0x14

    .line 275
    .local v7, "MarkingSize":I
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    int-to-float v1, v7

    sub-float v1, v0, v1

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    int-to-float v2, v7

    sub-float v2, v0, v2

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    int-to-float v3, v7

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    int-to-float v4, v7

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 276
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    int-to-float v1, v7

    sub-float v1, v0, v1

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    int-to-float v2, v7

    add-float/2addr v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    int-to-float v3, v7

    add-float/2addr v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    int-to-float v4, v7

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mCrossPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 277
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    int-to-float v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mRectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 279
    const/4 v6, 0x0

    .line 280
    .local v6, "BasedY":I
    const/high16 v9, 0x42700000    # 60.0f

    .line 281
    .local v9, "colHeight":F
    const/4 v8, 0x0

    .line 282
    .local v8, "VerticalOffset":I
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 283
    const/16 v8, 0x8

    .line 284
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/2addr v0, v8

    add-int/lit8 v6, v0, 0x28

    .line 290
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->pxsize:F

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->SIZE_RECT:I
    invoke-static {v3}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$100(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    div-double v10, v0, v2

    .line 291
    .local v10, "delta":D
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/2addr v0, v8

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    add-int/lit8 v4, v8, 0x1

    mul-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 292
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/2addr v0, v8

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    add-int/lit8 v4, v8, 0x1

    mul-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mRectPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x420c0000    # 35.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 295
    const-string v0, " Point T (X %.1f, Y %.1f),"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 296
    .local v12, "result":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Point A:(X %.1f, Y %.1f) \n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 297
    const/high16 v0, 0x41200000    # 10.0f

    int-to-float v1, v6

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 298
    int-to-float v0, v6

    add-float/2addr v0, v9

    float-to-int v6, v0

    .line 299
    const-string v0, " Delta %.1f mm"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 300
    const/high16 v0, 0x41200000    # 10.0f

    int-to-float v1, v6

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 303
    .end local v6    # "BasedY":I
    .end local v7    # "MarkingSize":I
    .end local v8    # "VerticalOffset":I
    .end local v9    # "colHeight":F
    .end local v10    # "delta":D
    .end local v12    # "result":Ljava/lang/String;
    :cond_1
    return-void

    .line 285
    .restart local v6    # "BasedY":I
    .restart local v7    # "MarkingSize":I
    .restart local v8    # "VerticalOffset":I
    .restart local v9    # "colHeight":F
    :cond_2
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x5

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 286
    const/4 v8, 0x1

    .line 287
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/2addr v0, v8

    add-int/lit8 v6, v0, 0x28

    goto/16 :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 204
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isHover:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    invoke-virtual {v0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->isHoveringEnbale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverCircleSize:F

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_HoverAccurayUI;

    # getter for: Lcom/sec/android/app/status/SPen_HoverAccurayUI;->mTouchView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI;->access$400(Lcom/sec/android/app/status/SPen_HoverAccurayUI;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;

    invoke-direct {v0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->initRect()V

    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->displayInfoBox(Landroid/graphics/Canvas;)V

    .line 211
    return-void
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 307
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 309
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 348
    :goto_0
    :pswitch_0
    return v7

    .line 311
    :pswitch_1
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    const-string v4, "ACTION_HOVER_ENTER "

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    .line 313
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    .line 314
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    const-string v4, "ACTION_HOVER_ENTER test start!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iput-boolean v6, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    .line 316
    iput-boolean v6, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isHover:Z

    goto :goto_0

    .line 319
    :pswitch_2
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    const-string v4, "ACTION_HOVER_MOVE"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 322
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredX:F

    .line 323
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredY:F

    .line 324
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 325
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 328
    :cond_0
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredX:F

    .line 329
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredY:F

    .line 330
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    .line 331
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    .line 332
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PreX,Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredX:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredY:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CurX,Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iput-boolean v6, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    .line 336
    iput-boolean v6, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isHover:Z

    goto/16 :goto_0

    .line 339
    .end local v1    # "i":I
    :pswitch_3
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    const-string v4, "ACTION_HOVER_EXIT"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iput-boolean v7, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    .line 341
    iput-boolean v7, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isHover:Z

    .line 342
    const-string v2, "SPenTestUI"

    const-string v3, "onHover"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PREX,Y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredX:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreHoveredY:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredX:F

    .line 344
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mHoveredY:F

    goto/16 :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 215
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 217
    .local v0, "action":I
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 218
    packed-switch v0, :pswitch_data_0

    .line 268
    :cond_0
    :goto_0
    return v4

    .line 220
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 221
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 222
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchX:F

    .line 223
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mFisrtTouchY:F

    .line 224
    iput-boolean v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    .line 225
    iput-boolean v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->invalidate()V

    goto :goto_0

    .line 231
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 232
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 233
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedX:F

    .line 234
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedY:F

    .line 235
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 236
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 232
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 239
    :cond_1
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedX:F

    .line 240
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedY:F

    .line 241
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 242
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 243
    iput-boolean v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    .line 244
    iput-boolean v4, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    goto :goto_0

    .line 253
    .end local v1    # "i":I
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 254
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedX:F

    .line 255
    iget v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mPreTouchedY:F

    .line 256
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedX:F

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->mTouchedY:F

    .line 259
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isTouchDown:Z

    .line 260
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_HoverAccurayUI$TouchView;->isPenDetect:Z

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
