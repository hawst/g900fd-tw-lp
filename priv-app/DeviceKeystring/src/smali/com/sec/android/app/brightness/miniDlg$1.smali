.class Lcom/sec/android/app/brightness/miniDlg$1;
.super Ljava/lang/Object;
.source "miniDlg.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/brightness/miniDlg;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/brightness/miniDlg;


# direct methods
.method constructor <init>(Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg$1;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 107
    move v0, p2

    .line 108
    .local v0, "brightness":I
    add-int/lit8 v0, v0, 0x0

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$1;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "value :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg$1;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    # invokes: Lcom/sec/android/app/brightness/miniDlg;->setBrightness(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/brightness/miniDlg;->access$000(Lcom/sec/android/app/brightness/miniDlg;I)V

    .line 111
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 105
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 102
    return-void
.end method
