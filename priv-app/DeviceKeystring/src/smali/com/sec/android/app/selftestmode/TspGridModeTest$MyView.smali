.class public Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;
.super Landroid/view/View;
.source "TspGridModeTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/selftestmode/TspGridModeTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyView"
.end annotation


# instance fields
.field private isDrawArea:[[Z

.field private isTouchDown:Z

.field private mClickPaint:Landroid/graphics/Paint;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/selftestmode/TspGridModeTest;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/selftestmode/TspGridModeTest;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->this$0:Lcom/sec/android/app/selftestmode/TspGridModeTest;

    .line 72
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 58
    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    .line 59
    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    .line 60
    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    .line 61
    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    .line 73
    invoke-virtual {p1}, Lcom/sec/android/app/selftestmode/TspGridModeTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    .line 74
    invoke-virtual {p1}, Lcom/sec/android/app/selftestmode/TspGridModeTest;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    .line 75
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 77
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 79
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    iget v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 80
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 81
    const/16 v0, 0x9

    const/16 v1, 0xd

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isDrawArea:[[Z

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->setPaint()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->initRect()V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    .line 85
    return-void
.end method

.method private drawLine(FFFF)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 168
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 170
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_0

    .line 171
    float-to-int v6, p1

    .line 172
    float-to-int v8, p3

    .line 178
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_1

    .line 179
    float-to-int v7, p2

    .line 180
    float-to-int v9, p4

    .line 186
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x6

    add-int/lit8 v2, v9, -0x6

    add-int/lit8 v3, v6, 0x6

    add-int/lit8 v4, v7, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 187
    return-void

    .line 174
    :cond_0
    float-to-int v6, p3

    .line 175
    float-to-int v8, p1

    goto :goto_0

    .line 182
    :cond_1
    float-to-int v7, p4

    .line 183
    float-to-int v9, p2

    goto :goto_1
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 191
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x6

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x6

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x6

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 192
    return-void
.end method

.method private drawRect(FFLandroid/graphics/Paint;)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    const/high16 v1, 0x41500000    # 13.0f

    div-float v8, v0, v1

    .line 216
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x41100000    # 9.0f

    div-float v9, v0, v1

    .line 217
    .local v9, "col_width":F
    div-float v0, p1, v9

    float-to-int v10, v0

    .line 218
    .local v10, "countX":I
    div-float v0, p2, v8

    float-to-int v11, v0

    .line 219
    .local v11, "countY":I
    const/4 v6, 0x0

    .line 220
    .local v6, "ColX":F
    const/4 v7, 0x0

    .line 223
    .local v7, "ColY":F
    const/16 v0, 0x9

    if-lt v10, v0, :cond_0

    .line 224
    const/16 v10, 0x8

    .line 227
    :cond_0
    const/16 v0, 0xd

    if-lt v11, v0, :cond_1

    .line 228
    const/16 v11, 0xc

    .line 231
    :cond_1
    int-to-float v0, v10

    mul-float v6, v9, v0

    .line 232
    int-to-float v0, v11

    mul-float v7, v8, v0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isDrawArea:[[Z

    aget-object v0, v0, v10

    aget-boolean v0, v0, v11

    if-nez v0, :cond_2

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isDrawArea:[[Z

    aget-object v0, v0, v10

    const/4 v1, 0x1

    aput-boolean v1, v0, v11

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    float-to-int v1, v6

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    float-to-int v2, v7

    add-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float v3, v6, v9

    float-to-int v3, v3

    int-to-float v3, v3

    add-float v4, v7, v8

    float-to-int v4, v4

    int-to-float v4, v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 238
    new-instance v0, Landroid/graphics/Rect;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, v6, v1

    float-to-int v1, v1

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float v2, v7, v2

    float-to-int v2, v2

    add-float v3, v6, v9

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v3, v4

    float-to-int v3, v3

    add-float v4, v7, v8

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->invalidate(Landroid/graphics/Rect;)V

    .line 241
    :cond_2
    return-void
.end method

.method private initRect()V
    .locals 12

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    int-to-float v0, v0

    const/high16 v1, 0x41500000    # 13.0f

    div-float v8, v0, v1

    .line 196
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x41100000    # 9.0f

    div-float v9, v0, v1

    .line 197
    .local v9, "col_width":F
    const/4 v6, 0x0

    .line 198
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 199
    .local v7, "ColY":I
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 200
    .local v5, "mRectPaint":Landroid/graphics/Paint;
    const/high16 v0, -0x1000000

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 202
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    const/16 v0, 0xd

    if-ge v10, v0, :cond_1

    .line 203
    int-to-float v0, v10

    mul-float/2addr v0, v8

    float-to-int v7, v0

    .line 205
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    const/16 v0, 0x9

    if-ge v11, v0, :cond_0

    .line 206
    int-to-float v0, v11

    mul-float/2addr v0, v9

    float-to-int v6, v0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v7

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mScreenHeight:I

    int-to-float v4, v4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isDrawArea:[[Z

    aget-object v0, v0, v11

    const/4 v1, 0x0

    aput-boolean v1, v0, v10

    .line 205
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 202
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 212
    .end local v11    # "j":I
    :cond_1
    return-void
.end method

.method private setPaint()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/high16 v3, -0x1000000

    .line 88
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 96
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 99
    .local v0, "e":Landroid/graphics/PathEffect;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 104
    return-void

    .line 96
    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 110
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 114
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 116
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 163
    :cond_0
    :goto_0
    return v6

    .line 118
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    .line 120
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 121
    iput-boolean v6, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 125
    :pswitch_1
    iget-boolean v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 126
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 127
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    .line 128
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    .line 129
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    .line 130
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    .line 131
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 132
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawLine(FFFF)V

    .line 126
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 135
    :cond_1
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    .line 136
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    .line 137
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    .line 138
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    .line 139
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iget-object v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mClickPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawRect(FFLandroid/graphics/Paint;)V

    .line 140
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    iget v4, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v5, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawLine(FFFF)V

    .line 141
    iput-boolean v6, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    goto :goto_0

    .line 147
    .end local v1    # "i":I
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    if-eqz v2, :cond_0

    .line 148
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    .line 149
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    .line 150
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    .line 151
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    .line 153
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    .line 154
    iget v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedX:F

    iget v3, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->mTouchedY:F

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->drawPoint(FF)V

    .line 157
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/selftestmode/TspGridModeTest$MyView;->isTouchDown:Z

    goto/16 :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
