.class Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;
.super Ljava/lang/Object;
.source "CoreCTRL.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

.field final synthetic val$parentID:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;I)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iput p2, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->val$parentID:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 201
    iget v0, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->val$parentID:I

    packed-switch v0, :pswitch_data_0

    .line 223
    :pswitch_0
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "default"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :goto_0
    return-void

    .line 203
    :pswitch_1
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "cpu0_freq_min"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v0, v0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    iget-object v1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v1, v1, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/CoreCTRL;->access$100(Lcom/sec/android/app/status/CoreCTRL;)[Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/status/CoreCTRL;->setChooseText(Landroid/widget/TextView;I)V

    .line 205
    const-string v0, "CPU0_MIN_FREQ"

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/CoreCTRL;->access$000()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 208
    :pswitch_2
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "cpu0_freq_max"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v0, v0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    iget-object v1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v1, v1, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/CoreCTRL;->access$100(Lcom/sec/android/app/status/CoreCTRL;)[Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/status/CoreCTRL;->setChooseText(Landroid/widget/TextView;I)V

    .line 210
    const-string v0, "CPU0_MAX_FREQ"

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/CoreCTRL;->access$000()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 213
    :pswitch_3
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "cpu1_freq_min"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v0, v0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    iget-object v1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v1, v1, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/CoreCTRL;->access$100(Lcom/sec/android/app/status/CoreCTRL;)[Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/status/CoreCTRL;->setChooseText(Landroid/widget/TextView;I)V

    .line 215
    const-string v0, "CPU1_MIN_FREQ"

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/CoreCTRL;->access$000()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 218
    :pswitch_4
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "cpu1_freq_max"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v0, v0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    iget-object v1, p0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment$1;->this$1:Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    iget-object v1, v1, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->this$0:Lcom/sec/android/app/status/CoreCTRL;

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/CoreCTRL;->access$100(Lcom/sec/android/app/status/CoreCTRL;)[Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/status/CoreCTRL;->setChooseText(Landroid/widget/TextView;I)V

    .line 220
    const-string v0, "CPU1_MAX_FREQ"

    # getter for: Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/CoreCTRL;->access$000()[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x7f0a004c
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
