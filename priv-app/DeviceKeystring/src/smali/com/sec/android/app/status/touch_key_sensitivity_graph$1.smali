.class Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;
.super Landroid/os/Handler;
.source "touch_key_sensitivity_graph.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/touch_key_sensitivity_graph;->drawGraph()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 285
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFiveKeyModel:Z
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Lcom/sec/android/app/status/touch_key_sensitivity_view;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I
    invoke-static {v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v6

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I
    invoke-static {v7}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I
    invoke-static {v8}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy5KeyValue:I
    invoke-static {v9}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v10}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v11}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v12}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v13}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I
    invoke-static {v14}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v14

    invoke-virtual/range {v1 .. v14}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFFIIIIIIII)V

    .line 303
    :goto_0
    return-void

    .line 290
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isSixKeyModel:Z
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Lcom/sec/android/app/status/touch_key_sensitivity_view;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I
    invoke-static {v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v6

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I
    invoke-static {v7}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I
    invoke-static {v8}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy4KeyValue:I
    invoke-static {v9}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy6KeyValue:I
    invoke-static {v10}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v11}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v12}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v13}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v14}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue4:I
    invoke-static {v15}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue5:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1800(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v16

    invoke-virtual/range {v1 .. v16}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFFIIIIIIIIII)V

    goto/16 :goto_0

    .line 295
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->isFourKeyModel:Z
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 296
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Lcom/sec/android/app/status/touch_key_sensitivity_view;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I
    invoke-static {v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v6

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy1KeyValue:I
    invoke-static {v7}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$600(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->dummy3KeyValue:I
    invoke-static {v8}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$700(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue0:I
    invoke-static {v9}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$900(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue1:I
    invoke-static {v10}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1000(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue2:I
    invoke-static {v11}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue3:I
    invoke-static {v12}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v12

    invoke-virtual/range {v1 .. v12}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFFIIIIII)V

    goto/16 :goto_0

    .line 300
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->touchKeyGraph:Lcom/sec/android/app/status/touch_key_sensitivity_view;
    invoke-static {v1}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$1400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)Lcom/sec/android/app/status/touch_key_sensitivity_view;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->backKeyValue:I
    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$100(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->homeKeyValue:I
    invoke-static {v3}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$200(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->recentKeyValue:I
    invoke-static {v4}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$300(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->menuKeyValue:I
    invoke-static {v5}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$400(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v5

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_graph$1;->this$0:Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    # getter for: Lcom/sec/android/app/status/touch_key_sensitivity_graph;->thresholdValue:I
    invoke-static {v6}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->access$500(Lcom/sec/android/app/status/touch_key_sensitivity_graph;)I

    move-result v6

    int-to-float v6, v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFF)V

    goto/16 :goto_0
.end method
