.class public Lcom/sec/android/app/status/GripSensorCalibrationEntry;
.super Landroid/app/Activity;
.source "GripSensorCalibrationEntry.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;,
        Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;,
        Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;
    }
.end annotation


# instance fields
.field private gripSensorName:Ljava/lang/String;

.field private mButtonGoCALTest:[Landroid/widget/Button;

.field private mButtonGripSensorSwitch:Landroid/widget/Button;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

.field mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

.field private mHandler:Landroid/os/Handler;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field public mUserBinFunc:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mUserBinFunc:Z

    .line 35
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mHandler:Landroid/os/Handler;

    .line 92
    sget-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 311
    return-void
.end method

.method private EnableButtons()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 188
    const-string v3, "GripSensorCalibrationEntry"

    const-string v4, "EnableButtons"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mUserBinFunc:Z

    if-eqz v3, :cond_0

    .line 191
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getUiID()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 197
    .end local v1    # "i":I
    :cond_0
    const/4 v2, 0x1

    .line 198
    .local v2, "unknownSensor":Z
    const-string v3, "GRIP_SENSOR_NAME"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "gripSensorName":Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getUiID()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 203
    const/4 v2, 0x0

    .line 200
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 207
    :cond_2
    if-eqz v2, :cond_3

    .line 208
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown sensor name("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 209
    const/4 v1, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 210
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getUiID()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 213
    .end local v0    # "gripSensorName":Ljava/lang/String;
    .end local v2    # "unknownSensor":Z
    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorCalibrationEntry;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationEntry;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->updateGripSensorSwitch()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibrationEntry;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationEntry;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->updateGripSensor2Switch()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibrationEntry;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationEntry;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    return-object v0
.end method

.method private registerGripSensor()Z
    .locals 5

    .prologue
    .line 281
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "registerGripSensor"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const/4 v0, 0x0

    .line 284
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_0

    .line 285
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    .line 287
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    if-nez v1, :cond_1

    .line 288
    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationEntry$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    .line 290
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensor:Landroid/hardware/Sensor;

    .line 294
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensor:Landroid/hardware/Sensor;

    if-eqz v1, :cond_3

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 298
    :cond_3
    return v0
.end method

.method private setGripSensorSwitch()V
    .locals 5

    .prologue
    .line 216
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "setGripSensorSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CurrentState"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    if-ne v1, v2, :cond_2

    .line 219
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 220
    const-string v1, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH_2ND"

    const-string v2, "0"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 224
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->gripSensorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 225
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "setGripSensorSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gripsensor Off : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->gripSensorName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :goto_0
    return-void

    .line 227
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GRIPSENSOR_CP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "cmd"

    const-string v2, "off"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->sendBroadcast(Landroid/content/Intent;)V

    .line 230
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->registerGripSensor()Z

    goto :goto_0

    .line 232
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    if-ne v1, v2, :cond_5

    .line 233
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 234
    const-string v1, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 235
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH_2ND"

    const-string v2, "1"

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 238
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->gripSensorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 239
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "setGripSensorSwitch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Gripsensor On : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->gripSensorName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GRIPSENSOR_CP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 242
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "cmd"

    const-string v2, "on"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->sendBroadcast(Landroid/content/Intent;)V

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->unRegisterGripSensor()V

    goto/16 :goto_0

    .line 247
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v1, "Can not set the Grip sensor on/off. Current Status is UNKNOWN"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private unRegisterGripSensor()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 302
    const-string v0, "GripSensorCalibrationEntry"

    const-string v1, "unRegisterGripSensor"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 306
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorListener;

    .line 307
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mSensorManager:Landroid/hardware/SensorManager;

    .line 308
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensor:Landroid/hardware/Sensor;

    .line 309
    return-void
.end method

.method private updateGripSensor2Switch()V
    .locals 5

    .prologue
    const v4, 0x7f0700b9

    .line 267
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "updateGripSensor2Switch"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH_2ND"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "gripSensor2OnOff":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 279
    :goto_0
    return-void

    .line 272
    :cond_0
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 273
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 274
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 276
    :cond_1
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateGripSensorSwitch()V
    .locals 5

    .prologue
    const v4, 0x7f0700b9

    .line 252
    const-string v1, "GripSensorCalibrationEntry"

    const-string v2, "updateGripSensorSwitch"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v1, "GRIP_SENSOR_EVENT_SWITCH"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "gripSensorOnOff":Ljava/lang/String;
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->ON:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 265
    :goto_0
    return-void

    .line 258
    :cond_0
    const-string v1, "0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 259
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->OFF:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 262
    :cond_1
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorState:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;->UNKNOWN:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 153
    const-string v3, "GripSensorCalibrationEntry"

    const-string v4, "onClick"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object v2, p1

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getUiID()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 176
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/status/GripSensorCalibrationCalScreen;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 178
    const-string v2, "grip_sensor_name"

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getSensorName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->startActivity(Landroid/content/Intent;)V

    .line 185
    .end local v0    # "i":I
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 156
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->setGripSensorSwitch()V

    move-object v2, p1

    .line 158
    check-cast v2, Landroid/widget/Button;

    const-string v3, "Waiting..."

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 159
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$1;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationEntry;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 174
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :pswitch_data_0
    .packed-switch 0x7f0a00b8
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    const-string v3, "GripSensorCalibrationEntry"

    const-string v4, "onCreate"

    const-string v5, "onCreate"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const v3, 0x7f03002b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->setContentView(I)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 107
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "GRIP_SENSOR_NAME"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->gripSensorName:Ljava/lang/String;

    .line 108
    if-eqz v2, :cond_1

    const-string v3, "host"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "host":Ljava/lang/String;
    :goto_0
    const-string v3, "1106"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    const-string v3, "GripSensorCalibrationEntry"

    const-string v4, "onCreate"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "host="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mUserBinFunc:Z

    .line 114
    :cond_0
    invoke-static {}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->values()[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v3, v3

    new-array v3, v3, [Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGoCALTest:[Landroid/widget/Button;

    .line 117
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 118
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGoCALTest:[Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->getUiID()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    aput-object v3, v4, v1

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGoCALTest:[Landroid/widget/Button;

    aget-object v3, v3, v1

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGoCALTest:[Landroid/widget/Button;

    aget-object v3, v3, v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v5, v1, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mGripSensorArray:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 117
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 108
    .end local v0    # "host":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    .restart local v0    # "host":Ljava/lang/String;
    .restart local v1    # "i":I
    :cond_2
    const v3, 0x7f0a00b8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    .line 124
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->mButtonGripSensorSwitch:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->EnableButtons()V

    .line 127
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 147
    const-string v0, "GripSensorCalibrationEntry"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 149
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 141
    const-string v0, "GripSensorCalibrationEntry"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 143
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 131
    const-string v0, "GripSensorCalibrationEntry"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->updateGripSensorSwitch()V

    .line 133
    const-string v0, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationEntry;->updateGripSensor2Switch()V

    .line 136
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 137
    return-void
.end method
