.class Lcom/sec/android/app/status/PCMDump$1;
.super Ljava/lang/Object;
.source "PCMDump.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/PCMDump;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/PCMDump;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/PCMDump;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/status/PCMDump$1;->this$0:Lcom/sec/android/app/status/PCMDump;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "focus"    # Z

    .prologue
    .line 81
    if-nez p2, :cond_1

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/status/PCMDump$1;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/sec/android/app/status/PCMDump;->access$000(Lcom/sec/android/app/status/PCMDump;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "volume":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 90
    .local v0, "iVolume":I
    :goto_0
    const/16 v3, 0x64

    if-le v0, v3, :cond_2

    .line 91
    const/16 v0, 0x64

    .line 95
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/PCMDump$1;->this$0:Lcom/sec/android/app/status/PCMDump;

    # invokes: Lcom/sec/android/app/status/PCMDump;->setStreamMusicVolume(I)V
    invoke-static {v3, v0}, Lcom/sec/android/app/status/PCMDump;->access$100(Lcom/sec/android/app/status/PCMDump;I)V

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/status/PCMDump$1;->this$0:Lcom/sec/android/app/status/PCMDump;

    # getter for: Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/sec/android/app/status/PCMDump;->access$000(Lcom/sec/android/app/status/PCMDump;)Landroid/widget/EditText;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 98
    .end local v0    # "iVolume":I
    .end local v2    # "volume":Ljava/lang/String;
    :cond_1
    return-void

    .line 86
    .restart local v2    # "volume":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 87
    .local v1, "ne":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    .line 88
    const/16 v0, 0x64

    .restart local v0    # "iVolume":I
    goto :goto_0

    .line 92
    .end local v1    # "ne":Ljava/lang/NumberFormatException;
    :cond_2
    if-gez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    goto :goto_1
.end method
