.class public Lcom/sec/android/app/status/LoopbackTest;
.super Landroid/app/Activity;
.source "LoopbackTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field final LOOPBACK_TYPE:[Ljava/lang/String;

.field final LOOPBACK_TYPE_CODEC:I

.field final LOOPBACK_TYPE_PACKET:I

.field final LOOPBACK_TYPE_PCM:I

.field final LOOPBACK_TYPE_REALTIME:I

.field private PATH_RCV:Ljava/lang/String;

.field private PATH_SPK:Ljava/lang/String;

.field private _am:Landroid/media/AudioManager;

.field private mIsDoingLoopback:Z

.field product:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    const-string v0, "LoopbackTest"

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->CLASS_NAME:Ljava/lang/String;

    .line 28
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->product:Ljava/lang/String;

    .line 31
    iput-boolean v2, p0, Lcom/sec/android/app/status/LoopbackTest;->mIsDoingLoopback:Z

    .line 167
    iput v2, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE_PACKET:I

    .line 168
    iput v3, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE_PCM:I

    .line 169
    iput v4, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE_REALTIME:I

    .line 170
    iput v5, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE_CODEC:I

    .line 171
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "packet;"

    aput-object v1, v0, v2

    const-string v1, "pcm;"

    aput-object v1, v0, v3

    const-string v1, "realtime"

    aput-object v1, v0, v4

    const-string v1, "codec"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE:[Ljava/lang/String;

    return-void
.end method

.method public static isSupportSecondMicTest()Z
    .locals 3

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "result":Z
    const-string v1, "SUPPORT_SECOND_MIC_TEST"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 220
    return v0
.end method


# virtual methods
.method public isNeedRcv()Z
    .locals 2

    .prologue
    .line 248
    const-string v0, "true"

    const-string v1, "IS_NEED_RCV"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVoiceCapable()Z
    .locals 6

    .prologue
    .line 235
    const/4 v1, 0x1

    .line 236
    .local v1, "r":Z
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 238
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1120044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 243
    :cond_0
    const-string v2, "LoopbackTest"

    const-string v3, "isVoiceCapable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isVoiceCapable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return v1
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 7
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 99
    const-string v0, "NONE"

    .line 101
    .local v0, "path":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 118
    :goto_0
    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 119
    iget-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest;->mIsDoingLoopback:Z

    if-nez v1, :cond_2

    .line 120
    const-string v1, "LoopbackTest"

    const-string v2, "onCheckedChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "product = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest;->product:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "force_packet"

    const-string v2, "WHAT_LOOPBACK_PATH_SUPPORTED"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTest;->startLoopback(Ljava/lang/String;I)V

    .line 139
    :goto_1
    return-void

    .line 103
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->PATH_RCV:Ljava/lang/String;

    .line 104
    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->PATH_SPK:Ljava/lang/String;

    .line 107
    goto :goto_0

    .line 109
    :pswitch_2
    const-string v0, "ear_ear;"

    .line 110
    goto :goto_0

    .line 112
    :pswitch_3
    const-string v0, "NONE"

    .line 113
    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTest;->startLoopback(Ljava/lang/String;I)V

    goto :goto_1

    .line 129
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "force_packet"

    const-string v2, "WHAT_LOOPBACK_PATH_SUPPORTED"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 131
    :cond_3
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTest;->setLoopbackPath(Ljava/lang/String;I)V

    goto :goto_1

    .line 133
    :cond_4
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTest;->setLoopbackPath(Ljava/lang/String;I)V

    goto :goto_1

    .line 137
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->stopLoopback()V

    goto :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0110
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 150
    :goto_0
    return-void

    .line 145
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->finish()V

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x7f0a00a4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    const-string v0, "LoopbackTest"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v0, 0x7f030042

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->setContentView(I)V

    .line 39
    const v0, 0x7f0a010f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 40
    const v0, 0x7f0a00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    .line 42
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 69
    const-string v0, "LoopbackTest"

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    packed-switch p1, :pswitch_data_0

    .line 79
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 73
    :pswitch_0
    const-string v0, "LoopbackTest"

    const-string v1, "ignore"

    const-string v2, " KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const/4 v0, 0x1

    goto :goto_0

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 84
    const-string v0, "UIBase"

    const-string v1, "onKeyUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    packed-switch p1, :pswitch_data_0

    .line 94
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 88
    :pswitch_0
    const-string v0, "LoopbackTest"

    const-string v1, "ignore"

    const-string v2, "KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "LoopbackTest"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 64
    const v0, 0x7f0a0113

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 65
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 47
    const-string v1, "LoopbackTest"

    const-string v2, "onResume"

    const-string v3, "onResume"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const v1, 0x7f0a0113

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isVoiceCapable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isNeedRcv()Z

    move-result v1

    if-nez v1, :cond_1

    .line 51
    :cond_0
    const v1, 0x7f0a0110

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTest;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 52
    .local v0, "RcvON_RadioButton":Landroid/widget/RadioButton;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 53
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setTextColor(I)V

    .line 56
    .end local v0    # "RcvON_RadioButton":Landroid/widget/RadioButton;
    :cond_1
    const-string v1, "PATH_RCV_LOOPBACK"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest;->PATH_RCV:Ljava/lang/String;

    .line 57
    const-string v1, "PATH_SPK_LOOPBACK"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest;->PATH_SPK:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setLoopbackPath(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 224
    const-string v0, "LoopbackTest"

    const-string v1, "setLoopbackPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLoopbackPath path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->setStreamMusicVolumeMax()V

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public setStreamMusicVolumeMax()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 232
    return-void
.end method

.method public startLoopback(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 176
    const-string v0, "LoopbackTest"

    const-string v1, "startLoopback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startLoopback path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->setStreamMusicVolumeMax()V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "force_packet"

    const-string v1, "WHAT_LOOPBACK_PATH_SUPPORTED"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/LoopbackTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/LoopbackTest$1;-><init>(Lcom/sec/android/app/status/LoopbackTest;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 195
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/LoopbackTest;->mIsDoingLoopback:Z

    .line 196
    return-void
.end method

.method public stopLoopback()V
    .locals 4

    .prologue
    .line 199
    const-string v1, "LoopbackTest"

    const-string v2, "stopLoopback"

    const-string v3, "stopLoopback"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    const-string v2, "factory_test_loopback=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "force_packet"

    const-string v2, "WHAT_LOOPBACK_PATH_SUPPORTED"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    const-string v1, "LoopbackTest"

    const-string v2, "stopLoopback"

    const-string v3, "send moduleAudioService stop intent"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 206
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/status/ModuleAudioService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 207
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest;->stopService(Landroid/content/Intent;)Z

    .line 210
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest;->mIsDoingLoopback:Z

    .line 212
    invoke-static {}, Lcom/sec/android/app/status/LoopbackTest;->isSupportSecondMicTest()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest;->_am:Landroid/media/AudioManager;

    const-string v2, "dualmic_enabled=false"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 215
    :cond_1
    return-void
.end method
