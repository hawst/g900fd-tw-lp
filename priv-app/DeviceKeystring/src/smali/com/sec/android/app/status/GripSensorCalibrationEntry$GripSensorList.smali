.class public final enum Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;
.super Ljava/lang/Enum;
.source "GripSensorCalibrationEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GripSensorList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

.field public static final enum AD7146:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

.field public static final enum ASP01:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

.field public static final enum SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

.field public static final enum SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;


# instance fields
.field private mComponent:Landroid/content/ComponentName;

.field private mSensorName:Ljava/lang/String;

.field private mUiID:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    const-string v1, "ASP01"

    const-string v3, "ASP01"

    const v4, 0x7f0a00b4

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.sec.android.app.factorykeystring"

    const-string v7, "com.sec.android.app.status.GripSensorCalibrationASPxx"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;-><init>(Ljava/lang/String;ILjava/lang/String;ILandroid/content/ComponentName;)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->ASP01:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 41
    new-instance v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    const-string v4, "AD7146"

    const-string v6, "AD7146"

    const v7, 0x7f0a00b5

    new-instance v8, Landroid/content/ComponentName;

    const-string v0, "com.sec.android.app.factorykeystring"

    const-string v1, "com.sec.android.app.status.GripSensorCalibrationADxxxx"

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;-><init>(Ljava/lang/String;ILjava/lang/String;ILandroid/content/ComponentName;)V

    sput-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->AD7146:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 42
    new-instance v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    const-string v4, "SX9500"

    const-string v6, "SX9500"

    const v7, 0x7f0a00b6

    const-string v0, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v8, Landroid/content/ComponentName;

    const-string v0, "com.sec.android.app.factorykeystring"

    const-string v1, "com.sec.android.app.status.GripSensorCalibrationDoubleSXxxxx"

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;-><init>(Ljava/lang/String;ILjava/lang/String;ILandroid/content/ComponentName;)V

    sput-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 43
    new-instance v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    const-string v4, "SX9306"

    const-string v6, "SX9306"

    const v7, 0x7f0a00b7

    const-string v0, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v8, Landroid/content/ComponentName;

    const-string v0, "com.sec.android.app.factorykeystring"

    const-string v1, "com.sec.android.app.status.GripSensorCalibrationDoubleSXxxxx"

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;-><init>(Ljava/lang/String;ILjava/lang/String;ILandroid/content/ComponentName;)V

    sput-object v3, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->ASP01:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->AD7146:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    aput-object v1, v0, v11

    sput-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->$VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    return-void

    .line 42
    :cond_0
    new-instance v8, Landroid/content/ComponentName;

    const-string v0, "com.sec.android.app.factorykeystring"

    const-string v1, "com.sec.android.app.status.GripSensorCalibrationSXxxxx"

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_1
    new-instance v8, Landroid/content/ComponentName;

    const-string v0, "com.sec.android.app.factorykeystring"

    const-string v1, "com.sec.android.app.status.GripSensorCalibrationSXxxxx"

    invoke-direct {v8, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ILandroid/content/ComponentName;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "uiID"    # I
    .param p5, "component"    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Landroid/content/ComponentName;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mSensorName:Ljava/lang/String;

    .line 47
    iput p4, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mUiID:I

    .line 48
    iput-object p5, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mComponent:Landroid/content/ComponentName;

    .line 49
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->$VALUES:[Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    return-object v0
.end method


# virtual methods
.method getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mComponent:Landroid/content/ComponentName;

    return-object v0
.end method

.method getSensorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mSensorName:Ljava/lang/String;

    return-object v0
.end method

.method getUiID()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mUiID:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->mSensorName:Ljava/lang/String;

    return-object v0
.end method
