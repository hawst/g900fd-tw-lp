.class Lcom/sec/android/app/status/TspDataMeasure$1;
.super Landroid/os/Handler;
.source "TspDataMeasure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspDataMeasure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/TspDataMeasure;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/TspDataMeasure;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 136
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 160
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "handleMessage"

    const-string v2, "START_SAVE_TSP_DATA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # getter for: Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$000(Lcom/sec/android/app/status/TspDataMeasure;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->getNodeData(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$100(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->hideProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspDataMeasure;->access$200(Lcom/sec/android/app/status/TspDataMeasure;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->refreshExtSdCard()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspDataMeasure;->access$300(Lcom/sec/android/app/status/TspDataMeasure;)V

    goto :goto_0

    .line 145
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "handleMessage"

    const-string v2, "START_SAVE_TSP_HOVER_DATA"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # getter for: Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$000(Lcom/sec/android/app/status/TspDataMeasure;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->getNodeDataHover(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$400(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->hideProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspDataMeasure;->access$200(Lcom/sec/android/app/status/TspDataMeasure;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->refreshExtSdCard()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspDataMeasure;->access$300(Lcom/sec/android/app/status/TspDataMeasure;)V

    goto :goto_0

    .line 152
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "handleMessage"

    const-string v2, "START_SAVE_SIDE_TOUCH"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # getter for: Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$000(Lcom/sec/android/app/status/TspDataMeasure;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->getNodeDataSideTouch(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/TspDataMeasure;->access$500(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    iget-object v0, v0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3eb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure$1;->this$0:Lcom/sec/android/app/status/TspDataMeasure;

    # invokes: Lcom/sec/android/app/status/TspDataMeasure;->hideProgressDialog()V
    invoke-static {v0}, Lcom/sec/android/app/status/TspDataMeasure;->access$200(Lcom/sec/android/app/status/TspDataMeasure;)V

    goto :goto_0

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
