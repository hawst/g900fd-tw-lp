.class Lcom/sec/android/app/version/FTAHWVersion$1;
.super Landroid/content/BroadcastReceiver;
.source "FTAHWVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/version/FTAHWVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/version/FTAHWVersion;


# direct methods
.method constructor <init>(Lcom/sec/android/app/version/FTAHWVersion;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/version/FTAHWVersion$1;->this$0:Lcom/sec/android/app/version/FTAHWVersion;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 42
    .local v0, "mAction":Ljava/lang/String;
    const-string v1, "android.intent.action.GET_FTA_RESPONSE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/version/FTAHWVersion$1;->this$0:Lcom/sec/android/app/version/FTAHWVersion;

    const-string v2, "fta_hw_ver"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/version/FTAHWVersion;->result:Ljava/lang/String;

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/version/FTAHWVersion$1;->this$0:Lcom/sec/android/app/version/FTAHWVersion;

    iget-object v1, v1, Lcom/sec/android/app/version/FTAHWVersion;->ftaHwvertv:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FTA HW VERSION: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/version/FTAHWVersion$1;->this$0:Lcom/sec/android/app/version/FTAHWVersion;

    iget-object v3, v3, Lcom/sec/android/app/version/FTAHWVersion;->result:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_0
    return-void
.end method
