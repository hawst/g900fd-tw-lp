.class Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;
.super Ljava/lang/Object;
.source "GripSensorTest2.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorTest2;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/GripSensorTest2;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/GripSensorTest2;Lcom/sec/android/app/status/GripSensorTest2$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p2, "x1"    # Lcom/sec/android/app/status/GripSensorTest2$1;

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorTest2;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 386
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const v8, 0x7f070099

    const/4 v7, 0x0

    const v6, 0x7f070096

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 389
    const-string v0, "IS_SUPPORT_DOUBLE_GRIPSNESOR"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 390
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor1 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    cmpl-float v0, v0, v7

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$900(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1000(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mWorkingColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1200(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/status/GripSensorTest2;->access$902(Lcom/sec/android/app/status/GripSensorTest2;Z)Z

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->startVibration(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorTest2;->access$1300(Lcom/sec/android/app/status/GripSensorTest2;I)V

    .line 397
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_0
    :goto_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    cmpl-float v0, v0, v7

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1400(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1700(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mWorkingColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1800(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->startVibration(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorTest2;->access$1300(Lcom/sec/android/app/status/GripSensorTest2;I)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/status/GripSensorTest2;->access$1402(Lcom/sec/android/app/status/GripSensorTest2;Z)Z

    .line 415
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_1
    :goto_1
    return-void

    .line 399
    :cond_2
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v5

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorTest2;->access$902(Lcom/sec/android/app/status/GripSensorTest2;Z)Z

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$900(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1400(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1500(Lcom/sec/android/app/status/GripSensorTest2;)V

    .line 405
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1000(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1600(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1200(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 407
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release1"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 417
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    const/high16 v1, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorTest2;->access$1402(Lcom/sec/android/app/status/GripSensorTest2;Z)Z

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$900(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1400(Lcom/sec/android/app/status/GripSensorTest2;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1500(Lcom/sec/android/app/status/GripSensorTest2;)V

    .line 423
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1700(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1600(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1800(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 425
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 429
    :cond_6
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor1 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    cmpl-float v0, v0, v7

    if-nez v0, :cond_7

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1000(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mWorkingColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1200(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->startVibration(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorTest2;->access$1300(Lcom/sec/android/app/status/GripSensorTest2;I)V

    .line 435
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 438
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # invokes: Lcom/sec/android/app/status/GripSensorTest2;->stopVibration()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1500(Lcom/sec/android/app/status/GripSensorTest2;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1000(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1600(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/status/GripSensorTest2;->access$1700(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$1600(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/GripSensorTest2;->setBackgroundColor(Landroid/widget/LinearLayout;I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1200(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$1800(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    .line 443
    const-string v0, "GripSensorTest2"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
