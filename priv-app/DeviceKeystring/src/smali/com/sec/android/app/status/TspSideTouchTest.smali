.class public Lcom/sec/android/app/status/TspSideTouchTest;
.super Landroid/app/Activity;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TspSideTouchTest$3;,
        Lcom/sec/android/app/status/TspSideTouchTest$CGMvClient;,
        Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;,
        Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;,
        Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;,
        Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;,
        Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;,
        Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;,
        Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;,
        Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;,
        Lcom/sec/android/app/status/TspSideTouchTest$PollerSideTouchSensitivityReader;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;,
        Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;,
        Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;,
        Lcom/sec/android/app/status/TspSideTouchTest$Key;,
        Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;,
        Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;,
        Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;,
        Lcom/sec/android/app/status/TspSideTouchTest$KeyValueHandler;,
        Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;
    }
.end annotation


# static fields
.field private static CLASS_NAME:Ljava/lang/String;


# instance fields
.field private mFinishHandler:Landroid/os/Handler;

.field private mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "TspSideTouchTest"

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-direct {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    .line 114
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$FinishHandler;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Lcom/sec/android/app/status/TspSideTouchTest$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mFinishHandler:Landroid/os/Handler;

    .line 2008
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/TspSideTouchTest;)Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V

    return-void
.end method

.method private addButtonWidget(Landroid/widget/LinearLayout;)V
    .locals 4
    .param p1, "layout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 147
    const-string v2, "Exit"

    new-instance v3, Lcom/sec/android/app/status/TspSideTouchTest$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/TspSideTouchTest$1;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;)V

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest;->buttonFactory(Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v0

    .line 155
    .local v0, "btnExit":Landroid/widget/Button;
    const-string v2, "Reset"

    new-instance v3, Lcom/sec/android/app/status/TspSideTouchTest$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/TspSideTouchTest$2;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;)V

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest;->buttonFactory(Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/widget/Button;

    move-result-object v1

    .line 164
    .local v1, "btnReset":Landroid/widget/Button;
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 165
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 166
    return-void
.end method

.method private buttonFactory(Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/widget/Button;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v3, -0x2

    .line 169
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p0}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 170
    .local v0, "button":Landroid/widget/Button;
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 171
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 172
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 173
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setWidth(I)V

    .line 174
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    return-object v0
.end method

.method private requestFinish()V
    .locals 0

    .prologue
    .line 112
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$ScreenSize;->setScreenSize(Landroid/app/Activity;)V

    .line 51
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    new-instance v3, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Lcom/sec/android/app/status/TspSideTouchTest$1;)V

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->makeKeys()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->add(Ljava/util/List;)V

    .line 53
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 54
    .local v0, "fLayout":Landroid/widget/FrameLayout;
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->addKeys(Landroid/widget/FrameLayout;)V

    .line 56
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 57
    .local v1, "middleLayout":Landroid/widget/LinearLayout;
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 58
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 59
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    invoke-direct {p0, v1}, Lcom/sec/android/app/status/TspSideTouchTest;->addButtonWidget(Landroid/widget/LinearLayout;)V

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->addViewTouchSpec(Landroid/widget/LinearLayout;)V

    .line 69
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 70
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspSideTouchTest;->setContentView(Landroid/view/View;)V

    .line 71
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 94
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 95
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->broadcastKeyEvent(ILandroid/view/KeyEvent;)V

    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->broadcastKeyEvent(ILandroid/view/KeyEvent;)V

    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->onFinish()V

    .line 84
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->onFinish(Landroid/content/Context;)V

    .line 85
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->onFinish(Landroid/content/Context;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest;->mKeyHandler:Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeyHandler;->release()V

    .line 87
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->terminate()V

    .line 88
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$PollerSideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->terminate()V

    .line 89
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 90
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 76
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->onStart(Landroid/content/Context;)V

    .line 77
    invoke-static {p0}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->onStart(Landroid/content/Context;)V

    .line 78
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->onStart(Landroid/content/Context;)V

    .line 79
    return-void
.end method
