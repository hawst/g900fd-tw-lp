.class public Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
.super Landroid/os/CountDownTimer;
.source "miniDlg.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/brightness/miniDlg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RepeatTimer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/brightness/miniDlg;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/brightness/miniDlg;JJ)V
    .locals 0
    .param p2, "millisInFuture"    # J
    .param p4, "countDownInterval"    # J

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    .line 220
    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 221
    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 0

    .prologue
    .line 226
    return-void
.end method

.method public onTick(J)V
    .locals 4
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 230
    iget-object v2, p0, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    const v3, 0x7f0a0039

    invoke-virtual {v2, v3}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    .line 231
    .local v1, "seek":Landroid/widget/SeekBar;
    iget-object v2, p0, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v2}, Lcom/sec/android/app/brightness/miniDlg;->getBrightnessLevel()I

    move-result v0

    .line 233
    .local v0, "brightnessLevel":I
    iget-object v2, p0, Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;->this$0:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v2}, Lcom/sec/android/app/brightness/miniDlg;->getRepeatType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 253
    :goto_0
    return-void

    .line 236
    :pswitch_0
    if-lez v0, :cond_0

    .line 237
    add-int/lit8 v0, v0, -0x1

    .line 240
    :cond_0
    # getter for: Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I
    invoke-static {}, Lcom/sec/android/app/brightness/miniDlg;->access$100()[I

    move-result-object v2

    aget v2, v2, v0

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 244
    :pswitch_1
    # getter for: Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I
    invoke-static {}, Lcom/sec/android/app/brightness/miniDlg;->access$100()[I

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 245
    add-int/lit8 v0, v0, 0x1

    .line 248
    :cond_1
    # getter for: Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I
    invoke-static {}, Lcom/sec/android/app/brightness/miniDlg;->access$100()[I

    move-result-object v2

    aget v2, v2, v0

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
