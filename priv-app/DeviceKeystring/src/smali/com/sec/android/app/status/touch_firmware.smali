.class public Lcom/sec/android/app/status/touch_firmware;
.super Landroid/app/Activity;
.source "touch_firmware.java"


# instance fields
.field isSupportTSK:Z

.field private mBluetoothVersion:Ljava/lang/String;

.field private mBluetoothVersionStrView:Landroid/widget/TextView;

.field private mBluetoothVersionView:Landroid/widget/TextView;

.field private mConfigTSPFwVersion:Ljava/lang/String;

.field private mConfigTSPFwVersionView:Landroid/widget/TextView;

.field private mConfig_tsp_fw_version:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mPanelVendorName:Ljava/lang/String;

.field private mPanelVendorNameView:Landroid/widget/TextView;

.field private mPanelVendorStrView:Landroid/widget/TextView;

.field private mPartTSKVersion:Ljava/lang/String;

.field private mPartTSKVersionView:Landroid/widget/TextView;

.field private mPartTSKVersion_str_View:Landroid/widget/TextView;

.field private mPartTSPVersion:Ljava/lang/String;

.field private mPartTSPVersionView:Landroid/widget/TextView;

.field private mPhoneTSKVersion:Ljava/lang/String;

.field private mPhoneTSKVersionView:Landroid/widget/TextView;

.field private mPhoneTSKVersion_str_View:Landroid/widget/TextView;

.field private mPhoneTSPVersion:Ljava/lang/String;

.field private mPhoneTSPVersionView:Landroid/widget/TextView;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mSensorHubBinFwVersion:Ljava/lang/String;

.field private mSensorHubCrashedFwUpdateButton:Landroid/widget/Button;

.field private mSensorHubFwBinVersionStringText:Landroid/widget/TextView;

.field private mSensorHubFwBinVersionText:Landroid/widget/TextView;

.field private mSensorHubFwMcuVersionStringText:Landroid/widget/TextView;

.field private mSensorHubFwMcuVersionText:Landroid/widget/TextView;

.field private mSensorHubFwUpdateButton:Landroid/widget/Button;

.field private mSensorHubMcuFwVersion:Ljava/lang/String;

.field private mTSPPinState:Ljava/lang/String;

.field private mTSPPinStateView:Landroid/widget/TextView;

.field private mTSPPinstrView:Landroid/widget/TextView;

.field private mTSPThreshold:Ljava/lang/String;

.field private mTSPThresholdView:Landroid/widget/TextView;

.field private mWacomFwUmsVersion:Ljava/lang/String;

.field private mWacomFwUmsVersionStringText:Landroid/widget/TextView;

.field private mWacomFwUmsVersionText:Landroid/widget/TextView;

.field private mWacomFwUpdateKernelButton:Landroid/widget/Button;

.field private mWacomFwUpdateUmsButton:Landroid/widget/Button;

.field private mWacomFwVersion:Ljava/lang/String;

.field private mWacomFwVersionStringText:Landroid/widget/TextView;

.field private mWacomFwVersionText:Landroid/widget/TextView;

.field private mWacomTuningVersion:Ljava/lang/String;

.field private mWacomTuningVersionStringText:Landroid/widget/TextView;

.field private mWacomTuningVersionText:Landroid/widget/TextView;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWifiHandler:Landroid/os/Handler;

.field private mWifiRunnable:Ljava/lang/Runnable;

.field private mWifiVersion:Ljava/lang/String;

.field private mWifiVersionRefreshButton:Landroid/widget/Button;

.field private mWifiVersionStrView:Landroid/widget/TextView;

.field private mWifiVersionView:Landroid/widget/TextView;

.field productDevice:Ljava/lang/String;

.field productModel:Ljava/lang/String;

.field progressDialog:Landroid/app/ProgressDialog;

.field private tskBtn:Landroid/widget/Button;

.field private tskInternalBtn:Landroid/widget/Button;

.field public tspUpdateResult:Ljava/lang/String;

.field public tspUpdateStatus:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 128
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->tspUpdateStatus:Ljava/lang/String;

    .line 129
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->tspUpdateResult:Ljava/lang/String;

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->productModel:Ljava/lang/String;

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->productDevice:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    .line 138
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiHandler:Landroid/os/Handler;

    .line 139
    new-instance v0, Lcom/sec/android/app/status/touch_firmware$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/touch_firmware$1;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiRunnable:Ljava/lang/Runnable;

    .line 872
    new-instance v0, Lcom/sec/android/app/status/touch_firmware$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/touch_firmware$12;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/touch_firmware;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->setWifiVersion()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/touch_firmware;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/touch_firmware;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/touch_firmware;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_firmware;->startNewThread(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_firmware;->readReturn_melfas(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/touch_firmware;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->updateText()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/touch_firmware;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_firmware;->waitUpdateComplete(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/touch_firmware;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private changeWifi(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "value"    # Z

    .prologue
    .line 1193
    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1194
    .local v0, "wifiMgr":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1195
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 1196
    invoke-virtual {v0, p2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 1197
    :cond_0
    return-void
.end method

.method private init()V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 181
    iput-object p0, p0, Lcom/sec/android/app/status/touch_firmware;->mContext:Landroid/content/Context;

    .line 182
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3, v8}, Lcom/sec/android/app/status/touch_firmware;->changeWifi(Landroid/content/Context;Z)V

    .line 184
    const-string v3, "TouchFirmware"

    const-string v4, "init"

    const-string v5, "init()"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    .line 186
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPowerManager:Landroid/os/PowerManager;

    .line 187
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPowerManager:Landroid/os/PowerManager;

    const v4, 0x3000001a

    const-string v5, "touch_firmware"

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 190
    const v3, 0x7f0a020c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 191
    .local v0, "tspBtn":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/status/touch_firmware$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/touch_firmware$2;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    const v3, 0x7f0a020d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 200
    .local v2, "tspBtn_ums":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/status/touch_firmware$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/touch_firmware$3;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const-string v3, "SUPPORT_TSP_UMS_UPDATE"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 208
    invoke-virtual {v2, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 210
    :cond_0
    const v3, 0x7f0a020b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 211
    .local v1, "tspBtn_factory":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/status/touch_firmware$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/status/touch_firmware$4;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    const-string v3, "factory"

    const-string v4, "ro.factory.factory_binary"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 220
    invoke-virtual {v1, v9}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    :cond_1
    const v3, 0x7f0a021b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateKernelButton:Landroid/widget/Button;

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateKernelButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$5;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    const v3, 0x7f0a021a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateUmsButton:Landroid/widget/Button;

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateUmsButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$6;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    const v3, 0x7f0a0212

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskBtn:Landroid/widget/Button;

    .line 239
    const v3, 0x7f0a0213

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskInternalBtn:Landroid/widget/Button;

    .line 240
    iget-boolean v3, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    if-eqz v3, :cond_3

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 242
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskBtn:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$7;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    const-string v3, "SUPPORT_TSK_FW_UPDATE_INTERNAL"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 250
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskInternalBtn:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 251
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskInternalBtn:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$8;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    :goto_0
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    const-string v4, "Now Updating... Warning!\nDo not turn off!"

    invoke-virtual {v3, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 284
    const v3, 0x7f0a0200

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSPVersionView:Landroid/widget/TextView;

    .line 285
    const v3, 0x7f0a0202

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSPVersionView:Landroid/widget/TextView;

    .line 286
    const v3, 0x7f0a0204

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPThresholdView:Landroid/widget/TextView;

    .line 287
    const v3, 0x7f0a0207

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mConfig_tsp_fw_version:Landroid/widget/TextView;

    .line 288
    const v3, 0x7f0a0208

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersionView:Landroid/widget/TextView;

    .line 289
    const v3, 0x7f0a0209

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorStrView:Landroid/widget/TextView;

    .line 290
    const v3, 0x7f0a020a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorNameView:Landroid/widget/TextView;

    .line 291
    const v3, 0x7f0a0205

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinstrView:Landroid/widget/TextView;

    .line 292
    const v3, 0x7f0a0206

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinStateView:Landroid/widget/TextView;

    .line 293
    const v3, 0x7f0a020e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion_str_View:Landroid/widget/TextView;

    .line 294
    const v3, 0x7f0a0210

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion_str_View:Landroid/widget/TextView;

    .line 295
    const v3, 0x7f0a020f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    .line 296
    const v3, 0x7f0a0211

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    .line 297
    const v3, 0x7f0a0217

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersionText:Landroid/widget/TextView;

    .line 298
    const v3, 0x7f0a0216

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersionStringText:Landroid/widget/TextView;

    .line 299
    const v3, 0x7f0a0215

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersionText:Landroid/widget/TextView;

    .line 300
    const v3, 0x7f0a0214

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersionStringText:Landroid/widget/TextView;

    .line 301
    const v3, 0x7f0a0219

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersionText:Landroid/widget/TextView;

    .line 302
    const v3, 0x7f0a0218

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersionStringText:Landroid/widget/TextView;

    .line 304
    const v3, 0x7f0a021c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwMcuVersionStringText:Landroid/widget/TextView;

    .line 305
    const v3, 0x7f0a021d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwMcuVersionText:Landroid/widget/TextView;

    .line 306
    const v3, 0x7f0a021e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwBinVersionStringText:Landroid/widget/TextView;

    .line 307
    const v3, 0x7f0a021f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwBinVersionText:Landroid/widget/TextView;

    .line 309
    const v3, 0x7f0a0222

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionStrView:Landroid/widget/TextView;

    .line 310
    const v3, 0x7f0a0224

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionView:Landroid/widget/TextView;

    .line 311
    const v3, 0x7f0a0223

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionRefreshButton:Landroid/widget/Button;

    .line 312
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionRefreshButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$9;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$9;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    const v3, 0x7f0a0225

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionStrView:Landroid/widget/TextView;

    .line 324
    const v3, 0x7f0a0226

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionView:Landroid/widget/TextView;

    .line 326
    const v3, 0x7f0a0220

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubCrashedFwUpdateButton:Landroid/widget/Button;

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubCrashedFwUpdateButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$10;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$10;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    const v3, 0x7f0a0221

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwUpdateButton:Landroid/widget/Button;

    .line 334
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwUpdateButton:Landroid/widget/Button;

    new-instance v4, Lcom/sec/android/app/status/touch_firmware$11;

    invoke-direct {v4, p0}, Lcom/sec/android/app/status/touch_firmware$11;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->updateText()V

    .line 342
    return-void

    .line 273
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskInternalBtn:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 276
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskInternalBtn:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->tskBtn:Landroid/widget/Button;

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private isSupportSensorHub()Z
    .locals 3

    .prologue
    .line 171
    const-string v0, "SENSORHUB_FIRMWARE_VERSION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "TouchFirmware"

    const-string v1, "isSupportSensorHub"

    const-string v2, "isSupportSensorHub = true"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    .line 176
    :cond_0
    const-string v0, "TouchFirmware"

    const-string v1, "isSupportSensorHub"

    const-string v2, "isSupportSensorHub = false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupportTSK()Z
    .locals 3

    .prologue
    .line 159
    const-string v0, "TSK_FIRMWARE_UPDATE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TSK_FIRMWARE_VERSION_PANEL"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TSK_FIRMWARE_VERSION_PHONE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->isExistFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const-string v0, "TouchFirmware"

    const-string v1, "isSupportTSK"

    const-string v2, "isSupportTSK = true"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    .line 166
    :cond_0
    const-string v0, "TouchFirmware"

    const-string v1, "isSupportTSK"

    const-string v2, "isSupportTSK = false"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readCmdResult(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 609
    const-string v3, "TouchFirmware"

    const-string v4, "readCmdResult"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "***readCmdResult = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const-string v2, ""

    .line 611
    .local v2, "status":Ljava/lang/String;
    const-string v1, ""

    .line 612
    .local v1, "result":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/status/touch_firmware;->writeCmd(Ljava/lang/String;)V

    .line 613
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_firmware;->readStatus()Ljava/lang/String;

    move-result-object v2

    .line 614
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_firmware;->readResult()Ljava/lang/String;

    move-result-object v1

    .line 616
    if-eqz v2, :cond_1

    const-string v3, "OK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 618
    if-eqz v1, :cond_0

    .line 619
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 620
    .local v0, "arr":[Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v1, v3, v4

    .line 632
    .end local v0    # "arr":[Ljava/lang/String;
    :goto_0
    return-object v1

    .line 622
    :cond_0
    const-string v1, "NA"

    goto :goto_0

    .line 624
    :cond_1
    if-eqz v2, :cond_2

    const-string v3, "RUNNING"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 625
    const-string v1, "running"

    goto :goto_0

    .line 626
    :cond_2
    if-eqz v2, :cond_3

    const-string v3, "FAIL"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 627
    const-string v1, "fail"

    goto :goto_0

    .line 629
    :cond_3
    const-string v1, "NA"

    goto :goto_0
.end method

.method private readPanelVendor_melfas()Ljava/lang/String;
    .locals 5

    .prologue
    .line 728
    const-string v1, "TouchFirmware"

    const-string v2, "readPanelVendor_melfas"

    const-string v3, "readPanelVendor_melfas()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    const/4 v0, 0x0

    .line 730
    .local v0, "PanelVendor":Ljava/lang/String;
    const-string v1, "get_panel_vendor"

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    const-string v1, "TouchFirmware"

    const-string v2, "readPanelVendor_melfas"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get_panel_vendor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    return-object v0
.end method

.method private readReturn(I)Ljava/lang/String;
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 493
    const/4 v1, 0x0

    .line 496
    .local v1, "s":Ljava/lang/String;
    const/4 v2, 0x1

    if-eq p1, v2, :cond_0

    const/16 v2, 0x8

    if-ne p1, v2, :cond_4

    .line 497
    :cond_0
    :try_start_0
    const-string v2, "TSK_FIRMWARE_UPDATE_STATUS"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 510
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 511
    :cond_2
    const-string v1, "nostring"

    .line 514
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 498
    :cond_4
    const/4 v2, 0x3

    if-eq p1, v2, :cond_5

    const/4 v2, 0x2

    if-ne p1, v2, :cond_6

    .line 499
    :cond_5
    :try_start_1
    const-string v2, "EPEN_FIRMWARE_UPDATE_STATUS"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 500
    :cond_6
    const/4 v2, 0x5

    if-ne p1, v2, :cond_7

    .line 501
    const-string v2, "SENSORHUB_FIRMWARE_UPDATE"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 502
    :cond_7
    const/4 v2, 0x6

    if-ne p1, v2, :cond_1

    .line 503
    const-string v2, "SENSORHUB_CRASHED_FIRMWARE_UPDATE"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 507
    const-string v1, "FAIL"

    goto :goto_0
.end method

.method private readReturn_melfas(I)Ljava/lang/String;
    .locals 6
    .param p1, "mode"    # I

    .prologue
    .line 518
    const-string v2, "TouchFirmware"

    const-string v3, "readReturn_melfas"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "readReturn("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-string v1, ""

    .line 521
    .local v1, "status":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 522
    const-string v2, "TouchFirmware"

    const-string v3, "readReturn_melfas"

    const-string v4, "readReturn_melfas(TSP)"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_firmware;->readStatus()Ljava/lang/String;

    move-result-object v1

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_firmware;->readResult()Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "result":Ljava/lang/String;
    const-string v2, "OK"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "OK"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 527
    const-string v2, "PASS"

    iput-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->tspUpdateResult:Ljava/lang/String;

    .line 532
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->tspUpdateResult:Ljava/lang/String;

    .line 535
    .end local v0    # "result":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 529
    .restart local v0    # "result":Ljava/lang/String;
    :cond_0
    const-string v2, "FAIL"

    iput-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->tspUpdateResult:Ljava/lang/String;

    goto :goto_0

    .line 535
    .end local v0    # "result":Ljava/lang/String;
    :cond_1
    const-string v2, "FAIL"

    goto :goto_1
.end method

.method private readTSPPinState()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 736
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    const-string v5, "readTSPPinState()"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const/4 v1, 0x0

    .line 738
    .local v1, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 739
    .local v0, "moduleVendor":Ljava/lang/String;
    const-string v3, "get_module_vendor"

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 740
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get_module_vendor = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 744
    .local v2, "state":[Ljava/lang/String;
    const-string v3, "OK"

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 745
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PIN state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    aget-object v1, v2, v8

    .line 755
    :goto_0
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "result= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    return-object v1

    .line 747
    :cond_0
    const-string v3, "NG"

    aget-object v4, v2, v7

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 748
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    const-string v5, "PIN state=NG"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    const-string v1, "Not supported"

    goto :goto_0

    .line 751
    :cond_1
    const-string v3, "TouchFirmware"

    const-string v4, "readTSPPinState"

    const-string v5, "PIN state=NA"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const-string v1, "NA"

    goto :goto_0
.end method

.method private readThreshold_melfas()Ljava/lang/String;
    .locals 5

    .prologue
    .line 720
    const-string v1, "TouchFirmware"

    const-string v2, "readThreshold_melfas"

    const-string v3, "readThreshold()"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const/4 v0, 0x0

    .line 722
    .local v0, "threshold":Ljava/lang/String;
    const-string v1, "get_threshold"

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 723
    const-string v1, "TouchFirmware"

    const-string v2, "readThreshold_melfas"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "threshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    return-object v0
.end method

.method private readVersion(II)Ljava/lang/String;
    .locals 13
    .param p1, "mode"    # I
    .param p2, "where"    # I

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 636
    const-string v2, ""

    .line 637
    .local v2, "readString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 640
    .local v1, "isWacomUpdate":Z
    if-ne p1, v9, :cond_5

    const/16 v5, 0x10

    if-ne p2, v5, :cond_5

    .line 641
    :try_start_0
    const-string v5, "TSK_FIRMWARE_VERSION_PHONE"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 657
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 658
    .local v3, "sensorhubFirmVer":[Ljava/lang/String;
    if-ne p1, v12, :cond_1

    .line 660
    :try_start_1
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 662
    if-ne p2, v9, :cond_a

    .line 663
    const/4 v5, 0x0

    aget-object v2, v3, v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 673
    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 674
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readString: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    :try_start_2
    const-string v5, "\t"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 677
    .local v4, "splitVersion":[Ljava/lang/String;
    array-length v5, v4

    if-lt v5, v10, :cond_2

    .line 678
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "splitVersion[0]: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "splitVersion[1]: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    if-ne p1, v11, :cond_b

    const/4 v5, 0x0

    aget-object v2, v4, v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 688
    .end local v4    # "splitVersion":[Ljava/lang/String;
    :cond_2
    :goto_2
    if-eqz v2, :cond_3

    const-string v5, ""

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 689
    :cond_3
    const-string v2, "Not supported"

    .line 690
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 642
    .end local v3    # "sensorhubFirmVer":[Ljava/lang/String;
    :cond_5
    if-ne p1, v9, :cond_6

    const/16 v5, 0x11

    if-ne p2, v5, :cond_6

    .line 643
    :try_start_3
    const-string v5, "TSK_FIRMWARE_VERSION_PANEL"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 644
    :cond_6
    if-eq p1, v11, :cond_7

    if-ne p1, v10, :cond_8

    .line 645
    :cond_7
    const/4 v1, 0x1

    .line 646
    const-string v5, "EPEN_FIRMWARE_VERSION"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 647
    :cond_8
    if-ne p1, v12, :cond_9

    .line 648
    const-string v5, "SENSORHUB_FIRMWARE_VERSION"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 649
    :cond_9
    const/16 v5, 0x20

    if-ne p1, v5, :cond_0

    .line 650
    const-string v5, "EPEN_TUNING_VERSION"

    invoke-static {v5}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v2

    goto/16 :goto_0

    .line 652
    :catch_0
    move-exception v0

    .line 653
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readVersion exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const-string v2, "0\t0"

    goto/16 :goto_0

    .line 664
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "sensorhubFirmVer":[Ljava/lang/String;
    :cond_a
    if-ne p2, v10, :cond_1

    .line 665
    const/4 v5, 0x1

    :try_start_4
    aget-object v2, v3, v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_1

    .line 667
    :catch_1
    move-exception v0

    .line 668
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sensor Hub readVersion exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 680
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v4    # "splitVersion":[Ljava/lang/String;
    :cond_b
    const/4 v5, 0x1

    :try_start_5
    aget-object v2, v4, v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_2

    .line 682
    .end local v4    # "splitVersion":[Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 683
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v5, "TouchFirmware"

    const-string v6, "readVersion"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readVersion exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method private readVersion_melfas(II)Ljava/lang/String;
    .locals 8
    .param p1, "mode"    # I
    .param p2, "where"    # I

    .prologue
    .line 694
    const-string v4, "TouchFirmware"

    const-string v5, "readVersion_melfas"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "***readVersion ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ,  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v3, ""

    .line 697
    .local v3, "result":Ljava/lang/String;
    const/4 v2, 0x0

    .line 700
    .local v2, "isWacomUpdate":Z
    const/4 v0, 0x0

    .line 702
    .local v0, "buf":Ljava/io/BufferedReader;
    if-nez p1, :cond_1

    const/16 v4, 0x10

    if-ne p2, v4, :cond_1

    .line 703
    :try_start_0
    const-string v4, "TouchFirmware"

    const-string v5, "readVersion_melfas"

    const-string v6, "***readVersion (TSP ,  PHONE)"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const-string v4, "get_fw_ver_bin"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 716
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 705
    :cond_1
    if-nez p1, :cond_2

    const/16 v4, 0x11

    if-ne p2, v4, :cond_2

    .line 706
    :try_start_1
    const-string v4, "TouchFirmware"

    const-string v5, "readVersion_melfas"

    const-string v6, "***readVersion (TSP ,  PART)"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    const-string v4, "get_fw_ver_ic"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 708
    :cond_2
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 709
    const-string v4, "TouchFirmware"

    const-string v5, "readVersion_melfas"

    const-string v6, "***readVersion (TSP ,  CONFIG)"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    const-string v4, "get_config_ver"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/touch_firmware;->readCmdResult(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto :goto_0

    .line 712
    :catch_0
    move-exception v1

    .line 713
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private readWifiVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1163
    const/4 v6, 0x0

    .line 1164
    .local v6, "fis":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 1165
    .local v1, "bis":Ljava/io/BufferedInputStream;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1166
    .local v5, "file":Ljava/io/File;
    new-instance v9, Ljava/lang/String;

    invoke-direct {v9}, Ljava/lang/String;-><init>()V

    .line 1169
    .local v9, "text":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 1170
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Not Found."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 1183
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 1184
    :cond_0
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1189
    :cond_1
    :goto_0
    return-object v10

    .line 1185
    :catch_0
    move-exception v4

    .line 1186
    .local v4, "e2":Ljava/lang/Exception;
    const-string v11, "TouchFirmware"

    const-string v12, "readWifiVersion"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1172
    .end local v4    # "e2":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1173
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .local v7, "fis":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1174
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .local v2, "bis":Ljava/io/BufferedInputStream;
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileInputStream;->available()I

    move-result v8

    .line 1175
    .local v8, "size":I
    new-array v0, v8, [B

    .line 1176
    .local v0, "array":[B
    :goto_1
    invoke-virtual {v2, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_3

    .line 1177
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    new-instance v11, Ljava/lang/String;

    invoke-direct {v11, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v9

    goto :goto_1

    .line 1183
    :cond_3
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 1184
    :cond_4
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_5
    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v6, v7

    .end local v0    # "array":[B
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "size":I
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    :cond_6
    :goto_2
    move-object v10, v9

    .line 1189
    goto :goto_0

    .line 1185
    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "array":[B
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "size":I
    :catch_1
    move-exception v4

    .line 1186
    .restart local v4    # "e2":Ljava/lang/Exception;
    const-string v10, "TouchFirmware"

    const-string v11, "readWifiVersion"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v6, v7

    .line 1188
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 1179
    .end local v0    # "array":[B
    .end local v4    # "e2":Ljava/lang/Exception;
    .end local v8    # "size":I
    :catch_2
    move-exception v3

    .line 1180
    .local v3, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v10, "TouchFirmware"

    const-string v11, "readWifiVersion"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1183
    if-eqz v1, :cond_7

    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 1184
    :cond_7
    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 1185
    :catch_3
    move-exception v4

    .line 1186
    .restart local v4    # "e2":Ljava/lang/Exception;
    const-string v10, "TouchFirmware"

    const-string v11, "readWifiVersion"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1182
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "e2":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    .line 1183
    :goto_4
    if-eqz v1, :cond_8

    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    .line 1184
    :cond_8
    if-eqz v6, :cond_9

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    .line 1187
    :cond_9
    :goto_5
    throw v10

    .line 1185
    :catch_4
    move-exception v4

    .line 1186
    .restart local v4    # "e2":Ljava/lang/Exception;
    const-string v11, "TouchFirmware"

    const-string v12, "readWifiVersion"

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1182
    .end local v4    # "e2":Ljava/lang/Exception;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v10

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v10

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 1179
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v3

    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v1    # "bis":Ljava/io/BufferedInputStream;
    .end local v6    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v3

    move-object v1, v2

    .end local v2    # "bis":Ljava/io/BufferedInputStream;
    .restart local v1    # "bis":Ljava/io/BufferedInputStream;
    move-object v6, v7

    .end local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "fis":Ljava/io/FileInputStream;
    goto :goto_3
.end method

.method private setWifiVersion()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1148
    const-string v2, "/data/.wifiver.info"

    .line 1149
    .local v2, "fileName_wifiver":Ljava/lang/String;
    const-string v1, "/data/.cid.info"

    .line 1150
    .local v1, "fileName_cid":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1152
    .local v0, "file2":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v2}, Lcom/sec/android/app/status/touch_firmware;->readWifiVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersion:Ljava/lang/String;

    .line 1153
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1154
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->readWifiVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersion:Ljava/lang/String;

    .line 1156
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionStrView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1157
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1158
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionView:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1159
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersionView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1160
    return-void
.end method

.method private startNewThread(I)Z
    .locals 7
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 1038
    const/4 v1, 0x0

    .line 1040
    .local v1, "thread":Ljava/lang/Thread;
    if-nez p1, :cond_2

    .line 1047
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$13;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1128
    .restart local v1    # "thread":Ljava/lang/Thread;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 1129
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_1
    move v2, v3

    .line 1132
    :goto_1
    return v2

    .line 1052
    :cond_2
    const/4 v4, 0x4

    if-ne p1, v4, :cond_3

    .line 1053
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$14;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto :goto_0

    .line 1058
    :cond_3
    const/4 v4, 0x7

    if-ne p1, v4, :cond_4

    .line 1059
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$15;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto :goto_0

    .line 1064
    :cond_4
    if-ne p1, v3, :cond_9

    .line 1065
    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    if-nez v4, :cond_6

    .line 1066
    :cond_5
    const-string v0, "firmware\'s value is null"

    .line 1067
    .local v0, "s":Ljava/lang/String;
    const-string v4, "TouchFirmware"

    const-string v5, "startNewThread"

    const-string v6, "firmware\'s value is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    const-string v4, "firmware\'s value is null"

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1072
    .end local v0    # "s":Ljava/lang/String;
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1073
    const-string v0, "No want to update. they are have same firmware version."

    .line 1074
    .restart local v0    # "s":Ljava/lang/String;
    const-string v4, "TouchFirmware"

    const-string v5, "startNewThread"

    const-string v6, "No want to update. they are have same firmware version."

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    const-string v4, "No want to update. they are have same firmware version."

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1078
    .end local v0    # "s":Ljava/lang/String;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_8

    .line 1079
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$16;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto :goto_0

    .line 1085
    :cond_8
    const-string v0, "IC\'s F/W is higher than Bin\'s F/W"

    .line 1086
    .restart local v0    # "s":Ljava/lang/String;
    const-string v4, "TouchFirmware"

    const-string v5, "startNewThread"

    const-string v6, "IC\'s F/W is higher than Bin\'s F/W"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    const-string v4, "IC\'s F/W is higher than Bin\'s F/W"

    invoke-static {p0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1090
    .end local v0    # "s":Ljava/lang/String;
    :cond_9
    const/16 v2, 0x8

    if-ne p1, v2, :cond_a

    .line 1091
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$17;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto/16 :goto_0

    .line 1096
    :cond_a
    const/4 v2, 0x2

    if-ne p1, v2, :cond_b

    .line 1097
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$18;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$18;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto/16 :goto_0

    .line 1102
    :cond_b
    const/4 v2, 0x3

    if-ne p1, v2, :cond_c

    .line 1103
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$19;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$19;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto/16 :goto_0

    .line 1108
    :cond_c
    const/4 v2, 0x5

    if-ne p1, v2, :cond_d

    .line 1115
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$20;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto/16 :goto_0

    .line 1120
    :cond_d
    const/4 v2, 0x6

    if-ne p1, v2, :cond_0

    .line 1121
    new-instance v1, Ljava/lang/Thread;

    .end local v1    # "thread":Ljava/lang/Thread;
    new-instance v2, Lcom/sec/android/app/status/touch_firmware$21;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/touch_firmware$21;-><init>(Lcom/sec/android/app/status/touch_firmware;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .restart local v1    # "thread":Ljava/lang/Thread;
    goto/16 :goto_0
.end method

.method private updateBluetoothText()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1201
    new-instance v2, Ljava/io/File;

    const-string v3, "/vendor/firmware"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1202
    .local v2, "patchramPath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 1204
    .local v0, "fl":[Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1206
    const/4 v1, 0x0

    .local v1, "i":I
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 1208
    const-string v3, "persist.bluetooth_fw_ver"

    const-string v4, "Error"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersion:Ljava/lang/String;

    .line 1210
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionStrView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1211
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1212
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionView:Landroid/widget/TextView;

    new-instance v4, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v4}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1213
    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersionView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/status/touch_firmware;->mBluetoothVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1218
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method private updateText()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/16 v7, 0x10

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 345
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    const-string v2, "updateText()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    invoke-direct {p0, v5, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion_melfas(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSPVersion:Ljava/lang/String;

    .line 348
    const/16 v0, 0x11

    invoke-direct {p0, v5, v0}, Lcom/sec/android/app/status/touch_firmware;->readVersion_melfas(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSPVersion:Ljava/lang/String;

    .line 350
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->readThreshold_melfas()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPThreshold:Ljava/lang/String;

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->productModel:Ljava/lang/String;

    const-string v1, "gt-p31"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->readPanelVendor_melfas()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorName:Ljava/lang/String;

    .line 355
    :cond_0
    const-string v0, "SUPPORT_EPEN"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    const/4 v0, 0x2

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersion:Ljava/lang/String;

    .line 357
    const/4 v0, 0x3

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersion:Ljava/lang/String;

    .line 358
    const/16 v0, 0x20

    invoke-direct {p0, v0, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersion:Ljava/lang/String;

    .line 361
    :cond_1
    invoke-direct {p0, v5, v5}, Lcom/sec/android/app/status/touch_firmware;->readVersion_melfas(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersion:Ljava/lang/String;

    .line 362
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "================="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersion:Ljava/lang/String;

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "IS_TSP_SUPPORT_CONFIG_VERSION"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 366
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mConfig_tsp_fw_version:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    :goto_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    if-eqz v0, :cond_3

    .line 373
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    .line 374
    const/16 v0, 0x11

    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    .line 377
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSPVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSPVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSPVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSPVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPThresholdView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPThreshold:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->productModel:Ljava/lang/String;

    const-string v1, "gt-p31"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 382
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    const-string v2, "gt-p31XX"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 393
    :goto_1
    const-string v0, "SUPPORT_TSP_PIN_STATE"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 394
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    const-string v2, "TSP PIN State read"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->readTSPPinState()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinState:Ljava/lang/String;

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinStateView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinState:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    :goto_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    if-eqz v0, :cond_9

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 410
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    .line 411
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPhoneTSKVersion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const/16 v0, 0x11

    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    .line 413
    const-string v0, "TouchFirmware"

    const-string v1, "updateText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mPartTSKVersion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    :goto_3
    const-string v0, "SUPPORT_EPEN"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersionText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersionText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateKernelButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUpdateUmsButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersionStringText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersionStringText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersionText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersionStringText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 448
    :goto_4
    iget-boolean v0, p0, Lcom/sec/android/app/status/touch_firmware;->isSupportTSK:Z

    if-eqz v0, :cond_4

    .line 449
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    .line 450
    const/16 v0, 0x11

    invoke-direct {p0, v6, v0}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 455
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->isSupportSensorHub()Z

    move-result v0

    if-nez v0, :cond_b

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwMcuVersionStringText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwMcuVersionText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwBinVersionStringText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwBinVersionText:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubCrashedFwUpdateButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwUpdateButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 472
    :cond_5
    :goto_5
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->updateWifiText()V

    .line 473
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->updateBluetoothText()V

    .line 474
    return-void

    .line 369
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mConfigTSPFwVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 385
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorStrView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPanelVendorNameView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 398
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinstrView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mTSPPinStateView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 417
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPhoneTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersion_str_View:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mPartTSKVersionView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->tskBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_3

    .line 440
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersionText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersionText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomFwUmsVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersionText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWacomTuningVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 463
    :cond_b
    const/4 v0, 0x5

    invoke-direct {p0, v0, v6}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubMcuFwVersion:Ljava/lang/String;

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwMcuVersionText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubMcuFwVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/touch_firmware;->readVersion(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubBinFwVersion:Ljava/lang/String;

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubFwBinVersionText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubBinFwVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    const-string v0, "user"

    const-string v1, "ro.build.type"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mSensorHubCrashedFwUpdateButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method private updateWifiText()V
    .locals 6

    .prologue
    .line 1136
    const-string v1, "/data/.wifiver.info"

    .line 1137
    .local v1, "fileName_wifiver":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1139
    .local v0, "file1":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1140
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->setWifiVersion()V

    .line 1145
    :goto_0
    return-void

    .line 1142
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/touch_firmware;->changeWifi(Landroid/content/Context;Z)V

    .line 1143
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/status/touch_firmware;->mWifiRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private waitUpdateComplete(I)V
    .locals 6
    .param p1, "mode"    # I

    .prologue
    const-wide/16 v4, 0x1388

    .line 477
    const-string v0, "TouchFirmware"

    const-string v1, "waitUpdateComplete"

    const-string v2, "waitUpdateComplete()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    if-nez p1, :cond_1

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xaf2

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 481
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xaf3

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 483
    :cond_2
    const/16 v0, 0x8

    if-ne p1, v0, :cond_3

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xaf9

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 485
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xaf4

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 487
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xaf5

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 148
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 149
    const v0, 0x7f030068

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_firmware;->setContentView(I)V

    .line 151
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_firmware;->init()V

    .line 156
    return-void
.end method

.method public readResult()Ljava/lang/String;
    .locals 8

    .prologue
    .line 581
    const-string v4, "TouchFirmware"

    const-string v5, "readResult"

    const-string v6, "***readResult ()"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const/4 v3, 0x0

    .line 583
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 586
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_result"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 588
    const-string v4, "TouchFirmware"

    const-string v5, "readResult"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 594
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 601
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    if-eqz v3, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 602
    :cond_0
    const-string v3, "nostring"

    .line 605
    :cond_1
    return-object v3

    .line 595
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 597
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 599
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 589
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 591
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 594
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 595
    :catch_2
    move-exception v2

    .line 597
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 593
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 594
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 598
    :goto_3
    throw v4

    .line 595
    :catch_3
    move-exception v2

    .line 597
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 593
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 589
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public readStatus()Ljava/lang/String;
    .locals 8

    .prologue
    .line 559
    const-string v4, "TouchFirmware"

    const-string v5, "readStatus"

    const-string v6, "***readStatus ()"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const/4 v3, 0x0

    .line 561
    .local v3, "status":Ljava/lang/String;
    const/4 v0, 0x0

    .line 564
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    const-string v5, "/sys/class/sec/tsp/cmd_status"

    invoke-direct {v4, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 566
    const-string v4, "TouchFirmware"

    const-string v5, "readStatus"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "status = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 571
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 577
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    return-object v3

    .line 572
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 573
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 575
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 567
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 568
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 571
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 572
    :catch_2
    move-exception v2

    .line 573
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 570
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 571
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 574
    :goto_3
    throw v4

    .line 572
    :catch_3
    move-exception v2

    .line 573
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 570
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 567
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method updateFirmware(I)V
    .locals 10
    .param p1, "mode"    # I

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x5

    .line 760
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 761
    .local v0, "msg":Landroid/os/Message;
    const-string v2, "Thread Started"

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 762
    iput v5, v0, Landroid/os/Message;->what:I

    .line 763
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 764
    const/4 v1, 0x1

    .line 766
    .local v1, "needToCheckStatus_INTERNAL_TSK":Z
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_0

    .line 767
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "<harris> Wakelock acquired"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const-string v2, "Wakelock acquired"

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 769
    iput v5, v0, Landroid/os/Message;->what:I

    .line 770
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 771
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 774
    :cond_0
    const-string v2, "Try Start()"

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 775
    iput v5, v0, Landroid/os/Message;->what:I

    .line 776
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 777
    const-string v2, "Try Completed()"

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 778
    iput v5, v0, Landroid/os/Message;->what:I

    .line 779
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 781
    if-ne p1, v6, :cond_3

    .line 782
    const-string v2, "TSK_FIRMWARE_UPDATE"

    const-string v3, "S"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 805
    :cond_1
    :goto_0
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "Update button clicked"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const-string v2, "Thread Notified DelayedMessage"

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 807
    iput v5, v0, Landroid/os/Message;->what:I

    .line 808
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 810
    if-ne p1, v6, :cond_7

    .line 811
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf3

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 824
    :cond_2
    :goto_1
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "Thread Notified Message"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    return-void

    .line 783
    :cond_3
    if-ne p1, v9, :cond_5

    .line 784
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "Update button clicked1"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    const-string v2, "TSK_FIRMWARE_UPDATE"

    const-string v3, "i"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 786
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "Update button clicked1"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 788
    :cond_4
    const-string v2, "TouchFirmware"

    const-string v3, "updateFirmware"

    const-string v4, "TSK Internal firmware update Failed"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xafa

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 790
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V

    .line 791
    const/4 v1, 0x0

    goto :goto_0

    .line 794
    :cond_5
    if-ne p1, v7, :cond_6

    .line 795
    const-string v2, "SUPPORT_EPEN"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 796
    const-string v2, "EPEN_FIRMWARE_UPDATE"

    const-string v3, "K"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 798
    :cond_6
    if-ne p1, v8, :cond_1

    .line 799
    const-string v2, "SUPPORT_EPEN"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 800
    const-string v2, "EPEN_FIRMWARE_UPDATE"

    const-string v3, "I"

    invoke-static {v2, v3}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 812
    :cond_7
    if-ne p1, v9, :cond_8

    if-eqz v1, :cond_8

    .line 813
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf9

    const-wide/16 v4, 0x3a98

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 814
    :cond_8
    if-ne p1, v7, :cond_9

    .line 815
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf4

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 816
    :cond_9
    if-ne p1, v8, :cond_a

    .line 817
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf5

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 818
    :cond_a
    if-ne p1, v5, :cond_b

    .line 819
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf6

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 820
    :cond_b
    const/4 v2, 0x6

    if-ne p1, v2, :cond_2

    .line 821
    iget-object v2, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xaf7

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1
.end method

.method updateFirmware_melfas(I)V
    .locals 8
    .param p1, "mode"    # I

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x4

    const/4 v5, 0x5

    .line 828
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "***updateFirmware ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 830
    .local v0, "msg":Landroid/os/Message;
    const-string v1, "Thread Started"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 831
    iput v5, v0, Landroid/os/Message;->what:I

    .line 832
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 834
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 835
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "<harris> Wakelock acquired"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    const-string v1, "Wakelock acquired"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 837
    iput v5, v0, Landroid/os/Message;->what:I

    .line 838
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 839
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 842
    :cond_0
    const-string v1, "Try Start()"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 843
    iput v5, v0, Landroid/os/Message;->what:I

    .line 844
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 845
    const-string v1, "Try Completed()"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 846
    iput v5, v0, Landroid/os/Message;->what:I

    .line 847
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 849
    if-nez p1, :cond_4

    .line 850
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "***updateFirmware (TSP)"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    const-string v1, "fw_update,0"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->writeCmd(Ljava/lang/String;)V

    .line 860
    :cond_1
    :goto_0
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "Update button clicked"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    const-string v1, "Thread Notified DelayedMessage"

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 862
    iput v5, v0, Landroid/os/Message;->what:I

    .line 863
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 865
    if-eqz p1, :cond_2

    if-eq p1, v6, :cond_2

    if-ne p1, v7, :cond_3

    .line 866
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xaf2

    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 869
    :cond_3
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "Thread Notified Message"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    return-void

    .line 852
    :cond_4
    if-ne p1, v6, :cond_5

    .line 853
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "***updateFirmware (TSP_UMS)"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v1, "fw_update,1"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->writeCmd(Ljava/lang/String;)V

    goto :goto_0

    .line 855
    :cond_5
    if-ne p1, v7, :cond_1

    .line 856
    const-string v1, "TouchFirmware"

    const-string v2, "updateFirmware_melfas"

    const-string v3, "***updateFirmware (TSP_FACTORY)"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    const-string v1, "fw_update,2"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/touch_firmware;->writeCmd(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeCmd(Ljava/lang/String;)V
    .locals 7
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 539
    const-string v3, "TouchFirmware"

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "writeCmd = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const/4 v0, 0x0

    .line 543
    .local v0, "bw":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/sys/class/sec/tsp/cmd"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .local v1, "bw":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 545
    const-string v3, "TouchFirmware"

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+++++++++++++++++++++++ write("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 551
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 556
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    :goto_0
    return-void

    .line 552
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v2

    .line 553
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 555
    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 546
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 547
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string v3, "TouchFirmware"

    const-string v4, "writeCmd"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "+++++++++++++++++++++++"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 551
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 552
    :catch_2
    move-exception v2

    .line 553
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 550
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 551
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 554
    :goto_3
    throw v3

    .line 552
    :catch_3
    move-exception v2

    .line 553
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 550
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 546
    .end local v0    # "bw":Ljava/io/BufferedWriter;
    .restart local v1    # "bw":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "bw":Ljava/io/BufferedWriter;
    .restart local v0    # "bw":Ljava/io/BufferedWriter;
    goto :goto_1
.end method
