.class final enum Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
.super Ljava/lang/Enum;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "KEY_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum HARD:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum NONE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum POLL_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum POLL_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum TOUCH:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum TOUCH_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

.field public static final enum TOUCH_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1654
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "TOUCH"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1655
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "HARD"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->HARD:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1656
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "TOUCH_SIDE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1657
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "POLL_SIDE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1658
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "TOUCH_SIDE_MULTIVERSE"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1659
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "POLL_SIDE_MULTIVERSE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1660
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    const-string v1, "NONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->NONE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1653
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->HARD:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->NONE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->$VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1653
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getType(Ljava/lang/String;)Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 1663
    const-string v0, "TOUCH"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1664
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 1676
    :goto_0
    return-object v0

    .line 1665
    :cond_0
    const-string v0, "HARD"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1666
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->HARD:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0

    .line 1667
    :cond_1
    const-string v0, "TOUCH_SIDE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1668
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0

    .line 1669
    :cond_2
    const-string v0, "POLL_SIDE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1670
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0

    .line 1671
    :cond_3
    const-string v0, "TOUCH_SIDE_MULTIVERSE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1672
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0

    .line 1673
    :cond_4
    const-string v0, "POLL_SIDE_MULTIVERSE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1674
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->POLL_SIDE_MULTIVERSE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0

    .line 1676
    :cond_5
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->NONE:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1653
    const-class v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .locals 1

    .prologue
    .line 1653
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->$VALUES:[Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    return-object v0
.end method
