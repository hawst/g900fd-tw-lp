.class public Lcom/sec/android/app/status/TspTestMode;
.super Landroid/app/ListActivity;
.source "TspTestMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TspTestMode$1;,
        Lcom/sec/android/app/status/TspTestMode$TspTest;
    }
.end annotation


# static fields
.field private static CLASS_NAME:Ljava/lang/String;


# instance fields
.field private mTestList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/status/TspTestMode$TspTest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "TspTestMode"

    sput-object v0, Lcom/sec/android/app/status/TspTestMode;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspTestMode;->mTestList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 52
    .local v4, "titleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/android/app/status/TspTestMode$TspTest;->values()[Lcom/sec/android/app/status/TspTestMode$TspTest;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/status/TspTestMode$TspTest;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 53
    .local v3, "t":Lcom/sec/android/app/status/TspTestMode$TspTest;
    invoke-virtual {v3}, Lcom/sec/android/app/status/TspTestMode$TspTest;->isEnable()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    iget-object v5, p0, Lcom/sec/android/app/status/TspTestMode;->mTestList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-virtual {v3}, Lcom/sec/android/app/status/TspTestMode$TspTest;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    .end local v3    # "t":Lcom/sec/android/app/status/TspTestMode$TspTest;
    :cond_1
    new-instance v5, Landroid/widget/ArrayAdapter;

    const v6, 0x1090003

    invoke-direct {v5, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/status/TspTestMode;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 66
    sget-object v2, Lcom/sec/android/app/status/TspTestMode$1;->$SwitchMap$com$sec$android$app$status$TspTestMode$TspTest:[I

    iget-object v1, p0, Lcom/sec/android/app/status/TspTestMode;->mTestList:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/TspTestMode$TspTest;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspTestMode$TspTest;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 93
    :goto_0
    return-void

    .line 68
    :pswitch_0
    new-instance v1, Lcom/sec/android/app/status/TspDataMeasure;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TspDataMeasure;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/TspDataMeasure;->startTspdataTest(Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;)V

    goto :goto_0

    .line 72
    :pswitch_1
    new-instance v1, Lcom/sec/android/app/status/TspDataMeasure;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TspDataMeasure;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_HOVER:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/TspDataMeasure;->startTspdataTest(Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;)V

    goto :goto_0

    .line 77
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/TspHoverAccuracyPoint;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspTestMode;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 83
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/TspTestMode;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 88
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_4
    new-instance v1, Lcom/sec/android/app/status/TspDataMeasure;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TspDataMeasure;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->TSP_SIDE_TOUCH:Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/TspDataMeasure;->startTspdataTest(Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;)V

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/status/TspTestMode;->setKeepScreenOn(Landroid/app/Activity;Z)V

    .line 98
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 99
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 104
    const/4 v0, 0x1

    invoke-virtual {p0, p0, v0}, Lcom/sec/android/app/status/TspTestMode;->setKeepScreenOn(Landroid/app/Activity;Z)V

    .line 105
    return-void
.end method

.method public setKeepScreenOn(Landroid/app/Activity;Z)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "keepScreenOn"    # Z

    .prologue
    const/16 v4, 0x80

    .line 109
    sget-object v0, Lcom/sec/android/app/status/TspTestMode;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "setKeepScreenOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keepScreenOn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    if-eqz p2, :cond_0

    .line 112
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    .line 117
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method
