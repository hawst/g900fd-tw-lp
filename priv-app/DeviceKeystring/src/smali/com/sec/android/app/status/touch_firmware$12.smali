.class Lcom/sec/android/app/status/touch_firmware$12;
.super Landroid/os/Handler;
.source "touch_firmware.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/touch_firmware;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/touch_firmware;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/touch_firmware;)V
    .locals 0

    .prologue
    .line 872
    iput-object p1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v9, 0xaf6

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 875
    const/4 v0, 0x0

    .line 877
    .local v0, "val":Ljava/lang/String;
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 1034
    :goto_0
    return-void

    .line 879
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const-string v2, "TSK Internal firmware update Failed"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 884
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn_melfas(I)Ljava/lang/String;
    invoke-static {v1, v6}, Lcom/sec/android/app/status/touch_firmware;->access$400(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    .line 887
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandleMessage CHK_TSP_RETURN VAL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 892
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 895
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 896
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to update : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto :goto_0

    .line 899
    :cond_1
    const-string v1, "Downloading"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 900
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->waitUpdateComplete(I)V
    invoke-static {v1, v6}, Lcom/sec/android/app/status/touch_firmware;->access$600(Lcom/sec/android/app/status/touch_firmware;I)V

    goto :goto_0

    .line 902
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 903
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 906
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update complete : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 909
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 915
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;
    invoke-static {v1, v5}, Lcom/sec/android/app/status/touch_firmware;->access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    .line 916
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandleMessage CHK_TSK_RETURN VAL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 918
    const-string v1, "Downloading"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 919
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->waitUpdateComplete(I)V
    invoke-static {v1, v5}, Lcom/sec/android/app/status/touch_firmware;->access$600(Lcom/sec/android/app/status/touch_firmware;I)V

    goto/16 :goto_0

    .line 921
    :cond_4
    const-string v1, "FAIL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 929
    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 930
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 933
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Update complete : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 938
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 923
    :cond_7
    const-string v1, "PASS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 926
    const-string v0, "Maybe"

    goto :goto_1

    .line 941
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;
    invoke-static {v1, v7}, Lcom/sec/android/app/status/touch_firmware;->access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    .line 942
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandleMessage CHK_TSK_RETURN VAL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 946
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 947
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 950
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 951
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WACOM FW Update(from Kernel) complete! : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 954
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 955
    :cond_9
    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 956
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 957
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 960
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 961
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to update WACOM FW(from Kernel): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 966
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->waitUpdateComplete(I)V
    invoke-static {v1, v7}, Lcom/sec/android/app/status/touch_firmware;->access$600(Lcom/sec/android/app/status/touch_firmware;I)V

    goto/16 :goto_0

    .line 971
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/sec/android/app/status/touch_firmware;->access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    .line 972
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandleMessage CHK_TSK_RETURN VAL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    const-string v1, "PASS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 976
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 980
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 981
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WACOM FW Update(from UMS) complete : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 984
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 985
    :cond_d
    const-string v1, "FAIL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 986
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 987
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 990
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 991
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to update WACOM FW(from UMS): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 994
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 996
    :cond_f
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->waitUpdateComplete(I)V
    invoke-static {v1, v8}, Lcom/sec/android/app/status/touch_firmware;->access$600(Lcom/sec/android/app/status/touch_firmware;I)V

    goto/16 :goto_0

    .line 1003
    :sswitch_5
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v9, :cond_14

    .line 1004
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const/4 v2, 0x5

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/touch_firmware;->access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    .line 1009
    :cond_10
    :goto_2
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HandleMessage CHK_SENSORHUB_RETURN VAL: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1012
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1013
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1016
    :cond_11
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    iget-object v1, v1, Lcom/sec/android/app/status/touch_firmware;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1018
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v9, :cond_13

    :cond_12
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xaf7

    if-ne v1, v2, :cond_15

    .line 1020
    :cond_13
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const-string v2, "SENSORHUB FW Update complete!"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1022
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 1005
    :cond_14
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0xaf7

    if-ne v1, v2, :cond_10

    .line 1006
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const/4 v2, 0x6

    # invokes: Lcom/sec/android/app/status/touch_firmware;->readReturn(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/touch_firmware;->access$700(Lcom/sec/android/app/status/touch_firmware;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1024
    :cond_15
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    const-string v2, "Failed to update SENSORHUB FW"

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1026
    iget-object v1, p0, Lcom/sec/android/app/status/touch_firmware$12;->this$0:Lcom/sec/android/app/status/touch_firmware;

    # invokes: Lcom/sec/android/app/status/touch_firmware;->updateText()V
    invoke-static {v1}, Lcom/sec/android/app/status/touch_firmware;->access$500(Lcom/sec/android/app/status/touch_firmware;)V

    goto/16 :goto_0

    .line 1031
    :sswitch_6
    const-string v1, "TouchFirmware"

    const-string v2, "handleMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handlemsg case 5: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 877
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_6
        0xaf2 -> :sswitch_1
        0xaf3 -> :sswitch_2
        0xaf4 -> :sswitch_3
        0xaf5 -> :sswitch_4
        0xaf6 -> :sswitch_5
        0xaf7 -> :sswitch_5
        0xaf9 -> :sswitch_2
        0xafa -> :sswitch_0
    .end sparse-switch
.end method
