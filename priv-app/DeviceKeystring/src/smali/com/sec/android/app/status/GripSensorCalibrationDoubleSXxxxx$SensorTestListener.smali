.class Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;
.super Ljava/lang/Object;
.source "GripSensorCalibrationDoubleSXxxxx.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V
    .locals 0

    .prologue
    .line 651
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;
    .param p2, "x1"    # Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$1;

    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationDoubleSXxxxx;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 653
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v4, 0x0

    .line 655
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 657
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    :goto_0
    return-void

    .line 660
    :cond_0
    const-string v0, "GripSensorCalibrationDoubleSXxxxx"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
