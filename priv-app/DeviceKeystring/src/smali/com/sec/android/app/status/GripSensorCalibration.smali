.class public Lcom/sec/android/app/status/GripSensorCalibration;
.super Landroid/app/Activity;
.source "GripSensorCalibration.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final CALDATA_SYSFS_PATH:Ljava/lang/String;

.field private final CALIBRATION:B

.field private final CALIBRATION_ERASE:B

.field private final CAL_SYSFS_PATH:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH:Ljava/lang/String;

.field private final GRIP_OFF:B

.field private final GRIP_ON:B

.field private final ONOFF_SYSFS_PATH:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

.field private final RESET_SYSFS_PATH:Ljava/lang/String;

.field private isTimerFinish:Z

.field private isgrip_On:Z

.field private ispass:Z

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mButton_Exit:Landroid/widget/Button;

.field private mButton_Next:Landroid/widget/Button;

.field private mCaldata_data:Ljava/lang/String;

.field private mCrAvg_data:Ljava/lang/String;

.field private mCsAvg_data:Ljava/lang/String;

.field private mCspercent_data:Ljava/lang/String;

.field private mIsCalSuccess:Z

.field private mProxpercent_data:Ljava/lang/String;

.field private mText_Caldata:Landroid/widget/TextView;

.field private mText_RAW_Count:Landroid/widget/TextView;

.field private mText_RAW_Count_crcount:Landroid/widget/TextView;

.field private mText_SPEC:Landroid/widget/TextView;

.field private mTimerHandler:Landroid/os/Handler;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x31

    const/16 v0, 0x30

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    iput-byte v3, p0, Lcom/sec/android/app/status/GripSensorCalibration;->CALIBRATION:B

    .line 37
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->CALIBRATION_ERASE:B

    .line 39
    iput-byte v3, p0, Lcom/sec/android/app/status/GripSensorCalibration;->GRIP_ON:B

    .line 40
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->GRIP_OFF:B

    .line 42
    const-string v0, "/sys/class/sensors/grip_sensor/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->CSPERSENT_SYSFS_PATH:Ljava/lang/String;

    .line 43
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

    .line 44
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->CALDATA_SYSFS_PATH:Ljava/lang/String;

    .line 45
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->CAL_SYSFS_PATH:Ljava/lang/String;

    .line 46
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->ONOFF_SYSFS_PATH:Ljava/lang/String;

    .line 47
    const-string v0, "/sys/class/sensors/grip_sensor/reset"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->RESET_SYSFS_PATH:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;

    .line 55
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibration;->ispass:Z

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isgrip_On:Z

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isTimerFinish:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z

    .line 417
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibration$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibration$2;-><init>(Lcom/sec/android/app/status/GripSensorCalibration;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    return-void
.end method

.method private UpdateCSPercent()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0xa

    .line 233
    const/4 v0, 0x0

    .line 234
    .local v0, "RAWCount":[Ljava/lang/String;
    const-string v5, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v5}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    .line 235
    const/4 v3, 0x0

    .line 236
    .local v3, "StrCS_Percent":Ljava/lang/String;
    const/4 v2, 0x0

    .line 238
    .local v2, "StrCR_Count":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 239
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 243
    :goto_0
    new-array v1, v8, [I

    .line 245
    .local v1, "RAWData":[I
    if-eqz v0, :cond_2

    array-length v5, v0

    if-lt v5, v8, :cond_2

    .line 246
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v8, :cond_1

    .line 247
    aget-object v5, v0, v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v1, v4

    .line 246
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 241
    .end local v1    # "RAWData":[I
    .end local v4    # "i":I
    :cond_0
    const-string v5, "GripSensorCalibration"

    const-string v6, "UpdateCSPercent"

    const-string v7, "mCspercent_data null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 250
    .restart local v1    # "RAWData":[I
    .restart local v4    # "i":I
    :cond_1
    const/4 v5, 0x4

    aget v6, v1, v9

    aget v7, v1, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    aget v7, v1, v11

    aget v8, v1, v12

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    aput v6, v1, v5

    .line 251
    const/16 v5, 0x9

    const/4 v6, 0x5

    aget v6, v1, v6

    const/4 v7, 0x6

    aget v7, v1, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    const/4 v7, 0x7

    aget v7, v1, v7

    const/16 v8, 0x8

    aget v8, v1, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    aput v6, v1, v5

    .line 253
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CS Percent ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v9

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v10

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v11

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v12

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Min: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x4

    aget v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 255
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CR Percent ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x5

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x6

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x7

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x8

    aget-object v6, v0, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Min: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x9

    aget v6, v1, v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 261
    .end local v4    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    const-string v6, "exception"

    if-eq v5, v6, :cond_3

    .line 262
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    const/16 v6, 0x3e8

    const-wide/16 v8, 0x0

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 271
    return-void

    .line 266
    :cond_3
    const-string v5, "GripSensorCalibration"

    const-string v6, "UpdateCSPercent"

    const-string v7, "mCspercent_data null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private UpdateGripdata()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    const-string v1, "wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    const-string v1, "wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mProxpercent_data:Ljava/lang/String;

    .line 226
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCaldata_data:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->isCalSuccess(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z

    .line 230
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorCalibration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibration;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCrAvg_data:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorCalibration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibration;->isSpecIn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCspercent_data:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorCalibration;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # B

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibration;->calibration(B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorCalibration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibration;->UpdateGripdata()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorCalibration;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorCalibration;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorCalibration;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibration;->toggleButtonClickable(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorCalibration;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/status/GripSensorCalibration;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mCsAvg_data:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorCalibration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibration;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z

    return v0
.end method

.method private adjustTextSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 100
    return-void
.end method

.method private calibration(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 274
    const-string v0, "GripSensorCalibration"

    const-string v1, "calibration"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calibration data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibration;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method private gripOnOff(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 280
    const-string v0, "GripSensorCalibration"

    const-string v1, "gripOnOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gripOnOff data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibration;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method private isCalSuccess(Ljava/lang/String;)Z
    .locals 9
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x1

    .line 355
    if-nez p1, :cond_0

    .line 356
    const-string v3, "GripSensorCalibration"

    const-string v4, "isCalSuccess()"

    const-string v5, "Sysfs Value Null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :goto_0
    return v2

    .line 360
    :cond_0
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 362
    .local v1, "caldatas":[Ljava/lang/String;
    const-string v0, "0"

    .line 364
    .local v0, "NOT_CALIBRATED":Ljava/lang/String;
    aget-object v4, v1, v3

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v1, v8

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 365
    const-string v4, "GripSensorCalibration"

    const-string v5, "isCalSuccess()"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOT CALIBRATED: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v3, v1, v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v6, v1, v8

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 368
    :cond_1
    const-string v2, "GripSensorCalibration"

    const-string v4, "isCalSuccess()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CALIBRATED: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v8

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 370
    goto :goto_0
.end method

.method private isSpecIn()Z
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 375
    const-string v10, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 376
    .local v7, "proxpercent_data":Ljava/lang/String;
    const-string v10, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 377
    .local v1, "caldata_data":Ljava/lang/String;
    const-string v10, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibration;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 379
    .local v4, "cspercent_data":Ljava/lang/String;
    const-string v10, "GripSensorCalibration"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "proxpercent_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v10, "GripSensorCalibration"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "caldata_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v10, "GripSensorCalibration"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "cspercent_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 384
    .local v3, "csPercentList":[Ljava/lang/String;
    const-string v10, ","

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "caldatas":[Ljava/lang/String;
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 414
    :cond_0
    :goto_0
    return v9

    .line 398
    :cond_1
    array-length v10, v3

    const/4 v11, 0x5

    if-ge v10, v11, :cond_2

    .line 399
    const-string v10, "GripSensorCalibration"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "csList.length : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 403
    :cond_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 405
    .local v8, "thdSpec":I
    const/4 v0, 0x4

    .line 406
    .local v0, "CS_PERCENT_CNT":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    const/4 v10, 0x4

    if-ge v5, v10, :cond_4

    .line 407
    aget-object v10, v3, v5

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 408
    .local v6, "percent":I
    if-ge v6, v8, :cond_3

    .line 409
    const-string v10, "GripSensorCalibration"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "spec out : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 406
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 414
    .end local v6    # "percent":I
    :cond_4
    const/4 v9, 0x1

    goto :goto_0
.end method

.method private readSysfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    .line 285
    const/4 v2, 0x0

    .line 286
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v1, ""

    .line 288
    .local v1, "readData":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 289
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 290
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 292
    :cond_0
    if-eqz v1, :cond_1

    .line 293
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 295
    :cond_1
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sysfs Data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 301
    if-eqz v3, :cond_2

    .line 302
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v2, v3

    .line 308
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-object v1

    .line 304
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 307
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 296
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 297
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 298
    const/4 v1, 0x0

    .line 301
    if-eqz v2, :cond_3

    .line 302
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 304
    :catch_2
    move-exception v0

    .line 305
    const-string v4, "GripSensorCalibration"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 301
    :goto_2
    if-eqz v2, :cond_4

    .line 302
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 306
    :cond_4
    :goto_3
    throw v4

    .line 304
    :catch_3
    move-exception v0

    .line 305
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibration"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 300
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 296
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method private toggleButtonClickable(Z)V
    .locals 4
    .param p1, "clickable"    # Z

    .prologue
    .line 146
    const-string v0, "GripSensorCalibration"

    const-string v1, "toggleButtonClickable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Clickable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 156
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 312
    const-string v4, "GripSensorCalibration"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    const/4 v3, 0x0

    .line 314
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 316
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 318
    const/4 v3, 0x1

    .line 323
    if-eqz v2, :cond_0

    .line 324
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 330
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibration"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    return v3

    .line 326
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibration"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 329
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 319
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 320
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibration"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 323
    if-eqz v1, :cond_1

    .line 324
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 326
    :catch_2
    move-exception v0

    .line 327
    const-string v4, "GripSensorCalibration"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 322
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 323
    :goto_2
    if-eqz v1, :cond_2

    .line 324
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 328
    :cond_2
    :goto_3
    throw v4

    .line 326
    :catch_3
    move-exception v0

    .line 327
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibration"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 322
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 319
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x3ea

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 218
    :goto_0
    return-void

    .line 163
    :sswitch_0
    const-string v1, "GripSensorCalibration"

    const-string v2, "onClick"

    const-string v3, "gripsensor_cal_btn"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 182
    :sswitch_1
    const-string v1, "GripSensorCalibration"

    const-string v2, "onClick"

    const-string v3, "gripsensor_calerase_btn"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ed

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 196
    :sswitch_2
    const-string v1, "GripSensorCalibration"

    const-string v2, "onClick"

    const-string v3, "gripsensor_next_btn"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v1, "GRIPSENSOR_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "AP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    const-string v1, "GripSensorCalibration"

    const-string v2, "onClick"

    const-string v3, "gripsensor_next_btn - AP go"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208
    .local v0, "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 204
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v1, "GripSensorCalibration"

    const-string v2, "onClick"

    const-string v3, "gripsensor_next_btn - CP go"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 213
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibration;->finish()V

    goto :goto_0

    .line 160
    :sswitch_data_0
    .sparse-switch
        0x7f0a0098 -> :sswitch_3
        0x7f0a009f -> :sswitch_0
        0x7f0a00a0 -> :sswitch_1
        0x7f0a00a1 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    const-string v0, "GripSensorCalibration"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->setContentView(I)V

    .line 75
    const v0, 0x7f0a008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0a009a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_SPEC:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_Caldata:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Next:Landroid/widget/Button;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v0, 0x7f0a0098

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Exit:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mText_RAW_Count:Landroid/widget/TextView;

    const-string v1, "wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->adjustTextSize(F)V

    .line 89
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 141
    const-string v0, "GripSensorCalibration"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 143
    return-void
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 130
    const-string v0, "GripSensorCalibration"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 134
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isgrip_On:Z

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isTimerFinish:Z

    .line 136
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mIsCalSuccess:Z

    .line 137
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 106
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->gripOnOff(B)Z

    .line 108
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibration;->gripOnOff(B)Z

    .line 109
    const-string v0, "GripSensorCalibration"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "GripSensorCalibration"

    const-string v1, "onResume"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isgrip_On: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isgrip_On:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibration;->UpdateGripdata()V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 116
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibration$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibration$1;-><init>(Lcom/sec/android/app/status/GripSensorCalibration;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    .line 124
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibration;->isTimerFinish:Z

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibration;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 126
    return-void
.end method
