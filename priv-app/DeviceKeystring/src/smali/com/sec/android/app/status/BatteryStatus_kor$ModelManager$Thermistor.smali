.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;
.super Lcom/sec/android/app/status/BatteryStatus_kor$Module;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Thermistor"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 513
    const-string v0, "Thermistor"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;-><init>(Ljava/lang/String;)V

    .line 514
    return-void
.end method


# virtual methods
.method protected setModule()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 519
    .local v0, "path":Ljava/lang/String;
    new-array v1, v8, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "Temp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/temp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "RADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/batt_temp_adc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 521
    new-array v1, v6, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "TSpec"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/batt_temp_adc_spec"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 522
    new-array v1, v8, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "CT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/chg_temp"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "RADC"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/chg_temp_adc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 524
    new-array v1, v8, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "ET"

    const-string v4, "/sys/devices/platform/sec-thermistor/temperature"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "RADC"

    const-string v4, "/sys/devices/platform/sec-thermistor/temp_adc"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 526
    new-array v1, v8, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "ET"

    const-string v4, "/sys/class/sec/sec-thermistor/temperature"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "RADC"

    const-string v4, "/sys/class/sec/sec-thermistor/temp_adc"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v6

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 528
    new-array v1, v6, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "SL"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/siop_level"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 529
    return-void
.end method
