.class Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeviceController"
.end annotation


# direct methods
.method public static enableBlockPowerKey(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enable"    # Z

    .prologue
    .line 1790
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "enableBlockPowerKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1791
    invoke-static {p0, p1}, Landroid/os/FactoryTest;->setBlockingPowerKey(Landroid/content/Context;Z)Z

    .line 1792
    return-void
.end method

.method public static enableSideTouch(Z)V
    .locals 5
    .param p0, "enable"    # Z

    .prologue
    .line 1767
    const-string v0, "sidekey_enable,0"

    .line 1768
    .local v0, "command":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 1769
    const-string v0, "sidekey_enable,1"

    .line 1771
    :cond_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enableSideTouch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "command="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1772
    const-string v1, "TSP_COMMAND_CMD"

    invoke-static {v1, v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1773
    const-string v1, "TSP_COMMAND_RESULT"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    .line 1774
    return-void
.end method

.method public static onFinish(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1760
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchThdReader;->release()V

    .line 1761
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$PollerSideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->terminate()V

    move-object v0, p0

    .line 1762
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->setKeepScreenOn(Landroid/app/Activity;Z)V

    .line 1763
    invoke-static {p0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->enableBlockPowerKey(Landroid/content/Context;Z)V

    .line 1764
    return-void
.end method

.method public static onStart(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 1753
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->setKeepScreenOn(Landroid/app/Activity;Z)V

    .line 1754
    invoke-static {p0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->enableBlockPowerKey(Landroid/content/Context;Z)V

    .line 1755
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->setSystemKeyBlock(Landroid/content/ComponentName;I)V

    .line 1756
    return-void
.end method

.method public static setKeepScreenOn(Landroid/app/Activity;Z)V
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "keepScreenOn"    # Z

    .prologue
    const/16 v4, 0x80

    .line 1778
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setKeepScreenOn"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keepScreenOn: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1780
    if-eqz p1, :cond_0

    .line 1781
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    .line 1787
    :goto_0
    return-void

    .line 1784
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method
