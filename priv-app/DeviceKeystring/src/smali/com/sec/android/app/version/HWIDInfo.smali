.class public Lcom/sec/android/app/version/HWIDInfo;
.super Landroid/app/Activity;
.source "HWIDInfo.java"


# instance fields
.field hwidtv:Landroid/widget/TextView;

.field result:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 17
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/version/HWIDInfo;->result:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/version/HWIDInfo;->hwidtv:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/version/HWIDInfo;->setContentView(I)V

    .line 25
    const v0, 0x7f0a00f4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/version/HWIDInfo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/version/HWIDInfo;->hwidtv:Landroid/widget/TextView;

    .line 26
    const-string v0, "ro.revision"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/version/HWIDInfo;->result:Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/sec/android/app/version/HWIDInfo;->hwidtv:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hardware ID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/version/HWIDInfo;->result:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    return-void
.end method
