.class Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;
.super Landroid/os/Handler;
.source "GripSensorInitialWorkingTestDouble.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 1

    .prologue
    .line 325
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 326
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->count:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 328
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 370
    :goto_0
    return-void

    .line 330
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->workingGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    goto :goto_0

    .line 334
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    goto :goto_0

    .line 338
    :pswitch_2
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "thread logs"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "log"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 342
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->workingGripSensor2()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2200(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    goto :goto_0

    .line 346
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # invokes: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->releaseGripSensor2()V
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    goto :goto_0

    .line 350
    :pswitch_5
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "thread logs"

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "log"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 354
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1700(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2300(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1700(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 361
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Working"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mResultTextView2:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$4;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mMode2:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$2000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Release"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method
