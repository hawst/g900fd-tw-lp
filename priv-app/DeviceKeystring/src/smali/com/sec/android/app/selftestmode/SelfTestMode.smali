.class public Lcom/sec/android/app/selftestmode/SelfTestMode;
.super Ljava/lang/Object;
.source "SelfTestMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/selftestmode/SelfTestMode$1;,
        Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;
    }
.end annotation


# instance fields
.field private isTestOn:Z

.field private mActiveTest:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mDefaultMusicVolume:I

.field private mDefaultVoicecallVolume:I

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mVibrator:Landroid/os/Vibrator;

.field private resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->resources:Landroid/content/res/Resources;

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->resources:Landroid/content/res/Resources;

    .line 59
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mVibrator:Landroid/os/Vibrator;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mPowerManager:Landroid/os/PowerManager;

    .line 64
    iput v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultMusicVolume:I

    .line 65
    return-void
.end method

.method private accSensorTest()V
    .locals 3

    .prologue
    .line 301
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/AccelerometerSensorTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 302
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 303
    return-void
.end method

.method private bluetoothTest()V
    .locals 3

    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/BluetoothTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 286
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "bt_on_off"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 288
    return-void
.end method

.method private cameraTest()V
    .locals 3

    .prologue
    .line 267
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 268
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 270
    const-string v1, "camera_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 271
    const-string v1, "test_type"

    const-string v2, "selftest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 273
    return-void
.end method

.method private dimmingTest()V
    .locals 3

    .prologue
    .line 262
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->resources:Landroid/content/res/Resources;

    const v2, 0x10e0050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 263
    .local v0, "brightness":I
    invoke-direct {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->setBrightness(I)V

    .line 264
    return-void
.end method

.method private lightSensorTest()V
    .locals 3

    .prologue
    .line 311
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/LightSensorTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 312
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 313
    return-void
.end method

.method private melodyTest()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultVoicecallVolume:I

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 190
    const/4 v6, 0x0

    .line 193
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f04000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    .line 195
    if-nez v6, :cond_1

    .line 196
    const-string v0, "SelfTestMode"

    const-string v1, "melodyTest"

    const-string v2, "afd is NULL"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    if-eqz v6, :cond_0

    .line 211
    :try_start_1
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 212
    :catch_0
    move-exception v7

    .line 213
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "SelfTestMode"

    const-string v1, "melodyTest"

    const-string v2, "AssetFileDescriptor Close error"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 199
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 209
    if-eqz v6, :cond_2

    .line 211
    :try_start_3
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 218
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    .line 212
    :catch_1
    move-exception v7

    .line 213
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v0, "SelfTestMode"

    const-string v1, "melodyTest"

    const-string v2, "AssetFileDescriptor Close error"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 206
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v8

    .line 207
    .local v8, "ex":Ljava/io/IOException;
    :try_start_4
    const-string v0, "SelfTestMode"

    const-string v1, "melodyTest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "create failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 209
    if-eqz v6, :cond_2

    .line 211
    :try_start_5
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 212
    :catch_3
    move-exception v7

    .line 213
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v0, "SelfTestMode"

    const-string v1, "melodyTest"

    const-string v2, "AssetFileDescriptor Close error"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 209
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 211
    :try_start_6
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 214
    :cond_3
    :goto_2
    throw v0

    .line 212
    :catch_4
    move-exception v7

    .line 213
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v1, "SelfTestMode"

    const-string v2, "melodyTest"

    const-string v3, "AssetFileDescriptor Close error"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private proximitySensorTest()V
    .locals 3

    .prologue
    .line 306
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/ProximityTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 307
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 308
    return-void
.end method

.method private setBrightness(I)V
    .locals 1
    .param p1, "brightness"    # I

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 387
    :cond_0
    return-void
.end method

.method private speakerDirectionTest(Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;)V
    .locals 5
    .param p1, "direction"    # Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    .prologue
    const v4, 0x7f04000e

    const/4 v3, 0x3

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultMusicVolume:I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 242
    sget-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$1;->$SwitchMap$com$sec$android$app$selftestmode$SelfTestMode$SPEAKER_TEST_DIR:[I

    invoke-virtual {p1}, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 257
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 259
    return-void

    .line 244
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const v1, 0x7f040013

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 247
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const v1, 0x7f040014

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 250
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-static {v0, v4}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private speakerTest()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultMusicVolume:I

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const v1, 0x7f04000e

    invoke-static {v0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 235
    return-void
.end method

.method private tspDotTest()V
    .locals 3

    .prologue
    .line 291
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/TspDotModeTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 293
    return-void
.end method

.method private tspGridTest()V
    .locals 3

    .prologue
    .line 296
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/android/app/selftestmode/TspGridModeTest;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 297
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 298
    return-void
.end method

.method private vibrationTest()V
    .locals 3

    .prologue
    .line 222
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 225
    .local v0, "pattern":[J
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mVibrator:Landroid/os/Vibrator;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 226
    return-void

    .line 222
    nop

    :array_0
    .array-data 8
        0x3e8
        0x5dc
    .end array-data
.end method

.method private vtCameraTest()V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 277
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.factory.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    const-string v1, "camera_id"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 280
    const-string v1, "test_type"

    const-string v2, "selftest"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 282
    return-void
.end method


# virtual methods
.method public selfTest(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    if-eqz v0, :cond_1

    .line 69
    iget v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    if-ne v0, p1, :cond_0

    .line 70
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-lt p1, v0, :cond_1

    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-gt p1, v0, :cond_1

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOff()V

    .line 123
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOff()V

    .line 80
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    .line 81
    iput p1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    .line 83
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-ne p1, v0, :cond_2

    .line 84
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "Melody test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->melodyTest()V

    goto :goto_0

    .line 86
    :cond_2
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    if-ne p1, v0, :cond_3

    .line 87
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "Vibration_test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->vibrationTest()V

    goto :goto_0

    .line 89
    :cond_3
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    if-ne p1, v0, :cond_4

    .line 90
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "SPEAKER_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->speakerTest()V

    goto :goto_0

    .line 92
    :cond_4
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-ne p1, v0, :cond_5

    .line 93
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "DIMMING_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->dimmingTest()V

    goto :goto_0

    .line 95
    :cond_5
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    if-ne p1, v0, :cond_6

    .line 96
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "CAMERA_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->cameraTest()V

    goto :goto_0

    .line 98
    :cond_6
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    if-ne p1, v0, :cond_7

    .line 99
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "VT_CAMERA_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->vtCameraTest()V

    goto :goto_0

    .line 101
    :cond_7
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    if-ne p1, v0, :cond_8

    .line 102
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "BLUETOOTH_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->bluetoothTest()V

    goto :goto_0

    .line 104
    :cond_8
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    if-ne p1, v0, :cond_9

    .line 105
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "TSP_DOT_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->tspDotTest()V

    goto/16 :goto_0

    .line 107
    :cond_9
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    if-ne p1, v0, :cond_a

    .line 108
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "TSP_GRID_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->tspGridTest()V

    goto/16 :goto_0

    .line 110
    :cond_a
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    if-ne p1, v0, :cond_b

    .line 111
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "ACC_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->accSensorTest()V

    goto/16 :goto_0

    .line 113
    :cond_b
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    if-ne p1, v0, :cond_c

    .line 114
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "PROXIMITY_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->proximitySensorTest()V

    goto/16 :goto_0

    .line 116
    :cond_c
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    if-ne p1, v0, :cond_d

    .line 117
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "LIGHT_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->lightSensorTest()V

    goto/16 :goto_0

    .line 120
    :cond_d
    const-string v0, "SelfTestMode"

    const-string v1, "selfTest"

    const-string v2, "Unkown test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    goto/16 :goto_0
.end method

.method public selfTestForTwoSpaker(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    if-eqz v0, :cond_1

    .line 127
    iget v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    if-ne v0, p1, :cond_0

    .line 128
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-lt p1, v0, :cond_1

    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-gt p1, v0, :cond_1

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOffForTwoSpeaker()V

    .line 184
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOffForTwoSpeaker()V

    .line 138
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    .line 139
    iput p1, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    .line 141
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-ne p1, v0, :cond_2

    .line 142
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "Melody test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->melodyTest()V

    goto :goto_0

    .line 144
    :cond_2
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    if-ne p1, v0, :cond_3

    .line 145
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "Vibration_test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->vibrationTest()V

    goto :goto_0

    .line 147
    :cond_3
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    if-ne p1, v0, :cond_4

    .line 148
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "SPEAKER_LEFT_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_LEFT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    invoke-direct {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->speakerDirectionTest(Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;)V

    goto :goto_0

    .line 150
    :cond_4
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    if-ne p1, v0, :cond_5

    .line 151
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "SPEAKER_RIGHT_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    sget-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_RIGHT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    invoke-direct {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->speakerDirectionTest(Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;)V

    goto :goto_0

    .line 153
    :cond_5
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-ne p1, v0, :cond_6

    .line 154
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "DIMMING_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->dimmingTest()V

    goto :goto_0

    .line 156
    :cond_6
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    if-ne p1, v0, :cond_7

    .line 157
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "CAMERA_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->cameraTest()V

    goto :goto_0

    .line 159
    :cond_7
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    if-ne p1, v0, :cond_8

    .line 160
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "VT_CAMERA_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->vtCameraTest()V

    goto/16 :goto_0

    .line 162
    :cond_8
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    if-ne p1, v0, :cond_9

    .line 163
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "BLUETOOTH_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->bluetoothTest()V

    goto/16 :goto_0

    .line 165
    :cond_9
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    if-ne p1, v0, :cond_a

    .line 166
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "TSP_DOT_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->tspDotTest()V

    goto/16 :goto_0

    .line 168
    :cond_a
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    if-ne p1, v0, :cond_b

    .line 169
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "TSP_GRID_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->tspGridTest()V

    goto/16 :goto_0

    .line 171
    :cond_b
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    if-ne p1, v0, :cond_c

    .line 172
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "ACC_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->accSensorTest()V

    goto/16 :goto_0

    .line 174
    :cond_c
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    if-ne p1, v0, :cond_d

    .line 175
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "PROXIMITY_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->proximitySensorTest()V

    goto/16 :goto_0

    .line 177
    :cond_d
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    if-ne p1, v0, :cond_e

    .line 178
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "LIGHT_SENSOR_TEST"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->lightSensorTest()V

    goto/16 :goto_0

    .line 181
    :cond_e
    const-string v0, "SelfTestMode"

    const-string v1, "selfTestForTwoSpaker"

    const-string v2, "Unkown test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    goto/16 :goto_0
.end method

.method public testOff()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 316
    iput-boolean v5, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    .line 318
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-ne v2, v3, :cond_2

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    .line 323
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    iget v3, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultVoicecallVolume:I

    invoke-virtual {v2, v5, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 346
    :cond_1
    :goto_0
    sget v2, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    iput v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    .line 347
    return-void

    .line 325
    :cond_2
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-ne v2, v3, :cond_3

    .line 326
    const/4 v0, 0x0

    .line 329
    .local v0, "brightness":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_brightness"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 335
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->setBrightness(I)V

    goto :goto_0

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 336
    .end local v0    # "brightness":I
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_3
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    if-ne v2, v3, :cond_4

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v2}, Landroid/os/Vibrator;->cancel()V

    goto :goto_0

    .line 338
    :cond_4
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    if-ne v2, v3, :cond_1

    .line 339
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_5

    .line 340
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 343
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    iget v4, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultMusicVolume:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method public testOffForTwoSpeaker()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 350
    iput-boolean v5, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->isTestOn:Z

    .line 352
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-ne v2, v3, :cond_2

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 354
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    .line 357
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    iget v3, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultVoicecallVolume:I

    invoke-virtual {v2, v5, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 380
    :cond_1
    :goto_0
    sget v2, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    iput v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    .line 381
    return-void

    .line 359
    :cond_2
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-ne v2, v3, :cond_3

    .line 360
    const/4 v0, 0x0

    .line 363
    .local v0, "brightness":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_brightness"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 369
    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->setBrightness(I)V

    goto :goto_0

    .line 365
    :catch_0
    move-exception v1

    .line 366
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 370
    .end local v0    # "brightness":I
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_3
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    if-ne v2, v3, :cond_4

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v2}, Landroid/os/Vibrator;->cancel()V

    goto :goto_0

    .line 372
    :cond_4
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    if-eq v2, v3, :cond_5

    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mActiveTest:I

    sget v3, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    if-ne v2, v3, :cond_1

    .line 374
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->stop()V

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    iget v4, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mDefaultMusicVolume:I

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 377
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestMode;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method
