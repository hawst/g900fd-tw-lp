.class public final enum Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;
.super Ljava/lang/Enum;
.source "SelfTestMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/selftestmode/SelfTestMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SPEAKER_TEST_DIR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

.field public static final enum SPEAKER_BOTH:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

.field public static final enum SPEAKER_LEFT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

.field public static final enum SPEAKER_RIGHT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    new-instance v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    const-string v1, "SPEAKER_LEFT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_LEFT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    new-instance v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    const-string v1, "SPEAKER_RIGHT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_RIGHT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    new-instance v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    const-string v1, "SPEAKER_BOTH"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_BOTH:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    .line 51
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    sget-object v1, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_LEFT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_RIGHT:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->SPEAKER_BOTH:Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->$VALUES:[Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->$VALUES:[Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    invoke-virtual {v0}, [Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/selftestmode/SelfTestMode$SPEAKER_TEST_DIR;

    return-object v0
.end method
