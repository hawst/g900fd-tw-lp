.class Lcom/sec/android/app/status/Proximity_offset$handlersensor;
.super Ljava/util/TimerTask;
.source "Proximity_offset.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/Proximity_offset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field private data:Ljava/lang/String;

.field private mIsRunningTask:Z

.field private mValue:I

.field final synthetic this$0:Lcom/sec/android/app/status/Proximity_offset;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/Proximity_offset;)V
    .locals 1

    .prologue
    .line 363
    iput-object p1, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mIsRunningTask:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/Proximity_offset;Lcom/sec/android/app/status/Proximity_offset$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/Proximity_offset;
    .param p2, "x1"    # Lcom/sec/android/app/status/Proximity_offset$1;

    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_offset;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->resume()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/Proximity_offset$handlersensor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/Proximity_offset$handlersensor;

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->pause()V

    return-void
.end method

.method private pause()V
    .locals 1

    .prologue
    .line 440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mIsRunningTask:Z

    .line 441
    return-void
.end method

.method private readToProximitySensor()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 396
    const/4 v1, 0x0

    .line 399
    .local v1, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    const-string v4, "sys/class/sensors/proximity_sensor/state"

    invoke-direct {v3, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .local v2, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->data:Ljava/lang/String;

    .line 403
    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->data:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 404
    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->data:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->data:Ljava/lang/String;

    .line 405
    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->data:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mValue:I

    .line 408
    :cond_0
    const-string v3, "Proximityoffset"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ADC: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mValue:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mbTMD27723:Z
    invoke-static {v3}, Lcom/sec/android/app/status/Proximity_offset;->access$1000(Lcom/sec/android/app/status/Proximity_offset;)Z

    move-result v3

    if-ne v3, v7, :cond_1

    .line 412
    iget v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mValue:I

    const/16 v4, 0xc8

    if-ge v3, v4, :cond_1

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mnOffsetPassState:I
    invoke-static {v3}, Lcom/sec/android/app/status/Proximity_offset;->access$1100(Lcom/sec/android/app/status/Proximity_offset;)I

    move-result v3

    if-ne v3, v7, :cond_1

    .line 414
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mValue:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 421
    :cond_1
    if-eqz v2, :cond_4

    .line 423
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 429
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    return-void

    .line 424
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 425
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Proximityoffset"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 426
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 418
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 419
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "Proximityoffset"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ProximitySensor IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 421
    if-eqz v1, :cond_2

    .line 423
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 424
    :catch_2
    move-exception v0

    .line 425
    const-string v3, "Proximityoffset"

    const-string v4, "requestGripSensorOn"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File close exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 421
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v1, :cond_3

    .line 423
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 426
    :cond_3
    :goto_3
    throw v3

    .line 424
    :catch_3
    move-exception v0

    .line 425
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "Proximityoffset"

    const-string v5, "requestGripSensorOn"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File close exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 421
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 418
    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v1    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_4
    move-object v1, v2

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v1    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method private resume()V
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mIsRunningTask:Z

    .line 437
    return-void
.end method


# virtual methods
.method public getADC()I
    .locals 1

    .prologue
    .line 432
    iget v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mValue:I

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 369
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v3, 0x0

    .line 372
    const-string v0, "Proximityoffset"

    const-string v1, "onSensorChanged"

    const-string v2, "SensorChanged!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v3

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$500(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Working"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$500(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$600(Lcom/sec/android/app/status/Proximity_offset;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # invokes: Lcom/sec/android/app/status/Proximity_offset;->startVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$700(Lcom/sec/android/app/status/Proximity_offset;)V

    .line 385
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$500(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Release"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mWorkView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$500(Lcom/sec/android/app/status/Proximity_offset;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mBackview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$600(Lcom/sec/android/app/status/Proximity_offset;)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # invokes: Lcom/sec/android/app/status/Proximity_offset;->stopVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$800(Lcom/sec/android/app/status/Proximity_offset;)V

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 390
    invoke-direct {p0}, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->readToProximitySensor()V

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_offset$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_offset;

    # getter for: Lcom/sec/android/app/status/Proximity_offset;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_offset;->access$900(Lcom/sec/android/app/status/Proximity_offset;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 393
    :cond_0
    return-void
.end method
