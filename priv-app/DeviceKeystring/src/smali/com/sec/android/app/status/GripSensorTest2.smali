.class public Lcom/sec/android/app/status/GripSensorTest2;
.super Landroid/app/Activity;
.source "GripSensorTest2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;,
        Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;
    }
.end annotation


# static fields
.field private static IS_GRIP_COUNT:I


# instance fields
.field private final CLASS_NAME:Ljava/lang/String;

.field private Grip_status_ON_1:Z

.field private Grip_status_ON_2:Z

.field private final VIBRATE_TIME:I

.field private info1:Landroid/widget/TextView;

.field private info2:Landroid/widget/TextView;

.field private isTimerFinish:Z

.field private mBackgroudLayout1:Landroid/widget/LinearLayout;

.field private mBackgroudLayout2:Landroid/widget/LinearLayout;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

.field private mHandler:Landroid/os/Handler;

.field private mMaxRawData:I

.field private mMinRawData:I

.field private mReleaseColor:I

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorName:Ljava/lang/String;

.field private mTextGripSensorNormalThd:Landroid/widget/TextView;

.field private mTextGripSensorRawData:[Landroid/widget/TextView;

.field private mThreadGetData:Ljava/lang/Thread;

.field private mTimerHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/Vibrator;

.field private mWorkingColor:I

.field private txtgripsensor1:Landroid/widget/TextView;

.field private txtgripsensor2:Landroid/widget/TextView;

.field private working:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/status/GripSensorTest2;->IS_GRIP_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    const-string v0, "GripSensorTest2"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->CLASS_NAME:Ljava/lang/String;

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->working:Z

    .line 47
    const v0, 0xffff

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->VIBRATE_TIME:I

    .line 61
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z

    .line 64
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I

    .line 65
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    .line 70
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->isTimerFinish:Z

    .line 82
    sget-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->UNKNOWN:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    .line 84
    new-instance v0, Lcom/sec/android/app/status/GripSensorTest2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorTest2$1;-><init>(Lcom/sec/android/app/status/GripSensorTest2;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;

    .line 384
    return-void
.end method

.method private UpdateCSPercent()V
    .locals 13

    .prologue
    const/16 v12, -0x100

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/high16 v9, -0x10000

    const/4 v8, 0x0

    .line 450
    const/4 v0, 0x0

    .line 451
    .local v0, "RAWCount":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 452
    .local v4, "rawData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 453
    .local v1, "diffData":Ljava/lang/String;
    const/4 v3, 0x0

    .line 454
    .local v3, "offsetData":Ljava/lang/String;
    const-string v5, "GRIP_SENSOR_RAWDATA"

    invoke-static {v5, v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 455
    .local v2, "mCspercent_data":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 456
    const-string v5, ","

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 461
    :goto_0
    if-eqz v0, :cond_0

    .line 462
    aget-object v4, v0, v8

    .line 463
    aget-object v1, v0, v10

    .line 464
    aget-object v3, v0, v11

    .line 466
    :cond_0
    if-eqz v2, :cond_3

    const-string v5, "exception"

    if-eq v2, v5, :cond_3

    .line 467
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v5

    invoke-static {v9}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    if-ne v5, v6, :cond_2

    .line 468
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v8

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 469
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v10

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 470
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v11

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 476
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v8

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v10

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v11

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 482
    :goto_2
    return-void

    .line 458
    :cond_1
    const-string v5, "GripSensorTest2"

    const-string v6, "UpdateCSPercent"

    const-string v7, "mCspercent_data null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 472
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v8

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 473
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v10

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 474
    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    aget-object v5, v5, v11

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 480
    :cond_3
    const-string v5, "GripSensorTest2"

    const-string v6, "UpdateCSPercent"

    const-string v7, "mCspercent_data null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private UpdateThreshold()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x2

    .line 158
    const-string v2, "GRIP_SENSOR_THRESHOLD"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "threshold":Ljava/lang/String;
    const-string v2, "GripSensorTest2"

    const-string v3, "UpdateThreshold"

    invoke-static {v2, v3, v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    if-eqz v0, :cond_1

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "thresholds":[Ljava/lang/String;
    :goto_0
    array-length v2, v1

    if-le v2, v5, :cond_0

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorNormalThd:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    :cond_0
    return-void

    .line 160
    .end local v1    # "thresholds":[Ljava/lang/String;
    :cond_1
    const/4 v2, 0x3

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "none"

    aput-object v3, v1, v2

    const-string v2, "none"

    aput-object v2, v1, v4

    const-string v2, "none"

    aput-object v2, v1, v5

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorTest2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/GripSensorTest2;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorTest2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mWorkingColor:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorTest2;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorTest2;->startVibration(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/status/GripSensorTest2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_2:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/GripSensorTest2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->stopVibration()V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/GripSensorTest2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorTest2;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/status/GripSensorTest2;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorTest2;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorTest2;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorTest2;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorTest2;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/status/GripSensorTest2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorTest2;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorTest2;->Grip_status_ON_1:Z

    return p1
.end method

.method private init()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 252
    const v0, 0x7f0a0093

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->txtgripsensor1:Landroid/widget/TextView;

    .line 253
    const v0, 0x7f0a0096

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->txtgripsensor2:Landroid/widget/TextView;

    .line 254
    const v0, 0x7f0a0094

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;

    .line 255
    const v0, 0x7f0a0097

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info1:Landroid/widget/TextView;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 258
    const v0, 0x7f0a0092

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout1:Landroid/widget/LinearLayout;

    .line 259
    const v0, 0x7f0a0095

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorTest2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mWorkingColor:I

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorTest2;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mReleaseColor:I

    .line 264
    const-string v0, "GRIPSENSOR_GRIP_COUNT"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/sec/android/app/status/GripSensorTest2;->IS_GRIP_COUNT:I

    .line 266
    const-string v0, "GripSensorTest2"

    const-string v1, "init"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IS_GRIP_COUNT : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/status/GripSensorTest2;->IS_GRIP_COUNT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    sget v0, Lcom/sec/android/app/status/GripSensorTest2;->IS_GRIP_COUNT:I

    if-ne v0, v6, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mBackgroudLayout2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->txtgripsensor1:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->txtgripsensor2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->info2:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->mSupportRawData:Z

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->ADxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    if-ne v0, v1, :cond_2

    .line 277
    const-string v0, "GripSensorTest2"

    const-string v1, "init"

    const-string v2, "UI Normal threshold is supported."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 280
    const v0, 0x7f0a008a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorNormalThd:Landroid/widget/TextView;

    .line 282
    new-array v0, v8, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v4

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a008c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v6

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a008d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v7

    .line 297
    :cond_1
    :goto_0
    const-string v0, "vibrator"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    .line 298
    return-void

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    if-ne v0, v1, :cond_1

    .line 287
    const-string v0, "GripSensorTest2"

    const-string v1, "init"

    const-string v2, "SX9500 rawdata is supported."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const v0, 0x7f0a008e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 290
    new-array v0, v8, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v4

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v6

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;

    const v0, 0x7f0a0091

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorTest2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v7

    goto :goto_0
.end method

.method private startThreadGettingRawData()V
    .locals 4

    .prologue
    .line 124
    const-string v0, "GripSensorTest2"

    const-string v1, "startThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->stopThreadGettingRawData()V

    .line 126
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/GripSensorTest2$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorTest2$2;-><init>(Lcom/sec/android/app/status/GripSensorTest2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 155
    return-void
.end method

.method private startVibration(I)V
    .locals 8
    .param p1, "intensity"    # I

    .prologue
    const-wide/16 v6, 0x1e

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 305
    const-string v1, "GripSensorTest2"

    const-string v2, "startVibration"

    const-string v3, "Vibration start"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v1, 0x2

    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 310
    .local v0, "pattern":[J
    if-nez p1, :cond_1

    .line 311
    aput-wide v6, v0, v4

    .line 312
    const-wide/16 v2, 0x64

    aput-wide v2, v0, v5

    .line 318
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1, v0, v4}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 319
    return-void

    .line 313
    :cond_1
    if-ne p1, v5, :cond_0

    .line 314
    aput-wide v6, v0, v4

    .line 315
    const-wide/16 v2, 0x190

    aput-wide v2, v0, v5

    goto :goto_0

    .line 306
    :array_0
    .array-data 8
        0x1e
        0x64
    .end array-data
.end method

.method private stopThreadGettingRawData()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 168
    const-string v0, "GripSensorTest2"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 173
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 176
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    .line 177
    const-string v0, "GripSensorTest2"

    const-string v1, "stopThreadGettingRawData"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 325
    :cond_0
    const-string v0, "GripSensorTest2"

    const-string v1, "stopVibration"

    const-string v2, "Vibration stop"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 182
    const v1, 0x7f030026

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest2;->setContentView(I)V

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorTest2;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 185
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 186
    const-string v1, "grip_sensor_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorName:Ljava/lang/String;

    .line 188
    const-string v1, "GripSensorTest2"

    const-string v2, "onCreate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSensorName="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->AD7146:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v1}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 192
    const-string v1, "GripSensorTest2"

    const-string v2, "init"

    const-string v3, "UI Normal threshold is supported."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->ADxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    .line 202
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->init()V

    .line 203
    const-string v1, "GripSensorTest2"

    const-string v2, "onCreate"

    const-string v3, "onCreate"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    return-void

    .line 194
    :cond_2
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9500:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v1}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 195
    const-string v1, "GripSensorTest2"

    const-string v2, "init"

    const-string v3, "SX9500 rawdata is supported."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    goto :goto_0

    .line 197
    :cond_3
    sget-object v1, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->SX9306:Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;

    invoke-virtual {v1}, Lcom/sec/android/app/status/GripSensorCalibrationEntry$GripSensorList;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    const-string v1, "GripSensorTest2"

    const-string v2, "init"

    const-string v3, "SX9306 rawdata is supported."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensorUIType:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 248
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 249
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->stopThreadGettingRawData()V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 238
    :cond_0
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

    .line 239
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    .line 240
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorTest2;->isTimerFinish:Z

    .line 242
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 243
    const-string v0, "GripSensorTest2"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 207
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 208
    const-string v1, "GripSensorTest2"

    const-string v2, "onResume"

    const-string v3, "onResume"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    new-instance v1, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorTest2;Lcom/sec/android/app/status/GripSensorTest2$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

    .line 211
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2;->mSensorListener:Lcom/sec/android/app/status/GripSensorTest2$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 215
    .local v0, "result":Z
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->UpdateThreshold()V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorTest2;->startThreadGettingRawData()V

    .line 217
    const-string v1, "GripSensorTest2"

    const-string v2, "onResume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v1, Lcom/sec/android/app/status/GripSensorTest2$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorTest2$3;-><init>(Lcom/sec/android/app/status/GripSensorTest2;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTimerHandler:Landroid/os/Handler;

    .line 226
    iput-boolean v5, p0, Lcom/sec/android/app/status/GripSensorTest2;->isTimerFinish:Z

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorTest2;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 228
    return-void
.end method

.method public setBackgroundColor(Landroid/widget/LinearLayout;I)V
    .locals 0
    .param p1, "layout"    # Landroid/widget/LinearLayout;
    .param p2, "color"    # I

    .prologue
    .line 301
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 302
    return-void
.end method
