.class Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SideTouchSensitivityReader"
.end annotation


# static fields
.field private static LOOP_TIME:I

.field private static WAIT_TIME_FOR_OTHER_KEY:I

.field private static _instance:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;


# instance fields
.field protected _pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;",
            ">;"
        }
    .end annotation
.end field

.field protected _readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;",
            ">;"
        }
    .end annotation
.end field

.field protected _running:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1395
    const/16 v0, 0x12c

    sput v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->WAIT_TIME_FOR_OTHER_KEY:I

    .line 1396
    const/16 v0, 0x3e8

    sput v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->LOOP_TIME:I

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 1398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1391
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_running:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1392
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 1393
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 1505
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$2;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->mHandler:Landroid/os/Handler;

    .line 1399
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;
    .locals 2

    .prologue
    .line 1402
    const-class v1, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    if-nez v0, :cond_0

    .line 1403
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    invoke-direct {v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    .line 1405
    :cond_0
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_instance:Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1402
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getSensitivityFromSysfs()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 1528
    const-string v2, "run_sidekey_delta_read"

    .line 1530
    .local v2, "RUN_CMD":Ljava/lang/String;
    const-string v0, "/sys/class/sec/tsp/cmd"

    .line 1531
    .local v0, "PATH_COMMAND":Ljava/lang/String;
    const-string v1, "/sys/class/sec/tsp/cmd_result"

    .line 1533
    .local v1, "PATH_RESULT":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-direct {v3}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;-><init>()V

    .line 1535
    .local v3, "clock":Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;
    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->restart()V

    .line 1536
    invoke-static {v7}, Lcom/sec/android/app/status/TspSideTouchTest$DeviceController;->enableSideTouch(Z)V

    .line 1537
    const-string v5, "/sys/class/sec/tsp/cmd"

    const-string v6, "run_sidekey_delta_read"

    invoke-static {v5, v6}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->writeToPath(Ljava/lang/String;Ljava/lang/String;)Z

    .line 1538
    const-string v5, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v5, v7}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->readFromPath(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 1540
    .local v4, "result":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "getSensitivityFromSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Read time: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->getTime()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", value: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    return-object v4
.end method

.method private tspResultParser(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1548
    if-nez p1, :cond_0

    .line 1561
    :goto_0
    return-object v3

    .line 1552
    :cond_0
    const-string v0, ":"

    .line 1553
    .local v0, "SPLIT_CMD_RESULT":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1554
    .local v2, "split":[Ljava/lang/String;
    array-length v4, v2

    const/4 v5, 0x2

    if-ge v4, v5, :cond_1

    .line 1555
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "tspResultParser"

    const-string v6, "split.length < 2"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1558
    :cond_1
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 1560
    const-string v1, ","

    .line 1561
    .local v1, "SPLIT_EACH_RESULT":Ljava/lang/String;
    const-string v3, ","

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private waitSomeTimeForOtherKeys(II)V
    .locals 5
    .param p1, "keyCount"    # I
    .param p2, "time"    # I

    .prologue
    .line 1437
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "waitSomeTimeForOtherKeys"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "keyCount:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1439
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    .line 1440
    .local v0, "pendingQueueSize":I
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "waitSomeTimeForOtherKeys"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Enter pendingQueueSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    if-gt v0, p1, :cond_0

    .line 1444
    :try_start_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "waitSomeTimeForOtherKeys"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wait some times for other keys:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1446
    int-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1450
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    .line 1451
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "waitSomeTimeForOtherKeys"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Out pendingQueueSize:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1453
    return-void

    .line 1447
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public cancel(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;

    .prologue
    .line 1424
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cancel"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 1426
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 1427
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_CANCEL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-interface {p1, v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;->onStatusChanged(Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;)V

    .line 1428
    return-void
.end method

.method protected read()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    .line 1457
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_running:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1500
    :goto_0
    return-void

    .line 1460
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_running:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1461
    sget v7, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->WAIT_TIME_FOR_OTHER_KEY:I

    invoke-direct {p0, v8, v7}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->waitSomeTimeForOtherKeys(II)V

    .line 1464
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;

    invoke-direct {v0}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;-><init>()V

    .line 1465
    .local v0, "clock":Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;
    const/4 v1, 0x0

    .line 1469
    .local v1, "loopCount":I
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1498
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v8, "run"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "finish: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1499
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_running:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 1473
    :cond_2
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v8, "read"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "count: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 1476
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v8, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7, v8}, Ljava/util/concurrent/ConcurrentLinkedQueue;->addAll(Ljava/util/Collection;)Z

    .line 1477
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x3e8

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 1478
    .local v2, "msg":Landroid/os/Message;
    sget-object v7, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_READING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v7, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1479
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1481
    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->restart()V

    .line 1483
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->getSensitivityFromSysfs()Ljava/lang/String;

    move-result-object v4

    .line 1484
    .local v4, "result":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->tspResultParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 1485
    .local v5, "value":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->mHandler:Landroid/os/Handler;

    const/16 v8, 0x3e9

    invoke-virtual {v7, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 1486
    .local v3, "msgUpdate":Landroid/os/Message;
    iput-object v5, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1487
    iget-object v7, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->mHandler:Landroid/os/Handler;

    invoke-virtual {v7, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1489
    sget v7, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->LOOP_TIME:I

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->getTime()I

    move-result v8

    sub-int v6, v7, v8

    .line 1490
    .local v6, "waitTime":I
    if-lez v6, :cond_1

    .line 1492
    int-to-long v8, v6

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 1493
    :catch_0
    move-exception v7

    goto/16 :goto_1
.end method

.method public request(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;

    .prologue
    .line 1409
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "request"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 1412
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PEADING:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-interface {p1, v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;->onStatusChanged(Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;)V

    .line 1414
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$1;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;)V

    invoke-virtual {v0}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader$1;->start()V

    .line 1421
    :cond_0
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 1431
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_pendingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->_readingQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    .line 1433
    return-void
.end method
