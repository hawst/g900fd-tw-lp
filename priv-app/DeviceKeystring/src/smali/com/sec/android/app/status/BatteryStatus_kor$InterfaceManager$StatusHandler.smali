.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;
.super Landroid/os/Handler;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StatusHandler"
.end annotation


# instance fields
.field private final mIFWF:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;)V
    .locals 1
    .param p1, "parent"    # Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    .prologue
    .line 661
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 662
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->mIFWF:Ljava/lang/ref/WeakReference;

    .line 663
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 667
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$StatusHandler;->mIFWF:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    .line 668
    .local v0, "ifm":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    if-eqz v0, :cond_0

    .line 669
    invoke-virtual {v0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->handleMessage(Landroid/os/Message;)V

    .line 671
    :cond_0
    return-void
.end method
