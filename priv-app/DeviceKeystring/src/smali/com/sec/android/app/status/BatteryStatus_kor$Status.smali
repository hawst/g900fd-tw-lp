.class Lcom/sec/android/app/status/BatteryStatus_kor$Status;
.super Ljava/io/File;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Status"
.end annotation


# instance fields
.field private mMessage:Ljava/lang/String;

.field private mMode:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mTitle:Ljava/lang/String;

    .line 69
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMessage:Ljava/lang/String;

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMode:I

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "mode"    # I

    .prologue
    .line 73
    const-string v0, ""

    invoke-direct {p0, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 74
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mTitle:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMessage:Ljava/lang/String;

    .line 76
    iput p3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMode:I

    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "mode"    # I

    .prologue
    .line 79
    invoke-direct {p0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mTitle:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMessage:Ljava/lang/String;

    .line 82
    iput p4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMode:I

    .line 83
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mMode:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public isFile()Z
    .locals 2

    .prologue
    .line 90
    const-string v0, ""

    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
