.class public Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
.super Landroid/app/Activity;
.source "GripSensorCalibrationASPxx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;
    }
.end annotation


# instance fields
.field private final CALDATA_SYSFS_PATH:Ljava/lang/String;

.field private final CALIBRATION:B

.field private final CALIBRATION_ERASE:B

.field private final CAL_SYSFS_PATH:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH:Ljava/lang/String;

.field private final GRIP_OFF:B

.field private final GRIP_ON:B

.field private final GRIP_SENSOR_ERASECAL:Ljava/lang/String;

.field private final ONOFF_SYSFS_PATH:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

.field private final RESET_SYSFS_PATH:Ljava/lang/String;

.field private finishRefresh:Z

.field private isTimerFinish:Z

.field private isgrip_On:Z

.field private ispass:Z

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mButton_Skip:Landroid/widget/Button;

.field private mButton_onoff:Landroid/widget/Button;

.field private mButton_reset:Landroid/widget/Button;

.field private mCaldata_data:Ljava/lang/String;

.field private mCspercent_data:Ljava/lang/String;

.field protected mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mIsCalSuccess:Z

.field mProductModel:Ljava/lang/String;

.field private mProxpercent_data:Ljava/lang/String;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSpecOutNumber:Ljava/lang/String;

.field private mText_Caldata:Landroid/widget/TextView;

.field private mText_RAW_Count:Landroid/widget/TextView;

.field private mText_RAW_Count_crcount:Landroid/widget/TextView;

.field private mText_Result:Landroid/widget/TextView;

.field private mText_ResultDetail:Landroid/widget/TextView;

.field private mText_SPEC:Landroid/widget/TextView;

.field private mTimerHandler:Landroid/os/Handler;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0x31

    const/16 v0, 0x30

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 50
    iput-byte v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->CALIBRATION:B

    .line 51
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->CALIBRATION_ERASE:B

    .line 53
    iput-byte v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->GRIP_ON:B

    .line 54
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->GRIP_OFF:B

    .line 56
    const-string v0, "/sys/class/sensors/grip_sensor/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->CSPERSENT_SYSFS_PATH:Ljava/lang/String;

    .line 57
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

    .line 58
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->CALDATA_SYSFS_PATH:Ljava/lang/String;

    .line 59
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->CAL_SYSFS_PATH:Ljava/lang/String;

    .line 60
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->ONOFF_SYSFS_PATH:Ljava/lang/String;

    .line 61
    const-string v0, "/sys/class/sensors/grip_sensor/reset"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->RESET_SYSFS_PATH:Ljava/lang/String;

    .line 62
    const-string v0, "/sys/class/sensors/grip_sensor/erase_cal"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->GRIP_SENSOR_ERASECAL:Ljava/lang/String;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    .line 65
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProxpercent_data:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->ispass:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z

    .line 80
    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z

    .line 82
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSpecOutNumber:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProductModel:Ljava/lang/String;

    .line 204
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 577
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    .line 669
    return-void
.end method

.method private UpdateCSPercent()V
    .locals 7

    .prologue
    .line 395
    const/4 v0, 0x0

    .line 396
    .local v0, "RAWCount":[Ljava/lang/String;
    const-string v3, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    .line 397
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 398
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 399
    array-length v3, v0

    const/16 v4, 0xa

    if-ge v3, v4, :cond_1

    .line 400
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "UpdateCSPercent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RAWCount.length: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :goto_0
    return-void

    .line 404
    :cond_0
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "UpdateCSPercent"

    const-string v5, "mCspercent_data null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CS Percent("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", MIN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x4

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 409
    .local v2, "StrCS_Percent":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CR Percent ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x5

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x8

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", MIN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x9

    aget-object v4, v0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 412
    .local v1, "StrCR_Count":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    const-string v4, "exception"

    if-eq v3, v4, :cond_2

    .line 413
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "   "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 417
    :cond_2
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "UpdateCSPercent"

    const-string v5, "mCspercent_data null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private UpdateGripdata()V
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 387
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProxpercent_data:Ljava/lang/String;

    .line 388
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    .line 392
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isCalSuccess()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isSpecIn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_ResultDetail:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSpecOutNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->test_start()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
    .param p1, "x1"    # B

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->calibration(B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->toggleButtonClickable(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProxpercent_data:Ljava/lang/String;

    return-object v0
.end method

.method private adjustTextSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Skip:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 144
    return-void
.end method

.method private calibration(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 422
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "calibration"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calibration data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method private gripOnOff(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 428
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "gripOnOff"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gripOnOff data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method private isCalSuccess()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 484
    const-string v4, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    .line 485
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 486
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "isCalSuccess()"

    const-string v6, "Sysfs Value Null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :goto_0
    return v3

    .line 490
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 492
    .local v2, "caldatas":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 493
    .local v1, "INDEX_CALDATA_CALIBRATED":I
    const-string v0, "1"

    .line 495
    .local v0, "CALIBRATED":Ljava/lang/String;
    const-string v4, "1"

    aget-object v5, v2, v3

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 496
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "isCalSuccess()"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOT CALIBRATED: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 500
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isCalSuccess(Ljava/lang/String;)Z
    .locals 8
    .param p1, "input"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 504
    if-nez p1, :cond_0

    .line 505
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "isCalSuccess()"

    const-string v6, "Sysfs Value Null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :goto_0
    return v3

    .line 509
    :cond_0
    const-string v4, ","

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 511
    .local v2, "caldatas":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 512
    .local v1, "INDEX_CALDATA_CALIBRATED":I
    const-string v0, "1"

    .line 514
    .local v0, "CALIBRATED":Ljava/lang/String;
    const-string v4, "1"

    aget-object v5, v2, v3

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 515
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "isCalSuccess()"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "NOT CALIBRATED: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v2, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 519
    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isSpecIn()Z
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 523
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateCSPercent()V

    .line 524
    const-string v10, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 525
    .local v7, "proxpercent_data":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    .line 526
    .local v1, "caldata_data":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;

    .line 529
    .local v4, "cspercent_data":Ljava/lang/String;
    const-string v10, "GripSensorCalibrationASPxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "proxpercent_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string v10, "GripSensorCalibrationASPxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "caldata_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    const-string v10, "GripSensorCalibrationASPxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "cspercent_data : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 534
    .local v3, "csPercentList":[Ljava/lang/String;
    const-string v10, ","

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 536
    .local v2, "caldatas":[Ljava/lang/String;
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 565
    :cond_0
    :goto_0
    return v9

    .line 548
    :cond_1
    array-length v10, v3

    const/4 v11, 0x5

    if-ge v10, v11, :cond_2

    .line 549
    const-string v10, "GripSensorCalibrationASPxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "csList.length : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 553
    :cond_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 555
    .local v8, "thdSpec":I
    const/4 v0, 0x4

    .line 556
    .local v0, "CS_PERCENT_CNT":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    const/4 v10, 0x4

    if-ge v5, v10, :cond_4

    .line 557
    aget-object v10, v3, v5

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 558
    .local v6, "percent":I
    if-ge v6, v8, :cond_3

    .line 559
    const-string v10, "GripSensorCalibrationASPxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "spec out : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSpecOutNumber:Ljava/lang/String;

    goto :goto_0

    .line 556
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 565
    .end local v6    # "percent":I
    :cond_4
    const/4 v9, 0x1

    goto :goto_0
.end method

.method private readSysfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    .line 433
    const/4 v2, 0x0

    .line 434
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v1, ""

    .line 436
    .local v1, "readData":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    if-eqz v3, :cond_0

    .line 438
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .line 440
    :cond_0
    if-eqz v1, :cond_1

    .line 441
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 443
    :cond_1
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file_path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file_path sysfs Data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 450
    if-eqz v3, :cond_2

    .line 451
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v2, v3

    .line 457
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    return-object v1

    .line 453
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 454
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 456
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 445
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 446
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 447
    const/4 v1, 0x0

    .line 450
    if-eqz v2, :cond_3

    .line 451
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 453
    :catch_2
    move-exception v0

    .line 454
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "readsysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 449
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 450
    :goto_2
    if-eqz v2, :cond_4

    .line 451
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 455
    :cond_4
    :goto_3
    throw v4

    .line 453
    :catch_3
    move-exception v0

    .line 454
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationASPxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 449
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 445
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method private registerTestReceiver()V
    .locals 4

    .prologue
    .line 226
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 227
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.factory.app.factorytest.GRIP_CONFIRM_Keystring"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 229
    const-string v1, "GripSensorCalibrationASPxx"

    const-string v2, "registerTestReceiver"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method private test_start()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 170
    const-string v1, "/sys/class/sensors/grip_sensor/onoff"

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "onOffStatus":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iput-boolean v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    const-string v2, "Off"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 178
    :goto_0
    const-string v1, "GripSensorCalibrationASPxx"

    const-string v2, "test_start"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isgrip_On: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateGripdata()V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 184
    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$1;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mTimerHandler:Landroid/os/Handler;

    .line 192
    iput-boolean v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isCalSuccess(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Skip:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 202
    return-void

    .line 175
    :cond_0
    iput-boolean v7, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 176
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    const-string v2, "On"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private toggleButtonClickable(Z)V
    .locals 4
    .param p1, "clickable"    # Z

    .prologue
    .line 279
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "toggleButtonClickable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Clickable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Skip:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 290
    return-void
.end method

.method private unregisterTestReceiver()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 235
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 236
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "unregisterTestReceiver"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 461
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const/4 v3, 0x0

    .line 463
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 465
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 466
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 467
    const/4 v3, 0x1

    .line 472
    if-eqz v2, :cond_0

    .line 473
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 479
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    return v3

    .line 475
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 478
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 468
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 469
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 472
    if-eqz v1, :cond_1

    .line 473
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 475
    :catch_2
    move-exception v0

    .line 476
    const-string v4, "GripSensorCalibrationASPxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 471
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 472
    :goto_2
    if-eqz v1, :cond_2

    .line 473
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 477
    :cond_2
    :goto_3
    throw v4

    .line 475
    :catch_3
    move-exception v0

    .line 476
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationASPxx"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 471
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 468
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x3e8

    const/16 v7, 0x3ea

    const/16 v6, 0x3e9

    const/4 v5, 0x1

    .line 294
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 297
    :sswitch_0
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "gripsensor_cal_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    const/16 v3, 0x3ec

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 313
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateGripdata()V

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 319
    :sswitch_1
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "gripsensor_calerase_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 325
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    const/16 v3, 0x3ed

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 329
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateGripdata()V

    .line 331
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 335
    :sswitch_2
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "gripsensor_skip_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v2, "GRIPSENSOR_TYPE"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "AP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/status/GripSensorTest2;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v0, "intent":Landroid/content/Intent;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 341
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/status/GripSensorTest;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_1

    .line 348
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_3
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "gripsensor_onoff_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    iget-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    if-ne v2, v5, :cond_2

    .line 350
    const/16 v2, 0x30

    invoke-direct {p0, v2}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->gripOnOff(B)Z

    move-result v1

    .line 351
    .local v1, "result":Z
    if-ne v1, v5, :cond_0

    .line 352
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    const-string v3, "On"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 354
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "grip off"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 356
    .end local v1    # "result":Z
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    if-nez v2, :cond_0

    .line 357
    const/16 v2, 0x31

    invoke-direct {p0, v2}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->gripOnOff(B)Z

    move-result v1

    .line 358
    .restart local v1    # "result":Z
    if-ne v1, v5, :cond_0

    .line 359
    iput-boolean v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    const-string v3, "Off"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 361
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "grip on"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 367
    .end local v1    # "result":Z
    :sswitch_4
    const-string v2, "GripSensorCalibrationASPxx"

    const-string v3, "onClick"

    const-string v4, "gripsensor_reset_btn"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    const/16 v3, 0x3ee

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 370
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->UpdateGripdata()V

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 294
    :sswitch_data_0
    .sparse-switch
        0x7f0a009f -> :sswitch_0
        0x7f0a00a0 -> :sswitch_1
        0x7f0a00a9 -> :sswitch_3
        0x7f0a00aa -> :sswitch_4
        0x7f0a00ab -> :sswitch_2
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 95
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 98
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->setContentView(I)V

    .line 99
    const v0, 0x7f0a008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0a009a

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count_crcount:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0a00ac

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    const v0, 0x7f0a00ad

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_ResultDetail:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Calibration:Landroid/widget/Button;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const v0, 0x7f0a00ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Skip:Landroid/widget/Button;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_Skip:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    const v0, 0x7f0a00a9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_onoff:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_RAW_Count:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    const v0, 0x7f0a00aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    const-string v1, "RESET"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const/high16 v0, 0x41400000    # 12.0f

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->adjustTextSize(F)V

    .line 122
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->toggleButtonClickable(Z)V

    .line 125
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    .line 126
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    const v1, 0x10018

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 130
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->finishRefresh:Z

    .line 131
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProductModel:Ljava/lang/String;

    const-string v1, "T315"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->unregisterTestReceiver()V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/status/FactoryTestPhone;->bindSecPhoneService()V

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/status/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_3

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    if-eqz v0, :cond_2

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 266
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationASPxx$SensorTestListener;

    .line 268
    :cond_2
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 270
    :cond_3
    iput-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 272
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isgrip_On:Z

    .line 274
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z

    .line 275
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 276
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 242
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z

    .line 247
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSpecOutNumber:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_ResultDetail:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 148
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 150
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProductModel:Ljava/lang/String;

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProductModel:Ljava/lang/String;

    const-string v1, "T315"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->registerTestReceiver()V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/status/FactoryTestPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    invoke-virtual {v0}, Lcom/sec/android/app/status/FactoryTestPhone;->bindSecPhoneService()V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mFactoryPhone:Lcom/sec/android/app/status/FactoryTestPhone;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/FactoryTestPhone;->requestGripSensorOn(Z)V

    .line 162
    :cond_1
    const-string v0, "GripSensorCalibrationASPxx"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProductModel:Ljava/lang/String;

    const-string v1, "T315"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 165
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->test_start()V

    .line 167
    :cond_2
    return-void
.end method
