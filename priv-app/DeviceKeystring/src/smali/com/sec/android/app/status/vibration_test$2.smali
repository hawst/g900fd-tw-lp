.class Lcom/sec/android/app/status/vibration_test$2;
.super Ljava/lang/Object;
.source "vibration_test.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/vibration_test;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/vibration_test;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/vibration_test;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/status/vibration_test;->access$400(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    # setter for: Lcom/sec/android/app/status/vibration_test;->mLED_G:I
    invoke-static {v0, v1}, Lcom/sec/android/app/status/vibration_test;->access$302(Lcom/sec/android/app/status/vibration_test;I)I

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/status/vibration_test;->access$600(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    # setter for: Lcom/sec/android/app/status/vibration_test;->mLED_B:I
    invoke-static {v0, v1}, Lcom/sec/android/app/status/vibration_test;->access$502(Lcom/sec/android/app/status/vibration_test;I)I

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_redValue:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/vibration_test;->access$700(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RED : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mLED_G:I
    invoke-static {v1}, Lcom/sec/android/app/status/vibration_test;->access$300(Lcom/sec/android/app/status/vibration_test;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/status/vibration_test$2;->this$0:Lcom/sec/android/app/status/vibration_test;

    # getter for: Lcom/sec/android/app/status/vibration_test;->mLED_B:I
    invoke-static {v2}, Lcom/sec/android/app/status/vibration_test;->access$500(Lcom/sec/android/app/status/vibration_test;)I

    move-result v2

    # invokes: Lcom/sec/android/app/status/vibration_test;->setLED(III)V
    invoke-static {v0, p2, v1, v2}, Lcom/sec/android/app/status/vibration_test;->access$800(Lcom/sec/android/app/status/vibration_test;III)V

    .line 119
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 113
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 110
    return-void
.end method
