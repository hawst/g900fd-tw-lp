.class final enum Lcom/sec/android/app/status/TspTestMode$TspTest;
.super Ljava/lang/Enum;
.source "TspTestMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspTestMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TspTest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/TspTestMode$TspTest;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/TspTestMode$TspTest;

.field public static final enum TSP_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

.field public static final enum TSP_HOVER_ACCURACY_TEST:Lcom/sec/android/app/status/TspTestMode$TspTest;

.field public static final enum TSP_HOVER_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

.field public static final enum TSP_SIDE_TOUCH_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

.field public static final enum TSP_SIDE_TOUCH_TEST_VALUE:Lcom/sec/android/app/status/TspTestMode$TspTest;


# instance fields
.field private mEnable:Z

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 21
    new-instance v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    const-string v1, "TSP_DATA"

    const-string v2, "TSP Data"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/status/TspTestMode$TspTest;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    .line 22
    new-instance v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    const-string v1, "TSP_HOVER_DATA"

    const-string v2, "TSP Hover Data"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/sec/android/app/status/TspTestMode$TspTest;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_HOVER_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    .line 23
    new-instance v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    const-string v1, "TSP_HOVER_ACCURACY_TEST"

    const-string v2, "Tsp Hover Accuracy test"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/android/app/status/TspTestMode$TspTest;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_HOVER_ACCURACY_TEST:Lcom/sec/android/app/status/TspTestMode$TspTest;

    .line 24
    new-instance v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    const-string v1, "TSP_SIDE_TOUCH_TEST_VALUE"

    const-string v2, "Side Touch Test(Value)"

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/sec/android/app/status/TspTestMode$TspTest;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_SIDE_TOUCH_TEST_VALUE:Lcom/sec/android/app/status/TspTestMode$TspTest;

    .line 25
    new-instance v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    const-string v1, "TSP_SIDE_TOUCH_DATA"

    const-string v2, "Side Touch Data"

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/sec/android/app/status/TspTestMode$TspTest;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_SIDE_TOUCH_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/status/TspTestMode$TspTest;

    sget-object v1, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_HOVER_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_HOVER_ACCURACY_TEST:Lcom/sec/android/app/status/TspTestMode$TspTest;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_SIDE_TOUCH_TEST_VALUE:Lcom/sec/android/app/status/TspTestMode$TspTest;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/status/TspTestMode$TspTest;->TSP_SIDE_TOUCH_DATA:Lcom/sec/android/app/status/TspTestMode$TspTest;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->$VALUES:[Lcom/sec/android/app/status/TspTestMode$TspTest;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 1
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "testEnable"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/TspTestMode$TspTest;->mEnable:Z

    .line 31
    iput-object p3, p0, Lcom/sec/android/app/status/TspTestMode$TspTest;->mTitle:Ljava/lang/String;

    .line 32
    iput-boolean p4, p0, Lcom/sec/android/app/status/TspTestMode$TspTest;->mEnable:Z

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/TspTestMode$TspTest;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/TspTestMode$TspTest;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/TspTestMode$TspTest;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/status/TspTestMode$TspTest;->$VALUES:[Lcom/sec/android/app/status/TspTestMode$TspTest;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/TspTestMode$TspTest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/TspTestMode$TspTest;

    return-object v0
.end method


# virtual methods
.method public isEnable()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/sec/android/app/status/TspTestMode$TspTest;->mEnable:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/status/TspTestMode$TspTest;->mTitle:Ljava/lang/String;

    return-object v0
.end method
