.class public Lcom/sec/android/app/status/AccSensorCalibration;
.super Landroid/app/Activity;
.source "AccSensorCalibration.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    }
.end annotation


# instance fields
.field private mAccSensor:Landroid/hardware/Sensor;

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mHandler:Landroid/os/Handler;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

.field private mText_CalibrationState:Landroid/widget/TextView;

.field private mText_X:Landroid/widget/TextView;

.field private mText_Y:Landroid/widget/TextView;

.field private mText_Z:Landroid/widget/TextView;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 55
    new-instance v0, Lcom/sec/android/app/status/AccSensorCalibration$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/AccSensorCalibration$1;-><init>(Lcom/sec/android/app/status/AccSensorCalibration;)V

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    .line 266
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_X:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/Sensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/AccSensorCalibration;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;
    .param p1, "x1"    # B

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/AccSensorCalibration;->calibration(B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_Y:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_Z:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/AccSensorCalibration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration;->updateCalState()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private calibration(B)Z
    .locals 9
    .param p1, "data"    # B

    .prologue
    .line 241
    const-string v5, "AccSensorCalibration"

    const-string v6, "calibration"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "acc sensor Calibration: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v4, 0x0

    .line 244
    .local v4, "res":Z
    const-string v0, "/sys/class/sensors/accelerometer_sensor/calibration"

    .line 245
    .local v0, "calib_sysfs":Ljava/lang/String;
    const/4 v2, 0x0

    .line 248
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const-string v5, "/sys/class/sensors/accelerometer_sensor/calibration"

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 250
    const/4 v4, 0x1

    .line 255
    if-eqz v3, :cond_0

    .line 256
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 263
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    return v4

    .line 258
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "AccSensorCalibration"

    const-string v6, "calibration"

    const-string v7, "acc sensor Calibration file close fail!!!"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 261
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 251
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 252
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "AccSensorCalibration"

    const-string v6, "calibration"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 255
    if-eqz v2, :cond_1

    .line 256
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 258
    :catch_2
    move-exception v1

    .line 259
    const-string v5, "AccSensorCalibration"

    const-string v6, "calibration"

    const-string v7, "acc sensor Calibration file close fail!!!"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 255
    :goto_2
    if-eqz v2, :cond_2

    .line 256
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 260
    :cond_2
    :goto_3
    throw v5

    .line 258
    :catch_3
    move-exception v1

    .line 259
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "AccSensorCalibration"

    const-string v7, "calibration"

    const-string v8, "acc sensor Calibration file close fail!!!"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 254
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 251
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private readCalibrationState()Z
    .locals 10

    .prologue
    .line 201
    const/4 v2, 0x0

    .line 202
    .local v2, "isCalibration":Z
    const/4 v3, 0x0

    .line 203
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v5, 0x0

    .line 206
    .local v5, "value":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/sys/class/sensors/accelerometer_sensor/calibration"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    const/4 v0, 0x0

    .line 209
    .local v0, "calData":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 210
    if-eqz v5, :cond_3

    .line 211
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 212
    if-eqz v0, :cond_0

    .line 213
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "acc sensor Cal Data : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v6

    const/16 v7, 0x30

    if-le v6, v7, :cond_0

    .line 217
    const/4 v2, 0x1

    .line 228
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 229
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    move-object v3, v4

    .line 236
    .end local v0    # "calData":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isCalibration : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    return v2

    .line 221
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "calData":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_3
    :try_start_3
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "calData = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 224
    .end local v0    # "calData":Ljava/lang/String;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :goto_2
    :try_start_4
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 228
    if-eqz v3, :cond_2

    .line 229
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 231
    :catch_1
    move-exception v1

    .line 232
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 231
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "calData":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    .line 232
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "AccSensorCalibration"

    const-string v7, "readCalibrationState"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 234
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 227
    .end local v0    # "calData":Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 228
    :goto_3
    if-eqz v3, :cond_4

    .line 229
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 233
    :cond_4
    :goto_4
    throw v6

    .line 231
    :catch_3
    move-exception v1

    .line 232
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "AccSensorCalibration"

    const-string v8, "readCalibrationState"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 227
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v0    # "calData":Ljava/lang/String;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 223
    .end local v0    # "calData":Ljava/lang/String;
    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method private updateCalState()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration;->readCalibrationState()Z

    move-result v0

    .line 79
    .local v0, "state":Z
    iget-object v2, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_CalibrationState:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Calibration: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v1, "Yes"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    if-eqz v0, :cond_1

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    const-string v3, "false;true"

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 88
    :goto_1
    return-void

    .line 79
    :cond_0
    const-string v1, "No"

    goto :goto_0

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;

    const-string v3, "true;false"

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method


# virtual methods
.method public getAutoRotateState()I
    .locals 5

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/status/AccSensorCalibration;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 145
    .local v0, "state":I
    const-string v1, "AccSensorCalibration"

    const-string v2, "getAutoRotate"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0a0008

    if-ne v2, v3, :cond_0

    const/16 v0, 0x31

    .line 154
    .local v0, "data":B
    :goto_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/status/AccSensorCalibration$2;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration$2;-><init>(Lcom/sec/android/app/status/AccSensorCalibration;B)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 197
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 198
    return-void

    .line 151
    .end local v0    # "data":B
    .end local v1    # "thread":Ljava/lang/Thread;
    :cond_0
    const/16 v0, 0x30

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const-string v0, "AccSensorCalibration"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->setContentView(I)V

    .line 95
    const v0, 0x7f0a0004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_X:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_Y:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0a0006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_Z:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0a0007

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mText_CalibrationState:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0a0008

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v0, 0x7f0a0009

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;

    .line 105
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTimer:Ljava/util/Timer;

    .line 106
    new-instance v0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;-><init>(Lcom/sec/android/app/status/AccSensorCalibration;Lcom/sec/android/app/status/AccSensorCalibration$1;)V

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x64

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 108
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 132
    const-string v0, "AccSensorCalibration"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 135
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 123
    const-string v0, "AccSensorCalibration"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 125
    const-string v0, "AccSensorCalibration"

    const-string v1, "onPause"

    const-string v2, "unregister acc sensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    iget-object v2, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->access$800(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V

    .line 128
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 112
    const-string v0, "AccSensorCalibration"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 114
    const-string v0, "AccSensorCalibration"

    const-string v1, "onResume"

    const-string v2, "register acc sensor"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration;->updateCalState()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    iget-object v2, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->resume()V
    invoke-static {v0}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->access$700(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V

    .line 119
    return-void
.end method

.method public setAutoRotate(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 138
    const-string v0, "AccSensorCalibration"

    const-string v1, "setAutoRotate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/status/AccSensorCalibration;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 140
    return-void
.end method
