.class public Lcom/sec/android/app/lcdtest/modules/ModulePower;
.super Lcom/sec/android/app/lcdtest/modules/ModuleObject;
.source "ModulePower.java"


# static fields
.field private static mInstance:Lcom/sec/android/app/lcdtest/modules/ModulePower;


# instance fields
.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 24
    const-string v0, "ModulePower"

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/lcdtest/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModulePower"

    const-string v2, "Create ModulePower"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModulePower;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sec/android/app/lcdtest/modules/ModulePower;

    invoke-direct {v0, p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    .line 33
    :cond_0
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModulePower;

    return-object v0
.end method


# virtual methods
.method public getTouchLedTime()I
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_key_light"

    const/16 v2, 0x5dc

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public resetFuelGaugeIC()Z
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModulePower;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "resetFuelGaugeIC"

    const-string v2, "Fuel Gauge IC reset"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v0, "FUEL_GAUGE_RESET"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setTouchLedTime(I)V
    .locals 2
    .param p1, "time"    # I

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/lcdtest/modules/ModulePower;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "button_key_light"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 208
    return-void
.end method
