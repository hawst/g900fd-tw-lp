.class public Lcom/sec/android/app/brightness/Brightness;
.super Landroid/app/Activity;
.source "Brightness.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/brightness/Brightness$KeycodeListener;,
        Lcom/sec/android/app/brightness/Brightness$CancelListener;,
        Lcom/sec/android/app/brightness/Brightness$OKListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v3, 0x7f03000e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/brightness/Brightness;->setContentView(I)V

    .line 21
    new-instance v0, Lcom/sec/android/app/brightness/miniDlg;

    invoke-direct {v0, p0}, Lcom/sec/android/app/brightness/miniDlg;-><init>(Landroid/content/Context;)V

    .line 22
    .local v0, "Panel":Lcom/sec/android/app/brightness/miniDlg;
    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->show()V

    .line 23
    const v3, 0x7f0a003c

    invoke-virtual {v0, v3}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 24
    .local v2, "buttonOK":Landroid/widget/Button;
    const v3, 0x7f0a003d

    invoke-virtual {v0, v3}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 25
    .local v1, "buttonCancel":Landroid/widget/Button;
    new-instance v3, Lcom/sec/android/app/brightness/Brightness$OKListener;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/brightness/Brightness$OKListener;-><init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    new-instance v3, Lcom/sec/android/app/brightness/Brightness$CancelListener;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/brightness/Brightness$CancelListener;-><init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    new-instance v3, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/brightness/Brightness$KeycodeListener;-><init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/brightness/miniDlg;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 28
    return-void
.end method
