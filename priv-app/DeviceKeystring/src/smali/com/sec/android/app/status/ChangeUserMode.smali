.class public Lcom/sec/android/app/status/ChangeUserMode;
.super Ljava/lang/Object;
.source "ChangeUserMode.java"


# static fields
.field private static final instance:Lcom/sec/android/app/status/ChangeUserMode;

.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/status/ChangeUserMode;

    invoke-direct {v0}, Lcom/sec/android/app/status/ChangeUserMode;-><init>()V

    sput-object v0, Lcom/sec/android/app/status/ChangeUserMode;->instance:Lcom/sec/android/app/status/ChangeUserMode;

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/ChangeUserMode;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/status/ChangeUserMode;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    sput-object p0, Lcom/sec/android/app/status/ChangeUserMode;->mContext:Landroid/content/Context;

    .line 120
    sget-object v0, Lcom/sec/android/app/status/ChangeUserMode;->instance:Lcom/sec/android/app/status/ChangeUserMode;

    return-object v0
.end method

.method private turnOnUserMode()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 59
    const/4 v5, 0x0

    .line 60
    .local v5, "fileW":Ljava/io/FileWriter;
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    const-string v9, "turnOnKeystringBlock"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :try_start_0
    new-instance v3, Ljava/io/File;

    const-string v7, "/efs/FactoryApp"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .local v3, "f":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    const-string v7, "/efs/FactoryApp/factorymode"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v4, "f2":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 67
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    const-string v9, "/efs/FactoryApp is not exist"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    move-result v7

    if-ne v7, v10, :cond_0

    .line 70
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z

    .line 71
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 75
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 76
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    const-string v9, "/efs/FactoryApp/factorymode is not exist"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    .line 78
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 87
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "f2":Ljava/io/File;
    :cond_1
    :goto_0
    :try_start_1
    new-instance v6, Ljava/io/FileWriter;

    const-string v7, "/efs/FactoryApp/factorymode"

    invoke-direct {v6, v7}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 88
    .end local v5    # "fileW":Ljava/io/FileWriter;
    .local v6, "fileW":Ljava/io/FileWriter;
    :try_start_2
    const-string v7, "ON"

    invoke-virtual {v6, v7}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v6}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    move-object v5, v6

    .line 110
    .end local v6    # "fileW":Ljava/io/FileWriter;
    .restart local v5    # "fileW":Ljava/io/FileWriter;
    :goto_1
    const-string v7, "persist.sys.factory.failhist"

    const-string v8, "none"

    invoke-static {v7, v8}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void

    .line 80
    :catch_0
    move-exception v2

    .line 81
    .local v2, "ex":Ljava/io/FileNotFoundException;
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "FileNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 82
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 83
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 91
    .restart local v2    # "ex":Ljava/io/FileNotFoundException;
    :goto_2
    if-eqz v5, :cond_2

    .line 93
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 98
    :cond_2
    :goto_3
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "FileNotFoundException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 94
    :catch_3
    move-exception v1

    .line 95
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 99
    .end local v1    # "e1":Ljava/io/IOException;
    .end local v2    # "ex":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v0

    .line 100
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    if-eqz v5, :cond_3

    .line 102
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 107
    :cond_3
    :goto_5
    const-string v7, "ChangeUserMode"

    const-string v8, "turnOnUserMode"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "IOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 103
    :catch_5
    move-exception v1

    .line 104
    .restart local v1    # "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "e1":Ljava/io/IOException;
    .end local v5    # "fileW":Ljava/io/FileWriter;
    .restart local v6    # "fileW":Ljava/io/FileWriter;
    :catch_6
    move-exception v0

    move-object v5, v6

    .end local v6    # "fileW":Ljava/io/FileWriter;
    .restart local v5    # "fileW":Ljava/io/FileWriter;
    goto :goto_4

    .line 90
    .end local v5    # "fileW":Ljava/io/FileWriter;
    .restart local v6    # "fileW":Ljava/io/FileWriter;
    :catch_7
    move-exception v2

    move-object v5, v6

    .end local v6    # "fileW":Ljava/io/FileWriter;
    .restart local v5    # "fileW":Ljava/io/FileWriter;
    goto :goto_2
.end method


# virtual methods
.method public changeToUserMode()Z
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/status/ChangeUserMode;->turnOnUserMode()V

    .line 55
    const/4 v0, 0x1

    return v0
.end method
