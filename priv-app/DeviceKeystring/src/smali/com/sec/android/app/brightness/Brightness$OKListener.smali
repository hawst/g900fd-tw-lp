.class public Lcom/sec/android/app/brightness/Brightness$OKListener;
.super Ljava/lang/Object;
.source "Brightness.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/brightness/Brightness;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "OKListener"
.end annotation


# instance fields
.field private brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

.field final synthetic this$0:Lcom/sec/android/app/brightness/Brightness;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/brightness/Brightness;Lcom/sec/android/app/brightness/miniDlg;)V
    .locals 0
    .param p2, "dialog"    # Lcom/sec/android/app/brightness/miniDlg;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/app/brightness/Brightness$OKListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/sec/android/app/brightness/Brightness$OKListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    .line 35
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$OKListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->settingSystemBrightness()V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$OKListener;->brightnessContrlDlg:Lcom/sec/android/app/brightness/miniDlg;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/miniDlg;->dismiss()V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/brightness/Brightness$OKListener;->this$0:Lcom/sec/android/app/brightness/Brightness;

    invoke-virtual {v0}, Lcom/sec/android/app/brightness/Brightness;->finish()V

    .line 41
    return-void
.end method
