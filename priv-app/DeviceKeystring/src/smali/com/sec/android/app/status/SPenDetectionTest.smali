.class public Lcom/sec/android/app/status/SPenDetectionTest;
.super Landroid/app/Activity;
.source "SPenDetectionTest.java"


# instance fields
.field SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

.field private mBackground:Landroid/widget/LinearLayout;

.field private mCount:I

.field private mTextViewCount:Landroid/widget/TextView;

.field private mTextViewState:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mCount:I

    return-void
.end method

.method private Detect()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mTextViewState:Landroid/widget/TextView;

    const-string v1, "DETECT"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mBackground:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 64
    return-void
.end method

.method private Pending()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mCount:I

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->isSpenReleased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->Release()V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->Detect()V

    goto :goto_0
.end method

.method private Release()V
    .locals 3

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mCount:I

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mTextViewState:Landroid/widget/TextView;

    const-string v1, "RELEASE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mTextViewCount:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "COUNT : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mBackground:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/SPenDetectionTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPenDetectionTest;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->Detect()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/SPenDetectionTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPenDetectionTest;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->Release()V

    return-void
.end method

.method private isSpenReleased()Z
    .locals 3

    .prologue
    .line 74
    const/16 v0, 0xd

    .line 75
    .local v0, "SW_PEN_INSERT":I
    const/4 v1, 0x0

    .line 85
    .local v1, "sw":I
    if-lez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v1, 0x7f030061

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/SPenDetectionTest;->setContentView(I)V

    .line 32
    const v1, 0x7f0a000b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/SPenDetectionTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mBackground:Landroid/widget/LinearLayout;

    .line 33
    const v1, 0x7f0a01e4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/SPenDetectionTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mTextViewState:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f0a01e5

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/SPenDetectionTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/SPenDetectionTest;->mTextViewCount:Landroid/widget/TextView;

    .line 35
    new-instance v1, Lcom/sec/android/app/status/SPenDetectionTest$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/SPenDetectionTest$1;-><init>(Lcom/sec/android/app/status/SPenDetectionTest;)V

    iput-object v1, p0, Lcom/sec/android/app/status/SPenDetectionTest;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.pen.INSERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/app/status/SPenDetectionTest;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/status/SPenDetectionTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 49
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/status/SPenDetectionTest;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPenDetectionTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 103
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 97
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/status/SPenDetectionTest;->Pending()V

    .line 92
    return-void
.end method
