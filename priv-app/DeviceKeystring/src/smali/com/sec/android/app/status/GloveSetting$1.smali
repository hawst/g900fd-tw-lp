.class Lcom/sec/android/app/status/GloveSetting$1;
.super Ljava/lang/Object;
.source "GloveSetting.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GloveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GloveSetting;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GloveSetting;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/status/GloveSetting$1;->this$0:Lcom/sec/android/app/status/GloveSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 39
    const-string v1, "TSP_COMMAND_RESULT"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "result":Ljava/lang/String;
    const-string v1, "glove_mode,1:OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/sec/android/app/status/GloveSetting$1;->this$0:Lcom/sec/android/app/status/GloveSetting;

    const-string v2, "glove : on"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 47
    :goto_0
    return-void

    .line 42
    :cond_0
    const-string v1, "glove_mode,0:OK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/status/GloveSetting$1;->this$0:Lcom/sec/android/app/status/GloveSetting;

    const-string v2, "glove : off"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GloveSetting$1;->this$0:Lcom/sec/android/app/status/GloveSetting;

    const-string v2, "glove : Setting failed!"

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
