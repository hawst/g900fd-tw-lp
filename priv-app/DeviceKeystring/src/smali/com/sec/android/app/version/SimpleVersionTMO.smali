.class public Lcom/sec/android/app/version/SimpleVersionTMO;
.super Lcom/sec/android/app/version/SimpleVersion;
.source "SimpleVersionTMO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    }
.end annotation


# instance fields
.field private currentModeTypeForEnd:C

.field private currentSVMode:C

.field private ftaswver:Landroid/widget/TextView;

.field public mHandler:Landroid/os/Handler;

.field private mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

.field private mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceMessenger:Landroid/os/Messenger;

.field private mStrings:[Ljava/lang/String;

.field private mSvcModeMessenger:Landroid/os/Messenger;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/version/SimpleVersion;-><init>()V

    .line 30
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, ""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;

    .line 34
    iput-object v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mServiceMessenger:Landroid/os/Messenger;

    .line 35
    iput-object v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    .line 148
    new-instance v0, Lcom/sec/android/app/version/SimpleVersionTMO$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/version/SimpleVersionTMO$1;-><init>(Lcom/sec/android/app/version/SimpleVersionTMO;)V

    iput-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 201
    new-instance v0, Lcom/sec/android/app/version/SimpleVersionTMO$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/version/SimpleVersionTMO$2;-><init>(Lcom/sec/android/app/version/SimpleVersionTMO;)V

    iput-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mHandler:Landroid/os/Handler;

    .line 259
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSvcModeMessenger:Landroid/os/Messenger;

    return-void
.end method

.method private SendData(CCCCC)V
    .locals 5
    .param p1, "svcMode"    # C
    .param p2, "modetype"    # C
    .param p3, "subtype"    # C
    .param p4, "keycode"    # C
    .param p5, "query"    # C

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 165
    .local v0, "data":[B
    packed-switch p1, :pswitch_data_0

    .line 178
    :goto_0
    iput-char p1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->currentSVMode:C

    .line 180
    if-nez v0, :cond_0

    .line 181
    const-string v1, "SimpleVersion"

    const-string v2, "SendData"

    const-string v3, " err - data is NULL"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :goto_1
    return-void

    .line 167
    :pswitch_0
    const-string v1, "SimpleVersion"

    const-string v2, "SendData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "svcMode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v1, "SimpleVersion"

    const-string v2, "SendData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "modetype : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const-string v1, "SimpleVersion"

    const-string v2, "SendData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "subtype : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v1, "SimpleVersion"

    const-string v2, "SendData"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "query : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    invoke-virtual {v1, p1, p2, p3, p5}, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->getServMEnterData(CCCC)[B

    move-result-object v0

    .line 172
    iput-char p2, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->currentModeTypeForEnd:C

    goto :goto_0

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x3f0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/version/SimpleVersionTMO;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_1

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private UpdateFTAVersion()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 262
    const-string v0, "SimpleVersion"

    const-string v1, "UpdateFTAVersion"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FTA SW VERSION  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/version/SimpleVersionTMO;Landroid/os/Messenger;)Landroid/os/Messenger;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO;
    .param p1, "x1"    # Landroid/os/Messenger;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mServiceMessenger:Landroid/os/Messenger;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/version/SimpleVersionTMO;)Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/version/SimpleVersionTMO;CCCCC)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO;
    .param p1, "x1"    # C
    .param p2, "x2"    # C
    .param p3, "x3"    # C
    .param p4, "x4"    # C
    .param p5, "x5"    # C

    .prologue
    .line 23
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/version/SimpleVersionTMO;->SendData(CCCCC)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/version/SimpleVersionTMO;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mStrings:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/version/SimpleVersionTMO;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/version/SimpleVersionTMO;->UpdateFTAVersion()V

    return-void
.end method

.method private connectToRilService()V
    .locals 4

    .prologue
    .line 142
    const-string v1, "SimpleVersion"

    const-string v2, "connectToRilService"

    const-string v3, "connect To Ril service"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.phone"

    const-string v2, "com.sec.phone.SecPhoneService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/version/SimpleVersionTMO;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 146
    return-void
.end method

.method private invokeOemRilRequestRaw([BLandroid/os/Message;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "response"    # Landroid/os/Message;

    .prologue
    .line 189
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 190
    .local v1, "req":Landroid/os/Bundle;
    const-string v2, "request"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 191
    invoke-virtual {p2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSvcModeMessenger:Landroid/os/Messenger;

    iput-object v2, p2, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 195
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mServiceMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, p2}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 197
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/app/version/SimpleVersion;->onCreate(Landroid/os/Bundle;)V

    .line 41
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    .line 42
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLines(I)V

    .line 45
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    const-string v2, "FTA SW VERSION:"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    const v1, 0x7f0a017b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/version/SimpleVersionTMO;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 47
    .local v0, "layout":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->ftaswver:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 48
    new-instance v1, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;-><init>(Lcom/sec/android/app/version/SimpleVersionTMO$1;)V

    iput-object v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mOem:Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/version/SimpleVersionTMO;->connectToRilService()V

    .line 50
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/version/SimpleVersionTMO;->unbindService(Landroid/content/ServiceConnection;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO;->mSecPhoneServiceConnection:Landroid/content/ServiceConnection;

    .line 56
    invoke-super {p0}, Lcom/sec/android/app/version/SimpleVersion;->onDestroy()V

    .line 57
    return-void
.end method
