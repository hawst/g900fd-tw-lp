.class Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
.super Ljava/util/TimerTask;
.source "AccSensorCalibration.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/AccSensorCalibration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorReadTask"
.end annotation


# instance fields
.field private mAcceSensorValues:[F

.field private mIsRunningTask:Z

.field private mRawData:[I

.field final synthetic this$0:Lcom/sec/android/app/status/AccSensorCalibration;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/AccSensorCalibration;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 266
    iput-object p1, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 267
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mIsRunningTask:Z

    .line 268
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mAcceSensorValues:[F

    .line 269
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/AccSensorCalibration;Lcom/sec/android/app/status/AccSensorCalibration$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration;
    .param p2, "x1"    # Lcom/sec/android/app/status/AccSensorCalibration$1;

    .prologue
    .line 266
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;-><init>(Lcom/sec/android/app/status/AccSensorCalibration;)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->resume()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->pause()V

    return-void
.end method

.method private pause()V
    .locals 3

    .prologue
    .line 357
    const-string v0, "AccSensorCalibration"

    const-string v1, "SensorReadTask"

    const-string v2, "pause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mIsRunningTask:Z

    .line 359
    return-void
.end method

.method private readToAccelerometerSensor()V
    .locals 14

    .prologue
    const-wide/high16 v12, 0x4090000000000000L    # 1024.0

    const-wide v10, 0x40239eb851eb851fL    # 9.81

    .line 289
    const/4 v2, 0x0

    .line 290
    .local v2, "reader":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 293
    .local v4, "value":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    const-string v6, "/sys/class/sensors/accelerometer_sensor/raw_data"

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 296
    const-string v5, "AccSensorCalibration"

    const-string v6, "readToAccelerometerSensor = "

    invoke-static {v5, v6, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    if-eqz v4, :cond_0

    .line 298
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 299
    .local v1, "rawDatas":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v1, v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    aget-object v5, v1, v5

    if-eqz v5, :cond_0

    const/4 v5, 0x2

    aget-object v5, v1, v5

    if-eqz v5, :cond_0

    .line 300
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-object v7, v1, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 301
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x1

    const/4 v7, 0x1

    aget-object v7, v1, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6

    .line 302
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x2

    const/4 v7, 0x2

    aget-object v7, v1, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    aput v7, v5, v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 329
    .end local v1    # "rawDatas":[Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_3

    .line 331
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 337
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    return-void

    .line 332
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "AccSensorCalibration"

    const-string v6, "readToAccelerometerSensor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 334
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 305
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 306
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "AccSensorCalibration"

    const-string v6, "readToAccelerometerSensor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Accelerometer Sensor IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mAcceSensorValues:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    float-to-double v8, v7

    mul-double/2addr v8, v12

    div-double/2addr v8, v10

    double-to-int v7, v8

    aput v7, v5, v6

    .line 314
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mAcceSensorValues:[F

    const/4 v8, 0x1

    aget v7, v7, v8

    float-to-double v8, v7

    mul-double/2addr v8, v12

    div-double/2addr v8, v10

    double-to-int v7, v8

    aput v7, v5, v6

    .line 321
    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mAcceSensorValues:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    float-to-double v8, v7

    mul-double/2addr v8, v12

    div-double/2addr v8, v10

    double-to-int v7, v8

    aput v7, v5, v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 329
    if-eqz v2, :cond_1

    .line 331
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 332
    :catch_2
    move-exception v0

    .line 333
    const-string v5, "AccSensorCalibration"

    const-string v6, "readToAccelerometerSensor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "File close exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v2, :cond_2

    .line 331
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 334
    :cond_2
    :goto_3
    throw v5

    .line 332
    :catch_3
    move-exception v0

    .line 333
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "AccSensorCalibration"

    const-string v7, "readToAccelerometerSensor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "File close exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 329
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 305
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1

    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method private resume()V
    .locals 3

    .prologue
    .line 352
    const-string v0, "AccSensorCalibration"

    const-string v1, "SensorReadTask"

    const-string v2, "resume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mIsRunningTask:Z

    .line 354
    return-void
.end method


# virtual methods
.method public getX()I
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getY()I
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getZ()I
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mRawData:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    return v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 273
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 277
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mAcceSensorValues:[F

    .line 278
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->mIsRunningTask:Z

    if-eqz v0, :cond_0

    .line 283
    invoke-direct {p0}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->readToAccelerometerSensor()V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/AccSensorCalibration;->access$900(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 286
    :cond_0
    return-void
.end method
