.class public Lcom/sec/android/app/lcdtest/modules/ModuleDevice;
.super Lcom/sec/android/app/lcdtest/modules/ModuleObject;
.source "ModuleDevice.java"


# static fields
.field private static final OTG_MUIC_OFF:[B

.field private static final OTG_MUIC_ON:[B

.field private static final OTG_TEST_OFF:[B

.field private static final OTG_TEST_ON:[B

.field private static final STORAGE_PATH:[Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;


# instance fields
.field private mIsMountedExternalStorage:Z

.field private mIsMountedUsbStorage:Z

.field private mIsMountedUsbStorageB:Z

.field private mStorageManager:Landroid/os/storage/StorageManager;

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    .line 71
    new-array v0, v4, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "/mnt/sdcard"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/mnt/extSdCard"

    aput-object v2, v0, v1

    const-string v1, "/system"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->STORAGE_PATH:[Ljava/lang/String;

    .line 231
    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->OTG_TEST_ON:[B

    .line 234
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->OTG_TEST_OFF:[B

    .line 237
    new-array v0, v3, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->OTG_MUIC_ON:[B

    .line 240
    new-array v0, v3, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->OTG_MUIC_OFF:[B

    return-void

    .line 231
    nop

    :array_0
    .array-data 1
        0x4ft
        0x4et
        0x0t
    .end array-data

    .line 234
    :array_1
    .array-data 1
        0x4ft
        0x46t
        0x46t
        0x0t
    .end array-data

    .line 237
    :array_2
    .array-data 1
        0x30t
        0x0t
    .end array-data

    .line 240
    nop

    :array_3
    .array-data 1
        0x31t
        0x0t
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 43
    const-string v1, "ModuleDevice"

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleObject;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 185
    iput-boolean v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mIsMountedExternalStorage:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mIsMountedUsbStorage:Z

    .line 187
    iput-boolean v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mIsMountedUsbStorageB:Z

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "Create ModuleDevice"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "vibrator"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mVibrator:Landroid/os/Vibrator;

    .line 46
    const-string v1, "storage"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mStorageManager:Landroid/os/storage/StorageManager;

    .line 47
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 48
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 49
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 50
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "ModuleDevice"

    const-string v3, "registerReceiver OK"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static instance(Landroid/content/Context;)Lcom/sec/android/app/lcdtest/modules/ModuleDevice;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    invoke-direct {v0, p0}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    .line 59
    :cond_0
    sget-object v0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->mInstance:Lcom/sec/android/app/lcdtest/modules/ModuleDevice;

    return-object v0
.end method

.method private startTSPTest([BI)Ljava/lang/String;
    .locals 6
    .param p1, "cmd"    # [B
    .param p2, "commandLength"    # I

    .prologue
    .line 303
    const-string v1, ""

    .line 304
    .local v1, "status":Ljava/lang/String;
    const-string v0, ""

    .line 306
    .local v0, "result":Ljava/lang/String;
    const-string v2, "TSP_COMMAND_CMD"

    invoke-static {v2, p1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;[B)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 307
    const-string v2, "TSP_COMMAND_STATUS"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 308
    const-string v2, "TSP_COMMAND_RESULT"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    if-nez v1, :cond_0

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Status Read => status == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v2, "NG"

    .line 340
    :goto_0
    return-object v2

    .line 313
    :cond_0
    const-string v2, "OK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    if-nez v0, :cond_1

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>1 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v2, "NG"

    goto :goto_0

    .line 319
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 322
    :cond_2
    const-string v2, "NOT_APPLICABLE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A- Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "N/A - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    if-nez v0, :cond_3

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Result Read =>2 result == null"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    const-string v2, "NG"

    goto/16 :goto_0

    .line 332
    :cond_3
    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 334
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - Status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Fail - result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v2, "NG"

    goto/16 :goto_0

    .line 339
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "startTSPTest"

    const-string v4, "Fail - Command Write"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v2, "NG"

    goto/16 :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "ModuleDevice"

    const-string v2, "finalize ModuleDevice"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 66
    return-void
.end method

.method public hoveringOnOFF(Ljava/lang/String;)V
    .locals 5
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 433
    const-string v0, "hover_enable"

    .line 434
    .local v0, "mainCmd":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 435
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "onOffHovering"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mainCmd="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const-string v1, "TSP_COMMAND_CMD"

    invoke-static {v1, v0}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 437
    return-void
.end method

.method public setHighSensitivityModeOnOff(Ljava/lang/String;)V
    .locals 4
    .param p1, "command"    # Ljava/lang/String;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "gloveModeOnOFF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "command="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v0, "PATH_HIGH_SENSITIVITY_MODE"

    invoke-static {v0, p1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 442
    return-void
.end method

.method public startTSPTest(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 297
    iget-object v1, p0, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "startTSPTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cmd name => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 299
    .local v0, "byteCMD":[B
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/lcdtest/modules/ModuleDevice;->startTSPTest([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
