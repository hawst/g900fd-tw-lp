.class Lcom/sec/android/app/status/Proximity_test$handlersensor;
.super Ljava/lang/Object;
.source "Proximity_test.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/Proximity_test;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "handlersensor"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/Proximity_test;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/Proximity_test;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/Proximity_test;Lcom/sec/android/app/status/Proximity_test$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/Proximity_test;
    .param p2, "x1"    # Lcom/sec/android/app/status/Proximity_test$1;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/Proximity_test$handlersensor;-><init>(Lcom/sec/android/app/status/Proximity_test;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 66
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 69
    const-string v0, "Proximity_test"

    const-string v1, "onSensorChanged"

    const-string v2, "event listen!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    float-to-int v0, v0

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # getter for: Lcom/sec/android/app/status/Proximity_test;->txtproximity:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$100(Lcom/sec/android/app/status/Proximity_test;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "PROXIMITY sensor is activated"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # getter for: Lcom/sec/android/app/status/Proximity_test;->Bacckview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$200(Lcom/sec/android/app/status/Proximity_test;)Landroid/view/View;

    move-result-object v0

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # invokes: Lcom/sec/android/app/status/Proximity_test;->startVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$300(Lcom/sec/android/app/status/Proximity_test;)V

    .line 80
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # getter for: Lcom/sec/android/app/status/Proximity_test;->txtproximity:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$100(Lcom/sec/android/app/status/Proximity_test;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "PROXIMITY sensor is deactivated"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # getter for: Lcom/sec/android/app/status/Proximity_test;->Bacckview:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$200(Lcom/sec/android/app/status/Proximity_test;)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/status/Proximity_test$handlersensor;->this$0:Lcom/sec/android/app/status/Proximity_test;

    # invokes: Lcom/sec/android/app/status/Proximity_test;->stopVibrate()V
    invoke-static {v0}, Lcom/sec/android/app/status/Proximity_test;->access$400(Lcom/sec/android/app/status/Proximity_test;)V

    goto :goto_0
.end method
