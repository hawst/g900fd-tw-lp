.class Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;
.super Ljava/lang/Object;
.source "FuelGaugeRegister.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;


# direct methods
.method constructor <init>(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;->this$0:Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 51
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 48
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;->this$0:Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    # getter for: Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mWriteFuelGauge1:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->access$100(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/Button;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister$1;->this$0:Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;

    # getter for: Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->mNewFuelGauge1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;->access$000(Lcom/sec/android/app/fuelgauge/FuelGaugeRegister;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
