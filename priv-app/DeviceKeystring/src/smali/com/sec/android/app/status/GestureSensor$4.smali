.class Lcom/sec/android/app/status/GestureSensor$4;
.super Ljava/lang/Object;
.source "GestureSensor.java"

# interfaces
.implements Lcom/samsung/android/sensorhub/SensorHubEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GestureSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GestureSensor;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GestureSensor;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private GetGesturedata([F)V
    .locals 2
    .param p1, "values"    # [F

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    const/4 v1, 0x0

    aget v1, p1, v1

    # invokes: Lcom/sec/android/app/status/GestureSensor;->DirectionSetText(F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/status/GestureSensor;->access$1100(Lcom/sec/android/app/status/GestureSensor;F)V

    .line 299
    return-void
.end method


# virtual methods
.method public onGetSensorHubData(Lcom/samsung/android/sensorhub/SensorHubEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/sensorhub/SensorHubEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 258
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GestureSensor$4;->GetGesturedata([F)V

    .line 260
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42880000    # 68.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$600(Lcom/sec/android/app/status/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/status/GestureSensor;->access$602(Lcom/sec/android/app/status/GestureSensor;Z)Z

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 295
    :goto_0
    return-void

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkBlue:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GestureSensor;->access$602(Lcom/sec/android/app/status/GestureSensor;Z)Z

    goto :goto_0

    .line 268
    :cond_1
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42aa0000    # 85.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$800(Lcom/sec/android/app/status/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/status/GestureSensor;->access$802(Lcom/sec/android/app/status/GestureSensor;Z)Z

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkGreen:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GestureSensor;->access$802(Lcom/sec/android/app/status/GestureSensor;Z)Z

    goto :goto_0

    .line 276
    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42a40000    # 82.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->checkRed:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$900(Lcom/sec/android/app/status/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkRed:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/status/GestureSensor;->access$902(Lcom/sec/android/app/status/GestureSensor;Z)Z

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 281
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkRed:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GestureSensor;->access$902(Lcom/sec/android/app/status/GestureSensor;Z)Z

    goto :goto_0

    .line 284
    :cond_5
    iget-object v0, p1, Lcom/samsung/android/sensorhub/SensorHubEvent;->values:[F

    aget v0, v0, v2

    const/high16 v1, 0x42980000    # 76.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$1000(Lcom/sec/android/app/status/GestureSensor;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/status/GestureSensor;->access$1002(Lcom/sec/android/app/status/GestureSensor;Z)Z

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0

    .line 289
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # setter for: Lcom/sec/android/app/status/GestureSensor;->checkYellow:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GestureSensor;->access$1002(Lcom/sec/android/app/status/GestureSensor;Z)Z

    goto/16 :goto_0

    .line 293
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/status/GestureSensor$4;->this$0:Lcom/sec/android/app/status/GestureSensor;

    # getter for: Lcom/sec/android/app/status/GestureSensor;->bgView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/status/GestureSensor;->access$700(Lcom/sec/android/app/status/GestureSensor;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0
.end method
