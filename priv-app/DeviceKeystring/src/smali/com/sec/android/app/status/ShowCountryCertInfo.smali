.class public Lcom/sec/android/app/status/ShowCountryCertInfo;
.super Landroid/app/Activity;
.source "ShowCountryCertInfo.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mMainStr:Ljava/lang/StringBuffer;

.field private mTitleStr:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 16
    const-string v0, "ShowCountryCertInfo"

    iput-object v0, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->TAG:Ljava/lang/String;

    .line 17
    const-string v0, "Information"

    iput-object v0, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mTitleStr:Ljava/lang/String;

    .line 18
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    return-void
.end method

.method private showSAR(Ljava/lang/String;)V
    .locals 3
    .param p1, "sarStr"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v0, "ShowCountryCertInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showSAR sarStr = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mTitleStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/status/ShowCountryCertInfo$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/ShowCountryCertInfo$1;-><init>(Lcom/sec/android/app/status/ShowCountryCertInfo;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 71
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const-string v7, "ShowCountryCertInfo"

    const-string v8, "onCreate"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    const v7, 0x7f030039

    invoke-virtual {p0, v7}, Lcom/sec/android/app/status/ShowCountryCertInfo;->setContentView(I)V

    .line 25
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 27
    .local v4, "infoList":Ljava/util/List;, "Ljava/util/List<[Ljava/lang/String;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/ShowCountryCertInfo;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "country_cert_info"

    invoke-static {v7, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "info":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 29
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, ";"

    invoke-direct {v6, v2, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .local v6, "strToken":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 31
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 32
    .local v3, "infoArray":[Ljava/lang/String;
    if-eqz v3, :cond_0

    array-length v7, v3

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 33
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    const-string v7, "ShowCountryCertInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "country_cert_info : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v3, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v3, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    .end local v2    # "info":Ljava/lang/String;
    .end local v3    # "infoArray":[Ljava/lang/String;
    .end local v6    # "strToken":Ljava/util/StringTokenizer;
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "ShowCountryCertInfo"

    const-string v8, "Failed to get country_cert_info from Settings."

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 43
    .local v5, "infoListSize":I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 44
    .restart local v3    # "infoArray":[Ljava/lang/String;
    if-ne v5, v10, :cond_2

    .line 45
    aget-object v7, v3, v11

    iput-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mTitleStr:Ljava/lang/String;

    .line 46
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    aget-object v8, v3, v10

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 48
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-lez v7, :cond_3

    .line 49
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v9, v3, v11

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v3, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 54
    .end local v3    # "infoArray":[Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    if-lez v7, :cond_5

    .line 55
    iget-object v7, p0, Lcom/sec/android/app/status/ShowCountryCertInfo;->mMainStr:Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/status/ShowCountryCertInfo;->showSAR(Ljava/lang/String;)V

    .line 60
    :goto_2
    return-void

    .line 57
    :cond_5
    const-string v7, "ShowCountryCertInfo"

    const-string v8, "country_cert_info is empty"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/status/ShowCountryCertInfo;->finish()V

    goto :goto_2
.end method
