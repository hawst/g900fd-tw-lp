.class public Lcom/sec/android/app/selftestmode/SelfTestListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SelfTestListAdapter.java"


# static fields
.field public static ACC_SENSOR_TEST:I

.field public static BLUETOOTH_TEST:I

.field public static CAMERA_TEST:I

.field public static DIMMING_TEST:I

.field private static EMPTY_TEST:I

.field public static LIGHT_SENSOR_TEST:I

.field public static MELODY_TEST:I

.field public static PROXIMITY_SENSOR_TEST:I

.field public static SPEAKER_LEFT_TEST:I

.field public static SPEAKER_RIGHT_TEST:I

.field public static SPEAKER_TEST:I

.field public static TSP_DOT_TEST:I

.field public static TSP_GRID_TEST:I

.field public static UNKOWN_TEST:I

.field public static VIBRATION_TEST:I

.field public static VT_CAMERA_TEST:I


# instance fields
.field private mBluePosition:I

.field private mContext:Landroid/content/Context;

.field private mDefaultTestName:[Ljava/lang/String;

.field private mDefaultTestNum:I

.field public mSelfTestMenuList:Ljava/lang/String;

.field private mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

.field mTestItemTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    .line 45
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 46
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 47
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 48
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 49
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 50
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 51
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 52
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 53
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 54
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 55
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 56
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 57
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 59
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    .line 60
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 38
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mBluePosition:I

    .line 61
    const-string v0, "Unknown"

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    .line 65
    const-string v0, "SELFTEST_MENU_LIST"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    .line 67
    const-string v0, "TWOSPEAKERNOPROXI"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForTwoSpeakerNoProximity()V

    .line 91
    :goto_0
    new-instance v0, Lcom/sec/android/app/selftestmode/SelfTestMode;

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/selftestmode/SelfTestMode;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

    .line 92
    return-void

    .line 69
    :cond_0
    const-string v0, "TWOSPEAKER"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForAriesForTwoSpeaker()V

    goto :goto_0

    .line 71
    :cond_1
    const-string v0, "EXCEPTIONMELODYANDPROXI"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_Melody_Proximity()V

    goto :goto_0

    .line 73
    :cond_2
    const-string v0, "self_test_list_no_light_sensor"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForNoLightSensor()V

    goto :goto_0

    .line 75
    :cond_3
    const-string v0, "self_test_list_exception_vtcam_light"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_VTCam_Light()V

    goto :goto_0

    .line 77
    :cond_4
    const-string v0, "self_test_list_exception_vtcam_light_proximity"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_VTCam_Light_Proximity()V

    goto :goto_0

    .line 79
    :cond_5
    const-string v0, "self_test_list_exception_light_sensor_proximity"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_Light_Proximity()V

    goto :goto_0

    .line 81
    :cond_6
    const-string v0, "self_test_list_excepting_proximity"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_Proximity()V

    goto :goto_0

    .line 83
    :cond_7
    const-string v0, "self_test_list_excepting_vibration_proximity"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForExcepting_Vibration_Proximity()V

    goto :goto_0

    .line 85
    :cond_8
    const-string v0, "self_test_list_twospeakers_excepting_vibration"

    iget-object v1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMenuList:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setFortwospeakerExcepting_Vibration()V

    goto/16 :goto_0

    .line 88
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setForAries()V

    goto/16 :goto_0
.end method

.method private setForAries()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 97
    return-void
.end method

.method private setForAriesForTwoSpeaker()V
    .locals 3

    .prologue
    .line 100
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForAriesForTwoSpeaker"

    const-string v2, "setForAriesForTwoSpeaker()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 102
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 103
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 104
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    .line 105
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    .line 106
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 107
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 108
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 109
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 110
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 111
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 112
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 113
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 114
    const/16 v0, 0xc

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 115
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 119
    return-void
.end method

.method private setForExcepting_Light_Proximity()V
    .locals 3

    .prologue
    .line 200
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_Light_Proximity"

    const-string v2, "setForExcepting_Light_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 202
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 207
    return-void
.end method

.method private setForExcepting_Melody_Proximity()V
    .locals 3

    .prologue
    .line 147
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_Melody_Proximity"

    const-string v2, "setForExcepting_Melody_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 149
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 150
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    .line 151
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    .line 152
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 153
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 154
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 155
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 156
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 157
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 158
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 159
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 160
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 161
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 162
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 166
    return-void
.end method

.method private setForExcepting_Proximity()V
    .locals 3

    .prologue
    .line 210
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_Proximity"

    const-string v2, "setForExcepting_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 212
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 216
    return-void
.end method

.method private setForExcepting_VTCam_Light()V
    .locals 3

    .prologue
    .line 178
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "self_test_list_exception_vtcam_light"

    const-string v2, "setForExcepting_VTCam_Light()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 181
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 182
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 183
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 184
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 185
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 186
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 187
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 188
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 189
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 190
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 192
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 193
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 197
    return-void
.end method

.method private setForExcepting_VTCam_Light_Proximity()V
    .locals 3

    .prologue
    .line 219
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_VTCam_Light_Proximity"

    const-string v2, "setForExcepting_VTCam_Light_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 221
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 222
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 223
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 224
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 225
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 226
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 227
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 228
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 229
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 230
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 231
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 232
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 236
    return-void
.end method

.method private setForExcepting_Vibration_Proximity()V
    .locals 3

    .prologue
    .line 239
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_Vibration_Proximity"

    const-string v2, "setForExcepting_Vibration_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 241
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 242
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 243
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 244
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 245
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 246
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 247
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 248
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 249
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 250
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 251
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 252
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 256
    return-void
.end method

.method private setForNoLightSensor()V
    .locals 3

    .prologue
    .line 169
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "self_test_list_no_light_sensor"

    const-string v2, "setForNoLightSensor()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 174
    return-void
.end method

.method private setForTwoSpeakerNoProximity()V
    .locals 3

    .prologue
    .line 123
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForTwoSpeakerNoProximity"

    const-string v2, "setForAriesForTwoSpeakerNoProximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 125
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 126
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 127
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    .line 128
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    .line 129
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 130
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 131
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 132
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 133
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 134
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 135
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 136
    const/16 v0, 0xb

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 137
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 138
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 142
    return-void
.end method

.method private setFortwospeakerExcepting_Vibration()V
    .locals 3

    .prologue
    .line 259
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "setForExcepting_Vibration_Proximity"

    const-string v2, "setForExcepting_Vibration_Proximity()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .line 261
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    .line 262
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VIBRATION_TEST:I

    .line 263
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_LEFT_TEST:I

    .line 264
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_RIGHT_TEST:I

    .line 265
    const/4 v0, 0x3

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    .line 266
    const/4 v0, 0x4

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->CAMERA_TEST:I

    .line 267
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->VT_CAMERA_TEST:I

    .line 268
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->BLUETOOTH_TEST:I

    .line 269
    const/4 v0, 0x7

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_DOT_TEST:I

    .line 270
    const/16 v0, 0x8

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->TSP_GRID_TEST:I

    .line 271
    const/16 v0, 0x9

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->ACC_SENSOR_TEST:I

    .line 272
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->LIGHT_SENSOR_TEST:I

    .line 273
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->PROXIMITY_SENSOR_TEST:I

    .line 274
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->EMPTY_TEST:I

    sput v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->SPEAKER_TEST:I

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f05000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    .line 278
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 332
    iget v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestNum:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 336
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 281
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 285
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03005a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 287
    .local v0, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setId(I)V

    .line 288
    const v2, 0x7f0a017d

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mTestItemTextView:Landroid/widget/TextView;

    .line 289
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, p1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 290
    .local v1, "numbering":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mTestItemTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mDefaultTestName:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mBluePosition:I

    if-ne p1, v2, :cond_0

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mTestItemTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 298
    :goto_0
    return-object v0

    .line 295
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mTestItemTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setBlue(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 327
    iget v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mBluePosition:I

    if-ne v0, p1, :cond_0

    sget p1, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->UNKOWN_TEST:I

    .end local p1    # "pos":I
    :cond_0
    iput p1, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mBluePosition:I

    .line 328
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->notifyDataSetChanged()V

    .line 329
    return-void
.end method

.method public startTest(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    .line 302
    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->MELODY_TEST:I

    if-lt p1, v0, :cond_0

    sget v0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->DIMMING_TEST:I

    if-gt p1, v0, :cond_0

    .line 303
    const-string v0, "SelfTestListAdaptor"

    const-string v1, "startTest"

    const-string v2, "select melody test ~ dimming test"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0, p1}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setBlue(I)V

    .line 309
    :goto_0
    const-string v0, "TWOSPEAKER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/selftestmode/SelfTestMode;->selfTestForTwoSpaker(I)V

    .line 314
    :goto_1
    return-void

    .line 306
    :cond_0
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setBlue(I)V

    goto :goto_0

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/selftestmode/SelfTestMode;->selfTest(I)V

    goto :goto_1
.end method

.method public stopTest()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->setBlue(I)V

    .line 319
    const-string v0, "TWOSPEAKER"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

    invoke-virtual {v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOffForTwoSpeaker()V

    .line 324
    :goto_0
    return-void

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/SelfTestListAdapter;->mSelfTestMode:Lcom/sec/android/app/selftestmode/SelfTestMode;

    invoke-virtual {v0}, Lcom/sec/android/app/selftestmode/SelfTestMode;->testOff()V

    goto :goto_0
.end method
