.class public Lcom/sec/android/app/status/touch_key_sensitivity_view;
.super Landroid/view/View;
.source "touch_key_sensitivity_view.java"


# instance fields
.field private final GRAPH_SCALING_COOR:I

.field private final INCREASING_COOR:I

.field private INDEX_1KEY:I

.field private INDEX_2KEY:I

.field private INDEX_3KEY:I

.field private INDEX_4KEY:I

.field private INDEX_5KEY:I

.field private INDEX_6KEY:I

.field private INDEX_BACK:I

.field private INDEX_HOME:I

.field private INDEX_MENU:I

.field private INDEX_RECENT:I

.field private final INIT_COOR_X:I

.field private final INIT_COOR_Y:I

.field private final TEXT_SCALING_COOR:F

.field private context:Landroid/content/Context;

.field private isFiveKey:Z

.field private isFourKey:Z

.field private isSixKey:Z

.field private isbackkey:Z

.field private isdummykey1:Z

.field private isdummykey3:Z

.field private isdummykey4:Z

.field private isdummykey5:Z

.field private isdummykey6:Z

.field private ishomekey:Z

.field private ismenukey:Z

.field private isrecentkey:Z

.field private lineScale_tune:I

.field private listSize:I

.field private mBaseLinePaint:Landroid/graphics/Paint;

.field private mDashEffects:Landroid/graphics/DashPathEffect;

.field private mEffects:Landroid/graphics/PathEffect;

.field private mPaintBack:Landroid/graphics/Paint;

.field private mPaintDummy1:Landroid/graphics/Paint;

.field private mPaintDummy3:Landroid/graphics/Paint;

.field private mPaintDummy4:Landroid/graphics/Paint;

.field private mPaintDummy5:Landroid/graphics/Paint;

.field private mPaintDummy6:Landroid/graphics/Paint;

.field private mPaintHome:Landroid/graphics/Paint;

.field private mPaintMenu:Landroid/graphics/Paint;

.field private mPaintRecent:Landroid/graphics/Paint;

.field private mPaintThreshold:Landroid/graphics/Paint;

.field private mPaintThreshold0:Landroid/graphics/Paint;

.field private mPaintThreshold1:Landroid/graphics/Paint;

.field private mPaintThreshold2:Landroid/graphics/Paint;

.field private mPaintThreshold3:Landroid/graphics/Paint;

.field private mPaintThreshold4:Landroid/graphics/Paint;

.field private mPaintThreshold5:Landroid/graphics/Paint;

.field private mPaintThreshold6:Landroid/graphics/Paint;

.field private mPathBack:Landroid/graphics/Path;

.field private mPathDummy1:Landroid/graphics/Path;

.field private mPathDummy3:Landroid/graphics/Path;

.field private mPathDummy4:Landroid/graphics/Path;

.field private mPathDummy5:Landroid/graphics/Path;

.field private mPathDummy6:Landroid/graphics/Path;

.field private mPathHome:Landroid/graphics/Path;

.field private mPathMenu:Landroid/graphics/Path;

.field private mPathRecent:Landroid/graphics/Path;

.field private mPathThreshold:Landroid/graphics/Path;

.field private mPathThreshold0:Landroid/graphics/Path;

.field private mPathThreshold1:Landroid/graphics/Path;

.field private mPathThreshold2:Landroid/graphics/Path;

.field private mPathThreshold3:Landroid/graphics/Path;

.field private mPathThreshold4:Landroid/graphics/Path;

.field private mPathThreshold5:Landroid/graphics/Path;

.field private mReduction:F

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTextPaint:Landroid/graphics/Paint;

.field private mValueBack:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueDummy1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueDummy3:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueDummy4:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueDummy5:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueDummy6:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueHome:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueMenu:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueRecent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold1:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold2:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold3:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold4:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mValueThreshold5:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private text_scale:F

.field private tuneYscale:I

.field private windowManager:Landroid/view/WindowManager;

.field private x_scale:F

.field private y_scale:F

.field zeroPositionPixcel:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 198
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_X:I

    .line 26
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_Y:I

    .line 27
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INCREASING_COOR:I

    .line 28
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->GRAPH_SCALING_COOR:I

    .line 29
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->TEXT_SCALING_COOR:F

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    .line 38
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    .line 39
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    .line 40
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    .line 42
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    .line 43
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    .line 44
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    .line 45
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    .line 46
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    .line 48
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    .line 102
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    .line 103
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    .line 104
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    .line 120
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    .line 121
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_HOME:I

    .line 122
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    .line 123
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_RECENT:I

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    .line 125
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_2KEY:I

    .line 126
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    .line 127
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_4KEY:I

    .line 128
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_5KEY:I

    .line 129
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_6KEY:I

    .line 131
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    .line 132
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    .line 133
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->lineScale_tune:I

    .line 134
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    .line 199
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->init(Landroid/content/Context;)V

    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 203
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_X:I

    .line 26
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_Y:I

    .line 27
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INCREASING_COOR:I

    .line 28
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->GRAPH_SCALING_COOR:I

    .line 29
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->TEXT_SCALING_COOR:F

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    .line 38
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    .line 39
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    .line 40
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    .line 42
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    .line 43
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    .line 44
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    .line 45
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    .line 46
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    .line 48
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    .line 102
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    .line 103
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    .line 104
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    .line 120
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    .line 121
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_HOME:I

    .line 122
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    .line 123
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_RECENT:I

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    .line 125
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_2KEY:I

    .line 126
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    .line 127
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_4KEY:I

    .line 128
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_5KEY:I

    .line 129
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_6KEY:I

    .line 131
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    .line 132
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    .line 133
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->lineScale_tune:I

    .line 134
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    .line 204
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->init(Landroid/content/Context;)V

    .line 205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 208
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/16 v0, 0x3c

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_X:I

    .line 26
    const/16 v0, 0x258

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INIT_COOR_Y:I

    .line 27
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INCREASING_COOR:I

    .line 28
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->GRAPH_SCALING_COOR:I

    .line 29
    const v0, 0x42654ca3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->TEXT_SCALING_COOR:F

    .line 37
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    .line 38
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    .line 39
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    .line 40
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    .line 42
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    .line 43
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    .line 44
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    .line 45
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    .line 46
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    .line 48
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    .line 49
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 50
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 51
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    .line 52
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    .line 53
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    .line 54
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    .line 102
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    .line 103
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    .line 104
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    .line 106
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    .line 108
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    .line 109
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 112
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 113
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    .line 115
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    .line 116
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    .line 117
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    .line 118
    iput-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    .line 120
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    .line 121
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_HOME:I

    .line 122
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    .line 123
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_RECENT:I

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    .line 125
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_2KEY:I

    .line 126
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    .line 127
    const/4 v0, 0x7

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_4KEY:I

    .line 128
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_5KEY:I

    .line 129
    const/16 v0, 0x9

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_6KEY:I

    .line 131
    iput v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    .line 132
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    .line 133
    iput v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->lineScale_tune:I

    .line 134
    iput v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    .line 209
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->init(Landroid/content/Context;)V

    .line 210
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v8, -0x1000000

    const v7, 0x40533333    # 3.3f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v4, 0x40400000    # 3.0f

    .line 214
    iput-object p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->context:Landroid/content/Context;

    .line 215
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->windowManager:Landroid/view/WindowManager;

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    .line 217
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    .line 218
    const-string v0, "touch_key_sensitivity_view"

    const-string v1, "init"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mScreenWidth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mScreenHeight = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0x320

    if-ne v0, v1, :cond_1

    .line 221
    invoke-virtual {p0, v6, v6, v6}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    .line 234
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xa

    int-to-float v0, v0

    const/high16 v1, 0x42700000    # 60.0f

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    div-float/2addr v0, v6

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->listSize:I

    .line 235
    new-instance v0, Landroid/graphics/CornerPathEffect;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-direct {v0, v1}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    .line 236
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mDashEffects:Landroid/graphics/DashPathEffect;

    .line 239
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    .line 240
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    .line 241
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    .line 242
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    .line 243
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    .line 244
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    .line 245
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    .line 247
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    .line 248
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    .line 249
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    .line 250
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    .line 251
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    .line 253
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    .line 254
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    .line 255
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    .line 256
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    .line 257
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    .line 258
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    .line 259
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold6:Landroid/graphics/Paint;

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mDashEffects:Landroid/graphics/DashPathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    const-string v1, "#AAF5DEB3"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    const-string v1, "#AAFF4500"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    const-string v1, "#AAFF4500"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    const-string v1, "#AA083725"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 320
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    const v1, -0xff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mEffects:Landroid/graphics/PathEffect;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41700000    # 15.0f

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41500000    # 13.0f

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->invalidate()V

    .line 353
    const-string v0, "touch_key_sensitivity_view"

    const-string v1, "init()"

    const-string v2, "init Okay!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void

    .line 222
    :cond_1
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0xf0

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0x140

    if-ne v0, v1, :cond_2

    .line 223
    const/high16 v0, 0x3f000000    # 0.5f

    const v1, 0x3ecccccd    # 0.4f

    const v2, 0x3f333333    # 0.7f

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    goto/16 :goto_0

    .line 224
    :cond_2
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0x140

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_3

    .line 225
    const v0, 0x3f19999a    # 0.6f

    const v1, 0x3f19999a    # 0.6f

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    goto/16 :goto_0

    .line 226
    :cond_3
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0x5a0

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0xa00

    if-ne v0, v1, :cond_4

    .line 227
    invoke-virtual {p0, v4, v7, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    goto/16 :goto_0

    .line 228
    :cond_4
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0x640

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0xa00

    if-ne v0, v1, :cond_5

    .line 229
    const v0, 0x404ccccd    # 3.2f

    invoke-virtual {p0, v0, v7, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    goto/16 :goto_0

    .line 230
    :cond_5
    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    const/16 v1, 0x5fc

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenHeight:I

    const/16 v1, 0xa00

    if-ne v0, v1, :cond_0

    .line 231
    const v0, 0x404ccccd    # 3.2f

    invoke-virtual {p0, v0, v7, v4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setScale(FFF)V

    goto/16 :goto_0

    .line 236
    nop

    :array_0
    .array-data 4
        0x40a00000    # 5.0f
        0x40a00000    # 5.0f
    .end array-data
.end method

.method private setPath()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x42700000    # 60.0f

    .line 953
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    if-eqz v1, :cond_0

    .line 954
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 955
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    .line 958
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    if-eqz v1, :cond_1

    .line 959
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 960
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    .line 963
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_2

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 965
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    .line 968
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_3

    .line 969
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 970
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    .line 973
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_4

    .line 974
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 975
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    .line 977
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_5

    .line 978
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 979
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    .line 981
    :cond_5
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    if-eqz v1, :cond_6

    .line 982
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 983
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    .line 985
    :cond_6
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    if-eqz v1, :cond_7

    .line 986
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 987
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    .line 989
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    if-eqz v1, :cond_8

    .line 990
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 991
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    .line 994
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 995
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    .line 997
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    if-eqz v1, :cond_9

    .line 998
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1000
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    if-eqz v1, :cond_a

    .line 1001
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1003
    :cond_a
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_b

    .line 1004
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x44160000    # 600.0f

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1006
    :cond_b
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_c

    .line 1007
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1010
    :cond_c
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_d

    .line 1011
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1013
    :cond_d
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_e

    .line 1014
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1016
    :cond_e
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    if-eqz v1, :cond_f

    .line 1017
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1019
    :cond_f
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    if-eqz v1, :cond_10

    .line 1020
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1022
    :cond_10
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    if-eqz v1, :cond_11

    .line 1023
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1026
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1029
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1b

    .line 1030
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    if-eqz v1, :cond_12

    .line 1031
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1033
    :cond_12
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    if-eqz v1, :cond_13

    .line 1034
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1036
    :cond_13
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_14

    .line 1037
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1039
    :cond_14
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_15

    .line 1040
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1043
    :cond_15
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_16

    .line 1044
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1047
    :cond_16
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_17

    .line 1048
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1050
    :cond_17
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    if-eqz v1, :cond_18

    .line 1051
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1053
    :cond_18
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    if-eqz v1, :cond_19

    .line 1054
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1056
    :cond_19
    iget-boolean v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    if-eqz v1, :cond_1a

    .line 1057
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1060
    :cond_1a
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1029
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 1064
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->invalidate()V

    .line 1065
    return-void
.end method

.method private setPathFiveKey()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x42700000    # 60.0f

    .line 844
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 845
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 846
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 847
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 849
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    .line 850
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 851
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    .line 852
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 853
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    .line 855
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 857
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 859
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 861
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 863
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 866
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 867
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 869
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 871
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 875
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 866
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 879
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->invalidate()V

    .line 880
    return-void
.end method

.method private setPathFourKey()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x42700000    # 60.0f

    .line 930
    const-string v1, "touch_key_sensitivity_view"

    const-string v2, "setPathFourKey"

    const-string v3, "before setPathFourKey "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 932
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 933
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 934
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 935
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 937
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 939
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 942
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 945
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 942
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 949
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->invalidate()V

    .line 950
    return-void
.end method

.method private setPathSixKey()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x42700000    # 60.0f

    .line 883
    const-string v1, "touch_key_sensitivity_view"

    const-string v2, "setPathSixKey"

    const-string v3, "before setPathSixKey "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 886
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    .line 887
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 888
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 890
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 892
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    .line 893
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 894
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    .line 895
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 896
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    .line 898
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 900
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 902
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 904
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 906
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 908
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 911
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 912
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 914
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 916
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 918
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 920
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 922
    iget-object v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v7

    float-to-int v1, v1

    mul-int/lit8 v3, v0, 0x1

    add-int/2addr v1, v3

    int-to-float v3, v1

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    iget v4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget v5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    iget v6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    div-float/2addr v5, v6

    div-float/2addr v1, v5

    sub-float v1, v4, v1

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 911
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 926
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->invalidate()V

    .line 927
    return-void
.end method


# virtual methods
.method public addValue(FFFFF)V
    .locals 3
    .param p1, "back"    # F
    .param p2, "home"    # F
    .param p3, "recent"    # F
    .param p4, "menu"    # F
    .param p5, "threshold"    # F

    .prologue
    const/4 v2, 0x0

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 832
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->listSize:I

    if-le v0, v1, :cond_0

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 836
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 837
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 840
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setPath()V

    .line 841
    return-void
.end method

.method public addValue(FFFFFIIIIII)V
    .locals 3
    .param p1, "back"    # F
    .param p2, "home"    # F
    .param p3, "recent"    # F
    .param p4, "menu"    # F
    .param p5, "threshold"    # F
    .param p6, "dummy1"    # I
    .param p7, "dummy3"    # I
    .param p8, "th0"    # I
    .param p9, "th1"    # I
    .param p10, "th2"    # I
    .param p11, "th3"    # I

    .prologue
    const/4 v2, 0x0

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    int-to-float v1, p6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    int-to-float v1, p7

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    int-to-float v1, p8

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    int-to-float v1, p9

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    int-to-float v1, p10

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    int-to-float v1, p11

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->listSize:I

    if-le v0, v1, :cond_0

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 774
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setPathFourKey()V

    .line 776
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFF)V

    .line 777
    return-void
.end method

.method public addValue(FFFFFIIIIIIII)V
    .locals 2
    .param p1, "back"    # F
    .param p2, "home"    # F
    .param p3, "recent"    # F
    .param p4, "menu"    # F
    .param p5, "threshold"    # F
    .param p6, "dummy1"    # I
    .param p7, "dummy3"    # I
    .param p8, "dummy5"    # I
    .param p9, "th0"    # I
    .param p10, "th1"    # I
    .param p11, "th2"    # I
    .param p12, "th3"    # I
    .param p13, "th4"    # I

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    int-to-float v1, p6

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    int-to-float v1, p7

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    int-to-float v1, p8

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 729
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    int-to-float v1, p9

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    int-to-float v1, p10

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 731
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    int-to-float v1, p11

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 732
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    int-to-float v1, p12

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    int-to-float v1, p13

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->listSize:I

    if-le v0, v1, :cond_0

    .line 736
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 737
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 738
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 741
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 743
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 744
    iget-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 747
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setPathFiveKey()V

    .line 749
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->addValue(FFFFF)V

    .line 750
    return-void
.end method

.method public addValue(FFFFFIIIIIIIIII)V
    .locals 3
    .param p1, "back"    # F
    .param p2, "home"    # F
    .param p3, "recent"    # F
    .param p4, "menu"    # F
    .param p5, "threshold"    # F
    .param p6, "dummy1"    # I
    .param p7, "dummy3"    # I
    .param p8, "dummy4"    # I
    .param p9, "dummy6"    # I
    .param p10, "th0"    # I
    .param p11, "th1"    # I
    .param p12, "th2"    # I
    .param p13, "th3"    # I
    .param p14, "th4"    # I
    .param p15, "th5"    # I

    .prologue
    .line 784
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 785
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    int-to-float v2, p6

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    int-to-float v2, p7

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    int-to-float v2, p8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 793
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    int-to-float v2, p9

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 795
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    int-to-float v2, p10

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 796
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    int-to-float v2, p11

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    int-to-float v2, p12

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    move/from16 v0, p13

    int-to-float v2, v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    move/from16 v0, p14

    int-to-float v2, v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 800
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    move/from16 v0, p15

    int-to-float v2, v0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 802
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->listSize:I

    if-le v1, v2, :cond_0

    .line 803
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 804
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 805
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 806
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 807
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 809
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 810
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 811
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 812
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 814
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 816
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 817
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 818
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 819
    iget-object v1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 821
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setPath()V

    .line 822
    invoke-direct {p0}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setPathSixKey()V

    .line 823
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 28
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 359
    const/4 v8, 0x0

    .line 360
    .local v8, "textBack":F
    const/4 v14, 0x0

    .line 361
    .local v14, "textHome":F
    const/16 v16, 0x0

    .line 362
    .local v16, "textRecent":F
    const/4 v15, 0x0

    .line 364
    .local v15, "textMenu":F
    const/4 v9, 0x0

    .line 365
    .local v9, "textDummy1":F
    const/4 v10, 0x0

    .line 366
    .local v10, "textDummy3":F
    const/4 v11, 0x0

    .line 367
    .local v11, "textDummy4":F
    const/4 v12, 0x0

    .line 368
    .local v12, "textDummy5":F
    const/4 v13, 0x0

    .line 370
    .local v13, "textDummy6":F
    const/16 v17, 0x0

    .line 372
    .local v17, "textThreshold":F
    const/16 v18, 0x0

    .line 373
    .local v18, "textThreshold0":F
    const/16 v19, 0x0

    .line 374
    .local v19, "textThreshold1":F
    const/16 v20, 0x0

    .line 375
    .local v20, "textThreshold2":F
    const/16 v21, 0x0

    .line 376
    .local v21, "textThreshold3":F
    const/16 v22, 0x0

    .line 377
    .local v22, "textThreshold4":F
    const/16 v23, 0x0

    .line 379
    .local v23, "textThreshold5":F
    const/16 v27, 0x0

    .line 380
    .local v27, "x_axis_tune":I
    const/16 v25, 0x0

    .line 381
    .local v25, "tuneYscale_50":I
    const/16 v24, 0x0

    .line 384
    .local v24, "tuneYscale_100":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueBack:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v8

    .line 385
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueHome:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v14

    .line 386
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueRecent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v16

    .line 387
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueMenu:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v15

    .line 388
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v17

    .line 390
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-eqz v1, :cond_17

    .line 391
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 392
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 393
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy5:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 395
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 396
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 397
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 398
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v21

    .line 399
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v22

    .line 428
    :cond_0
    :goto_0
    const/4 v1, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 431
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    if-eqz v1, :cond_1

    .line 432
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-eqz v1, :cond_19

    .line 433
    const-string v1, "Back"

    const/high16 v2, 0x43e60000    # 460.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 434
    const v1, 0x4405c000    # 535.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x42fa0000    # 125.0f

    const/high16 v4, 0x43e60000    # 460.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 435
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x4405c000    # 535.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 446
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    if-eqz v1, :cond_2

    .line 447
    const-string v1, "Home : "

    const/high16 v2, 0x43000000    # 128.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 448
    const/high16 v1, 0x43410000    # 193.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x43730000    # 243.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 450
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v14

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_HOME:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x43410000    # 193.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 455
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_3

    .line 456
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_1a

    .line 474
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_4

    .line 475
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-eqz v1, :cond_1c

    .line 476
    const-string v1, "| Menu"

    const/high16 v2, 0x43200000    # 160.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 477
    const/high16 v1, 0x436b0000    # 235.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x438e8000    # 285.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 478
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v15

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x436b0000    # 235.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 489
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    if-eqz v1, :cond_1d

    .line 490
    const/16 v26, 0x0

    .line 492
    .local v26, "xOffset":I
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_5

    .line 493
    const-string v1, "d1"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 494
    const/16 v1, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 495
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 498
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_6

    .line 499
    add-int/lit8 v26, v26, 0x50

    .line 500
    const-string v1, "| Menu"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 501
    const/16 v1, 0x73

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xa5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 502
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v15

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x73

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 505
    :cond_6
    add-int/lit8 v26, v26, 0x50

    .line 506
    const-string v1, "| Back"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 507
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 508
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 510
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_7

    .line 511
    add-int/lit8 v26, v26, 0x50

    .line 512
    const-string v1, "| d2"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 513
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 517
    :cond_7
    const/16 v26, 0x0

    .line 518
    const-string v1, "d1(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 519
    const/16 v1, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 520
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 522
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_8

    .line 523
    add-int/lit8 v26, v26, 0x50

    .line 524
    const-string v1, "| Menu(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 525
    const/16 v1, 0x73

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xa5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 526
    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x73

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 529
    :cond_8
    add-int/lit8 v26, v26, 0x50

    .line 530
    const-string v1, "| Back(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 531
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 532
    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 534
    add-int/lit8 v26, v26, 0x50

    .line 535
    const-string v1, "| d2(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 536
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 537
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 539
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_9

    .line 540
    add-int/lit8 v26, v26, 0x50

    .line 541
    const-string v1, "| Recent(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 542
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 543
    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 663
    .end local v26    # "xOffset":I
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    if-eqz v1, :cond_a

    .line 664
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathBack:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 666
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    if-eqz v1, :cond_b

    .line 667
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathHome:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintHome:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 669
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    if-eqz v1, :cond_c

    .line 670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathRecent:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 672
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    if-eqz v1, :cond_d

    .line 673
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathMenu:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 675
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_e

    .line 676
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy1:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 678
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_f

    .line 679
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy3:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 681
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    if-eqz v1, :cond_10

    .line 682
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy4:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 684
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    if-eqz v1, :cond_11

    .line 685
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy5:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 687
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    if-eqz v1, :cond_12

    .line 688
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathDummy6:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 690
    :cond_12
    const-string v1, "touch_key_sensitivity_view"

    const-string v2, "onDraw"

    const-string v3, "before drawLine"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-nez v1, :cond_13

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_27

    .line 692
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold0:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 693
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold1:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 694
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold2:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 695
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold3:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 696
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-nez v1, :cond_14

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_15

    :cond_14
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold4:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 697
    :cond_15
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_16

    .line 698
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold5:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold5:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 704
    :cond_16
    :goto_5
    const/high16 v1, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x44160000    # 600.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 707
    const/high16 v1, 0x42700000    # 60.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mScreenWidth:I

    add-int/lit8 v1, v1, -0xa

    add-int v1, v1, v27

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->zeroPositionPixcel:I

    rsub-int v1, v1, 0x258

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 710
    const/high16 v1, 0x43c80000    # 400.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 712
    const/high16 v1, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 714
    const/high16 v1, 0x43480000    # 200.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43c80000    # 400.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    move/from16 v0, v24

    int-to-float v4, v0

    sub-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 716
    const/high16 v1, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43fa0000    # 500.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->tuneYscale:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    move/from16 v0, v25

    int-to-float v4, v0

    sub-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 718
    const-string v1, "touch_key_sensitivity_view"

    const-string v2, "onDraw"

    const-string v3, "after drawLine"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    return-void

    .line 400
    :cond_17
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_18

    .line 401
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 402
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 403
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy4:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 404
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy6:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v13

    .line 406
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 407
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 408
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 409
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v21

    .line 410
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold4:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v22

    .line 411
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold5:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v23

    goto/16 :goto_0

    .line 412
    :cond_18
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    if-eqz v1, :cond_0

    .line 413
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v9

    .line 414
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueDummy3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v10

    .line 416
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold0:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v18

    .line 417
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold1:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v19

    .line 418
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v20

    .line 419
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mValueThreshold3:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v21

    goto/16 :goto_0

    .line 421
    :catch_0
    move-exception v7

    .line 423
    .local v7, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v1, "touch_key_sensitivity_view"

    const-string v2, "onDraw"

    const-string v3, "Exception ! "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {v7}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto/16 :goto_0

    .line 436
    .end local v7    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_19
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-nez v1, :cond_1

    .line 439
    const-string v1, "Back : "

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 440
    const/high16 v1, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x42fa0000    # 125.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 441
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 458
    :cond_1a
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    if-eqz v1, :cond_1b

    .line 459
    const-string v1, "| Recent"

    const/high16 v2, 0x43700000    # 240.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 460
    const v1, 0x43898000    # 275.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x43a28000    # 325.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 462
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v16

    float-to-int v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_RECENT:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x43898000    # 275.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 465
    :cond_1b
    const-string v1, "Recent : "

    const/high16 v2, 0x43760000    # 246.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 466
    const/high16 v1, 0x43a00000    # 320.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x43b90000    # 370.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintRecent:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 468
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v16

    float-to-int v2, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_RECENT:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x43a00000    # 320.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 479
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-nez v1, :cond_4

    .line 482
    const-string v1, "Menu : "

    const/high16 v2, 0x43b60000    # 364.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 483
    const/high16 v1, 0x43d50000    # 426.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x43ee0000    # 476.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42340000    # 45.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 484
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v15

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x43d50000    # 426.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 545
    :cond_1d
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    if-eqz v1, :cond_21

    .line 547
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_1e

    .line 548
    const-string v1, "dummy1"

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 549
    const/high16 v1, 0x42aa0000    # 85.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 550
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x42aa0000    # 85.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 553
    :cond_1e
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_1f

    .line 554
    const-string v1, "| dummy3"

    const/high16 v2, 0x439b0000    # 310.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 555
    const v1, 0x43c08000    # 385.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x43d98000    # 435.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x43c08000    # 385.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    const/high16 v4, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 559
    :cond_1f
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    if-eqz v1, :cond_20

    .line 560
    const-string v1, "| dummy5"

    const v2, 0x44188000    # 610.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 561
    const v1, 0x442b4000    # 685.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x4437c000    # 735.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy5:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 562
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v12

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_5KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x442b4000    # 685.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000    # 70.0f

    const/high16 v4, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 566
    :cond_20
    const-string v1, "dummy1(Thres)"

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 567
    const/high16 v1, 0x42aa0000    # 85.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 568
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x42aa0000    # 85.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 570
    const-string v1, "| Menu(Thres)"

    const/high16 v2, 0x43200000    # 160.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 571
    const/high16 v1, 0x436b0000    # 235.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x438e8000    # 285.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 572
    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x436b0000    # 235.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 574
    const-string v1, "| dummy3(Thres) "

    const/high16 v2, 0x439b0000    # 310.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 575
    const v1, 0x43c08000    # 385.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x43d98000    # 435.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 576
    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const v2, 0x43c08000    # 385.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 578
    const-string v1, "| Back(Thres)"

    const/high16 v2, 0x43e60000    # 460.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 579
    const v1, 0x4405c000    # 535.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x44124000    # 585.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 580
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const v2, 0x4405c000    # 535.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 582
    const-string v1, "| dummy5(Thres)"

    const v2, 0x44188000    # 610.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 583
    const v1, 0x442b4000    # 685.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const v1, 0x4437c000    # 735.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 584
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const v2, 0x442b4000    # 685.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 585
    :cond_21
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    if-eqz v1, :cond_26

    .line 587
    const/16 v26, 0x0

    .line 589
    .restart local v26    # "xOffset":I
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    if-eqz v1, :cond_22

    .line 590
    const-string v1, "d1"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 591
    const/16 v1, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 592
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_1KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 594
    :cond_22
    add-int/lit8 v26, v26, 0x50

    .line 595
    const-string v1, "| Menu"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 596
    const/16 v1, 0x73

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xa5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintMenu:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 597
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v15

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_MENU:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x73

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 599
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    if-eqz v1, :cond_23

    .line 600
    add-int/lit8 v26, v26, 0x50

    .line 601
    const-string v1, "| d3"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 602
    const/16 v1, 0xc3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xf5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 603
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v10

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_3KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 606
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    if-eqz v1, :cond_24

    .line 607
    add-int/lit8 v26, v26, 0x50

    .line 608
    const-string v1, "| d4"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 609
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy4:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 610
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v11

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_4KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 613
    :cond_24
    add-int/lit8 v26, v26, 0x50

    .line 614
    const-string v1, "| Back"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 615
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintBack:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 616
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_BACK:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 619
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    if-eqz v1, :cond_25

    .line 620
    add-int/lit8 v26, v26, 0x50

    .line 621
    const-string v1, "| d6"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42480000    # 50.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 622
    add-int/lit8 v1, v26, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    add-int/lit8 v1, v26, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42820000    # 65.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintDummy6:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 623
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v2, v13

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(MX:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->INDEX_6KEY:I

    invoke-static {v2}, Lcom/sec/android/app/status/touch_key_sensitivity_graph;->getMaxData(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v26, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42b40000    # 90.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 626
    :cond_25
    const/16 v26, 0x0

    .line 627
    const-string v1, "d1(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 628
    const/16 v1, 0x23

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x55

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold0:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 629
    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x23

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 631
    add-int/lit8 v26, v26, 0x50

    .line 632
    const-string v1, "| Menu(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 633
    const/16 v1, 0x73

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xa5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold1:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 634
    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x73

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 636
    add-int/lit8 v26, v26, 0x50

    .line 637
    const-string v1, "| d3(Th) "

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 638
    const/16 v1, 0xc3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0xf5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 639
    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 641
    add-int/lit8 v26, v26, 0x50

    .line 642
    const-string v1, "| d4(Th) "

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 643
    const/16 v1, 0x113

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x145

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold2:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 644
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x113

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 646
    add-int/lit8 v26, v26, 0x50

    .line 647
    const-string v1, "| Back(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 648
    const/16 v1, 0x163

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x195

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold3:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 649
    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x163

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 651
    add-int/lit8 v26, v26, 0x50

    .line 652
    const-string v1, "| d6(Th)"

    move/from16 v0, v26

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42f00000    # 120.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 653
    const/16 v1, 0x1b3

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/16 v1, 0x1e5

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x43070000    # 135.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold4:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 654
    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x1b3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x43250000    # 165.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 656
    .end local v26    # "xOffset":I
    :cond_26
    const-string v1, "Thres : "

    const/high16 v2, 0x41200000    # 10.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mBaseLinePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 657
    const/high16 v1, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    const/high16 v1, 0x42be0000    # 95.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-float v3, v1

    const/high16 v1, 0x42fa0000    # 125.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v4, v1

    const/high16 v1, 0x42be0000    # 95.0f

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 659
    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, 0x42960000    # 75.0f

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    const/high16 v3, 0x42fa0000    # 125.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 701
    :cond_27
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPathThreshold:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mPaintThreshold:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_5
.end method

.method public setKeyEnabled(ZZZZ)V
    .locals 0
    .param p1, "backkey"    # Z
    .param p2, "homekey"    # Z
    .param p3, "recentkey"    # Z
    .param p4, "menukey"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isbackkey:Z

    .line 149
    iput-boolean p2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ishomekey:Z

    .line 150
    iput-boolean p3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isrecentkey:Z

    .line 151
    iput-boolean p4, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->ismenukey:Z

    .line 152
    return-void
.end method

.method public setKeyEnabledFiveKey(ZZZZZZZ)V
    .locals 1
    .param p1, "backkey"    # Z
    .param p2, "homekey"    # Z
    .param p3, "recentkey"    # Z
    .param p4, "menukey"    # Z
    .param p5, "dummykey1"    # Z
    .param p6, "dummykey3"    # Z
    .param p7, "dummykey5"    # Z

    .prologue
    const/4 v0, 0x1

    .line 156
    iput-boolean p5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 157
    iput-boolean p6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 158
    iput-boolean p7, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey5:Z

    .line 160
    if-ne p5, v0, :cond_0

    if-ne p6, v0, :cond_0

    if-ne p7, v0, :cond_0

    .line 161
    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFiveKey:Z

    .line 164
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabled(ZZZZ)V

    .line 165
    return-void
.end method

.method public setKeyEnabledFourKey(ZZZZZZ)V
    .locals 3
    .param p1, "backkey"    # Z
    .param p2, "homekey"    # Z
    .param p3, "recentkey"    # Z
    .param p4, "menukey"    # Z
    .param p5, "dummykey1"    # Z
    .param p6, "dummykey3"    # Z

    .prologue
    .line 185
    const-string v0, "touch_key_sensitivity_view"

    const-string v1, "setKeyEnabledFourKey()"

    const-string v2, "FourKey Enabled! "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iput-boolean p5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 187
    iput-boolean p6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 189
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isFourKey:Z

    .line 193
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabled(ZZZZ)V

    .line 194
    return-void
.end method

.method public setKeyEnabledSixKey(ZZZZZZZZ)V
    .locals 3
    .param p1, "backkey"    # Z
    .param p2, "homekey"    # Z
    .param p3, "recentkey"    # Z
    .param p4, "menukey"    # Z
    .param p5, "dummykey1"    # Z
    .param p6, "dummykey3"    # Z
    .param p7, "dummykey4"    # Z
    .param p8, "dummykey6"    # Z

    .prologue
    .line 170
    const-string v0, "touch_key_sensitivity_view"

    const-string v1, "setKeyEnabledSixKey()"

    const-string v2, "SixKey Enabled! "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iput-boolean p5, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey1:Z

    .line 172
    iput-boolean p6, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey3:Z

    .line 173
    iput-boolean p7, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey4:Z

    .line 174
    iput-boolean p8, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isdummykey6:Z

    .line 176
    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->isSixKey:Z

    .line 180
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/app/status/touch_key_sensitivity_view;->setKeyEnabled(ZZZZ)V

    .line 181
    return-void
.end method

.method public setReduction(F)V
    .locals 0
    .param p1, "rateOfReduction"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->mReduction:F

    .line 145
    return-void
.end method

.method public setScale(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "text"    # F

    .prologue
    .line 137
    iput p1, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->x_scale:F

    .line 138
    iput p2, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->y_scale:F

    .line 139
    iput p3, p0, Lcom/sec/android/app/status/touch_key_sensitivity_view;->text_scale:F

    .line 141
    return-void
.end method
