.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;
.super Lcom/sec/android/app/status/BatteryStatus_kor$Module;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Charger"
.end annotation


# static fields
.field private static mNameArray:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 534
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "charger"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "bq24260"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->mNameArray:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 536
    const-string v0, "Charger"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$Module;-><init>(Ljava/lang/String;)V

    .line 537
    return-void
.end method


# virtual methods
.method public checkDriverPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 560
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 561
    .local v4, "temp_file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 562
    .local v1, "folder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->checkValidPath(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 563
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 566
    .end local v1    # "folder":Ljava/io/File;
    :goto_1
    return-object v5

    .line 561
    .restart local v1    # "folder":Ljava/io/File;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 566
    .end local v1    # "folder":Ljava/io/File;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public checkValidPath(Ljava/lang/String;)Z
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 552
    sget-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->mNameArray:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 553
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 555
    .end local v3    # "name":Ljava/lang/String;
    :goto_1
    return v4

    .line 552
    .restart local v3    # "name":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 555
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected setModule()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 541
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->checkDriverPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 543
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "Current Now"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/current_now"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "Avg"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/current_avg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v7

    const/4 v2, 0x2

    new-instance v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v4, "Max"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/current_max"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 546
    new-array v1, v7, [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    new-instance v2, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    const-string v3, "Status"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/status"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5, v7}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    aput-object v2, v1, v8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;->addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 548
    :cond_0
    return-void
.end method
