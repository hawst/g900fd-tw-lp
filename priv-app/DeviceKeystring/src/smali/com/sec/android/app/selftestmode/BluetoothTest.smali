.class public Lcom/sec/android/app/selftestmode/BluetoothTest;
.super Landroid/app/Activity;
.source "BluetoothTest.java"


# instance fields
.field private final BLUETOOTH_SETTING_ACTIVITY:I

.field mBa:Landroid/bluetooth/BluetoothAdapter;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field mFullWl:Landroid/os/PowerManager$WakeLock;

.field mFullWlTemp:Landroid/os/PowerManager$WakeLock;

.field private mIsDiscoveringStart:Z

.field private mIsForceStop:Z

.field private mIsOn:Z

.field private mRequest:I

.field private mTv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->BLUETOOTH_SETTING_ACTIVITY:I

    .line 80
    new-instance v0, Lcom/sec/android/app/selftestmode/BluetoothTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/selftestmode/BluetoothTest$1;-><init>(Lcom/sec/android/app/selftestmode/BluetoothTest;)V

    iput-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/selftestmode/BluetoothTest;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/BluetoothTest;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsForceStop:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/selftestmode/BluetoothTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/BluetoothTest;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->stopTest()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/selftestmode/BluetoothTest;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/BluetoothTest;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mRequest:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/selftestmode/BluetoothTest;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/selftestmode/BluetoothTest;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->startDiscovery()V

    return-void
.end method

.method private btOnOff()V
    .locals 3

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "bt_on_off"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mRequest:I

    .line 101
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsOn:Z

    if-eqz v0, :cond_0

    .line 102
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->startDiscovery()V

    .line 118
    :goto_0
    return-void

    .line 104
    :cond_0
    iget v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mRequest:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 106
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->finish()V

    goto :goto_0

    .line 109
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mTv:Landroid/widget/TextView;

    const v1, 0x7f07024b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    goto :goto_0

    .line 113
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mTv:Landroid/widget/TextView;

    const v1, 0x7f07024c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startDiscovery()V
    .locals 3

    .prologue
    .line 121
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    const/16 v2, 0x17

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    .line 122
    iget-object v1, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 123
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsDiscoveringStart:Z

    .line 124
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.BLUETOOTH_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 125
    .local v0, "i":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/selftestmode/BluetoothTest;->startActivityForResult(Landroid/content/Intent;I)V

    .line 126
    return-void
.end method

.method private stopTest()V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->finishActivity(I)V

    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsOn:Z

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->finish()V

    .line 137
    :goto_0
    return-void

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mTv:Landroid/widget/TextView;

    const-string v1, "Bluetooth is being turned Off\nPlease Wait..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x1a

    const/4 v3, 0x0

    .line 45
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v2, 0x7f03000d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/selftestmode/BluetoothTest;->setContentView(I)V

    .line 47
    const v2, 0x7f0a0035

    invoke-virtual {p0, v2}, Lcom/sec/android/app/selftestmode/BluetoothTest;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mTv:Landroid/widget/TextView;

    .line 48
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBa:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsOn:Z

    .line 50
    iput-boolean v3, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsDiscoveringStart:Z

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsForceStop:Z

    .line 52
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 53
    .local v0, "mIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 54
    const-string v2, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 55
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 56
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/selftestmode/BluetoothTest;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 57
    .local v1, "pm":Landroid/os/PowerManager;
    const-string v2, "FactoryTest_Full Wake Lock"

    invoke-virtual {v1, v4, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mFullWl:Landroid/os/PowerManager$WakeLock;

    .line 58
    const-string v2, "FactoryTest_Full Wake Lock"

    invoke-virtual {v1, v4, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mFullWlTemp:Landroid/os/PowerManager$WakeLock;

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mFullWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->btOnOff()V

    .line 61
    return-void
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/selftestmode/BluetoothTest;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mFullWlTemp:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mFullWl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 68
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 69
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 75
    iget-boolean v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsDiscoveringStart:Z

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/selftestmode/BluetoothTest;->mIsForceStop:Z

    .line 78
    :cond_0
    return-void
.end method
