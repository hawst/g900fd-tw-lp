.class Lcom/sec/android/app/status/BatteryStatus_kor$Module;
.super Ljava/lang/Object;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Module"
.end annotation


# instance fields
.field private mIsView:Z

.field private mLineStatusList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mStatusCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mName:Ljava/lang/String;

    .line 127
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mPath:Ljava/lang/String;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mIsView:Z

    .line 131
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "view"    # Z

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mName:Ljava/lang/String;

    .line 134
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mPath:Ljava/lang/String;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    .line 137
    iput-boolean p2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mIsView:Z

    .line 138
    return-void
.end method


# virtual methods
.method public varargs addLineStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V
    .locals 3
    .param p1, "args"    # [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;-><init>()V

    .line 142
    .local v0, "lstatus":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->addStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 143
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    iget v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    array-length v2, p1

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    .line 145
    return-void
.end method

.method public checkLineStatus(I)V
    .locals 7
    .param p1, "mode"    # I

    .prologue
    .line 155
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v4, "st_temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/status/BatteryStatus_kor$Status;>;"
    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;

    .line 157
    .local v2, "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->getStatusList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 158
    .local v5, "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    invoke-virtual {v5}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->isFile()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->canRead()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v5}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getMode()I

    move-result v6

    and-int/2addr v6, p1

    if-nez v6, :cond_1

    .line 161
    :cond_2
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 164
    .end local v5    # "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 165
    .local v3, "st":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->removeStatus(Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V

    .line 166
    iget v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    goto :goto_2

    .line 168
    .end local v3    # "st":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 169
    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 172
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    :cond_5
    return-void
.end method

.method public getIsView()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mIsView:Z

    return v0
.end method

.method public getLineStatusList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method protected setModule()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mPath:Ljava/lang/String;

    return-void
.end method

.method public updateLineStatus(I)Ljava/lang/String;
    .locals 13
    .param p1, "mode"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 174
    const-string v5, ""

    .local v5, "update_str":Ljava/lang/String;
    const-string v4, ""

    .line 175
    .local v4, "temp_str":Ljava/lang/String;
    and-int/lit8 v6, p1, 0x1

    if-ne v6, v11, :cond_0

    iget v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mStatusCount:I

    if-lez v6, :cond_0

    .line 176
    const-string v6, "[%s]\n"

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mName:Ljava/lang/String;

    aput-object v8, v7, v12

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 178
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/status/BatteryStatus_kor$Module;->mLineStatusList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;

    .line 179
    .local v2, "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    const-string v4, ""

    .line 180
    invoke-virtual {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->getStatusList()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 181
    .local v3, "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%s: %s %s\t\t"

    const/4 v6, 0x3

    new-array v9, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v12

    invoke-virtual {v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->isFile()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v3}, Lcom/sec/android/app/status/BatteryStatus_kor$FileManager;->readFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v6

    :goto_2
    aput-object v6, v9, v11

    const/4 v6, 0x2

    invoke-virtual {v3}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->getMessage()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 185
    goto :goto_1

    .line 181
    :cond_1
    const-string v6, ""

    goto :goto_2

    .line 186
    .end local v3    # "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v6, ""

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, ""

    :goto_3
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 187
    goto :goto_0

    .line 186
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\n"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 188
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "ls":Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
    :cond_4
    return-object v5
.end method
