.class Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$1;
.super Ljava/lang/Object;
.source "GripSensorCalibrationSXxxxx.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 124
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    iget-object v6, v6, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mThreadGetData:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_0

    .line 126
    const-string v6, "GRIP_SENSOR_RAWDATA"

    invoke-static {v6, v8}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 127
    .local v5, "rawData":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 128
    .local v2, "msg":Landroid/os/Message;
    invoke-virtual {v2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 129
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 130
    iput v8, v2, Landroid/os/Message;->what:I

    .line 133
    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 134
    .local v3, "nRawData":I
    const-string v6, "RAW_DATA_KEY"

    invoke-virtual {v0, v6, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$1;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->access$000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 142
    const-wide/16 v6, 0x64

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    .line 148
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "nRawData":I
    .end local v5    # "rawData":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 136
    .restart local v0    # "data":Landroid/os/Bundle;
    .restart local v2    # "msg":Landroid/os/Message;
    .restart local v5    # "rawData":Ljava/lang/String;
    :catch_1
    move-exception v4

    .line 137
    .local v4, "ne":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1
.end method
