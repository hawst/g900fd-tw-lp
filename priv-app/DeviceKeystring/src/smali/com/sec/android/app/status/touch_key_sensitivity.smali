.class public Lcom/sec/android/app/status/touch_key_sensitivity;
.super Landroid/app/Activity;
.source "touch_key_sensitivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/touch_key_sensitivity$MyView;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 17
    const-string v0, "touch_key_sensitivity"

    iput-object v0, p0, Lcom/sec/android/app/status/touch_key_sensitivity;->TAG:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    new-instance v1, Lcom/sec/android/app/status/touch_key_sensitivity$MyView;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/touch_key_sensitivity$MyView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/touch_key_sensitivity;->setContentView(Landroid/view/View;)V

    .line 24
    const-string v1, "IS_TOUCHKEY_GRAPH"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/touch_key_sensitivity_graph;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/touch_key_sensitivity;->startActivity(Landroid/content/Intent;)V

    .line 33
    invoke-virtual {p0}, Lcom/sec/android/app/status/touch_key_sensitivity;->finish()V

    .line 34
    return-void

    .line 29
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/touch_key_sensitivity_default;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
