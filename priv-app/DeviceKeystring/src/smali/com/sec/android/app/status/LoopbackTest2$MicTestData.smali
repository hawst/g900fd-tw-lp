.class public Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
.super Ljava/lang/Object;
.source "LoopbackTest2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/LoopbackTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MicTestData"
.end annotation


# instance fields
.field private mMic1Array:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mMic2Array:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 379
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 387
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    .line 388
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getAVG(I)I
    .locals 4
    .param p1, "MICIndex"    # I

    .prologue
    .line 433
    const/4 v2, 0x0

    .line 434
    .local v2, "sum":I
    const/4 v0, 0x0

    .line 435
    .local v0, "avg":I
    if-nez p1, :cond_1

    .line 436
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 437
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    .line 436
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 439
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int v0, v2, v3

    .line 446
    :goto_1
    return v0

    .line 441
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 442
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v2, v3

    .line 441
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 444
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int v0, v2, v3

    goto :goto_1
.end method

.method public getMAX(I)I
    .locals 3
    .param p1, "MICIndex"    # I

    .prologue
    .line 419
    const/high16 v1, -0x80000000

    .line 420
    .local v1, "max":I
    if-nez p1, :cond_1

    .line 421
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 421
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 425
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v1, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 425
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 429
    :cond_3
    return v1
.end method

.method public getMIN(I)I
    .locals 3
    .param p1, "MICIndex"    # I

    .prologue
    .line 405
    const v1, 0x7fffffff

    .line 406
    .local v1, "min":I
    if-nez p1, :cond_1

    .line 407
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 408
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 407
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 411
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v1, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 411
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 415
    :cond_3
    return v1
.end method

.method public setMicArray(II)Z
    .locals 4
    .param p1, "mic1"    # I
    .param p2, "mic2"    # I

    .prologue
    const/16 v3, 0x14

    const/4 v2, 0x0

    .line 391
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic1Array:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v3, :cond_1

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;->mMic2Array:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 401
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
