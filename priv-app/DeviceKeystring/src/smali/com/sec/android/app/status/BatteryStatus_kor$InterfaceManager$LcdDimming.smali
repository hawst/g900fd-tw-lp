.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LcdDimming"
.end annotation


# instance fields
.field private mLcdDimmingEn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1197
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;)V

    .line 1194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    .line 1199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    .line 1200
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1214
    const-string v0, "%s: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "LCD Dimming"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1205
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    .line 1206
    .local v0, "im":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
    iget-boolean v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    .line 1207
    iget-boolean v1, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->mLcdDimmingEn:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->setLcdDimming(Z)V

    .line 1208
    check-cast p1, Landroid/widget/Button;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$LcdDimming;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1209
    return-void

    .line 1206
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
