.class Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeyFactoryFromXML"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Lcom/sec/android/app/status/TspSideTouchTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest;
    .param p2, "x1"    # Lcom/sec/android/app/status/TspSideTouchTest$1;

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;)V

    return-void
.end method

.method private replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "_default"    # Ljava/lang/String;

    .prologue
    .line 344
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object p1, p2

    .line 347
    .end local p1    # "value":Ljava/lang/String;
    :cond_1
    return-object p1
.end method

.method private splitByComma(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 351
    if-nez p1, :cond_0

    .line 352
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 354
    :cond_0
    const-string v0, ","

    .line 355
    .local v0, "SPLIT_EACH_RESULT":Ljava/lang/String;
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "splited":[Ljava/lang/String;
    array-length v2, v1

    if-lt p2, v2, :cond_1

    .line 357
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v2

    .line 359
    :cond_1
    aget-object v2, v1, p2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public makeKeys()Ljava/util/List;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v12, "keyList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;>;"
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->getDocument()Lorg/w3c/dom/Document;

    move-result-object v3

    .line 240
    .local v3, "document":Lorg/w3c/dom/Document;
    const-string v30, "sidetouch"

    move-object/from16 v0, v30

    invoke-interface {v3, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 241
    .local v18, "nodeList":Lorg/w3c/dom/NodeList;
    if-nez v18, :cond_1

    .line 242
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v30

    const-string v31, "makeKeys"

    const-string v32, "Not found Element tags"

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    return-object v12

    .line 246
    :cond_1
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v30

    move/from16 v0, v30

    if-ge v8, v0, :cond_0

    .line 248
    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v17

    check-cast v17, Lorg/w3c/dom/Element;

    .line 249
    .local v17, "node":Lorg/w3c/dom/Element;
    if-eqz v17, :cond_0

    .line 253
    const-string v30, "id"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 254
    .local v11, "id":Ljava/lang/String;
    const-string v30, "enable"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 255
    .local v5, "enable":Ljava/lang/String;
    const-string v30, "keytype"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 256
    .local v14, "keytype":Ljava/lang/String;
    const-string v30, "param"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 257
    .local v19, "param":Ljava/lang/String;
    const-string v30, "true"

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_2

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_2

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v30

    if-eqz v30, :cond_3

    .line 246
    :cond_2
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 262
    :cond_3
    const-string v30, "keycode"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v31, "0"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 263
    .local v13, "keycode":Ljava/lang/String;
    const-string v30, "point"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v31, "300,300"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 264
    .local v20, "point":Ljava/lang/String;
    const-string v30, "name"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1, v11}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 265
    .local v16, "name":Ljava/lang/String;
    const-string v30, "size"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const/high16 v31, 0x41a00000    # 20.0f

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 267
    .local v23, "size":Ljava/lang/String;
    const-string v30, "spec"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v31, "30,200"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 268
    .local v24, "spec":Ljava/lang/String;
    const-string v30, "width_height"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v31, "200,400"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->replaceEmpty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 272
    .local v29, "width_height":Ljava/lang/String;
    const/16 v30, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 273
    .local v21, "px":I
    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    .line 274
    .local v22, "py":I
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    .line 275
    .local v28, "width":I
    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 276
    .local v7, "height":I
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 277
    .local v9, "iKeycode":I
    invoke-static/range {v23 .. v23}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v6

    .line 278
    .local v6, "fTextSize":F
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 279
    .local v10, "iParam":I
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v26

    .line 280
    .local v26, "specMin":I
    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->splitByComma(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v25

    .line 282
    .local v25, "specMax":I
    const/4 v15, 0x0

    .line 283
    .local v15, "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    invoke-static {v14}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->getType(Ljava/lang/String;)Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    move-result-object v27

    .line 284
    .local v27, "type":Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v30

    const-string v31, "makeKeys"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "make Key ID: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, ", type: "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->name()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    sget-object v30, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_TYPE:[I

    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->ordinal()I

    move-result v31

    aget v30, v30, v31

    packed-switch v30, :pswitch_data_0

    .line 320
    :goto_2
    :pswitch_0
    if-eqz v15, :cond_2

    .line 321
    move/from16 v0, v21

    move/from16 v1, v22

    invoke-interface {v15, v0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->setPosition(II)V

    .line 322
    invoke-interface {v15, v6}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->setTextSize(F)V

    .line 323
    invoke-interface {v15, v10}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->setParam(I)V

    .line 324
    move/from16 v0, v28

    invoke-interface {v15, v0, v7}, Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;->setSize(II)V

    .line 325
    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_1

    .line 328
    .end local v6    # "fTextSize":F
    .end local v7    # "height":I
    .end local v9    # "iKeycode":I
    .end local v10    # "iParam":I
    .end local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    .end local v21    # "px":I
    .end local v22    # "py":I
    .end local v25    # "specMax":I
    .end local v26    # "specMin":I
    .end local v27    # "type":Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .end local v28    # "width":I
    :catch_0
    move-exception v4

    .line 329
    .local v4, "e":Ljava/lang/NumberFormatException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 287
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    .restart local v6    # "fTextSize":F
    .restart local v7    # "height":I
    .restart local v9    # "iKeycode":I
    .restart local v10    # "iParam":I
    .restart local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    .restart local v21    # "px":I
    .restart local v22    # "py":I
    .restart local v25    # "specMax":I
    .restart local v26    # "specMin":I
    .restart local v27    # "type":Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .restart local v28    # "width":I
    :pswitch_1
    :try_start_1
    new-instance v15, Lcom/sec/android/app/status/TspSideTouchTest$Key;

    .end local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1, v9}, Lcom/sec/android/app/status/TspSideTouchTest$Key;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 288
    .restart local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    goto :goto_2

    .line 290
    :pswitch_2
    new-instance v30, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v15

    .line 292
    goto :goto_2

    .line 294
    :pswitch_3
    new-instance v30, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKey;->setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v15

    .line 296
    goto :goto_2

    .line 298
    :pswitch_4
    new-instance v30, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyPoll;->setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v15

    .line 300
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$PollerSideTouchSensitivityReader;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;

    move-result-object v31

    move-object v0, v15

    check-cast v0, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchSensitivityReader;->request(Lcom/sec/android/app/status/TspSideTouchTest$SideTouchReaderEventListener;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    .line 330
    .end local v6    # "fTextSize":F
    .end local v7    # "height":I
    .end local v9    # "iKeycode":I
    .end local v10    # "iParam":I
    .end local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    .end local v21    # "px":I
    .end local v22    # "py":I
    .end local v25    # "specMax":I
    .end local v26    # "specMin":I
    .end local v27    # "type":Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .end local v28    # "width":I
    :catch_1
    move-exception v4

    .line 331
    .local v4, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_1

    .line 304
    .end local v4    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v6    # "fTextSize":F
    .restart local v7    # "height":I
    .restart local v9    # "iKeycode":I
    .restart local v10    # "iParam":I
    .restart local v15    # "listener":Lcom/sec/android/app/status/TspSideTouchTest$KeyListener;
    .restart local v21    # "px":I
    .restart local v22    # "py":I
    .restart local v25    # "specMax":I
    .restart local v26    # "specMin":I
    .restart local v27    # "type":Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;
    .restart local v28    # "width":I
    :pswitch_5
    :try_start_2
    new-instance v30, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiverse;->setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v15

    .line 306
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    move-result-object v31

    move-object v0, v15

    check-cast v0, Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->registerListener(ILcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;)V

    goto/16 :goto_2

    .line 310
    :pswitch_6
    new-instance v30, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/status/TspSideTouchTest$KeyFactoryFromXML;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2, v9}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/TspSideTouchTest$SideTouchKeyMultiversePoll;->setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v10}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    move-result-object v15

    .line 312
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->getInstance()Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;

    move-result-object v31

    move-object v0, v15

    check-cast v0, Lcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;

    move-object/from16 v30, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/app/status/TspSideTouchTest$MultiverseEventAdaptor;->registerListener(ILcom/sec/android/app/status/TspSideTouchTest$IMultiverseEventListener;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_2

    .line 285
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
    .end packed-switch
.end method
