.class public Lcom/sec/android/app/status/vibration_test;
.super Landroid/app/Activity;
.source "vibration_test.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final mAutoPattern:[J

.field private static final mDCOnPattern:[J

.field private static final mOnPattern:[J


# instance fields
.field private mDCMoter:Z

.field private mLED_B:I

.field private mLED_G:I

.field private mLED_R:I

.field private mMagnitude:I

.field private mMaxLED:I

.field private mMaxMagnitude:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSeekBarTextView:Landroid/widget/TextView;

.field private mSeekBarTextView_blueValue:Landroid/widget/TextView;

.field private mSeekBarTextView_greenValue:Landroid/widget/TextView;

.field private mSeekBarTextView_redValue:Landroid/widget/TextView;

.field private mSeekBar_blue:Landroid/widget/SeekBar;

.field private mSeekBar_green:Landroid/widget/SeekBar;

.field private mSeekBar_red:Landroid/widget/SeekBar;

.field mVibrator:Landroid/os/SystemVibrator;

.field private mVolStep:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 20
    new-array v0, v1, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/status/vibration_test;->mAutoPattern:[J

    .line 23
    new-array v0, v1, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/status/vibration_test;->mOnPattern:[J

    .line 27
    new-array v0, v1, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/android/app/status/vibration_test;->mDCOnPattern:[J

    return-void

    .line 20
    nop

    :array_0
    .array-data 8
        0x3e8
        0x7d0
    .end array-data

    .line 23
    :array_1
    .array-data 8
        0x0
        0x7fffffff
    .end array-data

    .line 27
    :array_2
    .array-data 8
        0x0
        0x2710
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    .line 33
    iget v0, p0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    iput v0, p0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    .line 35
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/status/vibration_test;->mMaxLED:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_R:I

    .line 37
    iput v1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_G:I

    .line 38
    iput v1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_B:I

    .line 44
    iput-boolean v1, p0, Lcom/sec/android/app/status/vibration_test;->mDCMoter:Z

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/vibration_test;->mVolStep:F

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/vibration_test;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/status/vibration_test;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/vibration_test;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/vibration_test;->setMagnitudeValue(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_greenValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_blueValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/vibration_test;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/android/app/status/vibration_test;->stopVibration()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/vibration_test;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/status/vibration_test;->mLED_G:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/status/vibration_test;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_G:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/vibration_test;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/status/vibration_test;->mLED_B:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/status/vibration_test;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_B:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/vibration_test;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_redValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/vibration_test;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/vibration_test;->setLED(III)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/vibration_test;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;

    .prologue
    .line 18
    iget v0, p0, Lcom/sec/android/app/status/vibration_test;->mLED_R:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/status/vibration_test;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/vibration_test;
    .param p1, "x1"    # I

    .prologue
    .line 18
    iput p1, p0, Lcom/sec/android/app/status/vibration_test;->mLED_R:I

    return p1
.end method

.method private resetLED()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 291
    invoke-direct {p0, v0, v0, v0}, Lcom/sec/android/app/status/vibration_test;->setLED(III)V

    .line 292
    return-void
.end method

.method private resetVibrMagnitude()V
    .locals 3

    .prologue
    .line 286
    const-string v0, "vibration_test"

    const-string v1, "resetVibrMagnitude"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 288
    return-void
.end method

.method private setLED(III)V
    .locals 9
    .param p1, "setR"    # I
    .param p2, "setG"    # I
    .param p3, "setB"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 258
    const-string v1, "vibration_test"

    const-string v2, "setLED()"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setMix : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%02X"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%02X"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "setMix":Ljava/lang/String;
    const-string v1, "LED_BLINK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 266
    return-void
.end method

.method private setMagnitudeValue(I)V
    .locals 7
    .param p1, "mag"    # I

    .prologue
    .line 245
    const-string v1, "true"

    const-string v2, "SUPPORT_PIEZO_MOTOR"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%,.3f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/status/vibration_test;->mVolStep:F

    int-to-float v6, p1

    mul-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "v"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    .local v0, "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :goto_0
    return-void

    .line 252
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "value :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    .restart local v0    # "str":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private startVibration([J)V
    .locals 3
    .param p1, "vibrPattern"    # [J

    .prologue
    .line 269
    const-string v0, "vibration_test"

    const-string v1, "startVibration"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/SystemVibrator;->vibrate([JII)V

    .line 272
    return-void
.end method

.method private stopVibration()V
    .locals 3

    .prologue
    .line 281
    const-string v0, "vibration_test"

    const-string v1, "stopVibration"

    const-string v2, " "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v0}, Landroid/os/SystemVibrator;->cancel()V

    .line 283
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 188
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 189
    .local v0, "magnitude":I
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v3

    .line 190
    .local v3, "seek_red":I
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 191
    .local v2, "seek_green":I
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 194
    .local v1, "seek_blue":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 242
    :goto_0
    :pswitch_0
    return-void

    .line 196
    :pswitch_1
    iget-boolean v4, p0, Lcom/sec/android/app/status/vibration_test;->mDCMoter:Z

    if-nez v4, :cond_0

    .line 197
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    const-string v6, "Vibration on"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    sget-object v4, Lcom/sec/android/app/status/vibration_test;->mOnPattern:[J

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/vibration_test;->startVibration([J)V

    goto :goto_0

    .line 200
    :cond_0
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    const-string v6, "Vibration on mDCOnPattern"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-object v4, Lcom/sec/android/app/status/vibration_test;->mDCOnPattern:[J

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/vibration_test;->startVibration([J)V

    goto :goto_0

    .line 205
    :pswitch_2
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    const-string v6, "Vibration off"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-direct {p0}, Lcom/sec/android/app/status/vibration_test;->stopVibration()V

    goto :goto_0

    .line 209
    :pswitch_3
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    const-string v6, "Vibration auto"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->cancel()V

    .line 211
    sget-object v4, Lcom/sec/android/app/status/vibration_test;->mAutoPattern:[J

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/vibration_test;->startVibration([J)V

    goto :goto_0

    .line 214
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 215
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Minus Button = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 219
    const-string v4, "vibration_test"

    const-string v5, "onClick"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Plus Button = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    add-int/lit8 v7, v0, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 222
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 225
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 228
    :pswitch_8
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 231
    :pswitch_9
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 234
    :pswitch_a
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 237
    :pswitch_b
    iget-object v4, p0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x7f0a02b4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v13, 0x7f030073

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->setContentView(I)V

    .line 53
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v13

    if-nez v13, :cond_0

    .line 54
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 56
    :cond_0
    const-string v13, "true"

    const-string v14, "DC_MOTER"

    invoke-static {v14}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 57
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/status/vibration_test;->mDCMoter:Z

    .line 60
    :cond_1
    const-string v13, "true"

    const-string v14, "SUPPORT_PIEZO_MOTOR"

    invoke-static {v14}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 61
    const-string v13, "PIEZO_MOTOR_MAX_VOL"

    invoke-static {v13}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v13

    const v14, 0x461c4000    # 10000.0f

    div-float/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/status/vibration_test;->mVolStep:F

    .line 64
    :cond_2
    const-string v13, "vibrator"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/SystemVibrator;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    .line 65
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v13}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    .line 66
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    .line 67
    const-string v13, "vibration_test"

    const-string v14, "onCreate"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getMaxMagnitude = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", mMagnitude = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/status/vibration_test;->mMagnitude:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const v13, 0x7f0a02b4

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Button;

    .line 69
    .local v11, "buttonon":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v13, 0x7f0a02b5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 71
    .local v10, "buttonoff":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const v13, 0x7f0a02b6

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    .line 73
    .local v9, "buttonauto":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    const v13, 0x7f0a02b7

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView:Landroid/widget/TextView;

    .line 75
    const v13, 0x7f0a02bc

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_redValue:Landroid/widget/TextView;

    .line 76
    const v13, 0x7f0a02c0

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_greenValue:Landroid/widget/TextView;

    .line 77
    const v13, 0x7f0a02c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBarTextView_blueValue:Landroid/widget/TextView;

    .line 78
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/status/vibration_test;->resetLED()V

    .line 79
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/status/vibration_test;->setMagnitudeValue(I)V

    .line 80
    const v13, 0x7f0a02b8

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 82
    .local v12, "magnitudeRangeTextView":Landroid/widget/TextView;
    const-string v13, "true"

    const-string v14, "SUPPORT_PIEZO_MOTOR"

    invoke-static {v14}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 83
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "\n(0.0v ~ "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "PIEZO_MOTOR_MAX_VOL"

    invoke-static {v14}, Lcom/sec/android/app/lcdtest/support/Support$Spec;->getFloat(Ljava/lang/String;)F

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "v)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :goto_0
    const v13, 0x7f0a02b9

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    .line 89
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 90
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v14, Lcom/sec/android/app/status/vibration_test$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/status/vibration_test$1;-><init>(Lcom/sec/android/app/status/vibration_test;)V

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 105
    const v13, 0x7f0a02bd

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    .line 106
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxLED:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 107
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_red:Landroid/widget/SeekBar;

    new-instance v14, Lcom/sec/android/app/status/vibration_test$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/status/vibration_test$2;-><init>(Lcom/sec/android/app/status/vibration_test;)V

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 121
    const v13, 0x7f0a02c1

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    .line 122
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxLED:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 123
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_green:Landroid/widget/SeekBar;

    new-instance v14, Lcom/sec/android/app/status/vibration_test$3;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/status/vibration_test$3;-><init>(Lcom/sec/android/app/status/vibration_test;)V

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 137
    const v13, 0x7f0a02c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    .line 138
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxLED:I

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setMax(I)V

    .line 139
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/status/vibration_test;->mSeekBar_blue:Landroid/widget/SeekBar;

    new-instance v14, Lcom/sec/android/app/status/vibration_test$4;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/sec/android/app/status/vibration_test$4;-><init>(Lcom/sec/android/app/status/vibration_test;)V

    invoke-virtual {v13, v14}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 154
    const v13, 0x7f0a02ba

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 155
    .local v1, "buttonMinus":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    const v13, 0x7f0a02bb

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 157
    .local v5, "buttonPlus":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    const v13, 0x7f0a02be

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 160
    .local v4, "buttonMinus_red":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v13, 0x7f0a02bf

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 162
    .local v8, "buttonPlus_red":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v8, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    const v13, 0x7f0a02c2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 165
    .local v3, "buttonMinus_green":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    const v13, 0x7f0a02c3

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    .line 167
    .local v7, "buttonPlus_green":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v13, 0x7f0a02c6

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 170
    .local v2, "buttonMinus_blue":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const v13, 0x7f0a02c7

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/status/vibration_test;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 172
    .local v6, "buttonPlus_blue":Landroid/widget/Button;
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    return-void

    .line 85
    .end local v1    # "buttonMinus":Landroid/widget/Button;
    .end local v2    # "buttonMinus_blue":Landroid/widget/Button;
    .end local v3    # "buttonMinus_green":Landroid/widget/Button;
    .end local v4    # "buttonMinus_red":Landroid/widget/Button;
    .end local v5    # "buttonPlus":Landroid/widget/Button;
    .end local v6    # "buttonPlus_blue":Landroid/widget/Button;
    .end local v7    # "buttonPlus_green":Landroid/widget/Button;
    .end local v8    # "buttonPlus_red":Landroid/widget/Button;
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "\n(0 ~"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/status/vibration_test;->mMaxMagnitude:I

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ")"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/vibration_test;->mVibrator:Landroid/os/SystemVibrator;

    .line 184
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 185
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/sec/android/app/status/vibration_test;->resetVibrMagnitude()V

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/status/vibration_test;->resetLED()V

    .line 178
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 179
    return-void
.end method
