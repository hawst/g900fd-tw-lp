.class public Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;
.super Landroid/view/View;
.source "SPen_AccuracyUI.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/SPen_AccuracyUI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TouchView"
.end annotation


# instance fields
.field private isHover:Z

.field private isLandscape:Z

.field private isPenDetect:Z

.field private isTouchDown:Z

.field private isUpdated:Z

.field private mEmptyPaint:Landroid/graphics/Paint;

.field private mHoverCircleSize:F

.field private mHoverPaint:Landroid/graphics/Paint;

.field private mHoveredX:F

.field private mHoveredY:F

.field private mInfoBackgroundPaint:Landroid/graphics/Paint;

.field private mInfoPaint:Landroid/graphics/Paint;

.field private mLevelPaint:Landroid/graphics/Paint;

.field private mLineBitmap:Landroid/graphics/Bitmap;

.field private mLineCanvas:Landroid/graphics/Canvas;

.field private mLinePaint:Landroid/graphics/Paint;

.field private mMatrixBitmap:Landroid/graphics/Bitmap;

.field private mMatrixCanvas:Landroid/graphics/Canvas;

.field private mPenPressure:F

.field private mPenPressureMAX:F

.field private mPreHoveredX:F

.field private mPreHoveredY:F

.field private mPreTouchedX:F

.field private mPreTouchedY:F

.field private mScreenHeight:I

.field private mScreenIndi:I

.field private mScreenWidth:I

.field private mTouchedX:F

.field private mTouchedY:F

.field final synthetic this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/SPen_AccuracyUI;Landroid/content/Context;)V
    .locals 12
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 120
    iput-object p1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    .line 121
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 83
    iput v10, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenIndi:I

    .line 86
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    .line 87
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    .line 88
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    .line 89
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    .line 91
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreHoveredX:F

    .line 92
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreHoveredY:F

    .line 93
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoveredX:F

    .line 94
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoveredY:F

    .line 116
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressure:F

    .line 117
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressureMAX:F

    .line 122
    invoke-virtual {p0, v10}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->setKeepScreenOn(Z)V

    .line 123
    const-string v5, "window"

    invoke-virtual {p2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 124
    .local v1, "mDisplay":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 125
    .local v2, "outpoint":Landroid/graphics/Point;
    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 126
    iget v5, v2, Landroid/graphics/Point;->x:I

    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    .line 127
    iget v5, v2, Landroid/graphics/Point;->y:I

    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    .line 128
    const-string v5, "SPenTestUI"

    const-string v6, "TouchView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Screen size: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 131
    .local v4, "r":Landroid/content/res/Resources;
    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->SIZE_RECT:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$100(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v11, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 132
    .local v3, "pxsize":F
    iget v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->WIDTH_BASIS:I
    invoke-static {p1, v5}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$202(Lcom/sec/android/app/status/SPen_AccuracyUI;I)I

    .line 133
    iget v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    int-to-float v5, v5

    div-float/2addr v5, v3

    float-to-int v5, v5

    add-int/lit8 v5, v5, 0x1

    # setter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->HEIGHT_BASIS:I
    invoke-static {p1, v5}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$302(Lcom/sec/android/app/status/SPen_AccuracyUI;I)I

    .line 134
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v11, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverCircleSize:F

    .line 137
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 138
    :cond_0
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isLandscape:Z

    .line 143
    :goto_0
    const-string v5, "SPenTestUI"

    const-string v6, "TouchView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "channel number: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->WIDTH_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$200(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->HEIGHT_BASIS:I
    invoke-static {p1}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$300(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    iget v6, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 145
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    iget v6, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    invoke-static {v0, v5, v6, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    .line 146
    new-instance v5, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 149
    iget v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    iget v6, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLineBitmap:Landroid/graphics/Bitmap;

    .line 150
    new-instance v5, Landroid/graphics/Canvas;

    iget-object v6, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLineCanvas:Landroid/graphics/Canvas;

    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->setPaint()V

    .line 153
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->initRect()V

    .line 155
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    .line 156
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isUpdated:Z

    .line 157
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isPenDetect:Z

    .line 158
    iput-boolean v9, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isHover:Z

    .line 159
    return-void

    .line 140
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    iput-boolean v10, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isLandscape:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->initRect()V

    return-void
.end method

.method private drawAccuracy(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 450
    const/4 v6, 0x0

    .line 451
    .local v6, "BasedY":I
    const-string v12, ""

    .line 452
    .local v12, "result":Ljava/lang/String;
    const/4 v7, 0x0

    .line 453
    .local v7, "closedX":I
    const/4 v8, 0x0

    .line 454
    .local v8, "closedY":I
    const/high16 v9, 0x42700000    # 60.0f

    .line 455
    .local v9, "colHeight":F
    const/4 v10, 0x0

    .line 456
    .local v10, "delta":F
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    .line 457
    .local v11, "mRectPaint":Landroid/graphics/Paint;
    const v0, -0xffff01

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 458
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 460
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenIndi:I

    if-nez v0, :cond_1

    .line 461
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x1

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 462
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x1

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x3

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 463
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x1

    add-int/lit8 v6, v0, 0x28

    .line 470
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 473
    const-string v0, " Point T:(X %.1f,Y %.1f)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 475
    const/high16 v0, 0x41200000    # 10.0f

    int-to-float v1, v6

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 476
    int-to-float v0, v6

    add-float/2addr v0, v9

    float-to-int v6, v0

    .line 479
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressureMAX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressure:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 480
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressure:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressureMAX:F

    .line 483
    :cond_0
    const-string v0, " PressureMAX: %.3f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressureMAX:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 484
    const/high16 v0, 0x41200000    # 10.0f

    int-to-float v1, v6

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 486
    return-void

    .line 465
    :cond_1
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x6

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 466
    const/high16 v1, 0x41200000    # 10.0f

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x6

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    add-int/lit8 v0, v0, -0xb

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x8

    int-to-float v4, v0

    move-object v0, p1

    move-object v5, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 467
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v0, v0, 0xa

    mul-int/lit8 v0, v0, 0x6

    add-int/lit8 v6, v0, 0x28

    goto/16 :goto_0
.end method

.method private drawLine(FFFFZ)V
    .locals 10
    .param p1, "preX"    # F
    .param p2, "preY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "bTouch"    # Z

    .prologue
    .line 348
    if-eqz p5, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 352
    :cond_0
    const/4 v8, 0x0

    .local v8, "lowX":I
    const/4 v9, 0x0

    .local v9, "lowY":I
    const/4 v6, 0x0

    .local v6, "highX":I
    const/4 v7, 0x0

    .line 354
    .local v7, "highY":I
    cmpl-float v0, p1, p3

    if-ltz v0, :cond_1

    .line 355
    float-to-int v6, p1

    .line 356
    float-to-int v8, p3

    .line 362
    :goto_0
    cmpl-float v0, p2, p4

    if-ltz v0, :cond_2

    .line 363
    float-to-int v7, p2

    .line 364
    float-to-int v9, p4

    .line 370
    :goto_1
    if-eqz p5, :cond_3

    .line 371
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x3

    add-int/lit8 v2, v9, -0x3

    add-int/lit8 v3, v6, 0x3

    add-int/lit8 v4, v7, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 375
    :goto_2
    return-void

    .line 358
    :cond_1
    float-to-int v6, p3

    .line 359
    float-to-int v8, p1

    goto :goto_0

    .line 366
    :cond_2
    float-to-int v7, p4

    .line 367
    float-to-int v9, p2

    goto :goto_1

    .line 373
    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v1, v8, -0x7

    add-int/lit8 v2, v9, -0x7

    add-int/lit8 v3, v6, 0x7

    add-int/lit8 v4, v7, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_2
.end method

.method private drawPoint(FF)V
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, v1}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 379
    new-instance v0, Landroid/graphics/Rect;

    float-to-int v1, p1

    add-int/lit8 v1, v1, -0x3

    float-to-int v2, p2

    add-int/lit8 v2, v2, -0x3

    float-to-int v3, p1

    add-int/lit8 v3, v3, 0x3

    float-to-int v4, p2

    add-int/lit8 v4, v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->invalidate(Landroid/graphics/Rect;)V

    .line 380
    return-void
.end method

.method private initRect()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 489
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->HEIGHT_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$300(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v8, v0, v2

    .line 490
    .local v8, "col_height":F
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->WIDTH_BASIS:I
    invoke-static {v2}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$200(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v2

    int-to-float v2, v2

    div-float v9, v0, v2

    .line 491
    .local v9, "col_width":F
    const/4 v6, 0x0

    .line 492
    .local v6, "ColX":I
    const/4 v7, 0x0

    .line 493
    .local v7, "ColY":I
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 494
    .local v12, "mRectPaint":Landroid/graphics/Paint;
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v3, v2

    iget v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v4, v2

    iget-object v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 496
    const/high16 v0, -0x1000000

    invoke-virtual {v12, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 498
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->HEIGHT_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$300(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v0

    if-ge v10, v0, :cond_1

    .line 499
    int-to-float v0, v10

    mul-float/2addr v0, v8

    float-to-int v7, v0

    .line 501
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    # getter for: Lcom/sec/android/app/status/SPen_AccuracyUI;->WIDTH_BASIS:I
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_AccuracyUI;->access$200(Lcom/sec/android/app/status/SPen_AccuracyUI;)I

    move-result v0

    if-ge v11, v0, :cond_0

    .line 502
    int-to-float v0, v11

    mul-float/2addr v0, v9

    float-to-int v6, v0

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenWidth:I

    int-to-float v3, v3

    int-to-float v4, v7

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixCanvas:Landroid/graphics/Canvas;

    int-to-float v1, v6

    int-to-float v2, v7

    int-to-float v3, v6

    iget v4, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    int-to-float v4, v4

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 501
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 498
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 507
    .end local v11    # "j":I
    :cond_1
    return-void
.end method

.method private setPaint()V
    .locals 7

    .prologue
    const v6, -0xff0100

    const v5, -0xffff01

    const/16 v4, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 162
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 171
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 178
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mEmptyPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 181
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mInfoBackgroundPaint:Landroid/graphics/Paint;

    const/16 v1, 0xc0

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 188
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLevelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 190
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mMatrixBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mLineBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 198
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isHover:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->this$0:Lcom/sec/android/app/status/SPen_AccuracyUI;

    invoke-virtual {v0}, Lcom/sec/android/app/status/SPen_AccuracyUI;->isHoveringEnbale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoveredX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoveredY:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverCircleSize:F

    iget-object v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mHoverPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 202
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isUpdated:Z

    if-eqz v0, :cond_2

    .line 203
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    if-nez v0, :cond_1

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isUpdated:Z

    .line 207
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->drawAccuracy(Landroid/graphics/Canvas;)V

    .line 209
    :cond_2
    return-void
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 293
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 295
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 344
    :goto_0
    :pswitch_0
    const/4 v1, 0x0

    return v1

    .line 297
    :pswitch_1
    const-string v1, "SPenTestUI"

    const-string v2, "onHover"

    const-string v3, "ACTION_HOVER_ENTER "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :pswitch_2
    const-string v1, "SPenTestUI"

    const-string v2, "onHover"

    const-string v3, "ACTION_HOVER_MOVE"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 213
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 215
    .local v6, "action":I
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 216
    packed-switch v6, :pswitch_data_0

    .line 286
    :cond_0
    :goto_0
    return v5

    .line 219
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->initRect()V

    .line 221
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    .line 222
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    .line 223
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    .line 224
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isUpdated:Z

    .line 225
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isPenDetect:Z

    .line 227
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 228
    iput v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenIndi:I

    .line 233
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressureMAX:F

    .line 234
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressure:F

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->invalidate()V

    goto :goto_0

    .line 229
    :cond_2
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenHeight:I

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x6

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 230
    iput v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mScreenIndi:I

    goto :goto_1

    .line 240
    :pswitch_1
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 241
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-ge v7, v0, :cond_3

    .line 242
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    .line 243
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    .line 244
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    .line 245
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    .line 246
    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->drawLine(FFFFZ)V

    .line 241
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 249
    :cond_3
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    .line 250
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    .line 251
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    .line 252
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    .line 253
    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    iget v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    iget v3, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iget v4, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->drawLine(FFFFZ)V

    .line 254
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    .line 255
    iput-boolean v5, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isPenDetect:Z

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPenPressure:F

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->invalidate()V

    goto/16 :goto_0

    .line 264
    .end local v7    # "i":I
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    if-eqz v0, :cond_0

    .line 265
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    .line 266
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    .line 268
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    .line 270
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mPreTouchedY:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 271
    iget v0, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedX:F

    iget v1, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->mTouchedY:F

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->drawPoint(FF)V

    .line 274
    :cond_4
    iput-boolean v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isTouchDown:Z

    .line 275
    iput-boolean v2, p0, Lcom/sec/android/app/status/SPen_AccuracyUI$TouchView;->isPenDetect:Z

    goto/16 :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
