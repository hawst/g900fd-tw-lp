.class Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeySoundHandler"
.end annotation


# static fields
.field private static mEnable:Z

.field private static mMediaPlayer:Landroid/media/MediaPlayer;

.field private static mOldVolumeIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1707
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mEnable:Z

    .line 1708
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public static declared-synchronized onFinish(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1724
    const-class v2, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mEnable:Z

    if-eqz v1, :cond_1

    .line 1725
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1726
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 1727
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1729
    :cond_0
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1730
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x3

    sget v3, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mOldVolumeIndex:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1732
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_1
    monitor-exit v2

    return-void

    .line 1724
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static onStart(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x3

    .line 1713
    sget-boolean v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mEnable:Z

    if-eqz v1, :cond_0

    .line 1714
    const v1, 0x7f040009

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    sput-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1716
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1717
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    sput v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mOldVolumeIndex:I

    .line 1718
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onStart"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mOldVolume = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mOldVolumeIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v5, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 1721
    .end local v0    # "am":Landroid/media/AudioManager;
    :cond_0
    return-void
.end method

.method public static declared-synchronized playKeySound(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1735
    const-class v2, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mEnable:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1736
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 1738
    :try_start_1
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1746
    :cond_0
    :goto_0
    monitor-exit v2

    return-void

    .line 1739
    :catch_0
    move-exception v0

    .line 1740
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_2
    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1735
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 1744
    :cond_1
    :try_start_3
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
