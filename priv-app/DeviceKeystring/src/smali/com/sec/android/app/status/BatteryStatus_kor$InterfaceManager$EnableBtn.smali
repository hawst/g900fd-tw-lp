.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EnableBtn"
.end annotation


# static fields
.field private static ENABLEBTN_TITLE:Ljava/lang/String;


# instance fields
.field private mButtonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;",
            ">;"
        }
    .end annotation
.end field

.field private mButtonState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1030
    const-string v0, "Button Enable"

    sput-object v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->ENABLEBTN_TITLE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1035
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;Z)V

    .line 1037
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonState:Z

    .line 1038
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;->getButtonList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonList:Ljava/util/ArrayList;

    .line 1039
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1055
    const-string v0, "%s: %b"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->ENABLEBTN_TITLE:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonState:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1044
    iget-boolean v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonState:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonState:Z

    .line 1045
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;

    .line 1046
    .local v0, "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->getAwaysOn()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1047
    iget-boolean v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->mButtonState:Z

    invoke-virtual {v0, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;->setEnabled(Z)V

    goto :goto_1

    .line 1044
    .end local v0    # "btn":Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1049
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    check-cast p1, Landroid/widget/Button;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$EnableBtn;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1050
    return-void
.end method
