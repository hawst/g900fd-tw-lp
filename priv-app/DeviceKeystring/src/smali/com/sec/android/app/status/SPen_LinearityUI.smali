.class public Lcom/sec/android/app/status/SPen_LinearityUI;
.super Landroid/app/Activity;
.source "SPen_LinearityUI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;
    }
.end annotation


# instance fields
.field private HEIGHT_BASIS:I

.field private SIZE_RECT:I

.field private WIDTH_BASIS:I

.field private isHoveringTest:Z

.field private mTouchView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->SIZE_RECT:I

    .line 32
    const/16 v0, 0x1c

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I

    .line 33
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringTest:Z

    .line 80
    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/SPen_LinearityUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->SIZE_RECT:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/SPen_LinearityUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/status/SPen_LinearityUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->WIDTH_BASIS:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/SPen_LinearityUI;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/status/SPen_LinearityUI;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/SPen_LinearityUI;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->HEIGHT_BASIS:I

    return p1
.end method


# virtual methods
.method public isHoveringEnbale()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringTest:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 57
    new-instance v0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;-><init>(Lcom/sec/android/app/status/SPen_LinearityUI;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->setContentView(Landroid/view/View;)V

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    check-cast v0, Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 61
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x400

    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->requestWindowFeature(I)Z

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_LinearityUI;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 45
    new-instance v0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;-><init>(Lcom/sec/android/app/status/SPen_LinearityUI;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/SPen_LinearityUI;->setContentView(Landroid/view/View;)V

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    check-cast v0, Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 48
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 65
    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    .line 66
    iget-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringTest:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringTest:Z

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/status/SPen_LinearityUI;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Hovering draw "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->isHoveringTest:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 77
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 66
    goto :goto_0

    .line 69
    :cond_1
    const/16 v0, 0x19

    if-ne p1, v0, :cond_2

    .line 70
    const-string v0, "SPenTestUI"

    const-string v2, "onKeyDown"

    const-string v3, "Volume key pressed"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;

    # invokes: Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->initRect()V
    invoke-static {v0}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->access$000(Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    check-cast v0, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;

    # invokes: Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->drawInfo(FFF)V
    invoke-static {v0, v4, v4, v4}, Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;->access$100(Lcom/sec/android/app/status/SPen_LinearityUI$TouchView;FFF)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/status/SPen_LinearityUI;->mTouchView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_1

    .line 77
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method
