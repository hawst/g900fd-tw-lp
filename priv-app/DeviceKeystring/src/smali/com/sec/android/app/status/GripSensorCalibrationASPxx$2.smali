.class Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;
.super Landroid/content/BroadcastReceiver;
.source "GripSensorCalibrationASPxx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 206
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 207
    .local v0, "action":Ljava/lang/String;
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "mBroadcastReceiver.onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v3, "com.sec.factory.app.factorytest.GRIP_CONFIRM_Keystring"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    const/4 v2, 0x0

    .line 211
    .local v2, "isWakeupPass":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 212
    .local v1, "bd":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 213
    const-string v3, "IS_GRIP_WAKEUP_PASS"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 215
    :cond_0
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "mBroadcastReceiver.onReceive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GRIP_CONFIRM value (Keystring) = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    if-eqz v2, :cond_2

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->test_start()V
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V

    .line 222
    .end local v1    # "bd":Landroid/os/Bundle;
    .end local v2    # "isWakeupPass":Z
    :cond_1
    :goto_0
    return-void

    .line 219
    .restart local v1    # "bd":Landroid/os/Bundle;
    .restart local v2    # "isWakeupPass":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$2;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "Wake up Error"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
