.class public Lcom/sec/android/app/lcdtest/support/XMLDataStorage$ElementIdMismatchException;
.super Ljava/lang/RuntimeException;
.source "XMLDataStorage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/lcdtest/support/XMLDataStorage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ElementIdMismatchException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x481b5e841df8cf50L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 356
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 365
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 366
    return-void
.end method
