.class Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;
.super Ljava/lang/Object;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ModelManager"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$FuelGauge;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Online;,
        Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;
    }
.end annotation


# direct methods
.method private static checkAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;
    .locals 6

    .prologue
    .line 424
    const-string v1, "hardware: %s\nchipname:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "ro.hardware"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "ro.chipname"

    const-string v5, "Unknown"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 427
    .local v0, "name":Ljava/lang/String;
    const-string v1, "smdk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "universal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 428
    :cond_0
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->EXYNOS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    .line 432
    :goto_0
    return-object v1

    .line 429
    :cond_1
    const-string v1, "qcom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 430
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->QUALCOMM:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    goto :goto_0

    .line 432
    :cond_2
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->NONE:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    goto :goto_0
.end method

.method private static checkExternalStoragePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Landroid/os/Environment$UserEnvironment;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/os/Environment$UserEnvironment;-><init>(I)V

    .line 421
    .local v0, "ue":Landroid/os/Environment$UserEnvironment;
    invoke-virtual {v0}, Landroid/os/Environment$UserEnvironment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static checkMode()I
    .locals 4

    .prologue
    .line 415
    const/4 v0, 0x2

    .line 416
    .local v0, "eng_mode":I
    const-string v1, "eng"

    const-string v2, "ro.build.type"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    or-int/2addr v0, v1

    .line 417
    return v0

    .line 416
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static checkSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;)V
    .locals 3
    .param p0, "model_info"    # Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    move-result-object v0

    .line 445
    .local v0, "ap":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->NONE:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    if-eq v0, v1, :cond_0

    .line 446
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$1;->$SwitchMap$com$sec$android$app$status$BatteryStatus_kor$ModelInfo$AP:[I

    invoke-virtual {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 453
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->BRIGHTNESS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    const-string v2, "/sys/class/backlight/panel/brightness"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;Ljava/lang/Object;)V

    .line 457
    :goto_0
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->FG_RESET_SOC:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    const-string v2, "/sys/class/power_supply/battery/batt_reset_soc"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;Ljava/lang/Object;)V

    .line 460
    :cond_0
    return-void

    .line 448
    :pswitch_0
    sget-object v1, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;->BRIGHTNESS:Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;

    const-string v2, "/sys/class/leds/lcd-backlight/brightness"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$Setting;Ljava/lang/Object;)V

    goto :goto_0

    .line 446
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static getModelInfo()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 388
    const/4 v1, 0x0

    .line 389
    .local v1, "model_info":Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getIsCreated()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 390
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getInstance()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v1

    .line 405
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->getMode()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_0

    .line 406
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->showInfo()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    :cond_0
    :goto_1
    return-object v1

    .line 392
    :cond_1
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->create()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    move-result-object v1

    .line 393
    const-string v2, "ro.revision"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setRevision(Ljava/lang/String;)V

    .line 394
    const-string v2, "ro.product.model"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setName(Ljava/lang/String;)V

    .line 395
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->checkExternalStoragePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setExternalStoragePath(Ljava/lang/String;)V

    .line 396
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->checkMode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setMode(I)V

    .line 397
    invoke-static {}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->checkAP()Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setAP(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo$AP;)V

    .line 398
    invoke-static {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->setModule(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;)V

    .line 399
    invoke-static {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager;->checkSetting(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;)V

    .line 400
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setModule()V

    .line 401
    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->checkModule()V

    .line 402
    invoke-static {v4}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setIsCreated(Z)V

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "BatteryStatus"

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->setIsCreated(Z)V

    goto :goto_1
.end method

.method private static setModule(Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;)V
    .locals 2
    .param p0, "model_info"    # Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;

    .prologue
    .line 435
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Battery;-><init>()V

    const-string v1, "/sys/class/power_supply/battery"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V

    .line 436
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Online;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Online;-><init>()V

    const-string v1, "/sys/class/power_supply"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V

    .line 437
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Thermistor;-><init>()V

    const-string v1, "/sys/class/power_supply/battery"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V

    .line 438
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$Charger;-><init>()V

    const-string v1, "/sys/class/power_supply"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V

    .line 439
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$FuelGauge;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$FuelGauge;-><init>()V

    const-string v1, "/sys/class/power_supply"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;Ljava/lang/String;)V

    .line 440
    new-instance v0, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;

    invoke-direct {v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelManager$DataLogModule;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$ModelInfo;->addModule(Lcom/sec/android/app/status/BatteryStatus_kor$Module;)V

    .line 441
    return-void
.end method
