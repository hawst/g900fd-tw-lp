.class final enum Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;
.super Ljava/lang/Enum;
.source "GripSensorTest2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "UI_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

.field public static final enum ADxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

.field public static final enum SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

.field public static final enum UNKNOWN:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;


# instance fields
.field public mSupportRawData:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 73
    new-instance v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    const-string v1, "ADxxxx"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->ADxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    new-instance v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    const-string v1, "SXxxxx"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    new-instance v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v3}, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->UNKNOWN:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->ADxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->SXxxxx:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->UNKNOWN:Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->$VALUES:[Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p3, "supportRawData"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput-boolean p3, p0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->mSupportRawData:Z

    .line 77
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->$VALUES:[Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    invoke-virtual {v0}, [Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/status/GripSensorTest2$UI_TYPE;

    return-object v0
.end method
