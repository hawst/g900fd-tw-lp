.class Lcom/sec/android/app/status/GripSensorTest2$1;
.super Landroid/os/Handler;
.source "GripSensorTest2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorTest2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field count:I

.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorTest2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorTest2;)V
    .locals 1

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->count:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 87
    const/4 v1, 0x0

    .line 88
    .local v1, "result":Z
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    iget v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->count:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->count:I

    .line 91
    iget v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->count:I

    rem-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_2

    .line 92
    iput v4, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->count:I

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v4

    const v3, -0xffff01

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    :goto_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "RAW_DATA_KEY"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 98
    .local v0, "nRawData":I
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 101
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v2, v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$102(Lcom/sec/android/app/status/GripSensorTest2;I)I

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest2;->access$100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$200(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    if-ne v2, v5, :cond_4

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v2, v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$202(Lcom/sec/android/app/status/GripSensorTest2;I)I

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest2;->access$200(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 95
    .end local v0    # "nRawData":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v4

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 103
    .restart local v0    # "nRawData":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    if-le v2, v0, :cond_1

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v2, v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$102(Lcom/sec/android/app/status/GripSensorTest2;I)I

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v6

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMinRawData:I
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest2;->access$100(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 111
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$200(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v2

    if-ge v2, v0, :cond_0

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # setter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v2, v0}, Lcom/sec/android/app/status/GripSensorTest2;->access$202(Lcom/sec/android/app/status/GripSensorTest2;I)I

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mTextGripSensorRawData:[Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/app/status/GripSensorTest2;->access$000(Lcom/sec/android/app/status/GripSensorTest2;)[Landroid/widget/TextView;

    move-result-object v2

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorTest2$1;->this$0:Lcom/sec/android/app/status/GripSensorTest2;

    # getter for: Lcom/sec/android/app/status/GripSensorTest2;->mMaxRawData:I
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorTest2;->access$200(Lcom/sec/android/app/status/GripSensorTest2;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 88
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
