.class Lcom/sec/android/app/status/AccSensorCalibration$2;
.super Ljava/lang/Object;
.source "AccSensorCalibration.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/AccSensorCalibration;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/AccSensorCalibration;

.field final synthetic val$data:B


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/AccSensorCalibration;B)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    iput-byte p2, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->val$data:B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 156
    const/4 v0, -0x1

    .line 157
    .local v0, "autoRotateState":I
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$900(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/status/AccSensorCalibration;->access$900(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/os/Handler;

    move-result-object v4

    const-string v5, "false;false"

    invoke-virtual {v4, v6, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 159
    const-string v3, "SUPPORT_ADSP_SENSOR_TEMPORARY"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ADSP"

    const-string v4, "ACCELEROMETER_SENSOR_TYPE"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    invoke-virtual {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->getAutoRotateState()I

    move-result v0

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/status/AccSensorCalibration;->setAutoRotate(I)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$1100(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/SensorManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v4}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;
    invoke-static {v5}, Lcom/sec/android/app/status/AccSensorCalibration;->access$1000(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/Sensor;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 164
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->pause()V
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->access$800(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V

    .line 166
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    iget-byte v4, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->val$data:B

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration;->calibration(B)Z
    invoke-static {v3, v4}, Lcom/sec/android/app/status/AccSensorCalibration;->access$1200(Lcom/sec/android/app/status/AccSensorCalibration;B)Z

    .line 173
    const-string v3, "SUPPORT_ADSP_SENSOR_TEMPORARY"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "ADSP"

    const-string v4, "ACCELEROMETER_SENSOR_TYPE"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 176
    const-wide/16 v4, 0x3e8

    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 181
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$900(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 183
    const-string v3, "SUPPORT_ADSP_SENSOR_TEMPORARY"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "ADSP"

    const-string v4, "ACCELEROMETER_SENSOR_TYPE"

    invoke-static {v4}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 186
    const-wide/16 v4, 0x3e8

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    .line 190
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mSensorManager:Landroid/hardware/SensorManager;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$1100(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/SensorManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v4}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mAccSensor:Landroid/hardware/Sensor;
    invoke-static {v5}, Lcom/sec/android/app/status/AccSensorCalibration;->access$1000(Lcom/sec/android/app/status/AccSensorCalibration;)Landroid/hardware/Sensor;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v2

    .line 191
    .local v2, "r":Z
    const-string v3, "AccSensorCalibration"

    const-string v4, "onClick"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "RegisterListener = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    # getter for: Lcom/sec/android/app/status/AccSensorCalibration;->mTask:Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration;->access$000(Lcom/sec/android/app/status/AccSensorCalibration;)Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;

    move-result-object v3

    # invokes: Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->resume()V
    invoke-static {v3}, Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;->access$700(Lcom/sec/android/app/status/AccSensorCalibration$SensorReadTask;)V

    .line 193
    iget-object v3, p0, Lcom/sec/android/app/status/AccSensorCalibration$2;->this$0:Lcom/sec/android/app/status/AccSensorCalibration;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/status/AccSensorCalibration;->setAutoRotate(I)V

    .line 195
    .end local v2    # "r":Z
    :cond_2
    return-void

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 177
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 178
    .restart local v1    # "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_1

    .line 187
    .end local v1    # "ie":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v1

    .line 188
    .restart local v1    # "ie":Ljava/lang/InterruptedException;
    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/Exception;)V

    goto :goto_2
.end method
