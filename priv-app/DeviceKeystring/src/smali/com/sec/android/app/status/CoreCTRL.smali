.class public Lcom/sec/android/app/status/CoreCTRL;
.super Landroid/app/Activity;
.source "CoreCTRL.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;
    }
.end annotation


# static fields
.field private static final FREQUENCY_TABLE:[Ljava/lang/String;


# instance fields
.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mSwitches:[Landroid/widget/Switch;

.field private mTextView:[Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "384000"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "486000"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "594000"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "702000"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "810000"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "918000"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "1026000"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "1134000"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "1242000"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "1350000"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "1458000"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "1566000"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "1674000"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "1728000"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 184
    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/CoreCTRL;)[Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/CoreCTRL;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    return-object v0
.end method

.method private disableSwitch(Landroid/widget/CompoundButton;)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;

    .prologue
    const/4 v0, 0x0

    .line 156
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setActivated(Z)V

    .line 157
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setClickable(Z)V

    .line 158
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 159
    return-void
.end method

.method private showChooseDialog(I)V
    .locals 4
    .param p1, "parentID"    # I

    .prologue
    .line 178
    const-string v1, "CoreCTRL"

    const-string v2, "showChooseDialog"

    const-string v3, "showChooseDialog"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    new-instance v0, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;-><init>(Lcom/sec/android/app/status/CoreCTRL;)V

    .line 180
    .local v0, "dialog":Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;
    const v1, 0x7f070292

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->setParameter(II)V

    .line 181
    invoke-virtual {p0}, Lcom/sec/android/app/status/CoreCTRL;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "showChooseDialog"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/status/CoreCTRL$AlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 182
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 150
    :pswitch_0
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    const-string v2, "default"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 99
    :pswitch_1
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop_mpdecision is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    if-nez p2, :cond_0

    .line 102
    const-string v0, "stop mpdecision"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->doShellCmd(Ljava/lang/String;)Z

    .line 103
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    goto :goto_0

    .line 107
    :pswitch_2
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop_thermald is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    if-nez p2, :cond_0

    .line 110
    const-string v0, "stop thermald"

    invoke-static {v0}, Lcom/sec/android/app/lcdtest/support/LtUtil;->doShellCmd(Ljava/lang/String;)Z

    .line 111
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    goto :goto_0

    .line 115
    :pswitch_3
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpu0_disable_standalone_power_collapse is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    if-nez p2, :cond_0

    .line 118
    const-string v0, "CPU0_STANDALONE_COLLAPSE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 119
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    goto :goto_0

    .line 123
    :pswitch_4
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpu0_disable_idle_power_collapse is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    if-nez p2, :cond_0

    .line 126
    const-string v0, "CPU0_IDLE_COLLAPSE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 127
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    goto/16 :goto_0

    .line 131
    :pswitch_5
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cpu1_disable_standalone_power_collapse is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    if-nez p2, :cond_0

    .line 134
    const-string v0, "CPU1_STANDALONE_COLLAPSE"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 135
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    goto/16 :goto_0

    .line 139
    :pswitch_6
    const-string v0, "CoreCTRL"

    const-string v1, "onCheckedChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "switch_cpu1 is checked = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    if-nez p2, :cond_1

    .line 142
    const-string v0, "CPU1_ON"

    const-string v1, "0"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL;->mLinearLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 145
    :cond_1
    const-string v0, "CPU1_ON"

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/sec/android/app/lcdtest/support/Support$Kernel;->write(Ljava/lang/String;Ljava/lang/String;)Z

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/status/CoreCTRL;->mLinearLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x7f0a0049
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 172
    :pswitch_0
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    const-string v2, "default"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :goto_0
    return-void

    .line 168
    :pswitch_1
    const-string v0, "CoreCTRL"

    const-string v1, "onClick"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bottonID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/CoreCTRL;->showChooseDialog(I)V

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a004c
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v6, 0x1

    .line 41
    const-string v3, "CoreCTRL"

    const-string v4, "onCreate"

    const-string v5, "onCreate"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 43
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 45
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    const v3, 0x7f030013

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->setContentView(I)V

    .line 48
    const v3, 0x7f0a0051

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mLinearLayout:Landroid/widget/LinearLayout;

    .line 50
    const/4 v1, 0x4

    .line 51
    .local v1, "N_CHOOSER":I
    new-array v3, v7, [Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    .line 53
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    const v3, 0x7f0a004c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v8

    .line 54
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    const v3, 0x7f0a004d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v6

    .line 55
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    const v3, 0x7f0a0052

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v9

    .line 56
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    const v3, 0x7f0a0053

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    aput-object v3, v4, v10

    .line 58
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_1

    .line 59
    iget-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    aget-object v3, v3, v2

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setClickable(Z)V

    .line 60
    iget-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    aget-object v3, v3, v2

    const/high16 v4, 0x1060000

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 61
    iget-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mTextView:[Landroid/widget/TextView;

    aget-object v3, v3, v2

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    :cond_1
    const/4 v0, 0x7

    .line 65
    .local v0, "N_BUTTONS":I
    const/4 v3, 0x7

    new-array v3, v3, [Landroid/widget/Switch;

    iput-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    .line 67
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const v3, 0x7f0a0049

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v8

    .line 68
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const v3, 0x7f0a004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v6

    .line 69
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const v3, 0x7f0a004e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v9

    .line 70
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const v3, 0x7f0a004f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v10

    .line 71
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const v3, 0x7f0a0054

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v7

    .line 73
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const/4 v5, 0x5

    const v3, 0x7f0a004b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v5

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    invoke-direct {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->disableSwitch(Landroid/widget/CompoundButton;)V

    .line 76
    iget-object v4, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    const/4 v5, 0x6

    const v3, 0x7f0a0050

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/CoreCTRL;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Switch;

    aput-object v3, v4, v5

    .line 78
    const/4 v2, 0x0

    :goto_1
    const/4 v3, 0x7

    if-ge v2, v3, :cond_2

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/status/CoreCTRL;->mSwitches:[Landroid/widget/Switch;

    aget-object v3, v3, v2

    invoke-virtual {v3, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 78
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 81
    :cond_2
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 91
    const-string v0, "CoreCTRL"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 85
    const-string v0, "CoreCTRL"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 87
    return-void
.end method

.method public setChooseText(Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "which"    # I

    .prologue
    .line 245
    sget-object v0, Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;

    array-length v0, v0

    if-le v0, p2, :cond_0

    if-lez p2, :cond_0

    .line 246
    const-string v0, "CoreCTRL"

    const-string v1, "setChooseText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "freq = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;

    aget-object v3, v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/status/CoreCTRL;->FREQUENCY_TABLE:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/1000(MHz)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    :cond_0
    return-void
.end method
