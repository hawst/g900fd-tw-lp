.class Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;
.super Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DumpSysPower"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1121
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$ButtonEx;-><init>(Landroid/content/Context;Z)V

    .line 1122
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1137
    const-string v0, "DumpSys Power"

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1127
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1128
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/status/PowerStatus;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1129
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1130
    invoke-virtual {p0}, Lcom/sec/android/app/status/BatteryStatus_kor$InterfaceManager$DumpSysPower;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1131
    const-string v1, "BatteryStatus"

    const-string v2, "DumpSys Power"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    return-void
.end method
