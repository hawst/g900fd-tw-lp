.class Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;
.super Landroid/os/Handler;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;)V
    .locals 0

    .prologue
    .line 876
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;->this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 879
    iget v0, p1, Landroid/os/Message;->what:I

    .line 880
    .local v0, "keycode":I
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 881
    .local v1, "sensitivity":I
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mUiUpdateHandler.handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "keycode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", sencitivity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;->this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->setSensitivity(I)V

    .line 884
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;->this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    iget-object v2, v2, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    sget-object v3, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-ne v2, v3, :cond_0

    .line 885
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;->this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->finishTouchKeyThread()Z
    invoke-static {v2}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->access$600(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;)Z

    .line 886
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;->this$1:Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    iget-object v2, v2, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    # invokes: Lcom/sec/android/app/status/TspSideTouchTest;->requestFinish()V
    invoke-static {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->access$400(Lcom/sec/android/app/status/TspSideTouchTest;)V

    .line 888
    :cond_0
    return-void
.end method
