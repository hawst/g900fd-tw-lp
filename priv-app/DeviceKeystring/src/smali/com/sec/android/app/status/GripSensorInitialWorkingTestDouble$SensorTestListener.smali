.class Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTestDouble.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V
    .locals 0

    .prologue
    .line 494
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;
    .param p2, "x1"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$1;

    .prologue
    .line 494
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 496
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 9
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/high16 v8, 0x40a00000    # 5.0f

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 499
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    cmpl-float v0, v0, v7

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 502
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    if-nez v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 506
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$002(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    .line 515
    :cond_1
    :goto_0
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v6

    cmpl-float v0, v0, v7

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 517
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    if-nez v0, :cond_2

    .line 519
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 521
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$102(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    .line 530
    :cond_3
    :goto_1
    return-void

    .line 507
    :cond_4
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    cmpl-float v0, v0, v8

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$000(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking:Z

    if-ne v0, v4, :cond_5

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 511
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_1:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$002(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    goto :goto_0

    .line 522
    :cond_6
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v6

    cmpl-float v0, v0, v8

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$100(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Z

    move-result v0

    if-ne v0, v4, :cond_3

    .line 524
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mIsWorking2:Z

    if-ne v0, v4, :cond_7

    .line 525
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$1400(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 526
    const-string v0, "GripSensorInitialWorkingTestDouble"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Release2"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;

    # setter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->Grip_status_ON_2:Z
    invoke-static {v0, v5}, Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;->access$102(Lcom/sec/android/app/status/GripSensorInitialWorkingTestDouble;Z)Z

    goto :goto_1
.end method
