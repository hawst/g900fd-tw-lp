.class public Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;
.super Landroid/app/Activity;
.source "MainLabel.java"


# static fields
.field private static modelName:Ljava/lang/String;


# instance fields
.field private CLASS_NAME:Ljava/lang/String;

.field private mBarcode:Landroid/widget/TextView;

.field private mFilePath:Ljava/lang/String;

.field private mManufacturedate:Landroid/widget/TextView;

.field private mSerialnumber:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "ro.product.model"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->modelName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 21
    const-string v0, "MainLabel"

    iput-object v0, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    .line 22
    const-string v0, "/efs/FactoryApp/serial_no"

    iput-object v0, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mFilePath:Ljava/lang/String;

    return-void
.end method

.method public static isEfsProcessedModel()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 145
    const/4 v2, 0x0

    .line 146
    .local v2, "isEfsProcessed":Z
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SPH-M830"

    aput-object v4, v0, v3

    const-string v3, "SPH-M950"

    aput-object v3, v0, v5

    const/4 v3, 0x2

    const-string v4, " "

    aput-object v4, v0, v3

    .line 150
    .local v0, "APOSolutionArray":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 151
    aget-object v3, v0, v1

    sget-object v4, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->modelName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_1

    .line 152
    const/4 v2, 0x1

    .line 157
    :cond_0
    return v2

    .line 150
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private isFactoryAppAPO()Z
    .locals 9

    .prologue
    .line 119
    const/4 v4, 0x0

    .line 120
    .local v4, "isAPO":Z
    const/4 v2, 0x0

    .line 123
    .local v2, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v3, Ljava/io/FileReader;

    const-string v5, "system/bin/at_distributor"

    invoke-direct {v3, v5}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    .end local v2    # "fr":Ljava/io/FileReader;
    .local v3, "fr":Ljava/io/FileReader;
    const/4 v4, 0x1

    .line 125
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Found the at_distributor. Decided APO mode."

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 132
    if-eqz v3, :cond_2

    .line 134
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 141
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    :cond_0
    :goto_0
    return v4

    .line 135
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 137
    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 127
    .local v1, "ex":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v4, 0x0

    .line 128
    :try_start_3
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Can not find at_distributor. Decided CPO mode"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 132
    if-eqz v2, :cond_0

    .line 134
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 135
    :catch_2
    move-exception v0

    .line 136
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 130
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "Can not decide because of error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 132
    if-eqz v2, :cond_0

    .line 134
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 135
    :catch_4
    move-exception v0

    .line 136
    .local v0, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "isFactoryAppAPO"

    const-string v7, "File Close error"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v2, :cond_1

    .line 134
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 137
    :cond_1
    :goto_4
    throw v5

    .line 135
    :catch_5
    move-exception v0

    .line 136
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v6, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "isFactoryAppAPO"

    const-string v8, "File Close error"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 132
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .line 129
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 126
    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_1

    .end local v2    # "fr":Ljava/io/FileReader;
    .restart local v3    # "fr":Ljava/io/FileReader;
    :cond_2
    move-object v2, v3

    .end local v3    # "fr":Ljava/io/FileReader;
    .restart local v2    # "fr":Ljava/io/FileReader;
    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v11, 0x7f030049

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->setContentView(I)V

    .line 30
    const-string v11, "ro.product.model"

    const-string v12, "Unknown"

    invoke-static {v11, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 32
    .local v6, "product":Ljava/lang/String;
    const-string v11, "GT-I9260"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 33
    const v11, 0x7f0a013f

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    const-string v12, "2. FCC ID : A3LGTI9260"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    const v11, 0x7f0a0140

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    const-string v12, "3. RATED : 3.8V ---; 2100mA"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    :cond_0
    const v11, 0x7f0a0141

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mBarcode:Landroid/widget/TextView;

    .line 38
    const v11, 0x7f0a0142

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mSerialnumber:Landroid/widget/TextView;

    .line 39
    const v11, 0x7f0a0143

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mManufacturedate:Landroid/widget/TextView;

    .line 40
    const v11, 0x7f0a013e

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 41
    .local v5, "modelNameText":Landroid/widget/TextView;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "1. MODEL: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->modelName:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const/4 v10, 0x0

    .line 46
    .local v10, "value":Ljava/lang/String;
    const/4 v7, 0x0

    .line 48
    .local v7, "reader":Ljava/io/BufferedReader;
    invoke-direct {p0}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->isFactoryAppAPO()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-static {}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->isEfsProcessedModel()Z

    move-result v11

    if-nez v11, :cond_2

    .line 50
    const v11, 0x7f0a0141

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 51
    .local v9, "tmpView":Landroid/widget/TextView;
    const-string v11, "ril.barcode"

    const-string v12, "none"

    invoke-static {v11, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "mBarcode":Ljava/lang/String;
    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    const v11, 0x7f0a0142

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "tmpView":Landroid/widget/TextView;
    check-cast v9, Landroid/widget/TextView;

    .line 55
    .restart local v9    # "tmpView":Landroid/widget/TextView;
    const-string v11, "ril.serialnumber"

    const-string v12, "none"

    invoke-static {v11, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "mSerial":Ljava/lang/String;
    invoke-virtual {v9, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const v11, 0x7f0a0143

    invoke-virtual {p0, v11}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .end local v9    # "tmpView":Landroid/widget/TextView;
    check-cast v9, Landroid/widget/TextView;

    .line 59
    .restart local v9    # "tmpView":Landroid/widget/TextView;
    const-string v11, "ril.manufacturedate"

    const-string v12, "none"

    invoke-static {v11, v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "mManufacturedate":Ljava/lang/String;
    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    .end local v2    # "mBarcode":Ljava/lang/String;
    .end local v3    # "mManufacturedate":Ljava/lang/String;
    .end local v4    # "mSerial":Ljava/lang/String;
    .end local v9    # "tmpView":Landroid/widget/TextView;
    :cond_1
    :goto_0
    return-void

    .line 64
    :cond_2
    :try_start_0
    new-instance v8, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/FileReader;

    iget-object v12, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mFilePath:Ljava/lang/String;

    invoke-direct {v11, v12}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .local v8, "reader":Ljava/io/BufferedReader;
    if-eqz v8, :cond_3

    .line 67
    :try_start_1
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    .line 68
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "onCreate"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "value : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 73
    :cond_3
    if-eqz v8, :cond_9

    .line 75
    :try_start_2
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v7, v8

    .line 82
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    :cond_4
    :goto_1
    if-eqz v10, :cond_8

    .line 83
    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "Serial":[Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "onCreate"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Serial : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    array-length v11, v0

    const/4 v12, 0x1

    if-ne v11, v12, :cond_6

    .line 87
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mBarcode:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mSerialnumber:Landroid/widget/TextView;

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mManufacturedate:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 76
    .end local v0    # "Serial":[Ljava/lang/String;
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v1

    .line 77
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v7, v8

    .line 78
    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 70
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 73
    if-eqz v7, :cond_4

    .line 75
    :try_start_4
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 76
    :catch_2
    move-exception v1

    .line 77
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 73
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_3
    if-eqz v7, :cond_5

    .line 75
    :try_start_5
    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 78
    :cond_5
    :goto_4
    throw v11

    .line 76
    :catch_3
    move-exception v1

    .line 77
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 90
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "Serial":[Ljava/lang/String;
    :cond_6
    array-length v11, v0

    const/4 v12, 0x2

    if-ne v11, v12, :cond_7

    .line 91
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mBarcode:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mSerialnumber:Landroid/widget/TextView;

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mManufacturedate:Landroid/widget/TextView;

    const/4 v12, 0x1

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 94
    :cond_7
    array-length v11, v0

    const/4 v12, 0x3

    if-ne v11, v12, :cond_1

    .line 95
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mBarcode:Landroid/widget/TextView;

    const/4 v12, 0x2

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mSerialnumber:Landroid/widget/TextView;

    const/4 v12, 0x0

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mManufacturedate:Landroid/widget/TextView;

    const/4 v12, 0x1

    aget-object v12, v0, v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 100
    .end local v0    # "Serial":[Ljava/lang/String;
    :cond_8
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mBarcode:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mSerialnumber:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v11, p0, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->mManufacturedate:Landroid/widget/TextView;

    const-string v12, "none"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 73
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v11

    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 70
    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v7    # "reader":Ljava/io/BufferedReader;
    .restart local v8    # "reader":Ljava/io/BufferedReader;
    :cond_9
    move-object v7, v8

    .end local v8    # "reader":Ljava/io/BufferedReader;
    .restart local v7    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/serial_number_label_Indicator/MainLabel;->finish()V

    .line 110
    const/4 v0, 0x1

    return v0
.end method
