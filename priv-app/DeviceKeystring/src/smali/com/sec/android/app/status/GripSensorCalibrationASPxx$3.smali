.class Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;
.super Landroid/os/Handler;
.source "GripSensorCalibrationASPxx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorCalibrationASPxx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)V
    .locals 0

    .prologue
    .line 577
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/16 v8, 0x31

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 579
    const/4 v2, 0x0

    .line 580
    .local v2, "result":Z
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 582
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 583
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_DO_CAL"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "Doing Cal.."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->calibration(B)Z
    invoke-static {v3, v8}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;B)Z

    move-result v2

    .line 586
    if-nez v2, :cond_0

    .line 587
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "onClick"

    const-string v5, "gripsensor_cal_fail"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 591
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 592
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_DO_CALERASE"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    const-string v4, "/sys/class/sensors/grip_sensor/erase_cal"

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v3, v4, v8}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Ljava/lang/String;B)Z

    .line 594
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "Doing Erase Cal.."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    const/16 v4, 0x30

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->calibration(B)Z
    invoke-static {v3, v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$600(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;B)Z

    move-result v2

    .line 596
    if-nez v2, :cond_0

    .line 597
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "onClick"

    const-string v5, "gripsensor_calerase_fail"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 601
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 602
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_DO_RESET"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "Doing Reset.."

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    const-string v4, "/sys/class/sensors/grip_sensor/reset"

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->writeSysfs(Ljava/lang/String;B)Z
    invoke-static {v3, v4, v8}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Ljava/lang/String;B)Z

    goto :goto_0

    .line 607
    :pswitch_3
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_PROGRESS"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->toggleButtonClickable(Z)V
    invoke-static {v3, v7}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$800(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)V

    .line 609
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_PROGRESS"

    const-string v5, "Doing Cal.."

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 613
    :pswitch_4
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_RESTORE_UI"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isTimerFinish:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 615
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mTimerHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 616
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->toggleButtonClickable(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$800(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)V

    goto/16 :goto_0

    .line 621
    :pswitch_5
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_UI"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProxpercent_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 623
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isCalSuccess()Z
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v4

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 624
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_UI"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCalSuccess() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " :  Y ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 629
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " :  N ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 633
    :pswitch_6
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_RESULT"

    const-string v5, "Start"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_SPEC:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1000(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mProxpercent_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$900(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 635
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isCalSuccess()Z
    invoke-static {v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1200(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v4

    # setter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z
    invoke-static {v3, v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1102(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;Z)Z

    .line 636
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 637
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_UI"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCalSuccess() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " :  Y ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mIsCalSuccess:Z
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1100(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v0

    .line 644
    .local v0, "iscalsuccess":Z
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # invokes: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->isSpecIn()Z
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Z

    move-result v1

    .line 646
    .local v1, "isspecin":Z
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_RESULT"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isSpecIn(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const-string v3, "GripSensorCalibrationASPxx"

    const-string v4, "MSG_UPDATE_RESULT"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCalSuccess(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 652
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "PASS"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 653
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    invoke-virtual {v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060005

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 654
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_ResultDetail:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cal Data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCspercent_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1600(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 661
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mUIHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1900(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x3eb

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 641
    .end local v0    # "iscalsuccess":Z
    .end local v1    # "isspecin":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Caldata:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1400(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " :  N ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mCaldata_data:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1300(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 656
    .restart local v0    # "iscalsuccess":Z
    .restart local v1    # "isspecin":Z
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    const-string v4, "FAIL"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 657
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_Result:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$500(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    invoke-virtual {v4}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 658
    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mText_ResultDetail:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1700(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cal: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Spec out: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationASPxx$3;->this$0:Lcom/sec/android/app/status/GripSensorCalibrationASPxx;

    # getter for: Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->mSpecOutNumber:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/status/GripSensorCalibrationASPxx;->access$1800(Lcom/sec/android/app/status/GripSensorCalibrationASPxx;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 580
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
