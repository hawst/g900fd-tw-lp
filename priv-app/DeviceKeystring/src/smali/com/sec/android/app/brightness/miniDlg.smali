.class public Lcom/sec/android/app/brightness/miniDlg;
.super Landroid/app/Dialog;
.source "miniDlg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    }
.end annotation


# static fields
.field private static final brightnessTable:[I


# instance fields
.field private mButtonPushing:Z

.field private mContext:Landroid/content/Context;

.field private mOldBrightness:I

.field private mPowerManager:Landroid/os/PowerManager;

.field private mRepeatType:I

.field private mResolver:Landroid/content/ContentResolver;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x14
        0x28
        0x3c
        0x50
        0x64
        0x78
        0x8c
        0xa0
        0xb4
        0xc8
        0xdc
        0xe6
        0xf0
        0xff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 28
    iput-boolean v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mButtonPushing:Z

    .line 29
    iput v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mRepeatType:I

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/brightness/miniDlg;->mContext:Landroid/content/Context;

    .line 36
    iput-object v1, p0, Lcom/sec/android/app/brightness/miniDlg;->mPowerManager:Landroid/os/PowerManager;

    .line 47
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg;->mContext:Landroid/content/Context;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mResolver:Landroid/content/ContentResolver;

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/brightness/miniDlg;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/brightness/miniDlg;->setBrightness(I)V

    return-void
.end method

.method static synthetic access$100()[I
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/brightness/miniDlg;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/brightness/miniDlg;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/brightness/miniDlg;->mButtonPushing:Z

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/brightness/miniDlg;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/sec/android/app/brightness/miniDlg;->mRepeatType:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/brightness/miniDlg;)Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/brightness/miniDlg;Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;)Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/brightness/miniDlg;
    .param p1, "x1"    # Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/android/app/brightness/miniDlg;->timerButton:Lcom/sec/android/app/brightness/miniDlg$RepeatTimer;

    return-object p1
.end method

.method private setBrightness(I)V
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mPowerManager:Landroid/os/PowerManager;

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mPowerManager:Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mPowerManager:Landroid/os/PowerManager;

    invoke-virtual {v0, p1}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 203
    :cond_0
    return-void
.end method


# virtual methods
.method public getBrightnessLevel()I
    .locals 4

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "brightnessLevel":I
    iget-object v3, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    .line 67
    .local v1, "curBrightness":I
    add-int/lit8 v1, v1, 0x0

    .line 69
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v3, Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I

    array-length v3, v3

    if-ge v2, v3, :cond_2

    .line 70
    sget-object v3, Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I

    aget v3, v3, v2

    if-lt v1, v3, :cond_1

    .line 71
    move v0, v2

    .line 69
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    :cond_1
    sget-object v3, Lcom/sec/android/app/brightness/miniDlg;->brightnessTable:[I

    aget v3, v3, v2

    if-ge v1, v3, :cond_0

    .line 77
    :cond_2
    return v0
.end method

.method public getRepeatType()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mRepeatType:I

    return v0
.end method

.method public isButtonPushing()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mButtonPushing:Z

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 59
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 82
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v4, 0x7f03000f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/brightness/miniDlg;->setContentView(I)V

    .line 84
    const v4, 0x7f0a0039

    invoke-virtual {p0, v4}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iput-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v5, 0xff

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 89
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mResolver:Landroid/content/ContentResolver;

    const-string v5, "screen_brightness"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mOldBrightness:I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    iget v5, p0, Lcom/sec/android/app/brightness/miniDlg;->mOldBrightness:I

    add-int/lit8 v5, v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 96
    iget-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 97
    .local v0, "brightness":I
    add-int/lit8 v0, v0, 0x0

    .line 98
    const v4, 0x7f0a0038

    invoke-virtual {p0, v4}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "value :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v4, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v5, Lcom/sec/android/app/brightness/miniDlg$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/brightness/miniDlg$1;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 114
    const v4, 0x7f0a003a

    invoke-virtual {p0, v4}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 115
    .local v1, "btn_dec":Landroid/widget/Button;
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$2;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    invoke-virtual {v1, v7}, Landroid/widget/Button;->setLongClickable(Z)V

    .line 128
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$3;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 138
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$4;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 155
    const v4, 0x7f0a003b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/brightness/miniDlg;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 156
    .local v2, "btn_inc":Landroid/widget/Button;
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$5;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    invoke-virtual {v2, v7}, Landroid/widget/Button;->setLongClickable(Z)V

    .line 169
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$6;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 179
    new-instance v4, Lcom/sec/android/app/brightness/miniDlg$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/brightness/miniDlg$7;-><init>(Lcom/sec/android/app/brightness/miniDlg;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 195
    return-void

    .line 90
    .end local v0    # "brightness":I
    .end local v1    # "btn_dec":Landroid/widget/Button;
    .end local v2    # "btn_inc":Landroid/widget/Button;
    :catch_0
    move-exception v3

    .line 91
    .local v3, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v4, "Brightness"

    const-string v5, "onCreate"

    const-string v6, "Getting System brightness Error!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v3}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public restoreSystemBrightness()V
    .locals 3

    .prologue
    .line 206
    iget v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mOldBrightness:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/brightness/miniDlg;->setBrightness(I)V

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/brightness/miniDlg;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "screen_brightness"

    iget v2, p0, Lcom/sec/android/app/brightness/miniDlg;->mOldBrightness:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 208
    return-void
.end method

.method public settingSystemBrightness()V
    .locals 3

    .prologue
    .line 211
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 212
    .local v0, "brightness":I
    add-int/lit8 v0, v0, 0x0

    .line 213
    invoke-direct {p0, v0}, Lcom/sec/android/app/brightness/miniDlg;->setBrightness(I)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/brightness/miniDlg;->mResolver:Landroid/content/ContentResolver;

    const-string v2, "screen_brightness"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 215
    return-void
.end method
