.class Lcom/sec/android/app/status/WiFiStressTest$2;
.super Landroid/content/BroadcastReceiver;
.source "WiFiStressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/WiFiStressTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final CLASS_NAME:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/status/WiFiStressTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/WiFiStressTest;)V
    .locals 1

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/status/WiFiStressTest$2;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 171
    const-string v0, "WiFiStressTest.mAlarmManagerReceiver"

    iput-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest$2;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 174
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "action":Ljava/lang/String;
    const-string v1, "WiFiStressTest.mAlarmManagerReceiver"

    const-string v2, "onReceive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v1, "android.intent.action.START_FACTORY_TEST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    const-string v1, "WiFiStressTest.mAlarmManagerReceiver"

    const-string v2, "onReceive"

    const-string v3, "alarm on"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest$2;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.STOP_FACTORY_TEST"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/status/WiFiStressTest;->sendBroadcast(Landroid/content/Intent;)V

    .line 182
    :cond_0
    return-void
.end method
