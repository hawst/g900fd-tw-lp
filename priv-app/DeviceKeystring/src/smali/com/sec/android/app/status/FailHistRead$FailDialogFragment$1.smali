.class Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;
.super Ljava/lang/Object;
.source "FailHistRead.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;->this$0:Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 184
    const-string v1, "FailHistRead"

    const-string v2, "FailDialogFragment"

    const-string v3, "Click Yes"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v1, "V1"

    iget-object v2, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;->this$0:Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->mVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 186
    const-string v1, "persist.sys.factory.failhist"

    const-string v2, "NONE"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v1, "persist.sys.factory.fh.time"

    const-string v2, "NONE"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;->this$0:Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->dismiss()V

    .line 197
    return-void

    .line 188
    :cond_1
    const-string v1, "V2"

    iget-object v2, p0, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment$1;->this$0:Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;

    iget-object v2, v2, Lcom/sec/android/app/status/FailHistRead$FailDialogFragment;->mVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    new-instance v0, Ljava/io/File;

    const-string v1, "/efs/FactoryApp/SSFH"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 191
    .local v0, "ssfh":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 192
    const-string v1, "FailHistRead"

    const-string v2, "FailDialogFragment"

    const-string v3, "file exits"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
