.class Lcom/sec/android/app/status/WiFiStressTest$3;
.super Landroid/content/BroadcastReceiver;
.source "WiFiStressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/status/WiFiStressTest;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final CLASS_NAME:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/status/WiFiStressTest;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/WiFiStressTest;)V
    .locals 1

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/android/app/status/WiFiStressTest$3;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 239
    const-string v0, "WiFiStressTest.mWifiStatusBroadcastReceiver"

    iput-object v0, p0, Lcom/sec/android/app/status/WiFiStressTest$3;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 242
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 243
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 245
    .local v0, "networkInfo":Landroid/net/NetworkInfo;
    const-string v1, "WiFiStressTest.mWifiStatusBroadcastReceiver"

    const-string v2, "onReceive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wifi is connected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest$3;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mWifiSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/sec/android/app/status/WiFiStressTest;->access$300(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/widget/Switch;->setChecked(Z)V

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest$3;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    # getter for: Lcom/sec/android/app/status/WiFiStressTest;->mResultText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/status/WiFiStressTest;->access$500(Lcom/sec/android/app/status/WiFiStressTest;)Landroid/widget/TextView;

    move-result-object v1

    const-string v2, "WiFi ON"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/status/WiFiStressTest$3;->this$0:Lcom/sec/android/app/status/WiFiStressTest;

    sget-object v2, Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;->ENBLED:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    # setter for: Lcom/sec/android/app/status/WiFiStressTest;->mWifiState:Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;
    invoke-static {v1, v2}, Lcom/sec/android/app/status/WiFiStressTest;->access$402(Lcom/sec/android/app/status/WiFiStressTest;Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;)Lcom/sec/android/app/status/WiFiStressTest$WIFI_STATE;

    .line 266
    .end local v0    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    const-string v1, "networkInfo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 256
    .restart local v0    # "networkInfo":Landroid/net/NetworkInfo;
    const-string v1, "WiFiStressTest.mWifiStatusBroadcastReceiver"

    const-string v2, "onReceive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wifi is disconnected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-ne v1, v5, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method
