.class public Lcom/sec/android/app/status/LoopbackTest2;
.super Landroid/app/Activity;
.source "LoopbackTest2.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;,
        Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    }
.end annotation


# instance fields
.field public final CLASS_NAME:Ljava/lang/String;

.field final LOOPBACK_TYPE:[Ljava/lang/String;

.field final LOOPBACK_TYPE_CODEC:I

.field final LOOPBACK_TYPE_PACKET:I

.field final LOOPBACK_TYPE_PCM:I

.field final LOOPBACK_TYPE_REALTIME:I

.field public mAudioManager:Landroid/media/AudioManager;

.field private mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

.field private mButtonGroup:Landroid/widget/RadioGroup;

.field private mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

.field private mIsDoingLoopback:Z

.field private mLooopbackButton:[Landroid/widget/RadioButton;

.field private mLoopbackName:[Ljava/lang/String;

.field private mLoopbackPath:[Ljava/lang/String;

.field private mMicData1:Landroid/widget/TextView;

.field private mMicData2:Landroid/widget/TextView;

.field private mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 32
    const-string v0, "LoopbackTest2"

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->CLASS_NAME:Ljava/lang/String;

    .line 35
    iput-boolean v2, p0, Lcom/sec/android/app/status/LoopbackTest2;->mIsDoingLoopback:Z

    .line 306
    iput v2, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE_PACKET:I

    .line 307
    iput v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE_PCM:I

    .line 308
    iput v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE_REALTIME:I

    .line 309
    iput v5, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE_CODEC:I

    .line 310
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "packet;"

    aput-object v1, v0, v2

    const-string v1, "pcm;"

    aput-object v1, v0, v3

    const-string v1, "realtime"

    aput-object v1, v0, v4

    const-string v1, "codec"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE:[Ljava/lang/String;

    .line 450
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/lcdtest/view/Graph;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/LoopbackTest2;)Lcom/sec/android/app/status/LoopbackTest2$MicTestData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/LoopbackTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/LoopbackTest2;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/LoopbackTest2;->drawTextView(Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/LoopbackTest2;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/LoopbackTest2;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;

    return-object v0
.end method

.method private drawTextView(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 0
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "t"    # Ljava/lang/String;

    .prologue
    .line 263
    if-eqz p1, :cond_0

    .line 264
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :cond_0
    return-void
.end method

.method private startMicTest(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 215
    const-string v1, "LoopbackTest2"

    const-string v2, "startMicTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v1, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    invoke-direct {v1}, Lcom/sec/android/app/status/LoopbackTest2$MicTestData;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    .line 218
    const v1, 0x7f0a011d

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    const v1, 0x7f0a011e

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 223
    const v1, 0x7f0a011f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/lcdtest/view/Graph;

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/lcdtest/view/Graph;->setVisibility(I)V

    .line 225
    new-instance v0, Lcom/sec/android/app/status/LoopbackTest2$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/LoopbackTest2$1;-><init>(Lcom/sec/android/app/status/LoopbackTest2;)V

    .line 257
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->initInputSetting(Ljava/lang/String;)Z

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-virtual {v1}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->startRecording()Z

    .line 260
    return-void
.end method

.method private stopMicTest()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 269
    const-string v0, "LoopbackTest2"

    const-string v1, "stopMicTest"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    invoke-virtual {v0}, Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;->stopRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iput-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioRecording:Lcom/sec/android/app/status/LoopbackTest2$AudioRecording;

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    invoke-virtual {v0}, Lcom/sec/android/app/lcdtest/view/Graph;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/lcdtest/view/Graph;->setVisibility(I)V

    .line 278
    iput-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mGraph:Lcom/sec/android/app/lcdtest/view/Graph;

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData1:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicData2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    :cond_3
    iput-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mMicTestData:Lcom/sec/android/app/status/LoopbackTest2$MicTestData;

    .line 290
    return-void
.end method


# virtual methods
.method public isVoiceCapable()Z
    .locals 6

    .prologue
    .line 364
    const-string v2, "true"

    const-string v3, "SUPPORT_VOICE_CALL_FORCIBLY"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    const-string v2, "LoopbackTest2"

    const-string v3, "isVoiceCapable"

    const-string v4, "SUPPORT_VOICE_CALL_FORCIBLY true return true"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    const/4 v1, 0x1

    .line 376
    :goto_0
    return v1

    .line 368
    :cond_0
    const/4 v1, 0x1

    .line 369
    .local v1, "r":Z
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 371
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_1

    .line 372
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1120044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 375
    :cond_1
    const-string v2, "LoopbackTest2"

    const-string v3, "isVoiceCapable"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isVoiceCapable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 7
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 147
    const-string v1, "LoopbackTest2"

    const-string v2, "onCheckedChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Radio button checked = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const/4 v0, 0x0

    .line 150
    .local v0, "path":Ljava/lang/String;
    packed-switch p2, :pswitch_data_0

    .line 179
    const-string v1, "LoopbackTest2"

    const-string v2, "onCheckedChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkedId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :goto_0
    if-eqz v0, :cond_5

    .line 184
    const-string v1, "LoopbackTest2"

    const-string v2, "onCheckedChanged"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "path="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v1, "main"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopLoopback()V

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopMicTest()V

    .line 189
    invoke-direct {p0, v0}, Lcom/sec/android/app/status/LoopbackTest2;->startMicTest(Ljava/lang/String;)V

    .line 212
    :goto_1
    return-void

    .line 152
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    aget-object v0, v1, v5

    .line 153
    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v0, v1, v2

    .line 156
    goto :goto_0

    .line 158
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    aget-object v0, v1, v6

    .line 159
    goto :goto_0

    .line 161
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v0, v1, v2

    .line 162
    goto :goto_0

    .line 164
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v0, v1, v2

    .line 165
    goto :goto_0

    .line 167
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v0, v1, v2

    .line 168
    goto :goto_0

    .line 170
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v0, v1, v2

    .line 171
    goto :goto_0

    .line 174
    :pswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopLoopback()V

    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopMicTest()V

    goto :goto_1

    .line 191
    :cond_0
    const-string v1, "sub"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopLoopback()V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopMicTest()V

    .line 194
    invoke-direct {p0, v0}, Lcom/sec/android/app/status/LoopbackTest2;->startMicTest(Ljava/lang/String;)V

    goto :goto_1

    .line 196
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mIsDoingLoopback:Z

    if-nez v1, :cond_3

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 198
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTest2;->startLoopback(Ljava/lang/String;I)V

    goto :goto_1

    .line 200
    :cond_2
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTest2;->startLoopback(Ljava/lang/String;I)V

    goto :goto_1

    .line 203
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_4

    .line 204
    invoke-virtual {p0, v0, v6}, Lcom/sec/android/app/status/LoopbackTest2;->setLoopbackPath(Ljava/lang/String;I)V

    goto :goto_1

    .line 206
    :cond_4
    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/status/LoopbackTest2;->setLoopbackPath(Ljava/lang/String;I)V

    goto :goto_1

    .line 210
    :cond_5
    const-string v1, "LoopbackTest2"

    const-string v2, "onCheckedChanged"

    const-string v3, "path=null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0115
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 294
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 301
    :goto_0
    return-void

    .line 296
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->finish()V

    goto :goto_0

    .line 294
    :pswitch_data_0
    .packed-switch 0x7f0a00a4
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 49
    const-string v3, "LoopbackTest2"

    const-string v4, "onCreate"

    const-string v5, "onCreate"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const-string v3, "audio"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    .line 54
    const v3, 0x7f030043

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->setContentView(I)V

    .line 55
    const v3, 0x7f0a0114

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    iput-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mButtonGroup:Landroid/widget/RadioGroup;

    .line 56
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mButtonGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v3, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 58
    const/4 v3, 0x7

    new-array v3, v3, [Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    .line 59
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const v3, 0x7f0a0115

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v6

    .line 60
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const v3, 0x7f0a0116

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v7

    .line 61
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const/4 v5, 0x2

    const v3, 0x7f0a0117

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v5

    .line 62
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const/4 v5, 0x3

    const v3, 0x7f0a0118

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v5

    .line 63
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const/4 v5, 0x4

    const v3, 0x7f0a0119

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v5

    .line 64
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const/4 v5, 0x5

    const v3, 0x7f0a011a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v5

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    const/4 v5, 0x6

    const v3, 0x7f0a011b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    aput-object v3, v4, v5

    .line 67
    const-string v3, "SUPPORT_COMPLEX_LOOPBACK_PATH"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "rawPath":Ljava/lang/String;
    const-string v3, "SUPPORT_COMPLEX_LOOPBACK_PATH"

    invoke-static {v3}, Lcom/sec/android/app/lcdtest/support/Support$TestCase;->getTestName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "rawName":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    .line 71
    if-eqz v1, :cond_1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    :goto_1
    iput-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackName:[Ljava/lang/String;

    .line 73
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackName:[Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackPath:[Ljava/lang/String;

    array-length v3, v3

    iget-object v4, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackName:[Ljava/lang/String;

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackName:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    aget-object v3, v3, v0

    invoke-virtual {v3, v6}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 76
    iget-object v3, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLooopbackButton:[Landroid/widget/RadioButton;

    aget-object v3, v3, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Start : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/LoopbackTest2;->mLoopbackName:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 70
    .end local v0    # "i":I
    :cond_0
    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "mic_rcv,mic_spk,ear_ear"

    aput-object v4, v3, v6

    goto :goto_0

    .line 71
    :cond_1
    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "RCV,SPK,E/P"

    aput-object v4, v3, v6

    goto :goto_1

    .line 79
    :cond_2
    const-string v3, "LoopbackTest2"

    const-string v4, "onCreate"

    const-string v5, "Check the SUPPORT_COMPLEX_LOOPBACK_PATH feature"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_3
    const v3, 0x7f0a00a4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/status/LoopbackTest2;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 111
    const-string v0, "LoopbackTest2"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 113
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 117
    const-string v0, "LoopbackTest2"

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    packed-switch p1, :pswitch_data_0

    .line 127
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 121
    :pswitch_0
    const-string v0, "LoopbackTest2"

    const-string v1, "ignore"

    const-string v2, " KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v0, 0x1

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 132
    const-string v0, "UIBase"

    const-string v1, "onKeyUp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    packed-switch p1, :pswitch_data_0

    .line 142
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 136
    :pswitch_0
    const-string v0, "LoopbackTest2"

    const-string v1, "ignore"

    const-string v2, "KEYCODE_HEADSETHOOK"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const/4 v0, 0x1

    goto :goto_0

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x4f
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 103
    const-string v0, "LoopbackTest2"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopLoopback()V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/status/LoopbackTest2;->stopMicTest()V

    .line 106
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 107
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 96
    const-string v0, "LoopbackTest2"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mButtonGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0a011c

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 98
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 99
    return-void
.end method

.method public setLoopbackPath(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 352
    const-string v0, "LoopbackTest2"

    const-string v1, "setLoopbackPath"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLoopbackPath path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->setStreamMusicVolumeMax()V

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 356
    return-void
.end method

.method public setStreamMusicVolumeMax()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 361
    return-void
.end method

.method public startLoopback(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 315
    const-string v0, "LoopbackTest2"

    const-string v1, "startLoopback"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startLoopback path="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->setStreamMusicVolumeMax()V

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_test_loopback=on;factory_test_path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "factory_test_type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/LoopbackTest2;->LOOPBACK_TYPE:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->isVoiceCapable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/LoopbackTest2$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/LoopbackTest2$2;-><init>(Lcom/sec/android/app/status/LoopbackTest2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 334
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/LoopbackTest2;->mIsDoingLoopback:Z

    .line 335
    return-void
.end method

.method public stopLoopback()V
    .locals 4

    .prologue
    .line 338
    const-string v1, "LoopbackTest2"

    const-string v2, "stopLoopback"

    const-string v3, "stopLoopback"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mIsDoingLoopback:Z

    if-eqz v1, :cond_0

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_loopback=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->isVoiceCapable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 342
    const-string v1, "LoopbackTest2"

    const-string v2, "stopLoopback"

    const-string v3, "send moduleAudioService stop intent"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 344
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/status/LoopbackTest2;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/app/status/ModuleAudioService;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 345
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/LoopbackTest2;->stopService(Landroid/content/Intent;)Z

    .line 348
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/status/LoopbackTest2;->mIsDoingLoopback:Z

    .line 349
    return-void
.end method
