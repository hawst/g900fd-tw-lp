.class public Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
.super Landroid/app/Activity;
.source "GripSensorCalibrationSXxxxx.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;
    }
.end annotation


# instance fields
.field private final CALDATA_SYSFS_PATH:Ljava/lang/String;

.field private final CALIBRATION:B

.field private final CALIBRATION_ERASE:B

.field private final CAL_SYSFS_PATH:Ljava/lang/String;

.field private final CSPERSENT_SYSFS_PATH:Ljava/lang/String;

.field private final GRIP_OFF:B

.field private final GRIP_ON:B

.field private final MODE_SYSFS_PATH:Ljava/lang/String;

.field private final ONOFF_SYSFS_PATH:Ljava/lang/String;

.field private final PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

.field private final RESET_SYSFS_PATH:Ljava/lang/String;

.field private isTimerFinish:Z

.field private isgrip_On:Z

.field private ispass:Z

.field private mButton_Calibration:Landroid/widget/Button;

.field private mButton_CalibrationErase:Landroid/widget/Button;

.field private mButton_Exit:Landroid/widget/Button;

.field private mButton_Next:Landroid/widget/Button;

.field private mButton_reset:Landroid/widget/Button;

.field private mCaldata_data:Ljava/lang/String;

.field private mCspercent_data:Ljava/lang/String;

.field private mGripSensor:Landroid/hardware/Sensor;

.field private mIsCalSuccess:Z

.field private mMode_data:Ljava/lang/String;

.field private mProxpercent_data:Ljava/lang/String;

.field private mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mText_Caldata:Landroid/widget/TextView;

.field private mText_Diff:Landroid/widget/TextView;

.field private mText_Mode:Landroid/widget/TextView;

.field private mText_Offset:Landroid/widget/TextView;

.field private mText_RAW_Count:Landroid/widget/TextView;

.field private mText_Thd:Landroid/widget/TextView;

.field mThreadGetData:Ljava/lang/Thread;

.field private mTimerHandler:Landroid/os/Handler;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x31

    const/16 v0, 0x30

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mThreadGetData:Ljava/lang/Thread;

    .line 43
    iput-byte v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->CALIBRATION:B

    .line 44
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->CALIBRATION_ERASE:B

    .line 45
    iput-byte v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->GRIP_ON:B

    .line 46
    iput-byte v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->GRIP_OFF:B

    .line 47
    const-string v0, "/sys/class/sensors/grip_sensor/raw_data"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->CSPERSENT_SYSFS_PATH:Ljava/lang/String;

    .line 48
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->PROXIMITYPERCENT_SYSFS_PATH:Ljava/lang/String;

    .line 49
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->CALDATA_SYSFS_PATH:Ljava/lang/String;

    .line 50
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->CAL_SYSFS_PATH:Ljava/lang/String;

    .line 51
    const-string v0, "/sys/class/sensors/grip_sensor/onoff"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->ONOFF_SYSFS_PATH:Ljava/lang/String;

    .line 52
    const-string v0, "/sys/class/sensors/grip_sensor/reset"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->RESET_SYSFS_PATH:Ljava/lang/String;

    .line 53
    const-string v0, "/sys/class/sensors/grip_sensor/mode"

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->MODE_SYSFS_PATH:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mMode_data:Ljava/lang/String;

    .line 58
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->ispass:Z

    .line 59
    iput-boolean v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isgrip_On:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isTimerFinish:Z

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mIsCalSuccess:Z

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 438
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$3;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    .line 495
    return-void
.end method

.method private UpdateCSPercent()V
    .locals 9

    .prologue
    const/16 v8, -0x100

    const/high16 v7, -0x10000

    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "RAWCount":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 275
    .local v3, "rawData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 276
    .local v1, "diffData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 277
    .local v2, "offsetData":Ljava/lang/String;
    const-string v4, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 278
    const-string v4, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 279
    const-string v4, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    .line 280
    const-string v4, "/sys/class/sensors/grip_sensor/mode"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mMode_data:Ljava/lang/String;

    .line 281
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mProxpercent_data:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Thd:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mProxpercent_data:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 290
    :goto_0
    if-eqz v0, :cond_1

    .line 291
    const/4 v4, 0x0

    aget-object v3, v0, v4

    .line 292
    const/4 v4, 0x1

    aget-object v1, v0, v4

    .line 293
    const/4 v4, 0x2

    aget-object v2, v0, v4

    .line 295
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCspercent_data:Ljava/lang/String;

    const-string v5, "exception"

    if-eq v4, v5, :cond_6

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-static {v7}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v5

    if-ne v4, v5, :cond_4

    .line 297
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 299
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 307
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mMode_data:Ljava/lang/String;

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 311
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v5, "INIT"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    :cond_2
    :goto_2
    return-void

    .line 287
    :cond_3
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "UpdateCSPercent"

    const-string v6, "mCspercent_data null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 303
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 312
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mMode_data:Ljava/lang/String;

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 313
    iget-object v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Mode:Landroid/widget/TextView;

    const-string v5, "NORMAL"

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 317
    :cond_6
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "UpdateCSPercent"

    const-string v6, "mCspercent_data null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private UpdateGripdata()V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    const-string v0, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mProxpercent_data:Ljava/lang/String;

    .line 270
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;

    .line 271
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isTimerFinish:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mCaldata_data:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isCalSuccess()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isSpecIn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/hardware/Sensor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->UpdateCSPercent()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
    .param p1, "x1"    # B

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->calibration(B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Ljava/lang/String;B)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # B

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->toggleButtonClickable(Z)V

    return-void
.end method

.method private adjustTextSize(F)V
    .locals 1
    .param p1, "size"    # F

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Diff:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setTextSize(F)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Mode:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 163
    return-void
.end method

.method private calibration(B)Z
    .locals 4
    .param p1, "data"    # B

    .prologue
    .line 321
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "calibration"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "calibration data : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const-string v0, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->writeSysfs(Ljava/lang/String;B)Z

    move-result v0

    return v0
.end method

.method private isCalSuccess()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 378
    const-string v4, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v4}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "caldata_data":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 380
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "isCalSuccess()"

    const-string v6, "Sysfs Value Null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_0
    :goto_0
    return v3

    .line 383
    :cond_1
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "caldatas":[Ljava/lang/String;
    const-string v0, "0"

    .line 388
    .local v0, "NOT_CALIBRATED":Ljava/lang/String;
    const-string v4, "0"

    aget-object v5, v2, v3

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 389
    const-string v3, "GripSensorCalibrationSXxxxx"

    const-string v4, "isCalSuccess()"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NOT CALIBRATED: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private isSpecIn()Z
    .locals 14

    .prologue
    const/4 v9, 0x0

    .line 415
    const-string v10, "/sys/class/sensors/grip_sensor/threshold"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 416
    .local v7, "proxpercent_data":Ljava/lang/String;
    const-string v10, "/sys/class/sensors/grip_sensor/calibration"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 417
    .local v1, "caldata_data":Ljava/lang/String;
    const-string v10, "/sys/class/sensors/grip_sensor/raw_data"

    invoke-direct {p0, v10}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->readSysfs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 418
    .local v4, "cspercent_data":Ljava/lang/String;
    const-string v10, ","

    invoke-virtual {v4, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 419
    .local v3, "csPercentList":[Ljava/lang/String;
    const-string v10, ","

    invoke-virtual {v1, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 420
    .local v2, "caldatas":[Ljava/lang/String;
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 436
    :cond_0
    :goto_0
    return v9

    .line 423
    :cond_1
    array-length v10, v3

    const/4 v11, 0x5

    if-ge v10, v11, :cond_2

    .line 424
    const-string v10, "GripSensorCalibrationSXxxxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "csList.length : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    array-length v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    :cond_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 428
    .local v8, "thdSpec":I
    const/4 v0, 0x4

    .line 429
    .local v0, "CS_PERCENT_CNT":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    const/4 v10, 0x4

    if-ge v5, v10, :cond_4

    .line 430
    aget-object v10, v3, v5

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 431
    .local v6, "percent":I
    if-ge v6, v8, :cond_3

    .line 432
    const-string v10, "GripSensorCalibrationSXxxxx"

    const-string v11, "isSpecIn"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "spec out : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 429
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 436
    .end local v6    # "percent":I
    :cond_4
    const/4 v9, 0x1

    goto :goto_0
.end method

.method private readSysfs(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    .line 330
    const/4 v2, 0x0

    .line 331
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v1, ""

    .line 333
    .local v1, "readData":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 335
    .local v4, "temp":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 336
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 338
    :cond_0
    const-string v5, "GripSensorCalibrationSXxxxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sysfs Data : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 345
    if-eqz v3, :cond_1

    .line 346
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    move-object v2, v3

    .line 352
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v4    # "temp":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    return-object v1

    .line 348
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "temp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationSXxxxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 351
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 340
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "temp":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 341
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v5, "GripSensorCalibrationSXxxxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 342
    const/4 v1, 0x0

    .line 345
    if-eqz v2, :cond_2

    .line 346
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 348
    :catch_2
    move-exception v0

    .line 349
    const-string v5, "GripSensorCalibrationSXxxxx"

    const-string v6, "readsysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 344
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 345
    :goto_2
    if-eqz v2, :cond_3

    .line 346
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 350
    :cond_3
    :goto_3
    throw v5

    .line 348
    :catch_3
    move-exception v0

    .line 349
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "GripSensorCalibrationSXxxxx"

    const-string v7, "readsysfs"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 344
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 340
    .end local v2    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method private sensorOn()V
    .locals 5

    .prologue
    .line 204
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    if-nez v1, :cond_0

    .line 205
    new-instance v1, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$1;)V

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    .line 207
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 208
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    if-nez v1, :cond_2

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    const v2, 0x10018

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 213
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    iget-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    .line 215
    .local v0, "result":Z
    const-string v1, "GripSensorCalibrationSXxxxx"

    const-string v2, "sensorOn"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method private toggleButtonClickable(Z)V
    .locals 4
    .param p1, "clickable"    # Z

    .prologue
    .line 218
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "toggleButtonClickable"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set Clickable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 228
    return-void
.end method

.method private writeSysfs(Ljava/lang/String;B)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "data"    # B

    .prologue
    .line 355
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeSysfs path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const/4 v3, 0x0

    .line 357
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 359
    .local v1, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .local v2, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 361
    const/4 v3, 0x1

    .line 366
    if-eqz v2, :cond_0

    .line 367
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 373
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    return v3

    .line 369
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 370
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 372
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 362
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 363
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 366
    if-eqz v1, :cond_1

    .line 367
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 369
    :catch_2
    move-exception v0

    .line 370
    const-string v4, "GripSensorCalibrationSXxxxx"

    const-string v5, "writeSysfs"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 365
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 366
    :goto_2
    if-eqz v1, :cond_2

    .line 367
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 371
    :cond_2
    :goto_3
    throw v4

    .line 369
    :catch_3
    move-exception v0

    .line 370
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v5, "GripSensorCalibrationSXxxxx"

    const-string v6, "writeSysfs"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 365
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 362
    .end local v1    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x64

    const/16 v2, 0x3ea

    const/16 v6, 0x3e9

    const/16 v4, 0x3e8

    .line 231
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 264
    :goto_0
    return-void

    .line 233
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 240
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ed

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 247
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 248
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 251
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 252
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ee

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3ef

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 256
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->UpdateGripdata()V

    goto :goto_0

    .line 259
    :sswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->finish()V

    goto :goto_0

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x7f0a009f -> :sswitch_0
        0x7f0a00a0 -> :sswitch_1
        0x7f0a00a1 -> :sswitch_2
        0x7f0a00a4 -> :sswitch_4
        0x7f0a00aa -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "onCreate"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->setContentView(I)V

    .line 80
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Thd:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0a008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Diff:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0a0091

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Offset:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Caldata:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_Mode:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Calibration:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_CalibrationErase:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Next:Landroid/widget/Button;

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Next:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mText_RAW_Count:Landroid/widget/TextView;

    const-string v1, " : wait"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    const v0, 0x7f0a00aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_reset:Landroid/widget/Button;

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_reset:Landroid/widget/Button;

    const-string v1, "RESET"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_reset:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f0a00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Exit:Landroid/widget/Button;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mButton_Exit:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->adjustTextSize(F)V

    .line 118
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 200
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "onDestroy"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 202
    return-void
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 186
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "onPause"

    const-string v2, "onPause"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 190
    :cond_0
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorListener:Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$SensorTestListener;

    .line 191
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mSensorManager:Landroid/hardware/SensorManager;

    .line 192
    iput-object v3, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mGripSensor:Landroid/hardware/Sensor;

    .line 193
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isgrip_On:Z

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isTimerFinish:Z

    .line 195
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mIsCalSuccess:Z

    .line 196
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 197
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 166
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 167
    const-string v0, "GripSensorCalibrationSXxxxx"

    const-string v1, "onResume"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->sensorOn()V

    .line 169
    invoke-direct {p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->UpdateGripdata()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mUIHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 172
    new-instance v0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx$2;-><init>(Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;)V

    iput-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mTimerHandler:Landroid/os/Handler;

    .line 180
    iput-boolean v4, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->isTimerFinish:Z

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorCalibrationSXxxxx;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 182
    return-void
.end method
