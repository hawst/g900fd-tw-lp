.class Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
.super Lcom/sec/android/app/status/TspSideTouchTest$Key;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchKey"
.end annotation


# instance fields
.field protected _lLayout:Landroid/widget/LinearLayout;

.field protected _sensitivity:I

.field protected _sensitivityNode:Ljava/lang/String;

.field protected _specMax:I

.field protected _specMin:I

.field protected _threshold:Ljava/lang/String;

.field protected _touchNameView:Landroid/widget/TextView;

.field protected _touchResultView:Landroid/widget/TextView;

.field protected _touchSensitivityView:Landroid/widget/TextView;

.field private mUiUpdateHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sec/android/app/status/TspSideTouchTest;

.field private touchKeyThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V
    .locals 2
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "keyCode"    # I

    .prologue
    .line 638
    iput-object p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    .line 639
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/status/TspSideTouchTest$Key;-><init>(Lcom/sec/android/app/status/TspSideTouchTest;Ljava/lang/String;I)V

    .line 624
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    .line 625
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivityNode:Ljava/lang/String;

    .line 626
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_threshold:Ljava/lang/String;

    .line 631
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    .line 633
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    .line 634
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    .line 635
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    .line 636
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    .line 876
    new-instance v0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$2;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->mUiUpdateHandler:Landroid/os/Handler;

    .line 640
    sget-object v0, Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;->TOUCH:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_type:Lcom/sec/android/app/status/TspSideTouchTest$KEY_TYPE;

    .line 641
    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    .param p1, "x1"    # I

    .prologue
    .line 622
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->readSensitivity(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->finishTouchKeyThread()Z

    move-result v0

    return v0
.end method

.method private finishTouchKeyThread()Z
    .locals 3

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 869
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "finishTouchKeyThread"

    const-string v2, "finish touchkey thread"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 871
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    .line 873
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private readSensitivity(I)Z
    .locals 9
    .param p1, "keyCode"    # I

    .prologue
    const/4 v8, 0x0

    .line 836
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "readSensitivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Start Touchkey thread, KeyCode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    const/4 v2, 0x0

    .line 840
    .local v2, "result":Z
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 841
    .local v3, "sensitivity":Ljava/lang/Integer;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 844
    .local v1, "mLargestSensitivity":Ljava/lang/Integer;
    :cond_0
    iget v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_param:I

    invoke-static {v4}, Lcom/sec/android/app/status/TspSideTouchTest$KeyValueHandler;->getSensitivity(I)Ljava/lang/Integer;

    move-result-object v3

    .line 845
    if-nez v3, :cond_2

    .line 864
    :cond_1
    :goto_0
    return v8

    .line 849
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 850
    move-object v1, v3

    .line 851
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "readSensitivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update UI, keycode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mLargestSensitivity: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->mUiUpdateHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->mUiUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, p1, v6, v8}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 857
    :cond_3
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 862
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_0

    .line 858
    :catch_0
    move-exception v0

    .line 859
    .local v0, "ie":Ljava/lang/InterruptedException;
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v4

    const-string v5, "readSensitivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Interrupted Touchkey thread, keycode: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startTouchkeyThread()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 782
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "startTouchkeyThread"

    const-string v2, "touchKeyThread already running"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    :goto_0
    return v3

    .line 786
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey$1;-><init>(Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->touchKeyThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method


# virtual methods
.method public addTouchSpec(Landroid/widget/LinearLayout;)V
    .locals 5
    .param p1, "layout"    # Landroid/widget/LinearLayout;

    .prologue
    .line 709
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " THD:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_threshold:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MAX:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMax:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MIN:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 711
    .local v1, "value":Ljava/lang/String;
    new-instance v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-direct {v0, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 712
    .local v0, "textview":Landroid/widget/TextView;
    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_textsize:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 713
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 714
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 715
    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 716
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 717
    return-void
.end method

.method public addView(Landroid/widget/FrameLayout;)V
    .locals 5
    .param p1, "layout"    # Landroid/widget/FrameLayout;

    .prologue
    const/4 v4, 0x0

    .line 669
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->updateView()V

    .line 671
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 672
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_width:I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_height:I

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 673
    .local v1, "param":Landroid/widget/FrameLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_pX:I

    iget v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_pY:I

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 674
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 675
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    const v3, 0x7f020004

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 677
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 680
    .local v0, "lParam":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 681
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 682
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 684
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 685
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 688
    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_lLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 689
    return-void
.end method

.method public onKey(ILandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 749
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyCode:I

    if-eq p1, v0, :cond_1

    .line 771
    :cond_0
    :goto_0
    return-void

    .line 753
    :cond_1
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onKey"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keyCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 758
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-eq v0, v1, :cond_0

    .line 762
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->startTouchkeyThread()Z

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-static {v0}, Lcom/sec/android/app/status/TspSideTouchTest$KeySoundHandler;->playKeySound(Landroid/content/Context;)V

    goto :goto_0

    .line 766
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->finishTouchKeyThread()Z

    goto :goto_0

    .line 756
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onReset()V
    .locals 1

    .prologue
    .line 775
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    .line 776
    invoke-super {p0}, Lcom/sec/android/app/status/TspSideTouchTest$Key;->onReset()V

    .line 777
    return-void
.end method

.method public release()V
    .locals 0

    .prologue
    .line 893
    invoke-direct {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->finishTouchKeyThread()Z

    .line 894
    return-void
.end method

.method protected setSensitivity(I)V
    .locals 5
    .param p1, "sensitivity"    # I

    .prologue
    .line 798
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    .line 799
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMin:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    if-gt v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMax:I

    if-gt v1, v2, :cond_1

    const/4 v0, 0x1

    .line 800
    .local v0, "bSpecIn":Z
    :goto_0
    # getter for: Lcom/sec/android/app/status/TspSideTouchTest;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/status/TspSideTouchTest;->access$300()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setKeyStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sensitivity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bSpecIn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    if-eqz v0, :cond_2

    .line 804
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 831
    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->updateView()V

    .line 832
    return-void

    .line 799
    .end local v0    # "bSpecIn":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 807
    .restart local v0    # "bSpecIn":Z
    :pswitch_1
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_PASS:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    .line 808
    iget-boolean v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_bNvUpdate:Z

    if-eqz v1, :cond_0

    goto :goto_1

    .line 819
    :cond_2
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$3;->$SwitchMap$com$sec$android$app$status$TspSideTouchTest$KEY_STATUS:[I

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 822
    :pswitch_2
    sget-object v1, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_FAIL:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    iput-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    goto :goto_1

    .line 804
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 819
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public setSpec(II)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    .locals 0
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 644
    iput p1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMin:I

    .line 645
    iput p2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_specMax:I

    .line 646
    return-object p0
.end method

.method public setThreshold(I)Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;
    .locals 1
    .param p1, "param"    # I

    .prologue
    .line 655
    invoke-static {p1}, Lcom/sec/android/app/status/TspSideTouchTest$KeyValueHandler;->getThreshold(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_threshold:Ljava/lang/String;

    .line 656
    return-object p0
.end method

.method protected updateView()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x11

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 721
    invoke-super {p0}, Lcom/sec/android/app/status/TspSideTouchTest$Key;->updateView()V

    .line 722
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 723
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 724
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 725
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 728
    iget v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_sensitivity:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 729
    .local v0, "sensitivity":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    sget-object v2, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->STATUS_INIT:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    if-ne v1, v2, :cond_0

    .line 730
    const-string v0, ""

    .line 732
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 733
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 734
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 735
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 737
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchSensitivityView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 739
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_textsize:F

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 741
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 742
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->this$0:Lcom/sec/android/app/status/TspSideTouchTest;

    invoke-virtual {v2}, Lcom/sec/android/app/status/TspSideTouchTest;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_keyStatus:Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;

    invoke-virtual {v3}, Lcom/sec/android/app/status/TspSideTouchTest$KEY_STATUS;->getResourceId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 744
    iget-object v1, p0, Lcom/sec/android/app/status/TspSideTouchTest$TouchKey;->_touchResultView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 745
    return-void
.end method
