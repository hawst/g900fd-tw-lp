.class Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;
.super Ljava/lang/Object;
.source "GripSensorInitialWorkingTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorTestListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest;
    .param p2, "x1"    # Lcom/sec/android/app/status/GripSensorInitialWorkingTest$1;

    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;-><init>(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 339
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 342
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onSensorChanged"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sensor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v4

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 345
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status Grip"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    if-nez v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    const-string v0, "GripSensorInitialWorkingTest"

    const-string v1, "onSensorChanged"

    const-string v2, "============================ status release"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    iget-boolean v0, v0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mIsWorking:Z

    if-ne v0, v5, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/status/GripSensorInitialWorkingTest$SensorTestListener;->this$0:Lcom/sec/android/app/status/GripSensorInitialWorkingTest;

    # getter for: Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/status/GripSensorInitialWorkingTest;->access$800(Lcom/sec/android/app/status/GripSensorInitialWorkingTest;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
