.class Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;
.super Ljava/lang/Object;
.source "BatteryStatus_kor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus_kor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LineStatus"
.end annotation


# instance fields
.field private mLineStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$Status;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->mLineStatus:Ljava/util/ArrayList;

    .line 99
    return-void
.end method


# virtual methods
.method public varargs addStatus([Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V
    .locals 5
    .param p1, "args"    # [Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .prologue
    .line 101
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 102
    .local v3, "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    iget-object v4, p0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->mLineStatus:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v3    # "status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_0
    return-void
.end method

.method public getStatusList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/status/BatteryStatus_kor$Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->mLineStatus:Ljava/util/ArrayList;

    return-object v0
.end method

.method public removeStatus(Lcom/sec/android/app/status/BatteryStatus_kor$Status;)V
    .locals 3
    .param p1, "status"    # Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .prologue
    .line 106
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->mLineStatus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/status/BatteryStatus_kor$Status;

    .line 107
    .local v1, "temp_status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    invoke-virtual {v1, p1}, Lcom/sec/android/app/status/BatteryStatus_kor$Status;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/status/BatteryStatus_kor$LineStatus;->mLineStatus:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 112
    .end local v1    # "temp_status":Lcom/sec/android/app/status/BatteryStatus_kor$Status;
    :cond_1
    return-void
.end method
