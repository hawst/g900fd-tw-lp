.class public Lcom/sec/android/app/status/PCMDump;
.super Landroid/app/Activity;
.source "PCMDump.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private SavedFileNeme:Ljava/lang/String;

.field private audioSource:I

.field buffer:[B

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioTrack:Landroid/media/AudioTrack;

.field private mBufferSize:I

.field private mButtons:[Landroid/widget/Button;

.field private mEditTextMusicVolume:Landroid/widget/EditText;

.field private mEditTextVoiceCallVolume:Landroid/widget/EditText;

.field private mFileOutPutStream:Ljava/io/FileOutputStream;

.field private mIsRecording:Z

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mProgress:Landroid/widget/ProgressBar;

.field private mReadBytes:I

.field private mRecorder:Landroid/media/AudioRecord;

.field private mStatusTextView:Landroid/widget/TextView;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/PCMDump;->audioSource:I

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioTrack:Landroid/media/AudioTrack;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 60
    iput v2, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->buffer:[B

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    .line 63
    iput-boolean v2, p0, Lcom/sec/android/app/status/PCMDump;->mIsRecording:Z

    .line 64
    iput v2, p0, Lcom/sec/android/app/status/PCMDump;->mReadBytes:I

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->SavedFileNeme:Ljava/lang/String;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/PCMDump;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/PCMDump;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/PCMDump;->setStreamMusicVolume(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/PCMDump;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextVoiceCallVolume:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/PCMDump;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/PCMDump;->setStreamVoiceVolume(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/PCMDump;)Landroid/media/AudioRecord;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/status/PCMDump;Landroid/media/AudioRecord;)Landroid/media/AudioRecord;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # Landroid/media/AudioRecord;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;

    return-object p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/status/PCMDump;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/status/PCMDump;->mIsRecording:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/PCMDump;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/PCMDump;->mReadBytes:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/status/PCMDump;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/status/PCMDump;->mReadBytes:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/status/PCMDump;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/status/PCMDump;)Ljava/io/FileOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/status/PCMDump;Ljava/io/FileOutputStream;)Ljava/io/FileOutputStream;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;
    .param p1, "x1"    # Ljava/io/FileOutputStream;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/status/PCMDump;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/PCMDump;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    return-object v0
.end method

.method private getTimeToString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 187
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 189
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x2

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    .line 190
    .local v4, "month":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x5

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "day":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xb

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    .line 192
    .local v2, "hour":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xc

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    .line 193
    .local v3, "min":Ljava/lang/String;
    new-instance v7, Ljava/text/DecimalFormat;

    const-string v8, "00"

    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const/16 v8, 0xd

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    .line 195
    .local v5, "sec":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 197
    .local v6, "time":Ljava/lang/String;
    const-string v7, "PCMDump"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getTimeToString : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return-object v6
.end method

.method private initInputSetting(I)Z
    .locals 7
    .param p1, "inputDevice"    # I

    .prologue
    const/16 v2, 0x3e80

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 147
    if-nez p1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=main"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 152
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextVoiceCallVolume:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/PCMDump;->setStreamVoiceVolume(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/PCMDump;->setStreamMusicVolume(I)V

    .line 156
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    .line 157
    iget v0, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    .line 158
    iget v0, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->buffer:[B

    .line 161
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/app/status/PCMDump;->audioSource:I

    iget v5, p0, Lcom/sec/android/app/status/PCMDump;->mBufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mRecorder:Landroid/media/AudioRecord;

    .line 164
    iput v6, p0, Lcom/sec/android/app/status/PCMDump;->mReadBytes:I

    .line 166
    return v6

    .line 149
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=sub"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initOutputSetting()Z
    .locals 5

    .prologue
    .line 171
    new-instance v1, Ljava/io/File;

    const-string v3, "mnt/sdcard/PCMData"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .local v1, "outputDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 175
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mnt/sdcard/PCMData/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->getTimeToString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/status/PCMDump;->SavedFileNeme:Ljava/lang/String;

    .line 176
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/status/PCMDump;->SavedFileNeme:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 178
    .local v2, "outputFile":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v3, p0, Lcom/sec/android/app/status/PCMDump;->mFileOutPutStream:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    const/4 v3, 0x0

    return v3

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "fnfe":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private playSound(I)Z
    .locals 11
    .param p1, "device"    # I

    .prologue
    const v3, 0x7f04000d

    const/4 v10, 0x1

    .line 374
    const/4 v6, 0x0

    .line 375
    .local v6, "afd":Landroid/content/res/AssetFileDescriptor;
    const-string v0, "PCMDump"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playSound, device = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopSound()Z

    .line 378
    if-nez p1, :cond_4

    .line 380
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/status/PCMDump;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 381
    if-nez v6, :cond_1

    .line 394
    if-eqz v6, :cond_0

    .line 396
    :try_start_1
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 408
    :cond_0
    :goto_0
    return v10

    .line 397
    :catch_0
    move-exception v7

    .line 398
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "PCMDump"

    const-string v1, "AssetFileDescriptor Close error"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 384
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 394
    if-eqz v6, :cond_2

    .line 396
    :try_start_3
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 405
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v10}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    .line 397
    :catch_1
    move-exception v7

    .line 398
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v0, "PCMDump"

    const-string v1, "AssetFileDescriptor Close error"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 389
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v8

    .line 390
    .local v8, "ie":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 394
    if-eqz v6, :cond_2

    .line 396
    :try_start_5
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 397
    :catch_3
    move-exception v7

    .line 398
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v0, "PCMDump"

    const-string v1, "AssetFileDescriptor Close error"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 391
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "ie":Ljava/io/IOException;
    :catch_4
    move-exception v9

    .line 392
    .local v9, "ile":Ljava/lang/IllegalStateException;
    :try_start_6
    invoke-virtual {v9}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 394
    if-eqz v6, :cond_2

    .line 396
    :try_start_7
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_1

    .line 397
    :catch_5
    move-exception v7

    .line 398
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v0, "PCMDump"

    const-string v1, "AssetFileDescriptor Close error"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 394
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "ile":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 396
    :try_start_8
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 399
    :cond_3
    :goto_2
    throw v0

    .line 397
    :catch_6
    move-exception v7

    .line 398
    .restart local v7    # "e":Ljava/lang/Exception;
    const-string v1, "PCMDump"

    const-string v2, "AssetFileDescriptor Close error"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 403
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-static {p0, v3}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    goto :goto_1
.end method

.method private setButtonStatus(I)Z
    .locals 5
    .param p1, "buttonID"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 347
    packed-switch p1, :pswitch_data_0

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    const-string v2, "Running..."

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 361
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setActivated(Z)V

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 349
    .end local v0    # "i":I
    :pswitch_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setActivated(Z)V

    .line 349
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 353
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setClickable(Z)V

    .line 354
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setActivated(Z)V

    .line 355
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mProgress:Landroid/widget/ProgressBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 370
    :goto_2
    return v4

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setActivated(Z)V

    .line 366
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_2

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0155
        :pswitch_0
    .end packed-switch
.end method

.method private setStreamMusicVolume(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v4, 0x3

    const/4 v0, 0x0

    .line 439
    if-le p1, v1, :cond_0

    move p1, v1

    .line 440
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 442
    :cond_1
    const-string v1, "PCMDump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setStreamMusicVolume, volme="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    if-nez p1, :cond_2

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 450
    :goto_0
    return-void

    .line 447
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v4, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private setStreamVoiceVolume(I)V
    .locals 4
    .param p1, "volume"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 423
    if-le p1, v1, :cond_0

    move p1, v1

    .line 424
    :cond_0
    if-gez p1, :cond_1

    move p1, v0

    .line 426
    :cond_1
    const-string v1, "PCMDump"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setStreamVoiceVolume, volme="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    if-nez p1, :cond_2

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v0, v0, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 436
    :goto_0
    return-void

    .line 431
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v0}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    mul-int/2addr v2, p1

    div-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v0, v2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private startRecording()Z
    .locals 2

    .prologue
    .line 278
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/status/PCMDump$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/PCMDump$3;-><init>(Lcom/sec/android/app/status/PCMDump;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 325
    const/4 v0, 0x1

    return v0
.end method

.method private stopRecording()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 329
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 331
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/status/PCMDump;->mIsRecording:Z

    if-eqz v1, :cond_0

    .line 333
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 338
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recording done.\nPlease find saved file.\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/PCMDump;->SavedFileNeme:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 341
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mThread:Ljava/lang/Thread;

    .line 342
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 343
    return v4
.end method

.method private stopSound()Z
    .locals 2

    .prologue
    .line 412
    const-string v0, "PCMDump"

    const-string v1, "stopSound"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 417
    const/4 v0, 0x1

    .line 419
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 221
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/status/PCMDump;->setButtonStatus(I)Z

    .line 222
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 272
    const-string v1, "PCMDump"

    const-string v2, "onClick, default"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :goto_0
    return-void

    .line 224
    :sswitch_0
    const-string v1, "PCMDump"

    const-string v2, "onClick, recording_mainmic_spk"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=on"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 226
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/PCMDump;->initInputSetting(I)Z

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->initOutputSetting()Z

    .line 228
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/PCMDump;->playSound(I)Z

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->startRecording()Z

    goto :goto_0

    .line 232
    :sswitch_1
    const-string v1, "PCMDump"

    const-string v2, "onClick, recording_mainmic_rcv"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=on"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 234
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/PCMDump;->initInputSetting(I)Z

    .line 235
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->initOutputSetting()Z

    .line 236
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/PCMDump;->playSound(I)Z

    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->startRecording()Z

    goto :goto_0

    .line 240
    :sswitch_2
    const-string v1, "PCMDump"

    const-string v2, "onClick, recording_submic_spk"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=on"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 242
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/PCMDump;->initInputSetting(I)Z

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->initOutputSetting()Z

    .line 244
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/PCMDump;->playSound(I)Z

    .line 245
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->startRecording()Z

    goto :goto_0

    .line 248
    :sswitch_3
    const-string v1, "PCMDump"

    const-string v2, "onClick, recording_submic_rcv"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=on"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 250
    invoke-direct {p0, v4}, Lcom/sec/android/app/status/PCMDump;->initInputSetting(I)Z

    .line 251
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->initOutputSetting()Z

    .line 252
    invoke-direct {p0, v3}, Lcom/sec/android/app/status/PCMDump;->playSound(I)Z

    .line 253
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->startRecording()Z

    goto :goto_0

    .line 256
    :sswitch_4
    const-string v1, "PCMDump"

    const-string v2, "onClick, recording_off"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopSound()Z

    .line 258
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopRecording()Z

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "rms_value"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "result":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 265
    .end local v0    # "result":Ljava/lang/String;
    :sswitch_5
    const-string v1, "PCMDump"

    const-string v2, "onClick, exit"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopSound()Z

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopRecording()Z

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v2, "factory_test_mic_check=off"

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/status/PCMDump;->finish()V

    goto/16 :goto_0

    .line 222
    :sswitch_data_0
    .sparse-switch
        0x7f0a00a4 -> :sswitch_5
        0x7f0a0151 -> :sswitch_0
        0x7f0a0152 -> :sswitch_1
        0x7f0a0153 -> :sswitch_2
        0x7f0a0154 -> :sswitch_3
        0x7f0a0155 -> :sswitch_4
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    const-string v0, "PCMDump"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 72
    const v0, 0x7f03004d

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->setContentView(I)V

    .line 74
    const v0, 0x7f0a0156

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mProgress:Landroid/widget/ProgressBar;

    .line 75
    const v0, 0x7f0a0127

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mStatusTextView:Landroid/widget/TextView;

    const-string v1, "Ready"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const v0, 0x7f0a0157

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;

    const-string v1, "100"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextMusicVolume:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/status/PCMDump$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/PCMDump$1;-><init>(Lcom/sec/android/app/status/PCMDump;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 101
    const v0, 0x7f0a0158

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextVoiceCallVolume:Landroid/widget/EditText;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextVoiceCallVolume:Landroid/widget/EditText;

    const-string v1, "100"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mEditTextVoiceCallVolume:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/status/PCMDump$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/status/PCMDump$2;-><init>(Lcom/sec/android/app/status/PCMDump;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    const v0, 0x7f0a0151

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v2

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    const v0, 0x7f0a0152

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v3

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    const v0, 0x7f0a0153

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v4

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v0, v0, v4

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    const v0, 0x7f0a0154

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v5

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v0, v0, v5

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    const v0, 0x7f0a0155

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    aput-object v0, v1, v6

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mButtons:[Landroid/widget/Button;

    aget-object v0, v0, v6

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    const v0, 0x7f0a00a4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/status/PCMDump;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    .line 144
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 212
    const-string v0, "PCMDump"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 214
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopSound()Z

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/status/PCMDump;->stopRecording()Z

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=off"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 204
    const-string v0, "PCMDump"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 206
    const v0, 0x7f0a0155

    invoke-direct {p0, v0}, Lcom/sec/android/app/status/PCMDump;->setButtonStatus(I)Z

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/status/PCMDump;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "factory_test_mic_check=on"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    .line 208
    return-void
.end method
