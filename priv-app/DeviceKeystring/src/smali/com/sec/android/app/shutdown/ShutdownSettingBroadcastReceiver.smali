.class public Lcom/sec/android/app/shutdown/ShutdownSettingBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShutdownSettingBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 62
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 66
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->wasCompletedParsing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->instance()Lcom/sec/android/app/lcdtest/support/XMLDataStorage;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/lcdtest/support/XMLDataStorage;->parseXML(Landroid/content/Context;)Z

    .line 70
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "intent"

    const-string v2, "put in "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const-string v1, "Receiver"

    const-string v2, "7594"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-class v1, Lcom/sec/android/app/shutdown/ShutdownPreference;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 95
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 96
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 98
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    return-void
.end method
