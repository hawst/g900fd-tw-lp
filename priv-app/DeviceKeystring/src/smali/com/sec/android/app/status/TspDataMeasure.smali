.class public Lcom/sec/android/app/status/TspDataMeasure;
.super Ljava/lang/Object;
.source "TspDataMeasure.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/status/TspDataMeasure$4;,
        Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;
    }
.end annotation


# instance fields
.field protected CLASS_NAME:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field public mHandler:Landroid/os/Handler;

.field private mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

.field private mProduct:Ljava/lang/String;

.field private mSdcardPath:Ljava/lang/String;

.field public mShowValueDialogHandler:Landroid/os/Handler;

.field private mTspNotiDialog:Landroid/app/AlertDialog;

.field private mTspVendor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "TspDataMeasure"

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    .line 34
    const-string v0, "mnt/extSdCard/"

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mSdcardPath:Ljava/lang/String;

    .line 38
    const-string v0, "ro.product.model"

    const-string v1, "Unknown"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    .line 133
    new-instance v0, Lcom/sec/android/app/status/TspDataMeasure$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspDataMeasure$1;-><init>(Lcom/sec/android/app/status/TspDataMeasure;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    .line 172
    new-instance v0, Lcom/sec/android/app/status/TspDataMeasure$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/status/TspDataMeasure$2;-><init>(Lcom/sec/android/app/status/TspDataMeasure;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mShowValueDialogHandler:Landroid/os/Handler;

    .line 74
    new-instance v0, Lcom/sec/android/app/status/TspModuleDevice;

    invoke-direct {v0, p1}, Lcom/sec/android/app/status/TspModuleDevice;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    .line 75
    iput-object p1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mContext:Landroid/content/Context;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v1, "get_chip_vendor"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/status/TspDataMeasure;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspDataMeasure;->getNodeData(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/status/TspDataMeasure;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/status/TspDataMeasure;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/status/TspDataMeasure;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/status/TspDataMeasure;->refreshExtSdCard()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspDataMeasure;->getNodeDataHover(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspDataMeasure;->getNodeDataSideTouch(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/status/TspDataMeasure;Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/status/TspDataMeasure;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/status/TspDataMeasure;->showValueDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private createFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 245
    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->mSdcardPath:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 246
    .local v3, "dir":Ljava/io/File;
    const-string v4, ".txt"

    .line 247
    .local v4, "exr":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/status/TspDataMeasure;->mSdcardPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 248
    .local v5, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 250
    .local v2, "count":I
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    const-string v12, ""

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 253
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "arr":[Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "CreateFile"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "arr : "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    :goto_0
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v12, v10}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 257
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "file : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    if-eqz v0, :cond_2

    .line 260
    move-object v1, v0

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_2

    aget-object v9, v1, v6

    .line 261
    .local v9, "str":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "str : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "fileName"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x5

    if-le v10, v11, :cond_0

    .line 264
    const/4 v10, 0x0

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, -0x4

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 265
    add-int/lit8 v2, v2, 0x1

    .line 269
    :cond_0
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "count : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 254
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "str":Ljava/lang/String;
    :cond_1
    const-string v10, "null"

    goto/16 :goto_0

    .line 273
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/status/TspDataMeasure;->mSdcardPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 274
    .local v8, "path":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "path : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    :goto_2
    return-object v8

    .line 277
    .restart local v0    # "arr":[Ljava/lang/String;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/sec/android/app/status/TspDataMeasure;->mSdcardPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 278
    .restart local v8    # "path":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "CreateFile"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "path : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 283
    .end local v0    # "arr":[Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    :cond_4
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private getNodeData(Ljava/lang/String;)V
    .locals 8
    .param p1, "vendor"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getNodeData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " vendor :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/status/TspDataMeasure;->subStringProduct()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_TSP_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "prefix":Ljava/lang/String;
    const-string v1, ""

    .line 292
    .local v1, "result":Ljava/lang/String;
    const-string v2, "MELFAS"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_cm_abs_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 294
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_cm_abs"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 295
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_cm_delta_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_cm_delta"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 299
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    const-string v2, "SYNAPTICS"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_rawcap_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_rawcap"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 305
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_rx_to_rx_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_rx_to_rx"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 309
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_delta_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_delta"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 313
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_trx_short_test"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 316
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 317
    :cond_2
    const-string v2, "ATMEL"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_reference_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_reference"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_delta_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_delta"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    :cond_3
    const-string v2, "CYPRESS"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 327
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_raw_count_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 328
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_raw_count"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 329
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_local_idac_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_local_idac"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 334
    :cond_4
    const-string v2, "ZINITIX"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 335
    const-string v2, "SUPPORT_RUN_REF"

    invoke-static {v2}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_reference_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 337
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_reference"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 338
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getNodeData111"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " vendor :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_dnd_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_dnd"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 345
    :cond_6
    const-string v2, "IMAGIS"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 347
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_reference_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_reference"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 349
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 350
    :cond_7
    const-string v2, "STM"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_rawcap_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_rawcap"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 353
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_cx_data_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    .line 356
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "get_cx_data"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPReadTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 357
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getNodeDataHover(Ljava/lang/String;)V
    .locals 8
    .param p1, "vendor"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 363
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getNodeData"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " vendor :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/status/TspDataMeasure;->subStringProduct()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_TSP_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, "prefix":Ljava/lang/String;
    const-string v1, ""

    .line 368
    .local v1, "result":Ljava/lang/String;
    const-string v2, "SYNAPTICS"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 369
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_abscap_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 370
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_absdelta_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 372
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    const-string v2, "STM"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "hover_enable,1"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 375
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_abscap_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 376
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "run_absdelta_read"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v3, v7}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v3, "hover_enable,0"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getNodeDataSideTouch(Ljava/lang/String;)V
    .locals 8
    .param p1, "vendor"    # Ljava/lang/String;

    .prologue
    .line 385
    iget-object v4, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getNodeData"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " vendor :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/status/TspDataMeasure;->subStringProduct()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_SideTouch_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 388
    .local v3, "prefix":Ljava/lang/String;
    const-string v1, ""

    .line 389
    .local v1, "deltaResult":Ljava/lang/String;
    const-string v0, ""

    .line 391
    .local v0, "absCapResult":Ljava/lang/String;
    const-string v4, "SYNAPTICS"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 392
    const-string v4, "/sys/class/sec/tsp/cmd"

    const-string v5, "sidekey_enable,1"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/status/TspModuleDevice;->writeToFile(Ljava/lang/String;[B)Z

    .line 393
    const-string v4, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v4}, Lcom/sec/android/app/status/TspModuleDevice;->readFromFile(Ljava/lang/String;)Ljava/lang/String;

    .line 395
    iget-object v4, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v5, "run_sidekey_delta_read"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 396
    const-string v4, ",$"

    const-string v5, ""

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 397
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getSideTouchDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v4, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    const-string v5, "run_sidekey_abscap_read"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/status/TspModuleDevice;->startTSPTest(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    const-string v4, ",$"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 400
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getSideTouchDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/status/TspDataMeasure;->makeTspData(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string v4, "/sys/class/sec/tsp/cmd"

    const-string v5, "sidekey_enable,0"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/status/TspModuleDevice;->writeToFile(Ljava/lang/String;[B)Z

    .line 403
    const-string v4, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v4}, Lcom/sec/android/app/status/TspModuleDevice;->readFromFile(Ljava/lang/String;)Ljava/lang/String;

    .line 405
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ABS Cap]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n[Jitter]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 406
    .local v2, "dialogValue":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/status/TspDataMeasure;->mShowValueDialogHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/status/TspDataMeasure;->mShowValueDialogHandler:Landroid/os/Handler;

    const/16 v6, 0x3e8

    invoke-virtual {v5, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 409
    .end local v2    # "dialogValue":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private getSideTouchDataName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "tspVendor"    # Ljava/lang/String;
    .param p2, "readCnt"    # I

    .prologue
    .line 551
    const-string v0, "None"

    .line 552
    .local v0, "name":Ljava/lang/String;
    const-string v1, "SYNAPTICS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 555
    packed-switch p2, :pswitch_data_0

    .line 566
    :cond_0
    :goto_0
    return-object v0

    .line 557
    :pswitch_0
    const-string v0, "ABSCap"

    .line 558
    goto :goto_0

    .line 560
    :pswitch_1
    const-string v0, "Jitter"

    .line 561
    goto :goto_0

    .line 555
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getTspDataName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "tspVendor"    # Ljava/lang/String;
    .param p2, "readCnt"    # I

    .prologue
    .line 413
    const-string v0, "None"

    .line 415
    .local v0, "name":Ljava/lang/String;
    const-string v1, "MELFAS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 418
    packed-switch p2, :pswitch_data_0

    .line 515
    :cond_0
    :goto_0
    return-object v0

    .line 420
    :pswitch_0
    const-string v0, "CM_ABS"

    .line 421
    goto :goto_0

    .line 423
    :pswitch_1
    const-string v0, "CM_DELTA"

    .line 424
    goto :goto_0

    .line 429
    :cond_1
    const-string v1, "SYNAPTICS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 431
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 433
    :pswitch_2
    const-string v0, "RawCap"

    .line 434
    goto :goto_0

    .line 436
    :pswitch_3
    const-string v0, "Rx to Rx"

    .line 437
    goto :goto_0

    .line 439
    :pswitch_4
    const-string v0, "Jitter"

    .line 440
    goto :goto_0

    .line 442
    :pswitch_5
    const-string v0, "TrxShort"

    .line 443
    goto :goto_0

    .line 447
    :cond_2
    const-string v1, "ATMEL"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 450
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 452
    :pswitch_6
    const-string v0, "Reference"

    .line 453
    goto :goto_0

    .line 455
    :pswitch_7
    const-string v0, "Delta"

    .line 456
    goto :goto_0

    .line 460
    :cond_3
    const-string v1, "CYPRESS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 463
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 465
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    const-string v2, "GT-I8580"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    const-string v2, "SM-G3815"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 466
    :cond_4
    const-string v0, "Mutual Raw"

    goto :goto_0

    .line 468
    :cond_5
    const-string v0, "Raw Count"

    .line 470
    goto :goto_0

    .line 472
    :pswitch_9
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    const-string v2, "GT-I8580"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    const-string v2, "SM-G3815"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 473
    :cond_6
    const-string v0, "Mutual PWC"

    goto :goto_0

    .line 475
    :cond_7
    const-string v0, "Local IDAC"

    .line 477
    goto :goto_0

    .line 481
    :cond_8
    const-string v1, "ZINITIX"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 483
    packed-switch p2, :pswitch_data_4

    goto :goto_0

    .line 485
    :pswitch_a
    const-string v0, "DND"

    .line 486
    goto :goto_0

    .line 488
    :pswitch_b
    const-string v0, "SDND"

    .line 489
    goto/16 :goto_0

    .line 493
    :cond_9
    const-string v1, "IMAGIS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 495
    packed-switch p2, :pswitch_data_5

    goto/16 :goto_0

    .line 497
    :pswitch_c
    const-string v0, "REFERENCE"

    .line 498
    goto/16 :goto_0

    .line 502
    :cond_a
    const-string v1, "STM"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    packed-switch p2, :pswitch_data_6

    goto/16 :goto_0

    .line 505
    :pswitch_d
    const-string v0, "Raw data"

    .line 506
    goto/16 :goto_0

    .line 508
    :pswitch_e
    const-string v0, "Cx data"

    .line 509
    goto/16 :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 431
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 450
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 463
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 483
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 495
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_c
    .end packed-switch

    .line 503
    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "tspVendor"    # Ljava/lang/String;
    .param p2, "readCnt"    # I

    .prologue
    .line 520
    const-string v0, "None"

    .line 521
    .local v0, "name":Ljava/lang/String;
    const-string v1, "SYNAPTICS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 524
    packed-switch p2, :pswitch_data_0

    .line 546
    :cond_0
    :goto_0
    return-object v0

    .line 526
    :pswitch_0
    const-string v0, "AbsCap"

    .line 527
    goto :goto_0

    .line 529
    :pswitch_1
    const-string v0, "AbsDelta"

    .line 530
    goto :goto_0

    .line 534
    :cond_1
    const-string v1, "STM"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 535
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 537
    :pswitch_2
    const-string v0, "Abs Cap"

    .line 538
    goto :goto_0

    .line 540
    :pswitch_3
    const-string v0, "Cb jitter"

    .line 541
    goto :goto_0

    .line 524
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 535
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private hideProgressDialog()V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "hideProgressDialog"

    const-string v2, "hide Dialog"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 215
    :cond_0
    return-void
.end method

.method private makeTspData(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "result"    # Ljava/lang/String;

    .prologue
    .line 218
    const/4 v2, 0x0

    .line 219
    .local v2, "fos":Ljava/io/FileOutputStream;
    new-instance v0, Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/sec/android/app/status/TspDataMeasure;->createFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .local v0, "FileInspection":Ljava/io/File;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 224
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 230
    if-eqz v3, :cond_2

    .line 232
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    .line 238
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 233
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 235
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 225
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 226
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 230
    if-eqz v2, :cond_0

    .line 232
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 233
    :catch_2
    move-exception v1

    .line 234
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 227
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 228
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 230
    if-eqz v2, :cond_0

    .line 232
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 233
    :catch_4
    move-exception v1

    .line 234
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 230
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v2, :cond_1

    .line 232
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 235
    :cond_1
    :goto_4
    throw v4

    .line 233
    :catch_5
    move-exception v1

    .line 234
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 230
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 227
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 225
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :cond_2
    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method private refreshExtSdCard()V
    .locals 5

    .prologue
    .line 164
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "refreshExtSdCard"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    new-instance v1, Ljava/io/File;

    const-string v2, "/storage/extSdCard/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    .local v1, "sdcard":Ljava/io/File;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 168
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 169
    return-void
.end method

.method private showProgressDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "showProgressDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 203
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 204
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 205
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 206
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private showValueDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 187
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "showValueDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "message: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 189
    .local v0, "ab":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 190
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 191
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 192
    const-string v1, "Close"

    new-instance v2, Lcom/sec/android/app/status/TspDataMeasure$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/status/TspDataMeasure$3;-><init>(Lcom/sec/android/app/status/TspDataMeasure;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 197
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method private subStringProduct()Ljava/lang/String;
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mProduct:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public startTspdataTest(Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;)V
    .locals 10
    .param p1, "type"    # Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    invoke-virtual {v1}, Lcom/sec/android/app/status/TspModuleDevice;->isExternalMemoryExist()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mModuleDevice:Lcom/sec/android/app/status/TspModuleDevice;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/status/TspModuleDevice;->isMountedStorage(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "StartTspdataTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StartTspdata Type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v1, Lcom/sec/android/app/status/TspDataMeasure$4;->$SwitchMap$com$sec$android$app$status$TspDataMeasure$TSP_DATATYPE:[I

    invoke-virtual {p1}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 131
    :goto_0
    return-void

    .line 87
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reading TSP node data\nRead1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "message":Ljava/lang/String;
    const-string v1, "SUPPORT_RUN_REF"

    invoke-static {v1}, Lcom/sec/android/app/lcdtest/support/Support$Feature;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRead2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94
    :cond_0
    const-string v1, "SYNAPTICS"

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRead3: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRead4: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/status/TspDataMeasure;->getTspDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    :cond_1
    const-string v1, "Reading"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/status/TspDataMeasure;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->getId()I

    move-result v2

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 105
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reading TSP Hover node data\nRead1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRead2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getTspHoverDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 108
    .restart local v0    # "message":Ljava/lang/String;
    const-string v1, "Reading"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/status/TspDataMeasure;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    .line 109
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 110
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->getId()I

    move-result v2

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 114
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reading Side Touch Data\nRead1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/status/TspDataMeasure;->getSideTouchDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRead2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspVendor:Ljava/lang/String;

    invoke-direct {p0, v2, v6}, Lcom/sec/android/app/status/TspDataMeasure;->getSideTouchDataName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    .restart local v0    # "message":Ljava/lang/String;
    const-string v1, "Reading"

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/status/TspDataMeasure;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mTspNotiDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/sec/android/app/status/TspDataMeasure$TSP_DATATYPE;->getId()I

    move-result v2

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 128
    .end local v0    # "message":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->mContext:Landroid/content/Context;

    const-string v2, "Please insert SD card."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/status/TspDataMeasure;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "StartTspdataTest"

    const-string v3, "SD Card is not Detect"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/status/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
