.class Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;
.super Ljava/lang/Object;
.source "SimpleVersionTMO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/version/SimpleVersionTMO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OemCommands"
.end annotation


# instance fields
.field final OEM_SERVM_FUNCTAG:C

.field final OEM_SM_ACTION:C

.field final OEM_SM_DUMMY:C

.field final OEM_SM_END_MODE_MESSAGE:C

.field final OEM_SM_ENTER_MODE_MESSAGE:C

.field final OEM_SM_GET_DISPLAY_DATA_MESSAGE:C

.field final OEM_SM_PROCESS_KEY_MESSAGE:C

.field final OEM_SM_QUERY:C

.field final OEM_SM_TYPE_MONITOR:C

.field final OEM_SM_TYPE_NAM_EDIT:C

.field final OEM_SM_TYPE_PHONE_TEST:C

.field final OEM_SM_TYPE_SUB_ALL_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_BAND_SEL_ENTER:C

.field final OEM_SM_TYPE_SUB_BATTERY_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_BLUETOOTH_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_CIPHERING_PROTECTION_ENTER:C

.field final OEM_SM_TYPE_SUB_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_PRECONFIG_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_RESET_ENTER:C

.field final OEM_SM_TYPE_SUB_FACTORY_VF_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_GCF_TESTMODE_ENTER:C

.field final OEM_SM_TYPE_SUB_GET_SELLOUT_SMS_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_GPSONE_SS_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_GSM_FACTORY_AUDIO_LB_ENTER:C

.field final OEM_SM_TYPE_SUB_IMEI_READ_ENTER:C

.field final OEM_SM_TYPE_SUB_INTEGRITY_PROTECTION_ENTER:C

.field final OEM_SM_TYPE_SUB_MELODY_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_MP3_TEST_ENTER:C

.field final OEM_SM_TYPE_SUB_RRC_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_RSC_FILE_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_DISABLE_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_ENABLE_ENTER:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_PRODUCT_MODE_ON:C

.field final OEM_SM_TYPE_SUB_SELLOUT_SMS_TEST_MODE_ON:C

.field final OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TFS4_EXPLORE_ENTER:C

.field final OEM_SM_TYPE_SUB_TOTAL_CALL_TIME_INFO_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_AUTO_ANSWER_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

.field final OEM_SM_TYPE_SUB_TST_NV_RESET_ENTER:C

.field final OEM_SM_TYPE_SUB_USB_DRIVER_ENTER:C

.field final OEM_SM_TYPE_SUB_USB_UART_DIAG_CONTROL_ENTER:C

.field final OEM_SM_TYPE_SUB_VIBRATOR_TEST_ENTER:C

.field final OEM_SM_TYPE_TEST_AUTO:C

.field final OEM_SM_TYPE_TEST_MANUAL:C


# direct methods
.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-char v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SERVM_FUNCTAG:C

    .line 64
    iput-char v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_DUMMY:C

    .line 65
    iput-char v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_ENTER_MODE_MESSAGE:C

    .line 66
    iput-char v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_END_MODE_MESSAGE:C

    .line 67
    iput-char v4, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_PROCESS_KEY_MESSAGE:C

    .line 68
    iput-char v5, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_GET_DISPLAY_DATA_MESSAGE:C

    .line 71
    iput-char v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_TEST_MANUAL:C

    .line 72
    iput-char v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_TEST_AUTO:C

    .line 73
    iput-char v4, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_NAM_EDIT:C

    .line 74
    iput-char v5, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_MONITOR:C

    .line 75
    const/4 v0, 0x5

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_PHONE_TEST:C

    .line 78
    iput-char v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_ENTER:C

    .line 79
    iput-char v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_SW_VERSION_ENTER:C

    .line 80
    iput-char v3, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_FTA_SW_VERSION_ENTER:C

    .line 81
    iput-char v4, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_FTA_HW_VERSION_ENTER:C

    .line 82
    iput-char v5, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_ALL_VERSION_ENTER:C

    .line 83
    const/4 v0, 0x5

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_BATTERY_INFO_ENTER:C

    .line 84
    const/4 v0, 0x6

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_CIPHERING_PROTECTION_ENTER:C

    .line 85
    const/4 v0, 0x7

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_INTEGRITY_PROTECTION_ENTER:C

    .line 86
    const/16 v0, 0x8

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_IMEI_READ_ENTER:C

    .line 87
    const/16 v0, 0x9

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_BLUETOOTH_TEST_ENTER:C

    .line 88
    const/16 v0, 0xa

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_VIBRATOR_TEST_ENTER:C

    .line 89
    const/16 v0, 0xb

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_MELODY_TEST_ENTER:C

    .line 90
    const/16 v0, 0xc

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_MP3_TEST_ENTER:C

    .line 91
    const/16 v0, 0xd

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_RESET_ENTER:C

    .line 92
    const/16 v0, 0xe

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_PRECONFIG_ENTER:C

    .line 93
    const/16 v0, 0xf

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TFS4_EXPLORE_ENTER:C

    .line 94
    const/16 v0, 0x11

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_RSC_FILE_VERSION_ENTER:C

    .line 95
    const/16 v0, 0x12

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_USB_DRIVER_ENTER:C

    .line 96
    const/16 v0, 0x13

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_USB_UART_DIAG_CONTROL_ENTER:C

    .line 97
    const/16 v0, 0x14

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_RRC_VERSION_ENTER:C

    .line 98
    const/16 v0, 0x15

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_GPSONE_SS_TEST_ENTER:C

    .line 99
    const/16 v0, 0x16

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_BAND_SEL_ENTER:C

    .line 100
    const/16 v0, 0x17

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_GCF_TESTMODE_ENTER:C

    .line 101
    const/16 v0, 0x18

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_GSM_FACTORY_AUDIO_LB_ENTER:C

    .line 102
    const/16 v0, 0x19

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_FACTORY_VF_TEST_ENTER:C

    .line 103
    const/16 v0, 0x1a

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TOTAL_CALL_TIME_INFO_ENTER:C

    .line 104
    const/16 v0, 0x1b

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_ENABLE_ENTER:C

    .line 105
    const/16 v0, 0x1c

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_DISABLE_ENTER:C

    .line 106
    const/16 v0, 0x1d

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_TEST_MODE_ON:C

    .line 107
    const/16 v0, 0x1e

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_SELLOUT_SMS_PRODUCT_MODE_ON:C

    .line 108
    const/16 v0, 0x1f

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_GET_SELLOUT_SMS_INFO_ENTER:C

    .line 109
    const/16 v0, 0x20

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TST_AUTO_ANSWER_ENTER:C

    .line 110
    const/16 v0, 0x21

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TST_NV_RESET_ENTER:C

    .line 111
    const/16 v0, 0x1002

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_SW_VERSION_ENTER:C

    .line 112
    const/16 v0, 0x1003

    iput-char v0, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_TYPE_SUB_TST_FTA_HW_VERSION_ENTER:C

    .line 115
    iput-char v2, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_ACTION:C

    .line 116
    iput-char v1, p0, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;->OEM_SM_QUERY:C

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/version/SimpleVersionTMO$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/version/SimpleVersionTMO$1;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/version/SimpleVersionTMO$OemCommands;-><init>()V

    return-void
.end method


# virtual methods
.method getServMEnterData(CCCC)[B
    .locals 8
    .param p1, "svcMode"    # C
    .param p2, "modeType"    # C
    .param p3, "subType"    # C
    .param p4, "query"    # C

    .prologue
    .line 119
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 120
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 121
    .local v2, "dos":Ljava/io/DataOutputStream;
    const/16 v4, 0x8

    .line 122
    .local v4, "fileSize":C
    const/4 v0, 0x4

    .line 125
    .local v0, "MODEM_GSM":C
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 126
    invoke-virtual {v2, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 127
    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 128
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 129
    invoke-virtual {v2, p2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 130
    invoke-virtual {v2, p3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 131
    invoke-virtual {v2, p4}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    :goto_0
    return-object v5

    .line 132
    :catch_0
    move-exception v3

    .line 133
    .local v3, "e":Ljava/io/IOException;
    const-string v5, "SimpleVersion"

    const-string v6, "getServMEnterData"

    const-string v7, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/lcdtest/support/LtUtil;->log_d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const/4 v5, 0x0

    goto :goto_0
.end method
