.class Lcom/sec/android/app/status/BatteryStatus$4;
.super Landroid/os/Handler;
.source "BatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/BatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/status/BatteryStatus;


# direct methods
.method constructor <init>(Lcom/sec/android/app/status/BatteryStatus;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/app/status/BatteryStatus$4;->this$0:Lcom/sec/android/app/status/BatteryStatus;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 472
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 480
    :goto_0
    return-void

    .line 474
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus$4;->this$0:Lcom/sec/android/app/status/BatteryStatus;

    invoke-virtual {v1}, Lcom/sec/android/app/status/BatteryStatus;->updateBatteryStatus()V

    .line 475
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus$4;->this$0:Lcom/sec/android/app/status/BatteryStatus;

    iget-object v1, v1, Lcom/sec/android/app/status/BatteryStatus;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 476
    .local v0, "update_msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/status/BatteryStatus$4;->this$0:Lcom/sec/android/app/status/BatteryStatus;

    iget-object v1, v1, Lcom/sec/android/app/status/BatteryStatus;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 472
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
