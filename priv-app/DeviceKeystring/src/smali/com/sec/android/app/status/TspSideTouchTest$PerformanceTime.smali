.class Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;
.super Ljava/lang/Object;
.source "TspSideTouchTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/status/TspSideTouchTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PerformanceTime"
.end annotation


# instance fields
.field private nEnd:J

.field private nStart:J

.field private nTime:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944
    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nStart:J

    .line 1945
    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nEnd:J

    .line 1946
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nTime:I

    .line 1949
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nStart:J

    .line 1950
    return-void
.end method


# virtual methods
.method public getTime()I
    .locals 4

    .prologue
    .line 1957
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nEnd:J

    .line 1958
    iget-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nEnd:J

    iget-wide v2, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nStart:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nTime:I

    .line 1959
    iget v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nTime:I

    return v0
.end method

.method public restart()V
    .locals 2

    .prologue
    .line 1953
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/status/TspSideTouchTest$PerformanceTime;->nStart:J

    .line 1954
    return-void
.end method
