.class public Lcom/android/mms/service/http/NetworkAwareThreadSafeClientConnManager;
.super Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
.source "NetworkAwareThreadSafeClientConnManager.java"


# direct methods
.method public constructor <init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;Lcom/android/mms/service/http/NameResolver;Z)V
    .locals 1
    .param p1, "params"    # Lorg/apache/http/params/HttpParams;
    .param p2, "schreg"    # Lorg/apache/http/conn/scheme/SchemeRegistry;
    .param p3, "resolver"    # Lcom/android/mms/service/http/NameResolver;
    .param p4, "shouldUseIpv6"    # Z

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 32
    iget-object v0, p0, Lcom/android/mms/service/http/NetworkAwareThreadSafeClientConnManager;->connOperator:Lorg/apache/http/conn/ClientConnectionOperator;

    check-cast v0, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;

    invoke-virtual {v0, p3}, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;->setNameResolver(Lcom/android/mms/service/http/NameResolver;)V

    .line 33
    iget-object v0, p0, Lcom/android/mms/service/http/NetworkAwareThreadSafeClientConnManager;->connOperator:Lorg/apache/http/conn/ClientConnectionOperator;

    check-cast v0, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;

    invoke-virtual {v0, p4}, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;->setShouldUseIpv6(Z)V

    .line 34
    return-void
.end method


# virtual methods
.method protected createConnectionOperator(Lorg/apache/http/conn/scheme/SchemeRegistry;)Lorg/apache/http/conn/ClientConnectionOperator;
    .locals 1
    .param p1, "schreg"    # Lorg/apache/http/conn/scheme/SchemeRegistry;

    .prologue
    .line 38
    new-instance v0, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;

    invoke-direct {v0, p1}, Lcom/android/mms/service/http/NetworkAwareClientConnectionOperator;-><init>(Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    return-object v0
.end method
