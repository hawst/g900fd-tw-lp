.class Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;
.super Ljava/lang/Object;
.source "NetworkAwareHttpClient.java"

# interfaces
.implements Lorg/apache/http/HttpRequestInterceptor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/service/http/NetworkAwareHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CurlLogger"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;


# direct methods
.method private constructor <init>(Lcom/android/mms/service/http/NetworkAwareHttpClient;)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;->this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/mms/service/http/NetworkAwareHttpClient;Lcom/android/mms/service/http/NetworkAwareHttpClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/mms/service/http/NetworkAwareHttpClient;
    .param p2, "x1"    # Lcom/android/mms/service/http/NetworkAwareHttpClient$1;

    .prologue
    .line 395
    invoke-direct {p0, p1}, Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;-><init>(Lcom/android/mms/service/http/NetworkAwareHttpClient;)V

    return-void
.end method


# virtual methods
.method public process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 2
    .param p1, "request"    # Lorg/apache/http/HttpRequest;
    .param p2, "context"    # Lorg/apache/http/protocol/HttpContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/HttpException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 399
    iget-object v1, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;->this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;

    # getter for: Lcom/android/mms/service/http/NetworkAwareHttpClient;->curlConfiguration:Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;
    invoke-static {v1}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->access$300(Lcom/android/mms/service/http/NetworkAwareHttpClient;)Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;

    move-result-object v0

    .line 400
    .local v0, "configuration":Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;
    if-eqz v0, :cond_0

    # invokes: Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->isLoggable()Z
    invoke-static {v0}, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->access$400(Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;)Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p1, Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v1, :cond_0

    .line 405
    check-cast p1, Lorg/apache/http/client/methods/HttpUriRequest;

    .end local p1    # "request":Lorg/apache/http/HttpRequest;
    const/4 v1, 0x0

    # invokes: Lcom/android/mms/service/http/NetworkAwareHttpClient;->toCurl(Lorg/apache/http/client/methods/HttpUriRequest;Z)Ljava/lang/String;
    invoke-static {p1, v1}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->access$500(Lorg/apache/http/client/methods/HttpUriRequest;Z)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->println(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->access$600(Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;Ljava/lang/String;)V

    .line 407
    :cond_0
    return-void
.end method
