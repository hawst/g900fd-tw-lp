.class public Lcom/android/mms/service/DownloadRequest;
.super Lcom/android/mms/service/MmsRequest;
.source "DownloadRequest.java"


# instance fields
.field private final mContentUri:Landroid/net/Uri;

.field private final mDownloadedIntent:Landroid/app/PendingIntent;

.field private final mLocationUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/mms/service/MmsRequest$RequestManager;JLjava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "manager"    # Lcom/android/mms/service/MmsRequest$RequestManager;
    .param p2, "subId"    # J
    .param p4, "locationUrl"    # Ljava/lang/String;
    .param p5, "contentUri"    # Landroid/net/Uri;
    .param p6, "downloadedIntent"    # Landroid/app/PendingIntent;
    .param p7, "creator"    # Ljava/lang/String;
    .param p8, "configOverrides"    # Landroid/os/Bundle;

    .prologue
    .line 61
    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/android/mms/service/MmsRequest;-><init>(Lcom/android/mms/service/MmsRequest$RequestManager;Landroid/net/Uri;JLjava/lang/String;Landroid/os/Bundle;)V

    .line 62
    iput-object p4, p0, Lcom/android/mms/service/DownloadRequest;->mLocationUrl:Ljava/lang/String;

    .line 63
    iput-object p6, p0, Lcom/android/mms/service/DownloadRequest;->mDownloadedIntent:Landroid/app/PendingIntent;

    .line 64
    iput-object p5, p0, Lcom/android/mms/service/DownloadRequest;->mContentUri:Landroid/net/Uri;

    .line 65
    return-void
.end method

.method private storeInboxMessage(Landroid/content/Context;I[B)V
    .locals 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "result"    # I
    .param p3, "response"    # [B

    .prologue
    .line 107
    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    array-length v4, v0

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v12

    .line 112
    .local v12, "identity":J
    :try_start_0
    new-instance v4, Lcom/google/android/mms/pdu/PduParser;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    invoke-virtual {v4}, Lcom/google/android/mms/pdu/PduParser;->parse()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v3

    .line 113
    .local v3, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-eqz v3, :cond_2

    instance-of v4, v3, Lcom/google/android/mms/pdu/RetrieveConf;

    if-nez v4, :cond_3

    .line 114
    :cond_2
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: invalid parsed PDU"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 118
    :cond_3
    :try_start_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v2

    .line 119
    .local v2, "persister":Lcom/google/android/mms/pdu/PduPersister;
    sget-object v4, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZZLjava/util/HashMap;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/mms/service/DownloadRequest;->mMessageUri:Landroid/net/Uri;

    .line 125
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mms/service/DownloadRequest;->mMessageUri:Landroid/net/Uri;

    if-nez v4, :cond_4

    .line 126
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: can not persist message"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 130
    :cond_4
    :try_start_2
    new-instance v7, Landroid/content/ContentValues;

    const/4 v4, 0x5

    invoke-direct {v7, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 131
    .local v7, "values":Landroid/content/ContentValues;
    const-string v4, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    const-wide/16 v14, 0x3e8

    div-long/2addr v8, v14

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 132
    const-string v4, "read"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    const-string v4, "seen"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/mms/service/DownloadRequest;->mCreator:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 135
    const-string v4, "creator"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/mms/service/DownloadRequest;->mCreator:Ljava/lang/String;

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_5
    const-string v4, "sub_id"

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/mms/service/DownloadRequest;->mSubId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v7, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 138
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/mms/service/DownloadRequest;->mMessageUri:Landroid/net/Uri;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p1

    invoke-static/range {v4 .. v9}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_6

    .line 145
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: can not update message"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    const-string v6, "m_type=? AND ct_l =?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const/16 v11, 0x82

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v9

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/mms/service/DownloadRequest;->mLocationUrl:Ljava/lang/String;

    aput-object v11, v8, v9

    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v6, v8}, Lcom/google/android/mms/util/SqliteWrapper;->delete(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 157
    .end local v2    # "persister":Lcom/google/android/mms/pdu/PduPersister;
    .end local v3    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    .end local v7    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v10

    .line 158
    .local v10, "e":Lcom/google/android/mms/MmsException;
    :try_start_3
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: can not persist message"

    invoke-static {v4, v5, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 159
    .end local v10    # "e":Lcom/google/android/mms/MmsException;
    :catch_1
    move-exception v10

    .line 160
    .local v10, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: can not update message"

    invoke-static {v4, v5, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 161
    .end local v10    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_2
    move-exception v10

    .line 162
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_5
    const-string v4, "MmsService"

    const-string v5, "DownloadRequest.updateStatus: can not parse response"

    invoke-static {v4, v5, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 164
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method


# virtual methods
.method protected doHttp(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Lcom/android/mms/service/ApnSettings;)[B
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "netMgr"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p3, "apn"    # Lcom/android/mms/service/ApnSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v3, p0, Lcom/android/mms/service/DownloadRequest;->mLocationUrl:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/mms/service/DownloadRequest;->doHttpForResolvedAddresses(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;[BILcom/android/mms/service/ApnSettings;)[B

    move-result-object v0

    return-object v0
.end method

.method protected getPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/mms/service/DownloadRequest;->mDownloadedIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method protected getRunningQueue()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method protected prepareForHttpRequest()Z
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method protected revokeUriPermission(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/android/mms/service/DownloadRequest;->mContentUri:Landroid/net/Uri;

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    .line 207
    return-void
.end method

.method protected transferResponse(Landroid/content/Intent;[B)Z
    .locals 2
    .param p1, "fillIn"    # Landroid/content/Intent;
    .param p2, "response"    # [B

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/mms/service/DownloadRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    iget-object v1, p0, Lcom/android/mms/service/DownloadRequest;->mContentUri:Landroid/net/Uri;

    invoke-interface {v0, v1, p2}, Lcom/android/mms/service/MmsRequest$RequestManager;->writePduToContentUri(Landroid/net/Uri;[B)Z

    move-result v0

    return v0
.end method

.method public tryDownloadingByCarrierApp(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 178
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/telephony/TelephonyManager;

    .line 180
    .local v11, "telephonyManager":Landroid/telephony/TelephonyManager;
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.provider.Telephony.MMS_DOWNLOAD"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v11, v1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v10

    .line 184
    .local v10, "carrierPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/DownloadRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    invoke-interface {v0, p0}, Lcom/android/mms/service/MmsRequest$RequestManager;->addRunning(Lcom/android/mms/service/MmsRequest;)V

    .line 202
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    const-string v0, "android.provider.Telephony.extra.MMS_LOCATION_URL"

    iget-object v2, p0, Lcom/android/mms/service/DownloadRequest;->mLocationUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v0, "android.provider.Telephony.extra.MMS_CONTENT_URI"

    iget-object v2, p0, Lcom/android/mms/service/DownloadRequest;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 190
    const/high16 v0, 0x8000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 191
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    const-string v3, "android.permission.RECEIVE_MMS"

    const/16 v4, 0x12

    iget-object v5, p0, Lcom/android/mms/service/DownloadRequest;->mCarrierAppResultReceiver:Landroid/content/BroadcastReceiver;

    move-object v0, p1

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;ILandroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected updateStatus(Landroid/content/Context;I[B)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "result"    # I
    .param p3, "response"    # [B

    .prologue
    .line 90
    iget-object v0, p0, Lcom/android/mms/service/DownloadRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    invoke-interface {v0}, Lcom/android/mms/service/MmsRequest$RequestManager;->getAutoPersistingPref()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/service/DownloadRequest;->storeInboxMessage(Landroid/content/Context;I[B)V

    .line 93
    :cond_0
    return-void
.end method
