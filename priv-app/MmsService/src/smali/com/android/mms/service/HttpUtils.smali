.class public Lcom/android/mms/service/HttpUtils;
.super Ljava/lang/Object;
.source "HttpUtils.java"


# static fields
.field private static final MACRO_P:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 380
    const-string v0, "##(\\S+)##"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/mms/service/HttpUtils;->MACRO_P:Ljava/util/regex/Pattern;

    return-void
.end method

.method private static addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V
    .locals 3
    .param p0, "builder"    # Ljava/lang/StringBuilder;
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 369
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/mms/service/HttpUtils;->convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 370
    .local v1, "language":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 371
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 374
    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    .end local v0    # "country":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static convertObsoleteLanguageCodeToNew(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "langCode"    # Ljava/lang/String;

    .prologue
    .line 352
    if-nez p0, :cond_1

    .line 353
    const/4 p0, 0x0

    .line 365
    .end local p0    # "langCode":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 355
    .restart local p0    # "langCode":Ljava/lang/String;
    :cond_1
    const-string v0, "iw"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 357
    const-string p0, "he"

    goto :goto_0

    .line 358
    :cond_2
    const-string v0, "in"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    const-string p0, "id"

    goto :goto_0

    .line 361
    :cond_3
    const-string v0, "ji"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    const-string p0, "yi"

    goto :goto_0
.end method

.method private static createHttpClient(Landroid/content/Context;Lcom/android/mms/service/http/NameResolver;ZLcom/android/mms/service/MmsConfig$Overridden;)Lcom/android/mms/service/http/NetworkAwareHttpClient;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolver"    # Lcom/android/mms/service/http/NameResolver;
    .param p2, "useIpv6"    # Z
    .param p3, "mmsConfig"    # Lcom/android/mms/service/MmsConfig$Overridden;

    .prologue
    .line 307
    invoke-virtual {p3}, Lcom/android/mms/service/MmsConfig$Overridden;->getUserAgent()Ljava/lang/String;

    move-result-object v3

    .line 308
    .local v3, "userAgent":Ljava/lang/String;
    invoke-static {v3, p0, p1, p2}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->newInstance(Ljava/lang/String;Landroid/content/Context;Lcom/android/mms/service/http/NameResolver;Z)Lcom/android/mms/service/http/NetworkAwareHttpClient;

    move-result-object v0

    .line 310
    .local v0, "client":Lcom/android/mms/service/http/NetworkAwareHttpClient;
    invoke-virtual {v0}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    .line 311
    .local v1, "params":Lorg/apache/http/params/HttpParams;
    const-string v4, "UTF-8"

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 314
    invoke-virtual {p3}, Lcom/android/mms/service/MmsConfig$Overridden;->getHttpSocketTimeout()I

    move-result v2

    .line 316
    .local v2, "soTimeout":I
    const-string v4, "MmsService"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 317
    const-string v4, "MmsService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpUtils: createHttpClient w/ socket timeout "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms, UA="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_0
    invoke-static {v1, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 321
    return-object v0
.end method

.method public static getCurrentAcceptLanguage(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 335
    .local v0, "buffer":Ljava/lang/StringBuilder;
    invoke-static {v0, p0}, Lcom/android/mms/service/HttpUtils;->addLocaleToHttpAcceptLanguage(Ljava/lang/StringBuilder;Ljava/util/Locale;)V

    .line 337
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, p0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 338
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 339
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    :cond_0
    const-string v1, "en-US"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getMethodString(I)Ljava/lang/String;
    .locals 1
    .param p0, "method"    # I

    .prologue
    .line 295
    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    const-string v0, "POST"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    if-ne p0, v0, :cond_1

    const-string v0, "GET"

    goto :goto_0

    :cond_1
    const-string v0, "UNKNOWN"

    goto :goto_0
.end method

.method public static httpConnection(Landroid/content/Context;Ljava/lang/String;[BIZLjava/lang/String;ILcom/android/mms/service/http/NameResolver;ZLcom/android/mms/service/MmsConfig$Overridden;)[B
    .locals 40
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "pdu"    # [B
    .param p3, "method"    # I
    .param p4, "isProxySet"    # Z
    .param p5, "proxyHost"    # Ljava/lang/String;
    .param p6, "proxyPort"    # I
    .param p7, "resolver"    # Lcom/android/mms/service/http/NameResolver;
    .param p8, "useIpv6"    # Z
    .param p9, "mmsConfig"    # Lcom/android/mms/service/MmsConfig$Overridden;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 90
    invoke-static/range {p3 .. p3}, Lcom/android/mms/service/HttpUtils;->getMethodString(I)Ljava/lang/String;

    move-result-object v17

    .line 91
    .local v17, "methodString":Ljava/lang/String;
    const-string v35, "MmsService"

    const/16 v36, 0x2

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v35

    if-eqz v35, :cond_5

    .line 92
    const-string v36, "MmsService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: request param list\nurl="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "method="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "isProxySet="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "proxyHost="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "proxyPort="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "size="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    if-eqz p2, :cond_4

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v35, v0

    :goto_0
    move-object/from16 v0, v37

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :goto_1
    const/4 v8, 0x0

    .line 106
    .local v8, "client":Lcom/android/mms/service/http/NetworkAwareHttpClient;
    :try_start_0
    new-instance v14, Ljava/net/URI;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 107
    .local v14, "hostUrl":Ljava/net/URI;
    new-instance v30, Lorg/apache/http/HttpHost;

    invoke-virtual {v14}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v35

    invoke-virtual {v14}, Ljava/net/URI;->getPort()I

    move-result v36

    const-string v37, "http"

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    move/from16 v2, v36

    move-object/from16 v3, v37

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 109
    .local v30, "target":Lorg/apache/http/HttpHost;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    move/from16 v2, p8

    move-object/from16 v3, p9

    invoke-static {v0, v1, v2, v3}, Lcom/android/mms/service/HttpUtils;->createHttpClient(Landroid/content/Context;Lcom/android/mms/service/http/NameResolver;ZLcom/android/mms/service/MmsConfig$Overridden;)Lcom/android/mms/service/http/NetworkAwareHttpClient;

    move-result-object v8

    .line 110
    const/16 v25, 0x0

    .line 112
    .local v25, "req":Lorg/apache/http/HttpRequest;
    packed-switch p3, :pswitch_data_0

    .line 127
    :goto_2
    invoke-virtual {v8}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v22

    .line 128
    .local v22, "params":Lorg/apache/http/params/HttpParams;
    if-eqz p4, :cond_0

    .line 129
    new-instance v35, Lorg/apache/http/HttpHost;

    move-object/from16 v0, v35

    move-object/from16 v1, p5

    move/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lorg/apache/http/HttpHost;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, v22

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Lorg/apache/http/conn/params/ConnRouteParams;->setDefaultProxy(Lorg/apache/http/params/HttpParams;Lorg/apache/http/HttpHost;)V

    .line 131
    :cond_0
    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lorg/apache/http/HttpRequest;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 134
    const-string v35, "Accept"

    const-string v36, "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"

    move-object/from16 v0, v25

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual/range {p9 .. p9}, Lcom/android/mms/service/MmsConfig$Overridden;->getUaProfTagName()Ljava/lang/String;

    move-result-object v33

    .line 138
    .local v33, "xWapProfileTagName":Ljava/lang/String;
    invoke-virtual/range {p9 .. p9}, Lcom/android/mms/service/MmsConfig$Overridden;->getUaProfUrl()Ljava/lang/String;

    move-result-object v34

    .line 139
    .local v34, "xWapProfileUrl":Ljava/lang/String;
    if-eqz v34, :cond_2

    .line 140
    const-string v35, "MmsService"

    const/16 v36, 0x2

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v35

    if-eqz v35, :cond_1

    .line 141
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: xWapProfUrl="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_1
    move-object/from16 v0, v25

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_2
    invoke-virtual/range {p9 .. p9}, Lcom/android/mms/service/MmsConfig$Overridden;->getHttpParams()Ljava/lang/String;

    move-result-object v12

    .line 155
    .local v12, "extraHttpParams":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_6

    .line 157
    const-string v35, "\\|"

    move-object/from16 v0, v35

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 158
    .local v20, "paramList":[Ljava/lang/String;
    move-object/from16 v4, v20

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_3
    move/from16 v0, v16

    if-ge v15, v0, :cond_6

    aget-object v21, v4, v15

    .line 159
    .local v21, "paramPair":Ljava/lang/String;
    const-string v35, ":"

    const/16 v36, 0x2

    move-object/from16 v0, v21

    move-object/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v28

    .line 160
    .local v28, "splitPair":[Ljava/lang/String;
    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v35, v0

    const/16 v36, 0x2

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_3

    .line 161
    const/16 v35, 0x0

    aget-object v35, v28, v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 162
    .local v18, "name":Ljava/lang/String;
    const/16 v35, 0x1

    aget-object v35, v28, v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p9

    invoke-static {v0, v1, v2}, Lcom/android/mms/service/HttpUtils;->resolveMacro(Landroid/content/Context;Ljava/lang/String;Lcom/android/mms/service/MmsConfig$Overridden;)Ljava/lang/String;

    move-result-object v32

    .line 163
    .local v32, "value":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_3

    invoke-static/range {v32 .. v32}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v35

    if-nez v35, :cond_3

    .line 164
    move-object/from16 v0, v25

    move-object/from16 v1, v18

    move-object/from16 v2, v32

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 158
    .end local v18    # "name":Ljava/lang/String;
    .end local v32    # "value":Ljava/lang/String;
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 92
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v8    # "client":Lcom/android/mms/service/http/NetworkAwareHttpClient;
    .end local v12    # "extraHttpParams":Ljava/lang/String;
    .end local v14    # "hostUrl":Ljava/net/URI;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    .end local v20    # "paramList":[Ljava/lang/String;
    .end local v21    # "paramPair":Ljava/lang/String;
    .end local v22    # "params":Lorg/apache/http/params/HttpParams;
    .end local v25    # "req":Lorg/apache/http/HttpRequest;
    .end local v28    # "splitPair":[Ljava/lang/String;
    .end local v30    # "target":Lorg/apache/http/HttpHost;
    .end local v33    # "xWapProfileTagName":Ljava/lang/String;
    .end local v34    # "xWapProfileUrl":Ljava/lang/String;
    :cond_4
    const/16 v35, 0x0

    goto/16 :goto_0

    .line 100
    :cond_5
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, " "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 114
    .restart local v8    # "client":Lcom/android/mms/service/http/NetworkAwareHttpClient;
    .restart local v14    # "hostUrl":Ljava/net/URI;
    .restart local v25    # "req":Lorg/apache/http/HttpRequest;
    .restart local v30    # "target":Lorg/apache/http/HttpHost;
    :pswitch_0
    :try_start_1
    new-instance v11, Lorg/apache/http/entity/ByteArrayEntity;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 116
    .local v11, "entity":Lorg/apache/http/entity/ByteArrayEntity;
    const-string v35, "application/vnd.wap.mms-message"

    move-object/from16 v0, v35

    invoke-virtual {v11, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 117
    new-instance v23, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 118
    .local v23, "post":Lorg/apache/http/client/methods/HttpPost;
    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 119
    move-object/from16 v25, v23

    .line 120
    goto/16 :goto_2

    .line 122
    .end local v11    # "entity":Lorg/apache/http/entity/ByteArrayEntity;
    .end local v23    # "post":Lorg/apache/http/client/methods/HttpPost;
    :pswitch_1
    new-instance v25, Lorg/apache/http/client/methods/HttpGet;

    .end local v25    # "req":Lorg/apache/http/HttpRequest;
    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .restart local v25    # "req":Lorg/apache/http/HttpRequest;
    goto/16 :goto_2

    .line 169
    .restart local v12    # "extraHttpParams":Ljava/lang/String;
    .restart local v22    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v33    # "xWapProfileTagName":Ljava/lang/String;
    .restart local v34    # "xWapProfileUrl":Ljava/lang/String;
    :cond_6
    const-string v35, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/android/mms/service/HttpUtils;->getCurrentAcceptLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v25

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    invoke-interface {v0, v1, v2}, Lorg/apache/http/HttpRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->execute(Lorg/apache/http/HttpHost;Lorg/apache/http/HttpRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v26

    .line 172
    .local v26, "response":Lorg/apache/http/HttpResponse;
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v29

    .line 173
    .local v29, "status":Lorg/apache/http/StatusLine;
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    .line 174
    .local v11, "entity":Lorg/apache/http/HttpEntity;
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: status="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, " size="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    if-eqz v11, :cond_8

    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v36

    :goto_4
    move-object/from16 v0, v38

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const-string v35, "MmsService"

    const/16 v36, 0x2

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v35

    if-eqz v35, :cond_9

    .line 177
    invoke-interface/range {v25 .. v25}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    .local v4, "arr$":[Lorg/apache/http/Header;
    array-length v0, v4

    move/from16 v16, v0

    .restart local v16    # "len$":I
    const/4 v15, 0x0

    .restart local v15    # "i$":I
    :goto_5
    move/from16 v0, v16

    if-ge v15, v0, :cond_9

    aget-object v13, v4, v15

    .line 178
    .local v13, "header":Lorg/apache/http/Header;
    if-eqz v13, :cond_7

    .line 179
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: header "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-interface {v13}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v37, "="

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-interface {v13}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 177
    :cond_7
    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    .line 174
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v13    # "header":Lorg/apache/http/Header;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    :cond_8
    const-wide/16 v36, -0x1

    goto :goto_4

    .line 184
    :cond_9
    const/4 v5, 0x0

    .line 185
    .local v5, "body":[B
    if-eqz v11, :cond_f

    .line 187
    :try_start_2
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v36

    const-wide/16 v38, 0x0

    cmp-long v35, v36, v38

    if-lez v35, :cond_a

    .line 188
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v35, v0

    move/from16 v0, v35

    new-array v5, v0, [B

    .line 189
    new-instance v9, Ljava/io/DataInputStream;

    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-direct {v9, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 191
    .local v9, "dis":Ljava/io/DataInputStream;
    :try_start_3
    invoke-virtual {v9, v5}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 194
    :try_start_4
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 201
    .end local v9    # "dis":Ljava/io/DataInputStream;
    :cond_a
    :goto_6
    :try_start_5
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->isChunked()Z

    move-result v35

    if-eqz v35, :cond_e

    .line 202
    const-string v35, "MmsService"

    const-string v36, "HttpUtils: transfer encoding is chunked"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual/range {p9 .. p9}, Lcom/android/mms/service/MmsConfig$Overridden;->getMaxMessageSize()I

    move-result v7

    .line 204
    .local v7, "bytesTobeRead":I
    new-array v0, v7, [B

    move-object/from16 v31, v0

    .line 205
    .local v31, "tempBody":[B
    new-instance v9, Ljava/io/DataInputStream;

    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-direct {v9, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 207
    .restart local v9    # "dis":Ljava/io/DataInputStream;
    const/4 v6, 0x0

    .line 208
    .local v6, "bytesRead":I
    const/16 v19, 0x0

    .line 209
    .local v19, "offset":I
    const/16 v24, 0x0

    .line 212
    .local v24, "readError":Z
    :cond_b
    :try_start_6
    move-object/from16 v0, v31

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1, v7}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result v6

    .line 218
    if-lez v6, :cond_c

    .line 219
    sub-int/2addr v7, v6

    .line 220
    add-int v19, v19, v6

    .line 222
    :cond_c
    if-ltz v6, :cond_d

    if-gtz v7, :cond_b

    .line 223
    :cond_d
    :goto_7
    const/16 v35, -0x1

    move/from16 v0, v35

    if-ne v6, v0, :cond_14

    if-lez v19, :cond_14

    if-nez v24, :cond_14

    .line 226
    :try_start_7
    move/from16 v0, v19

    new-array v5, v0, [B

    .line 227
    const/16 v35, 0x0

    const/16 v36, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v35

    move/from16 v2, v36

    move/from16 v3, v19

    invoke-static {v0, v1, v5, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 228
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: Chunked response length "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 234
    :goto_8
    :try_start_8
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 241
    .end local v6    # "bytesRead":I
    .end local v7    # "bytesTobeRead":I
    .end local v9    # "dis":Ljava/io/DataInputStream;
    .end local v19    # "offset":I
    .end local v24    # "readError":Z
    .end local v31    # "tempBody":[B
    :cond_e
    :goto_9
    if-eqz v11, :cond_f

    .line 242
    :try_start_9
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 246
    :cond_f
    invoke-interface/range {v29 .. v29}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v35

    const/16 v36, 0xc8

    move/from16 v0, v35

    move/from16 v1, v36

    if-eq v0, v1, :cond_19

    .line 247
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v27, "sb":Ljava/lang/StringBuilder;
    if-eqz v5, :cond_10

    .line 249
    const-string v35, "response: text="

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    new-instance v36, Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>([B)V

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const/16 v36, 0xa

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 251
    :cond_10
    invoke-interface/range {v25 .. v25}, Lorg/apache/http/HttpRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    .restart local v4    # "arr$":[Lorg/apache/http/Header;
    array-length v0, v4

    move/from16 v16, v0

    .restart local v16    # "len$":I
    const/4 v15, 0x0

    .restart local v15    # "i$":I
    :goto_a
    move/from16 v0, v16

    if-ge v15, v0, :cond_15

    aget-object v13, v4, v15

    .line 252
    .restart local v13    # "header":Lorg/apache/http/Header;
    if-eqz v13, :cond_11

    .line 253
    const-string v35, "req header: "

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface {v13}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const/16 v36, 0x3d

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface {v13}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const/16 v36, 0xa

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 251
    :cond_11
    add-int/lit8 v15, v15, 0x1

    goto :goto_a

    .line 195
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v13    # "header":Lorg/apache/http/Header;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    .restart local v9    # "dis":Ljava/io/DataInputStream;
    :catch_0
    move-exception v10

    .line 196
    .local v10, "e":Ljava/io/IOException;
    :try_start_a
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: Error closing input stream: "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_6

    .line 241
    .end local v9    # "dis":Ljava/io/DataInputStream;
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v35

    if-eqz v11, :cond_12

    .line 242
    :try_start_b
    invoke-interface {v11}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_12
    throw v35
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 281
    .end local v5    # "body":[B
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v12    # "extraHttpParams":Ljava/lang/String;
    .end local v14    # "hostUrl":Ljava/net/URI;
    .end local v22    # "params":Lorg/apache/http/params/HttpParams;
    .end local v25    # "req":Lorg/apache/http/HttpRequest;
    .end local v26    # "response":Lorg/apache/http/HttpResponse;
    .end local v29    # "status":Lorg/apache/http/StatusLine;
    .end local v30    # "target":Lorg/apache/http/HttpHost;
    .end local v33    # "xWapProfileTagName":Ljava/lang/String;
    .end local v34    # "xWapProfileUrl":Ljava/lang/String;
    :catch_1
    move-exception v10

    .line 282
    .restart local v10    # "e":Ljava/io/IOException;
    :try_start_c
    const-string v35, "MmsService"

    const-string v36, "HttpUtils: IO failure"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    new-instance v35, Lcom/android/mms/service/exception/MmsHttpException;

    move-object/from16 v0, v35

    invoke-direct {v0, v10}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/Throwable;)V

    throw v35
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 288
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v35

    if-eqz v8, :cond_13

    .line 289
    invoke-virtual {v8}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->close()V

    :cond_13
    throw v35

    .line 193
    .restart local v5    # "body":[B
    .restart local v9    # "dis":Ljava/io/DataInputStream;
    .restart local v11    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v12    # "extraHttpParams":Ljava/lang/String;
    .restart local v14    # "hostUrl":Ljava/net/URI;
    .restart local v22    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v25    # "req":Lorg/apache/http/HttpRequest;
    .restart local v26    # "response":Lorg/apache/http/HttpResponse;
    .restart local v29    # "status":Lorg/apache/http/StatusLine;
    .restart local v30    # "target":Lorg/apache/http/HttpHost;
    .restart local v33    # "xWapProfileTagName":Ljava/lang/String;
    .restart local v34    # "xWapProfileUrl":Ljava/lang/String;
    :catchall_2
    move-exception v35

    .line 194
    :try_start_d
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 198
    :goto_b
    :try_start_e
    throw v35

    .line 195
    :catch_2
    move-exception v10

    .line 196
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v36, "MmsService"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "HttpUtils: Error closing input stream: "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_b

    .line 213
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v6    # "bytesRead":I
    .restart local v7    # "bytesTobeRead":I
    .restart local v19    # "offset":I
    .restart local v24    # "readError":Z
    .restart local v31    # "tempBody":[B
    :catch_3
    move-exception v10

    .line 214
    .restart local v10    # "e":Ljava/io/IOException;
    const/16 v24, 0x1

    .line 215
    :try_start_f
    const-string v35, "MmsService"

    const-string v36, "HttpUtils: error reading input stream"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto/16 :goto_7

    .line 233
    .end local v10    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v35

    .line 234
    :try_start_10
    invoke-virtual {v9}, Ljava/io/DataInputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 237
    :goto_c
    :try_start_11
    throw v35
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 230
    :cond_14
    :try_start_12
    const-string v35, "MmsService"

    const-string v36, "HttpUtils: Response entity too large or empty"

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    goto/16 :goto_8

    .line 235
    :catch_4
    move-exception v10

    .line 236
    .restart local v10    # "e":Ljava/io/IOException;
    :try_start_13
    const-string v35, "MmsService"

    const-string v36, "HttpUtils: Error closing input stream"

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_9

    .line 235
    .end local v10    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v10

    .line 236
    .restart local v10    # "e":Ljava/io/IOException;
    const-string v36, "MmsService"

    const-string v37, "HttpUtils: Error closing input stream"

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto :goto_c

    .line 260
    .end local v6    # "bytesRead":I
    .end local v7    # "bytesTobeRead":I
    .end local v9    # "dis":Ljava/io/DataInputStream;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v19    # "offset":I
    .end local v24    # "readError":Z
    .end local v31    # "tempBody":[B
    .restart local v4    # "arr$":[Lorg/apache/http/Header;
    .restart local v15    # "i$":I
    .restart local v16    # "len$":I
    .restart local v27    # "sb":Ljava/lang/StringBuilder;
    :cond_15
    :try_start_14
    invoke-interface/range {v26 .. v26}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v4

    array-length v0, v4

    move/from16 v16, v0

    const/4 v15, 0x0

    :goto_d
    move/from16 v0, v16

    if-ge v15, v0, :cond_17

    aget-object v13, v4, v15

    .line 261
    .restart local v13    # "header":Lorg/apache/http/Header;
    if-eqz v13, :cond_16

    .line 262
    const-string v35, "resp header: "

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface {v13}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const/16 v36, 0x3d

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface {v13}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const/16 v36, 0xa

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 260
    :cond_16
    add-int/lit8 v15, v15, 0x1

    goto :goto_d

    .line 269
    .end local v13    # "header":Lorg/apache/http/Header;
    :cond_17
    const-string v36, "MmsService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: error response -- \nmStatusCode="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface/range {v29 .. v29}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v37

    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "reason="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-interface/range {v29 .. v29}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "url="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "method="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "isProxySet="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "proxyHost="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v37, "proxyPort="

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    if-eqz v27, :cond_18

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "\n"

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    :goto_e
    move-object/from16 v0, v37

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    new-instance v35, Lcom/android/mms/service/exception/MmsHttpException;

    invoke-interface/range {v29 .. v29}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;)V

    throw v35
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_14 .. :try_end_14} :catch_6
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 284
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v5    # "body":[B
    .end local v11    # "entity":Lorg/apache/http/HttpEntity;
    .end local v12    # "extraHttpParams":Ljava/lang/String;
    .end local v14    # "hostUrl":Ljava/net/URI;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    .end local v22    # "params":Lorg/apache/http/params/HttpParams;
    .end local v25    # "req":Lorg/apache/http/HttpRequest;
    .end local v26    # "response":Lorg/apache/http/HttpResponse;
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    .end local v29    # "status":Lorg/apache/http/StatusLine;
    .end local v30    # "target":Lorg/apache/http/HttpHost;
    .end local v33    # "xWapProfileTagName":Ljava/lang/String;
    .end local v34    # "xWapProfileUrl":Ljava/lang/String;
    :catch_6
    move-exception v10

    .line 285
    .local v10, "e":Ljava/net/URISyntaxException;
    :try_start_15
    const-string v35, "MmsService"

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "HttpUtils: invalid url "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v35 .. v36}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    new-instance v35, Lcom/android/mms/service/exception/MmsHttpException;

    new-instance v36, Ljava/lang/StringBuilder;

    invoke-direct/range {v36 .. v36}, Ljava/lang/StringBuilder;-><init>()V

    const-string v37, "Invalid url "

    invoke-virtual/range {v36 .. v37}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;)V

    throw v35
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    .line 269
    .end local v10    # "e":Ljava/net/URISyntaxException;
    .restart local v4    # "arr$":[Lorg/apache/http/Header;
    .restart local v5    # "body":[B
    .restart local v11    # "entity":Lorg/apache/http/HttpEntity;
    .restart local v12    # "extraHttpParams":Ljava/lang/String;
    .restart local v14    # "hostUrl":Ljava/net/URI;
    .restart local v15    # "i$":I
    .restart local v16    # "len$":I
    .restart local v22    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v25    # "req":Lorg/apache/http/HttpRequest;
    .restart local v26    # "response":Lorg/apache/http/HttpResponse;
    .restart local v27    # "sb":Ljava/lang/StringBuilder;
    .restart local v29    # "status":Lorg/apache/http/StatusLine;
    .restart local v30    # "target":Lorg/apache/http/HttpHost;
    .restart local v33    # "xWapProfileTagName":Ljava/lang/String;
    .restart local v34    # "xWapProfileUrl":Ljava/lang/String;
    :cond_18
    :try_start_16
    const-string v35, ""
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_1
    .catch Ljava/net/URISyntaxException; {:try_start_16 .. :try_end_16} :catch_6
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto :goto_e

    .line 288
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    :cond_19
    if-eqz v8, :cond_1a

    .line 289
    invoke-virtual {v8}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->close()V

    :cond_1a
    return-object v5

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static resolveMacro(Landroid/content/Context;Ljava/lang/String;Lcom/android/mms/service/MmsConfig$Overridden;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "mmsConfig"    # Lcom/android/mms/service/MmsConfig$Overridden;

    .prologue
    .line 390
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 416
    .end local p1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 393
    .restart local p1    # "value":Ljava/lang/String;
    :cond_1
    sget-object v6, Lcom/android/mms/service/HttpUtils;->MACRO_P:Ljava/util/regex/Pattern;

    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 394
    .local v3, "matcher":Ljava/util/regex/Matcher;
    const/4 v4, 0x0

    .line 395
    .local v4, "nextStart":I
    const/4 v5, 0x0

    .line 396
    .local v5, "replaced":Ljava/lang/StringBuilder;
    :goto_1
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 397
    if-nez v5, :cond_2

    .line 398
    new-instance v5, Ljava/lang/StringBuilder;

    .end local v5    # "replaced":Ljava/lang/StringBuilder;
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 400
    .restart local v5    # "replaced":Ljava/lang/StringBuilder;
    :cond_2
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    .line 401
    .local v2, "matchedStart":I
    if-le v2, v4, :cond_3

    .line 402
    invoke-virtual {p1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 404
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "macro":Ljava/lang/String;
    invoke-virtual {p2, p0, v0}, Lcom/android/mms/service/MmsConfig$Overridden;->getHttpParamMacro(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 406
    .local v1, "macroValue":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 407
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    :goto_2
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v4

    .line 412
    goto :goto_1

    .line 409
    :cond_4
    const-string v6, "MmsService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HttpUtils: invalid macro "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 413
    .end local v0    # "macro":Ljava/lang/String;
    .end local v1    # "macroValue":Ljava/lang/String;
    .end local v2    # "matchedStart":I
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_6

    .line 414
    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    :cond_6
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
