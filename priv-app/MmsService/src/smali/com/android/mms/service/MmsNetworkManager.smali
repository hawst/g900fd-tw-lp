.class public Lcom/android/mms/service/MmsNetworkManager;
.super Ljava/lang/Object;
.source "MmsNetworkManager.java"

# interfaces
.implements Lcom/android/mms/service/http/NameResolver;


# instance fields
.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mMmsRequestCount:I

.field private mNetwork:Landroid/net/Network;

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mNetworkRequest:Landroid/net/NetworkRequest;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkRequest:Landroid/net/NetworkRequest;

    .line 67
    iput-object p1, p0, Lcom/android/mms/service/MmsNetworkManager;->mContext:Landroid/content/Context;

    .line 68
    iput-object v2, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 69
    iput-object v2, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    .line 70
    iput v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    .line 71
    iput-object v2, p0, Lcom/android/mms/service/MmsNetworkManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 72
    return-void
.end method

.method static synthetic access$002(Lcom/android/mms/service/MmsNetworkManager;Landroid/net/Network;)Landroid/net/Network;
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p1, "x1"    # Landroid/net/Network;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/mms/service/MmsNetworkManager;Landroid/net/ConnectivityManager$NetworkCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p1, "x1"    # Landroid/net/ConnectivityManager$NetworkCallback;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/android/mms/service/MmsNetworkManager;->releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mms/service/MmsNetworkManager;)Landroid/net/ConnectivityManager$NetworkCallback;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsNetworkManager;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/service/MmsNetworkManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsNetworkManager;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->resetLocked()V

    return-void
.end method

.method private getConnectivityManager()Landroid/net/ConnectivityManager;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private inAirplaneMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 225
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "airplane_mode_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private newRequest()V
    .locals 4

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 144
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    new-instance v1, Lcom/android/mms/service/MmsNetworkManager$1;

    invoke-direct {v1, p0}, Lcom/android/mms/service/MmsNetworkManager$1;-><init>(Lcom/android/mms/service/MmsNetworkManager;)V

    iput-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 181
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkRequest:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    const v3, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/ConnectivityManager;->requestNetwork(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;I)V

    .line 183
    return-void
.end method

.method private releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V
    .locals 1
    .param p1, "callback"    # Landroid/net/ConnectivityManager$NetworkCallback;

    .prologue
    .line 191
    if-eqz p1, :cond_0

    .line 192
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->getConnectivityManager()Landroid/net/ConnectivityManager;

    move-result-object v0

    .line 193
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 195
    .end local v0    # "connectivityManager":Landroid/net/ConnectivityManager;
    :cond_0
    return-void
.end method

.method private resetLocked()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 201
    iput-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 202
    iput-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    .line 203
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    .line 204
    return-void
.end method


# virtual methods
.method public acquireNetwork()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsNetworkException;
        }
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->inAirplaneMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    new-instance v1, Lcom/android/mms/service/exception/MmsNetworkException;

    const-string v6, "In airplane mode"

    invoke-direct {v1, v6}, Lcom/android/mms/service/exception/MmsNetworkException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_0
    monitor-enter p0

    .line 91
    :try_start_0
    iget v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    .line 92
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    if-eqz v1, :cond_1

    .line 94
    const-string v1, "MmsService"

    const-string v6, "MmsNetworkManager: already available"

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    monitor-exit p0

    .line 110
    :goto_0
    return-void

    .line 97
    :cond_1
    const-string v1, "MmsService"

    const-string v6, "MmsNetworkManager: start new network request"

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->newRequest()V

    .line 100
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v6

    const-wide/32 v8, 0xfde8

    add-long v2, v6, v8

    .line 101
    .local v2, "shouldEnd":J
    const-wide/32 v4, 0xfde8

    .line 102
    .local v4, "waitTime":J
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    .line 104
    :try_start_1
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    :goto_2
    :try_start_2
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    if-eqz v1, :cond_2

    .line 110
    monitor-exit p0

    goto :goto_0

    .line 120
    .end local v2    # "shouldEnd":J
    .end local v4    # "waitTime":J
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 105
    .restart local v2    # "shouldEnd":J
    .restart local v4    # "waitTime":J
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v1, "MmsService"

    const-string v6, "MmsNetworkManager: acquire network wait interrupted"

    invoke-static {v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 113
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    sub-long v4, v2, v6

    goto :goto_1

    .line 116
    :cond_3
    const-string v1, "MmsService"

    const-string v6, "MmsNetworkManager: timed out"

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-direct {p0, v1}, Lcom/android/mms/service/MmsNetworkManager;->releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 118
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->resetLocked()V

    .line 119
    new-instance v1, Lcom/android/mms/service/exception/MmsNetworkException;

    const-string v6, "Acquiring network timed out"

    invoke-direct {v1, v6}, Lcom/android/mms/service/exception/MmsNetworkException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 208
    monitor-enter p0

    .line 209
    :try_start_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    invoke-virtual {v0, p1}, Landroid/net/Network;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    monitor-exit p0

    .line 212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/net/InetAddress;

    monitor-exit p0

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNetwork()Landroid/net/Network;
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;

    monitor-exit p0

    return-object v0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public releaseNetwork()V
    .locals 3

    .prologue
    .line 127
    monitor-enter p0

    .line 128
    :try_start_0
    iget v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    if-lez v0, :cond_0

    .line 129
    iget v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    .line 130
    const-string v0, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MmsNetworkManager: release, count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mMmsRequestCount:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-direct {p0, v0}, Lcom/android/mms/service/MmsNetworkManager;->releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 133
    invoke-direct {p0}, Lcom/android/mms/service/MmsNetworkManager;->resetLocked()V

    .line 136
    :cond_0
    monitor-exit p0

    .line 137
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
