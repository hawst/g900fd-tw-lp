.class public Lcom/android/mms/service/MmsService;
.super Landroid/app/Service;
.source "MmsService.java"

# interfaces
.implements Lcom/android/mms/service/MmsRequest$RequestManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/service/MmsService$RequestQueue;
    }
.end annotation


# instance fields
.field private final mExecutor:Ljava/util/concurrent/ExecutorService;

.field private final mMmsNetworkManager:Lcom/android/mms/service/MmsNetworkManager;

.field private final mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/mms/service/MmsRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

.field private mStub:Lcom/android/internal/telephony/IMms$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 91
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/service/MmsService;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    .line 94
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/mms/service/MmsService;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 124
    new-instance v0, Lcom/android/mms/service/MmsService$1;

    invoke-direct {v0, p0}, Lcom/android/mms/service/MmsService$1;-><init>(Lcom/android/mms/service/MmsService;)V

    iput-object v0, p0, Lcom/android/mms/service/MmsService;->mStub:Lcom/android/internal/telephony/IMms$Stub;

    .line 336
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/android/mms/service/MmsService$RequestQueue;

    iput-object v0, p0, Lcom/android/mms/service/MmsService;->mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

    .line 339
    new-instance v0, Lcom/android/mms/service/MmsNetworkManager;

    invoke-direct {v0, p0}, Lcom/android/mms/service/MmsNetworkManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/mms/service/MmsService;->mMmsNetworkManager:Lcom/android/mms/service/MmsNetworkManager;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/service/MmsService;)Lcom/android/mms/service/MmsNetworkManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/mms/service/MmsService;->mMmsNetworkManager:Lcom/android/mms/service/MmsNetworkManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/mms/service/MmsService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/mms/service/MmsService;->enforceSystemUid()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/mms/service/MmsService;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/mms/service/MmsService;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/mms/service/MmsService;Ljava/lang/String;ILjava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J
    .param p6, "x5"    # Z
    .param p7, "x6"    # Z
    .param p8, "x7"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct/range {p0 .. p8}, Lcom/android/mms/service/MmsService;->importSms(Ljava/lang/String;ILjava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Ljava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # J
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .param p7, "x6"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct/range {p0 .. p7}, Lcom/android/mms/service/MmsService;->importMms(Landroid/net/Uri;Ljava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Landroid/net/Uri;)Z
    .locals 1
    .param p0, "x0"    # Landroid/net/Uri;

    .prologue
    .line 73
    invoke-static {p0}, Lcom/android/mms/service/MmsService;->isSmsMmsContentUri(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Landroid/content/ContentValues;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Landroid/content/ContentValues;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/mms/service/MmsService;->updateMessageStatus(Landroid/net/Uri;Landroid/content/ContentValues;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/android/mms/service/MmsService;JZ)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/service/MmsService;->archiveConversation(JZ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/mms/service/MmsService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/android/mms/service/MmsService;->addSmsDraft(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsService;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/mms/service/MmsService;->addMmsDraft(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private addMmsDraft(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 12
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "creator"    # Ljava/lang/String;

    .prologue
    .line 604
    const/high16 v2, 0x800000

    invoke-virtual {p0, p1, v2}, Lcom/android/mms/service/MmsService;->readPduFromContentUri(Landroid/net/Uri;I)[B

    move-result-object v9

    .line 605
    .local v9, "pduData":[B
    if-eqz v9, :cond_0

    array-length v2, v9

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 606
    :cond_0
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: empty PDU"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const/4 v4, 0x0

    .line 652
    :goto_0
    return-object v4

    .line 612
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 614
    .local v10, "identity":J
    :try_start_0
    new-instance v2, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v2, v9}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    invoke-virtual {v2}, Lcom/google/android/mms/pdu/PduParser;->parse()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v1

    .line 615
    .local v1, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-nez v1, :cond_2

    .line 616
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: can\'t parse input PDU"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 617
    const/4 v4, 0x0

    .line 650
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 619
    :cond_2
    :try_start_1
    instance-of v2, v1, Lcom/google/android/mms/pdu/SendReq;

    if-nez v2, :cond_3

    .line 620
    const-string v2, "MmsService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addMmsDraft; invalid MMS type: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 621
    const/4 v4, 0x0

    .line 650
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 623
    :cond_3
    :try_start_2
    invoke-static {p0}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v0

    .line 624
    .local v0, "persister":Lcom/google/android/mms/pdu/PduPersister;
    sget-object v2, Landroid/provider/Telephony$Mms$Draft;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZZLjava/util/HashMap;)Landroid/net/Uri;

    move-result-object v4

    .line 630
    .local v4, "uri":Landroid/net/Uri;
    if-nez v4, :cond_4

    .line 631
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: failed to persist message"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 632
    const/4 v4, 0x0

    .line 650
    .end local v4    # "uri":Landroid/net/Uri;
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 634
    .restart local v4    # "uri":Landroid/net/Uri;
    :cond_4
    :try_start_3
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 635
    .local v5, "values":Landroid/content/ContentValues;
    const-string v2, "read"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 636
    const-string v2, "seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 637
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 638
    const-string v2, "creator"

    invoke-virtual {v5, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_5
    invoke-virtual {p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v7}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_6

    .line 642
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: failed to update message"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 650
    :cond_6
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 645
    .end local v0    # "persister":Lcom/google/android/mms/pdu/PduPersister;
    .end local v1    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 646
    .local v8, "e":Ljava/lang/RuntimeException;
    :try_start_4
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: failed to parse input PDU"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 650
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 652
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :goto_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 647
    :catch_1
    move-exception v8

    .line 648
    .local v8, "e":Lcom/google/android/mms/MmsException;
    :try_start_5
    const-string v2, "MmsService"

    const-string v3, "addMmsDraft: failed to persist message"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 650
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v8    # "e":Lcom/google/android/mms/MmsException;
    :catchall_0
    move-exception v2

    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method private addSmsDraft(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 7
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "creator"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 581
    new-instance v1, Landroid/content/ContentValues;

    const/4 v4, 0x5

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 582
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "address"

    invoke-virtual {v1, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v4, "body"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    const-string v4, "read"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 585
    const-string v4, "seen"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 586
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 587
    const-string v4, "creator"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 594
    .local v2, "identity":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/Telephony$Sms$Draft;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 598
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 600
    :goto_0
    return-object v4

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v4, "MmsService"

    const-string v5, "addSmsDraft: failed to store draft message"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 600
    const/4 v4, 0x0

    goto :goto_0

    .line 598
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method private archiveConversation(JZ)Z
    .locals 13
    .param p1, "conversationId"    # J
    .param p3, "archived"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 556
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 557
    .local v1, "values":Landroid/content/ContentValues;
    const-string v7, "archived"

    if-eqz p3, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 561
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 563
    .local v2, "identity":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v7, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v4, v7, v1, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    if-eq v4, v5, :cond_1

    .line 568
    const-string v4, "MmsService"

    const-string v5, "archiveConversation: failed to update database"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 577
    :goto_1
    return v6

    .end local v2    # "identity":J
    :cond_0
    move v4, v6

    .line 557
    goto :goto_0

    .line 575
    .restart local v2    # "identity":J
    :cond_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v6, v5

    goto :goto_1

    .line 572
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v4, "MmsService"

    const-string v5, "archiveConversation: failed to update database"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 575
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method private enforceSystemUid()V
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    .line 120
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only system can call this service"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    return-void
.end method

.method private importMms(Landroid/net/Uri;Ljava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    .locals 19
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "timestampSecs"    # J
    .param p5, "seen"    # Z
    .param p6, "read"    # Z
    .param p7, "creator"    # Ljava/lang/String;

    .prologue
    .line 431
    const/high16 v5, 0x800000

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5}, Lcom/android/mms/service/MmsService;->readPduFromContentUri(Landroid/net/Uri;I)[B

    move-result-object v14

    .line 432
    .local v14, "pduData":[B
    if-eqz v14, :cond_0

    array-length v5, v14

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 433
    :cond_0
    const-string v5, "MmsService"

    const-string v6, "importMessage: empty PDU"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v7, 0x0

    .line 494
    :goto_0
    return-object v7

    .line 439
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v12

    .line 441
    .local v12, "identity":J
    :try_start_0
    new-instance v5, Lcom/google/android/mms/pdu/PduParser;

    invoke-direct {v5, v14}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    invoke-virtual {v5}, Lcom/google/android/mms/pdu/PduParser;->parse()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v3

    .line 442
    .local v3, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-nez v3, :cond_2

    .line 443
    const-string v5, "MmsService"

    const-string v6, "importMessage: can\'t parse input PDU"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    const/4 v7, 0x0

    .line 492
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 446
    :cond_2
    const/4 v4, 0x0

    .line 447
    .local v4, "insertUri":Landroid/net/Uri;
    :try_start_1
    instance-of v5, v3, Lcom/google/android/mms/pdu/SendReq;

    if-eqz v5, :cond_4

    .line 448
    sget-object v4, Landroid/provider/Telephony$Mms$Sent;->CONTENT_URI:Landroid/net/Uri;

    .line 455
    :cond_3
    :goto_1
    if-nez v4, :cond_6

    .line 456
    const-string v5, "MmsService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "importMessage; invalid MMS type: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 457
    const/4 v7, 0x0

    .line 492
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 449
    :cond_4
    :try_start_2
    instance-of v5, v3, Lcom/google/android/mms/pdu/RetrieveConf;

    if-nez v5, :cond_5

    instance-of v5, v3, Lcom/google/android/mms/pdu/NotificationInd;

    if-nez v5, :cond_5

    instance-of v5, v3, Lcom/google/android/mms/pdu/DeliveryInd;

    if-nez v5, :cond_5

    instance-of v5, v3, Lcom/google/android/mms/pdu/ReadOrigInd;

    if-eqz v5, :cond_3

    .line 453
    :cond_5
    sget-object v4, Landroid/provider/Telephony$Mms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    .line 459
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v2

    .line 460
    .local v2, "persister":Lcom/google/android/mms/pdu/PduPersister;
    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZZLjava/util/HashMap;)Landroid/net/Uri;

    move-result-object v7

    .line 466
    .local v7, "uri":Landroid/net/Uri;
    if-nez v7, :cond_7

    .line 467
    const-string v5, "MmsService"

    const-string v6, "importMessage: failed to persist message"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 468
    const/4 v7, 0x0

    .line 492
    .end local v7    # "uri":Landroid/net/Uri;
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 470
    .restart local v7    # "uri":Landroid/net/Uri;
    :cond_7
    :try_start_3
    new-instance v8, Landroid/content/ContentValues;

    const/4 v5, 0x5

    invoke-direct {v8, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 471
    .local v8, "values":Landroid/content/ContentValues;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 472
    const-string v5, "m_id"

    move-object/from16 v0, p2

    invoke-virtual {v8, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_8
    const-wide/16 v16, -0x1

    cmp-long v5, p3, v16

    if-eqz v5, :cond_9

    .line 475
    const-string v5, "date"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v8, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 477
    :cond_9
    const-string v6, "read"

    if-eqz p5, :cond_c

    const/4 v5, 0x1

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 478
    const-string v6, "seen"

    if-eqz p6, :cond_d

    const/4 v5, 0x1

    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v8, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 479
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 480
    const-string v5, "creator"

    move-object/from16 v0, p7

    invoke-virtual {v8, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-static/range {v5 .. v10}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_b

    .line 484
    const-string v5, "MmsService"

    const-string v6, "importMessage: failed to update message"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 492
    :cond_b
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 477
    :cond_c
    const/4 v5, 0x0

    goto :goto_2

    .line 478
    :cond_d
    const/4 v5, 0x0

    goto :goto_3

    .line 487
    .end local v2    # "persister":Lcom/google/android/mms/pdu/PduPersister;
    .end local v3    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    .end local v4    # "insertUri":Landroid/net/Uri;
    .end local v7    # "uri":Landroid/net/Uri;
    .end local v8    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v11

    .line 488
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_4
    const-string v5, "MmsService"

    const-string v6, "importMessage: failed to parse input PDU"

    invoke-static {v5, v6, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 492
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 494
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :goto_4
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 489
    :catch_1
    move-exception v11

    .line 490
    .local v11, "e":Lcom/google/android/mms/MmsException;
    :try_start_5
    const-string v5, "MmsService"

    const-string v6, "importMessage: failed to persist message"

    invoke-static {v5, v6, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 492
    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_4

    .end local v11    # "e":Lcom/google/android/mms/MmsException;
    :catchall_0
    move-exception v5

    invoke-static {v12, v13}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v5
.end method

.method private importSms(Ljava/lang/String;ILjava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    .locals 10
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "timestampMillis"    # J
    .param p6, "seen"    # Z
    .param p7, "read"    # Z
    .param p8, "creator"    # Ljava/lang/String;

    .prologue
    .line 392
    const/4 v3, 0x0

    .line 393
    .local v3, "insertUri":Landroid/net/Uri;
    packed-switch p2, :pswitch_data_0

    .line 402
    :goto_0
    if-nez v3, :cond_0

    .line 403
    const-string v7, "MmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "importTextMessage: invalid message type for importing: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const/4 v7, 0x0

    .line 426
    :goto_1
    return-object v7

    .line 395
    :pswitch_0
    sget-object v3, Landroid/provider/Telephony$Sms$Inbox;->CONTENT_URI:Landroid/net/Uri;

    .line 397
    goto :goto_0

    .line 399
    :pswitch_1
    sget-object v3, Landroid/provider/Telephony$Sms$Sent;->CONTENT_URI:Landroid/net/Uri;

    goto :goto_0

    .line 406
    :cond_0
    new-instance v6, Landroid/content/ContentValues;

    const/4 v7, 0x6

    invoke-direct {v6, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 407
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "address"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    const-string v7, "date"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 409
    const-string v8, "seen"

    if-eqz p6, :cond_2

    const/4 v7, 0x1

    :goto_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 410
    const-string v8, "read"

    if-eqz p7, :cond_3

    const/4 v7, 0x1

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 411
    const-string v7, "body"

    invoke-virtual {v6, v7, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 413
    const-string v7, "creator"

    move-object/from16 v0, p8

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 420
    .local v4, "identity":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v3, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 424
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .line 409
    .end local v4    # "identity":J
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 410
    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    .line 421
    .restart local v4    # "identity":J
    :catch_0
    move-exception v2

    .line 422
    .local v2, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v7, "MmsService"

    const-string v8, "importTextMessage: failed to persist imported text message"

    invoke-static {v7, v8, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 424
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 426
    const/4 v7, 0x0

    goto :goto_1

    .line 424
    .end local v2    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v7

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v7

    .line 393
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static isSmsMmsContentUri(Landroid/net/Uri;)Z
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v1, 0x0

    .line 498
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 499
    .local v0, "uriString":Ljava/lang/String;
    const-string v2, "content://sms/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "content://mms/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 505
    :cond_0
    :goto_0
    return v1

    .line 502
    :cond_1
    invoke-static {p0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 505
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private startRequestQueueIfNeeded(I)V
    .locals 4
    .param p1, "queueIndex"    # I

    .prologue
    .line 347
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/android/mms/service/MmsService;->mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

    array-length v1, v1

    if-lt p1, v1, :cond_1

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    monitor-enter p0

    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/android/mms/service/MmsService;->mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    .line 352
    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MmsService RequestQueue "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 354
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 355
    iget-object v1, p0, Lcom/android/mms/service/MmsService;->mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

    new-instance v2, Lcom/android/mms/service/MmsService$RequestQueue;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/mms/service/MmsService$RequestQueue;-><init>(Lcom/android/mms/service/MmsService;Landroid/os/Looper;)V

    aput-object v2, v1, p1

    .line 357
    .end local v0    # "thread":Landroid/os/HandlerThread;
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateMessageStatus(Landroid/net/Uri;Landroid/content/ContentValues;)Z
    .locals 10
    .param p1, "messageUri"    # Landroid/net/Uri;
    .param p2, "statusValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 509
    invoke-static {p1}, Lcom/android/mms/service/MmsService;->isSmsMmsContentUri(Landroid/net/Uri;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 510
    const-string v6, "MmsService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateMessageStatus: invalid messageUri: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :goto_0
    return v5

    .line 513
    :cond_0
    if-nez p2, :cond_1

    .line 514
    const-string v6, "MmsService"

    const-string v7, "updateMessageStatus: empty values to update"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 517
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 518
    .local v4, "values":Landroid/content/ContentValues;
    const-string v7, "read"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 519
    const-string v7, "read"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 520
    .local v1, "val":Ljava/lang/Integer;
    if-eqz v1, :cond_2

    .line 522
    const-string v7, "read"

    invoke-virtual {v4, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 531
    .end local v1    # "val":Ljava/lang/Integer;
    :cond_2
    :goto_1
    invoke-virtual {v4}, Landroid/content/ContentValues;->size()I

    move-result v7

    if-ge v7, v6, :cond_4

    .line 532
    const-string v6, "MmsService"

    const-string v7, "updateMessageStatus: no value to update"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 524
    :cond_3
    const-string v7, "seen"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 525
    const-string v7, "seen"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 526
    .restart local v1    # "val":Ljava/lang/Integer;
    if-eqz v1, :cond_2

    .line 528
    const-string v7, "seen"

    invoke-virtual {v4, v7, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 538
    .end local v1    # "val":Ljava/lang/Integer;
    :cond_4
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 540
    .local v2, "identity":J
    :try_start_0
    invoke-virtual {p0}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, p1, v4, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    if-eq v7, v6, :cond_5

    .line 542
    const-string v6, "MmsService"

    const-string v7, "updateMessageStatus: failed to update database"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :cond_5
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v5, v6

    goto :goto_0

    .line 546
    :catch_0
    move-exception v0

    .line 547
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v6, "MmsService"

    const-string v7, "updateMessageStatus: failed to update database"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 549
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v5

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v5
.end method


# virtual methods
.method public addPending(ILcom/android/mms/service/MmsRequest;)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "request"    # Lcom/android/mms/service/MmsRequest;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/android/mms/service/MmsService;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    return-void
.end method

.method public addRunning(Lcom/android/mms/service/MmsRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/android/mms/service/MmsRequest;

    .prologue
    .line 362
    if-nez p1, :cond_0

    .line 370
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-virtual {p1}, Lcom/android/mms/service/MmsRequest;->getRunningQueue()I

    move-result v1

    .line 366
    .local v1, "queue":I
    invoke-direct {p0, v1}, Lcom/android/mms/service/MmsService;->startRequestQueueIfNeeded(I)V

    .line 367
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 368
    .local v0, "message":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 369
    iget-object v2, p0, Lcom/android/mms/service/MmsService;->mRequestQueues:[Lcom/android/mms/service/MmsService$RequestQueue;

    aget-object v2, v2, v1

    invoke-virtual {v2, v0}, Lcom/android/mms/service/MmsService$RequestQueue;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public getAutoPersistingPref()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 720
    const-string v1, "mmspref"

    invoke-virtual {p0, v1, v2}, Lcom/android/mms/service/MmsService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 722
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v1, "autopersisting"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/android/mms/service/MmsService;->mStub:Lcom/android/internal/telephony/IMms$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 383
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 384
    const-string v0, "MmsService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-static {}, Lcom/android/mms/service/MmsConfigManager;->getInstance()Lcom/android/mms/service/MmsConfigManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/mms/service/MmsConfigManager;->init(Landroid/content/Context;)V

    .line 388
    return-void
.end method

.method public readPduFromContentUri(Landroid/net/Uri;I)[B
    .locals 7
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "maxSize"    # I

    .prologue
    .line 732
    new-instance v0, Lcom/android/mms/service/MmsService$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/mms/service/MmsService$2;-><init>(Lcom/android/mms/service/MmsService;Landroid/net/Uri;I)V

    .line 766
    .local v0, "copyPduToArray":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<[B>;"
    iget-object v4, p0, Lcom/android/mms/service/MmsService;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v3

    .line 768
    .local v3, "pendingResult":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<[B>;"
    const-wide/16 v4, 0x7530

    :try_start_0
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    :goto_0
    return-object v2

    .line 770
    :catch_0
    move-exception v1

    .line 772
    .local v1, "e":Ljava/lang/Exception;
    const/4 v4, 0x1

    invoke-interface {v3, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 774
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public writePduToContentUri(Landroid/net/Uri;[B)Z
    .locals 9
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "pdu"    # [B

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 784
    new-instance v0, Lcom/android/mms/service/MmsService$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/android/mms/service/MmsService$3;-><init>(Lcom/android/mms/service/MmsService;Landroid/net/Uri;[B)V

    .line 806
    .local v0, "copyDownloadedPduToOutput":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Boolean;>;"
    iget-object v6, p0, Lcom/android/mms/service/MmsService;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 808
    .local v2, "pendingResult":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<Ljava/lang/Boolean;>;"
    const-wide/16 v6, 0x7530

    :try_start_0
    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v8}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 809
    .local v3, "succeeded":Ljava/lang/Boolean;
    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v3, v6, :cond_0

    .line 814
    .end local v3    # "succeeded":Ljava/lang/Boolean;
    :goto_0
    return v4

    .restart local v3    # "succeeded":Ljava/lang/Boolean;
    :cond_0
    move v4, v5

    .line 809
    goto :goto_0

    .line 810
    .end local v3    # "succeeded":Ljava/lang/Boolean;
    :catch_0
    move-exception v1

    .line 812
    .local v1, "e":Ljava/lang/Exception;
    invoke-interface {v2, v4}, Ljava/util/concurrent/Future;->cancel(Z)Z

    move v4, v5

    .line 814
    goto :goto_0
.end method
