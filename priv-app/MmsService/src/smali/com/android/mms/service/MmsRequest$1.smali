.class Lcom/android/mms/service/MmsRequest$1;
.super Landroid/content/BroadcastReceiver;
.source "MmsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/service/MmsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/MmsRequest;


# direct methods
.method constructor <init>(Lcom/android/mms/service/MmsRequest;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/android/mms/service/MmsRequest$1;->this$0:Lcom/android/mms/service/MmsRequest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 110
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.provider.Telephony.MMS_SEND"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android.provider.Telephony.MMS_DOWNLOAD"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 113
    :cond_0
    const-string v4, "MmsService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Carrier app result for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual {p0}, Lcom/android/mms/service/MmsRequest$1;->getResultCode()I

    move-result v1

    .line 115
    .local v1, "rc":I
    const/4 v4, -0x1

    if-ne v1, v4, :cond_2

    .line 117
    const-string v4, "MmsService"

    const-string v5, "Sending/downloading MMS by IP pending."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/mms/service/MmsRequest$1;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object v3

    .line 119
    .local v3, "resultExtras":Landroid/os/Bundle;
    if-eqz v3, :cond_1

    const-string v4, "messageref"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 120
    const-string v4, "messageref"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 121
    .local v2, "ref":I
    const-string v4, "MmsService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "messageref = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v4, p0, Lcom/android/mms/service/MmsRequest$1;->this$0:Lcom/android/mms/service/MmsRequest;

    iget-object v4, v4, Lcom/android/mms/service/MmsRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    iget-object v5, p0, Lcom/android/mms/service/MmsRequest$1;->this$0:Lcom/android/mms/service/MmsRequest;

    invoke-interface {v4, v2, v5}, Lcom/android/mms/service/MmsRequest$RequestManager;->addPending(ILcom/android/mms/service/MmsRequest;)V

    .line 136
    .end local v1    # "rc":I
    .end local v2    # "ref":I
    .end local v3    # "resultExtras":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 125
    .restart local v1    # "rc":I
    .restart local v3    # "resultExtras":Landroid/os/Bundle;
    :cond_1
    const-string v4, "MmsService"

    const-string v5, "Can\'t find messageref in result extras."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 129
    .end local v3    # "resultExtras":Landroid/os/Bundle;
    :cond_2
    const-string v4, "MmsService"

    const-string v5, "Sending/downloading MMS by IP failed."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v4, p0, Lcom/android/mms/service/MmsRequest$1;->this$0:Lcom/android/mms/service/MmsRequest;

    iget-object v4, v4, Lcom/android/mms/service/MmsRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    iget-object v5, p0, Lcom/android/mms/service/MmsRequest$1;->this$0:Lcom/android/mms/service/MmsRequest;

    invoke-interface {v4, v5}, Lcom/android/mms/service/MmsRequest$RequestManager;->addRunning(Lcom/android/mms/service/MmsRequest;)V

    goto :goto_0

    .line 133
    .end local v1    # "rc":I
    :cond_3
    const-string v4, "MmsService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected BroadcastReceiver action: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
