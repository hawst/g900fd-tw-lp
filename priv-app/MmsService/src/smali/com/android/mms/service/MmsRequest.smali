.class public abstract Lcom/android/mms/service/MmsRequest;
.super Ljava/lang/Object;
.source "MmsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mms/service/MmsRequest$RequestManager;
    }
.end annotation


# instance fields
.field protected final mCarrierAppResultReceiver:Landroid/content/BroadcastReceiver;

.field protected mCreator:Ljava/lang/String;

.field protected mMessageUri:Landroid/net/Uri;

.field protected mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

.field protected mMmsConfigOverrides:Landroid/os/Bundle;

.field protected mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

.field protected mSubId:J


# direct methods
.method public constructor <init>(Lcom/android/mms/service/MmsRequest$RequestManager;Landroid/net/Uri;JLjava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "requestManager"    # Lcom/android/mms/service/MmsRequest$RequestManager;
    .param p2, "messageUri"    # Landroid/net/Uri;
    .param p3, "subId"    # J
    .param p5, "creator"    # Ljava/lang/String;
    .param p6, "configOverrides"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Lcom/android/mms/service/MmsRequest$1;

    invoke-direct {v0, p0}, Lcom/android/mms/service/MmsRequest$1;-><init>(Lcom/android/mms/service/MmsRequest;)V

    iput-object v0, p0, Lcom/android/mms/service/MmsRequest;->mCarrierAppResultReceiver:Landroid/content/BroadcastReceiver;

    .line 141
    iput-object p1, p0, Lcom/android/mms/service/MmsRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    .line 142
    iput-object p2, p0, Lcom/android/mms/service/MmsRequest;->mMessageUri:Landroid/net/Uri;

    .line 143
    iput-wide p3, p0, Lcom/android/mms/service/MmsRequest;->mSubId:J

    .line 144
    iput-object p5, p0, Lcom/android/mms/service/MmsRequest;->mCreator:Ljava/lang/String;

    .line 145
    iput-object p6, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfigOverrides:Landroid/os/Bundle;

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    .line 147
    return-void
.end method

.method private ensureMmsConfigLoaded()Z
    .locals 4

    .prologue
    .line 150
    iget-object v1, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    if-nez v1, :cond_0

    .line 152
    invoke-static {}, Lcom/android/mms/service/MmsConfigManager;->getInstance()Lcom/android/mms/service/MmsConfigManager;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/mms/service/MmsRequest;->mSubId:J

    invoke-virtual {v1, v2, v3}, Lcom/android/mms/service/MmsConfigManager;->getMmsConfigBySubId(J)Lcom/android/mms/service/MmsConfig;

    move-result-object v0

    .line 153
    .local v0, "config":Lcom/android/mms/service/MmsConfig;
    if-eqz v0, :cond_0

    .line 154
    new-instance v1, Lcom/android/mms/service/MmsConfig$Overridden;

    iget-object v2, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfigOverrides:Landroid/os/Bundle;

    invoke-direct {v1, v0, v2}, Lcom/android/mms/service/MmsConfig$Overridden;-><init>(Lcom/android/mms/service/MmsConfig;Landroid/os/Bundle;)V

    iput-object v1, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    .line 157
    .end local v0    # "config":Lcom/android/mms/service/MmsConfig;
    :cond_0
    iget-object v1, p0, Lcom/android/mms/service/MmsRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getMmsLinkAddressTypes(Landroid/net/ConnectivityManager;Landroid/net/Network;)I
    .locals 6
    .param p0, "connMgr"    # Landroid/net/ConnectivityManager;
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 316
    const/4 v3, 0x0

    .line 318
    .local v3, "result":I
    if-nez p1, :cond_0

    move v4, v3

    .line 331
    .end local v3    # "result":I
    .local v4, "result":I
    :goto_0
    return v4

    .line 321
    .end local v4    # "result":I
    .restart local v3    # "result":I
    :cond_0
    invoke-virtual {p0, p1}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v2

    .line 322
    .local v2, "linkProperties":Landroid/net/LinkProperties;
    if-eqz v2, :cond_3

    .line 323
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 324
    .local v0, "addr":Ljava/net/InetAddress;
    instance-of v5, v0, Ljava/net/Inet4Address;

    if-eqz v5, :cond_2

    .line 325
    or-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 326
    :cond_2
    instance-of v5, v0, Ljava/net/Inet6Address;

    if-eqz v5, :cond_1

    .line 327
    or-int/lit8 v3, v3, 0x2

    goto :goto_1

    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    move v4, v3

    .line 331
    .end local v3    # "result":I
    .restart local v4    # "result":I
    goto :goto_0
.end method

.method private static resolveDestination(Landroid/net/ConnectivityManager;Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;Lcom/android/mms/service/ApnSettings;)Ljava/util/List;
    .locals 6
    .param p0, "connMgr"    # Landroid/net/ConnectivityManager;
    .param p1, "netMgr"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "apn"    # Lcom/android/mms/service/ApnSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/ConnectivityManager;",
            "Lcom/android/mms/service/MmsNetworkManager;",
            "Ljava/lang/String;",
            "Lcom/android/mms/service/ApnSettings;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 287
    const-string v3, "MmsService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MmsRequest: resolve url "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const/4 v1, 0x0

    .line 290
    .local v1, "host":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/android/mms/service/ApnSettings;->isProxySet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    invoke-virtual {p3}, Lcom/android/mms/service/ApnSettings;->getProxyAddress()Ljava/lang/String;

    move-result-object v1

    .line 297
    :goto_0
    invoke-virtual {p1}, Lcom/android/mms/service/MmsNetworkManager;->getNetwork()Landroid/net/Network;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/mms/service/MmsRequest;->getMmsLinkAddressTypes(Landroid/net/ConnectivityManager;Landroid/net/Network;)I

    move-result v0

    .line 298
    .local v0, "addressTypes":I
    const-string v3, "MmsService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MmsRequest: addressTypes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    invoke-static {p1, v1, v0}, Lcom/android/mms/service/MmsRequest;->resolveHostName(Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    return-object v3

    .line 293
    .end local v0    # "addressTypes":I
    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 294
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {v2}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static resolveHostName(Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;I)Ljava/util/List;
    .locals 9
    .param p0, "netMgr"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "addressTypes"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/mms/service/MmsNetworkManager;",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 346
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v5, "resolved":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    if-eqz p2, :cond_2

    .line 349
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/mms/service/MmsNetworkManager;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v1

    .local v1, "arr$":[Ljava/net/InetAddress;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v0, v1, v3

    .line 350
    .local v0, "addr":Ljava/net/InetAddress;
    and-int/lit8 v6, p2, 0x2

    if-eqz v6, :cond_1

    instance-of v6, v0, Ljava/net/Inet6Address;

    if-eqz v6, :cond_1

    .line 353
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 354
    :cond_1
    and-int/lit8 v6, p2, 0x1

    if-eqz v6, :cond_0

    instance-of v6, v0, Ljava/net/Inet4Address;

    if-eqz v6, :cond_0

    .line 357
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 366
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "arr$":[Ljava/net/InetAddress;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catch_0
    move-exception v2

    .line 367
    .local v2, "e":Ljava/net/UnknownHostException;
    new-instance v6, Lcom/android/mms/service/exception/MmsHttpException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to resolve "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v2}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 361
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :cond_2
    :try_start_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_3

    .line 362
    new-instance v6, Lcom/android/mms/service/exception/MmsHttpException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to resolve "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for allowed address types: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0

    .line 365
    :cond_3
    return-object v5
.end method


# virtual methods
.method protected abstract doHttp(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Lcom/android/mms/service/ApnSettings;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation
.end method

.method protected doHttpForResolvedAddresses(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;[BILcom/android/mms/service/ApnSettings;)[B
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "netMgr"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "pdu"    # [B
    .param p5, "method"    # I
    .param p6, "apn"    # Lcom/android/mms/service/ApnSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 230
    const/16 v17, 0x0

    .line 231
    .local v17, "lastException":Lcom/android/mms/service/exception/MmsHttpException;
    const-string v3, "connectivity"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/ConnectivityManager;

    .line 234
    .local v14, "connMgr":Landroid/net/ConnectivityManager;
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p6

    invoke-static {v14, v0, v1, v2}, Lcom/android/mms/service/MmsRequest;->resolveDestination(Landroid/net/ConnectivityManager;Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;Lcom/android/mms/service/ApnSettings;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/net/InetAddress;

    .line 243
    .local v13, "address":Ljava/net/InetAddress;
    const/4 v3, 0x2

    :try_start_0
    invoke-virtual {v14, v3, v13}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 245
    new-instance v3, Lcom/android/mms/service/exception/MmsHttpException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MmsRequest: can not request a route for host "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lcom/android/mms/service/exception/MmsHttpException; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    :catch_0
    move-exception v15

    .line 260
    .local v15, "e":Lcom/android/mms/service/exception/MmsHttpException;
    move-object/from16 v17, v15

    .line 261
    const-string v3, "MmsService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MmsRequest: failure in trying address "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 248
    .end local v15    # "e":Lcom/android/mms/service/exception/MmsHttpException;
    :cond_0
    :try_start_1
    invoke-virtual/range {p6 .. p6}, Lcom/android/mms/service/ApnSettings;->isProxySet()Z

    move-result v7

    invoke-virtual/range {p6 .. p6}, Lcom/android/mms/service/ApnSettings;->getProxyAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p6 .. p6}, Lcom/android/mms/service/ApnSettings;->getProxyPort()I

    move-result v9

    instance-of v11, v13, Ljava/net/Inet6Address;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/mms/service/MmsRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v10, p2

    invoke-static/range {v3 .. v12}, Lcom/android/mms/service/HttpUtils;->httpConnection(Landroid/content/Context;Ljava/lang/String;[BIZLjava/lang/String;ILcom/android/mms/service/http/NameResolver;ZLcom/android/mms/service/MmsConfig$Overridden;)[B
    :try_end_1
    .catch Lcom/android/mms/service/exception/MmsHttpException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    return-object v3

    .line 264
    .end local v13    # "address":Ljava/net/InetAddress;
    :cond_1
    if-eqz v17, :cond_2

    .line 265
    throw v17

    .line 268
    :cond_2
    new-instance v3, Lcom/android/mms/service/exception/MmsHttpException;

    const-string v4, "MmsRequest: unknown failure"

    invoke-direct {v3, v4}, Lcom/android/mms/service/exception/MmsHttpException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public execute(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkManager"    # Lcom/android/mms/service/MmsNetworkManager;

    .prologue
    .line 167
    const/4 v4, 0x1

    .line 168
    .local v4, "result":I
    const/4 v3, 0x0

    .line 169
    .local v3, "response":[B
    invoke-direct {p0}, Lcom/android/mms/service/MmsRequest;->ensureMmsConfigLoaded()Z

    move-result v5

    if-nez v5, :cond_1

    .line 170
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: mms config is not loaded yet"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v4, 0x7

    .line 213
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, v4, v3}, Lcom/android/mms/service/MmsRequest;->processResult(Landroid/content/Context;I[B)V

    .line 214
    return-void

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/android/mms/service/MmsRequest;->prepareForHttpRequest()Z

    move-result v5

    if-nez v5, :cond_2

    .line 173
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: failed to prepare for request"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    const/4 v4, 0x5

    goto :goto_0

    .line 176
    :cond_2
    const-wide/16 v6, 0x2

    .line 178
    .local v6, "retryDelaySecs":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v5, 0x3

    if-ge v2, v5, :cond_0

    .line 180
    :try_start_0
    invoke-virtual {p2}, Lcom/android/mms/service/MmsNetworkManager;->acquireNetwork()V
    :try_end_0
    .catch Lcom/android/mms/service/exception/ApnException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/mms/service/exception/MmsNetworkException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/mms/service/exception/MmsHttpException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 182
    const/4 v5, 0x0

    :try_start_1
    iget-wide v8, p0, Lcom/android/mms/service/MmsRequest;->mSubId:J

    invoke-static {p1, v5, v8, v9}, Lcom/android/mms/service/ApnSettings;->load(Landroid/content/Context;Ljava/lang/String;J)Lcom/android/mms/service/ApnSettings;

    move-result-object v0

    .line 183
    .local v0, "apn":Lcom/android/mms/service/ApnSettings;
    invoke-virtual {p0, p1, p2, v0}, Lcom/android/mms/service/MmsRequest;->doHttp(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Lcom/android/mms/service/ApnSettings;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 184
    const/4 v4, -0x1

    .line 188
    :try_start_2
    invoke-virtual {p2}, Lcom/android/mms/service/MmsNetworkManager;->releaseNetwork()V
    :try_end_2
    .catch Lcom/android/mms/service/exception/ApnException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/mms/service/exception/MmsNetworkException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/mms/service/exception/MmsHttpException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 190
    .end local v0    # "apn":Lcom/android/mms/service/ApnSettings;
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Lcom/android/mms/service/exception/ApnException;
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: APN failure"

    invoke-static {v5, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    const/4 v4, 0x2

    .line 193
    goto :goto_0

    .line 188
    .end local v1    # "e":Lcom/android/mms/service/exception/ApnException;
    :catchall_0
    move-exception v5

    :try_start_3
    invoke-virtual {p2}, Lcom/android/mms/service/MmsNetworkManager;->releaseNetwork()V

    throw v5
    :try_end_3
    .catch Lcom/android/mms/service/exception/ApnException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lcom/android/mms/service/exception/MmsNetworkException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/mms/service/exception/MmsHttpException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 194
    :catch_1
    move-exception v1

    .line 195
    .local v1, "e":Lcom/android/mms/service/exception/MmsNetworkException;
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: MMS network acquiring failure"

    invoke-static {v5, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 196
    const/4 v4, 0x3

    .line 208
    .end local v1    # "e":Lcom/android/mms/service/exception/MmsNetworkException;
    :goto_2
    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v6

    const/4 v5, 0x0

    :try_start_4
    invoke-static {v8, v9, v5}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4

    .line 210
    :goto_3
    const/4 v5, 0x1

    shl-long/2addr v6, v5

    .line 178
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 198
    :catch_2
    move-exception v1

    .line 199
    .local v1, "e":Lcom/android/mms/service/exception/MmsHttpException;
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: HTTP or network I/O failure"

    invoke-static {v5, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    const/4 v4, 0x4

    .line 206
    goto :goto_2

    .line 202
    .end local v1    # "e":Lcom/android/mms/service/exception/MmsHttpException;
    :catch_3
    move-exception v1

    .line 203
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "MmsService"

    const-string v8, "MmsRequest: unexpected failure"

    invoke-static {v5, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 204
    const/4 v4, 0x1

    .line 205
    goto :goto_0

    .line 209
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v5

    goto :goto_3
.end method

.method protected abstract getPendingIntent()Landroid/app/PendingIntent;
.end method

.method protected abstract getRunningQueue()I
.end method

.method protected abstract prepareForHttpRequest()Z
.end method

.method public processResult(Landroid/content/Context;I[B)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "result"    # I
    .param p3, "response"    # [B

    .prologue
    .line 380
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/mms/service/MmsRequest;->updateStatus(Landroid/content/Context;I[B)V

    .line 383
    invoke-virtual {p0}, Lcom/android/mms/service/MmsRequest;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    .line 384
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz v2, :cond_3

    .line 385
    const/4 v3, 0x1

    .line 387
    .local v3, "succeeded":Z
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 388
    .local v1, "fillIn":Landroid/content/Intent;
    if-eqz p3, :cond_0

    .line 389
    invoke-virtual {p0, v1, p3}, Lcom/android/mms/service/MmsRequest;->transferResponse(Landroid/content/Intent;[B)Z

    move-result v3

    .line 391
    :cond_0
    iget-object v4, p0, Lcom/android/mms/service/MmsRequest;->mMessageUri:Landroid/net/Uri;

    if-eqz v4, :cond_1

    .line 392
    const-string v4, "uri"

    iget-object v5, p0, Lcom/android/mms/service/MmsRequest;->mMessageUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    :cond_1
    if-nez v3, :cond_2

    .line 396
    const/4 p2, 0x5

    .line 398
    :cond_2
    :try_start_0
    invoke-virtual {v2, p1, p2, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    .end local v1    # "fillIn":Landroid/content/Intent;
    .end local v3    # "succeeded":Z
    :cond_3
    :goto_0
    invoke-virtual {p0, p1}, Lcom/android/mms/service/MmsRequest;->revokeUriPermission(Landroid/content/Context;)V

    .line 405
    return-void

    .line 399
    .restart local v1    # "fillIn":Landroid/content/Intent;
    .restart local v3    # "succeeded":Z
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    const-string v4, "MmsService"

    const-string v5, "MmsRequest: sending pending intent canceled"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected abstract revokeUriPermission(Landroid/content/Context;)V
.end method

.method protected abstract transferResponse(Landroid/content/Intent;[B)Z
.end method

.method protected abstract updateStatus(Landroid/content/Context;I[B)V
.end method
