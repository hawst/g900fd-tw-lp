.class Lcom/android/mms/service/http/NetworkAwareHttpClient$2;
.super Lorg/apache/http/impl/client/DefaultHttpClient;
.source "NetworkAwareHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/service/http/NetworkAwareHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;


# direct methods
.method constructor <init>(Lcom/android/mms/service/http/NetworkAwareHttpClient;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .param p2, "x0"    # Lorg/apache/http/conn/ClientConnectionManager;
    .param p3, "x1"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$2;->this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;

    invoke-direct {p0, p2, p3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    return-void
.end method


# virtual methods
.method protected createHttpContext()Lorg/apache/http/protocol/HttpContext;
    .locals 3

    .prologue
    .line 176
    new-instance v0, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v0}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 177
    .local v0, "context":Lorg/apache/http/protocol/HttpContext;
    const-string v1, "http.authscheme-registry"

    invoke-virtual {p0}, Lcom/android/mms/service/http/NetworkAwareHttpClient$2;->getAuthSchemes()Lorg/apache/http/auth/AuthSchemeRegistry;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 180
    const-string v1, "http.cookiespec-registry"

    invoke-virtual {p0}, Lcom/android/mms/service/http/NetworkAwareHttpClient$2;->getCookieSpecs()Lorg/apache/http/cookie/CookieSpecRegistry;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 183
    const-string v1, "http.auth.credentials-provider"

    invoke-virtual {p0}, Lcom/android/mms/service/http/NetworkAwareHttpClient$2;->getCredentialsProvider()Lorg/apache/http/client/CredentialsProvider;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 186
    return-object v0
.end method

.method protected createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;
    .locals 4

    .prologue
    .line 165
    invoke-super {p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->createHttpProcessor()Lorg/apache/http/protocol/BasicHttpProcessor;

    move-result-object v0

    .line 166
    .local v0, "processor":Lorg/apache/http/protocol/BasicHttpProcessor;
    # getter for: Lcom/android/mms/service/http/NetworkAwareHttpClient;->sThreadCheckInterceptor:Lorg/apache/http/HttpRequestInterceptor;
    invoke-static {}, Lcom/android/mms/service/http/NetworkAwareHttpClient;->access$000()Lorg/apache/http/HttpRequestInterceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 167
    new-instance v1, Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;

    iget-object v2, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$2;->this$0:Lcom/android/mms/service/http/NetworkAwareHttpClient;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/android/mms/service/http/NetworkAwareHttpClient$CurlLogger;-><init>(Lcom/android/mms/service/http/NetworkAwareHttpClient;Lcom/android/mms/service/http/NetworkAwareHttpClient$1;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/protocol/BasicHttpProcessor;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 169
    return-object v0
.end method
