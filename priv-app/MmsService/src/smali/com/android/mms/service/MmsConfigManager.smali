.class public Lcom/android/mms/service/MmsConfigManager;
.super Ljava/lang/Object;
.source "MmsConfigManager.java"


# static fields
.field private static volatile sInstance:Lcom/android/mms/service/MmsConfigManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mSubIdConfigMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/mms/service/MmsConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/android/mms/service/MmsConfigManager;

    invoke-direct {v0}, Lcom/android/mms/service/MmsConfigManager;-><init>()V

    sput-object v0, Lcom/android/mms/service/MmsConfigManager;->sInstance:Lcom/android/mms/service/MmsConfigManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    .line 62
    new-instance v0, Lcom/android/mms/service/MmsConfigManager$1;

    invoke-direct {v0, p0}, Lcom/android/mms/service/MmsConfigManager$1;-><init>(Lcom/android/mms/service/MmsConfigManager;)V

    iput-object v0, p0, Lcom/android/mms/service/MmsConfigManager;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/mms/service/MmsConfigManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsConfigManager;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/mms/service/MmsConfigManager;->loadInBackground()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/mms/service/MmsConfigManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/MmsConfigManager;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/mms/service/MmsConfigManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/mms/service/MmsConfigManager;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/MmsConfigManager;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/android/mms/service/MmsConfigManager;->load(Landroid/content/Context;)V

    return-void
.end method

.method public static getInstance()Lcom/android/mms/service/MmsConfigManager;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/android/mms/service/MmsConfigManager;->sInstance:Lcom/android/mms/service/MmsConfigManager;

    return-object v0
.end method

.method private load(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoList()Ljava/util/List;

    move-result-object v6

    .line 130
    .local v6, "subs":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubInfoRecord;>;"
    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    .line 131
    :cond_0
    const-string v7, "MmsService"

    const-string v8, "MmsConfigManager.load -- empty getActiveSubInfoList"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :goto_0
    return-void

    .line 136
    :cond_1
    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    .line 137
    .local v3, "newConfigMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/android/mms/service/MmsConfig;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/SubInfoRecord;

    .line 138
    .local v4, "sub":Landroid/telephony/SubInfoRecord;
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    .line 139
    .local v1, "configuration":Landroid/content/res/Configuration;
    iget v7, v4, Landroid/telephony/SubInfoRecord;->mcc:I

    if-nez v7, :cond_2

    iget v7, v4, Landroid/telephony/SubInfoRecord;->mnc:I

    if-nez v7, :cond_2

    .line 140
    iget-object v7, p0, Lcom/android/mms/service/MmsConfigManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 141
    .local v0, "config":Landroid/content/res/Configuration;
    iget v7, v0, Landroid/content/res/Configuration;->mcc:I

    iput v7, v1, Landroid/content/res/Configuration;->mcc:I

    .line 142
    iget v7, v0, Landroid/content/res/Configuration;->mnc:I

    iput v7, v1, Landroid/content/res/Configuration;->mnc:I

    .line 143
    const-string v7, "MmsService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MmsConfigManager.load -- no mcc/mnc for sub: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " using mcc/mnc from main context: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v1, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v1, Landroid/content/res/Configuration;->mnc:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    .end local v0    # "config":Landroid/content/res/Configuration;
    :goto_2
    invoke-virtual {p1, v1}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v5

    .line 157
    .local v5, "subContext":Landroid/content/Context;
    iget-wide v8, v4, Landroid/telephony/SubInfoRecord;->subId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    new-instance v8, Lcom/android/mms/service/MmsConfig;

    iget-wide v10, v4, Landroid/telephony/SubInfoRecord;->subId:J

    invoke-direct {v8, v5, v10, v11}, Lcom/android/mms/service/MmsConfig;-><init>(Landroid/content/Context;J)V

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 149
    .end local v5    # "subContext":Landroid/content/Context;
    :cond_2
    const-string v7, "MmsService"

    const-string v8, "MmsConfigManager.load -- mcc/mnc for sub: XXX"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget v7, v4, Landroid/telephony/SubInfoRecord;->mcc:I

    iput v7, v1, Landroid/content/res/Configuration;->mcc:I

    .line 153
    iget v7, v4, Landroid/telephony/SubInfoRecord;->mnc:I

    iput v7, v1, Landroid/content/res/Configuration;->mnc:I

    goto :goto_2

    .line 159
    .end local v1    # "configuration":Landroid/content/res/Configuration;
    .end local v4    # "sub":Landroid/telephony/SubInfoRecord;
    :cond_3
    iget-object v8, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    monitor-enter v8

    .line 160
    :try_start_0
    iget-object v7, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->clear()V

    .line 161
    iget-object v7, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    invoke-interface {v7, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 162
    monitor-exit v8

    goto/16 :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7
.end method

.method private loadInBackground()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/android/mms/service/MmsConfigManager$2;

    invoke-direct {v0, p0}, Lcom/android/mms/service/MmsConfigManager$2;-><init>(Lcom/android/mms/service/MmsConfigManager;)V

    invoke-virtual {v0}, Lcom/android/mms/service/MmsConfigManager$2;->start()V

    .line 102
    return-void
.end method


# virtual methods
.method public getMmsConfigBySubId(J)Lcom/android/mms/service/MmsConfig;
    .locals 5
    .param p1, "subId"    # J

    .prologue
    .line 115
    iget-object v2, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    monitor-enter v2

    .line 116
    :try_start_0
    iget-object v1, p0, Lcom/android/mms/service/MmsConfigManager;->mSubIdConfigMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mms/service/MmsConfig;

    .line 117
    .local v0, "mmsConfig":Lcom/android/mms/service/MmsConfig;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    const-string v1, "MmsService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMmsConfigBySubId -- for sub: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mmsConfig: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return-object v0

    .line 117
    .end local v0    # "mmsConfig":Lcom/android/mms/service/MmsConfig;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public init(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_SUBINFO_RECORD_UPDATED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/mms/service/MmsConfigManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    new-instance v1, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.ACTION_SUBINFO_CONTENT_CHANGE"

    invoke-direct {v1, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 80
    .local v1, "intentFilterChange":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/mms/service/MmsConfigManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 81
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "LOADED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 83
    .local v2, "intentFilterLoaded":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/android/mms/service/MmsConfigManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 85
    iput-object p1, p0, Lcom/android/mms/service/MmsConfigManager;->mContext:Landroid/content/Context;

    .line 86
    invoke-direct {p0}, Lcom/android/mms/service/MmsConfigManager;->loadInBackground()V

    .line 87
    return-void
.end method
