.class Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;
.super Ljava/lang/Object;
.source "NetworkAwareHttpClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/service/http/NetworkAwareHttpClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoggingConfiguration"
.end annotation


# instance fields
.field private final level:I

.field private final tag:Ljava/lang/String;


# direct methods
.method static synthetic access$400(Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->isLoggable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->println(Ljava/lang/String;)V

    return-void
.end method

.method private isLoggable()Z
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->tag:Ljava/lang/String;

    iget v1, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->level:I

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method private println(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 360
    iget v0, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->level:I

    iget-object v1, p0, Lcom/android/mms/service/http/NetworkAwareHttpClient$LoggingConfiguration;->tag:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 361
    return-void
.end method
