.class Lcom/android/mms/service/MmsNetworkManager$1;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "MmsNetworkManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/mms/service/MmsNetworkManager;->newRequest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/MmsNetworkManager;


# direct methods
.method constructor <init>(Lcom/android/mms/service/MmsNetworkManager;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 3
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onAvailable(Landroid/net/Network;)V

    .line 148
    const-string v0, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetworkCallbackListener.onAvailable: network="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    monitor-enter v1

    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # setter for: Lcom/android/mms/service/MmsNetworkManager;->mNetwork:Landroid/net/Network;
    invoke-static {v0, p1}, Lcom/android/mms/service/MmsNetworkManager;->access$002(Lcom/android/mms/service/MmsNetworkManager;Landroid/net/Network;)Landroid/net/Network;

    .line 151
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 152
    monitor-exit v1

    .line 153
    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 3
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 157
    invoke-super {p0, p1}, Landroid/net/ConnectivityManager$NetworkCallback;->onLost(Landroid/net/Network;)V

    .line 158
    const-string v0, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetworkCallbackListener.onLost: network="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # invokes: Lcom/android/mms/service/MmsNetworkManager;->releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V
    invoke-static {v0, p0}, Lcom/android/mms/service/MmsNetworkManager;->access$100(Lcom/android/mms/service/MmsNetworkManager;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 161
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # getter for: Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;
    invoke-static {v0}, Lcom/android/mms/service/MmsNetworkManager;->access$200(Lcom/android/mms/service/MmsNetworkManager;)Landroid/net/ConnectivityManager$NetworkCallback;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # invokes: Lcom/android/mms/service/MmsNetworkManager;->resetLocked()V
    invoke-static {v0}, Lcom/android/mms/service/MmsNetworkManager;->access$300(Lcom/android/mms/service/MmsNetworkManager;)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 165
    monitor-exit v1

    .line 166
    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onUnavailable()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Landroid/net/ConnectivityManager$NetworkCallback;->onUnavailable()V

    .line 171
    const-string v0, "MmsService"

    const-string v1, "NetworkCallbackListener.onUnavailable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v1, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    monitor-enter v1

    .line 173
    :try_start_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # invokes: Lcom/android/mms/service/MmsNetworkManager;->releaseRequest(Landroid/net/ConnectivityManager$NetworkCallback;)V
    invoke-static {v0, p0}, Lcom/android/mms/service/MmsNetworkManager;->access$100(Lcom/android/mms/service/MmsNetworkManager;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 174
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # getter for: Lcom/android/mms/service/MmsNetworkManager;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;
    invoke-static {v0}, Lcom/android/mms/service/MmsNetworkManager;->access$200(Lcom/android/mms/service/MmsNetworkManager;)Landroid/net/ConnectivityManager$NetworkCallback;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    # invokes: Lcom/android/mms/service/MmsNetworkManager;->resetLocked()V
    invoke-static {v0}, Lcom/android/mms/service/MmsNetworkManager;->access$300(Lcom/android/mms/service/MmsNetworkManager;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/MmsNetworkManager$1;->this$0:Lcom/android/mms/service/MmsNetworkManager;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 178
    monitor-exit v1

    .line 179
    return-void

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
