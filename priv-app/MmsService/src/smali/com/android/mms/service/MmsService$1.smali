.class Lcom/android/mms/service/MmsService$1;
.super Lcom/android/internal/telephony/IMms$Stub;
.source "MmsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/service/MmsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/MmsService;


# direct methods
.method constructor <init>(Lcom/android/mms/service/MmsService;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-direct {p0}, Lcom/android/internal/telephony/IMms$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public addMultimediaMessageDraft(Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    const-string v0, "MmsService"

    const-string v1, "addMultimediaMessageDraft"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v0}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 306
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->addMmsDraft(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p2, p1}, Lcom/android/mms/service/MmsService;->access$900(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public addTextMessageDraft(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 296
    const-string v0, "MmsService"

    const-string v1, "addTextMessageDraft"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v0}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 298
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->addSmsDraft(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    invoke-static {v0, p2, p3, p1}, Lcom/android/mms/service/MmsService;->access$800(Lcom/android/mms/service/MmsService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public archiveStoredConversation(Ljava/lang/String;JZ)Z
    .locals 4
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "conversationId"    # J
    .param p4, "archived"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 285
    const-string v0, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "archiveStoredConversation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 287
    const-string v0, "MmsService"

    const-string v1, "archiveStoredConversation: invalid thread id"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v0, 0x0

    .line 290
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->archiveConversation(JZ)Z
    invoke-static {v0, p2, p3, p4}, Lcom/android/mms/service/MmsService;->access$700(Lcom/android/mms/service/MmsService;JZ)Z

    move-result v0

    goto :goto_0
.end method

.method public deleteStoredConversation(Ljava/lang/String;J)Z
    .locals 10
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "conversationId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 249
    const-string v6, "MmsService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteStoredConversation "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v6, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v6}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 251
    const-wide/16 v6, -0x1

    cmp-long v6, p2, v6

    if-nez v6, :cond_0

    .line 252
    const-string v5, "MmsService"

    const-string v6, "deleteStoredConversation: invalid thread id"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :goto_0
    return v4

    .line 255
    :cond_0
    sget-object v6, Landroid/provider/Telephony$Threads;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 260
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 262
    .local v2, "identity":J
    :try_start_0
    iget-object v6, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v6}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v1, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    if-eq v6, v5, :cond_1

    .line 263
    const-string v6, "MmsService"

    const-string v7, "deleteStoredConversation: failed to delete"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :cond_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_1
    move v4, v5

    .line 271
    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v4, "MmsService"

    const-string v6, "deleteStoredConversation: failed to delete"

    invoke-static {v4, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 269
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
.end method

.method public deleteStoredMessage(Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 8
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "messageUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 222
    const-string v5, "MmsService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleteStoredMessage "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v5, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v5}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 224
    # invokes: Lcom/android/mms/service/MmsService;->isSmsMmsContentUri(Landroid/net/Uri;)Z
    invoke-static {p2}, Lcom/android/mms/service/MmsService;->access$500(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 225
    const-string v4, "MmsService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deleteStoredMessage: invalid message URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :goto_0
    return v1

    .line 231
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 233
    .local v2, "identity":J
    :try_start_0
    iget-object v5, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v5}, Lcom/android/mms/service/MmsService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, p2, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-eq v5, v4, :cond_1

    .line 235
    const-string v5, "MmsService"

    const-string v6, "deleteStoredMessage: failed to delete"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :cond_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_1
    move v1, v4

    .line 243
    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v1, "MmsService"

    const-string v5, "deleteStoredMessage: failed to delete"

    invoke-static {v1, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 241
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v1
.end method

.method public downloadMessage(JLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/os/Bundle;Landroid/app/PendingIntent;)V
    .locals 9
    .param p1, "subId"    # J
    .param p3, "callingPkg"    # Ljava/lang/String;
    .param p4, "locationUrl"    # Ljava/lang/String;
    .param p5, "contentUri"    # Landroid/net/Uri;
    .param p6, "configOverrides"    # Landroid/os/Bundle;
    .param p7, "downloadedIntent"    # Landroid/app/PendingIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 145
    const-string v1, "MmsService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "downloadMessage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 147
    new-instance v0, Lcom/android/mms/service/DownloadRequest;

    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    move-wide v2, p1

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p7

    move-object v7, p3

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/android/mms/service/DownloadRequest;-><init>(Lcom/android/mms/service/MmsRequest$RequestManager;JLjava/lang/String;Landroid/net/Uri;Landroid/app/PendingIntent;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 150
    .local v0, "request":Lcom/android/mms/service/DownloadRequest;
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v0, v1}, Lcom/android/mms/service/DownloadRequest;->tryDownloadingByCarrierApp(Landroid/content/Context;)V

    .line 151
    return-void
.end method

.method public getAutoPersisting()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 328
    const-string v0, "MmsService"

    const-string v1, "getAutoPersisting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v0}, Lcom/android/mms/service/MmsService;->getAutoPersistingPref()Z

    move-result v0

    return v0
.end method

.method public getCarrierConfigValues(J)Landroid/os/Bundle;
    .locals 3
    .param p1, "subId"    # J

    .prologue
    .line 195
    const-string v1, "MmsService"

    const-string v2, "getCarrierConfigValues"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-static {}, Lcom/android/mms/service/MmsConfigManager;->getInstance()Lcom/android/mms/service/MmsConfigManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/android/mms/service/MmsConfigManager;->getMmsConfigBySubId(J)Lcom/android/mms/service/MmsConfig;

    move-result-object v0

    .line 197
    .local v0, "mmsConfig":Lcom/android/mms/service/MmsConfig;
    if-nez v0, :cond_0

    .line 198
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 200
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/mms/service/MmsConfig;->getCarrierConfigValues()Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0
.end method

.method public importMultimediaMessage(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;JZZ)Landroid/net/Uri;
    .locals 10
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "messageId"    # Ljava/lang/String;
    .param p4, "timestampSecs"    # J
    .param p6, "seen"    # Z
    .param p7, "read"    # Z

    .prologue
    .line 214
    const-string v0, "MmsService"

    const-string v1, "importMultimediaMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v0}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 216
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    move-object v8, p1

    # invokes: Lcom/android/mms/service/MmsService;->importMms(Landroid/net/Uri;Ljava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    invoke-static/range {v1 .. v8}, Lcom/android/mms/service/MmsService;->access$400(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Ljava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public importTextMessage(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JZZ)Landroid/net/Uri;
    .locals 9
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "address"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "text"    # Ljava/lang/String;
    .param p5, "timestampMillis"    # J
    .param p7, "seen"    # Z
    .param p8, "read"    # Z

    .prologue
    .line 206
    const-string v0, "MmsService"

    const-string v1, "importTextMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v0}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 208
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-wide v4, p5

    move/from16 v6, p7

    move/from16 v7, p8

    move-object v8, p1

    # invokes: Lcom/android/mms/service/MmsService;->importSms(Ljava/lang/String;ILjava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;
    invoke-static/range {v0 .. v8}, Lcom/android/mms/service/MmsService;->access$300(Lcom/android/mms/service/MmsService;Ljava/lang/String;ILjava/lang/String;JZZLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public sendMessage(JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/PendingIntent;)V
    .locals 11
    .param p1, "subId"    # J
    .param p3, "callingPkg"    # Ljava/lang/String;
    .param p4, "contentUri"    # Landroid/net/Uri;
    .param p5, "locationUrl"    # Ljava/lang/String;
    .param p6, "configOverrides"    # Landroid/os/Bundle;
    .param p7, "sentIntent"    # Landroid/app/PendingIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 129
    const-string v1, "MmsService"

    const-string v2, "sendMessage"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 131
    new-instance v0, Lcom/android/mms/service/SendRequest;

    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    const/4 v5, 0x0

    move-wide v2, p1

    move-object v4, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object v8, p3

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/android/mms/service/SendRequest;-><init>(Lcom/android/mms/service/MmsRequest$RequestManager;JLandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 133
    .local v0, "request":Lcom/android/mms/service/SendRequest;
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-static {p3, v1}, Lcom/android/internal/telephony/SmsApplication;->shouldWriteMessageForPackage(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v0, v1}, Lcom/android/mms/service/SendRequest;->storeInOutbox(Landroid/content/Context;)V

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v0, v1}, Lcom/android/mms/service/SendRequest;->trySendingByCarrierApp(Landroid/content/Context;)V

    .line 139
    return-void
.end method

.method public sendStoredMessage(JLjava/lang/String;Landroid/net/Uri;Landroid/os/Bundle;Landroid/app/PendingIntent;)V
    .locals 1
    .param p1, "subId"    # J
    .param p3, "callingPkg"    # Ljava/lang/String;
    .param p4, "messageUri"    # Landroid/net/Uri;
    .param p5, "configOverrides"    # Landroid/os/Bundle;
    .param p6, "sentIntent"    # Landroid/app/PendingIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 312
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAutoPersisting(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 317
    const-string v2, "MmsService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAutoPersisting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    iget-object v2, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v2}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 319
    iget-object v2, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    const-string v3, "mmspref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/android/mms/service/MmsService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 321
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 322
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "autopersisting"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 323
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 324
    return-void
.end method

.method public updateMmsDownloadStatus(II)V
    .locals 4
    .param p1, "messageRef"    # I
    .param p2, "status"    # I

    .prologue
    .line 175
    const-string v1, "MmsService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateMmsDownloadStatus: ref="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 177
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # getter for: Lcom/android/mms/service/MmsService;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$200(Lcom/android/mms/service/MmsService;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mms/service/MmsRequest;

    .line 178
    .local v0, "request":Lcom/android/mms/service/MmsRequest;
    if-eqz v0, :cond_1

    .line 179
    const/4 v1, 0x6

    if-eq p2, v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Lcom/android/mms/service/MmsRequest;->processResult(Landroid/content/Context;I[B)V

    .line 191
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v1, v0}, Lcom/android/mms/service/MmsService;->addRunning(Lcom/android/mms/service/MmsRequest;)V

    goto :goto_0

    .line 189
    :cond_1
    const-string v1, "MmsService"

    const-string v2, "Failed to find the request to update download status"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateMmsSendStatus(I[BI)V
    .locals 4
    .param p1, "messageRef"    # I
    .param p2, "pdu"    # [B
    .param p3, "status"    # I

    .prologue
    .line 155
    const-string v2, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateMmsSendStatus: ref="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", pdu="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", status="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 158
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # getter for: Lcom/android/mms/service/MmsService;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v1}, Lcom/android/mms/service/MmsService;->access$200(Lcom/android/mms/service/MmsService;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/mms/service/MmsRequest;

    .line 159
    .local v0, "request":Lcom/android/mms/service/MmsRequest;
    if-eqz v0, :cond_2

    .line 160
    const/4 v1, 0x6

    if-eq p3, v1, :cond_1

    .line 162
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v0, v1, p3, p2}, Lcom/android/mms/service/MmsRequest;->processResult(Landroid/content/Context;I[B)V

    .line 171
    :goto_1
    return-void

    .line 155
    .end local v0    # "request":Lcom/android/mms/service/MmsRequest;
    :cond_0
    array-length v1, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 165
    .restart local v0    # "request":Lcom/android/mms/service/MmsRequest;
    :cond_1
    iget-object v1, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    invoke-virtual {v1, v0}, Lcom/android/mms/service/MmsService;->addRunning(Lcom/android/mms/service/MmsRequest;)V

    goto :goto_1

    .line 169
    :cond_2
    const-string v1, "MmsService"

    const-string v2, "Failed to find the request to update send status"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public updateStoredMessageStatus(Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Z
    .locals 3
    .param p1, "callingPkg"    # Ljava/lang/String;
    .param p2, "messageUri"    # Landroid/net/Uri;
    .param p3, "statusValues"    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 277
    const-string v0, "MmsService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateStoredMessageStatus "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->enforceSystemUid()V
    invoke-static {v0}, Lcom/android/mms/service/MmsService;->access$100(Lcom/android/mms/service/MmsService;)V

    .line 279
    iget-object v0, p0, Lcom/android/mms/service/MmsService$1;->this$0:Lcom/android/mms/service/MmsService;

    # invokes: Lcom/android/mms/service/MmsService;->updateMessageStatus(Landroid/net/Uri;Landroid/content/ContentValues;)Z
    invoke-static {v0, p2, p3}, Lcom/android/mms/service/MmsService;->access$600(Lcom/android/mms/service/MmsService;Landroid/net/Uri;Landroid/content/ContentValues;)Z

    move-result v0

    return v0
.end method
