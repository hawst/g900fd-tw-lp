.class Lcom/android/mms/service/MmsService$RequestQueue;
.super Landroid/os/Handler;
.source "MmsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/mms/service/MmsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestQueue"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/mms/service/MmsService;


# direct methods
.method public constructor <init>(Lcom/android/mms/service/MmsService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/android/mms/service/MmsService$RequestQueue;->this$0:Lcom/android/mms/service/MmsService;

    .line 106
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 107
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 111
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/android/mms/service/MmsRequest;

    .line 112
    .local v0, "request":Lcom/android/mms/service/MmsRequest;
    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/android/mms/service/MmsService$RequestQueue;->this$0:Lcom/android/mms/service/MmsService;

    iget-object v2, p0, Lcom/android/mms/service/MmsService$RequestQueue;->this$0:Lcom/android/mms/service/MmsService;

    # getter for: Lcom/android/mms/service/MmsService;->mMmsNetworkManager:Lcom/android/mms/service/MmsNetworkManager;
    invoke-static {v2}, Lcom/android/mms/service/MmsService;->access$000(Lcom/android/mms/service/MmsService;)Lcom/android/mms/service/MmsNetworkManager;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/mms/service/MmsRequest;->execute(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;)V

    .line 115
    :cond_0
    return-void
.end method
