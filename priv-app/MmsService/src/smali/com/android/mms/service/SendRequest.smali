.class public Lcom/android/mms/service/SendRequest;
.super Lcom/android/mms/service/MmsRequest;
.source "SendRequest.java"


# instance fields
.field private final mLocationUrl:Ljava/lang/String;

.field private mPduData:[B

.field private final mPduUri:Landroid/net/Uri;

.field private final mSentIntent:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Lcom/android/mms/service/MmsRequest$RequestManager;JLandroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "manager"    # Lcom/android/mms/service/MmsRequest$RequestManager;
    .param p2, "subId"    # J
    .param p4, "contentUri"    # Landroid/net/Uri;
    .param p5, "messageUri"    # Landroid/net/Uri;
    .param p6, "locationUrl"    # Ljava/lang/String;
    .param p7, "sentIntent"    # Landroid/app/PendingIntent;
    .param p8, "creator"    # Ljava/lang/String;
    .param p9, "configOverrides"    # Landroid/os/Bundle;

    .prologue
    .line 60
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-wide v4, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v1 .. v7}, Lcom/android/mms/service/MmsRequest;-><init>(Lcom/android/mms/service/MmsRequest$RequestManager;Landroid/net/Uri;JLjava/lang/String;Landroid/os/Bundle;)V

    .line 61
    iput-object p4, p0, Lcom/android/mms/service/SendRequest;->mPduUri:Landroid/net/Uri;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    .line 63
    iput-object p6, p0, Lcom/android/mms/service/SendRequest;->mLocationUrl:Ljava/lang/String;

    .line 64
    iput-object p7, p0, Lcom/android/mms/service/SendRequest;->mSentIntent:Landroid/app/PendingIntent;

    .line 65
    return-void
.end method

.method private readPduFromContentUri()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 157
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    if-eqz v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v1

    .line 160
    :cond_1
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mMmsConfig:Lcom/android/mms/service/MmsConfig$Overridden;

    invoke-virtual {v2}, Lcom/android/mms/service/MmsConfig$Overridden;->getMaxMessageSize()I

    move-result v0

    .line 161
    .local v0, "bytesTobeRead":I
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    iget-object v3, p0, Lcom/android/mms/service/SendRequest;->mPduUri:Landroid/net/Uri;

    invoke-interface {v2, v3, v0}, Lcom/android/mms/service/MmsRequest$RequestManager;->readPduFromContentUri(Landroid/net/Uri;I)[B

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    .line 162
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected doHttp(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Lcom/android/mms/service/ApnSettings;)[B
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "netMgr"    # Lcom/android/mms/service/MmsNetworkManager;
    .param p3, "apn"    # Lcom/android/mms/service/ApnSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/mms/service/exception/MmsHttpException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/mms/service/SendRequest;->mLocationUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/android/mms/service/SendRequest;->mLocationUrl:Ljava/lang/String;

    :goto_0
    iget-object v4, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/android/mms/service/SendRequest;->doHttpForResolvedAddresses(Landroid/content/Context;Lcom/android/mms/service/MmsNetworkManager;Ljava/lang/String;[BILcom/android/mms/service/ApnSettings;)[B

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p3}, Lcom/android/mms/service/ApnSettings;->getMmscUrl()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected getPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/mms/service/SendRequest;->mSentIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method protected getRunningQueue()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method protected prepareForHttpRequest()Z
    .locals 1

    .prologue
    .line 220
    invoke-direct {p0}, Lcom/android/mms/service/SendRequest;->readPduFromContentUri()Z

    move-result v0

    return v0
.end method

.method protected revokeUriPermission(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/android/mms/service/SendRequest;->mPduUri:Landroid/net/Uri;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    .line 258
    return-void
.end method

.method public storeInOutbox(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 89
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 92
    .local v10, "identity":J
    :try_start_0
    invoke-direct {p0}, Lcom/android/mms/service/SendRequest;->readPduFromContentUri()Z

    move-result v2

    if-nez v2, :cond_0

    .line 93
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: empty PDU"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/mms/MmsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 150
    :goto_0
    return-void

    .line 96
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    if-nez v2, :cond_6

    .line 98
    new-instance v2, Lcom/google/android/mms/pdu/PduParser;

    iget-object v3, p0, Lcom/android/mms/service/SendRequest;->mPduData:[B

    invoke-direct {v2, v3}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    invoke-virtual {v2}, Lcom/google/android/mms/pdu/PduParser;->parse()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v1

    .line 99
    .local v1, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-nez v1, :cond_1

    .line 100
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: can\'t parse input PDU"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/google/android/mms/MmsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 103
    :cond_1
    :try_start_2
    instance-of v2, v1, Lcom/google/android/mms/pdu/SendReq;

    if-nez v2, :cond_2

    .line 104
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: not SendReq"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/mms/MmsException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 107
    :cond_2
    :try_start_3
    invoke-static {p1}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    move-result-object v0

    .line 108
    .local v0, "persister":Lcom/google/android/mms/pdu/PduPersister;
    sget-object v2, Landroid/provider/Telephony$Mms$Outbox;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/mms/pdu/PduPersister;->persist(Lcom/google/android/mms/pdu/GenericPdu;Landroid/net/Uri;ZZLjava/util/HashMap;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    .line 114
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    if-nez v2, :cond_3

    .line 115
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: can not persist message"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Lcom/google/android/mms/MmsException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 118
    :cond_3
    :try_start_4
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x5

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 119
    .local v5, "values":Landroid/content/ContentValues;
    const-string v2, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v12, 0x3e8

    div-long/2addr v6, v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    const-string v2, "read"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 121
    const-string v2, "seen"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 122
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mCreator:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 123
    const-string v2, "creator"

    iget-object v3, p0, Lcom/android/mms/service/SendRequest;->mCreator:Ljava/lang/String;

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_4
    const-string v2, "sub_id"

    iget-wide v6, p0, Lcom/android/mms/service/SendRequest;->mSubId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eq v2, v9, :cond_5

    .line 128
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: failed to update message"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Lcom/google/android/mms/MmsException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 148
    .end local v0    # "persister":Lcom/google/android/mms/pdu/PduPersister;
    .end local v1    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    :cond_5
    :goto_1
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 133
    .end local v5    # "values":Landroid/content/ContentValues;
    :cond_6
    :try_start_5
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 135
    .restart local v5    # "values":Landroid/content/ContentValues;
    const-string v2, "date"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v12, 0x3e8

    div-long/2addr v6, v12

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 136
    const-string v2, "msg_box"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    const-string v2, "sub_id"

    iget-wide v6, p0, Lcom/android/mms/service/SendRequest;->mSubId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 138
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eq v2, v9, :cond_5

    .line 140
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: failed to update message"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/google/android/mms/MmsException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 143
    .end local v5    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v8

    .line 144
    .local v8, "e":Lcom/google/android/mms/MmsException;
    :try_start_6
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: can not persist/update message"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .line 145
    .end local v8    # "e":Lcom/google/android/mms/MmsException;
    :catch_1
    move-exception v8

    .line 146
    .local v8, "e":Ljava/lang/RuntimeException;
    :try_start_7
    const-string v2, "MmsService"

    const-string v3, "SendRequest.storeInOutbox: unexpected parsing failure"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 148
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto/16 :goto_0

    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method protected transferResponse(Landroid/content/Intent;[B)Z
    .locals 1
    .param p1, "fillIn"    # Landroid/content/Intent;
    .param p2, "response"    # [B

    .prologue
    .line 209
    if-eqz p2, :cond_0

    .line 210
    const-string v0, "android.telephony.extra.MMS_DATA"

    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 212
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public trySendingByCarrierApp(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 229
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/telephony/TelephonyManager;

    .line 231
    .local v11, "telephonyManager":Landroid/telephony/TelephonyManager;
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.provider.Telephony.MMS_SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 232
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v11, v1}, Landroid/telephony/TelephonyManager;->getCarrierPackageNamesForIntent(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v10

    .line 235
    .local v10, "carrierPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v10, :cond_0

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/android/mms/service/SendRequest;->mRequestManager:Lcom/android/mms/service/MmsRequest$RequestManager;

    invoke-interface {v0, p0}, Lcom/android/mms/service/MmsRequest$RequestManager;->addRunning(Lcom/android/mms/service/MmsRequest;)V

    .line 253
    :goto_0
    return-void

    .line 238
    :cond_1
    invoke-interface {v10, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 239
    const-string v0, "android.provider.Telephony.extra.MMS_CONTENT_URI"

    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mPduUri:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 240
    const-string v0, "android.provider.Telephony.extra.MMS_LOCATION_URL"

    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mLocationUrl:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const/high16 v0, 0x8000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 242
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    const-string v3, "android.permission.RECEIVE_MMS"

    const/16 v4, 0x12

    iget-object v5, p0, Lcom/android/mms/service/SendRequest;->mCarrierAppResultReceiver:Landroid/content/BroadcastReceiver;

    move-object v0, p1

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;ILandroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected updateStatus(Landroid/content/Context;I[B)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "result"    # I
    .param p3, "response"    # [B

    .prologue
    .line 167
    iget-object v2, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    if-nez v2, :cond_0

    .line 197
    :goto_0
    return-void

    .line 170
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v10

    .line 172
    .local v10, "identity":J
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    const/4 v9, 0x2

    .line 174
    .local v9, "messageStatus":I
    :goto_1
    const/4 v13, 0x0

    .line 175
    .local v13, "sendConf":Lcom/google/android/mms/pdu/SendConf;
    if-eqz p3, :cond_1

    :try_start_0
    move-object/from16 v0, p3

    array-length v2, v0

    if-lez v2, :cond_1

    .line 176
    new-instance v2, Lcom/google/android/mms/pdu/PduParser;

    move-object/from16 v0, p3

    invoke-direct {v2, v0}, Lcom/google/android/mms/pdu/PduParser;-><init>([B)V

    invoke-virtual {v2}, Lcom/google/android/mms/pdu/PduParser;->parse()Lcom/google/android/mms/pdu/GenericPdu;

    move-result-object v12

    .line 177
    .local v12, "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    if-eqz v12, :cond_1

    instance-of v2, v12, Lcom/google/android/mms/pdu/SendConf;

    if-eqz v2, :cond_1

    .line 178
    move-object v0, v12

    check-cast v0, Lcom/google/android/mms/pdu/SendConf;

    move-object v13, v0

    .line 181
    .end local v12    # "pdu":Lcom/google/android/mms/pdu/GenericPdu;
    :cond_1
    new-instance v5, Landroid/content/ContentValues;

    const/4 v2, 0x3

    invoke-direct {v5, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 182
    .local v5, "values":Landroid/content/ContentValues;
    const-string v2, "msg_box"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    if-eqz v13, :cond_2

    .line 184
    const-string v2, "resp_st"

    invoke-virtual {v13}, Lcom/google/android/mms/pdu/SendConf;->getResponseStatus()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 185
    const-string v2, "m_id"

    invoke-virtual {v13}, Lcom/google/android/mms/pdu/SendConf;->getMessageId()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/mms/pdu/PduPersister;->toIsoString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/android/mms/service/SendRequest;->mMessageUri:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/mms/util/SqliteWrapper;->update(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 172
    .end local v5    # "values":Landroid/content/ContentValues;
    .end local v9    # "messageStatus":I
    .end local v13    # "sendConf":Lcom/google/android/mms/pdu/SendConf;
    :cond_3
    const/4 v9, 0x5

    goto :goto_1

    .line 190
    .restart local v9    # "messageStatus":I
    .restart local v13    # "sendConf":Lcom/google/android/mms/pdu/SendConf;
    :catch_0
    move-exception v8

    .line 191
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    const-string v2, "MmsService"

    const-string v3, "SendRequest.updateStatus: can not update message"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 195
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .line 192
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 193
    .local v8, "e":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v2, "MmsService"

    const-string v3, "SendRequest.updateStatus: can not parse response"

    invoke-static {v2, v3, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 195
    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    invoke-static {v10, v11}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method
