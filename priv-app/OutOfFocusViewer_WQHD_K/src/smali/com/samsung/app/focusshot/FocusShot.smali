.class public Lcom/samsung/app/focusshot/FocusShot;
.super Ljava/lang/Object;
.source "FocusShot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/focusshot/FocusShot$AIFParam;,
        Lcom/samsung/app/focusshot/FocusShot$EyeRoi;,
        Lcom/samsung/app/focusshot/FocusShot$FaceInfo;,
        Lcom/samsung/app/focusshot/FocusShot$FaceRoi;,
        Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;,
        Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;,
        Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;,
        Lcom/samsung/app/focusshot/FocusShot$MouthRoi;,
        Lcom/samsung/app/focusshot/FocusShot$OFParam;,
        Lcom/samsung/app/focusshot/FocusShot$Point;,
        Lcom/samsung/app/focusshot/FocusShot$RFParam;
    }
.end annotation


# instance fields
.field public FIPlane:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

.field public FRParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

.field public aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

.field public focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

.field public mapData:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

.field public ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

.field public outdata:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

.field public pFS_Handle:I

.field public point:Lcom/samsung/app/focusshot/FocusShot$Point;

.field public rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

.field public runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->outdata:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    .line 17
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->mapData:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    .line 18
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 19
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$OFParam;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$OFParam;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 20
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$AIFParam;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 21
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$RFParam;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$RFParam;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 22
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$Point;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$Point;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 23
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    invoke-direct {v0, p0}, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;-><init>(Lcom/samsung/app/focusshot/FocusShot;)V

    iput-object v0, p0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    .line 24
    return-void
.end method
