.class public Lcom/samsung/app/focusshot/Engine;
.super Ljava/lang/Object;
.source "Engine.java"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    :try_start_0
    const-string v1, "SRIB_FocusShot"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 21
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 17
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 18
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "FocusShot"

    const-string v2, "FocusShot Engine load fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native FocusShotCreate()I
.end method

.method public static native FocusShotDeInit(I)I
.end method

.method public static native FocusShotInit(ILcom/samsung/app/focusshot/FocusShot$FocalImagePlane;Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;II)I
.end method

.method public static native FocusShotJniVersion()V
.end method

.method public static native FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I
.end method

.method public static native FocusShotSetInputFrame(I[BIIIII)I
.end method

.method public static native IsFastJpegCodecUsed()I
.end method

.method public static native getKernelSize(II)I
.end method
