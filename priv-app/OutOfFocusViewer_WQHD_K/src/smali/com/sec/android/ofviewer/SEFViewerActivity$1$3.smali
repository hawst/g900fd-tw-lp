.class Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;)Lcom/sec/android/ofviewer/SEFViewerActivity$1;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 234
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "textEntered":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v4

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$14(Lcom/sec/android/ofviewer/SEFViewerActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 236
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 235
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "saveFilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 239
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v2

    .line 240
    const v3, 0x7f070006

    const/4 v4, 0x1

    .line 239
    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    .line 240
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 254
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v2

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$15(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 243
    new-instance v2, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3$1;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;Ljava/lang/String;)V

    .line 249
    invoke-virtual {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3$1;->start()V

    .line 250
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v2

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->cancel()V

    goto :goto_0
.end method
