.class Lcom/sec/android/ofviewer/SEFViewerActivity$4;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;->doSaveAs(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

.field private final synthetic val$outFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iput-object p2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->val$outFilePath:Ljava/lang/String;

    .line 653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 3

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->conn:Landroid/media/MediaScannerConnection;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$35(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/media/MediaScannerConnection;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->val$outFilePath:Ljava/lang/String;

    const-string v2, "image/jpeg"

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 657
    const-string v0, "oftime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MediaScannerConnection time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 658
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$23(Lcom/sec/android/ofviewer/SEFViewerActivity;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 657
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$4;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->conn:Landroid/media/MediaScannerConnection;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$35(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/media/MediaScannerConnection;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 660
    return-void
.end method
