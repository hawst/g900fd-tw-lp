.class public Lcom/sec/android/ofviewer/AnimImageView;
.super Landroid/widget/FrameLayout;
.source "AnimImageView.java"


# static fields
.field private static final ANIM_DURATION:I = 0xfa

.field private static final USE_PINCH_ZOOM:Z = true


# instance fields
.field private animBitmap:Landroid/graphics/Bitmap;

.field private hideAnim:Landroid/view/animation/Animation;

.field public isAnimating:Z

.field private mBitmap:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mImageView0:Landroid/widget/ImageView;

.field private mImageView1:Landroid/widget/ImageView;

.field private mImageView2:Landroid/widget/ImageView;

.field private showAnim:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->isAnimating:Z

    .line 34
    invoke-direct {p0}, Lcom/sec/android/ofviewer/AnimImageView;->init()V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->isAnimating:Z

    .line 29
    invoke-direct {p0}, Lcom/sec/android/ofviewer/AnimImageView;->init()V

    .line 30
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->animBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private init()V
    .locals 7

    .prologue
    const-wide/16 v5, 0xfa

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 38
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/ofviewer/AnimImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    .line 39
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/ofviewer/AnimImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    .line 41
    new-instance v0, Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-virtual {p0}, Lcom/sec/android/ofviewer/AnimImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/ofviewer/ZoomImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    .line 44
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 46
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 47
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/AnimImageView;->setBackgroundColor(I)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/AnimImageView;->addView(Landroid/view/View;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/AnimImageView;->addView(Landroid/view/View;)V

    .line 50
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/AnimImageView;->addView(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 61
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->hideAnim:Landroid/view/animation/Animation;

    .line 62
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->showAnim:Landroid/view/animation/Animation;

    .line 63
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->hideAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->showAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->hideAnim:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/ofviewer/AnimImageView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/ofviewer/AnimImageView$1;-><init>(Lcom/sec/android/ofviewer/AnimImageView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 91
    return-void
.end method


# virtual methods
.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 96
    if-nez p1, :cond_0

    .line 113
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mBitmap:Ljava/lang/ref/WeakReference;

    .line 99
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 105
    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView0:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mBitmap:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->animBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 107
    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mBitmap:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->hideAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 110
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView;->showAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 111
    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView;->animBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
