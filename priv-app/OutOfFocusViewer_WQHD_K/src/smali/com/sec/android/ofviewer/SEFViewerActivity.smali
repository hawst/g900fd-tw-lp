.class public Lcom/sec/android/ofviewer/SEFViewerActivity;
.super Landroid/app/Activity;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextView;,
        Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;,
        Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;,
        Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;,
        Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "OFViewerApp"

.field static final TEMP:Ljava/lang/String; = "yim"


# instance fields
.field private final DEVICE_NAME:Ljava/lang/String;

.field private final INVALID_CHAR:[Ljava/lang/String;

.field private final MSG_HIDE_PROGRESS:I

.field private final MSG_OUTPUT_UPDATED:I

.field private final MSG_SHOW_COVER:I

.field private final MSG_SHOW_OUTPUT:I

.field private final MSG_SHOW_PROGRESS:I

.field private final MSG_SHOW_SAVE_AS:I

.field private RGB_IMAGE_HEIGHT:I

.field private RGB_IMAGE_WIDTH:I

.field private final SAVE_AS_QUEUE:I

.field private final SCREEN_HEIGHT:I

.field private final SCREEN_WIDTH:I

.field private afData:[B

.field private conn:Landroid/media/MediaScannerConnection;

.field private depthData:[B

.field private disableAll:Z

.field public effectInProgress:Z

.field private exitingApp:Z

.field private inEngineCall:Z

.field private infData:[[B

.field private info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

.field private isFocusShotEngineInitialized:Z

.field private mAllFocus:Landroid/widget/TextView;

.field private mAnimView:Lcom/sec/android/ofviewer/AnimImageView;

.field private mCoverBitmap:Landroid/graphics/Bitmap;

.field private mCoverFocusedArea:I

.field private mCoverImageMode:I

.field private mCurrentMode:I

.field private mDateFormat:Ljava/text/SimpleDateFormat;

.field private mDefaultBlur:I

.field private mDisableAIF:Z

.field private mDisplayBmp:Landroid/graphics/Bitmap;

.field private mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

.field private mFilePath:Ljava/lang/String;

.field private mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

.field private mFocusedArea:I

.field private mFrontFocus:Landroid/widget/TextView;

.field private mHandler:Landroid/os/Handler;

.field private mImageHeight:I

.field private mImageRotation:I

.field private mImageWidth:I

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mQueue:I

.field private mRearFocus:Landroid/widget/TextView;

.field private mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

.field private mReturnCode:I

.field private mSave:Landroid/view/MenuItem;

.field private mSaveAs:Landroid/view/MenuItem;

.field private mSaveAsDialog:Landroid/app/AlertDialog;

.field private mSaveAsEditText:Landroid/widget/EditText;

.field private mTime:J

.field private outputData:[B

.field private outputUpdated:Z

.field private prevEngMode:I

.field private psHandle:I

.field private rgbCache:[[I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x3

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 57
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    .line 65
    iput v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_OUTPUT_UPDATED:I

    .line 66
    iput v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_SHOW_PROGRESS:I

    .line 67
    iput v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_HIDE_PROGRESS:I

    .line 68
    iput v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_SHOW_OUTPUT:I

    .line 69
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_SHOW_COVER:I

    .line 70
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->MSG_SHOW_SAVE_AS:I

    .line 72
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    .line 73
    iput v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    .line 86
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    .line 87
    iput v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 92
    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    .line 93
    const/16 v0, 0x780

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->SCREEN_WIDTH:I

    .line 94
    const/16 v0, 0x438

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->SCREEN_HEIGHT:I

    .line 95
    new-array v0, v3, [[I

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    .line 96
    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverImageMode:I

    .line 98
    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverFocusedArea:I

    .line 99
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    .line 101
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputUpdated:Z

    .line 104
    iput-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 106
    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    .line 109
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->SAVE_AS_QUEUE:I

    .line 111
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 113
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "\\"

    aput-object v1, v0, v2

    const-string v1, "/"

    aput-object v1, v0, v4

    const-string v1, ":"

    aput-object v1, v0, v5

    const-string v1, "*"

    aput-object v1, v0, v3

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->INVALID_CHAR:[Ljava/lang/String;

    .line 114
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->DEVICE_NAME:Ljava/lang/String;

    .line 116
    new-instance v0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    .line 1298
    new-instance v0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$2;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 57
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1164
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->prepareOutput()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1206
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/ofviewer/SEFViewerActivity;)Ljava/text/SimpleDateFormat;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDateFormat:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/ofviewer/SEFViewerActivity;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->INVALID_CHAR:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/ofviewer/SEFViewerActivity;Landroid/app/AlertDialog;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/ofviewer/SEFViewerActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$15(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1200
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/ofviewer/SEFViewerActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 555
    invoke-direct {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->doSaveAs(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$17(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    return v0
.end method

.method static synthetic access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    return v0
.end method

.method static synthetic access$19(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V
    .locals 0

    .prologue
    .line 87
    iput p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V
    .locals 0

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputUpdated:Z

    return-void
.end method

.method static synthetic access$20(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 800
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->doReFocus()V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/ofviewer/SEFViewerActivity;J)V
    .locals 0

    .prologue
    .line 100
    iput-wide p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    return-void
.end method

.method static synthetic access$22(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 948
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->initSEF()V

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/ofviewer/SEFViewerActivity;)J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    return-wide v0
.end method

.method static synthetic access$24(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    return v0
.end method

.method static synthetic access$25(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V
    .locals 0

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    return-void
.end method

.method static synthetic access$26(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1038
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->initOutFocusEngine()V

    return-void
.end method

.method static synthetic access$27(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    return v0
.end method

.method static synthetic access$28(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    return v0
.end method

.method static synthetic access$29(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1229
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAllViews()V

    return-void
.end method

.method static synthetic access$30(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V
    .locals 0

    .prologue
    .line 493
    invoke-direct {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V

    return-void
.end method

.method static synthetic access$31(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$32(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V
    .locals 0

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    return-void
.end method

.method static synthetic access$33(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1213
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$34(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 675
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->saveSefOutputAndExit()V

    return-void
.end method

.method static synthetic access$35(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/media/MediaScannerConnection;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->conn:Landroid/media/MediaScannerConnection;

    return-object v0
.end method

.method static synthetic access$36(Lcom/sec/android/ofviewer/SEFViewerActivity;)[[I
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    return-object v0
.end method

.method static synthetic access$37(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    return-void
.end method

.method static synthetic access$38(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    return v0
.end method

.method static synthetic access$39(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/samsung/app/focusshot/FocusShot;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1215
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->enableAllViews()V

    return-void
.end method

.method static synthetic access$40(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    return-void
.end method

.method static synthetic access$41(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V
    .locals 0

    .prologue
    .line 106
    iput p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    return-void
.end method

.method static synthetic access$42(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    return v0
.end method

.method static synthetic access$43(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    return v0
.end method

.method static synthetic access$44(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    return v0
.end method

.method static synthetic access$45(Lcom/sec/android/ofviewer/SEFViewerActivity;)I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/sec/android/ofviewer/AnimImageView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAnimView:Lcom/sec/android/ofviewer/AnimImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/ofviewer/SEFViewerActivity;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method private disableAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1230
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1231
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1232
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1233
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 1234
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1235
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0x66

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1237
    :cond_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAs:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 1238
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAs:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1241
    :cond_1
    return-void
.end method

.method private doInitialAIF()V
    .locals 9

    .prologue
    const/4 v5, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1126
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v1, v4, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 1127
    .local v1, "sFocalRunParam":Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;
    iput v5, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    .line 1128
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v4, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 1129
    .local v0, "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    const/4 v4, 0x2

    iput v4, v0, Lcom/samsung/app/focusshot/FocusShot$AIFParam;->s32Frames:I

    .line 1130
    iput-object v0, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sAllFocus:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 1131
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iput-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sOutFocus:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 1132
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iput-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 1133
    iput v5, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumThreads:I

    .line 1134
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    iput v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumFaces:I

    .line 1135
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 1136
    iget-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v5, v5, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    iput v5, v4, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 1137
    iget-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v5, v5, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    iput v5, v4, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    .line 1138
    iput v7, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->ps32Progresscallback:I

    .line 1140
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.s32DispHeight "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1141
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32DispHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1140
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1142
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.s32DispWidth "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32DispWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.s32OrgWidth "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OrgWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.s32OrgHeight "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OrgHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.s32OpMode "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mFocusShot.focusShotProc.ps32PrevBuf.length "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1147
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->ps32PrevBuf:[I

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1146
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1148
    const-string v4, "OFViewerApp"

    const-string v5, "INITIAL ALL IN FOCUS"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1149
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1150
    .local v2, "time":J
    iput-boolean v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 1152
    iget v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    invoke-static {v4, v1, v7, v5}, Lcom/samsung/app/focusshot/Engine;->FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I

    move-result v4

    .line 1151
    iput v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    .line 1153
    iput-boolean v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 1154
    iget-boolean v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    if-eqz v4, :cond_0

    .line 1162
    :goto_0
    return-void

    .line 1156
    :cond_0
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Engine.FocusShotProcess doInitialAIF returned "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    iget v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    if-eqz v4, :cond_1

    .line 1158
    iput-boolean v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    .line 1160
    :cond_1
    const-string v4, "oftime"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "initial AllinFocus "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    const-string v4, "OFViewerApp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PROCESSING TIME "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private doReFocus()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v3, 0x3

    const/4 v5, 0x0

    .line 802
    invoke-direct {p0, v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V

    .line 804
    iget-boolean v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-nez v2, :cond_0

    .line 805
    iput v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    .line 806
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 873
    :goto_0
    return-void

    .line 810
    :cond_0
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v1, v2, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 811
    .local v1, "sFocalRunParam":Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;
    iget v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    if-nez v2, :cond_1

    .line 812
    iput v6, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    .line 816
    :goto_1
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v2, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 817
    .local v0, "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    iput-object v0, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sAllFocus:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 818
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v2, v2, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iput-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sOutFocus:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 819
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v2, v2, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iput-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 820
    const/4 v2, 0x4

    iput v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumThreads:I

    .line 821
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v2, v2, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v2, v2, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    iput v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumFaces:I

    .line 823
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v2, v2, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 824
    iget-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    aget v3, v3, v5

    iput v3, v2, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 825
    iget-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    iput v3, v2, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    .line 826
    iget-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v2, v2, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput v5, v2, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 827
    iget-object v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v2, v2, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput v5, v2, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    .line 829
    const-string v2, "OFViewerApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "APPLYING REFOCUS AT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v4, v4, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 830
    iget-object v4, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v4, v4, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 829
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iput v5, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->ps32Progresscallback:I

    .line 832
    iput v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    .line 833
    const-string v2, "OFViewerApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isFocusChanged() "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusChanged()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusChanged()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 835
    new-instance v2, Lcom/sec/android/ofviewer/SEFViewerActivity$5;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity$5;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;)V

    .line 868
    invoke-virtual {v2}, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->start()V

    goto/16 :goto_0

    .line 814
    .end local v0    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    :cond_1
    const/4 v2, 0x6

    iput v2, v1, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    goto/16 :goto_1

    .line 870
    .restart local v0    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 871
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V

    goto/16 :goto_0
.end method

.method private doSaveAs(Ljava/lang/String;)V
    .locals 17
    .param p1, "outFilePath"    # Ljava/lang/String;

    .prologue
    .line 556
    const-string v3, "OFViewerApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SAVING JPG..."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v4, 0x1

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 558
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-static {v3, v4}, Lcom/sec/android/ofviewer/Constants;->getRGBCacheId(II)I

    move-result v13

    .line 559
    .local v13, "id":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    if-ne v13, v3, :cond_0

    .line 560
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v4, 0x2

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 562
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    move-object/from16 v0, p1

    iput-object v0, v3, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->savePath:Ljava/lang/String;

    .line 563
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v14, v3, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 564
    .local v14, "sFocalRunParam":Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_9

    .line 565
    const/4 v3, 0x4

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    .line 566
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v9, v3, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 567
    .local v9, "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    const/4 v3, 0x2

    iput v3, v9, Lcom/samsung/app/focusshot/FocusShot$AIFParam;->s32Frames:I

    .line 568
    iput-object v9, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sAllFocus:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 569
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sOutFocus:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 570
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 571
    const/4 v3, 0x4

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumThreads:I

    .line 572
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumFaces:I

    .line 573
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 574
    const/4 v3, 0x0

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->ps32Progresscallback:I

    .line 592
    :goto_0
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 593
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 594
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v4, v4, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/app/focusshot/Engine;->FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I

    .line 595
    const-string v3, "oftime"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FINAL FOCUSSHOTPROCESS: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v5, v15

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    new-instance v3, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 597
    const-string v3, "TAG"

    const-string v4, "temporary jpeg file was not created... manual creation starts"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    new-instance v2, Landroid/graphics/YuvImage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    const/16 v4, 0x14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    .line 599
    const/4 v7, 0x0

    .line 598
    invoke-direct/range {v2 .. v7}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    .line 600
    .local v2, "img":Landroid/graphics/YuvImage;
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 602
    .local v11, "f":Ljava/io/File;
    :try_start_0
    invoke-virtual {v11}, Ljava/io/File;->createNewFile()Z

    .line 603
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 604
    .local v12, "fos":Ljava/io/FileOutputStream;
    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v12}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    .line 605
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->flush()V

    .line 606
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    .end local v2    # "img":Landroid/graphics/YuvImage;
    .end local v11    # "f":Ljava/io/File;
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 612
    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    .line 613
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v4, 0x0

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 615
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 617
    new-instance v8, Landroid/media/ExifInterface;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 618
    .local v8, "_interface":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    new-instance v4, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitude:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 620
    const-string v3, "GPSAltitude"

    .line 621
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitude:Ljava/lang/String;

    .line 620
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitudeRef:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 623
    const-string v3, "GPSAltitudeRef"

    .line 624
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitudeRef:Ljava/lang/String;

    .line 623
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitude:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 632
    const-string v3, "GPSLatitude"

    .line 633
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitude:Ljava/lang/String;

    .line 632
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitudeRef:Ljava/lang/String;

    if-eqz v3, :cond_5

    .line 635
    const-string v3, "GPSLatitudeRef"

    .line 636
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitudeRef:Ljava/lang/String;

    .line 635
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitude:Ljava/lang/String;

    if-eqz v3, :cond_6

    .line 638
    const-string v3, "GPSLongitude"

    .line 639
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitude:Ljava/lang/String;

    .line 638
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitudeRef:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 641
    const-string v3, "GPSLongitudeRef"

    .line 642
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitudeRef:Ljava/lang/String;

    .line 641
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v3, v3, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsProcessMethod:Ljava/lang/String;

    if-eqz v3, :cond_8

    .line 644
    const-string v3, "GPSProcessingMethod"

    .line 645
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v4, v4, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsProcessMethod:Ljava/lang/String;

    .line 644
    invoke-virtual {v8, v3, v4}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    :cond_8
    invoke-virtual {v8}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 650
    const-string v3, "oftime"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RESTORING EXIF ATTRIBUTES: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-wide v15, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v5, v15

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 651
    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 650
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    new-instance v3, Landroid/media/MediaScannerConnection;

    new-instance v4, Lcom/sec/android/ofviewer/SEFViewerActivity$4;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity$4;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->conn:Landroid/media/MediaScannerConnection;

    .line 667
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->conn:Landroid/media/MediaScannerConnection;

    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->connect()V

    .line 668
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 673
    .end local v8    # "_interface":Landroid/media/ExifInterface;
    :goto_2
    return-void

    .line 576
    .end local v9    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    :cond_9
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    if-nez v3, :cond_a

    .line 577
    const/4 v3, 0x5

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    .line 580
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v9, v3, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 581
    .restart local v9    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    iput-object v9, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sAllFocus:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 582
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sOutFocus:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 583
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 584
    const/4 v3, 0x4

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumThreads:I

    .line 585
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumFaces:I

    .line 586
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 587
    iget-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 588
    iget-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    .line 589
    iget-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    const/4 v4, 0x0

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 590
    iget-object v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot$RFParam;->sTouchPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    const/4 v4, 0x0

    iput v4, v3, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    goto/16 :goto_0

    .line 579
    .end local v9    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    :cond_a
    const/4 v3, 0x6

    iput v3, v14, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    goto :goto_3

    .line 607
    .restart local v2    # "img":Landroid/graphics/YuvImage;
    .restart local v9    # "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    .restart local v11    # "f":Ljava/io/File;
    :catch_0
    move-exception v10

    .line 608
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 670
    .end local v2    # "img":Landroid/graphics/YuvImage;
    .end local v10    # "e":Ljava/io/IOException;
    .end local v11    # "f":Ljava/io/File;
    :catch_1
    move-exception v10

    .line 671
    .restart local v10    # "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method private enableAllViews()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1216
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1217
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1218
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1219
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 1220
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1221
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v2, 0xff

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAs:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 1224
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAs:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1227
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1218
    goto :goto_0
.end method

.method private hideProgressIcon()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    .line 1208
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1209
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1211
    :cond_0
    return-void
.end method

.method private initAllViews()V
    .locals 2

    .prologue
    .line 1248
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    .line 1249
    const v0, 0x7f0a0004

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    .line 1250
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    .line 1255
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1256
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1257
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1262
    return-void
.end method

.method private initOutFocusEngine()V
    .locals 12

    .prologue
    .line 1039
    const-string v0, "OFViewerApp"

    const-string v1, "INITIALIZING FOCUS SHOT ENGINE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    new-instance v0, Lcom/samsung/app/focusshot/FocusShot;

    invoke-direct {v0}, Lcom/samsung/app/focusshot/FocusShot;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    .line 1041
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 1042
    invoke-static {}, Lcom/samsung/app/focusshot/Engine;->FocusShotCreate()I

    move-result v0

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    .line 1043
    const-string v0, "oftime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Engine.FocusShotCreate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    if-nez v0, :cond_0

    .line 1045
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 1123
    :goto_0
    return-void

    .line 1049
    :cond_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v8, v0, Lcom/samsung/app/focusshot/FocusShot;->outdata:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    .line 1050
    .local v8, "fOutImg":Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Width:I

    .line 1051
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Height:I

    .line 1052
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Pitch:I

    .line 1053
    const/4 v0, 0x0

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32TopLeft:I

    .line 1054
    const/4 v0, 0x1

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32ImgFmt:I

    .line 1055
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    iput-object v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->ps8PacData:[B

    .line 1056
    const/4 v0, 0x0

    iput v0, v8, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32FCode:I

    .line 1058
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v7, v0, Lcom/samsung/app/focusshot/FocusShot;->mapData:Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;

    .line 1059
    .local v7, "fDepthMap":Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    div-int/lit8 v0, v0, 0x2

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Width:I

    .line 1060
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Height:I

    .line 1062
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    div-int/lit8 v0, v0, 0x2

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32Pitch:I

    .line 1063
    const/4 v0, 0x0

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32TopLeft:I

    .line 1064
    const/16 v0, 0xc

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32ImgFmt:I

    .line 1065
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->depthData:[B

    iput-object v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->ps8PacData:[B

    .line 1067
    const/4 v0, 0x1

    iput v0, v7, Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;->s32FCode:I

    .line 1068
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 1069
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    .line 1070
    const/4 v2, 0x1

    .line 1069
    invoke-static {v0, v8, v7, v1, v2}, Lcom/samsung/app/focusshot/Engine;->FocusShotInit(ILcom/samsung/app/focusshot/FocusShot$FocalImagePlane;Lcom/samsung/app/focusshot/FocusShot$FocalImagePlane;II)I

    move-result v10

    .line 1071
    .local v10, "retValue":I
    const-string v0, "oftime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Engine.FocusShotInit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    if-eqz v10, :cond_1

    .line 1073
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    goto :goto_0

    .line 1076
    :cond_1
    const-string v0, "OFViewerApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FocusShotSetInputFrame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1077
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    invoke-virtual {v2}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->secondHeader()Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1076
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1079
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 1080
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    array-length v2, v2

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    .line 1081
    iget v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v6, v6, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v6, v6, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    const/4 v11, 0x0

    aget v6, v6, v11

    .line 1080
    invoke-static/range {v0 .. v6}, Lcom/samsung/app/focusshot/Engine;->FocusShotSetInputFrame(I[BIIIII)I

    move-result v10

    .line 1082
    const-string v0, "oftime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Engine.FocusShotSetInputFrame(af) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1083
    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1082
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    if-eqz v10, :cond_2

    .line 1085
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    goto/16 :goto_0

    .line 1088
    :cond_2
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    array-length v0, v0

    if-lt v9, v0, :cond_4

    .line 1104
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 1105
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OrgWidth:I

    .line 1106
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OrgHeight:I

    .line 1107
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32DispWidth:I

    .line 1108
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32DispHeight:I

    .line 1109
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    mul-int/2addr v1, v2

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->ps32PrevBuf:[I

    .line 1114
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v0, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDefaultBlur:I

    iput v1, v0, Lcom/samsung/app/focusshot/FocusShot$OFParam;->s32Lens:I

    .line 1116
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v0, :cond_3

    .line 1117
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->doInitialAIF()V

    .line 1119
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    .line 1120
    const-string v0, "OFViewerApp"

    const-string v1, "INITIALIZING FOCUS SHOT ENGINE DONE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1121
    const-string v0, "OFViewerApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IsFastJpegCodecUsed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/app/focusshot/Engine;->IsFastJpegCodecUsed()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1089
    :cond_4
    const-string v0, "OFViewerApp"

    .line 1090
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FocusShotSetInputFrame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    aget-object v2, v2, v9

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v9, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1091
    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    invoke-virtual {v2}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->secondHeader()Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    add-int/lit8 v3, v9, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1090
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1089
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 1093
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    aget-object v1, v1, v9

    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    aget-object v2, v2, v9

    array-length v2, v2

    .line 1094
    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    iget v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    add-int/lit8 v5, v9, 0x1

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v6, v6, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v6, v6, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    add-int/lit8 v11, v9, 0x1

    aget v6, v6, v11

    .line 1093
    invoke-static/range {v0 .. v6}, Lcom/samsung/app/focusshot/Engine;->FocusShotSetInputFrame(I[BIIIII)I

    move-result v10

    .line 1095
    const-string v0, "oftime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Engine.FocusShotSetInputFrame(inf) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1096
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1095
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    if-eqz v10, :cond_5

    .line 1098
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    goto/16 :goto_0

    .line 1088
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1
.end method

.method private initSEF()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 949
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "INITIALIZING SEF.."

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/secvision/sef/SEF;->getSEFDataCount(Ljava/lang/String;)I

    move-result v0

    .line 951
    .local v0, "count":I
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/secvision/sef/SEF;->listKeyNames(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 952
    .local v4, "keyNames":[Ljava/lang/String;
    if-eqz v4, :cond_0

    if-ge v0, v14, :cond_1

    .line 953
    :cond_0
    iput-boolean v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 1036
    :goto_0
    return-void

    .line 957
    :cond_1
    array-length v9, v4

    move v8, v6

    :goto_1
    if-lt v8, v9, :cond_2

    .line 961
    add-int/lit8 v8, v0, -0x3

    new-array v8, v8, [[B

    iput-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    .line 962
    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    iget v9, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    mul-int/2addr v8, v9

    mul-int/lit8 v8, v8, 0x2

    new-array v8, v8, [B

    iput-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    .line 964
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-lt v2, v0, :cond_3

    .line 1029
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    array-length v8, v8

    if-lez v8, :cond_a

    .line 1030
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    aget-object v8, v8, v6

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->depthData:[B

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    if-eqz v8, :cond_a

    .line 1031
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mReturnCode:I

    if-nez v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfInputs:I

    if-le v8, v7, :cond_a

    .line 1029
    :goto_3
    iput-boolean v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 1033
    const-string v6, "OFViewerApp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "disableAll = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    const-string v6, "OFViewerApp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "output buffer size is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 957
    .end local v2    # "i":I
    :cond_2
    aget-object v5, v4, v8

    .line 958
    .local v5, "str":Ljava/lang/String;
    const-string v10, "OFViewerApp"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "SEF KEY - "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 965
    .end local v5    # "str":Ljava/lang/String;
    .restart local v2    # "i":I
    :cond_3
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    aget-object v9, v4, v2

    invoke-static {v8, v9}, Lcom/sec/android/secvision/sef/SEF;->getSEFData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 966
    .local v1, "data":[B
    aget-object v8, v4, v2

    const-string v9, "FocusShot_1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 968
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "AF RECEIVED FROM SEF, size : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    .line 964
    :cond_4
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 979
    :cond_5
    aget-object v8, v4, v2

    const-string v9, "FocusShot_Meta_Info"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 980
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "META INFO RECEIVED FROM SEF, size : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    new-instance v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    invoke-direct {v8, v1}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;-><init>([B)V

    iput-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    .line 988
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mSelectedFocusBox:[I

    aget v8, v8, v7

    if-ne v8, v7, :cond_7

    .line 989
    iput v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    .line 994
    :goto_5
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v8, v8, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mProcessMode:I

    iput v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    .line 995
    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    const/4 v9, 0x5

    if-eq v8, v9, :cond_6

    .line 996
    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    if-eq v8, v14, :cond_6

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    if-eq v8, v13, :cond_6

    .line 997
    iput v13, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    .line 1000
    :cond_6
    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    iput v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverFocusedArea:I

    .line 1001
    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    iput v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverImageMode:I

    .line 1002
    const-string v8, "yim"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "INFO RECEIVED : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v10, v10, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v10, v10, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mReturnCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1003
    iget-object v10, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v10, v10, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v10, v10, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfInputs:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1002
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 991
    :cond_7
    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    goto :goto_5

    .line 1005
    :cond_8
    aget-object v8, v4, v2

    const-string v9, "FocusShot_Map"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1006
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "DEPTH MAP RECEIVED FROM SEF, size : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->depthData:[B

    goto/16 :goto_4

    .line 1012
    :cond_9
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "keyNames[i] "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v10, v4, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    aget-object v8, v4, v2

    const-string v9, "FocusShot_"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1015
    :try_start_0
    aget-object v8, v4, v2

    const/16 v9, 0xa

    aget-object v10, v4, v2

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1016
    .local v3, "id":I
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "id = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    add-int/lit8 v9, v3, -0x2

    aput-object v1, v8, v9

    .line 1018
    const-string v8, "OFViewerApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "INF RECEIVED FROM SEF, size : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, v1

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    .line 1023
    .end local v3    # "id":I
    :catch_0
    move-exception v8

    goto/16 :goto_4

    .end local v1    # "data":[B
    :cond_a
    move v6, v7

    .line 1031
    goto/16 :goto_3
.end method

.method private isFocusChanged()Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 935
    const-string v1, "OFViewerApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mCurrentMode, mFocusedArea "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    const-string v1, "OFViewerApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mCoverImageMode, mCoverFocusedArea "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverImageMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 937
    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverFocusedArea:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 936
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    if-ne v1, v4, :cond_1

    .line 939
    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverImageMode:I

    if-ne v1, v4, :cond_1

    .line 945
    :cond_0
    :goto_0
    return v0

    .line 941
    :cond_1
    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    iget v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverImageMode:I

    if-ne v1, v2, :cond_2

    .line 942
    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    iget v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverFocusedArea:I

    if-eq v1, v2, :cond_0

    .line 945
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private prepareOutput()V
    .locals 9

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x6

    const/4 v2, 0x0

    .line 1165
    const-string v0, "OFViewerApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "PREPARING OUTPUT BITMAP mode,focused "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/Constants;->getRGBCacheId(II)I

    move-result v8

    .line 1169
    .local v8, "rgbId":I
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    aget-object v0, v0, v8

    if-nez v0, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return-void

    .line 1172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 1173
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    if-eq v0, v4, :cond_2

    .line 1174
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    if-ne v0, v5, :cond_5

    .line 1175
    :cond_2
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    .line 1176
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1175
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    .line 1183
    :cond_3
    :goto_1
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    if-eq v0, v4, :cond_4

    .line 1184
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    if-ne v0, v5, :cond_6

    .line 1185
    :cond_4
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    aget-object v1, v1, v8

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    .line 1186
    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    move v4, v2

    move v5, v2

    .line 1185
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0

    .line 1178
    :cond_5
    iget v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    .line 1179
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 1178
    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 1188
    :cond_6
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    aget-object v1, v1, v8

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    .line 1189
    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    move v4, v2

    move v5, v2

    .line 1188
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto :goto_0
.end method

.method private saveSefOutputAndExit()V
    .locals 25
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 676
    const-string v5, "OFViewerApp"

    const-string v6, "SAVING OUTPUT AND EXITING..."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Lcom/sec/android/ofviewer/DataUtils;->getDateTaken(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v15

    .line 679
    .local v15, "dateTaken":J
    sget-object v19, Lcom/sec/android/ofviewer/Constants;->TEMP_FILE_PATH:Ljava/lang/String;

    .line 680
    .local v19, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v6, 0x1

    iput v6, v5, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 682
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-static {v5, v6}, Lcom/sec/android/ofviewer/Constants;->getRGBCacheId(II)I

    move-result v21

    .line 684
    .local v21, "id":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    move/from16 v0, v21

    if-ne v0, v5, :cond_0

    .line 685
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    const/4 v6, 0x2

    iput v6, v5, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->s32OpMode:I

    .line 688
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    move-object/from16 v0, v19

    iput-object v0, v5, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->savePath:Ljava/lang/String;

    .line 689
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 690
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 691
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v8, v8, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    invoke-static {v5, v6, v7, v8}, Lcom/samsung/app/focusshot/Engine;->FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I

    .line 692
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FINAL FOCUSSHOTPROCESS: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 694
    const-string v5, "TAG"

    const-string v6, "temporary jpeg file was not created... manual creation starts"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    new-instance v2, Landroid/graphics/YuvImage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    const/16 v4, 0x14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    .line 696
    const/4 v7, 0x0

    .line 695
    invoke-direct/range {v2 .. v7}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    .line 697
    .local v2, "img":Landroid/graphics/YuvImage;
    new-instance v18, Ljava/io/File;

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 699
    .local v18, "f":Ljava/io/File;
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->createNewFile()Z

    .line 700
    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 701
    .local v20, "fos":Ljava/io/FileOutputStream;
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-direct {v5, v6, v7, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 v6, 0x64

    move-object/from16 v0, v20

    invoke-virtual {v2, v5, v6, v0}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    .line 702
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->flush()V

    .line 703
    invoke-virtual/range {v20 .. v20}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 708
    .end local v2    # "img":Landroid/graphics/YuvImage;
    .end local v18    # "f":Ljava/io/File;
    .end local v20    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 709
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    if-nez v5, :cond_2

    .line 710
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    .line 798
    :goto_1
    return-void

    .line 704
    .restart local v2    # "img":Landroid/graphics/YuvImage;
    .restart local v18    # "f":Ljava/io/File;
    :catch_0
    move-exception v17

    .line 705
    .local v17, "e":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 714
    .end local v2    # "img":Landroid/graphics/YuvImage;
    .end local v17    # "e":Ljava/io/IOException;
    .end local v18    # "f":Ljava/io/File;
    :cond_2
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 716
    new-instance v14, Landroid/media/ExifInterface;

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 717
    .local v14, "_interface":Landroid/media/ExifInterface;
    const-string v5, "Orientation"

    new-instance v6, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitude:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 719
    const-string v5, "GPSAltitude"

    .line 720
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitude:Ljava/lang/String;

    .line 719
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitudeRef:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 722
    const-string v5, "GPSAltitudeRef"

    .line 723
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitudeRef:Ljava/lang/String;

    .line 722
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsDateStamp:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 725
    const-string v5, "GPSDateStamp"

    .line 726
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsDateStamp:Ljava/lang/String;

    .line 725
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->dateTime:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 728
    const-string v5, "DateTime"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->dateTime:Ljava/lang/String;

    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitude:Ljava/lang/String;

    if-eqz v5, :cond_7

    .line 730
    const-string v5, "GPSLatitude"

    .line 731
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitude:Ljava/lang/String;

    .line 730
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitudeRef:Ljava/lang/String;

    if-eqz v5, :cond_8

    .line 733
    const-string v5, "GPSLatitudeRef"

    .line 734
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitudeRef:Ljava/lang/String;

    .line 733
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitude:Ljava/lang/String;

    if-eqz v5, :cond_9

    .line 736
    const-string v5, "GPSLongitude"

    .line 737
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitude:Ljava/lang/String;

    .line 736
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitudeRef:Ljava/lang/String;

    if-eqz v5, :cond_a

    .line 739
    const-string v5, "GPSLongitudeRef"

    .line 740
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitudeRef:Ljava/lang/String;

    .line 739
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsProcessMethod:Ljava/lang/String;

    if-eqz v5, :cond_b

    .line 742
    const-string v5, "GPSProcessingMethod"

    .line 743
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsProcessMethod:Ljava/lang/String;

    .line 742
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v5, v5, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsTimeStamp:Ljava/lang/String;

    if-eqz v5, :cond_c

    .line 745
    const-string v5, "GPSTimeStamp"

    .line 746
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v6, v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsTimeStamp:Ljava/lang/String;

    .line 745
    invoke-virtual {v14, v5, v6}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    :cond_c
    invoke-virtual {v14}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 749
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "RESTORING EXIF ATTRIBUTES: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 750
    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 749
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 755
    .end local v14    # "_interface":Landroid/media/ExifInterface;
    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    invoke-virtual {v5, v6}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->updateProcessMode(I)V

    .line 756
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$OFParam;->s32Lens:I

    invoke-virtual {v5, v6}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->updateLens(I)V

    .line 757
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    const/4 v5, 0x2

    new-array v7, v5, [I

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    if-nez v5, :cond_e

    const/4 v5, 0x1

    :goto_3
    aput v5, v7, v8

    const/4 v8, 0x1

    .line 758
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    if-nez v5, :cond_f

    const/4 v5, 0x0

    :goto_4
    aput v5, v7, v8

    .line 757
    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->updateSelectedBoxes([I)V

    .line 760
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 761
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    const-string v6, "FocusShot_Meta_Info"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    invoke-virtual {v7}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->getMetaInfo()[B

    move-result-object v7

    .line 762
    const/16 v8, 0x840

    const/16 v23, 0x1

    .line 761
    move/from16 v0, v23

    invoke-static {v5, v6, v7, v8, v0}, Lcom/sec/android/secvision/sef/SEF;->addSEFData(Ljava/lang/String;Ljava/lang/String;[BII)I

    .line 763
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ADDING UPDATED METAINFO TO SEF: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 764
    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 763
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 767
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-static {v5, v0}, Lcom/sec/android/secvision/sef/SEF;->copyAllSEFData(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "COPYING SEF DATA: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 770
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 772
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 774
    .local v22, "toSavePath":Ljava/io/File;
    const/4 v4, 0x0

    .line 775
    .local v4, "inputChannel":Ljava/nio/channels/FileChannel;
    const/4 v3, 0x0

    .line 777
    .local v3, "outputChannel":Ljava/nio/channels/FileChannel;
    :try_start_2
    new-instance v5, Ljava/io/FileInputStream;

    new-instance v6, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v5}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v4

    .line 778
    new-instance v5, Ljava/io/FileOutputStream;

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v3

    .line 779
    const-wide/16 v5, 0x0

    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v7

    invoke-virtual/range {v3 .. v8}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 781
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V

    .line 782
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 784
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    .line 785
    .local v11, "title":Ljava/lang/String;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v12

    .line 787
    .local v12, "imgSize":J
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "RESTORING ORIGINAL FILE:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long v9, v5, v7

    .line 789
    .local v9, "fileModifiedTime":J
    const-string v5, "OFViewerApp"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MODIFIED TIME: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 791
    const-wide/16 v5, -0x1

    cmp-long v5, v15, v5

    if-eqz v5, :cond_d

    .line 792
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 793
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    move-object/from16 v5, p0

    move-wide v7, v15

    invoke-static/range {v5 .. v13}, Lcom/sec/android/ofviewer/DataUtils;->saveImageInfo(Landroid/content/Context;Ljava/lang/String;JJLjava/lang/String;J)V

    .line 795
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "RESTORING DATEFIELDS :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    move-wide/from16 v23, v0

    sub-long v7, v7, v23

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto/16 :goto_1

    .line 751
    .end local v3    # "outputChannel":Ljava/nio/channels/FileChannel;
    .end local v4    # "inputChannel":Ljava/nio/channels/FileChannel;
    .end local v9    # "fileModifiedTime":J
    .end local v11    # "title":Ljava/lang/String;
    .end local v12    # "imgSize":J
    .end local v22    # "toSavePath":Ljava/io/File;
    :catch_1
    move-exception v17

    .line 752
    .restart local v17    # "e":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 757
    .end local v17    # "e":Ljava/io/IOException;
    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 758
    :cond_f
    const/4 v5, 0x1

    goto/16 :goto_4

    .line 780
    .restart local v3    # "outputChannel":Ljava/nio/channels/FileChannel;
    .restart local v4    # "inputChannel":Ljava/nio/channels/FileChannel;
    .restart local v22    # "toSavePath":Ljava/io/File;
    :catchall_0
    move-exception v5

    .line 781
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V

    .line 782
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 783
    throw v5
.end method

.method private setCurrentMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 494
    iput p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    .line 495
    const-string v0, "OFViewerApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CURRENT MODE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->updateAllViews()V

    .line 498
    return-void
.end method

.method private showProgressIcon()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1201
    iput-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    .line 1202
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1204
    :cond_0
    return-void
.end method

.method private updateAllViews()V
    .locals 9

    .prologue
    const v8, 0x7f020015

    const v7, 0x7f02000b

    const v6, 0x7f020001

    const/4 v5, 0x0

    .line 1266
    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    if-nez v3, :cond_0

    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    .line 1267
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1268
    .local v0, "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1269
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1270
    .local v1, "drawableFront":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v1, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1271
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1272
    .local v2, "drawableRear":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v2, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1296
    .end local v0    # "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    .end local v1    # "drawableFront":Landroid/graphics/drawable/Drawable;
    .end local v2    # "drawableRear":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 1273
    :cond_0
    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    if-nez v3, :cond_2

    .line 1274
    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    if-nez v3, :cond_1

    .line 1275
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1276
    .restart local v0    # "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1279
    .end local v0    # "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1280
    .restart local v1    # "drawableFront":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v1, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1281
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1282
    .restart local v2    # "drawableRear":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v2, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1283
    .end local v1    # "drawableFront":Landroid/graphics/drawable/Drawable;
    .end local v2    # "drawableRear":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1284
    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z

    if-nez v3, :cond_3

    .line 1285
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1286
    .restart local v0    # "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1289
    .end local v0    # "drawableAllFocus":Landroid/graphics/drawable/Drawable;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1290
    .restart local v1    # "drawableFront":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFrontFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v1, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1291
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020016

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1292
    .restart local v2    # "drawableRear":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mRearFocus:Landroid/widget/TextView;

    invoke-virtual {v3, v5, v2, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1294
    .end local v1    # "drawableFront":Landroid/graphics/drawable/Drawable;
    .end local v2    # "drawableRear":Landroid/graphics/drawable/Drawable;
    :cond_4
    const-string v3, "OFViewerApp"

    const-string v4, "Unknown SS_MODE"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public cleanResources()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 530
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputData:[B

    .line 531
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->afData:[B

    .line 532
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->infData:[[B

    .line 533
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->depthData:[B

    .line 534
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    .line 535
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    .line 536
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    .line 537
    iput-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    .line 538
    return-void
.end method

.method public doAllInFocus()V
    .locals 12

    .prologue
    const/4 v11, 0x5

    const/4 v7, 0x4

    const/4 v10, 0x0

    .line 876
    const-string v5, "OFViewerApp"

    const-string v6, "ALL IN FOCUS"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    invoke-direct {p0, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V

    .line 878
    iget-boolean v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-nez v5, :cond_1

    .line 879
    iput v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    .line 880
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 932
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v2, v5, Lcom/samsung/app/focusshot/FocusShot;->runParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 884
    .local v2, "sFocalRunParam":Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;
    iput v7, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32ProcessMode:I

    .line 885
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v0, v5, Lcom/samsung/app/focusshot/FocusShot;->aifParam:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 886
    .local v0, "aifParam":Lcom/samsung/app/focusshot/FocusShot$AIFParam;
    const/4 v5, 0x2

    iput v5, v0, Lcom/samsung/app/focusshot/FocusShot$AIFParam;->s32Frames:I

    .line 887
    iput-object v0, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sAllFocus:Lcom/samsung/app/focusshot/FocusShot$AIFParam;

    .line 888
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->ofParam:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    iput-object v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sOutFocus:Lcom/samsung/app/focusshot/FocusShot$OFParam;

    .line 889
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->rfParam:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    iput-object v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->sReFocus:Lcom/samsung/app/focusshot/FocusShot$RFParam;

    .line 890
    iput v7, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumThreads:I

    .line 891
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->info:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    iget-object v5, v5, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v5, v5, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    iput v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->s32NumFaces:I

    .line 892
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iput-object v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    .line 893
    iget-object v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    iput v6, v5, Lcom/samsung/app/focusshot/FocusShot$Point;->s32X:I

    .line 894
    iget-object v5, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->FocusPoint:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->point:Lcom/samsung/app/focusshot/FocusShot$Point;

    iget v6, v6, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    iput v6, v5, Lcom/samsung/app/focusshot/FocusShot$Point;->s32Y:I

    .line 895
    iput v10, v2, Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;->ps32Progresscallback:I

    .line 896
    iput v10, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    .line 897
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusChanged()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 900
    iget v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I

    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    invoke-static {v5, v6}, Lcom/sec/android/ofviewer/Constants;->getRGBCacheId(II)I

    move-result v1

    .line 901
    .local v1, "rgbId":I
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    aget-object v5, v5, v1

    if-nez v5, :cond_2

    .line 902
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 903
    .local v3, "time":J
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 904
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 905
    iget v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    .line 906
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    .line 905
    invoke-static {v5, v2, v10, v6}, Lcom/samsung/app/focusshot/Engine;->FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I

    move-result v5

    iput v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    .line 907
    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->prevEngMode:I

    .line 908
    iput-boolean v10, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    .line 909
    iget-boolean v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    if-nez v5, :cond_0

    .line 911
    const-string v5, "oftime"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "AIF PROCESSING TIME "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v3

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 912
    const-string v7, " ms"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 911
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    const-string v5, "OFViewerApp"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FocusShotProcess returned: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    iget v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    if-nez v5, :cond_2

    .line 915
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    mul-int/2addr v6, v7

    new-array v6, v6, [I

    aput-object v6, v5, v1

    .line 916
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget-object v5, v5, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->ps32PrevBuf:[I

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I

    aget-object v6, v6, v1

    .line 917
    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    iget v9, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    .line 916
    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/ofviewer/DataUtils;->copyRGBArray([I[IIII)V

    .line 920
    .end local v3    # "time":J
    :cond_2
    iget v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I

    if-nez v5, :cond_3

    .line 921
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 923
    :cond_3
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 924
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V

    goto/16 :goto_0

    .line 929
    .end local v1    # "rgbId":I
    :cond_4
    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 930
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V

    goto/16 :goto_0
.end method

.method public finish()V
    .locals 4

    .prologue
    .line 1196
    const-string v0, "OFViewerApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CLOSING APP - FILE: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 1198
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 542
    const-string v0, "OFViewerApp"

    const-string v1, "BACK PRESSED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-nez v0, :cond_1

    .line 544
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    .line 545
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 553
    :cond_0
    :goto_0
    return-void

    .line 548
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    if-nez v0, :cond_0

    .line 549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    .line 550
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x400

    const/4 v9, 0x1

    const/4 v12, 0x0

    .line 281
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 289
    const/high16 v6, 0x7f030000

    invoke-virtual {p0, v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->setContentView(I)V

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "inputfile"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    .line 343
    :goto_0
    return-void

    .line 296
    :cond_0
    const/4 v6, 0x6

    invoke-static {v6}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    move-result v0

    .line 297
    .local v0, "checkDNIe":Z
    const-string v6, "yim"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DNIeSet is "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "inputfile"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    .line 299
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/ofviewer/DataUtils;->getImageRotation(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I

    .line 300
    new-instance v6, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-direct {v6, p0, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mExifInformation:Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;

    .line 301
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/ofviewer/DataUtils;->getDimensions(Ljava/lang/String;)[I

    move-result-object v3

    .line 302
    .local v3, "mWH":[I
    aget v6, v3, v12

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    .line 303
    aget v6, v3, v9

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    .line 304
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    int-to-float v6, v6

    const/high16 v7, 0x44f00000    # 1920.0f

    div-float v5, v6, v7

    .line 305
    .local v5, "widthScale":F
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    int-to-float v6, v6

    const/high16 v7, 0x44870000    # 1080.0f

    div-float v2, v6, v7

    .line 306
    .local v2, "heightScale":F
    invoke-static {v5, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 307
    .local v4, "scale":F
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    int-to-float v6, v6

    div-float/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    .line 308
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    int-to-float v6, v6

    div-float/2addr v6, v4

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    .line 309
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    rem-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_1

    .line 310
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    .line 312
    :cond_1
    const-string v6, "OFViewerApp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "RGB_IMAGE_WIDTH, RGB_IMAGE_HEIGHT "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const-string v6, "OFViewerApp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Original Image Width,Height: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageHeight:I

    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageWidth:I

    invoke-static {v6, v7}, Lcom/samsung/app/focusshot/Engine;->getKernelSize(II)I

    move-result v6

    iput v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDefaultBlur:I

    .line 317
    const-string v6, "OFViewerApp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DEFAULT BLUR: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDefaultBlur:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iput v12, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I

    .line 322
    const/high16 v6, 0x7f0a0000

    invoke-virtual {p0, v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/ofviewer/AnimImageView;

    iput-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mAnimView:Lcom/sec/android/ofviewer/AnimImageView;

    .line 324
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->initAllViews()V

    .line 325
    new-instance v6, Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    invoke-direct {v6, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    iput-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    .line 326
    new-instance v1, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v1, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 327
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v6, "file"

    invoke-virtual {v1, v6}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 328
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    invoke-virtual {p0, v6, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 329
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/secvision/sef/SEF;->isSEFFile(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 330
    iput-boolean v12, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 331
    iput-boolean v9, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    .line 332
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    .line 333
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I

    iget v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I

    invoke-static {v6, v7, v8}, Lcom/sec/android/ofviewer/DataUtils;->decodeFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverBitmap:Landroid/graphics/Bitmap;

    .line 334
    const-string v6, "oftime"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Cover image decode "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J

    sub-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 336
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 337
    new-instance v6, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;)V

    new-array v7, v12, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 339
    :cond_2
    iput-boolean v9, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    .line 340
    const/high16 v6, 0x7f070000

    invoke-static {p0, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 434
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 436
    const v0, 0x7f0a0006

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSave:Landroid/view/MenuItem;

    .line 437
    const v0, 0x7f0a0007

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAs:Landroid/view/MenuItem;

    .line 438
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 7

    .prologue
    .line 502
    const-string v3, "OFViewerApp"

    const-string v4, "DESTROYING ACTIVITY...."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    .line 504
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 505
    .local v1, "exitTime":J
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->inEngineCall:Z

    if-nez v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-eqz v3, :cond_6

    .line 515
    :cond_1
    :goto_1
    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v3, :cond_2

    .line 516
    iget v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I

    invoke-static {v3}, Lcom/samsung/app/focusshot/Engine;->FocusShotDeInit(I)I

    .line 518
    :cond_2
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    if-eqz v3, :cond_3

    .line 519
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    invoke-virtual {p0, v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 520
    :cond_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mReceiver:Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;

    .line 521
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->cleanResources()V

    .line 522
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_4

    .line 523
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 524
    :cond_4
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_5

    .line 525
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 526
    :cond_5
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 527
    return-void

    .line 507
    :cond_6
    const-wide/16 v3, 0x32

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    .line 508
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/16 v5, 0x1388

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    goto :goto_1

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 443
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->onBackPressed()V

    .line 490
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    .line 445
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a0006

    if-ne v1, v2, :cond_5

    .line 446
    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    if-nez v1, :cond_1

    .line 450
    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-nez v1, :cond_3

    .line 451
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    .line 452
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    goto :goto_1

    .line 456
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->outputUpdated:Z

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusChanged()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 457
    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z

    .line 458
    new-instance v0, Lcom/sec/android/ofviewer/SEFViewerActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$3;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 472
    invoke-virtual {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity$3;->start()V

    goto :goto_0

    .line 475
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto :goto_0

    .line 478
    :cond_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0a0007

    if-ne v1, v2, :cond_0

    .line 479
    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    if-nez v1, :cond_1

    .line 482
    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->isFocusShotEngineInitialized:Z

    if-nez v1, :cond_6

    .line 483
    invoke-direct {p0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V

    .line 484
    const/16 v1, 0xa

    iput v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I

    goto :goto_1

    .line 487
    :cond_6
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
