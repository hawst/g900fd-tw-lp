.class Lcom/sec/android/ofviewer/ZoomImageView$1;
.super Ljava/lang/Object;
.source "ZoomImageView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/ZoomImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/ZoomImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/ZoomImageView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 55
    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # invokes: Lcom/sec/android/ofviewer/ZoomImageView;->isEffectInProgress()Z
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$0(Lcom/sec/android/ofviewer/ZoomImageView;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v5, v6

    .line 89
    :goto_0
    return v5

    .line 57
    :cond_0
    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$1(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/view/ScaleGestureDetector;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 58
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    invoke-direct {v0, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 60
    .local v0, "curr":Landroid/graphics/PointF;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 87
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    iget-object v6, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v6}, Lcom/sec/android/ofviewer/ZoomImageView;->access$9(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/ofviewer/ZoomImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 88
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-virtual {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->invalidate()V

    move v5, v7

    .line 89
    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 63
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->initPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->access$3(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;
    invoke-static {v6}, Lcom/sec/android/ofviewer/ZoomImageView;->access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 64
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-static {v5, v7}, Lcom/sec/android/ofviewer/ZoomImageView;->access$4(Lcom/sec/android/ofviewer/ZoomImageView;I)V

    goto :goto_1

    .line 68
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mState:I
    invoke-static {v6}, Lcom/sec/android/ofviewer/ZoomImageView;->access$5(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 69
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float v3, v6, v8

    .line 70
    .local v3, "xDiff":F
    iget v6, v0, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float v4, v6, v8

    .line 71
    .local v4, "yDiff":F
    iget-object v6, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    iget v6, v6, Lcom/sec/android/ofviewer/ZoomImageView;->origWidth:F

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v8

    mul-float/2addr v6, v8

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$7(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v8

    int-to-float v8, v8

    cmpg-float v6, v6, v8

    if-gtz v6, :cond_2

    move v1, v5

    .line 72
    .local v1, "transX":F
    :goto_2
    iget-object v6, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    iget v6, v6, Lcom/sec/android/ofviewer/ZoomImageView;->origHeight:F

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v8

    mul-float/2addr v6, v8

    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I
    invoke-static {v8}, Lcom/sec/android/ofviewer/ZoomImageView;->access$8(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v8

    int-to-float v8, v8

    cmpg-float v6, v6, v8

    if-gtz v6, :cond_3

    move v2, v5

    .line 73
    .local v2, "transY":F
    :goto_3
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->access$9(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 74
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-virtual {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->correctTranslate()V

    .line 75
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;
    invoke-static {v5}, Lcom/sec/android/ofviewer/ZoomImageView;->access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;

    move-result-object v5

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v8, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v5, v6, v8}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_1

    .end local v1    # "transX":F
    .end local v2    # "transY":F
    :cond_2
    move v1, v3

    .line 71
    goto :goto_2

    .restart local v1    # "transX":F
    :cond_3
    move v2, v4

    .line 72
    goto :goto_3

    .line 80
    .end local v1    # "transX":F
    .end local v3    # "xDiff":F
    .end local v4    # "yDiff":F
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-static {v5, v6}, Lcom/sec/android/ofviewer/ZoomImageView;->access$4(Lcom/sec/android/ofviewer/ZoomImageView;I)V

    goto/16 :goto_1

    .line 83
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView$1;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-static {v5, v6}, Lcom/sec/android/ofviewer/ZoomImageView;->access$4(Lcom/sec/android/ofviewer/ZoomImageView;I)V

    goto/16 :goto_1

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
