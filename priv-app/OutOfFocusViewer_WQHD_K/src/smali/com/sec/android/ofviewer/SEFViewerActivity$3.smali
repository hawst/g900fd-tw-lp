.class Lcom/sec/android/ofviewer/SEFViewerActivity$3;
.super Ljava/lang/Thread;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$3;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    .line 458
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 461
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$3;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$15(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 462
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 463
    .local v1, "time":J
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$3;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->saveSefOutputAndExit()V
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$34(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 464
    const-string v3, "oftime"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TOTAl SAVING TIME... "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 465
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 464
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    .end local v1    # "time":J
    :goto_0
    return-void

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 468
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$3;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto :goto_0
.end method
