.class Lcom/sec/android/ofviewer/SEFViewerActivity$2;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    .line 1298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x4

    .line 1302
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iget-boolean v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    if-eqz v0, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return-void

    .line 1304
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1306
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$17(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-nez v0, :cond_2

    .line 1307
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1308
    :cond_2
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$19(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 1309
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->doReFocus()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$20(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_0

    .line 1315
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$17(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-ne v0, v2, :cond_3

    .line 1316
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1317
    :cond_3
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {v0, v2}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$19(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 1318
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->doReFocus()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$20(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_0

    .line 1324
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1325
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$2;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->doAllInFocus()V

    goto :goto_0

    .line 1304
    :pswitch_data_0
    .packed-switch 0x7f0a0003
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
