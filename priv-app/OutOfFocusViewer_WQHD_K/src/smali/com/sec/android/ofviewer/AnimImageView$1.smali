.class Lcom/sec/android/ofviewer/AnimImageView$1;
.super Ljava/lang/Object;
.source "AnimImageView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/AnimImageView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/AnimImageView;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/AnimImageView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v2, 0x8

    .line 81
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/ofviewer/AnimImageView;->setEnabled(Z)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/AnimImageView;->access$0(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 83
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/AnimImageView;->access$1(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 84
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/ofviewer/AnimImageView;->isAnimating:Z

    .line 85
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/AnimImageView;->access$0(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->mImageView2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/AnimImageView;->access$1(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->mImageView1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/AnimImageView;->access$0(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    # getter for: Lcom/sec/android/ofviewer/AnimImageView;->animBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/ofviewer/AnimImageView;->access$2(Lcom/sec/android/ofviewer/AnimImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 88
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 77
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/ofviewer/AnimImageView;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/ofviewer/AnimImageView$1;->this$0:Lcom/sec/android/ofviewer/AnimImageView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/ofviewer/AnimImageView;->isAnimating:Z

    .line 72
    return-void
.end method
