.class Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

.field private tmpString:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 1

    .prologue
    .line 1376
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1378
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->tmpString:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;)V
    .locals 0

    .prologue
    .line 1376
    invoke-direct {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/16 v5, 0x32

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1382
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v3

    .line 1384
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1383
    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1385
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v5, :cond_2

    .line 1386
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->tmpString:Ljava/lang/String;

    .line 1396
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 1384
    goto :goto_0

    .line 1389
    :cond_2
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    .line 1390
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/Object;

    .line 1391
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    .line 1390
    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1388
    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1391
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1392
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->tmpString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1393
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;->tmpString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 1401
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 1406
    return-void
.end method
