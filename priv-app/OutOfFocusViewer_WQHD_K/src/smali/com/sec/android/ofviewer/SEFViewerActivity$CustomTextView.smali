.class public Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextView;
.super Landroid/widget/TextView;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomTextView"
.end annotation


# static fields
.field private static final DISABLED_ALPHA:F = 0.4f


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 1415
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1416
    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1420
    invoke-super {p0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1421
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextView;->setAlpha(F)V

    .line 1422
    return-void

    .line 1421
    :cond_0
    const v0, 0x3ecccccd    # 0.4f

    goto :goto_0
.end method
