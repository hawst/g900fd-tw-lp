.class Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LocalBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1363
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 1366
    const-string v1, "OFViewerApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BROADCAST RECEIVED: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1368
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$14(Lcom/sec/android/ofviewer/SEFViewerActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1369
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1370
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$LocalBroadcastReceiver;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    .line 1373
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-void
.end method
