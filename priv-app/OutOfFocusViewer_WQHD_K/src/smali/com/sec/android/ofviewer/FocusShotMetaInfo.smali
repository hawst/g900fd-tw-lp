.class public Lcom/sec/android/ofviewer/FocusShotMetaInfo;
.super Ljava/lang/Object;
.source "FocusShotMetaInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;,
        Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;
    }
.end annotation


# instance fields
.field mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

.field mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

.field private metaBuffer:[B


# direct methods
.method public constructor <init>([B)V
    .locals 8
    .param p1, "metadata"    # [B

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x6

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    .line 15
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 16
    .local v0, "bis":Ljava/io/ByteArrayInputStream;
    new-instance v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    invoke-direct {v3, p0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;-><init>(Lcom/sec/android/ofviewer/FocusShotMetaInfo;)V

    iput-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    .line 17
    new-instance v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-direct {v3, p0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;-><init>(Lcom/sec/android/ofviewer/FocusShotMetaInfo;)V

    iput-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    .line 18
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->signature:I

    .line 19
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->ver_major:I

    .line 20
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->ver_minor:I

    .line 21
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->header_length:I

    .line 22
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfInputs:I

    .line 23
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mProcessMode:I

    .line 24
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mAFIdx:I

    .line 25
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mINFIdx:I

    .line 26
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mRefFrame1:I

    .line 27
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mRefFrame2:I

    .line 28
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mAligned:I

    .line 29
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mWidth:I

    .line 30
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mHeight:I

    .line 31
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    .line 32
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v6, :cond_0

    .line 37
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v4, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v5

    aput v5, v3, v4

    .line 38
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v4, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v5

    aput v5, v3, v4

    .line 39
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mPanCode:I

    .line 40
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMacroCode:I

    .line 41
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v6, :cond_2

    .line 44
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v6, :cond_3

    .line 47
    const/4 v1, 0x0

    :goto_3
    if-lt v1, v6, :cond_4

    .line 50
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMapNameSize:I

    .line 51
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMapNameSize:I

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readString(Ljava/io/ByteArrayInputStream;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMapName:Ljava/lang/String;

    .line 52
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mReturnCode:I

    .line 53
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mLens:I

    .line 54
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mBloom:I

    .line 55
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mBloomthresh:I

    .line 56
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    iput v4, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mPeakIndex:I

    .line 58
    const/4 v1, 0x0

    :goto_4
    const/16 v3, 0x15

    if-lt v1, v3, :cond_5

    .line 64
    const/4 v1, 0x0

    :goto_5
    if-lt v1, v7, :cond_7

    .line 70
    const/4 v1, 0x0

    :goto_6
    if-lt v1, v7, :cond_9

    .line 74
    return-void

    .line 33
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_7
    const/16 v3, 0xd

    if-lt v2, v3, :cond_1

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :cond_1
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFaceInfo:[[I

    aget-object v3, v3, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    aput v4, v3, v2

    .line 33
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 42
    .end local v2    # "j":I
    :cond_2
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    aput v4, v3, v1

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    :cond_3
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFileNameSize:[I

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    aput v4, v3, v1

    .line 44
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 48
    :cond_4
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFrameNameList:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFileNameSize:[I

    aget v4, v4, v1

    invoke-virtual {p0, v0, v4}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readString(Ljava/io/ByteArrayInputStream;I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 59
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_8
    if-lt v2, v7, :cond_6

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 60
    :cond_6
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mPhaseData:[[S

    aget-object v3, v3, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readShort(Ljava/io/ByteArrayInputStream;)S

    move-result v4

    aput-short v4, v3, v2

    .line 59
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 65
    .end local v2    # "j":I
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_9
    const/4 v3, 0x4

    if-lt v2, v3, :cond_8

    .line 64
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 66
    :cond_8
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocusBox:[[I

    aget-object v3, v3, v1

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    aput v4, v3, v2

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 71
    .end local v2    # "j":I
    :cond_9
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mSelectedFocusBox:[I

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->readInt(Ljava/io/ByteArrayInputStream;)I

    move-result v4

    aput v4, v3, v1

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method


# virtual methods
.method public firstHeader()Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    return-object v0
.end method

.method public getMetaInfo()[B
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    return-object v0
.end method

.method public readInt(Ljava/io/ByteArrayInputStream;)I
    .locals 4
    .param p1, "bis"    # Ljava/io/ByteArrayInputStream;

    .prologue
    .line 200
    const/4 v2, 0x4

    new-array v1, v2, [B

    .line 201
    .local v1, "data":[B
    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 202
    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 203
    const/4 v2, 0x2

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 204
    const/4 v2, 0x3

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 205
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 206
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 207
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    return v2
.end method

.method public readShort(Ljava/io/ByteArrayInputStream;)S
    .locals 4
    .param p1, "bis"    # Ljava/io/ByteArrayInputStream;

    .prologue
    .line 211
    const/4 v2, 0x2

    new-array v1, v2, [B

    .line 212
    .local v1, "data":[B
    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 213
    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 214
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 215
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 216
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    return v2
.end method

.method public readString(Ljava/io/ByteArrayInputStream;I)Ljava/lang/String;
    .locals 4
    .param p1, "bis"    # Ljava/io/ByteArrayInputStream;
    .param p2, "len"    # I

    .prologue
    .line 187
    new-array v0, p2, [B

    .line 189
    .local v0, "data":[B
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayInputStream;->read([B)I

    .line 190
    rsub-int v2, p2, 0x80

    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Ljava/io/ByteArrayInputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    :goto_0
    return-object v2

    .line 191
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 193
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public secondHeader()Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x3

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v0, "builder":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "signature "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->signature:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ver_major "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->ver_major:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ver_minor "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->ver_minor:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "header_length "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader1:Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$FirstHeader;->header_length:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mNumberOfInputs "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfInputs:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mProcessMode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mProcessMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mAFIdx "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mAFIdx:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mINFIdx "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mINFIdx:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mRefFrame1 "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mRefFrame1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mRefFrame2 "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mRefFrame2:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mAligned "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mAligned:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mWidth "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mHeight "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mNumberOfFaces "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mPanCode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mPanCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mMacroCode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMacroCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mHeader2.mTouch[0] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mHeader2.mTouch[1] "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v7, :cond_0

    .line 147
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v7, :cond_1

    .line 152
    const/4 v1, 0x0

    :goto_2
    if-lt v1, v6, :cond_2

    .line 159
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v3, v3, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mNumberOfFaces:I

    if-lt v1, v3, :cond_4

    .line 165
    const/4 v1, 0x0

    :goto_4
    if-lt v1, v6, :cond_6

    .line 170
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mMapName "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mMapName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mReturnCode "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mReturnCode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mLens "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mLens:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mBloom "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mBloom:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mBloomthresh "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mBloomthresh:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 145
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mFocalCodes["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 148
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mInputFrameNameList["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFrameNameList:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 149
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 148
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 153
    :cond_2
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_5
    const/4 v3, 0x4

    if-lt v2, v3, :cond_3

    .line 152
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 154
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mFocusBox+["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocusBox:[[I

    aget-object v4, v4, v1

    aget v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 155
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 154
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 160
    .end local v2    # "j":I
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_6
    const/16 v3, 0xd

    if-lt v2, v3, :cond_5

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    .line 161
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mHeader2.mFaceInfo["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 162
    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFaceInfo:[[I

    aget-object v4, v4, v1

    aget v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 166
    .end local v2    # "j":I
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mHeader2.mSelectedFocusBox["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 167
    iget-object v4, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->mHeader2:Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;

    iget-object v4, v4, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mSelectedFocusBox:[I

    aget v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 166
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_4
.end method

.method public updateLens(I)V
    .locals 5
    .param p1, "lens"    # I

    .prologue
    const/4 v3, 0x4

    .line 234
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 235
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 236
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 237
    new-array v1, v3, [B

    .line 238
    .local v1, "value":[B
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 239
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 240
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x538

    const/4 v4, 0x0

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 241
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x539

    const/4 v4, 0x1

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 242
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x53a

    const/4 v4, 0x2

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 243
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x53b

    const/4 v4, 0x3

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 244
    return-void
.end method

.method public updateProcessMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .prologue
    const/4 v3, 0x4

    .line 220
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 221
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 222
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 223
    new-array v1, v3, [B

    .line 224
    .local v1, "value":[B
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 225
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 226
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x14

    const/4 v4, 0x0

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 227
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x15

    const/4 v4, 0x1

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 228
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x16

    const/4 v4, 0x2

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 229
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x17

    const/4 v4, 0x3

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 231
    return-void
.end method

.method public updateSelectedBoxes([I)V
    .locals 6
    .param p1, "focus"    # [I

    .prologue
    const/16 v3, 0x8

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 247
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 248
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 249
    aget v2, p1, v4

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 250
    aget v2, p1, v5

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 251
    new-array v1, v3, [B

    .line 252
    .local v1, "value":[B
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 253
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 254
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5f6

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 255
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5f7

    aget-byte v4, v1, v5

    aput-byte v4, v2, v3

    .line 256
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5f8

    const/4 v4, 0x2

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 257
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5f9

    const/4 v4, 0x3

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 258
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5fa

    const/4 v4, 0x4

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 259
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5fb

    const/4 v4, 0x5

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 260
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5fc

    const/4 v4, 0x6

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 261
    iget-object v2, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo;->metaBuffer:[B

    const/16 v3, 0x5fd

    const/4 v4, 0x7

    aget-byte v4, v1, v4

    aput-byte v4, v2, v3

    .line 262
    return-void
.end method
