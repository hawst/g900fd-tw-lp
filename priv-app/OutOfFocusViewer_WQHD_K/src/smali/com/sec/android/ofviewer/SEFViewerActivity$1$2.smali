.class Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity$1;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 167
    sub-int v7, p3, p2

    const/16 v8, 0x64

    if-le v7, v8, :cond_0

    .line 168
    add-int/lit8 p3, p2, 0x64

    .line 169
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 170
    .local v6, "validTxt":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/ofviewer/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 171
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v7

    const v8, 0x7f070009

    .line 172
    const/4 v9, 0x0

    .line 171
    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    .line 172
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 173
    const-string v7, ""

    .line 214
    :goto_0
    return-object v7

    .line 175
    :cond_1
    const/4 v4, 0x0

    .line 176
    .local v4, "invalidFlag":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v7

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$11(Lcom/sec/android/ofviewer/SEFViewerActivity;)[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    if-lt v1, v7, :cond_2

    .line 209
    if-eqz v4, :cond_6

    .line 210
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v7

    const v8, 0x7f070009

    .line 211
    const/4 v9, 0x0

    .line 210
    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    .line 211
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    move-object v7, v6

    .line 212
    goto :goto_0

    .line 177
    :cond_2
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v5, v7, :cond_3

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 178
    :cond_3
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v7

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$11(Lcom/sec/android/ofviewer/SEFViewerActivity;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 179
    .local v3, "index":I
    if-ltz v3, :cond_5

    .line 180
    const/4 v4, 0x1

    .line 181
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v3, v7, :cond_4

    .line 184
    new-instance v7, Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v6, v8, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 185
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 184
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 177
    :cond_4
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 192
    :cond_5
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;->this$1:Lcom/sec/android/ofviewer/SEFViewerActivity$1;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;

    move-result-object v7

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$11(Lcom/sec/android/ofviewer/SEFViewerActivity;)[Ljava/lang/String;

    move-result-object v7

    aget-object v7, v7, v1

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 193
    .local v0, "c":C
    const/16 v7, 0x21

    if-lt v0, v7, :cond_4

    const/16 v7, 0x7e

    if-ge v0, v7, :cond_4

    const/16 v7, 0x3f

    if-eq v0, v7, :cond_4

    .line 194
    const v7, 0xfee0

    add-int/2addr v7, v0

    int-to-char v0, v7

    .line 195
    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 196
    .local v2, "iDBC":I
    if-ltz v2, :cond_4

    .line 197
    const/4 v4, 0x1

    .line 198
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 201
    new-instance v7, Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    invoke-virtual {v6, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 202
    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 201
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    .line 214
    .end local v0    # "c":C
    .end local v2    # "iDBC":I
    .end local v3    # "index":I
    .end local v5    # "j":I
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0
.end method
