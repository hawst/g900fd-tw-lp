.class Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ZoomImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/ZoomImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyScaleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/ZoomImageView;


# direct methods
.method private constructor <init>(Lcom/sec/android/ofviewer/ZoomImageView;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/ofviewer/ZoomImageView;Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;-><init>(Lcom/sec/android/ofviewer/ZoomImageView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 104
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # invokes: Lcom/sec/android/ofviewer/ZoomImageView;->isEffectInProgress()Z
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$0(Lcom/sec/android/ofviewer/ZoomImageView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    const/4 v2, 0x0

    .line 125
    :goto_0
    return v2

    .line 106
    :cond_0
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 107
    .local v0, "mScaleFactor":F
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v1

    .line 108
    .local v1, "origScale":F
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v3

    mul-float/2addr v3, v0

    invoke-static {v2, v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$10(Lcom/sec/android/ofviewer/ZoomImageView;F)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_3

    .line 110
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-static {v2, v5}, Lcom/sec/android/ofviewer/ZoomImageView;->access$10(Lcom/sec/android/ofviewer/ZoomImageView;F)V

    .line 111
    div-float v0, v5, v1

    .line 117
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    iget v2, v2, Lcom/sec/android/ofviewer/ZoomImageView;->origWidth:F

    iget-object v3, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$7(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    iget v2, v2, Lcom/sec/android/ofviewer/ZoomImageView;->origHeight:F

    iget-object v3, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$8(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    .line 118
    :cond_2
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$9(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/Matrix;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/ZoomImageView;->access$7(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    .line 119
    iget-object v4, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I
    invoke-static {v4}, Lcom/sec/android/ofviewer/ZoomImageView;->access$8(Lcom/sec/android/ofviewer/ZoomImageView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    .line 118
    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 124
    :goto_2
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-virtual {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->correctTranslate()V

    .line 125
    const/4 v2, 0x1

    goto :goto_0

    .line 112
    :cond_3
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_1

    .line 113
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    invoke-static {v2, v4}, Lcom/sec/android/ofviewer/ZoomImageView;->access$10(Lcom/sec/android/ofviewer/ZoomImageView;F)V

    .line 114
    div-float v0, v4, v1

    goto :goto_1

    .line 121
    :cond_4
    iget-object v2, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # getter for: Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v2}, Lcom/sec/android/ofviewer/ZoomImageView;->access$9(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    .line 122
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    .line 121
    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_2
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    # invokes: Lcom/sec/android/ofviewer/ZoomImageView;->isEffectInProgress()Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/ZoomImageView;->access$0(Lcom/sec/android/ofviewer/ZoomImageView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x0

    .line 99
    :goto_0
    return v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;->this$0:Lcom/sec/android/ofviewer/ZoomImageView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/ZoomImageView;->access$4(Lcom/sec/android/ofviewer/ZoomImageView;I)V

    .line 99
    const/4 v0, 0x1

    goto :goto_0
.end method
