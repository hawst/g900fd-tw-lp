.class public final Lcom/sec/android/ofviewer/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final BACKGROUND:I = 0x1

.field public static final DUMP_SEF_FILES:Z = false

.field public static final EDIT_TEXT_MAX_LEN:I = 0x32

.field public static final FOREGROUND:I = 0x0

.field public static final SEF_DUMP_PATH:Ljava/lang/String;

.field public static final SS_FORMAT_Y:I = 0xc

.field public static final SS_FORMAT_YUYV422:I = 0x1

.field public static final SS_MODE_ALLINFOCUS:I = 0x4

.field public static final SS_MODE_OUTFOCUS:I = 0x1

.field public static final SS_MODE_OUTFOCUSFAST:I = 0x2

.field public static final SS_MODE_OUTFOCUS_GALLERY:I = 0x5

.field public static final SS_MODE_REFOCUS:I = 0x3

.field public static final SS_MODE_REVERSEFOCUS:I = 0x6

.field public static final SS_MODE_UNKNOWN:I

.field public static final TEMP_FILE_PATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 23
    const-string v1, "/ofs_tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/ofviewer/Constants;->TEMP_FILE_PATH:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 29
    const-string v1, "/sefdump/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/ofviewer/Constants;->SEF_DUMP_PATH:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRGBCacheId(II)I
    .locals 3
    .param p0, "mode"    # I
    .param p1, "focusRegion"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    const/4 v2, 0x4

    if-ne p0, v2, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 72
    goto :goto_0

    .line 73
    :cond_2
    if-ne p1, v1, :cond_0

    .line 78
    const/4 v0, 0x2

    goto :goto_0
.end method
