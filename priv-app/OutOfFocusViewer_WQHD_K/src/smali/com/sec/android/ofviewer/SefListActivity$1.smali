.class Lcom/sec/android/ofviewer/SefListActivity$1;
.super Ljava/lang/Object;
.source "SefListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SefListActivity;->populateFiles()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SefListActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SefListActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SefListActivity$1;->this$0:Lcom/sec/android/ofviewer/SefListActivity;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SefListActivity$1;->this$0:Lcom/sec/android/ofviewer/SefListActivity;

    const-class v2, Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "inputfile"

    iget-object v2, p0, Lcom/sec/android/ofviewer/SefListActivity$1;->this$0:Lcom/sec/android/ofviewer/SefListActivity;

    iget-object v2, v2, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    aget-object v2, v2, p3

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    iget-object v1, p0, Lcom/sec/android/ofviewer/SefListActivity$1;->this$0:Lcom/sec/android/ofviewer/SefListActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/ofviewer/SefListActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method
