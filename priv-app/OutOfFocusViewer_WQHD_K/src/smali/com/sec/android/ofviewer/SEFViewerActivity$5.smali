.class Lcom/sec/android/ofviewer/SEFViewerActivity$5;
.super Ljava/lang/Thread;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;->doReFocus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

.field private final synthetic val$sFocalRunParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iput-object p2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->val$sFocalRunParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 835
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 838
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$17(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/ofviewer/Constants;->getRGBCacheId(II)I

    move-result v0

    .line 839
    .local v0, "rgbId":I
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$36(Lcom/sec/android/ofviewer/SEFViewerActivity;)[[I

    move-result-object v3

    aget-object v3, v3, v0

    if-nez v3, :cond_1

    .line 840
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 841
    .local v1, "time":J
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->showProgressIcon()V
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$15(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 842
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$37(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V

    .line 843
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->psHandle:I
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$38(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->val$sFocalRunParam:Lcom/samsung/app/focusshot/FocusShot$FocalRunParam;

    .line 844
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$39(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/samsung/app/focusshot/FocusShot;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    .line 843
    invoke-static {v4, v5, v8, v6}, Lcom/samsung/app/focusshot/Engine;->FocusShotProcess(ILcom/samsung/app/focusshot/FocusShot$FocalRunParam;ILcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;)I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$40(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 845
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {v3, v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$41(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 846
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {v3, v8}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$37(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V

    .line 847
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$28(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 867
    .end local v1    # "time":J
    :goto_0
    return-void

    .line 849
    .restart local v1    # "time":J
    :cond_0
    const-string v4, "oftime"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusedArea:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$17(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "OF"

    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 850
    const-string v3, " PROCESSING TIME "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v1

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " ms"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 849
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    const-string v3, "OFViewerApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "FocusShotProcess returned: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I
    invoke-static {v5}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$42(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$42(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v3

    if-nez v3, :cond_1

    .line 853
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$36(Lcom/sec/android/ofviewer/SEFViewerActivity;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$43(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I
    invoke-static {v5}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$44(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v5

    mul-int/2addr v4, v5

    new-array v4, v4, [I

    aput-object v4, v3, v0

    .line 854
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mFocusShot:Lcom/samsung/app/focusshot/FocusShot;
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$39(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/samsung/app/focusshot/FocusShot;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot;->focusShotProc:Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;

    iget-object v3, v3, Lcom/samsung/app/focusshot/FocusShot$FocusShotProcImgMode;->ps32PrevBuf:[I

    .line 855
    iget-object v4, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->rgbCache:[[I
    invoke-static {v4}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$36(Lcom/sec/android/ofviewer/SEFViewerActivity;)[[I

    move-result-object v4

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_WIDTH:I
    invoke-static {v5}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$43(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->RGB_IMAGE_HEIGHT:I
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$44(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v6

    .line 856
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mImageRotation:I
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$45(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v7

    .line 854
    invoke-static {v3, v4, v5, v6, v7}, Lcom/sec/android/ofviewer/DataUtils;->copyRGBArray([I[IIII)V

    .line 860
    .end local v1    # "time":J
    :cond_1
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mReturnCode:I
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$42(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v3

    if-nez v3, :cond_3

    .line 861
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$31(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 849
    .restart local v1    # "time":J
    :cond_2
    const-string v3, "ROF"

    goto/16 :goto_1

    .line 863
    .end local v1    # "time":J
    :cond_3
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$31(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 864
    iget-object v3, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$5;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto/16 :goto_0
.end method
