.class Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;
.super Ljava/lang/Object;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExifInformation"
.end annotation


# instance fields
.field dateTime:Ljava/lang/String;

.field gpsAltitude:Ljava/lang/String;

.field gpsAltitudeRef:Ljava/lang/String;

.field gpsDateStamp:Ljava/lang/String;

.field gpsLatitude:Ljava/lang/String;

.field gpsLatitudeRef:Ljava/lang/String;

.field gpsLongitude:Ljava/lang/String;

.field gpsLongitudeRef:Ljava/lang/String;

.field gpsProcessMethod:Ljava/lang/String;

.field gpsTimeStamp:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Ljava/lang/String;)V
    .locals 3
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 1343
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1346
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 1347
    .local v0, "_interface":Landroid/media/ExifInterface;
    const-string v2, "DateTime"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->dateTime:Ljava/lang/String;

    .line 1348
    const-string v2, "GPSAltitude"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitude:Ljava/lang/String;

    .line 1349
    const-string v2, "GPSAltitudeRef"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsAltitudeRef:Ljava/lang/String;

    .line 1350
    const-string v2, "GPSDateStamp"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsDateStamp:Ljava/lang/String;

    .line 1351
    const-string v2, "GPSLatitude"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitude:Ljava/lang/String;

    .line 1352
    const-string v2, "GPSLatitudeRef"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLatitudeRef:Ljava/lang/String;

    .line 1353
    const-string v2, "GPSLongitude"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitude:Ljava/lang/String;

    .line 1354
    const-string v2, "GPSLongitudeRef"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsLongitudeRef:Ljava/lang/String;

    .line 1355
    const-string v2, "GPSProcessingMethod"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsProcessMethod:Ljava/lang/String;

    .line 1356
    const-string v2, "GPSTimeStamp"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$ExifInformation;->gpsTimeStamp:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1360
    .end local v0    # "_interface":Landroid/media/ExifInterface;
    :goto_0
    return-void

    .line 1357
    :catch_0
    move-exception v1

    .line 1358
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
