.class public Lcom/sec/android/ofviewer/ZoomImageView;
.super Landroid/widget/ImageView;
.source "ZoomImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;
    }
.end annotation


# static fields
.field private static final MAX_SCALE:F = 2.0f

.field private static final STATE_DRAG:I = 0x1

.field private static final STATE_NONE:I = 0x0

.field private static final STATE_ZOOM:I = 0x2


# instance fields
.field private currScale:F

.field private initPoint:Landroid/graphics/PointF;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPrevViewHeight:I

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mState:I

.field private mValues:[F

.field private mViewHeight:I

.field private mViewWidth:I

.field protected origHeight:F

.field protected origWidth:F

.field private prevPoint:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mState:I

    .line 22
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;

    .line 23
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->initPoint:Landroid/graphics/PointF;

    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    .line 51
    new-instance v0, Lcom/sec/android/ofviewer/ZoomImageView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/ofviewer/ZoomImageView$1;-><init>(Lcom/sec/android/ofviewer/ZoomImageView;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 33
    invoke-direct {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->initPinchZoom()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mState:I

    .line 22
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;

    .line 23
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->initPoint:Landroid/graphics/PointF;

    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    .line 51
    new-instance v0, Lcom/sec/android/ofviewer/ZoomImageView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/ofviewer/ZoomImageView$1;-><init>(Lcom/sec/android/ofviewer/ZoomImageView;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->initPinchZoom()V

    .line 39
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/ofviewer/ZoomImageView;)Z
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->isEffectInProgress()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/view/ScaleGestureDetector;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$10(Lcom/sec/android/ofviewer/ZoomImageView;F)V
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    return-void
.end method

.method static synthetic access$2(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->prevPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->initPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/ofviewer/ZoomImageView;I)V
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mState:I

    return-void
.end method

.method static synthetic access$5(Lcom/sec/android/ofviewer/ZoomImageView;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mState:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/ofviewer/ZoomImageView;)F
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    return v0
.end method

.method static synthetic access$7(Lcom/sec/android/ofviewer/ZoomImageView;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/ofviewer/ZoomImageView;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    return v0
.end method

.method static synthetic access$9(Lcom/sec/android/ofviewer/ZoomImageView;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private initPinchZoom()V
    .locals 4

    .prologue
    .line 42
    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 43
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;-><init>(Lcom/sec/android/ofviewer/ZoomImageView;Lcom/sec/android/ofviewer/ZoomImageView$MyScaleGestureListener;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 44
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    .line 45
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mValues:[F

    .line 46
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/ZoomImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 47
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/ZoomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/ZoomImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 49
    return-void
.end method

.method private isEffectInProgress()Z
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/ofviewer/SEFViewerActivity;

    iget-boolean v0, v0, Lcom/sec/android/ofviewer/SEFViewerActivity;->effectInProgress:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/sec/android/ofviewer/AnimImageView;

    iget-boolean v0, v0, Lcom/sec/android/ofviewer/AnimImageView;->isAnimating:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private reset()V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 175
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    if-eqz v8, :cond_0

    .line 176
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    if-nez v8, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 179
    .local v1, "bmpWidth":I
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 180
    .local v0, "bmpHeight":I
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    int-to-float v8, v8

    int-to-float v9, v1

    div-float v6, v8, v9

    .line 181
    .local v6, "scaleX":F
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    int-to-float v8, v8

    int-to-float v9, v0

    div-float v7, v8, v9

    .line 182
    .local v7, "scaleY":F
    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 183
    .local v5, "scale":F
    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 184
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    int-to-float v8, v8

    int-to-float v9, v0

    mul-float/2addr v9, v5

    sub-float/2addr v8, v9

    div-float v4, v8, v10

    .line 185
    .local v4, "marginTop":F
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    int-to-float v8, v8

    int-to-float v9, v1

    mul-float/2addr v9, v5

    sub-float/2addr v8, v9

    div-float v3, v8, v10

    .line 186
    .local v3, "marginLeft":F
    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 187
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    int-to-float v8, v8

    mul-float v9, v10, v3

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->origWidth:F

    .line 188
    iget v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    int-to-float v8, v8

    mul-float v9, v10, v4

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->origHeight:F

    .line 189
    iget-object v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v8}, Lcom/sec/android/ofviewer/ZoomImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 190
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mState:I

    .line 191
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->correctTranslate()V

    goto :goto_0
.end method


# virtual methods
.method correctTranslate()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 148
    iget-object v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mValues:[F

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->getValues([F)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mValues:[F

    const/4 v5, 0x2

    aget v2, v4, v5

    .line 150
    .local v2, "xTrans":F
    iget-object v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mValues:[F

    const/4 v5, 0x5

    aget v3, v4, v5

    .line 152
    .local v3, "yTrans":F
    iget v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/ofviewer/ZoomImageView;->origWidth:F

    iget v6, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    mul-float/2addr v5, v6

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/ofviewer/ZoomImageView;->getCorrectedTranslate(FFF)F

    move-result v0

    .line 153
    .local v0, "correctXTrans":F
    iget v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/ofviewer/ZoomImageView;->origHeight:F

    iget v6, p0, Lcom/sec/android/ofviewer/ZoomImageView;->currScale:F

    mul-float/2addr v5, v6

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/ofviewer/ZoomImageView;->getCorrectedTranslate(FFF)F

    move-result v1

    .line 155
    .local v1, "correctYTrans":F
    cmpl-float v4, v0, v7

    if-nez v4, :cond_0

    cmpl-float v4, v1, v7

    if-eqz v4, :cond_1

    .line 156
    :cond_0
    iget-object v4, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 157
    :cond_1
    return-void
.end method

.method getCorrectedTranslate(FFF)F
    .locals 3
    .param p1, "t"    # F
    .param p2, "vSize"    # F
    .param p3, "conSize"    # F

    .prologue
    .line 132
    cmpg-float v2, p3, p2

    if-gtz v2, :cond_0

    .line 133
    const/4 v1, 0x0

    .line 134
    .local v1, "transMin":F
    sub-float v0, p2, p3

    .line 140
    .local v0, "transMax":F
    :goto_0
    cmpg-float v2, p1, v1

    if-gez v2, :cond_1

    .line 141
    neg-float v2, p1

    add-float/2addr v2, v1

    .line 144
    :goto_1
    return v2

    .line 136
    .end local v0    # "transMax":F
    .end local v1    # "transMin":F
    :cond_0
    sub-float v1, p2, p3

    .line 137
    .restart local v1    # "transMin":F
    const/4 v0, 0x0

    .restart local v0    # "transMax":F
    goto :goto_0

    .line 142
    :cond_1
    cmpl-float v2, p1, v0

    if-lez v2, :cond_2

    .line 143
    neg-float v2, p1

    add-float/2addr v2, v0

    goto :goto_1

    .line 144
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 161
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 162
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    .line 163
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    .line 164
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    if-eqz v0, :cond_0

    .line 165
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mPrevViewHeight:I

    iget v1, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mPrevViewHeight:I

    iget v1, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    if-ne v0, v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    iput v0, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mPrevViewHeight:I

    .line 168
    const-string v0, "nay"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "viewWidth,viewHeight: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/ofviewer/ZoomImageView;->mViewHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    invoke-direct {p0}, Lcom/sec/android/ofviewer/ZoomImageView;->reset()V

    goto :goto_0
.end method
