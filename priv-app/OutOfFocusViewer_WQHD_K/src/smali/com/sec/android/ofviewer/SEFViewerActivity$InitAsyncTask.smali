.class Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;
.super Landroid/os/AsyncTask;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;)V
    .locals 0

    .prologue
    .line 345
    invoke-direct {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v7, 0x1

    .line 350
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$21(Lcom/sec/android/ofviewer/SEFViewerActivity;J)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->initSEF()V
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$22(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 352
    const-string v1, "oftime"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Parsing Sef "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mTime:J
    invoke-static {v5}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$23(Lcom/sec/android/ofviewer/SEFViewerActivity;)J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 354
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$24(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 358
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$24(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->initOutFocusEngine()V
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$26(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 369
    :cond_1
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$27(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 370
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 372
    :cond_2
    const/4 v1, 0x0

    return-object v1

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {v1, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$25(Lcom/sec/android/ofviewer/SEFViewerActivity;Z)V

    .line 364
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v2, -0x1

    .line 377
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 378
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->exitingApp:Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$28(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    :goto_0
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$27(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$24(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mQueue:I
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$27(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 405
    :goto_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-static {v0, v2}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$32(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAll:Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$24(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 408
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    const/high16 v1, 0x7f070000

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 410
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->finish()V

    goto :goto_0

    .line 386
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->doReFocus()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$20(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_1

    .line 389
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$29(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 390
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->doAllInFocus()V

    goto :goto_1

    .line 392
    :cond_2
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    const/4 v1, 0x3

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V
    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$30(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 393
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_1

    .line 397
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    .line 398
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->onBackPressed()V

    goto :goto_1

    .line 401
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$31(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 402
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->hideProgressIcon()V
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$1(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_1

    .line 413
    :cond_3
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisableAIF:Z
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$29(Lcom/sec/android/ofviewer/SEFViewerActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 414
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mAllFocus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$33(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 421
    :cond_4
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v1

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V
    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$30(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    goto/16 :goto_0

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0xa -> :sswitch_3
    .end sparse-switch
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 426
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    iget-object v1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCurrentMode:I
    invoke-static {v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$18(Lcom/sec/android/ofviewer/SEFViewerActivity;)I

    move-result v1

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->setCurrentMode(I)V
    invoke-static {v0, v1}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$30(Lcom/sec/android/ofviewer/SEFViewerActivity;I)V

    .line 428
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sec/android/ofviewer/SEFViewerActivity$InitAsyncTask;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
