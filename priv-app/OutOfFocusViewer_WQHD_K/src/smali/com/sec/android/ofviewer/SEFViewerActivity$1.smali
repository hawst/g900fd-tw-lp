.class Lcom/sec/android/ofviewer/SEFViewerActivity$1;
.super Landroid/os/Handler;
.source "SEFViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/SEFViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/ofviewer/SEFViewerActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    .line 116
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)Lcom/sec/android/ofviewer/SEFViewerActivity;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v7, 0x7f0a0001

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 119
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 275
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 121
    :pswitch_1
    new-instance v6, Lcom/sec/android/ofviewer/SEFViewerActivity$1$1;

    invoke-direct {v6, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$1;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)V

    .line 131
    invoke-virtual {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$1;->start()V

    goto :goto_0

    .line 134
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->disableAllViews()V
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$3(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_0

    .line 138
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # invokes: Lcom/sec/android/ofviewer/SEFViewerActivity;->enableAllViews()V
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$4(Lcom/sec/android/ofviewer/SEFViewerActivity;)V

    goto :goto_0

    .line 142
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mAnimView:Lcom/sec/android/ofviewer/AnimImageView;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$5(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/sec/android/ofviewer/AnimImageView;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 143
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mAnimView:Lcom/sec/android/ofviewer/AnimImageView;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$5(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/sec/android/ofviewer/AnimImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mDisplayBmp:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$6(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/AnimImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 147
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mAnimView:Lcom/sec/android/ofviewer/AnimImageView;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$5(Lcom/sec/android/ofviewer/SEFViewerActivity;)Lcom/sec/android/ofviewer/AnimImageView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mCoverBitmap:Landroid/graphics/Bitmap;
    invoke-static {v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$7(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/ofviewer/AnimImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 150
    :pswitch_6
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 151
    iget-object v7, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    const v8, 0x7f070005

    invoke-virtual {v7, v8}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 152
    const v7, 0x7f07000b

    invoke-virtual {v6, v7, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 153
    const v7, 0x7f07000a

    invoke-virtual {v6, v7, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 154
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    new-instance v7, Landroid/widget/EditText;

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {v7, v8}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-static {v6, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$8(Lcom/sec/android/ofviewer/SEFViewerActivity;Landroid/widget/EditText;)V

    .line 155
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    new-instance v7, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;

    iget-object v8, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-direct {v7, v8, v10}, Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity;Lcom/sec/android/ofviewer/SEFViewerActivity$CustomTextWatcher;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 156
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mDateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$10(Lcom/sec/android/ofviewer/SEFViewerActivity;)Ljava/text/SimpleDateFormat;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 157
    .local v2, "defaultName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v9, v7}, Landroid/widget/EditText;->setSelection(II)V

    .line 160
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    .line 161
    const/high16 v7, 0x2000000

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 163
    new-instance v3, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;

    invoke-direct {v3, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$2;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)V

    .line 217
    .local v3, "filter":Landroid/text/InputFilter;
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    const-string v7, "inputType=filename"

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 218
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/text/InputFilter;

    aput-object v3, v7, v9

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 220
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 221
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$12(Lcom/sec/android/ofviewer/SEFViewerActivity;Landroid/app/AlertDialog;)V

    .line 222
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 223
    const/4 v7, 0x5

    .line 222
    invoke-virtual {v6, v7}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 224
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 225
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsEditText:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$9(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/widget/EditText;

    move-result-object v6

    .line 226
    invoke-virtual {v6}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 225
    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 227
    .local v5, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    invoke-virtual {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060002

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 228
    .local v4, "margin":I
    invoke-virtual {v5, v4, v4, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 229
    iget-object v6, p0, Lcom/sec/android/ofviewer/SEFViewerActivity$1;->this$0:Lcom/sec/android/ofviewer/SEFViewerActivity;

    # getter for: Lcom/sec/android/ofviewer/SEFViewerActivity;->mSaveAsDialog:Landroid/app/AlertDialog;
    invoke-static {v6}, Lcom/sec/android/ofviewer/SEFViewerActivity;->access$13(Lcom/sec/android/ofviewer/SEFViewerActivity;)Landroid/app/AlertDialog;

    move-result-object v6

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 230
    .local v0, "b":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;

    invoke-direct {v6, p0}, Lcom/sec/android/ofviewer/SEFViewerActivity$1$3;-><init>(Lcom/sec/android/ofviewer/SEFViewerActivity$1;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
