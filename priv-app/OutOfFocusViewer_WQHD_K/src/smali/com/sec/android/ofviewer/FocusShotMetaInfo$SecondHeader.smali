.class public Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;
.super Ljava/lang/Object;
.source "FocusShotMetaInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/ofviewer/FocusShotMetaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SecondHeader"
.end annotation


# instance fields
.field mAFIdx:I

.field mAligned:I

.field mBloom:I

.field mBloomthresh:I

.field mFaceInfo:[[I

.field mFocalCodes:[I

.field mFocusBox:[[I

.field mHeight:I

.field mINFIdx:I

.field mInputFileNameSize:[I

.field mInputFrameNameList:[Ljava/lang/String;

.field mLens:I

.field mMacroCode:I

.field mMapName:Ljava/lang/String;

.field mMapNameSize:I

.field mNumberOfFaces:I

.field mNumberOfInputs:I

.field mPanCode:I

.field mPeakIndex:I

.field mPhaseData:[[S

.field mProcessMode:I

.field mRefFrame1:I

.field mRefFrame2:I

.field mReturnCode:I

.field mSelectedFocusBox:[I

.field mTouch:[I

.field mWidth:I

.field final synthetic this$0:Lcom/sec/android/ofviewer/FocusShotMetaInfo;


# direct methods
.method public constructor <init>(Lcom/sec/android/ofviewer/FocusShotMetaInfo;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x6

    .line 83
    iput-object p1, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->this$0:Lcom/sec/android/ofviewer/FocusShotMetaInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/16 v0, 0xd

    filled-new-array {v2, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFaceInfo:[[I

    .line 100
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mTouch:[I

    .line 103
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocalCodes:[I

    .line 104
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFileNameSize:[I

    .line 105
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mInputFrameNameList:[Ljava/lang/String;

    .line 111
    const/16 v0, 0x15

    filled-new-array {v0, v3}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[S

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mPhaseData:[[S

    .line 117
    const/4 v0, 0x4

    filled-new-array {v3, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mFocusBox:[[I

    .line 118
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/ofviewer/FocusShotMetaInfo$SecondHeader;->mSelectedFocusBox:[I

    return-void
.end method
