.class public Lcom/sec/android/ofviewer/SefListActivity;
.super Landroid/app/Activity;
.source "SefListActivity.java"


# instance fields
.field files:[Ljava/io/File;

.field private fromRestart:Z

.field listView:Landroid/widget/ListView;

.field private mHandler:Landroid/os/Handler;

.field private mSefFolderPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/Camera"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->mSefFolderPath:Ljava/lang/String;

    .line 26
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->mHandler:Landroid/os/Handler;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->fromRestart:Z

    .line 18
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SefListActivity;->requestWindowFeature(I)Z

    .line 32
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->listView:Landroid/widget/ListView;

    .line 33
    iget-object v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sec/android/ofviewer/SefListActivity;->setContentView(Landroid/view/View;)V

    .line 34
    return-void
.end method

.method protected onRestart()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->fromRestart:Z

    .line 87
    return-void
.end method

.method protected onStart()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 71
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->fromRestart:Z

    if-nez v0, :cond_0

    .line 72
    iget-object v2, p0, Lcom/sec/android/ofviewer/SefListActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/ofviewer/SefListActivity$2;

    invoke-direct {v3, p0}, Lcom/sec/android/ofviewer/SefListActivity$2;-><init>(Lcom/sec/android/ofviewer/SefListActivity;)V

    .line 79
    iget-boolean v0, p0, Lcom/sec/android/ofviewer/SefListActivity;->fromRestart:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x1f4

    :goto_0
    int-to-long v4, v0

    .line 72
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 80
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/ofviewer/SefListActivity;->fromRestart:Z

    .line 81
    return-void

    :cond_1
    move v0, v1

    .line 79
    goto :goto_0
.end method

.method protected populateFiles()V
    .locals 7

    .prologue
    .line 37
    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->mSefFolderPath:Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    .line 39
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    array-length v5, v5

    if-nez v5, :cond_2

    .line 40
    :cond_0
    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->mSefFolderPath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 41
    .local v4, "sefFolder":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 42
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 44
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No Files in "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/sec/android/ofviewer/SefListActivity;->mSefFolderPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 45
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 64
    .end local v4    # "sefFolder":Ljava/io/File;
    :goto_0
    return-void

    .line 48
    :cond_2
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    array-length v5, v5

    new-array v2, v5, [Ljava/lang/String;

    .line 49
    .local v2, "fileNames":[Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    array-length v5, v5

    if-lt v3, v5, :cond_3

    .line 52
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 53
    const v5, 0x1090003

    .line 52
    invoke-direct {v0, p0, v5, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 54
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->listView:Landroid/widget/ListView;

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->listView:Landroid/widget/ListView;

    new-instance v6, Lcom/sec/android/ofviewer/SefListActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/ofviewer/SefListActivity$1;-><init>(Lcom/sec/android/ofviewer/SefListActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    .line 50
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    :cond_3
    iget-object v5, p0, Lcom/sec/android/ofviewer/SefListActivity;->files:[Ljava/io/File;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    .line 49
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
