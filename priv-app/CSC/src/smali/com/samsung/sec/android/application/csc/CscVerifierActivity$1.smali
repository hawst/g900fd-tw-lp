.class Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;
.super Landroid/os/Handler;
.source "CscVerifierActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;


# direct methods
.method constructor <init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 76
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$000()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sCscVerifierActivity:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$000()Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    if-eq v3, v4, :cond_2

    .line 77
    :cond_0
    const-string v3, "CscVerifierActivity"

    const-string v4, "handleMessage() : CscVerifierActivity instance is wrong..."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 85
    :cond_2
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-boolean v1, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    .line 87
    .local v1, "bUpdated":Z
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$100()Ljava/util/Queue;

    move-result-object v4

    monitor-enter v4

    .line 88
    const/4 v2, 0x0

    .line 89
    .local v2, "sp":Landroid/text/SpannableStringBuilder;
    :goto_1
    :try_start_0
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextQueue:Ljava/util/Queue;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$100()Ljava/util/Queue;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/text/SpannableStringBuilder;

    move-object v2, v0

    if-eqz v2, :cond_3

    .line 90
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-object v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiTv:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 91
    const/4 v1, 0x1

    goto :goto_1

    .line 95
    :cond_3
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$200()Landroid/text/SpannableStringBuilder;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 98
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-object v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiResultTv:Landroid/widget/TextView;

    # getter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;
    invoke-static {}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$200()Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 99
    const/4 v3, 0x0

    # setter for: Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->sTextResult:Landroid/text/SpannableStringBuilder;
    invoke-static {v3}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->access$202(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    .line 100
    const/4 v1, 0x1

    .line 106
    :cond_4
    if-eqz v1, :cond_1

    .line 107
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-boolean v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvHeight:I

    if-nez v3, :cond_6

    .line 108
    :cond_5
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-object v4, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    const/4 v5, 0x1

    iput v5, v4, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvHeight:I

    iput v5, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mnUiSvOffset:I

    .line 110
    :cond_6
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mbUiSvUpdate:Z

    .line 112
    iget-object v3, p0, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;->this$0:Lcom/samsung/sec/android/application/csc/CscVerifierActivity;

    iget-object v3, v3, Lcom/samsung/sec/android/application/csc/CscVerifierActivity;->mUiSv:Landroid/widget/ScrollView;

    new-instance v4, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1$1;

    invoke-direct {v4, p0}, Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1$1;-><init>(Lcom/samsung/sec/android/application/csc/CscVerifierActivity$1;)V

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
